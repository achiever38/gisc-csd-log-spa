﻿define(["dojo/_base/declare"], function (declre) {
    var _instance = null;
    var _cls = null;

    _cls = declre(null, {

        /* enable debug log */
        debugLog: false,

        /*Dev Local Testing*/
        sendbackOrigin: "*",

        /*Print Map Endpoint*/
        printMapURL: printMapEndPoint,
        printRouteMapURL: printRouteMapEndPoint,

        /* Theme Logo Loading Path */
        loadingPath: "../gis-js/images/comp-logo/",

        /* User Information : set from SPA app */
        userInfo: null,

        /* User Permission : set from SPA app */
        userPermission: null,

        /* Map Provider Data : get from MapProvider */
        mapProviderData: null,

        /*Map object for Measurement / PolygonEditor / DeleteGraphic */
        mapObject: null,
        mapManager: null,

        /* Bookmark Data */
        bookmarkData: [],

        /* computed from MapManager */
        lstBasemapState: null,
        lstLayerState: null,

        /* vehicle tracking global track */
        IsFollowTrackable: true,

        /* reset filter when track change */
        isResetFilter: false,

        /* Hide MIS Menu when click on map*/
        isClickMapAndHideMenu: true,

        /* Street view URL */
        streetViewUrl: "https://maps.google.com/maps?q=&layer=c&cbll=",

        /* GoogleMap API Key*/
        GOOGLEAPIKEY: "AIzaSyDZ9pRpGh8knAGC_yvfcf2Q5pPAT5OWlxE", //supakorn key

        //Custom POI - Show POI Cluster?
        showPoiCluster: true,

        /* Map Setting */
        // defaultMapInitialize: {
        //     center: [100.530602, 13.723114],
        //     zoom: 15
        // },

        defaultMapExtent: {
            "xmin":100.49069072985564,
            "ymin":13.704540253806444,
            "xmax":100.57051327014095,
            "ymax":13.741686275948116,
            "spatialReference":{
                "wkid":4326
            }
        },

        constantKeys:{
            "defaultIconHeight":"defaultIconHeight",
            "defaultIconWidth":"defaultIconWidth",
            "defaultTextSize":"defaultTextSize"
        },

        haloColor: [255, 255, 255],
        haloSize: 2,

        currentLocatinPin: {
            url: "./gis-js/images/cursor_current_location.png",
            width: 18,
            height: 18
        },

        clickMapPin: {
            url: "./gis-js/images/pin_location.png",
            width: 35,
            height: 50,
            offsetX: 0,
            offsetY: 23.5
        },

        locationInfoPin: {
            url: "./gis-js/images/pin_location_info.png",
            width: 35,
            height: 50,
            offsetX: 0,
            offsetY: 23.5
        },

        bufferConfig: {
            defaultBufferFillColor: "#ff0000",
            defaultBufferFillOpacity: 0.6,

            defaultBufferBorderColor: "#aa0000",
            defaultBufferBorderOpacity: 0.8,
            defaultBufferBorderWidth: 2,

            defaultBufferRadius: 200, //KM
            defaultBufferUnit: "meters", //KM
        },

        customPOIDefaultExpandFactor: 2,
        customPOIMinimumLevelShow: 0,

        currentLocationZoomLevel: 16,

        measurementSetting: {
            pointSymbol: {
                url: "./gis-js/images/pin_location.png",
                width: 35,
                height: 50,
                offsetX: 0,
                offsetY: 23.5
            },

            lineSymbol: {
                lineColor: "#fff800",
                lineOpacity: 1,
                lineWidth: 5, //px

                pointColor: "#ffbe00", //px
                pointOpacity: 1,
                pointSize: 12,

                destinationPointColor: "#be1e2d", //px
                destinationPointSize: 18,
            },

            polygonSymbol: {
                lineColor: "#fff800",
                lineOpacity: 1,
                lineWidth: 5, //px

                pointColor: "#ffbe00", //px
                pointOpacity: 1,
                pointSize: 12,

                fillColor: "#fff800",
                fillOpacity: 0.2,
            }
        },

        defaultGraphicLayerList: [ //desc is a comment :)
            { id: "lyr-custom-area-layer", onclick: false, desc: "Custom Area Layer" },
            { id: "lyr-custom-area-label-layer", onclick: false, desc: "Custom Area Label Layer" },
            { id: "lyr-custom-route-layer", onclick: false, desc: "Custom Route Layer" },

            { id: "lyr-measurement-drawing-wg", onclick: false, desc: "Measurement Operation GP Layer" },
            { id: "lyr-measurement-wg", onclick: false, desc: "Measurement GP Layer" },
            
            { id: "lyr-draw-main-area-command", onclick: false, desc: "Polygon Drawing Main Area",context:"MainArea" },
            { id: "lyr-draw-sub-area-command", onclick: false, desc: "Polygon Drawing Sub Area",context:"SubArea" },

            { id: "lyr-draw-command", onclick: false, desc: "Drawing Command GP Layer" },

            { id: "lyr-playback-buffer-command", onclick: false, desc: "Playback Route Buffer GP Layer" },
            { id: "lyr-playback-route-command", onclick: false, desc: "Playback Route GP Layer" },
            { id: "lyr-playback-stop-command", onclick: true, context: "vehicle", desc: "Playback Stop Point GP Layer", onmouseover: true, showInfo: true, enableResize: true },
            { id: "lyr-playback-alert-command", onclick: false, context: "vehicle", desc: "Playback Alert Point GP Layer", showInfo: true, enableResize: true },
            { id: "lyr-playback-alert-chart-command", onclick: false, context: "vehicle", desc: "Playback Alert Point GP Layer from Chart", showInfo: true },
            
            
            { id: "lyr-polygon-drawing-POIArea", onclick: false, desc: "Polygon Drawing Tools GP Layer",context:"POIArea" },
            { id: "lyr-polygon-drawing-tools", onclick: false, desc: "Polygon Drawing Tools GP Layer" },
            { id: "lyr-draw-buffer-command", onclick: false, desc: "Drawing Buffer Command GP Layer" },
            //          { id: "lyr-draw-command", onclick: false, desc: "Drawing Command GP Layer" },
            { id: "lyr-draw-route", onclick: false, desc: "Drawing Route GP Layer" },
            { id: "lyr-draw-alert", onclick: true, desc:"Drawing Alert GP Layer", context: "alert", showInfo: true},

            { id: "lyr-custom-poi-layer", visible: false, onclick: true, desc: "Custom POI Layer", lyrType: "cluster", context: "poi"},
            { id: "lyr-custom-poi-single-label-layer", onclick: false, desc: "Custom POI Label Layer" },
            { id: "lyr-custom-poi-label-layer", onclick: false, desc: "Custom POI Label Layer" },

            { id: "lyr-custom-poi-suggestion-nearby", onclick: false, desc: "Custom POI Suggestion Nearby Layer", showInfo: true },            
            { id: "lyr-custom-poi-suggestion", onclick: true, desc: "Custom POI Suggestion Layer", context: "poiSuggestion"},

            { id: "lyr-playback-animate-command", onclick: false, desc: "Playback Animate GP Layer" },

            { id: "lyr-shipment-poi-area-info", onclick: false, desc: "Shipment POI Area GP Layer", showInfo: true,  }, 
            { id: "lyr-shipment-plan", onclick: false, desc: "Shipment Plan GP Layer" }, //Champเพิ่ม
            { id: "lyr-shipment-actual", onclick: false, desc: "Shipment Actual GP Layer" }, //Champเพิ่ม
            { id: "lyr-shipment-poi-plan", onclick: false, desc: "Shipment Plan POI GP Layer", showInfo: true  }, //Champเพิ่ม
            { id: "lyr-shipment-poi-actual", onclick: false, desc: "Shipment Actual POI GP Layer", showInfo: true }, //Champเพิ่ม
            { id: "lyr-shipment-waiting", onclick: false, desc: "Shipment Waiting GP Layer", showInfo: true },
            { id: "lyr-shipment-poi-waiting", onclick: false, desc: "Shipment Waiting POI GP Layer", showInfo: true },
            { id: "lyr-shipment-vehicle", onclick: false, desc: "Shipment Vehicle GP Layer", showInfo: true, onclickDisplayInfo: true  }, //Champเพิ่ม

            { id: "lyr-draw-poi", onclick: false, desc: "Drawing POI GP Layer" },
            { id: "lyr-track-command", onclick: true, desc: "Track Vehicle GP Layer", context: "vehicle", onmouseover: true, showInfo: true, enableResize: true, onclickDisplayInfo: true },
            { id: "lyr-drawing-track-command", onclick: true, desc: "Drawing Vehicle GP Layer", context: "vehicle", onmouseover: true, onmouseover: true, showInfo: true },

            { id: "lyr-current-location", onclick: false, desc: "Current Location Pin Layer" },
            { id: "lyr-goto-xy", onclick: false, desc: "Goto XY Pin Layer" },
            { id: "lyr-location-info", onclick: false, desc: "Location Info Pin Layer" },

            { id: "lyr-custom-poi-non-cluster", onclick: true, desc: "Custom POI Graphic Layer", lyrType: "non-cluster-graphics", },
            { id: "lyr-custom-poi-non-cluster-single-labael-layer", onclick: false, desc: "Custom POI Label Layer" },
            { id: "lyr-custom-poi-non-cluster-label-layer", onclick: false, desc: "Custom POI Label Layer" },
            { id: "lyr-track-nearest", onclick: true, desc: "Track Vehicle GP Layer", context: "vehicle", onmouseover: true, showInfo: true,onclickDisplayInfo: true },
            

        ],

        /* Command Setting */
        commandOptions: {
            "zoom-point": {},

            "draw-one-point-zoom": { layer: "lyr-draw-command" },
            "draw-point-main-area": { layer: "lyr-draw-command" },
            "draw-one-line-zoom": { layer: "lyr-draw-command" },
            "draw-one-polygon-zoom": { layer: "lyr-draw-command" },
            "draw-polygon-main-area": { layer: "lyr-draw-main-area-command" },
            "draw-polygon-sub-area": { layer: "lyr-draw-sub-area-command" },
            "draw-multiple-polygon-zoom": { layer: "lyr-draw-command" },
            "draw-multiple-line-zoom": { layer: "lyr-draw-command" },
            "draw-multiple-vehicles": { layer: "lyr-drawing-track-command" },
            "draw-route-buffer": { layer: "lyr-draw-buffer-command" },
            "draw-point-buffer": { layer: "lyr-draw-buffer-command" },
            "draw-custom-area": { layer: "lyr-draw-command" },
            "draw-multiple-point": { layer: "lyr-draw-poi" },
            "draw-multiple-polygon-zoom-area":{layer: "lyr-draw-command"},

            "draw-alert-point": { layer: "lyr-draw-alert" },

            "draw-one-route": { layer: { route: "lyr-draw-route", stop: "lyr-draw-poi" } },

            "enable-draw-polygon-area":{layer:"lyr-polygon-drawing-POIArea"},
            "enable-draw-polygon": { layer: "lyr-polygon-drawing-tools" },
            "enable-click-map": {},

            "cancel-click-map": {},
            "cancel-draw-polygon": {},
            "cancel-draw-line": {},

            "track-vehicles": { layer: "lyr-track-command" },
            "remove-track-vehicles": { layer: "lyr-track-command" },

            "playback-command": {
                layer: {
                    buffer: "lyr-playback-buffer-command",
                    route: "lyr-playback-route-command",
                    stop: "lyr-playback-stop-command",
                    animate: "lyr-playback-animate-command",
                    alert: "lyr-playback-alert-command",
                    alertChart : "lyr-playback-alert-chart-command"
                }
            },

            "set-startup-resources": {},
            "clear-graphics": {
                layer:
                [
                    "lyr-draw-buffer-command",
                    "lyr-polygon-drawing-tools",
                    "lyr-draw-command",
                    "lyr-draw-alert",
                    "lyr-draw-route",
                    "lyr-playback-buffer-command",
                    "lyr-playback-route-command",
                    "lyr-playback-stop-command",
                    "lyr-playback-alert-command",
                    "lyr-playback-alert-chart-command",
                    // "lyr-track-command",
                    "lyr-drawing-track-command",
                    "lyr-draw-poi",
                    "lyr-measurement-wg",
                    "lyr-goto-xy",

                    "lyr-shipment-plan",
                    "lyr-shipment-actual",
                    "lyr-shipment-poi-plan",
                    "lyr-shipment-poi-actual",
                    "lyr-shipment-waiting",
                    "lyr-shipment-poi-waiting",
                    "lyr-shipment-vehicle",

                    "lyr-custom-poi-suggestion-nearby",
                    "lyr-custom-poi-suggestion",

                    "lyr-track-nearest"
                ]
            },

            "open-shipment-legend": {
                layer: {
                    actual: {
                        line: "lyr-shipment-actual",
                        poi: "lyr-shipment-poi-actual"
                    },
                    plan: {
                        line: "lyr-shipment-plan",
                        poi: "lyr-shipment-poi-plan"
                    },
                    waiting: {
                        line: "lyr-shipment-waiting",
                        poi: "lyr-shipment-poi-waiting"
                    },
                    vehicle: "lyr-shipment-vehicle"
                }
            },

            "draw-shipment-route": {
                layer: {
                    actual: {
                        line: "lyr-shipment-actual",
                        poi: "lyr-shipment-poi-actual"
                    },
                    plan: {
                        line: "lyr-shipment-plan",
                        poi: "lyr-shipment-poi-plan"
                    },
                    waiting: {
                        line: "lyr-shipment-waiting",
                        poi: "lyr-shipment-poi-waiting"
                    }
                }
            },
            "clear-shipment-route": {
                layer: [
                    "lyr-shipment-plan",
                    "lyr-shipment-actual",
                    "lyr-shipment-poi-plan",
                    "lyr-shipment-poi-actual",
                    "lyr-shipment-waiting",
                    "lyr-shipment-poi-waiting"
                ]
            },
            "track-nearest-vehicle":{
                layer:"lyr-track-nearest"
            }
            //"command" : optionObject
        },

        /* WebAPI Setting */
        webAPIConfig: {
            getMapProvider: baseAPINostraUrl + "/api/mapservice/usemap",

            getLocationInfo: baseAPINostraUrl + "/api/location/identify",

            getCustomPOI: baseAPITrackingCoreUrl + "/api/customPoi/summary/list",
            getCustomArea: baseAPITrackingCoreUrl + "/api/customArea/summary/list",

            getCustomPOILightWeight : baseAPITrackingCoreUrl + "/api/customPoi/summary/lightWeight",

            userPreference: baseAPICoreUrl + "/api/user/preferences",
            userPreferenceBookmarkKey: 5
        },

        /* Menu Setting */
        menuConfig: [
            [
                {
                    key: "zoom-in",
                    type: "action",
                    cls: null,
                    svg: "./gis-js/images/svg/icon_toolbar_zoomin.svg",
                    name: "Zoom In"
                },
                {
                    key: "zoom-out",
                    type: "action",
                    cls: null,
                    svg: "./gis-js/images/svg/icon_toolbar_zoomout.svg",
                    name: "Zoom Out"
                },
                {
                    key: "goto-xy",
                    type: "content",
                    cls: "GOTModule/contents/GotoXY",
                    svg: "./gis-js/images/svg/icon_toolbar_goto.svg",
                    name: "Goto XY",
                    resourceKey: "menu-goto-xy-content-header"
                },
                {
                    key: "current-location",
                    type: "action",
                    cls: null,
                    svg: "./gis-js/images/svg/icon_toolbar_currentlocation.svg",
                    name: "Current Location"
                }
            ],
            [
                {
                    key: "basemap",
                    type: "content",
                    cls: "GOTModule/contents/Basemap",
                    svg: "./gis-js/images/svg/icon_toolbar_basemap.svg",
                    name: "Basemap",
                    resourceKey: "menu-basemap-content-header"
                },
                {
                    key: "layers",
                    type: "content",
                    cls: "GOTModule/contents/Layers",
                    svg: "./gis-js/images/svg/icon_toolbar_layer.svg",
                    name: "Layers",
                    resourceKey: "menu-layers-content-header"
                },
                {
                    key: "vehicle",
                    type: "content",
                    cls: "GOTModule/contents/Vehicle",
                    svg: "./gis-js/images/svg/icon_toolbar_vehicle.svg",
                    name: "Vehicle",
                    resourceKey: "menu-vehicle-content-header"
                },
                //{
                //    key: "custom-poi",
                //    type: "toggle",
                //    cls: null,
                //    svg: "./gis-js/images/svg/icon_toolbar_custompoi.svg",
                //    name: "Custom POI"
                //},
                {
                    key: "custom-poi",
                    type: "content",
                    cls: "GOTModule/contents/CustomPOI",
                    svg: "./gis-js/images/svg/icon_toolbar_custompoi.svg",
                    resourceKey: "menu-custom-poi-content-header"
                },
                //{
                //    key: "custom-area",
                //    type: "toggle",
                //    cls: null,
                //    svg: "./gis-js/images/svg/icon_toolbar_customarea.svg",
                //    name: "Custom Area"
                //}
                {
                    key: "custom-area",
                    type: "content",
                    cls: "GOTModule/contents/CustomArea",
                    svg: "./gis-js/images/svg/icon_toolbar_customarea.svg",
                    resourceKey: "menu-custom-area-content-header"
                }
            ],
            [
                {
                    key: "tools",
                    type: "content",
                    cls: "GOTModule/contents/Tools",
                    svg: "./gis-js/images/svg/icon_toolbar_tools.svg",
                    name: "Tools",
                    resourceKey: "menu-tools-content-header"
                },
                {
                    key: "favorite",
                    type: "content",
                    cls: "GOTModule/contents/Favorite",
                    svg: "./gis-js/images/svg/icon_toolbar_favorite.svg",
                    name: "Favorite",
                    resourceKey: "menu-favorite-content-header"
                },
                {
                    key: "clear-graphics",
                    type: "action",
                    cls: null,
                    svg: "./gis-js/images/svg/icon_maptools_delete.svg",
                    name: "Clear Graphics",
                    resourceKey: "menu-clear-graphics-content-header"
                }
            ],
            [
                {
                    key: "shipment-legend",
                    type: "content",
                    cls: "GOTModule/contents/ShipmentLegend",
                    svg: "./gis-js/images/svg/icon_toolbar_shipment.svg",
                    name: "Shipment Legend",
                    resourceKey: "menu-vehicle-content-header"
                },
            ]
            //,[{
            //    key: "dev-test",
            //    type: "content",
            //    cls: "GOTModule/contents/DevTest",
            //    icon: "./gis-js/images/menu/tool.png"
            //}]
        ],

        otherContents: [
            { key: "location-info", cls: "GOTModule/contents/LocationInfo", icon: "", name: "Location Info", resourceKey: "menu-location-info-content-header" },
            { key: "vehicle-info", cls: "GOTModule/contents/VehicleInfo", icon: "", name: "Vehicle Info", resourceKey: "menu-vehicle-info-content-header" },
            { key: "playback-control", cls: "GOTModule/contents/PlaybackControl", icon: "", name: "Playback Control", resourceKey: "menu-playback-content-header" },
            { key: "polygon-editor", cls: "GOTModule/contents/PolygonEditor", icon: "", name: "Polygon Editor", resourceKey: "menu-polygon-editor-content-header" },
            { key: "polygon-editor-poi-area", cls: "GOTModule/contents/PolygonArea", icon: "", name: "POI Area Editor", resourceKey: "menu-polygon-area-content-header" },
            { key: "playback-multiple", cls: "GOTModule/contents/PlaybackMultiple", icon: "", name: "Multiple Playback Control", resourceKey: "menu-multiple-playback-content-header" },
            { key: "shipment-legend", cls: "GOTModule/contents/ShipmentLegend", icon: "", name: "Shipment Legend", resourceKey: "menu-shipment-header-content-header" },
            { key: "liveview-control", cls: "GOTModule/contents/LiveviewControl", icon: "", name: "Liveview Control", resourceKey: "menu-liveview-control-content-header" }
        ],

        contextMenu: {
            POIArea:[
                {key: "location-info", svg: "./gis-js/images/svg/icon_contextmenu_locationonfo.svg"}
            ],
            map: [
                { key: "location-info", svg: "./gis-js/images/svg/icon_contextmenu_locationonfo.svg" },
                { key: "mark-poi", svg: "./gis-js/images/svg/icon_contextmenu_markpoi.svg", permission: "COM0131" },
                { key: "route-from-here", svg: "./gis-js/images/svg/icon_contextmenu_routefromhere.svg", permission: "COM0137" },
                { key: "route-to-here", svg: "./gis-js/images/svg/icon_contextmenu_routetohere.svg", permission: "COM0137" },
                { key: "find-nearest-asset", svg: "./gis-js/images/svg/icon_contextmenu_findnearest.svg", permission: "COM011" },
                { key: "send-to-navigator", svg: "./gis-js/images/svg/icon_contextmenu_sendtonavigate.svg", permission: "COM011" },
                {key : "open-street-view", svg: "./gis-js/images/svg/icon_contextmenu_locationonfo.svg", permission: "GOO001"}
            ],
            poi: [
                //{ key: "location-info", svg: "./gis-js/images/svg/icon_contextmenu_locationonfo.svg" },
                //{ key: "mark-poi", svg: "./gis-js/images/svg/icon_contextmenu_markpoi.svg", permission: "COM0131" },
                { key: "view-detail-poi", svg: "./gis-js/images/svg/icon_contextmenu_viewdetial.svg" },
                //{ key: "route-from-here", svg: "./gis-js/images/svg/icon_contextmenu_routefromhere.svg", permission: "COM0137" },
                { key: "find-nearest-asset", svg: "./gis-js/images/svg/icon_contextmenu_findnearest.svg", permission: "COM011" },
                { key: "send-to-navigator", svg: "./gis-js/images/svg/icon_contextmenu_sendtonavigate.svg", permission: "COM011" },
                { key: "show-radius", svg: "./gis-js/images/svg/icon_contextmenu_showradius.svg" }
            ],
            vehicle: [
                { key: "location-info", svg: "./gis-js/images/svg/icon_contextmenu_locationonfo.svg" },
                { key: "mark-poi", svg: "./gis-js/images/svg/icon_contextmenu_markpoi.svg", permission: "COM0131" },
                { key: "route-from-here", svg: "./gis-js/images/svg/icon_contextmenu_routefromhere.svg", permission: "COM0137" },
                { key: "find-nearest-asset", svg: "./gis-js/images/svg/icon_contextmenu_findnearest.svg", permission: "COM011" },
                { key: "send-to-navigator", svg: "./gis-js/images/svg/icon_contextmenu_sendtonavigate.svg", permission: "COM011" },
                { key: "view-details", svg: "./gis-js/images/svg/icon_contextmenu_viewdetial.svg" },
                { key: "view-playback", svg: "./gis-js/images/svg/icon_contextmenu_viewplayback.svg", permission: "COM0103" }
                //{ key: "live-view", svg: "./gis-js/images/svg/icon_contextmenu_viewplayback.svg" }
            ],
            alert:[
                {key : "open-street-view", svg: "./gis-js/images/svg/icon_contextmenu_locationonfo.svg", permission: "GOO001"}
            ],
            poiSuggestion: [
                { key: "poi-suggestion", svg: "./gis-js/images/svg/icon_contextmenu_markpoi.svg", permission: "COM01319" }
            ],
            waypoint : [
                { key: "show-radius", svg: "./gis-js/images/svg/icon_contextmenu_showradius.svg" }
            ]
        },

        basemapPicConfig: {
            "bm-item-1": "./gis-js/images/svg/basemap_nostra.svg",
            "bm-item-2": "./gis-js/images/svg/basemap_worldimagery.svg",
            "bm-item-3": "./gis-js/images/svg/basemap_streetmap.svg"
        },

        vehicleStatusConfig: [
            //{ key: 1, text: "wg-vehicles-layer-move", svg: "./gis-js/images/svg/icon_toolbar_vehicle.svg", selected: true },
            //{ key: 2, text: "wg-vehicles-layer-stop", svg: "./gis-js/images/svg/icon_toolbar_vehicle.svg", selected: true },
            //{ key: 3, text: "wg-vehicles-layer-park", svg: "./gis-js/images/svg/icon_toolbar_vehicle.svg", selected: true }
            { key: 1, text: "wg-vehicles-layer-move", icon: "./gis-js/images/icon_vehicle_move.png"},
            { key: 2, text: "wg-vehicles-layer-stop", icon: "./gis-js/images/icon_vehicle_stop.png"},
            { key: 3, text: "wg-vehicles-layer-park", icon: "./gis-js/images/icon_vehicle_park.png"},
            { key: 4, text: "wg-vehicles-layer-park-engine-on", icon: "./gis-js/images/icon_vehicle_park_engine_on.png"},
            { key: 5, text: "wg-vehicles-layer-move-over3-5", icon: "./gis-js/images/icon_vehicle_over3.5.png"},
            { key: 6, text: "wg-vehicles-layer-move-over4", icon: "./gis-js/images/icon_vehicle_over4.png"}
        ],

        groupVehicleStatusConfig: [
            {
                id: 1,
                name: "Move",
                text: "wg-vehicles-layer-move",
                items:[
                    { id:11, key: 1, parentId: 1, text: "wg-vehicles-layer-move", icon: "./gis-js/images/icon_vehicle_move.png"},
                    { id:12, key: 5, parentId: 1, text: "wg-vehicles-layer-move-over3-5", icon: "./gis-js/images/icon_vehicle_over3.5.png"},
                    { id:13, key: 6, parentId: 1, text: "wg-vehicles-layer-move-over4", icon: "./gis-js/images/icon_vehicle_over4.png"},
                ]
            },
            {
                id: 2,
                name: "Stop",
                text: "wg-vehicles-layer-stop",
                items:[
                    { id:21, key: 2, parentId: 2, text: "wg-vehicles-layer-stop", icon: "./gis-js/images/icon_vehicle_stop.png"},
                ]
            },
            {
                id: 3,
                name: "Park",
                text: "wg-vehicles-layer-park",
                items:[
                    { id:31, key: 3, parentId: 3, text: "wg-vehicles-layer-park", icon: "./gis-js/images/icon_vehicle_park.png"},
                    { id:32, key: 4, parentId: 3, text: "wg-vehicles-layer-park-engine-on", icon: "./gis-js/images/icon_vehicle_park_engine_on.png"}
                ]
            }
        ],

        currentTrackVehicleFilter: [1, 2, 3, 4, 5, 6],

        animationConfig: {
            animateTime: 3,
            fps: 60,
            maxPlaybackZoomLevel: 13
        },

        playbackFootprintSetting: {
            color: ["#0070EB", "#BE1E2D", "#FFDA00"],
            footprintPin: {
                //url: "./gis-js/images/playback_arrow.png",
                //url: "./gis-js/images/playback_arrow_normal@3x.png",
                url: "./gis-js/images/footprint_move.png",
                width: 24,
                height: 24
            },
            footprintParkPin: {
                url: "./gis-js/images/footprint_park.png",
                width: 24,
                height: 24
            },

            footprintStopPin: {
                //url: "./gis-js/images/playback_arrow.png",
                url: "./gis-js/images/footprint_stop.png",
                width: 24,
                height: 24
            },

            footprintEngineOnPin: {
                //url: "./gis-js/images/playback_arrow.png",
                url: "./gis-js/images/footprint_parkengineon.png",
                width: 24,
                height: 24
            },

            destinationPin: {
                url: "./gis-js/images/pin_destination.png",
                width: 35,
                height: 50,
                offsetX: 0,
                offsetY: 23
            },

            pinBegin: {
                url: "./gis-js/images/pin_destinationB.png",
                width: 35,
                height: 50,
                offsetX: 0,
                offsetY: 23
            },

            pinEnd: {
                url: "./gis-js/images/pin_destinationE.png",
                width: 35,
                height: 50,
                offsetX: 0,
                offsetY: 23
            }
        },

        playbackFullySetting: {
            animateTime: 3,
            fps: 60,
            stepSpeed: [1, 3, 5, 10],

            stepFps:[15,30,60],

            color: ["#0070EB", "#BE1E2D", "#FFDA00"],
            colorDebrief: ["#0070EB", "#BE1E2D"],
            footprintPin: {
                //url: "./gis-js/images/playback_arrow.png",
                //url: "./gis-js/images/playback_arrow_normal@3x.png",
                url: "./gis-js/images/footprint_move.png",
                width: 24,
                height: 24
            },
            footprintParkPin: {
                url: "./gis-js/images/footprint_park.png",
                width: 24,
                height: 24
            },
            footprintStopPin: {
                //url: "./gis-js/images/playback_arrow.png",
                url: "./gis-js/images/footprint_stop.png",
                width: 24,
                height: 24
            },

            footprintEngineOnPin: {
                //url: "./gis-js/images/playback_arrow.png",
                url: "./gis-js/images/footprint_direction.png",
                width: 24,
                height: 24
            },

            destinationPin: {
                url: "./gis-js/images/pin_destination.png",
                width: 35,
                height: 50,
                offsetX: 0,
                offsetY: 23
            },

            pinBegin: {
                url: "./gis-js/images/pin_destinationB.png",
                width: 35,
                height: 50,
                offsetX: 0,
                offsetY: 23
            },

            pinEnd: {
                url: "./gis-js/images/pin_destinationE.png",
                width: 35,
                height: 50,
                offsetX: 0,
                offsetY: 23
            }

        },


        viewonmapConfig: [
            { key: "plan", text: "wg-shipmentlegend-route-plan", icon: "./gis-js/images/icon_route_plan.png", selected: true },
            { key: "actual", text: "wg-shipmentlegend-route-actual", icon: "./gis-js/images/icon_route_actual.png", selected: true },
            { key: "waiting", text: "wg-shipmentlegend-route-waiting", icon: "./gis-js/images/icon_route_waiting.png", selected: true },
            { key: "poiarea", text: "wg-shipmentlegend-poi-area", icon: "./gis-js/images/ic_area1.png", selected: false }
        ],

        isResetFilterViewOnMap: false,


        waypointConfig: {
            routeColor: {
                plan: "#4e4e4e",
                actual:[
                        {color:"#017FFA"},
                        {color:"#FF29CC"},
                        {color:"#471581"},
                        {color:"#4CD65E"},
                        {color:"#AE0252"},
                    ],
               // actual: "#0080fc"
            },
            status : {
                1: {
                    name: "Waiting",
                    key: "Menu_Map_JobWaypointStatus_Waiting",
                    url: "./gis-js/images/waypoint/pin_wp_waiting.png"
                },
                2: {
                    name: "Inside Waypoint",
                    key: "Menu_Map_JobWaypointStatus_InsideWaypoint",
                    url: "./gis-js/images/waypoint/pin_wp_insidewaypoint.png"
                },
                3: {
                    name: "Loading",
                    key: "Menu_Map_JobWaypointStatus_Loading",
                    url: "./gis-js/images/waypoint/pin_wp_loading.png"
                },
                4: {
                    name: "Unloading",
                    key: "Menu_Map_JobWaypointStatus_Unloading",
                    url: "./gis-js/images/waypoint/pin_wp_unloading.png"
                },
                5: {
                    name: "Holding",
                    key: "Menu_Map_JobWaypointStatus_Holding",
                    url: "./gis-js/images/waypoint/pin_wp_holding.png"
                },
                6: {
                    name: "Finished",
                    key: "Menu_Map_JobWaypointStatus_Finished",
                    url: "./gis-js/images/waypoint/pin_wp_finished.png"
                },
                7: {
                    name: "Finished Late",
                    key: "Menu_Map_JobWaypointStatus_FinishedLate",
                    url: "./gis-js/images/waypoint/pin_wp_finishedlate.png"
                },
                8: {
                    name: "Finished Incomplete",
                    key: "Menu_Map_JobWaypointStatus_FinishedIncomplete",
                    url: "./gis-js/images/waypoint/pin_wp_finishedincomplete.png"
                },
                9: {
                    name: "Finished Late Incomplete",
                    key: "Menu_Map_JobWaypointStatus_FinishedLateIncomplete",
                    url: "./gis-js/images/waypoint/pin_wp_finishedlateincomplete.png"
                },
                10: {
                    name: "Cancelled",
                    key: "Menu_Map_JobWaypointStatus_Cancelled",
                    url: "./gis-js/images/waypoint/pin_wp_cancelled.png"
                }
            }

        },

        zoomFactor:[
            50, //lv 0
            50, //lv 1
            50, //lv 2
            50, //lv 3
            50, //lv 4
            50, //lv 5
            75, //lv 6
            75, //lv 7
            75, //lv 8
            75, //lv 9
            75, //lv 10
            75, //lv 11
            75, //lv 12
            75, //lv 13
            100, //lv 14
            100, //lv 15
            100, //lv 16
            100, //lv 17
            100, //lv 18
            100, //lv 19
        ],

        cluster: {
            classbreakSymbol: "picture", 
            classbreak: [
                {
                    name: "blue", url: "./gis-js/images/cluster/cluster_01.png", width: 30, height: 30, min : 0, max: 1
                },
                {
                    name: "green", url: "./gis-js/images/cluster/cluster_02.png", width: 40, height: 40, min : 2, max: 200
                },
                {
                    name: "red", url: "./gis-js/images/cluster/cluster_03.png", width: 80, height: 80, min : 201, max: 1000
                },
                {
                    name: "pink", url: "./gis-js/images/cluster/cluster_04.png", width: 120, height: 120, min : 1001, max: 10000
                },
                {
                    name: "orange", url: "./gis-js/images/cluster/cluster_05.png", width: 160, height: 160, min: 10001, max: 999999
                }
            ],
            //classbreakSymbol: "simple", 
            //classbreak: [
            //    {
            //        name:"blue" , class : "simple", type: "circle", size: 10, borderColor: [0,0,0], borderSize: 2, fillColor: [0,0,255,1], min : 0, max: 1
            //    },
            //    {
            //        name:"green" , class : "simple", type: "circle", size: 40, borderColor: [0,0,0], borderSize: 2, fillColor: [0,150,0,1], min : 2, max: 200
            //    },
            //    {
            //        name:"red" , class : "simple", type: "circle", size: 80, borderColor: [0,0,0], borderSize: 2, fillColor: [255,0,0,1], min : 201, max: 1000
            //    },
            //    {
            //        name:"pink" , class : "simple", type: "circle", size: 120, borderColor: [0,0,0], borderSize: 2, fillColor: [255,0,255,1], min : 1001, max: 10000
            //    }
            //],
            default : {
                class : "simple", type: "circle", size: 5, borderColor: [0,0,0], borderSize: 2, fillColor: [0,0,0,1], min : 0, max: 0
            },
            distance: 100,
            labelSize: '10px',
            labelOffset: -3,
            pinSize: {
                small : 16,
                medium : 20,
                large : 32
            }
        },

        resultPinWidget : {
            infostatus: "สถานะ",
            infoplanIncomingDate:"เวลาวางแผน",
            infoactualIncomingDate:"เวลาเข้าถึง",
            infoactualOutgoingDate:"เวลาสำเร็จ",
            inforemark:"หมายเหตุ"
        },
        resultVehicleWidget : {
            Common_Driver: "คนขับรถ",
            Common_BusinessUnit:"แผนก",
            Common_Datetime:"วันเวลา",
            Common_Location:"สถานที่"
        },

        setMapProvider: function (arrData) {
            let lstBasemaps = [];
            let lstBasemapKey = [];
            let lstLayers = [];
            let lstLayerKey = [];
            let lstAllBasemap = [];

//arrData.sort(function(a,b){return a.sortIndex - b.sortIndex})

// console.log("arrData",arrData);

            let cntArr = arrData.length;
            for (let ind = 0; ind < cntArr; ind++) {
                let basemapID = arrData[ind]["baseMapItem"];

                //Remove HD Map
                if (arrData[ind]["urlEnglish"] && arrData[ind]["urlEnglish"].toLowerCase().indexOf("streetmaphd") > -1) {
                    continue;
                }

                //for debuging with localhost
                //if (basemapID != "bm-item-3")
                //    continue;
// console.log("basemapID",basemapID);

                if (basemapID != null) {
                    //basemap
                    //lstBasemaps.push(arrData[ind]);
                    let keyIndex = lstBasemapKey.indexOf(basemapID);
                    let grpLayer = null;

                    if (keyIndex == -1) {
                        //have group
                        lstBasemapKey.push(basemapID);
                        keyIndex = lstBasemapKey.length - 1;
                        grpLayer = [];
                        lstBasemaps[keyIndex] = grpLayer;
                    }
                    else {
                        grpLayer = lstBasemaps[keyIndex];
                    }

                    grpLayer.push(arrData[ind]);
                    lstAllBasemap.push(arrData[ind])

                }
                else {
                    //layer
                   // lstLayerKey.push(arrData[ind]["layerName"]);
                    lstLayers.push(arrData[ind]);
                }
            }

            lstLayers.sort(function(a, b){return a.sortIndex - b.sortIndex});

            lstLayers.forEach(function (element) {
               lstLayerKey.push(element.layerName)
            });
            
            this.mapProviderData = { basemap: { keys: lstBasemapKey, items: lstBasemaps, all: lstAllBasemap }, layer: { keys: lstLayerKey, items: lstLayers } };
        }
    });

    if (!_instance) {
        _instance = new _cls();
    }

    return _instance;
});
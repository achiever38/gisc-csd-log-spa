﻿define([
    "dijit/_WidgetBase",
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/on",

    "GOTModule/config/ApplicationConfig"
], function (_WidgetBase, declare, lang, on, ApplicationConfig) {
    var _instance = null;
    var _MISInterface = null;
    var _postbackHandler = null;
    var _SUCCESS_STATUS = "SUCCESS";
    var _FAIL_STATUS = "ERROR";

    var _PRINT_WINDOW = null;
    var _PRINT_PARAM = null;

    _MISInterface = declare([_WidgetBase], {

        sendbackOrigin: ApplicationConfig.sendbackOrigin,

        /*
           @description constructor
       */
        constructor: function () {
            _postbackHandler = window.addEventListener('message', lang.hitch(this, function (e) {
                var postbackArgs = e.data;

                if (postbackArgs["command"] == "printpage-document-load-complete") {
                    _PRINT_WINDOW.window.setPrintData(_PRINT_PARAM);
                }

                if (this.onRecieveMessage) {
                    this.onRecieveMessage(postbackArgs);
                }
            }));
        },
        /*
           @description on recieve post message event
           @param {args} parameter from post message
       */
        onRecieveMessage: function (args) { /*attach event*/ },

        /*
            @description send post message to parent page
            @param {obj} parameter to send
        */
        sendMessage: function (obj) {
            if (!parent || (parent.window === window)) {
                console.log("parent page not found");
            }
            else if (!obj) {
                console.log("empty parameter, cancel sending. ");
            }
            else {
                parent.postMessage(obj, this.sendbackOrigin);
            }
        },

        /*
            @description create post message response object
            @param {obj} data for create PostMessageResult format : { command|string, status|bool, message|string, result|object, guid|unique-id, [expect|object] , [actual|oject] }
            - command : command operation
            - status : command execute status (true:success / false:fail )
            - message : operation message / error message
            - result : result from command executing
            - guid : unique id for identify caller
            - expect : unit test expect result
            - actual : unit test actual result
            @return PostBack response object
        */
        createResponseResult: function (param) {
            let data = {
                command: param.command || null,
                status: param.status ? _SUCCESS_STATUS : _FAIL_STATUS,
                message: param.message || null,
                result: param.result || null,
                guid: param.guid || null
            }

            if (param.expect && param.actual) {
                data.actual = param.actual;
                data.expect = param.expect;
            }

            return data;
        },

        sendPrintmapData: function (data) {
            _PRINT_PARAM = { command: "set-startup-printmap", param: data };

            _PRINT_WINDOW = window.open(ApplicationConfig.printMapURL, "printmapPage");

        },

        sendPrintRouteData: function (data) {
            _PRINT_PARAM = { command: "set-startup-printmap", param: data };

            _PRINT_WINDOW = window.open(ApplicationConfig.printRouteMapURL, "printRoutePage");

        }
    });

    /*
        @description singelton resource object
    */
    if (!_instance) {
        _instance = new _MISInterface();
    }

    return _instance;
});
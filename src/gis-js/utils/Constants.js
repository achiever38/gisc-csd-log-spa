﻿define([
    "dojo/_base/declare",
    "dojo/_base/lang" ,
    "GOTModule/config/ApplicationConfig"], function (declre, lang, ApplicationConfig) {
        var _instance = null;
        var _cls = null;

        _cls = declre(null, {
            AreaUnit: {
                SquareMetres: 1,
                SquareKilometres: 2,
                ThailandUnit: 3
            },

            DistanceUnit: {
                Metre: 1,
                Kilometre: 2,
                Mile: 3
            }
        });

        if (!_instance) {
            _instance = new _cls();
        }

        return _instance;
    });
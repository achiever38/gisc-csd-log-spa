﻿define([
    "dojo/_base/declare",
    "dojo/_base/lang",

    "GOTModule/config/ApplicationConfig",

    "BowerModule/jquery/src/jquery"],
function (declre, lang, ApplicationConfig, $) {

    //var requestOrigin = ApplicationConfig.webAPIConfig["baseAPIUrl"];
    var requestMethod = {
        GET: "GET",
        POST: "POST",
        PUT: "PUT",
        DEL: "DELETE"
    };

    var mockupUser = {
        username: "sysadmin",
        password: "nostra1234"
    }

    return declre(null, {

        

        requestGET: function (objParam) {
            var options = {
                url: objParam.url,
                headers: objParam.headers ? objParam.headers : null,
                type: requestMethod.GET,
                data: objParam.data || {},
                //headers: {
                //    "GISC-CompanyId": ApplicationConfig.userInfo["GISC-CompanyId"] || "",
                //    "GISC-CompanyLanguage": ApplicationConfig.userInfo["GISC-CompanyLanguage"] || "",
                //    "GISC-UserLanguage": ApplicationConfig.userInfo["GISC-UserLanguage"] || ""
                //},
                xhrFields: { // Force jQuery send cookie request.
                    withCredentials: true
                }
            };

            return $.ajax(options)
                .done(objParam.success || function (data) { console.log(data); })
                .fail(objParam.error || function (err) { console.log(err); })
                .always(objParam.finished || function () { });

        },

        requestPOST: function (objParam) {
            var options = {
                url: objParam.url,
                type: requestMethod.POST,
                data: objParam.data || {},
                //headers: {
                //    "GISC-CompanyId": ApplicationConfig.userInfo["GISC-CompanyId"] || "",
                //    "GISC-CompanyLanguage": ApplicationConfig.userInfo["GISC-CompanyLanguage"] || "",
                //    "GISC-UserLanguage": ApplicationConfig.userInfo["GISC-UserLanguage"] || ""
                //},
                xhrFields: { // Force jQuery send cookie request.
                    withCredentials: true
                }
            };

            return $.ajax(options)
                .done(objParam.success || function (data) { console.log(data); })
                .fail(objParam.error || function (err) { console.log(err); })
                .always(objParam.finished || function () { });

        },

        requestPUT: function (objParam) {
            var options = {
                url: objParam.url,
                type: requestMethod.PUT,
                data: objParam.data || {},
                //headers: {
                //    "GISC-CompanyId": ApplicationConfig.userInfo["GISC-CompanyId"] || "",
                //    "GISC-CompanyLanguage": ApplicationConfig.userInfo["GISC-CompanyLanguage"] || "",
                //    "GISC-UserLanguage": ApplicationConfig.userInfo["GISC-UserLanguage"] || ""
                //},
                xhrFields: { // Force jQuery send cookie request.
                    withCredentials: true
                }
            };

            return $.ajax(options)
                .done(objParam.success || function (data) { console.log(data); })
                .fail(objParam.error || function (err) { console.log(err); })
                .always(objParam.finished || function () { });

        },

        requestDELETE: function (objParam) {
            var options = {
                url: objParam.url,
                type: requestMethod.DEL,
                data: objParam.data || {},
                //headers: {
                //    "GISC-CompanyId": ApplicationConfig.userInfo["GISC-CompanyId"] || "",
                //    "GISC-CompanyLanguage": ApplicationConfig.userInfo["GISC-CompanyLanguage"] || "",
                //    "GISC-UserLanguage": ApplicationConfig.userInfo["GISC-UserLanguage"] || ""
                //},
                xhrFields: { // Force jQuery send cookie request.
                    withCredentials: true
                }
            };

            return $.ajax(options)
                .done(objParam.success || function (data) { console.log(data); })
                .fail(objParam.error || function (err) { console.log(err); })
                .always(objParam.finished || function () { });

        },

        requestAuthen: function (obj) {
            var options = {
                url: baseAPICoreUrl + "/api/user/login",
                type: requestMethod.POST,
                data: mockupUser,
                xhrFields: { // Force jQuery send cookie request.
                    withCredentials: true
                }
            };

            return $.ajax(options)
                .done(function (data) {
                    //if (ApplicationConfig.userInfo) {
                    //    lang.mixin(ApplicationConfig.userInfo, data);
                    //}
                    //else {
                    //    ApplicationConfig.userInfo = data;
                    //}
                })
                .fail(function (err) { console.log(err); })
                .always(function () { });
        },

        requestTrackingAuthen: function () {
            var options = {
                url: baseAPITrackingCoreUrl + "/api/user/login",
                type: requestMethod.POST,
                data: mockupUser,
                xhrFields: { // Force jQuery send cookie request.
                    withCredentials: true
                }
            };

            return $.ajax(options)
                .done(function (data) {
                    if (ApplicationConfig.userInfo) {
                        lang.mixin(ApplicationConfig.userInfo, data);
                    }
                    else {
                        ApplicationConfig.userInfo = data;
                    }
                })
                .fail(function (err) { console.log(err); })
                .always(function () { });
        }
    });
});
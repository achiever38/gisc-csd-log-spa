﻿define([
    "dijit/_WidgetBase",
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/on",
    "dojo/topic",
    "dojox/json/query",

    "GOTModule/config/ApplicationConfig"
], function (_WidgetBase, declare, lang, on, topic, jsonQuery, ApplicationConfig) {
    var _cls = declare([_WidgetBase], {

        vehicleTrackingList: null,

        constructor: function () {
            this.vehicleTrackingList = [];
        },

        addTrackableItem: function (obj) {
            //console.log("addTrackableItem", obj);
            if (jsonQuery("[?id='" + obj.id + "']", this.vehicleTrackingList).length == 0) {
                this.vehicleTrackingList.push({
                    id: obj.id,
                    vehicle: new VehicleItemCls({
                        vehicleID: obj.id,
                        directionPic: obj.directionPic,
                        location: obj.location,
                        graphic: obj.graphic,
                        textGraphic: obj.textGraphic,
                        animateTime: obj.animateTime,
                        fps: obj.fps,
                    }),
                    track: (obj.track == true)
                });

                let lstTrack = jsonQuery("[?track=true][=vehicle]", this.vehicleTrackingList);

                this.onTrackLocationChange(lstTrack);
            }
        },

        checkAlreadyAddItem: function (id) {
            return jsonQuery("[?id='" + id + "']", this.vehicleTrackingList).length > 0;
        },

        getVehicle: function (id) {
            return jsonQuery("[?id='" + id + "']", this.vehicleTrackingList)[0] || null;
        },

        recieveData: function (objTrack) {
            var itm = objTrack

            if (this.checkAlreadyAddItem(itm.id)) {

                let itmCurrent = this.getVehicle(itm.id);
                itmCurrent.track = itm.track;
                //itmCurrent.directionPic = itm.directionPic;

                topic.publish("Fully-Playback-Changed", itm);

                let lstTrack = jsonQuery("[?track=true][=vehicle]", this.vehicleTrackingList);

                this.onTrackLocationChange(lstTrack);
            }
            else {
                this.addTrackableItem(itm);
            }
        },

        removeTrackableItem: function (vehicle) {
            //console.log(vehicle);

            for (let i = 0; i < this.vehicleTrackingList.length; i++) {
                if (this.vehicleTrackingList[i].id == vehicle.id) {
                    this.vehicleTrackingList.splice(i, 1);
                }
            }
        },

        clearAllTrackable: function () {
            this.vehicleTrackingList = [];
        },

        onTrackLocationChange: function (data) { /* attach event */ }
    });

    var VehicleItemCls = declare([_WidgetBase], {

        directionConst: ["N", "NE", "E", "SE", "S", "SW", "W", "NW"],

        animateTime: ApplicationConfig.animationConfig.animateTime,
        fps: ApplicationConfig.animationConfig.fps,

        vehicleID: null,
        directionPic: { "N": null, "NE": null, "E": null, "SE": null, "S": null, "SW": null, "W": null, "NW": null },
        animateHandler: null,

        startLocation: null,
        currentLocation: null,
        nextLocation: null,
        lat: 0,
        lon: 0,

        lonPerTime: 0,
        latPerTime: 0,

        graphic: null,
        geometry: null,
        symbol: null,

        textGraphic: null,
        textGeometry: null,

        animateState: true,
        currentAttributes: null,

        constructor: function (obj) {
            this.vehicleID = obj.vehicleID;
            this.directionPic = obj.pic;
            this.startLocation = obj.location;

            this.graphic = obj.graphic;
            this.geometry = this.graphic.geometry;
            this.symbol = this.graphic.symbol;

            this.lat = obj.location.lat;
            this.lon = obj.location.lon;
            
            if (obj.textGraphic) {
                this.textGraphic = obj.textGraphic;
                this.textGeometry = this.textGraphic.geometry;
            }

            topic.subscribe("Fully-Playback-Changed", lang.hitch(this, function (data) {
                if (data.id == this.vehicleID) {

                    this.stopAnimate();

                    this.directionPic = data.directionPic;

                    this.graphic.setAttributes(data.attributes);

                    if (this.nextLocation) {
                        this.startLocation = lang.clone(this.nextLocation);
                        this.lat = this.startLocation.lat;
                        this.lon = this.startLocation.lon;
                    }

                    this.nextLocation = data.location;

                    if (this.startLocation.lat != this.nextLocation.lat || this.startLocation.lon != this.nextLocation.lon)
                        this.calculateRotation();

                    this.startAnimate();
                }
            }));
        },

        calculateRotation: function () {
            var rotateDegree = (Math.atan2(this.nextLocation["lon"] - this.startLocation["lon"], this.nextLocation["lat"] - this.startLocation["lat"]) * (180 / Math.PI)).toFixed(1);

            //console.log(rotateDegree);

            var directionIndex = (Math.round((rotateDegree - 22.5) / 45));

            if (directionIndex < 0)
                directionIndex += 8;

            //console.log(directionIndex);

            var directoinPic = this.directionPic[this.directionConst[directionIndex]];
            //console.log(directoinPic);

            //test rotation
            //this.symbol.setAngle(rotateDegree);
            this.symbol.setUrl(directoinPic);
            this.graphic.setSymbol(this.symbol);
        },

        calculatePosition: function () {
            
            lonPerTime = (this.startLocation["lon"] - this.nextLocation["lon"]) / (this.fps * this.animateTime);
            latPerTime = (this.startLocation["lat"] - this.nextLocation["lat"]) / (this.fps * this.animateTime);

            this.lon = (this.lon - lonPerTime);
            this.lat = (this.lat - latPerTime);

            this.geometry.setLatitude(this.lat);
            this.geometry.setLongitude(this.lon);

            this.graphic.setGeometry(this.geometry);

            if (this.textGraphic) {
                this.textGeometry.setLatitude(this.lat);
                this.textGeometry.setLongitude(this.lon);

                this.textGraphic.setGeometry(this.geometry);
            }
        },

        startAnimate: function () {
            if (!this.nextLocation) { return; }
            this.calculatePosition();

            var maxLon = Math.max(this.nextLocation["lon"], this.startLocation["lon"]),
                minLon = Math.min(this.nextLocation["lon"], this.startLocation["lon"]),
                maxLat = Math.max(this.nextLocation["lat"], this.startLocation["lat"]),
                minLat = Math.min(this.nextLocation["lat"], this.startLocation["lat"]);

            if ((this.lon >= minLon && this.lon <= maxLon) &&
                (this.lat >= minLat && this.lat <= maxLat)) {
                this.animateHandler = requestAnimationFrame(lang.hitch(this, this.startAnimate))
            }
            else {
                this.stopAnimate();
            }
        },

        stopAnimate: function () {
            cancelAnimationFrame(this.animateHandler);
        }
    });

    return _cls;
});
﻿
require([
    "dojo/on",
    "dojo/dom",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/_base/lang",

    "GOTModule/config/ApplicationConfig",

    "GOTModule/utils/ResourceLib",
    "GOTModule/utils/MISInterface",
    "GOTModule/utils/WebAPIInterface",
    "GOTModule/utils/Utility",

    "GOTModule/controls/MapManager",
    "GOTModule/controls/ToolPanel",

    "dojox/json/query",

    "dojo/domReady!"
], function (on, dom, domClass, domConstruct, lang, ApplicationConfig, ResourceLib, MISInterface, WebAPIInterface, Utility, MapManager, ToolPanel, jsonQuery) {
    
    /* Initial Parameter */
    var startupData = null;
    var wgMapManager = null;
    var wgToolPanel = null;
    var checkOpenPoi = false;
    var checkOpenArea = false;
    var viewDetailID = null;
    var flagCloseContextMenu = true;

    /* initialize web api interface */
    var wgWebApiCaller = new WebAPIInterface();

    /* MISInterface RecieveMessage event */
    on(MISInterface, "RecieveMessage", onRecievePostMessage);

    /* Mappage functions declaration */
    /* initialize all function */

    function initialize(startupParam) {
        if (startupData) { return; }
        startupData = startupParam;
        /* set app language & resources */
        ResourceLib.loadResource(startupParam.i18nResources);
        ResourceLib.setLanguage(startupParam.userInfo.appLanguage);

        /* Setting startup config */
        ApplicationConfig.userPermission = startupParam.permission;

        if (ApplicationConfig.userInfo) {
            lang.mixin(ApplicationConfig.userInfo, startupParam.userInfo);
        }
        else {
            ApplicationConfig.userInfo = startupParam.userInfo;
        }
        
        ApplicationConfig.showPoiCluster =  startupParam.showPoiCluster;

        /* Loading Theme */
        loadTheme();

        wgWebApiCaller.requestPOST({
            url: ApplicationConfig.webAPIConfig.getMapProvider,
            data: {
                "companyId": ApplicationConfig.userInfo.info._userSession.currentCompanyId
            },
            success: function (data) {
                Utility.debugLog("Get Map Provider Success", data);
                //// Test add google map object supakorn
                //var exampleGoogleItem = {
                //    "companyId": null, "baseMapItem": "bm-item-4", "layerName": null, "serviceID": "27"
                //    , "urlLocal": "", "urlEnglish": "", "tokenLocal": null, "tokenEnglish": null, "sortIndex": 27
                //    , "mapServiceType": "GOOGLE", "dependMap": null, "minLevel": 0, "maxLevel": 0, "extentMinX": 0.000000
                //    , "extentMinY": 0.000000, "extentMaxX": 0.000000, "extentMaxY": 0.000000
                //    , "isVisible": true, "isEnable": true, "isAllCompanies": true
                //    , "infoStatus": 1, "id": 1, "createDate": "2018-07-01T18:10:30.1066667"
                //    , "updateDate": "2018-07-01T18:10:30.1066667"
                //    , "createBy": "bsysadmin", "updateBy": "bsysadmin", "pageProperty": null
                //};
                //data.push(exampleGoogleItem);
                //console.log("b debug", data)
                ApplicationConfig.setMapProvider(data);
            },
            error: function (err) {
                Utility.debugLog("Get Map Provider Error", err);
                ApplicationConfig.setMapProvider([]);
            },
            finished: function () {
                Utility.debugLog("Get Map Provider Finished Job");
                Utility.debugLog("Map provider data", ApplicationConfig.mapProviderData);

                /* initialize Map */
                wgMapManager = new MapManager({ domId: "divMap" });
                ApplicationConfig.mapManager = wgMapManager;

                on(wgMapManager, "TriggerFire", onTriggerFired);

                on(document.getElementById("divMap"), "mouseenter", lang.hitch(this, function(e) {

                    if(wgToolPanel.isOpen() && !wgToolPanel.isPin())
                    {
                        wgToolPanel.hidePanel();
                    }
                }));

                if(Utility.isMobileDevice())
                {
                    on(document.getElementById("divMap"), "click", lang.hitch(this, function(e) {

                        if(wgToolPanel.isOpen() && !wgToolPanel.isPin())
                        {
                            wgToolPanel.hidePanel();
                        }
                    }));
                }

                wgMapManager.initializeMap();

                /* Send message back */
                // MISInterface.sendMessage(MISInterface.createResponseResult({
                //     command: "set-startup-resources",
                //     status: true
                // }));
            }
        });

    }

    function loadTheme() {
        /*=== Loading Theme ===*/

        /* Main Theme Class*/
        var mainTheme = ApplicationConfig.userInfo.info["_companyThemeName"];
        if (!mainTheme) mainTheme = "app-theme-gis";
        /* Add Class Theme to Body */
        domClass.add(document.body, mainTheme);

        /* Draw Loading Screen */
        var loadingLogoUrl = Utility.generateThemeLogoPath(mainTheme);

        require(["dojo/text!" + loadingLogoUrl], function(svgLogo){
            //domConstruct.place(domConstruct.toDom(svgLogo), dom.byId("divLoadingLogo"), "last");
        });
    }

    /* All PostMessage Process */
    function onRecievePostMessage(postData) {
        let isPass = true;
        let msg = null;

        Utility.debugLog("onRecievePostMessage", postData);

        try {
            switch (postData["command"]) {
                case "set-startup-resources": {

                    /* Startup Process */
                    initialize(postData["param"]);

                    break;
                }

                case "track-vehicles": {
                    wgMapManager.trackVehicles(postData["param"]);


                    for (var i = 0; i < postData["param"]["vehicles"].length; i++) {
                        if (viewDetailID == postData["param"]["vehicles"][i]["attributes"]["ID"]) {
                            wgToolPanel.openPanel("vehicle-info", postData["param"]["vehicles"][i]);
                            break;
                        }
                    }

                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "track-vehicles",
                        status: true
                    }));

                    break;
                }

                case "track-nearest-vehicles": {
                    wgMapManager.trackNearestVehicles(postData["param"]);


                    for (var i = 0; i < postData["param"]["vehicles"].length; i++) {
                        if (viewDetailID == postData["param"]["vehicles"][i]["attributes"]["ID"]) {
                            wgToolPanel.openPanel("vehicle-info", postData["param"]["vehicles"][i]);
                            break;
                        }
                    }

                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "track-nearest-vehicles",
                        status: true
                    }));

                    break;
                }

                case "remove-track-vehicles": {
                    wgMapManager.removeTrackVehicle(postData["param"]);

                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "remove-track-vehicles",
                        status: true
                    }));
                    break;
                }

                case "reset-vehicle-filter": {
                    ApplicationConfig.currentTrackVehicleFilter = [1, 2, 3, 4, 5, 6];
                    if (wgToolPanel.currentWidget != null) {
                        if (wgToolPanel.currentWidget.widgetKey == "vehicle") {
                            wgToolPanel.currentWidget.resetFilter();
                        }
                        else {
                            ApplicationConfig.isResetFilter = true;
                        }
                    }

                    wgMapManager.setTrackAllVisible();


                    //if (wgToolPanel.currentWidget.widget){

                    //}
                    break;
                }

                case "enable-click-map": {
                    ApplicationConfig.IsFollowTrackable = false;

                    wgMapManager.enableClickMap(postData["param"]);
                    break;
                }

                case "enable-draw-POIArea":{
                    // ApplicationConfig.IsFollowTrackable = false;

                    wgMapManager.enableClickdrawPOIArea();
                    break;
                }
                case "enable-draw-polygon-area":{
                    ApplicationConfig.IsFollowTrackable = false;
                    wgToolPanel.openPanel("polygon-editor-poi-area",postData["param"])
                    wgMapManager.enableDrawingPolygonArea(postData["param"]);
                // console.log("pin",wgToolPanel.isPin());
                //     wgToolPanel.isOpen(true)
                    wgToolPanel.pinPanel(true)
                    break;
                }

                case "enable-draw-polygon": {
                    ApplicationConfig.IsFollowTrackable = false;
                    wgToolPanel.openPanel("polygon-editor", postData["param"]);
                    wgMapManager.enableDrawingPolygon(postData["param"]);
                    break;
                }

                case "cancel-click-map": {
                    wgMapManager.cancelClickMap();

                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "cancel-click-map",
                        status: true
                    }));

                    break;
                }
                case "cancel-draw-polygon": {
                    wgToolPanel.closePanel();
                    wgMapManager.cancelDrawingPolygon();

                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "cancel-draw-polygon",
                        status: true
                    }));

                    break;
                }


                case "draw-point-buffer": {
                    ApplicationConfig.IsFollowTrackable = false;

                    if (!postData["param"]
                        || !postData["param"]["location"]
                        || !postData["param"]["location"]["lat"]
                        || !postData["param"]["location"]["lon"]
                        || !postData["param"]["location"]["symbol"]
                        || !postData["param"]["color"]
                        || !postData["param"]["color"]["border"]
                        || !postData["param"]["color"]["fill"]
                        || !postData["param"]["opacity"]
                        || !postData["param"]["opacity"]["border"]
                        || !postData["param"]["opacity"]["fill"]) {

                        isPass = false;
                        msg = "invalid parameter";
                    }


                    wgMapManager.drawPointBuffer(postData["param"]);

                    if (isPass) {
                        MISInterface.sendMessage(MISInterface.createResponseResult({
                            command: "draw-point-buffer",
                            status: true
                        }));
                    }
                    else {
                        MISInterface.sendMessage(MISInterface.createResponseResult({
                            command: "draw-point-buffer",
                            status: false,
                            message: msg
                        }));
                    }

                    break;
                }

                case "draw-one-point-zoom": {
                    ApplicationConfig.IsFollowTrackable = false;

                    /* Startup Process */
                    wgMapManager.drawOnePointZoom(postData["param"]);

                    /* Send message back */
                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "draw-one-point-zoom",
                        status: true
                    }));
                    break;
                }
                case "draw-point-main-area":{
                    ApplicationConfig.IsFollowTrackable = false;

                    /* Startup Process */
                    wgMapManager.drawPointPoiArea(postData["param"]);

                    /* Send message back */
                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "draw-point-main-area",
                        status: true
                    }));
                    break;
                }
                case "draw-one-line-zoom": {
                    ApplicationConfig.IsFollowTrackable = false;

                    /* Startup Process */
                    wgMapManager.drawOneLineZoom(postData["param"]);

                    /* Send message back */
                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "draw-one-line-zoom",
                        status: true
                    }));
                    break;
                }
                case "draw-one-polygon-zoom": {
                    ApplicationConfig.IsFollowTrackable = false;

                    /* Startup Process */
                    wgMapManager.drawOnePolygonZoom(postData["param"]);

                    /* Send message back */
                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "draw-one-polygon-zoom",
                        status: true
                    }));
                    break;
                }
                case "draw-one-polygon-zoom-area": {
                    ApplicationConfig.IsFollowTrackable = false;

                    /* Startup Process */
                    wgMapManager.drawOnePolygonZoomArea(postData["param"]);

                    /* Send message back */
                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "draw-one-polygon-zoom-area",
                        status: true
                    }));
                    break;
                }
                case "draw-multiple-polygon-zoom-area":{
                    ApplicationConfig.IsFollowTrackable = false;

                    /* Startup Process */
                    wgMapManager.drawMultiplePolygonZoomArea(postData["param"]);

                    /* Send message back */
                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "draw-multiple-polygon-zoom-area",
                        status: true
                    }));
                    break;
                }
                case "draw-multiple-polygon-zoom": {
                    ApplicationConfig.IsFollowTrackable = false;

                    /* Startup Process */
                    wgMapManager.drawMultiplePolygonZoom(postData["param"]);

                    /* Send message back */
                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "draw-multiple-polygon-zoom",
                        status: true
                    }));
                    break;
                }
                case "draw-multiple-line-zoom": {
                    ApplicationConfig.IsFollowTrackable = false;

                    /* Startup Process */
                    wgMapManager.drawMultipleLineZoom(postData["param"]);

                    /* Send message back */
                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "draw-multiple-line-zoom",
                        status: true
                    }));
                    break;
                }
                case "draw-multiple-vehicles": {
                    ApplicationConfig.IsFollowTrackable = false;

                    /* Startup Process */
                    wgMapManager.drawMultipleVehicles(postData["param"]);

                    /* Send message back */
                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "draw-multiple-vehicles",
                        status: true
                    }));
                    break;
                }
                case "zoom-point": {
                    ApplicationConfig.IsFollowTrackable = false;

                    /* Startup Process */
                    wgMapManager.zoomPoint(postData["param"]);

                    // /* Send message back */
                    // MISInterface.sendMessage(MISInterface.createResponseResult({
                    //     command: "zoom-point",
                    //     status: true
                    // }));
                    break;
                }
                case "draw-one-route": {
                    ApplicationConfig.IsFollowTrackable = false;

                    /* Startup Process */
                    wgMapManager.drawOneRoute(postData["param"]);

                    /* Send message back */
                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "draw-one-route",
                        status: true
                    }));
                    break;
                }
                case "draw-route-buffer": {
                    ApplicationConfig.IsFollowTrackable = false;

                    /* Startup Process */
                    var result = wgMapManager.drawRouteBuffer(postData["param"]);

                    /* Send message back */
                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "draw-route-buffer",
                        status: true,
                        result: result
                    }));
                    break;
                }
                case "draw-custom-area": {
                    ApplicationConfig.IsFollowTrackable = false;

                    /* Startup Process */
                    wgMapManager.drawCustomArea(postData["param"]);

                    /* Send message back */
                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "draw-custom-area",
                        status: true
                    }));
                    break;
                }
                case "clear-graphics": {
                    ApplicationConfig.IsFollowTrackable = false;

                    wgMapManager.clearLayers(ApplicationConfig.commandOptions["clear-graphics"]);
                    wgMapManager.closePlaybackFootprintLegend();
                    wgMapManager.showAllTrackVehicle();
                    ApplicationConfig.isClickMapAndHideMenu = true;
                    wgToolPanel.invokeContentFunction("tools", "clearMeasurement");
                    wgToolPanel.invokeContentFunction("vehicle", "resetFilter");
                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "clear-graphics",
                        status: true
                    }));
                    break;
                }
                case "clear-graphics-id":{
                    ApplicationConfig.IsFollowTrackable = false;
                    
                    wgMapManager.clearLayersId(postData["param"])
                   
                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "clear-graphics-id",
                        status: true
                    }));
                    break;
                }
                case "draw-multiple-point": {
                    ApplicationConfig.IsFollowTrackable = false;

                    wgMapManager.drawMultiplePoint(postData["param"]);
                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "draw-multiple-point",
                        status: true
                    }));
                    break;
                }
                case "draw-poi-suggestion" : {
                    ApplicationConfig.IsFollowTrackable = false;

                    wgMapManager.drawPOISuggestion(postData["param"]);
                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "draw-poi-suggestion",
                        status: true
                    }));

                    break;
                }

                case "playback-animate-vehicles": {
                    ApplicationConfig.IsFollowTrackable = false;

                    wgMapManager.hidePlaybackLegend();
                    wgToolPanel.openPanel("playback-control", postData["param"]);
                    wgToolPanel.hidePanel();

                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "playback-animate-vehicles",
                        status: true
                    }));
                    break;
                }
                case "playback-footprint-vehicles": {

                    ApplicationConfig.IsFollowTrackable = false;

                    wgToolPanel.closePanel();
                    wgMapManager.drawPlaybackFootprint(postData["param"]);
                   // wgToolPanel.hidePanel();
                    
                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "playback-footprint-vehicles",
                        status: true
                    }));
                    break;
                }
                case "playback-draw-alert-chart": {
                    wgMapManager.drawPlaybackAlertChart(postData["param"]);
                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "playback-draw-alert-chart",
                        status: true
                    }));
                    break;
                }

                case "close-footprint-vehicles":
                    wgMapManager.closePlaybackFootprintLegend();
                    break;

                case "playback-fully-vehicles": {

                    ApplicationConfig.IsFollowTrackable = false;

                    wgMapManager.hidePlaybackLegend();
                    wgToolPanel.openPanel("playback-multiple", postData["param"]);
                    wgToolPanel.hidePanel();
                    

                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "playback-fully-vehicles",
                        status: true
                    }));
                    break;
                }

                case "print-route-map": {
                    let dataTrasfer = wgMapManager.getTransferMapData();
                    let routeData = postData["param"];

                    MISInterface.sendPrintRouteData({ startupData: startupData, dataTrasfer: dataTrasfer, routeData: routeData });
                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "print-route-map",
                        status: true
                    }));
                    break;
                }

                case "follow-trackable": {
                    ApplicationConfig.IsFollowTrackable = postData["param"]["follow"];

                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "follow-trackable",
                        status: true
                    }));
                    break;
                }

                case "open-poi-layer": {
                    checkOpenPoi = true;
                    wgMapManager.customPoi();
                    $("[data-key='custom-poi']").addClass("content-active");

                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "open-poi-layer",
                        status: true
                    }));
                    break;
                }

                case "open-shipment-legend": {
                    ApplicationConfig.IsFollowTrackable = false;
                    ApplicationConfig.isResetFilterViewOnMap = true;

                    ApplicationConfig.isClickMapAndHideMenu = false;

                    var data = postData["param"].data;
                    // wgMapManager.drawViewOnMap(data.plan, data.actual, data.vehicle);
                    wgMapManager.drawViewOnMap(data);
                    wgMapManager.hideAllTrackVehicle();
                    wgToolPanel.openPanel("shipment-legend", data.panel);
                    wgToolPanel._onShowMenu({data: data.panel, menu: data.menu});
                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "open-shipment-legend",
                        status: true
                    }));
                    break;
                }

                case "open-shipment-legend-template": {
                    ApplicationConfig.IsFollowTrackable = false;
                    ApplicationConfig.isResetFilterViewOnMap = true;

                    ApplicationConfig.isClickMapAndHideMenu = false;

                    var data = postData["param"].data;
                    // wgMapManager.drawViewOnMap(data.plan, data.actual, data.vehicle);
                    wgMapManager.drawViewOnMap(data);
                    wgMapManager.hideAllTrackVehicle();
                  //  wgToolPanel.openPanel("shipment-legend", data.panel);

                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "open-shipment-legend-template",
                        status: true
                    }));
                    break;
                }

                case "draw-shipment-route": {
                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "draw-shipment-route",
                        status: true
                    }));
                    break;
                }

                case "clear-shipment-route": {
                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "clear-shipment-route",
                        status: true
                    }));
                    break;
                }

                case "show-all-trackvehicle": {
                    ApplicationConfig.isClickMapAndHideMenu = true;
                    wgMapManager.showAllTrackVehicle();
                    break;
                }
                case "hide-all-trackvehicle": {
                    wgMapManager.hideAllTrackVehicle();
                    break;
                }

                case "close-playback": {
                    wgMapManager.closePlaybackFootprintLegend();
                    if (wgToolPanel.currentWidget != null) {
                        if (wgToolPanel.currentWidget.widgetKey == "playback-multiple" ||
                            wgToolPanel.currentWidget.widgetKey == "playback-control") {
                            wgToolPanel.closePanel();
                        }
                    }
                    break;
                }

                case "close-viewonmap": {
                    wgMapManager.closeViewOnMap();
                    ApplicationConfig.isClickMapAndHideMenu = true;
                    var data = postData["param"];
                    if (wgToolPanel.currentWidget != null) {
                        if (wgToolPanel.currentWidget.widgetKey == "shipment-legend") {
                            wgToolPanel.closePanel();
                        }
                    }
                    wgToolPanel._onShowMenu({menu: data.menu});
                    break;
                }
                case "close-liveview": {
                    //wgMapManager.closeViewOnMap();
                    ApplicationConfig.isClickMapAndHideMenu = true;
                    if (wgToolPanel.currentWidget != null) {
                        if (wgToolPanel.currentWidget.widgetKey == "liveview-control") {
                            wgToolPanel.closePanel();
                        }
                    }
                    break;
                }
                case "enable-click-map-and-hide":
                    //console.log("enable-click-map-and-hide");
                    ApplicationConfig.isClickMapAndHideMenu = true;
                    break;
                case "disable-click-map-and-hide":
                    //console.log("disable-click-map-and-hide");
                    ApplicationConfig.isClickMapAndHideMenu = false;
                    break;

                case "draw-alert-point": {
                    ApplicationConfig.IsFollowTrackable = false;

                    /* Startup Process */
                    wgMapManager.drawAlertPoint(postData["param"]);

                    /* Send message back */
                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "draw-alert-point",
                        status: true
                    }));
                    break;
                }
                case "pan-one-point-zoom":{
                    ApplicationConfig.IsFollowTrackable = false;

                    /* Startup Process */
                    wgMapManager.panOnePointZoom(postData["param"]);

                    break;
                }

                case "open-liveview": {
                    ApplicationConfig.IsFollowTrackable = false;
                    ApplicationConfig.isResetFilterViewOnMap = true;

                    ApplicationConfig.isClickMapAndHideMenu = false;

                    var params = postData["param"];

                    //wgMapManager.drawViewOnMap(data.plan, data.actual, data.vehicle);
                    //wgMapManager.hideAllTrackVehicle();
                    //wgToolPanel.openPanel("shipment-legend", data.panel);

                    wgToolPanel.openPanel("liveview-control", params);
                    wgToolPanel.pinPanel(true);

                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "open-liveview",
                        status: true
                    }));
                    break;
                }
                

                
                default: {
                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: postData["command"],
                        status: false,
                        message: "command not found"
                    }));
                    break;
                }
            }
        }
        catch (ex) {

            let message = "invalid parameter";

            if (ApplicationConfig.debugLog) { message = "command error : " + ex.message };

            MISInterface.sendMessage(MISInterface.createResponseResult({
                command: postData["command"],
                status: false,
                message: message
            }));
        }
    }

    function onActionClick(key) {
        Utility.debugLog("onActionClick", [key]);

        wgMapManager.hideContextMenu();

        switch (key) {
            case "zoom-in": {
                wgMapManager.zoomIn();
                break;
            }
            case "zoom-out": {
                wgMapManager.zoomOut();
                break;
            }
            case "current-location": {
                wgMapManager.currentLocation();
                break;
            }
            case "custom-poi": {

                if (checkOpenPoi == false) {
                    checkOpenPoi = true;
                    wgMapManager.customPoi();
                } else {
                    checkOpenPoi = false;
                    wgMapManager.disableCustomPoi();
                }
                break;
            }
            case "custom-area": {
                if (checkOpenArea == false) {
                    checkOpenArea = true;
                    wgMapManager.customArea();
                } else {
                    checkOpenArea = false;
                    wgMapManager.disableCustomArea();
                }
                break;
            }
            case "clear-graphics": {
                wgMapManager.clearLayers(ApplicationConfig.commandOptions["clear-graphics"]);
                wgMapManager.closePlaybackFootprintLegend();
                wgMapManager.showAllTrackVehicle();
                ApplicationConfig.isClickMapAndHideMenu = true;
                wgToolPanel.invokeContentFunction("tools", "clearMeasurement");
                wgToolPanel.invokeContentFunction("vehicle", "resetFilter");
                break;
            }
      
            default: break;
        }
    }

    function onContentAction(obj) {
        Utility.debugLog("onContentAction", [obj]);

        wgMapManager.hideContextMenu();

        let key = obj.key;
        let param = obj.param;

        switch (key) {
            case "change-basemap": {
                wgMapManager.changeBasemap(param);
                break;
            }
            case "toggle-layer": {
                wgMapManager.toggleLayer(param);
                break;
            }
            case "goto-location": {
                wgMapManager.gotoXY(param);
                break;
            }
            case "measurement": {
                wgMapManager.measurement(param);
                break;
            }
            case "drawing-polygon-sub-area-complete": {
                guid = param["guid"];
                delete param["guid"];

                MISInterface.sendMessage(MISInterface.createResponseResult({
                    command: "enable-draw-polygon-area",
                    status: true,
                    result: param,
                    guid: guid
                }));

                wgToolPanel.closePanel(false);
                break;
            }
            case "drawing-polygon-complete": {
                guid = param["guid"];
                delete param["guid"];

                MISInterface.sendMessage(MISInterface.createResponseResult({
                    command: "enable-draw-polygon",
                    status: true,
                    result: param,
                    guid: guid
                }));

                wgToolPanel.closePanel(false);
                break;
            }
            case "cancel-drawing-polygon": {
                guid = param["guid"];
                delete param["guid"];

                wgToolPanel.closePanel(false);

                MISInterface.sendMessage(MISInterface.createResponseResult({
                    command: "cancel-drawing-polygon",
                    status: true,
                    guid: guid
                }));
                break;
            }
            case "close-drawing-polygon": {
                guid = param["guid"];
                delete param["guid"];

                MISInterface.sendMessage(MISInterface.createResponseResult({
                    command: "close-drawing-polygon",
                    status: true,
                    guid: guid
                }));
                break;
            }
            case "close-panel": {
                //wgToolPanel.closePanel();
                viewDetailID = null;
                break;
            }
            case "print-map": {
                var dataTrasfer = wgMapManager.getTransferMapData();
                Utility.debugLog("Printmap Data", [startupData, dataTrasfer]);

                MISInterface.sendPrintmapData({ startupData: startupData, dataTrasfer: dataTrasfer });
                //MISInterface.sendPrintRouteData({ startupData: startupData, dataTrasfer: dataTrasfer });
                break;
            }

            case "clear-graphics": {
                wgMapManager.clearLayers(ApplicationConfig.commandOptions["clear-graphics"]);
                wgMapManager.closePlaybackFootprintLegend();
                wgMapManager.showAllTrackVehicle();
                ApplicationConfig.isClickMapAndHideMenu = true;
                wgToolPanel.invokeContentFunction("tools", "clearMeasurement");
                wgToolPanel.invokeContentFunction("vehicle", "resetFilter");
                break;
            }

            case "clear-graphics-measurement": {
                wgMapManager.clearLayers({ layer : ["lyr-measurement-drawing-wg","lyr-measurement-wg"]});
                wgToolPanel.invokeContentFunction("tools", "clearMeasurement");
                break;
            }

            case "zoom-favorite": {
                wgMapManager.favoriteZoom(param);
            }

            case "toggle-vehicle": {
                //wgMapManager.toggleVehicle(param);
                wgMapManager.toggleVehicle();
                MISInterface.sendMessage(MISInterface.createResponseResult({
                    command: "toggle-vehicle-status",
                    status: true,
                    result: param
                }));
                break;
            }
            case "close-viewonmap": {
                //Commentออก เพราะRequirementเปลี่ยน 20180614
                //wgMapManager.closeViewOnMap();
                break;
            }
            case "toggle-view-on-map": {
                wgMapManager.toggleViewOnMap(param);
                break;
            }

            case "show-all-trackvehicle": {
                ApplicationConfig.isClickMapAndHideMenu = true;
                wgMapManager.showAllTrackVehicle();
                break;
            }
            case "hide-all-trackvehicle": {
                ApplicationConfig.isClickMapAndHideMenu = false;
                wgMapManager.hideAllTrackVehicle();
                break;
            }

            case "close-playback-widget": {
                MISInterface.sendMessage(MISInterface.createResponseResult({
                    command: "close-playback-widget",
                    status: true,
                    result: null
                }));
                break;
            }
            case "open-live-view-iqtech": {
                MISInterface.sendMessage(MISInterface.createResponseResult({
                    command: "open-live-view-iqtech",
                    status: true,
                    result: param
                }));
                break;
            }
            case "open-vehicle-status-window": {
                MISInterface.sendMessage(MISInterface.createResponseResult({
                    command: "open-vehicle-status-window",
                    status: true,
                    result: param
                }));
                break;
            }
                
            default: break;
        }
    }

    function onToolsPanelContentOpen() {
        if (flagCloseContextMenu) {
            wgMapManager.hideContextMenu();
        } else {
            flagCloseContextMenu = true;
        }
    }

    function onTriggerFired(obj) {
        switch (obj["command"]) {
            case "first-map-loaded": {
                //console.log(obj["param"]);
                //wgToolPanel.onMapLoad(obj["param"]);

                wgToolPanel = new ToolPanel({ domNode: "divToolPanel" });
                lang.mixin(ApplicationConfig, { isPanelOpen: wgToolPanel.isOpen });

                /* ToolPanel Event */
                on(wgToolPanel, "ActionClick", onActionClick);
                on(wgToolPanel, "ContentAction", onContentAction);
                on(wgToolPanel, "ContentOpen", onToolsPanelContentOpen);

                document.getElementById("divLoading").style["display"] = "none";

                MISInterface.sendMessage(MISInterface.createResponseResult({
                    command: "set-startup-resources",
                    status: true
                }));
                
                break;
            }

            case "suggestion-poi-complete": {
                MISInterface.sendMessage(MISInterface.createResponseResult({
                    command: "suggestion-poi-complete",
                    status: true
                }));
                break;
            }

            case "zoom-point-complete": {
                MISInterface.sendMessage(MISInterface.createResponseResult({
                    command: "zoom-point",
                    status: true
                }));
                break;
            }

            case "enable-click-map": {
                /* Send message back */
                MISInterface.sendMessage(MISInterface.createResponseResult({
                    command: "enable-click-map",
                    status: true,
                    result: { location: obj["result"]["location"] },
                    guid: obj["result"]["guid"]
                }));

                break;
            }

            case "location-info": {
                var ptData = obj["param"]["geometry"];

                ptData.extent = wgMapManager.getMapExtent();
              

                wgWebApiCaller.requestGET({
                    url: ApplicationConfig.webAPIConfig.getLocationInfo,
                    headers: {
                        'GISC-CompanyId': ApplicationConfig.userInfo.info._userSession.currentCompanyId,
                        'GISC-CompanyLanguage': '',
                        'GISC-UserLanguage': ApplicationConfig.userInfo.appLanguage
                    },
                    data: ptData,
                    success: function (data) {
                        data.LatLon = ptData;
                        wgToolPanel.openPanel("location-info", data);
                        wgMapManager.drawLocationInfoPin(ptData);
                    },
                    error: function (err) { console.log(err); }
                });

                break;
            }

            case "view-details": {
                var ptData = obj["param"]["geometry"];

                viewDetailID = obj["param"]["attributes"]["ID"];

                wgToolPanel.openPanel("vehicle-info", obj["param"]);
                break;
            }

            case "view-custompoi-info": {
                flagCloseContextMenu = false;
                var data = obj["param"];
                wgToolPanel.openPanel("location-info", data);
                break;
            }

            case "show-radius": {
                wgMapManager.showRadius(obj["param"]);
                break;
            }

            case "poi-suggestion":
            case "mark-poi":
            case "find-nearest-asset":
            case "view-playback":
            case "view-detail-poi":
            case "send-to-navigator": {
                // console.log(obj);
                MISInterface.sendMessage(MISInterface.createResponseResult({
                    command: obj["command"],
                    status: true,
                    result: obj["param"]
                }));
                break;
            }

            case "route-from-here":
            case "route-to-here": {
                // console.log(obj);
                MISInterface.sendMessage(MISInterface.createResponseResult({
                    command: obj["command"],
                    status: true,
                    result: obj["param"]
                }));

                wgMapManager.hideContextMenu();
                break;
            }

            case "open-vehicle-info-window":
            case "close-vehicle-info-window":
            case "open-playback-footprint-window":
            case "close-playback-footprint-window":
                MISInterface.sendMessage(MISInterface.createResponseResult({
                    command: obj["command"],
                    status: true,
                    result: obj["param"]
                }));
                break;
            case "enable-click-map-and-hide":
                ApplicationConfig.isClickMapAndHideMenu = true;
                break;
            case "disable-click-map-and-hide":
                ApplicationConfig.isClickMapAndHideMenu = false;
                break;
            case "mapclick-close-mis-menu":

                //console.log("ApplicationConfig.isClickMapAndHideMenu", ApplicationConfig.isClickMapAndHideMenu);
                if (ApplicationConfig.isClickMapAndHideMenu) {
                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "mapclick-close-mis-menu",
                        status: true,
                        result: {}
                    }));
                }
                break;
            case "open-street-view":
                var url = Utility.createStreetViewUrl(obj["param"]["geometry"]);

                window.open(url,"about_blank");
                break;
            case "re-draw-poi-suggestion":
                MISInterface.sendMessage(MISInterface.createResponseResult({
                    command: "re-draw-poi-suggestion",
                    status: true,
                    result: obj["param"]
                }));
                break;
            case "live-view":
                if(/*@cc_on!@*/false || !!document.documentMode) { // check ie
                    //window.open("http://gdev.geotalent.co.th/Bweb/SerVisionCam/Main2.htm", "live-view-window");
                    window.open("https://www.youtube.com/embed/XHCzdAD16fA?autoplay=1", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
                } else {
                    alert('support IE Only!!');
                }
                
                break;
            default:
                break;
        }
    }

    MISInterface.sendMessage(MISInterface.createResponseResult({
        command: "map-api-ready",
        status: true
    }));
});
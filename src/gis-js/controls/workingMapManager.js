﻿define(["dojo/_base/declare",
    "dijit/_WidgetBase",

    "esri/Color",
    "esri/map",
    "esri/graphic",
    "esri/layers/GraphicsLayer",

    "esri/geometry/Point",
    "esri/geometry/Polyline",
    "esri/geometry/Multipoint",
    "esri/SpatialReference",

    "esri/geometry/geometryEngine",
    "esri/geometry/webMercatorUtils",

    "esri/symbols/SimpleMarkerSymbol",
    "esri/symbols/SimpleLineSymbol",
    "esri/symbols/SimpleFillSymbol",
    "esri/symbols/PictureMarkerSymbol",

    "esri/toolbars/edit",
    "GOTModule/utils/Utility",
    "dojox/json/query",
    "dojo/query",

    "GOTModule/config/ApplicationConfig",
    "esri/layers/ArcGISTiledMapServiceLayer",


    "dojo/dom",
    "dojo/dom-style",
    "dojo/on",
    "dojo/_base/array",
    "dojo/_base/lang",

    "dojo/domReady!"
], function (declare,
    _WidgetBase,

    Color,
    Map,
    Graphic,
    GraphicsLayer,

    Point,
    Polyline,
    Multipoint,
    SpatialReference,
    geometryEngine,
    webMercatorUtils,

    SimpleMarkerSymbol,
    SimpleLineSymbol,
    SimpleFillSymbol,
    PictureMarkerSymbol,

    Edit,
    Utility,
    jsonQuery,
    nodeQuery,

    ApplicationConfig,
    ArcGISTiledMapServiceLayer,


    dom,
    domStyle,
    on,
    array,
    lang) {

    var map = null;
    var gpLyr = null;
    var pointLyr = null;
    var routeLyr = null;
    var pinLyr = null;
    var lyrAlert = null;
    var bufferLyr = null;
    var domMapId = null;
    var editToolbar = null;
    var arrLyr = [];
    var mpPoint = null;
    var lstColorMapping = null;
    var zoomLevel = null;
    var bLayer = null;
    // let map = ApplicationConfig.mapObject;
    let mapManager = ApplicationConfig.mapManager;
    var lyrStopSetting = jsonQuery("[?id='" + ApplicationConfig.commandOptions["playback-command"].layer.stop + "']", ApplicationConfig.defaultGraphicLayerList)[0];
    var lyrAlertSetting = jsonQuery("[?id='" + ApplicationConfig.commandOptions["playback-command"].layer.alert + "']", ApplicationConfig.defaultGraphicLayerList)[0];
    var widthDf = 15;
    var heightDf = 15;

    return declare([_WidgetBase], {

        constructor: function (args) {
            domMapId = args.domId;
        },

        createMap: function () {
            this.hideLoading();
            

            bufferLyr = new GraphicsLayer();
            gpLyr = new GraphicsLayer();
            pointLyr = new GraphicsLayer();
            pinLyr = new GraphicsLayer();
            lyrAlert = new GraphicsLayer();

            var fixLoadMap = (window.location.origin.indexOf("localhost:8080") > -1);
            if (fixLoadMap) {
                bLayer = new ArcGISTiledMapServiceLayer("https://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer");
                 
            }else{
                var basemap = ApplicationConfig.mapProviderData.basemap.all[0]
                var basemaptoDebrief = ApplicationConfig.mapProviderData.basemap.all[2]
                localStorage.setItem("basemap", JSON.stringify(basemap.urlLocal+"?token="+basemap.tokenLocal));
                localStorage.setItem("basemaptoDebrief", JSON.stringify(basemaptoDebrief.urlLocal+"?token="+basemaptoDebrief.tokenLocal));
                bLayer = new ArcGISTiledMapServiceLayer(basemaptoDebrief.urlLocal+"?token="+basemap.tokenLocal);     
            }

            arrLyr.push(bLayer,bufferLyr, gpLyr, pointLyr, pinLyr, lyrAlert);



            map = new Map(domMapId, {
                center: [100.5286114, 13.7236165],
                zoom: 16,
                //  basemap: "topo"
                basemap: "streets",
                slider: false,
                logo: false,
                showAttribution: false
            });

            map.addLayers(arrLyr);
            map.addLayers(bLayer);

            zoomLevel = map.getZoom();




            // on(map, 'load', lang.hitch(this, function (e) {
            //     editToolbar = new Edit(map);
            //     //this.drawPolygonFromPolyLine();

            // }));
        },

        drawPolygonFromPolyLine: function (param) {
            this.clearGraphics();
            var route = {
                paths: param.paths,
                spatialReference: {
                    wkid: 4326
                },
                type: "polyline"
            };

            var polyLine = new Polyline(route);
            var simpleLineSymbol = new SimpleLineSymbol();
            simpleLineSymbol.setColor(new Color([0, 0, 255, 1]));

            var buffer = geometryEngine.convexHull([polyLine], [20], "meters", true);
            var symbol = new SimpleFillSymbol();
            symbol.setColor(new Color([255, 0, 0, 0.333]));

            array.forEach(buffer, function (geometry) {
                var gp = new Graphic(geometry, symbol);

                editToolbar.activate(Edit.EDIT_VERTICES, gp);
                bufferLyr.add(gp);
            });

            gpLyr.add(new Graphic(polyLine, simpleLineSymbol));
        },
        drawRoute: function (param) {
            lstColorMapping = {};

            //get route from data
            let lstID = Utility.getUniqueItem(jsonQuery("[=id]", param.paths.playback));

            let indColor = 0;
            // lstID.map(lang.hitch(this, function (val) {
            //     if(indColor>1){
            //         indColor = 0;
            //     }

            //     lstColorMapping[val] = ApplicationConfig.playbackFullySetting.colorDebrief[indColor];
            //     indColor++;
            // }));
            var mpPoint = null;

            if (lstID.length > 0) {
                mpPoint = new Multipoint();
            }

            array.forEach(lstID, lang.hitch(this, function (id, ind) {
                let lstItem = jsonQuery("[?id='" + id + "'][=[lon,lat]]", param.paths.playback);
                let lstData = jsonQuery("[?id='" + id + "']", param.paths.playback);
                if (indColor > 1) {
                    indColor = 0;
                }
                //draw route
                let route = new Polyline(lstItem);
                let sym = new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color(ApplicationConfig.playbackFullySetting.color[indColor]), 6);
                let gp = new Graphic(route, sym);
                gpLyr.add(gp);

                array.forEach(lstData, lang.hitch(this, function (itm, index) {
                    let pt = new Point([itm.lon, itm.lat]);
                    let symb = null;

                    mpPoint.addPoint(pt);

                    // console.log(itm);

                    if (index != 0 && index != lstData.length - 1) {


                        switch (itm.movement) {

                            case "PARKENGINEON":
                                {

                                    let width = ApplicationConfig.playbackFullySetting.footprintEngineOnPin.width;
                                    let height = ApplicationConfig.playbackFullySetting.footprintEngineOnPin.height;

                                    if (lyrStopSetting.enableResize) {
                                        itm.attributes[ApplicationConfig.constantKeys["defaultIconHeight"]] = height;
                                        itm.attributes[ApplicationConfig.constantKeys["defaultIconWidth"]] = width;


                                        // width = Utility.calculateSizeForZoomFactor(width, zoomLevel);
                                        // height = Utility.calculateSizeForZoomFactor(height, zoomLevel);
                                    }

                                    symb = new PictureMarkerSymbol(
                                        ApplicationConfig.playbackFullySetting.footprintEngineOnPin.url,
                                        widthDf,
                                        heightDf);
                                    break;
                                }

                            case "PARK":
                                {
                                    //  console.log("PARK  JAAAAAAAAAAAA")
                                    let width = ApplicationConfig.playbackFullySetting.footprintParkPin.width;
                                    let height = ApplicationConfig.playbackFullySetting.footprintParkPin.height;

                                    if (lyrStopSetting.enableResize) {
                                        itm.attributes[ApplicationConfig.constantKeys["defaultIconHeight"]] = height;
                                        itm.attributes[ApplicationConfig.constantKeys["defaultIconWidth"]] = width;


                                        width = Utility.calculateSizeForZoomFactor(width, zoomLevel);
                                        height = Utility.calculateSizeForZoomFactor(height, zoomLevel);


                                    }

                                    symb = new PictureMarkerSymbol(
                                        ApplicationConfig.playbackFullySetting.footprintParkPin.url,
                                        widthDf,
                                        heightDf);

                                    break;
                                }
                            case "STOP":
                                {
                                    // console.log("MOVE  JAAAAAAAAAAAA")
                                    let width = ApplicationConfig.playbackFullySetting.footprintStopPin.width;
                                    let height = ApplicationConfig.playbackFullySetting.footprintStopPin.height;

                                    if (lyrStopSetting.enableResize) {
                                        itm.attributes[ApplicationConfig.constantKeys["defaultIconHeight"]] = height;
                                        itm.attributes[ApplicationConfig.constantKeys["defaultIconWidth"]] = width;

                                        width = Utility.calculateSizeForZoomFactor(width, zoomLevel);
                                        height = Utility.calculateSizeForZoomFactor(height, zoomLevel);
                                    }

                                    symb = new PictureMarkerSymbol(
                                        ApplicationConfig.playbackFullySetting.footprintStopPin.url,
                                        widthDf,
                                        heightDf);
                                    break;
                                }
                            default:
                                {

                                    let width = ApplicationConfig.playbackFullySetting.footprintPin.width;
                                    let height = ApplicationConfig.playbackFullySetting.footprintPin.height;

                                    if (lyrStopSetting.enableResize) {
                                        itm.attributes[ApplicationConfig.constantKeys["defaultIconHeight"]] = height;
                                        itm.attributes[ApplicationConfig.constantKeys["defaultIconWidth"]] = width;

                                        width = Utility.calculateSizeForZoomFactor(width, zoomLevel);
                                        height = Utility.calculateSizeForZoomFactor(height, zoomLevel);
                                    }

                                    symb = new PictureMarkerSymbol(
                                        ApplicationConfig.playbackFullySetting.footprintPin.url,
                                        widthDf,
                                        heightDf);
                                    break;
                                }
                        }

                        symb.setAngle(itm.rotation || 0);

                        pointLyr.add(new Graphic(pt, symb, itm.attributes));

                    }

                }));



                let startWidth = ApplicationConfig.playbackFullySetting.pinBegin.width;
                let startHeight = ApplicationConfig.playbackFullySetting.pinBegin.height;
                let endWidth = ApplicationConfig.playbackFullySetting.pinEnd.width;
                let endHeight = ApplicationConfig.playbackFullySetting.pinEnd.height;

                if (lyrStopSetting.enableResize) {
                    lstData[0].attributes[ApplicationConfig.constantKeys["defaultIconWidth"]] = startWidth;
                    lstData[0].attributes[ApplicationConfig.constantKeys["defaultIconHeight"]] = startHeight;
                    lstData[lstData.length - 1].attributes[ApplicationConfig.constantKeys["defaultIconWidth"]] = endWidth;
                    lstData[lstData.length - 1].attributes[ApplicationConfig.constantKeys["defaultIconHeight"]] = endHeight;

                    startWidth = Utility.calculateSizeForZoomFactor(startWidth, zoomLevel);
                    startHeight = Utility.calculateSizeForZoomFactor(startHeight, zoomLevel);
                    endWidth = Utility.calculateSizeForZoomFactor(endWidth, zoomLevel);
                    endHeight = Utility.calculateSizeForZoomFactor(endHeight, zoomLevel);
                }
                // startWidth,startHeight  endWidth,endHeight
                // gen pin start end
                var symbBegin = new PictureMarkerSymbol(
                    ApplicationConfig.playbackFootprintSetting.pinBegin.url,
                    widthDf,
                    heightDf);
                var symbEnd = new PictureMarkerSymbol(
                    ApplicationConfig.playbackFootprintSetting.pinEnd.url,
                    widthDf,
                    heightDf);

                //symbPoint.setOffset(ApplicationConfig.playbackFullySetting.destinationPin.offsetX, ApplicationConfig.playbackFullySetting.destinationPin.offsetY);

                var pointStart = new Point(lstData[0].lon, lstData[0].lat);
                var pointEnd = new Point(lstData[lstData.length - 1].lon, lstData[lstData.length - 1].lat);

                pinLyr.add(new Graphic(pointStart, symbBegin, lstData[0].attributes));
                pinLyr.add(new Graphic(pointEnd, symbEnd, lstData[lstData.length - 1].attributes));

                //clone data and set empty status
                let data = JSON.parse(JSON.stringify(lstData[0].attributes));
                data["STATUS"] = [];

                //this.drawAttributes(data, ApplicationConfig.playbackFullySetting.color[ind]);
                // this.drawAttributes(data, lstColorMapping[id]);
                indColor++;
                //map.setExtent(route.getExtent());
            }));

            // gen Alert
            array.forEach(param.paths.alert, lang.hitch(this, function (alertItem, ind) {
                let alertWidth = 32;
                let alertHeight = 32;

                if (lyrAlertSetting.enableResize) {
                    alertItem.attributes[ApplicationConfig.constantKeys["defaultIconHeight"]] = alertWidth;
                    alertItem.attributes[ApplicationConfig.constantKeys["defaultIconWidth"]] = alertHeight;

                    alertWidth = Utility.calculateSizeForZoomFactor(alertWidth, zoomLevel);
                    alertHeight = Utility.calculateSizeForZoomFactor(alertHeight, zoomLevel);
                }

                var pointAlert = new Point(alertItem.longitude, alertItem.latitude);
                var symbAlert = new PictureMarkerSymbol(alertItem.attributes.imageUrl, alertWidth, alertHeight);

                lyrAlert.add(new Graphic(pointAlert, symbAlert, alertItem.attributes));
            }));
            
            map.setExtent(mpPoint.getExtent().expand(1.5));

            // map.disableScrollWheelZoom();
            // map.disableRubberBandZoom();
            // map.disablePan();
            map.disableMapNavigation();

            // mpPoint = new Multipoint();

            // this.clearGraphics();
            // var route = {
            //     paths: param.paths.playback,
            //     spatialReference: {
            //         wkid: 4326
            //     },
            //     type: "polyline"
            // };

            // console.log("this route", route);


            // var polyLine = new Polyline(route);
            // var simpleLineSymbol = new SimpleLineSymbol();
            // simpleLineSymbol.setColor(new Color([0, 0, 255, 1]));

            // gpLyr.add(new Graphic(polyLine, simpleLineSymbol));

            // map.setExtent(polyLine.getExtent());
            // // let pt = new Point(100.88141,14.69539,100.32493,13.6783);
            // array.forEach(param.paths.playback, lang.hitch(this, function (itm, index) {
            //     console.log("itm", itm[index][0]);
            //     console.log("itm", itm[index][1]);
            //     console.log("index", index);
            //     console.log(">>>", ApplicationConfig.playbackFullySetting.footprintEngineOnPin.url);
            //     //let pt = new Point(100, 13, new SpatialReference({wkid:4326}));
            //     let pt = new Point(itm[index][0], itm[index][1], new SpatialReference({
            //         wkid: 4326
            //     }));
            //     let symb = null;


            //     mpPoint.addPoint(pt);

            //     // symb = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 10,
            //     //     new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
            //     //     new Color([255,0,0]), 1),
            //     //     new Color([0,255,0,0.25]));

            //     symb = new PictureMarkerSymbol();
            //     symb.setHeight(30);
            //     symb.setWidth(30);
            //     symb.setUrl("http://img3.wikia.nocookie.net/__cb20140427224234/caramelangel714/images/7/72/Location_Icon.png");
            //     //symb.serUrl(ApplicationConfig.playbackFullySetting.footprintPin.url);

            //     console.log(pt);
            //     console.log(symb);


            //     pointLyr.add(new Graphic(pt, symb));

            // }));

            // console.log(map);
        },

        getResultPolygon: function () {
            var geometryLst = [];

            array.forEach(bufferLyr.graphics, function (gp) {
                geometryLst.push(gp.geometry.rings);
            });

            return geometryLst;
        },

        clearGraphics: function () {
            array.forEach(arrLyr, function (lyr) {
                lyr.clear();
            })
            editToolbar.deactivate();
        },

        hideLoading: function () {
            var divLoading = dom.byId("divLoading")
            domStyle.set(divLoading, "display", "none");
        },

        drawAttributes: function (attr, color) {

            console.log("attr", attr);
            console.log("color", color);

            // //create attributes div

            // let divAttrContainer = nodeQuery("[data-id='" + attr.ID + "']", this.divListStat)[0];
            // console.log("divAttrContainer",divAttrContainer);

            // if (!divAttrContainer) {
            //     divAttrContainer = domConstruct.create("div", { "data-id": attr.ID, class: "div-stat-item" }, this.divListStat, "last");
            // } else {
            //     domConstruct.empty(divAttrContainer);
            // }

            // let divAssetHeader = domConstruct.create("div", {}, divAttrContainer, "last");
            // let divColor = domConstruct.create("div", { class: "div-asset-pb-color" }, divAssetHeader);
            // let divAssetID = domConstruct.create("div", { class: "div-attr-asset-id", innerHTML: attr["TITLE"] + " (" + attr["BOX_ID"] + ")" }, divAssetHeader);

            // domStyle.set(divColor, { "backgroundColor": color });

            let divAttrList = domConstruct.create("div", {
                class: "div-lst-attr"
            }, divAttrContainer);
            let divDoubleContainer = null;

            //console.log("dasdasdas");

            //console.log("attr", attr);

            if (attr["STATUS"].length > 0) {
                let divLocation = domConstruct.create("div", {
                    class: "div-status-blog"
                }, divAttrList, "last");
                let divLocationContainer = domConstruct.create("div", {
                    class: "div-location-container"
                }, divLocation, "last");
                let divLocationSVG = domConstruct.create("div", {
                    class: "div-svg-icon-status"
                }, divLocationContainer, "last");
                require([
                    "dojo/text!./gis-js/images/svg/icon_location_playback_info.svg"
                ], lang.hitch(this, function (svgRaw) {
                    let svgIcon = domConstruct.toDom(svgRaw);
                    domConstruct.place(svgIcon, divLocationSVG, "last");
                }));


                let divLocationText = domConstruct.create("div", {
                    class: "div-location-text",
                    innerHTML: attr.LOCATION
                }, divLocationContainer, "last");
            }


            array.forEach(attr["STATUS"], lang.hitch(this, function (data, i) {
                if (i % 2 == 0) {
                    divBlock = domConstruct.create("div", {
                        class: "div-status-blog"
                    }, divAttrList, "last");
                }

                let divContainer = domConstruct.create("div", {
                    class: "div-icon-container",
                    title: data.title || ""
                }, divBlock, "last");

                let divIcon = domConstruct.create("div", {
                    class: "div-svg-icon-status " + data.cssClass
                }, divContainer, "last");
                let svg = domConstruct.toDom(data.icon);

                let divText = domConstruct.create("div", {
                    class: "div-svg-icon-status-text",
                    innerHTML: data.text || "",
                    title: data.text || ""
                }, divContainer, "last");

                domConstruct.place(svg, divIcon, "last");
            }));
        }
    });
});
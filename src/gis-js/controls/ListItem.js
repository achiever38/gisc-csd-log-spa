﻿define([
    "dojo/_base/declare",
    "dojo/_base/event",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",

    "dojo/text!./templates/ListItem.html",

    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-attr",
    "dojo/dom-style",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/on",
    "dojox/json/query"
], function (declare, dojoEvent, _WidgetBase, _TemplatedMixin, itemTemplate, domConstruct, domClass, domAttr, domStyle, lang, array, on, jsonQuery) {
    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: itemTemplate,
        data: null,

        type: "default",
        useHighlight: true,
        selected: false,

        _checkImgNormalUrl: "./gis-js/images/checkbox_normal.png",
        _checkImgSelectedUrl: "./gis-js/images/checkbox_selected.png",

        constructor: function (params) {
            this.data = params;
        },

        postCreate: function () {
            /* set attributes */
            domAttr.set(this.divItem, "data-item-key", this.data.key);
            domAttr.set(this.divText, "innerHTML", this.data.text);

            if (this.type == "checked") {
                domStyle.set(this.divIconCheck,"display","table-cell");
            }

            if (this.data.icon) {
                domAttr.set(this.imgIcon, "src", this.data.icon);
            }
            else if (this.data.svg) {
                domConstruct.empty(this.divIcon);

                require(["dojo/text!"+ this.data.svg], lang.hitch(this, function (divContainer, svgText) {
                    //console.log(divContainer);
                    domConstruct.place(domConstruct.toDom(svgText), divContainer, "last");
                }, this.divIcon));
            }
            else
                domStyle.set(this.divIcon, "display", "none");

            if (this.data.blankIcon) {
                domStyle.set(this.divIcon, "display", "table-cell");
                domStyle.set(this.divIcon, { width: 0 });
                domConstruct.empty(this.divIcon);
            }

            if (this.data.hasDelete) {
                domClass.add(this.divItem, "has-delete");

                require(["dojo/text!./gis-js/images/svg/icon_panel_close.svg"], lang.hitch(this, function (svgText) {
                    domConstruct.place(domConstruct.toDom(svgText), this.divDelete, "last");
                }));

                on(this.divIcon, "click", lang.hitch(this, "onItemClick", this.data, this.domNode, this));
                on(this.divText, "click", lang.hitch(this, "onItemClick", this.data, this.domNode, this));

                on(this.divDelete, "click", lang.hitch(this, "_onDeleteClick", this.data, this.domNode));
            }
            else {
                /* binding event */
                on(this.divItem, "click", lang.hitch(this, "onItemClick", this.data, this.domNode, this));
            }

            this._setSelectedAttr(this.data.selected);
            //if (this.data.selected) {
            //    domClass.add(this.domNode, "selected");
            //}
        },

        _setSelectedAttr: function (selected) {

            //console.log(selected);
            if (this.type == "checked") {
                if (selected) {
                    domAttr.set(this.imgCheck, "src", this._checkImgSelectedUrl);
                }
                else {
                    domAttr.set(this.imgCheck, "src", this._checkImgNormalUrl);
                }
            }

            if (this.useHighlight){
                if (selected) {
                    domClass.add(this.domNode, "selected");
                }
                else {
                    domClass.remove(this.domNode, "selected");
                    //domAttr.set(this.imgCheck, "src", this._checkImgNormalUrl);
                }
            }

            this.selected = selected;
        },

        _onDeleteClick: function (item, node, e) {
            this.onDeleteClick(item, node, e);
        },

        onItemClick: function (item, node, self) { /* attach event */
        
        },

        onDeleteClick: function (item, node, e) { /* attach event */
            
        }

    });
});
﻿define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",

    "dojo/text!./templates/ListItemContainer.html",
    
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/on",
    "dojox/json/query",

    "GOTModule/controls/ListItem",
    "GOTModule/controls/ListItemCheckAll",
    "GOTModule/controls/ListItemCheckBox",
    

    "GOTModule/utils/Utility"
], function (declare, _WidgetBase, _TemplatedMixin, containerTemplate, domClass, domConstruct, lang, array, on, jsonQuery, ListItem, ListItemCheckAll, ListItemCheckBox, Utility) {

    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: containerTemplate,
        items: null,

        selectedMode: "no", //one : toggle : no : one-toggle

        type: "default",
        useHighlight: true,

        constructor: function (params) {
            if (params) {
                this.selectedMode = params.selectedMode || "no";
                this.type = params.type;
            }
            else { this.selectedMode = "no"; }

            this.items = [];
        },

        createItem: function (params) {
            if (params.length && params.length > 0) {
                array.forEach(params, lang.hitch(this, "createItem"));
            }
            else if (params.length == 0) { }
            else if (params) {
                params.type = this.type;
                params.useHighlight = this.useHighlight;
                // let itm = new ListItem(params);
                if (this.type == "checkAll"){
                    if(params.isCheckAll){
                        itm = new ListItemCheckAll(params);
                    }else{
                        itm = new ListItemCheckBox(params);
                    }
                }
                else {
                    itm = new ListItem(params);
                }

                
                this.items.push(itm);

                on(itm, "ItemClick", lang.hitch(this, "_onItemClick"));
                on(itm, "DeleteClick", lang.hitch(this, "onDeleteClick"));
                domConstruct.place(itm.domNode, this.divContainer, "last");
            }
            else { }
        },

        setSelectedIndex: function (lst, flg) {

            if(flg == undefined || flg == null)
                flg = true;

            if(lst && !lst.length)
                lst = [lst];

            for (let ind in lst) {
                this.items[lst[ind]].set("selected", flg);
                //domClass.add(this.items[lst[ind]].domNode, "selected");
            }
        },

        removeItem: function (index) { },

        removeAllItem: function () {
            domConstruct.empty(this.divContainer);
            this.items = [];
        },

        getCountSelected: function () {
            let count = 0;

            array.forEach(this.items, lang.hitch(this, function (itm) {
                if (itm.get("selected")
                    //domClass.contains(itm.domNode, "selected")
                ) {
                    count++;
                }
            }));

            return count;
        },

        _onItemClick: function (item, node, widget) {

            if (this.selectedMode == "one") {
                array.forEach(this.items, lang.hitch(this, function (itm) {
                    //domClass.remove(itm.domNode, "selected");
                    itm.set("selected",false);
                }));

                widget.set("selected",true);

                // domClass.add(node, "selected");
            }
            else if (this.selectedMode == "toggle") {
                //domClass.toggle(node, "selected");
                //widget.set("selected", true);

                if (widget.get("selected")){
                    widget.set("selected", false);
                }
                else {
                    widget.set("selected", true);
                }
                
                //lang.mixin(item, { isSelected: domClass.contains(node, "selected") });
               lang.mixin(item, { isSelected: widget.get("selected") });
            }
            else if (this.selectedMode == "one-toggle") {
                //if (domClass.contains(node, "selected")) {
                if (widget.get("selected")) {
                    array.forEach(this.items, lang.hitch(this, function (itm) {
                        //domClass.remove(itm.domNode, "selected");
                        itm.set("selected", false);
                    }));
                }
                else {
                    array.forEach(this.items, lang.hitch(this, function (itm) {
                        //domClass.remove(itm.domNode, "selected");
                        itm.set("selected", false);
                    }));
                    widget.set("selected", true);
                    //domClass.add(node, "selected");
                }
            }
            else { }

            this.onItemClick(item);
        },

        onItemClick: function (item) { /* attach event */ },
        onDeleteClick: function (item, node) { /* attach event */ }
    });
});
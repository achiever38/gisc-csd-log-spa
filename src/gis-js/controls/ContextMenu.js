﻿define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",

    "dojo/text!./templates/ContextMenu.html",

    "dojo/dom",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/dom-attr",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/on",
    "dojox/json/query",

    "GOTModule/controls/ListItemContainer",

    "GOTModule/config/ApplicationConfig",
    "GOTModule/utils/ResourceLib",
    "GOTModule/utils/Utility"
], function (declare, _WidgetBase, _TemplatedMixin, template, dom, domConstruct, domClass, domStyle, domAttr, lang, array, on, jsonQuery,
    ListItemContainer, ApplicationConfig, ResourceLib, Utility) {

    var contextType = {
        MAP: "map",
        POI: "poi",
        VEHICLE: "vehicle"
    }

    var _cls = declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,
        context: null,
        contextData: null,

        constructor: function (params) {
            this.context = {};
        },
        postCreate: function () {
            let contextItemGroup = ApplicationConfig.contextMenu;

            for (let grpKey in contextItemGroup) {
                let arrCreate = [];
                let grp = contextItemGroup[grpKey];

                for (let key in grp) {
                    grp[key].text = ResourceLib.text("context-" + grp[key]["key"] + "-text");

                    if (grp[key].permission) {

                        if (ApplicationConfig.userPermission.indexOf(grp[key].permission) > -1) {
                            arrCreate.push(grp[key]);
                        }
                    }
                    else {
                        arrCreate.push(grp[key]);
                    }
                }

                let grpContext = new ListItemContainer();
                grpContext.createItem(arrCreate);

                on(grpContext, "ItemClick", lang.hitch(this, "_ListClickHandler"));

                domConstruct.place(grpContext.domNode, this.divMain, "last");

                this.context[grpKey] = { wg: grpContext, node: grpContext.domNode };
            }
        },

        show: function (top, left, type, data) {

            this.contextData = data;

            var maxLeft = window.innerWidth;
            var maxTop = window.innerHeight;
            var constHeight = ApplicationConfig.contextMenu[type].length * 50;
            left += 5;

            //console.log(top, left);

            //if (type == contextType.MAP) {
            //    constHeight = 250;
            //}

            if ((top + constHeight) > maxTop) {
                top = maxTop - (constHeight + 10);
            }

            if ((left + 230) > maxLeft) {
                left = left - 200;
            }

            for (let key in this.context) {
                if (type == key) {
                    domStyle.set(this.context[key].node, "display", "block");
                }
                else {
                    domStyle.set(this.context[key].node, "display", "none");
                }
            }

            //console.log(top, left, type, data);

            domStyle.set(this.domNode, {
                "display": "block",
                "top": top + "px",
                "left": left + "px"
            });
        },

        hide: function () {
            domStyle.set(this.domNode, "display", "none");
        },

        _ListClickHandler: function (e) {
            this.onAction(lang.mixin(this.contextData, { command: e.key }));
        },

        onAction: function (obj) { /* attach event */ }
    });

    lang.mixin(_cls, { contextType: contextType } );

    return _cls;
});
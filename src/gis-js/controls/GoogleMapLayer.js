﻿
define(["dojo/_base/declare",
    "dojo/on",
    "dojo/_base/lang",
    "esri/layers/layer"
],
    function (declare, on, lang,
        layer) {
        return declare( [layer], {
            onGMapsApiLoad: null,
            MAP_TYPE_SATELLITE: "satellite",
            MAP_TYPE_HYBRID: "hybrid",
            MAP_TYPE_ROADMAP: "roadmap",
            MAP_TYPE_TERRAIN: "terrain",
            MAP_STYLE_GRAY: [{
                featureType: 'all',
                stylers: [{
                    saturation: -80
                }, {
                    lightness: 20
                }]
            }],
            MAP_STYLE_LIGHT_GRAY: [{
                featureType: 'all',
                stylers: [{
                    saturation: -80
                }, {
                    lightness: 60
                }]
            }],
            MAP_STYLE_NIGHT: [{
                featureType: 'all',
                stylers: [{
                    invert_lightness: 'true'
                }]
            }],
            _map: null,
            _gmap: null,
            _google: null,
            _traffic: null,
            _istraffic: false,
            _transportation: null,
            _isTransportation: false,
            events: {
                load: null,
                error: null
            },
            _key: null,
            _clientID: null,
            _language: "TH",
            _maxScale: 20,
            _minScale: 0,
            layer: null,


            constructor: function (map, opts) {
                console.log("constructor nostraMap", map);

                opts = opts || {};
                this.isGoogleMapLayer = dojo.mixin({}, true);

                // declare.safeMixin(this, opts);

                // this._key = opts.key;
                // this tileInfo does not actually do anything. It simply tricks nav control to 
                // show a slider bar if only gmaps are used, which should be a rare case.
                this.tileInfo = new esri.layers.TileInfo({
                    rows: 256,
                    cols: 256,
                    dpi: 96,
                    origin: {
                        x: -20037508.342787,
                        y: 20037508.342787
                    },
                    spatialReference: {
                        wkid: 102100
                    },
                    lods: [{
                        level: 0,
                        resolution: 156543.033928,
                        scale: 591657527.591555
                    }, {
                        level: 1,
                        resolution: 78271.5169639999,
                        scale: 295828763.795777
                    }, {
                        level: 2,
                        resolution: 39135.7584820001,
                        scale: 147914381.897889
                    }, {
                        level: 3,
                        resolution: 19567.8792409999,
                        scale: 73957190.948944
                    }, {
                        level: 4,
                        resolution: 9783.93962049996,
                        scale: 36978595.474472
                    }, {
                        level: 5,
                        resolution: 4891.96981024998,
                        scale: 18489297.737236
                    }, {
                        level: 6,
                        resolution: 2445.98490512499,
                        scale: 9244648.868618
                    }, {
                        level: 7,
                        resolution: 1222.99245256249,
                        scale: 4622324.434309
                    }, {
                        level: 8,
                        resolution: 611.49622628138,
                        scale: 2311162.217155
                    }, {
                        level: 9,
                        resolution: 305.748113140558,
                        scale: 1155581.108577
                    }, {
                        level: 10,
                        resolution: 152.874056570411,
                        scale: 577790.554289
                    }, {
                        level: 11,
                        resolution: 76.4370282850732,
                        scale: 288895.277144
                    }, {
                        level: 12,
                        resolution: 38.2185141425366,
                        scale: 144447.638572
                    }, {
                        level: 13,
                        resolution: 19.1092570712683,
                        scale: 72223.819286
                    }, {
                        level: 14,
                        resolution: 9.55462853563415,
                        scale: 36111.909643
                    }, {
                        level: 15,
                        resolution: 4.77731426794937,
                        scale: 18055.954822
                    }, {
                        level: 16,
                        resolution: 2.38865713397468,
                        scale: 9027.977411
                    }, {
                        level: 17,
                        resolution: 1.19432856685505,
                        scale: 4513.988705
                    }, {
                        level: 18,
                        resolution: 0.597164283559817,
                        scale: 2256.994353
                    }, {
                        level: 19,
                        resolution: 0.298582141647617,
                        scale: 1128.497176
                    }, {
                        level: 20,
                        resolution: 0.149291070823808,
                        scale: 564.248588
                    }]
                });

                this.fullExtent = new esri.geometry.Extent({
                    xmin: -20037508.34,
                    ymin: -20037508.34,
                    xmax: 20037508.34,
                    ymax: 20037508.34,
                    spatialReference: {
                        wkid: 102100
                    }
                });
                this.initialExtent = new esri.geometry.Extent({
                    xmin: -20037508.34,
                    ymin: -20037508.34,
                    xmax: 20037508.34,
                    ymax: 20037508.34,
                    spatialReference: {
                        wkid: 102100
                    }
                });
                this.opacity = opts.opacity || 1;
                this._mapOptions = opts.mapOptions || {};

                var apiOpts = {};

                if (opts.hasOwnProperty("googleKey")) {
                    apiOpts.key = opts.googleKey;
                }

                if (opts.hasOwnProperty("googleClientID")) {
                    apiOpts.client = opts.googleClientID;
                }

                if (opts.hasOwnProperty("mapType")) {
                    this._mapOptions.mapTypeId = opts.mapType;
                }

                if (opts.hasOwnProperty("language")) {
                    apiOpts.language = opts.language;
                }

                if (opts.hasOwnProperty("traffic")) {
                    this._istraffic = opts.traffic;
                }

                if (opts.hasOwnProperty("transit")) {
                    this._isTransportation = opts.transit;
                }

                /*opts.apiOptions*/
                this._apiOptions = dojo.mixin({
                    sensor: false,
                    streetViewControl: false,
                    disableDefaultUI: false
                }, apiOpts || {});


                /*if (this._mapOptions.streetViewControl == undefined) {
                 this._mapOptions.streetViewControl = false;
                 }*/

                this.attachEvent();
            },

            attachEvent: function () {
                this._gmap = null;
                this.loaded = true;// it seems _setMap will only get called if loaded = true, so set it here first.
                this.onLoad(this);
            },

            /**
             * get the wrapped <code>google.maps.Map</code> for further customization.
             * @function
             * @name GoogleMapsLayer#getGoogleMapInstance
             * @return {google.maps.Map}
             */
            getGoogleMapInstance: function () {
                return this._gmap;
            },
            // override parent class abstract method
            /**********************
             * @see http://help.arcgis.com/EN/webapi/javascript/arcgis/demos/exp/javascript/RasterLayer.js
             * Internal Properties
             *
             * _map
             * _element
             * _context
             * _mapWidth
             * _mapHeight
             * _connects
             *
             **********************/
            _setMap: function (map, container) {
                // This overrides an undocumented private method from ESRI API. 
                // It's possible not to do this, but it requires
                // map instance implicitly set to the layer instance, which is a little bit inconvenient.
                // this is likely called inside esriMap.addLayer()
                this._map = map;
                // this style is used to style 0 size divs, mainly for holding it's DOM position but not obstructing events.
                var style = {
                    position: 'absolute',
                    top: '0px',
                    left: '0px',
                    width: '0px',
                    height: '0px'
                };
                var element = dojo.create('div', {}, container);
                if (this.id) {
                    element.id = this.id;
                }
                dojo.style(element, style);
                this._element = element;
                var div = dojo.create('div', {}, element);
                dojo.style(div, style);
                dojo.style(div, 'width', (map.width || container.offsetWidth) + 'px');
                dojo.style(div, 'height', (map.height || container.offsetHeight) + 'px');
                this._gmapDiv = div;

                // topDiv is used to mask all esri events in oblique mode.
                var tdiv = dojo.create('div', {}, map.id);
                tdiv.id = 'gmaps_top_' + div.id;
                dojo.style(tdiv, style);
                this._topDiv = tdiv;
                // controlDiv is used to hold pegman and oblique rotater.
                var cdiv = dojo.create('div', {}, map.id);
                cdiv.id = 'gmaps_controls_' + div.id;
                dojo.style(cdiv, dojo.mixin(style, {
                    // width: '0px',
                    // height: '0px',
                    top: '5px',
                    left: '5px'
                }));
                this._controlDiv = cdiv;

                //this._container = layersDiv;
                // Event connections
                this._connects = [];
                this._connects.push(dojo.connect(this, 'onVisibilityChange', this, this._visibilityChangeHandler));
                this._connects.push(dojo.connect(this, 'onOpacityChange', this, this._opacityChangeHandler));

                this.visible = (this.visible === undefined) ? true : this.visible;
                if (this.visible) {
                    this._initGMap();
                }
                return element;
            },
            _unsetMap: function (map, layersDiv) {
                // see _setMap. Undocumented method, but probably should be public.
                // console.log('unsetmap');
                dojo.forEach(this._connects, dojo.disconnect, dojo);
                if (this._streetView) {
                    this._streetView.setVisible(false);
                }
                if (google && google.maps && google.maps.event) {
                    if (this._gmapTypeChangeHandle)
                        google.maps.event.removeListener(this._gmapTypeChangeHandle);
                    if (this._svVisibleHandle)
                        google.maps.event.removeListener(this._svVisibleHandle);
                }
                if (this._element)
                    this._element.parentNode.removeChild(this._element);
                dojo.destroy(this._element);
                if (this._controlDiv)
                    this._controlDiv.parentNode.removeChild(this._controlDiv);
                dojo.destroy(this._controlDiv);
                if (this._topDiv)
                    this._topDiv.parentNode.removeChild(this._topDiv);
                dojo.destroy(this._topDiv);
                //this._element = this._gmapDiv = this._controlDiv = null;
            },

            // delayed init and Api loading.
            _initGMap: function () {
                window.google = window.google || {}; // somehow IE needs this, otherwise complain google.maps namespace;
                if (window.google && google.maps) {
                    this._google = google.maps;
                    var ext = this._map.extent;
                    var center = this._mapOptions.center || this._esriPointToLatLng(ext.getCenter());
                    var level = this._map.getLevel();
                    var myOptions = dojo.mixin({
                        disableDefaultUI: true,
                        center: center,
                        zoom: (level > -1) ? level : 1,
                        panControl: false,
                        mapTypeControl: false,
                        zoomControl: false
                    }, this._mapOptions);

                    if (myOptions.mapTypeId) {
                        myOptions.mapTypeId = this._getGMapTypeId(myOptions.mapTypeId);
                    } else {
                        myOptions.mapTypeId = google.maps.MapTypeId.ROADMAP;
                    }
                    var gmap = new google.maps.Map(this._gmapDiv, myOptions);
                    if (level < 0) {
                        dojo.connect(this._map, 'onLoad', dojo.hitch(this, function () {
                            this._setExtent(ext);
                        }));
                    }
                    this._gmap = gmap;
                    this.layer = gmap;
                    this._setExtent(ext);

                    this._extentChangeHandle = dojo.connect(this._map, 'onExtentChange', this, this._extentChangeHandler);
                    this._panHandle = dojo.connect(this._map, 'onPan', this, this._panHandler);
                    this._resizeHandle = dojo.connect(this._map, 'onResize', this, this._resizeHandler);
                    // 45 deg need move up regardless of streetview
                    this._mvHandle = dojo.connect(this._map, 'onMouseMove', dojo.hitch(this, this._moveControls));
                    this._gmapTypeChangeHandle = google.maps.event.addListener(this._gmap, 'maptypeid_changed', dojo.hitch(this, this._mapTypeChangeHandler));
                    this._gmapTiltChangeHandle = google.maps.event.addListener(this._gmap, 'tilt_changed', dojo.hitch(this, this._mapTiltChangeHandler));

                    this._traffic = new google.maps.TrafficLayer();
                    this._transportation = new google.maps.TransitLayer();

                    if (this._istraffic == true) {
                        this._traffic.setMap(gmap);
                    }

                    if (this._isTransportation == true) {
                        this._transportation.setMap(gmap);
                    }

                    this.onLoad();

                } else if (this.onGMapsApiLoad) {
                    // did another instance already started loading agsjs API but not done?
                    // this should be very very rare because one instance of this layer would be sufficient with setMapTypeId.
                    dojo.connect(this._map, 'onGMapsApiLoad', this, this._initGMap);

                } else {
                    // this is the first instance that tries to load agsjs API on-demand
                    this._map.onGMapsApiLoad = function () {
                        // do nothing, just needed to dispatch event.
                    };
                    dojo.connect(this._map, 'onGMapsApiLoad', this, this._initGMap);
                    var script = document.createElement('script');
                    script.type = 'text/javascript';
                    var pro = window.location.protocol;

                    if (pro.toLowerCase().indexOf('http') == -1) {
                        pro = 'http:';
                    }

                    var src = pro + '//maps.googleapis.com/maps/api/js?callback=gmapA.onGMapsApiLoad';

                    //if (this._key && !this._clientID) {
                    //    src += "&key=" + this._key;
                    //} else if (this._clientID && !this._key) {
                    //    src += "&client=" + this._clientID;
                    //}

                    // var src = pro + '//maps.googleapis.com/maps/api/js?callback=nostra.maps.onGMapsApiLoad&key=AIzaSyBkmNJSEDkC7MAPayuLS-WlhXqKWjXXoMA';
                    for (var x in this._apiOptions) {
                        if (this._apiOptions.hasOwnProperty(x)) {
                            src += '&' + x + '=' + this._apiOptions[x];
                        }
                    }

                    script.src = src;
                    if (document.getElementsByTagName('head').length > 0) {
                        document.getElementsByTagName('head')[0].appendChild(script);
                    } else {
                        document.body.appendChild(script);
                    }
                }

            },

            /**
             * Sets Opacity
             * @name GoogleMapsLayer#setOpacity
             * @function
             * @param {Number} opacity from 0-1
             */
            setOpacity: function (opacity) {
                // dojo core should have something do this
                if (this._gmapDiv) {
                    opacity = Math.min(Math.max(opacity, 0), 1);
                    var st = this._gmapDiv.style;
                    if (typeof st.opacity !== 'undefined') {
                        st.opacity = opacity;
                    } else if (typeof st.filters !== 'undefined') {
                        st.filters.alpha.opacity = Math.floor(100 * opacity);
                    } else if (typeof st.filter !== 'undefined') {
                        st.filter = "alpha(opacity:" + Math.floor(opacity * 100) + ")";
                    }
                }
                this.opacity = opacity;
            },
            /**
             * set map type id. e.g <code>GoogleMapsLayer.MAP_TYPE_ROADMAP</code>
             *
             * @name GoogleMapsLayer#setMapTypeId
             * @function
             * @param {String} mapTypeId one of GoogleMapsLayer.MAP_TYPE_ROADMAP|MAP_TYPE_HYBRID|MAP_TYPE_STELLITE|MAP_TYPE_TERRIAN
             */
            setMapTypeId: function (mapTypeId) {
                if (this._gmap) {
                    this._gmap.setMapTypeId(this._getGMapTypeId(mapTypeId));
                    this._mapTypeChangeHandler();
                } else {
                    this._mapOptions.mapTypeId = mapTypeId;
                }
                return;
            },
            /**
             * set map style for customized base map. The style specs are available at <a href="http://code.google.com/apis/maps/documentation/javascript/styling.html">Google Documentation</a>.
             * This class privide a few pre-defined styles: MAP_STYLE_GRAY|MAP_STYLE_NIGHT.
             * @name GoogleMapsLayer#setMapStyle
             * @function
             * @param {Object[]} styles
             */
            setMapStyles: function (styles) {
                if (this._styleOptions) {
                    // if has _styleOptions, set it null means clear it.
                    styles = styles || [];
                }
                if (styles) {
                    if (this._gmap) {
                        this._styleOptions = styles;
                        this._gmap.setOptions({
                            styles: styles
                        });
                    } else {
                        this._mapOptions.styles = styles;
                    }
                }
                return;
            },
            /**
             * Fired when Google Map Type (ROAD, SATERLLITE etc) changed
             * @name GoogleMapsLayer#onMapTypeChange
             * @param {String} mapTypeId
             * @event
             */
            setClientID: function (clientID) {
                this._clientID = clientID;
            },

            onMapTypeChange: function (mapTypeId) {
                // event
            },

            _getGMapTypeId: function (type) {
                // typically the constants is same, however, if google changes, 
                // and API is loaded dynamically but map type is specified before API load,
                // there is a slight chance they are out of sync, so fix here.
                if (google && google.maps) {
                    switch (type) {
                        case this.MAP_TYPE_ROADMAP:
                            return google.maps.MapTypeId.ROADMAP;
                        case this.MAP_TYPE_HYBRID:
                            return google.maps.MapTypeId.HYBRID;
                        case this.MAP_TYPE_SATELLITE:
                            return google.maps.MapTypeId.SATELLITE;
                        case this.MAP_TYPE_TERRAIN:
                            return google.maps.MapTypeId.TERRAIN;
                    }
                }
                return type;
            },
            _opacityChangeHandler: function (opacity) {
                // this probably should be handled in the core ESRI API using the div returned from _setMap().
                this.setOpacity(opacity);
            },

            _visibilityChangeHandler: function (v) {
                if (v) {
                    esri.show(this._gmapDiv);
                    //if (!this._svDisabled) {
                    esri.show(this._controlDiv);
                    //}
                    this.visible = true;
                    if (this._gmap) {
                        google.maps.event.trigger(this._gmap, 'resize');
                        this._panHandle = this._panHandle || dojo.connect(this._map, "onPan", this, this._panHandler);
                        this._extentChangeHandle = this._extentChangeHandle || dojo.connect(this._map, "onExtentChange", this, this._extentChangeHandler);
                        this._setExtent(this._map.extent);
                    } else {
                        this._initGMap();
                    }
                } else {
                    if (this._gmapDiv) {
                        esri.hide(this._gmapDiv);
                        esri.hide(this._controlDiv);
                        this.visible = false;
                        if (this._gmap) {
                            this._map.setExtent(this._latLngBoundsToEsriExtent(this._gmap.getBounds()));
                        }
                        if (this._streetView) {
                            this._streetView.setVisible(false);
                        }
                        if (this._panHandle) {
                            dojo.disconnect(this._panHandle);
                            this._panHandle = null;
                        }
                        if (this._extentChangeHandle) {
                            dojo.disconnect(this._extentChangeHandle);
                            this._extentChangeHandle = null;
                        }
                    }
                }
            },
            _resizeHandler: function (extent, height, width) {
                dojo.style(this._gmapDiv, {
                    width: this._map.width + "px",
                    height: this._map.height + "px"
                });
                google.maps.event.trigger(this._gmap, 'resize');
            },
            _extentChangeHandler: function (extent, delta, levelChange, lod) {
                if (levelChange) {
                    this._setExtent(extent);
                } else {
                    this._gmap.setCenter(this._esriPointToLatLng(extent.getCenter()));
                }
            },
            _panHandler: function (extent, delta) {
                //console.log('pan:'+extent.xmin+','+extent.ymin+','+extent.xmax+','+extent.ymax);
                if (this._gmap.getTilt() == 0) {
                    this._gmap.setCenter(this._esriPointToLatLng(extent.getCenter()));
                }
            },
            _mapTypeChangeHandler: function () {
                this._checkZoomLevel();
                this.onMapTypeChange(this._gmap.getMapTypeId());
            },

            _checkZoomLevel: function () {
                var id = this._gmap.getMapTypeId();
                var types = this._gmap.mapTypes;
                var maptype = null;
                for (var x in types) {
                    if (types.hasOwnProperty(x) && x == id) {
                        maptype = types[x];
                        break;
                    }
                }
                // prevent the case when switch to terrain causing misalignment because terrain only up to level 15.
                if (maptype != null) {
                    var mi = maptype.minZoom;
                    var mx = maptype.maxZoom;
                    var z = this._map.getLevel();
                    if (mx !== undefined && z > mx) {
                        this._map.setLevel(mx);
                    }
                    if (mi != undefined && z < mi) {
                        this._map.setLevel(mi);
                    }
                }
            },
            _setExtent: function (extent) {
                var ct = this._esriPointToLatLng(extent.getCenter());
                var lv = this._map.getLevel();
                /*
                 
                 if (lv >= 0) {
                 this._gmap.setZoom(lv);
                 
                 } else {
                 
                 }*/
                // esrimap.getLevel is not reliable. result differnt if first layer is Bing Map vs arcgis tile layer.
                // Google maps fit always get a smaller zoom.
                this._gmap.fitBounds(this._esriExtentToLatLngBounds(extent.expand(0.5)));
                this._gmap.setCenter(ct);
                this._checkZoomLevel();

                //console.log('gmaps fit:'+this._gmap.getZoom()+' emap:'+lv);
            },
            // move the street view control on top of map container.
            // Esri API prevents mouse event progaginate to lower divs inside map container
            // this method sort of move it up so it can be dragged. A little bit hack, 
            // but as long as stick to a certain version, should still be workable.
            _moveControls: function () {
                if (this._mvHandle) {
                    if (!this._gmap) {
                        dojo.disconnect(this._mvHandle);
                        this._mvHandle = null;
                    } else {
                        if (!this._svMoved) {
                            this._streetView = this._gmap.getStreetView();
                            if (this._streetView) {
                                var sv = dojo.query('.gmnoprint img[src*="cb_scout_sprite"]', this._gmapDiv);
                                if (sv.length > 0) {
                                    dojo.forEach(sv, function (s, idx) {
                                        dojo.place(s.parentNode.parentNode, this._controlDiv);
                                    }, this);

                                    this._svMoved = true;
                                    this._svVisibleHandle = google.maps.event.addListener(this._streetView, 'visible_changed', dojo.hitch(this, this._streetViewVisibilityChangeHandler));
                                }
                            } else {
                                this._svMoved = true;
                            }
                        }
                        if (!this._rotateMoved) {
                            var ob = dojo.query('.gmnoprint img[src*="rotate"]', this._gmapDiv);
                            if (ob.length > 0) {
                                dojo.forEach(ob, function (s, idx) {
                                    dojo.place(s.parentNode.parentNode, this._controlDiv);
                                    dojo.style(s, 'position', 'absolute');
                                    dojo.style(s, 'left', '20px');
                                }, this);
                                this._rotateMoved = true;
                            }
                        }
                        if (this._rotateMoved && this._svMoved) {
                            dojo.disconnect(this._mvHandle);
                            this._mvHandle = null;
                        }
                    }
                }
            },
            _streetViewVisibilityChangeHandler: function () {
                //console.log('_streetViewVisibilityChangeHandler');
                if (this._streetView) {
                    var vis = this._streetView.getVisible();
                    this._toggleEsriControl(vis);
                    this.onStreetViewVisibilityChange(vis);
                }

            },
            // when oblique is shown, we should disable esri mouse events because the projection changes.
            _mapTiltChangeHandler: function () {
                //console.log('_mapTiltChangeHandler');
                var t = this._gmap.getTilt();
                if (t == 45) {
                    //this._toggleEsriControl(true);
                    //this._map._isPanGMaps = this._map.isPan;
                    //this._map.disablePan();
                    dojo.place(this._gmapDiv, this._topDiv);
                    this._map.disableMapNavigation();
                } else if (t == 0) {
                    //this._toggleEsriControl(false);
                    //if (this._map._isPanGMaps) this._map.enablePan();
                    dojo.place(this._gmapDiv, this._element);
                    this._map.enableMapNavigation();
                }

            },
            _toggleEsriControl: function (turnOff) {
                if (turnOff) {
                    this._isZoomSliderDefault = this._map.isZoomSlider;
                    this._map.hideZoomSlider();
                    // gmaps (as of v3.6) still dispatch events even street view is visible. so we disable it here.
                    this._map.disableMapNavigation();

                } else {
                    if (this._isZoomSliderDefault) {
                        this._map.showZoomSlider();
                    }
                    this._map.enableMapNavigation();
                }
            },
            /**
             * Fired when Street View visibility changed.
             * @name GoogleMapsLayer#onStreetViewVisibilityChange
             * @param {boolean} visibility
             * @event
             */
            onStreetViewVisibilityChange: function (vis) {
                // attach events
            },
            _esriPointToLatLng: function (pt) {
                var ll = esri.geometry.webMercatorToGeographic(pt);
                return new google.maps.LatLng(ll.y, ll.x);
            },
            _esriExtentToLatLngBounds: function (ext) {
                var llb = esri.geometry.webMercatorToGeographic(ext);
                return new google.maps.LatLngBounds(new google.maps.LatLng(llb.ymin, llb.xmin, true), new google.maps.LatLng(llb.ymax, llb.xmax, true));
            },
            _latLngBoundsToEsriExtent: function (bounds) {
                var ext = new esri.geometry.Extent(bounds.getSouthWest().lng(), bounds.getSouthWest().lat(), bounds.getNorthEast().lng(), bounds.getNorthEast().lat());
                return esri.geometry.geographicToWebMercator(ext);
            },
            setVisibility: function (opt) {
                console.log("GoogleMapLayer setVisibility", this._streetView);
                this.setVisible(opt);
                this.visible = opt;
                //if (opt == true) {
                //    console.log(this);
                //    this.setVisible(true);
                //} else if (opt == false) {
                //    this.visible = false;
                //}
            },
            setTrafficVisible: function (opt) {
                if (opt == true) {
                    this._istraffic = true;
                    this._traffic.setMap(this._gmap);
                } else if (opt == false) {
                    this._istraffic = false;
                    this._traffic.setMap(null);
                }
            },
            setTransitVisible: function (opt) {
                if (opt == true) {
                    this._isTransportation = true;
                    this._transportation.setMap(this._gmap);
                } else if (opt == false) {
                    this._isTransportation = false;
                    this._transportation.setMap(null);
                }
            },
            setMapType: function (mapType) {
                if (mapType) {
                    switch (mapType.toLowerCase()) {
                        case this.MAP_TYPE_SATELLITE:
                            this.setMapTypeId(this.MAP_TYPE_SATELLITE);
                            break;
                        case this.MAP_TYPE_HYBRID:
                            this.setMapTypeId(this.MAP_TYPE_HYBRID);
                            break;
                        case this.MAP_TYPE_ROADMAP:
                            this.setMapTypeId(this.MAP_TYPE_ROADMAP);
                            break;
                        case this.MAP_TYPE_TERRAIN:
                            this.setMapTypeId(this.MAP_TYPE_TERRAIN);
                            break;
                        default:
                            return;
                            break;
                    }
                }
            },
            setVisible: function (visible) {

                if (visible == true) {
                    this._gmapDiv.style.display = 'block';
                } else if (visible == false) {
                    // console.log("yars");
                    this._gmapDiv.style.display = 'none';
                    // console.log(this._gmapDiv.style);
                }

            },
            setMaxScale: function (maxScale) {
                if (maxScale) {
                    this._maxScale = maxScale;

                    google.maps.event.addListenerOnce(this._gmap, "projection_changed", lang.hitch(this, function () {

                        var mapTypeId = this._gmap.mapTypeId;
                        var mapType = this._gmap.mapTypes.get(mapTypeId);
                        mapType.maxZoom = this._maxScale;

                    }));

                }

            },
            setMinScale: function (minScale) {
                if (minScale) {
                    this.minScale = minScale;

                    google.maps.event.addListenerOnce(this._gmap, "projection_changed", lang.hitch(this, function () {

                        var mapTypeId = this._gmap.mapTypeId;
                        var mapType = this._gmap.mapTypes.get(mapTypeId);
                        mapType.minZoom = this._minScale;

                    }));
                }
            },
            onLoad: function () {
                if (this.events.load && typeof (this.events.load) == "function") {
                    this.events.load();
                }
            },
            onError: function () {
                if (this.events.error && typeof (this.events.error) == "function") {
                    this.events.error();
                }
            },
        })
    });
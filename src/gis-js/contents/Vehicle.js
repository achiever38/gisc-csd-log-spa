﻿define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",

    "dojo/text!./templates/Vehicle.html",

    "dojo/dom",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/dom-attr",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/on",
    "dojox/json/query",
    "dojo/query",
    "GOTModule/controls/ListItemContainer",

    "GOTModule/config/ApplicationConfig",
    "GOTModule/utils/ResourceLib",
    "GOTModule/utils/Utility",
    "BowerModule/jquery/src/jquery"
], function (declare, _WidgetBase, _TemplatedMixin, template, dom, domConstruct, domClass, domStyle, domAttr, lang, array, on, jsonQuery, nodeQuery,
    ListItemContainer, ApplicationConfig, ResourceLib, Utility, $) {

    let ctrlHead = null;
    let ctrl = null;

    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,

        constructor: function (params) { 
            this._vehicleStatusUrl = "../gis-js/images/svg/icon_vehicle_status.svg";
        },
        postCreate: function () { },

        /* Event from ToolPanel */
        /* Flag : is First Load this widget ? */
        isFirstLoad: true,
        toggleList: [],
        /* On widget display on screen */
        onOpen: function () {

            //First Load Operation
            if (this.isFirstLoad) {
                this.isFirstLoad = false;

                ctrl = new ListItemContainer({ selectedMode: "toggle", type : "checkAll", useHighlight : false });
                domConstruct.place(ctrl.domNode, this.divList, "last");

                let lstConfig = [];
                
                ApplicationConfig.groupVehicleStatusConfig.forEach(lang.hitch(this, function (data) {
                    lstConfig.push({
                        id: data.id,
                        key: data.key,
                        text: ResourceLib.text(data.text),
                        isCheckAll: true,
                        selected: true,
                    });
                    data.items.forEach(lang.hitch(this, function (itm) {
                        var isSelected = (ApplicationConfig.currentTrackVehicleFilter.indexOf(itm.key) != -1);
                        lstConfig.push({
                            id: itm.id,
                            key: itm.key,
                            parentId: data.id,
                            text: ResourceLib.text(itm.text),
                            icon: itm.icon,
                            selected: isSelected,
                            isSelected: isSelected,
                            isCheckAll: false
                        });
                        this.toggleList.push(itm.key);
                    }));
                }));

                ctrl.createItem(lstConfig);

                on(ctrl, "ItemClick", lang.hitch(this, "_ListClickHandler"));


                // Vehicle Status
                domAttr.set(this.imgQuestion, "src", this._vehicleStatusUrl);
                $(this.vehicleStatusTitle).text(ResourceLib.text("Vehicle_Status_Title"));
                on(this.vehicleStatusTitle,"click",lang.hitch(this, function(){
                    this.onAction({ key: "open-vehicle-status-window", param: {} });
                }));
            }
            else {
                this.resetFilter();
                //if (ApplicationConfig.isResetFilter) {
                //    this.resetFilter();
                //    ApplicationConfig.isResetFilter = false;
                //}
            }

        },

        _ListClickHandler: function (obj) {
            if (obj.isSelected && !obj.isCheckAll) {
                // if(ApplicationConfig.currentTrackVehicleFilter.indexOf(obj.key) == -1){
                //     ApplicationConfig.currentTrackVehicleFilter.push(obj.key);
                // }
                ApplicationConfig.currentTrackVehicleFilter.push(obj.key);
                //this.toggleList.push(obj.key);
            }
            else {
                let indexArr = ApplicationConfig.currentTrackVehicleFilter.indexOf(obj.key);
                if(indexArr != -1){
                    ApplicationConfig.currentTrackVehicleFilter.splice(indexArr, 1);
                }
                //this.toggleList.splice(this.toggleList.indexOf(obj.key),1);
            }
            this._setSelectedAll(obj);
            //this.onAction({ key: "toggle-vehicle", param: { toggle: this.toggleList } });
            this.onAction({ key: "toggle-vehicle", param: { } });
            this.onHasActiveContent(ctrl.getCountSelected());
            
        },


        resetFilter: function () {
            if (ctrl){
                ctrl.items.forEach(function(item) {
                    if(ApplicationConfig.currentTrackVehicleFilter.indexOf(item.key) !== -1){
                        item._setSelectedAttr(true);
                    }
                    if(item.isCheckAll){
                        item._setSelectedAttr(true);
                    }
                });
            }

            // if (ctrl){
            //     for (var i = 0; i < ApplicationConfig.currentTrackVehicleFilter.length; i++) {
            //         ctrl.setSelectedIndex(ApplicationConfig.currentTrackVehicleFilter[i] - 1);
            //     }
            // }
            //ctrl.setSelectedIndex(0);
            //ctrl.setSelectedIndex(1);
            //ctrl.setSelectedIndex(2);
            //this.toggleList = [1,2,3];
        },

        /* On toolpanel close */
        onClose: function () { },

        /* Send Action to other control */
        onAction: function (obj) { /* attach event */ },

        /* Send State to ToolsPanel */
        onHasActiveContent: function (hasActive) {/* attach event */ },

        _setSelectedAll: function(obj){
            if(obj.isCheckAll){
                //checked child element
                if(obj.isSelected){
                    ctrl.items.forEach(lang.hitch(this, function (item) {
                        if(item.data.parentId == obj.id){
                            if(ApplicationConfig.currentTrackVehicleFilter.indexOf(item.data.key) == -1){
                                item._setSelectedAttr(true);
                                ApplicationConfig.currentTrackVehicleFilter.push(item.data.key);
                            }
                        }
                    }));
                }else{
                    //un check all child element
                    if(obj.isCheckAll){
                        ctrl.items.forEach(lang.hitch(this, function (item) {
                            if(item.data.parentId == obj.id){
                                item._setSelectedAttr(false);
                                let indexArr = ApplicationConfig.currentTrackVehicleFilter.indexOf(item.data.key);
                                if(indexArr != -1){
                                    ApplicationConfig.currentTrackVehicleFilter.splice(indexArr, 1);
                                }
                            }
                        }));
                    }
                }
            }else{
                //checked parent element
                var isChildChecked = new Array();
                var childCheckBox = 0;
                var parentCheckBox = null;
                ctrl.items.forEach(lang.hitch(this, function (item) {
                    if(obj.parentId == item.data.parentId){
                        if(item.selected){
                            isChildChecked.push(true);
                        }
                        childCheckBox++;
                    }
                    if(obj.parentId == item.data.id){
                        parentCheckBox = item;
                    }
                }));
                if(isChildChecked.length == childCheckBox){
                    parentCheckBox._setSelectedAttr(true);
                }else{
                    parentCheckBox._setSelectedAttr(false);
                }

            }

        }
    });
});
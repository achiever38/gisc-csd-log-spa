﻿define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",

    "dojo/text!./templates/CustomArea.html",

    "GOTModule/config/ApplicationConfig",
    "GOTModule/controls/ListItemContainer",
    "GOTModule/utils/ResourceLib",
    "GOTModule/utils/Utility",
    "GOTModule/utils/WebAPIInterface",

    "dojo/_base/lang",
    "dojo/on",
    "dojo/dom-construct",
    "BowerModule/jquery/src/jquery"
], function (declare, _WidgetBase, _TemplatedMixin, template, ApplicationConfig, ListItemContainer, ResourceLib, Utility, WebAPIInterface, lang, on, domConstruct, $) {
    let wgWebApiCaller = new WebAPIInterface();

    let AREA_GP = "open-area-gp";
    let AREA_TEXT = "open-area-text";
    var ctrl = null;
    let mainState = false;

    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,
        constructor: function (params) { },
        postCreate: function () { },
        
        /* Event from ToolPanel */
        /* Flag : is First Load this widget ? */
        isFirstLoad: true,
        /* On widget display on screen */
        onOpen: function () {
            //First Load Operation
            if (this.isFirstLoad) {
                this.isFirstLoad = false;

                ctrl = new ListItemContainer({ selectedMode: "toggle" ,type : "checked", useHighlight : false });
                domConstruct.place(ctrl.domNode, this.divItemList, "last");

                var lstItem = [
                    {
                        key: AREA_GP,
                        text: ResourceLib.text('wg-custom-area-show-gp'),
                        blankIcon: true,
                        selected: false
                    },
                    {
                        key: AREA_TEXT,
                        text: ResourceLib.text('wg-custom-area-show-text'),
                        blankIcon: true,
                        selected: false
                    }
                ];

                ctrl.createItem(lstItem);
                on(ctrl, "ItemClick", lang.hitch(this, "_ListClickHandler"));
            }

            if(ApplicationConfig.CustomAreaMainState == undefined) ApplicationConfig.CustomAreaMainState = false;
            if(ApplicationConfig.CustomAreaTextState == undefined) ApplicationConfig.CustomAreaTextState = false;

            ctrl.setSelectedIndex(0, ApplicationConfig.CustomAreaMainState);
            ctrl.setSelectedIndex(1, ApplicationConfig.CustomAreaTextState);
        },
        /* On toolpanel close */
        onClose: function () {},

        /* Send Action to other control */
        onAction: function (obj) { /* attach event */ },

        _ListClickHandler: function(obj) {

            switch(obj["key"]) 
            {
                case AREA_GP: {
                    mainState = obj["isSelected"];

                    if(obj["isSelected"]) {
                        ctrl.setSelectedIndex(0, true);
                        ctrl.setSelectedIndex(1, true);

                        //open poi
                        //open text
                        var def = ApplicationConfig.mapManager.customArea();
                        ApplicationConfig.mapManager.setCustomAreaText(true);

                        ApplicationConfig.CustomAreaMainState = true
                        ApplicationConfig.CustomAreaTextState = true;

                        this.onHasActiveContent(true);

                        def.done(lang.hitch(this, function(){
                            this.onContentLoading(false);
                        })).fail(lang.hitch(this, function(e){
                            this.onContentLoading(false);
                        }));
                    }
                    else {
                        ctrl.setSelectedIndex(0, false);
                        ctrl.setSelectedIndex(1, false);

                        //hide poi
                        //hide text
                        ApplicationConfig.mapManager.disableCustomArea();
                        ApplicationConfig.mapManager.setCustomAreaText(false);

                        ApplicationConfig.CustomAreaMainState = false
                        ApplicationConfig.CustomAreaTextState = false;

                        this.onHasActiveContent(false);
                    }
                    break;
                }

                case AREA_TEXT: {
                    if(mainState) {
                        ApplicationConfig.mapManager.setCustomAreaText(obj["isSelected"]);
                        ApplicationConfig.CustomAreaTextState = obj["isSelected"];
                    }
                    else
                        ctrl.setSelectedIndex(1, false);
                    break;
                }
            }

            //console.log(obj);
        },

        /* Send State to ToolsPanel */
        onHasActiveContent: function (hasActive) {/* attach event */ },

        /* Show Loadinng Panel */
        onContentLoading: function (isLoad) { }
    });
});
﻿define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",

    "dojo/text!./templates/LocationInfo.html",

    "dojo/dom",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/dom-attr",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/on",
    "dojox/json/query",

    "GOTModule/config/ApplicationConfig",
    "GOTModule/utils/ResourceLib",
    "GOTModule/utils/Utility",
    "GOTModule/utils/WebAPIInterface"
], function (declare, _WidgetBase, _TemplatedMixin, template, dom, domConstruct, domClass, domStyle, domAttr, lang, array, on, jsonQuery,
    ApplicationConfig, ResourceLib, Utility, WebAPIInterface) {

    let wgWebApiCaller = new WebAPIInterface();

    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,
        data: null,
        //localLang: ResourceLib.text("wg-location-info"),
        localLang: ResourceLib.currentLanguage.toUpperCase(),

        constructor: function (params) { },
        postCreate: function () {
            require(["dojo/text!./gis-js/images/svg/icon_locationinfo_favorite.svg"], lang.hitch(this, function (svgTxt) {
                domConstruct.place(domConstruct.toDom(svgTxt), this.divLocationFavorite, "last");
            }));

            on(this.divLocationFavorite, "click", lang.hitch(this, "_btnFavClick"));

        },

        _btnFavClick: function () {

            if (domClass.contains(this.divLocationFavorite, "added")) {
                return;
            }
            else {
                domClass.add(this.divLocationFavorite, "added");
            }

            let addrEN = this.data.adminLevel4English + " " + this.data.adminLevel3English + " " + this.data.adminLevel2English + " " + this.data.adminLevel1English + " " + this.data.postCode;
            let addrLC = this.data.adminLevel4Local + " " + this.data.adminLevel3Local + " " + this.data.adminLevel2Local + " " + this.data.adminLevel1Local + " " + this.data.postCode;;
            let param = {
                key: Date.now(),
                nameEnglish: this.data.nameEnglish,
                nameLocal: this.data.nameLocal,
                address_Local: addrLC,
                address_English: addrEN,
                lat: this.data.LatLon.lat,
                lon: this.data.LatLon.lon,
                zoom: ApplicationConfig.mapObject.getZoom()
            };

            wgWebApiCaller.requestGET({
                url: ApplicationConfig.webAPIConfig.userPreference,
                data: { keys: ApplicationConfig.webAPIConfig.userPreferenceBookmarkKey },
                success: lang.hitch(this, function (data) {

                    let lstFavData = JSON.parse(data[0].value) || [];
                    lstFavData.push(param);

                    let paramPOST = {
                        key: ApplicationConfig.webAPIConfig.userPreferenceBookmarkKey,
                        value: JSON.stringify(lstFavData)
                    }

                    wgWebApiCaller.requestPOST({
                        url: ApplicationConfig.webAPIConfig.userPreference,
                        data: Utility.postify([paramPOST]),
                        success: lang.hitch(this, function (postData) {
                            //console.log(postData);
                        })
                    });

                }),
                fail: lang.hitch(this, function (e) { console.log("Error : Cannot get favorite", e) })
            });
        },

        /* Event from ToolPanel */
        /* Flag : is First Load this widget ? */
        isFirstLoad: true,
        /* On widget display on screen */
        onOpen: function (param) {
            //First Load Operation
            if (this.isFirstLoad) {
                this.isFirstLoad = false;
            }

            if (!param) { return;}

            this.data = param;

            domClass.remove(this.divLocationFavorite, "added");

            Utility.debugLog("LocationInfo OnOpen ", param);
            if (param.adminLevel1English) // if return result from identify
            {
                this.divLocationFavorite.style.display = "block";
                var loadEn = param.adminLevel4English;
                var loadLocal = param.adminLevel4Local;
                //console.log("Local", loadLocal);
                if ((loadEn.length > 0) && (loadLocal.length > 0)) {
                    loadEn += ", "
                    loadLocal += ", "
                }
                if (this.localLang.toUpperCase().indexOf("EN") >= 0) {
                    this.divLocation.innerHTML = param.nameEnglish;
                    this.divLocationInfo.innerHTML = loadEn + param.adminLevel3English + ", " + param.adminLevel2English + ", " + param.adminLevel1English + ", " + param.postCode + "<br/>" + param.LatLon.lat.toFixed(4) + "," + param.LatLon.lon.toFixed(4);
                } else {
                    this.divLocation.innerHTML = param.nameLocal;
                    this.divLocationInfo.innerHTML = loadLocal + param.adminLevel3Local + ", " + param.adminLevel2Local + ", " + param.adminLevel1Local + ", " + param.postCode + "<br/>" + param.LatLon.lat.toFixed(4) + "," + param.LatLon.lon.toFixed(4);
                }
            } else if (param.latitude && param.longitude) {
                //case result from custompoi
                this.divLocationFavorite.style.display = "none";
                this.divLocation.innerHTML = param.name;
                this.divLocationInfo.innerHTML = param.townName + ", " + param.cityName + ", " + param.provinceName + "<br/>" + param.latitude.toFixed(4) + "," + param.longitude.toFixed(4);
            } else {
                this.divLocationFavorite.style.display = "none";
                this.divLocation.innerHTML = "";
                this.divLocationInfo.innerHTML = "";
            }
        },

        /* On toolpanel close */
        onClose: function () {
            domClass.remove(this.divLocationFavorite, "added");
            var lyr = ApplicationConfig.mapManager.getLayer("lyr-location-info");
            lyr.clear();
        },

        /* Send Action to other control */
        onAction: function (obj) { /* attach event */ }
    });
});
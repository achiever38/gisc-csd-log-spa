﻿define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",

    "dijit/form/Select",

    "dojo/text!./templates/PlaybackMultiple.html",

    "dojo/dom",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/dom-attr",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/_base/event",
    "dojo/on",
    "dojox/json/query",
    "dojo/query",

    "esri/geometry/Point",
    "esri/geometry/Polyline",
    "esri/geometry/Multipoint",
    "esri/geometry/Extent",
    "esri/symbols/PictureMarkerSymbol",
    "esri/symbols/SimpleFillSymbol",
    "esri/symbols/SimpleLineSymbol",
    "esri/symbols/TextSymbol",
    "esri/Color",
    "esri/SpatialReference",
    "esri/graphic",
    "esri/symbols/Font",
    "esri/geometry/geometryEngine",
    "esri/geometry/webMercatorUtils",

    "GOTModule/config/ApplicationConfig",
    "GOTModule/utils/ResourceLib",
    "GOTModule/utils/Utility",
    "GOTModule/utils/FullyPlaybackTracking",

    "BowerModule/jquery/src/jquery",
    "BowerModule/jRange/jquery.range"
], function (declare, _WidgetBase, _TemplatedMixin, Select, template, dom, domConstruct, domClass, domStyle, domAttr, lang, array, dojoEvent, on, jsonQuery, nodeQuery,
    Point, PolyLine, Multipoint, Extent,
    PictureMarkerSymbol, SimpleFillSymbol, SimpleLineSymbol, TextSymbol, Color, SpatialReference, Graphic, Font, geometryEngine, webMercatorUtils,
    ApplicationConfig, ResourceLib, Utility, VehicleTracking, $) {

    Utility.loadWidgetCSS(require.toUrl("./gis-js/libs/jRange/jquery.range.css"));

    let map = ApplicationConfig.mapObject;
    let mapManager = ApplicationConfig.mapManager;
    //let maxLevel = ApplicationConfig.animationConfig.maxPlaybackZoomLevel;

    var lyrDraw = mapManager.getLayer(ApplicationConfig.commandOptions["draw-one-point-zoom"].layer);
    var lyrRoute = mapManager.getLayer(ApplicationConfig.commandOptions["playback-command"].layer.route);
    var lyrStop = mapManager.getLayer(ApplicationConfig.commandOptions["playback-command"].layer.stop);
    var lyrAnimate = mapManager.getLayer(ApplicationConfig.commandOptions["playback-command"].layer.animate);
    var lyrAlert = mapManager.getLayer(ApplicationConfig.commandOptions["playback-command"].layer.alert);
    var lyrAlertChart = mapManager.getLayer(ApplicationConfig.commandOptions["playback-command"].layer.alertChart);

    var lyrStopSetting = jsonQuery("[?id='" + ApplicationConfig.commandOptions["playback-command"].layer.stop + "']", ApplicationConfig.defaultGraphicLayerList)[0];
    var lyrAlertSetting = jsonQuery("[?id='" + ApplicationConfig.commandOptions["playback-command"].layer.alert + "']", ApplicationConfig.defaultGraphicLayerList)[0];

    var wgVehicleTracking = new VehicleTracking();
    var lstColorMapping = null;

    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,

        playbackData: null,
        currentIndex: null,
        maxLength: null,
        animateHandler: null,
        counter: 0,
        fps: ApplicationConfig.playbackFullySetting.fps,

        isPause: false,
        currentSpeedFactor: 0,

        constructor: function (params) {},
        postCreate: function () {
            on(wgVehicleTracking, "TrackLocationChange", lang.hitch(this, "_onTrackLocationChange"));
            on(this.btnPlay, "click", lang.hitch(this, "_btnPlayClick"));
            on(this.btnPause, "click", lang.hitch(this, "_btnPauseClick"));

            on(this.btnStop, "click", lang.hitch(this, "_btnStopClick"));
            on(this.ddSpreed, "change", lang.hitch(this, "_ddChangeSpreed"));


            require([
                "dojo/text!./gis-js/images/svg/btn_playback_play.svg",
                "dojo/text!./gis-js/images/svg/btn_playback_pause.svg",
                "dojo/text!./gis-js/images/svg/btn_stop_over.svg"
            ], lang.hitch(this, function (svgPlay, svgPause,svgStop) {
                domConstruct.place(domConstruct.toDom(svgPlay), this.btnPlay, "last");
                domConstruct.place(domConstruct.toDom(svgPause), this.btnPause, "last");
                domConstruct.place(domConstruct.toDom(svgStop), this.btnStop, "last");
            }));
        },

        /* Event from ToolPanel */
        /* Flag : is First Load this widget ? */
        isFirstLoad: true,
        /* On widget display on screen */
        onOpen: function (param) {

            let mode = param.mode || "ALL";
            var zoomLevel = map.getZoom();

            lstColorMapping = {};
            lyrRoute.clear();
            lyrStop.clear();
            lyrAnimate.clear();
            lyrAlert.clear();
            lyrAlertChart.clear();
            lyrDraw.clear();

            cancelAnimationFrame(this.animateHandler);
            this._onAnimateEnd();

            this.playbackData = param.playback;
            this.currentIndex = 0;
            this.isPause = null;
            this.currentSpeedFactor = 1;
            this.counter = 0;
            let speed1 = dojo.byId("ix1");
            speed1.innerHTML = ResourceLib.text("Playback_control_Speedx1")

            let speed2 = dojo.byId("ix2");
            speed2.innerHTML = ResourceLib.text("Playback_control_Speedx2")

            let speed4 = dojo.byId("ix4");
            speed4.innerHTML = ResourceLib.text("Playback_control_Speedx4")
            //domConstruct.place('<option value="2x" id="ix2">'+ResourceLib.text("Common_Location")+'</option>',ix2);

            // console.log("ResourceLib", ResourceLib);
            // this.ddSpreed = ResourceLib.text("Common_Location");

            this.maxLength = param.playback.length;

            wgVehicleTracking.clearAllTrackable();

            if (this.isFirstLoad) {
                this.isFirstLoad = false;

                $(this.inpSlider).jRange({
                    from: 0,
                    to: this.maxLength - 1,
                    width: 150,
                    step: 1,
                    format: '%s',
                    showLabels: false,
                    showScale: false,
                    snap: true,
                    onstatechange: lang.hitch(this, function (data) {}),

                    ondragstart: lang.hitch(this, function () {
                        //pause
                        this._btnPauseClick();
                    }),

                    ondragend: lang.hitch(this, function (data) {
                        //clear layer and unpause
                        this.stepAnimate(data);
                    }),

                    onbarclicked: lang.hitch(this, function (data) {
                        //clear layer nad unpause
                        this._btnPauseClick();
                        this.stepAnimate(data);
                    })

                });

                $(this.inpSlider).jRange("setValue", '0');
            } else {
                $(this.inpSlider).jRange("setValue", '0');
            }

            domConstruct.empty(this.divListStat);

            //get route from data
            let lstID = Utility.getUniqueItem(jsonQuery("[=id]", param.playback));

            let indColor = 0;

            lstID.map(lang.hitch(this, function (val) {
                lstColorMapping[val] = ApplicationConfig.playbackFullySetting.color[indColor];
                indColor++;
            }));

            var mpPoint = null;

            if (lstID.length > 0) {
                mpPoint = new Multipoint();
            }

            array.forEach(lstID, lang.hitch(this, function (id, ind) {
                let lstItem = jsonQuery("[?id='" + id + "'][=[lon,lat]]", param.playback);
                let lstData = jsonQuery("[?id='" + id + "']", param.playback);

                //draw route
                let route = new PolyLine(lstItem);
                let sym = new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color(ApplicationConfig.playbackFullySetting.color[ind]), 6);

                let gp = new Graphic(route, sym);
                lyrRoute.add(gp);

                //draw point
                array.forEach(lstData, lang.hitch(this, function (itm, index) {
                    let pt = new Point([itm.lon, itm.lat]);
                    let symb = null;

                    mpPoint.addPoint(pt);

                    if (mode == "ALL") {

                        if (index != 0 && index != lstData.length - 1) {

                            switch (itm.movement) {

                                case "PARKENGINEON":
                                    {


                                        let width = ApplicationConfig.playbackFullySetting.footprintEngineOnPin.width;
                                        let height = ApplicationConfig.playbackFullySetting.footprintEngineOnPin.height;

                                        if (lyrStopSetting.enableResize) {
                                            itm.attributes[ApplicationConfig.constantKeys["defaultIconHeight"]] = height;
                                            itm.attributes[ApplicationConfig.constantKeys["defaultIconWidth"]] = width;

                                            width = Utility.calculateSizeForZoomFactor(width, zoomLevel);
                                            height = Utility.calculateSizeForZoomFactor(height, zoomLevel);
                                        }

                                        symb = new PictureMarkerSymbol(
                                            ApplicationConfig.playbackFullySetting.footprintEngineOnPin.url,
                                            width,
                                            height);
                                        break;
                                    }

                                case "PARK":
                                    {
                                        // console.log("PARK  JAAAAAAAAAAAA")
                                        let width = ApplicationConfig.playbackFullySetting.footprintParkPin.width;
                                        let height = ApplicationConfig.playbackFullySetting.footprintParkPin.height;

                                        if (lyrStopSetting.enableResize) {
                                            itm.attributes[ApplicationConfig.constantKeys["defaultIconHeight"]] = height;
                                            itm.attributes[ApplicationConfig.constantKeys["defaultIconWidth"]] = width;

                                            width = Utility.calculateSizeForZoomFactor(width, zoomLevel);
                                            height = Utility.calculateSizeForZoomFactor(height, zoomLevel);
                                        }

                                        symb = new PictureMarkerSymbol(
                                            ApplicationConfig.playbackFullySetting.footprintParkPin.url,
                                            width,
                                            height);
                                        break;
                                    }
                                case "STOP":
                                    {
                                        // console.log("MOVE  JAAAAAAAAAAAA")
                                        let width = ApplicationConfig.playbackFullySetting.footprintStopPin.width;
                                        let height = ApplicationConfig.playbackFullySetting.footprintStopPin.height;

                                        if (lyrStopSetting.enableResize) {
                                            itm.attributes[ApplicationConfig.constantKeys["defaultIconHeight"]] = height;
                                            itm.attributes[ApplicationConfig.constantKeys["defaultIconWidth"]] = width;

                                            width = Utility.calculateSizeForZoomFactor(width, zoomLevel);
                                            height = Utility.calculateSizeForZoomFactor(height, zoomLevel);
                                        }

                                        symb = new PictureMarkerSymbol(
                                            ApplicationConfig.playbackFullySetting.footprintStopPin.url,
                                            width,
                                            height);
                                        break;
                                    }
                                default:
                                    {

                                        let width = ApplicationConfig.playbackFullySetting.footprintPin.width;
                                        let height = ApplicationConfig.playbackFullySetting.footprintPin.height;

                                        if (lyrStopSetting.enableResize) {
                                            itm.attributes[ApplicationConfig.constantKeys["defaultIconHeight"]] = height;
                                            itm.attributes[ApplicationConfig.constantKeys["defaultIconWidth"]] = width;

                                            width = Utility.calculateSizeForZoomFactor(width, zoomLevel);
                                            height = Utility.calculateSizeForZoomFactor(height, zoomLevel);
                                        }

                                        symb = new PictureMarkerSymbol(
                                            ApplicationConfig.playbackFullySetting.footprintPin.url,
                                            width,
                                            height);
                                        break;
                                    }
                            }

                            symb.setAngle(itm.rotation || 0);

                            lyrStop.add(new Graphic(pt, symb, itm.attributes));
                        } else {}
                    }
                }));

                // var symbPoint = new PictureMarkerSymbol(
                //     ApplicationConfig.playbackFullySetting.destinationPin.url,
                //     ApplicationConfig.playbackFullySetting.destinationPin.width,
                //     ApplicationConfig.playbackFullySetting.destinationPin.height);

                let startWidth = ApplicationConfig.playbackFullySetting.pinBegin.width;
                let startHeight = ApplicationConfig.playbackFullySetting.pinBegin.height;
                let endWidth = ApplicationConfig.playbackFullySetting.pinEnd.width;
                let endHeight = ApplicationConfig.playbackFullySetting.pinEnd.height;

                if (lyrStopSetting.enableResize) {
                    lstData[0].attributes[ApplicationConfig.constantKeys["defaultIconWidth"]] = startWidth;
                    lstData[0].attributes[ApplicationConfig.constantKeys["defaultIconHeight"]] = startHeight;
                    lstData[lstData.length - 1].attributes[ApplicationConfig.constantKeys["defaultIconWidth"]] = endWidth;
                    lstData[lstData.length - 1].attributes[ApplicationConfig.constantKeys["defaultIconHeight"]] = endHeight;

                    startWidth = Utility.calculateSizeForZoomFactor(startWidth, zoomLevel);
                    startHeight = Utility.calculateSizeForZoomFactor(startHeight, zoomLevel);
                    endWidth = Utility.calculateSizeForZoomFactor(endWidth, zoomLevel);
                    endHeight = Utility.calculateSizeForZoomFactor(endHeight, zoomLevel);
                }


                var symbBegin = new PictureMarkerSymbol(
                    ApplicationConfig.playbackFootprintSetting.pinBegin.url,
                    startWidth,
                    startHeight);

                var symbEnd = new PictureMarkerSymbol(
                    ApplicationConfig.playbackFootprintSetting.pinEnd.url,
                    endWidth,
                    endHeight);

                //symbPoint.setOffset(ApplicationConfig.playbackFullySetting.destinationPin.offsetX, ApplicationConfig.playbackFullySetting.destinationPin.offsetY);

                var pointStart = new Point(lstData[0].lon, lstData[0].lat);
                var pointEnd = new Point(lstData[lstData.length - 1].lon, lstData[lstData.length - 1].lat);

                lyrStop.add(new Graphic(pointStart, symbBegin, lstData[0].attributes));
                lyrStop.add(new Graphic(pointEnd, symbEnd, lstData[lstData.length - 1].attributes));

                //clone data and set empty status
                let data = JSON.parse(JSON.stringify(lstData[0].attributes));
                data["STATUS"] = [];

                //this.drawAttributes(data, ApplicationConfig.playbackFullySetting.color[ind]);
                this.drawAttributes(data, lstColorMapping[id]);
            }));

            array.forEach(param.alert, lang.hitch(this, function (alertItem, ind) {

                let alertWidth = 32;
                let alertHeight = 32;

                if (lyrAlertSetting.enableResize) {
                    alertItem.attributes[ApplicationConfig.constantKeys["defaultIconHeight"]] = alertWidth;
                    alertItem.attributes[ApplicationConfig.constantKeys["defaultIconWidth"]] = alertHeight;

                    alertWidth = Utility.calculateSizeForZoomFactor(alertWidth, zoomLevel);
                    alertHeight = Utility.calculateSizeForZoomFactor(alertHeight, zoomLevel);
                }

                var pointAlert = new Point(alertItem.longitude, alertItem.latitude);
                var symbAlert = new PictureMarkerSymbol(alertItem.attributes.imageUrl, alertWidth, alertHeight);

                lyrAlert.add(new Graphic(pointAlert, symbAlert, alertItem.attributes));
            }));

            //เพิ่มการ Zoom ไปยังจุดแรก
            //var ptZoom = new Point(param.playback[0].lon, param.playback[0].lat);
            //map.centerAndZoom(ptZoom, 17);

            //Update Min 20180314 - Zoom Extent ทั้งหมดแทน

            if (mpPoint != null) {
                map.setExtent(mpPoint.getExtent().expand(1.5));
            }
            //console.log("fully param", param);
        },

        _btnPlayClick: function () {

            lyrDraw.clear();

            domClass.add(this.btnPlay, "hide");
            domClass.remove(this.btnPause, "hide");
            domClass.remove(this.btnStop, "hide");

            if (this.isPause == null) {
                lyrAnimate.clear();
                wgVehicleTracking.clearAllTrackable();

                $(this.inpSlider).jRange('setValue', '0');
                this.drawVehicle(this.playbackData[0]);
            }

            this.onAction({
                key: "close-playback-widget",
                param: {}
            });
            this.isPause = false;
            this.stepAnimate(this.currentIndex);
            this.processAnimation();
        },

        _btnPauseClick: function () {

            lyrDraw.clear();

            domClass.remove(this.btnPlay, "hide");
            domClass.add(this.btnPause, "hide");
            cancelAnimationFrame(this.animateHandler);

            this.isPause = true;
        },

        _btnStopClick: function () {
            //clear ค่าต่างๆให้เป็นค่าเริ่มต้น
            lyrDraw.clear();
            domClass.add(this.btnStop, "hide");

            domClass.remove(this.btnPlay, "hide");
            domClass.add(this.btnPause, "hide");
            lyrAnimate.clear();
            wgVehicleTracking.clearAllTrackable();
            cancelAnimationFrame(null);
            this.currentIndex = 0
            $(this.inpSlider).jRange("setValue", '0');
            this.drawVehicle(this.playbackData[0]);

            this.isPause = true;

        },
        _ddChangeSpreed: function (res) {
            // set ค่าความเร็วใหม่
            this.fps = res.target.value;
            switch (res.target.value) {
                case "1x":
                this.fps = ApplicationConfig.playbackFullySetting.stepFps[2];
                //ตำแหน่งใน stepSpeed
                    this.currentSpeedFactor = 1;
                    break;
                case "2x":
                    this.fps = ApplicationConfig.playbackFullySetting.stepFps[1];
                    //ตำแหน่งใน stepSpeed
                    this.currentSpeedFactor = 1;
                    break;
                case "4x":
                    this.fps = ApplicationConfig.playbackFullySetting.stepFps[0];
                    //ตำแหน่งใน stepSpeed
                    this.currentSpeedFactor = 0;
                    break
                default:
                    break;
            }
            if(this.isPause == null) return
            if (!this.isPause) {
                //set ระยะที่รถจะขยับในจุดต่อไป
                this.stepAnimate(this.currentIndex);
                // สั่งให้เล่น
                this.processAnimation();
            }
        },

        _onAnimateEnd: function () {
            domClass.remove(this.btnPlay, "hide");
            domClass.add(this.btnPause, "hide");
            domClass.add(this.btnStop, "hide");

            this.isPause = null;
            this.currentIndex = 0;
            this.counter = 0;
        },

        _onTrackLocationChange: function (lstDest) {
            let mulPt = new Multipoint(new SpatialReference({
                wkid: 4326
            }));

            array.forEach(lstDest, lang.hitch(this, function (itm, ind) {
                let lat, lon;
                if (itm.nextLocation) {
                    mulPt.addPoint(new Point(itm.nextLocation.lon, itm.nextLocation.lat));
                }

                if (itm.startLocation) {
                    mulPt.addPoint(new Point(itm.startLocation.lon, itm.startLocation.lat));
                }

                //this.drawAttributes(itm.graphic.attributes, ApplicationConfig.playbackFullySetting.color[ind]);
                this.drawAttributes(itm.graphic.attributes, lstColorMapping[itm.graphic.attributes["ID"]]);
            }));

            var objExtent = mulPt.getExtent();

            if (mulPt.points.length > 2) {
                // multiple vehicle
                if (!map.extent.contains(objExtent)) {

                    map.setExtent(objExtent.expand(1.5));
                } else { /* use same extent */ }
            } else {
                //single vehicle
                // 1.แปลง extent ของ map เป็น 4326 ( lalt, lon )
                var geoMapExt = webMercatorUtils.webMercatorToGeographic(map.extent);

                // 1.1 หา diff ของ xmax - xmin และ ymax - ymin
                var currMapWidth = geoMapExt.getWidth(); //เวลา + - ให้หาร 2 ก่อนเพราะจาก fn จะได้ระยะจาก xmax - xmin เลย
                var currMapHeight = geoMapExt.getHeight(); //เวลา + - ให้หาร 2 ก่อนเพราะจาก fn จะได้ระยะจาก xmax - xmin เลย

                //2.สร้าง extent ใหม่ จาก evt[1] โดยให้ evt[1] เป็น center
                var newXMin = mulPt.points[0][0] - (currMapWidth / 2);
                var newXMax = mulPt.points[0][0] + (currMapWidth / 2);
                var newYMin = mulPt.points[0][1] - (currMapHeight / 2);
                var newYMax = mulPt.points[0][1] + (currMapHeight / 2);
                var nextExt = new Extent({
                    xmin: newXMin,
                    ymin: newYMin,
                    xmax: newXMax,
                    ymax: newYMax,
                    spatialReference: {
                        wkid: 4326
                    }
                });

                if (!map.extent.contains(objExtent)) {

                    if (nextExt.contains(objExtent)) {
                        map.centerAt(nextExt.getCenter());
                    } else {
                        // 3.1 ถ้าไม่ เปลี่ยน extent เป็นของ ext เหมือนเดิม
                        map.setExtent(objExtent.expand(3));
                    }
                } else { /* use same extent */ }
            }
        },

        stepAnimate: function (index) {

            cancelAnimationFrame(this.animateHandler);

            this.counter = (ApplicationConfig.playbackFullySetting.stepSpeed[this.currentSpeedFactor] * this.fps) - 1;
            this.currentIndex = Number(index);

            lyrAnimate.clear();
            wgVehicleTracking.clearAllTrackable();

            domConstruct.empty(this.divListStat);

            //get route from data
            let lstID = Utility.getUniqueItem(jsonQuery("[=id]", this.playbackData));

            array.forEach(this.playbackData, lang.hitch(this, function (itm) {
                let data = JSON.parse(JSON.stringify(itm.attributes));
                data["STATUS"] = [];

                //this.drawAttributes(data, ApplicationConfig.playbackFullySetting.color[ind]);
                this.drawAttributes(data, lstColorMapping[itm.attributes["ID"]]);
            }));


            this.drawVehicle(this.playbackData[this.currentIndex]);

            if (this.currentIndex == this.maxLength - 1) {
                this._onAnimateEnd();
            }
        },

        processAnimation: function () {
            //isPause
            if (!this.isPause) {
                this.counter += 1;

                if (Math.floor(this.counter / this.fps) >= ApplicationConfig.playbackFullySetting.stepSpeed[this.currentSpeedFactor]) {
                    this.counter = 0;
                    this.currentIndex += 1;

                    //เปลี่ยนตัวเล่นที่เมนูขวา
                    $(this.inpSlider).jRange('setValue', '' + this.currentIndex);

                    //ข้อมูลของจุดถัดไป
                    let item = this.playbackData[this.currentIndex];
                    if ((this.currentIndex < this.maxLength))
                        this.drawVehicle(item);
                }


            }

            if (this.currentIndex < this.maxLength) {
                this.animateHandler = requestAnimationFrame(lang.hitch(this, this.processAnimation));
            } else {
                cancelAnimationFrame(this.animateHandler);
                this._onAnimateEnd();
            }
        },

        drawVehicle: function (item) {
            // console.log("DRAW ===> ",item);

            let isAdded = wgVehicleTracking.checkAlreadyAddItem(item.attributes.ID);

            //this.drawTime(item.attributes.DATE);
            this.drawTime(item.attributes["TIME"]);

            if (!isAdded) {
                var symbol = new PictureMarkerSymbol(item.pic["N"], 32, 32);
                var geo = new Point([item.lon, item.lat]);
                var gp = new Graphic(geo, symbol, item.attributes);
                var textSymb = new TextSymbol();
                textSymb.setText(item.attributes.TITLE);
                textSymb.setOffset(0, -27);
                textSymb.setFont(new Font("14pt", Font.STYLE_NORMAL, Font.VARIANT_NORMAL, Font.WEIGHT_NORMAL, "DB-Regular"));
                textSymb.setHaloColor(new Color(ApplicationConfig.haloColor));
                textSymb.setHaloSize(ApplicationConfig.haloSize);

                var textGp = new Graphic(geo, textSymb, item.attributes);

                lyrAnimate.add(gp);
                lyrAnimate.add(textGp);

                wgVehicleTracking.addTrackableItem({
                    id: item.attributes.ID,
                    directionPic: item.pic,
                    location: {
                        lon: item.lon,
                        lat: item.lat
                    },
                    graphic: gp,
                    textGraphic: textGp,
                    track: true,
                    animateTime: ApplicationConfig.playbackFullySetting.stepSpeed[this.currentSpeedFactor],
                    fps: this.fps
                });
            } else {
                wgVehicleTracking.recieveData({
                    id: item.attributes.ID,
                    location: {
                        lon: item.lon,
                        lat: item.lat
                    },
                    directionPic: item.pic,
                    attributes: item.attributes,
                    track: true,
                    animateTime: ApplicationConfig.playbackFullySetting.stepSpeed[this.currentSpeedFactor],
                    fps: this.fps
                });
            }
        },

        drawAttributes: function (attr, color) {

            //create attributes div

            let divAttrContainer = nodeQuery("[data-id='" + attr.ID + "']", this.divListStat)[0];

            if (!divAttrContainer) {
                divAttrContainer = domConstruct.create("div", {
                    "data-id": attr.ID,
                    class: "div-stat-item"
                }, this.divListStat, "last");
            } else {
                domConstruct.empty(divAttrContainer);
            }

            let divAssetHeader = domConstruct.create("div", {}, divAttrContainer, "last");
            let divColor = domConstruct.create("div", {
                class: "div-asset-pb-color"
            }, divAssetHeader);
            let divAssetID = domConstruct.create("div", {
                class: "div-attr-asset-id",
                innerHTML: attr["TITLE"] + " (" + attr["BOX_ID"] + ")"
            }, divAssetHeader);

            domStyle.set(divColor, {
                "backgroundColor": color
            });

            let divAttrList = domConstruct.create("div", {
                class: "div-lst-attr"
            }, divAttrContainer);
            let divDoubleContainer = null;

            //console.log("dasdasdas");

            //console.log("attr", attr);

            if (attr["STATUS"].length > 0) {
                let divLocation = domConstruct.create("div", {
                    class: "div-status-blog"
                }, divAttrList, "last");
                let divLocationContainer = domConstruct.create("div", {
                    class: "div-location-container"
                }, divLocation, "last");
                let divLocationSVG = domConstruct.create("div", {
                    class: "div-svg-icon-status"
                }, divLocationContainer, "last");
                require([
                    "dojo/text!./gis-js/images/svg/icon_location_playback_info.svg"
                ], lang.hitch(this, function (svgRaw) {
                    let svgIcon = domConstruct.toDom(svgRaw);
                    domConstruct.place(svgIcon, divLocationSVG, "last");
                }));


                let divLocationText = domConstruct.create("div", {
                    class: "div-location-text",
                    innerHTML: attr.LOCATION
                }, divLocationContainer, "last");
            }


            array.forEach(attr["STATUS"], lang.hitch(this, function (data, i) {
                if (i % 2 == 0) {
                    divBlock = domConstruct.create("div", {
                        class: "div-status-blog"
                    }, divAttrList, "last");
                }

                let divContainer = domConstruct.create("div", {
                    class: "div-icon-container",
                    title: data.title || ""
                }, divBlock, "last");

                let divIcon = domConstruct.create("div", {
                    class: "div-svg-icon-status " + data.cssClass
                }, divContainer, "last");
                let svg = domConstruct.toDom(data.icon);

                let divText = domConstruct.create("div", {
                    class: "div-svg-icon-status-text",
                    innerHTML: data.text || "",
                    title: data.text || ""
                }, divContainer, "last");

                domConstruct.place(svg, divIcon, "last");
            }));
        },

        drawTime: function (txt) {
            this.divTimer.innerHTML = txt;
        },

        /* On toolpanel close */
        onClose: function () {
            mapManager.clearLayers([ApplicationConfig.commandOptions["playback-command"].layer.animate]);
            lyrRoute.clear();
            lyrStop.clear();
            lyrAnimate.clear();
            lyrAlert.clear();
            lyrAlertChart.clear();
            lyrDraw.clear();

            cancelAnimationFrame(this.animateHandler);
            this._onAnimateEnd();

            //this.onAction({ key: "show-all-trackvehicle", param: {} });

            //wgVehicleTracking.clearAllTrackable();
        },

        /* Send Action to other control */
        onAction: function (obj) { /* attach event */ }
    });
});
﻿define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",

    "dojo/text!./templates/PrintMap.html",

    "dojo/dom",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/dom-attr",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/on",
    "dojox/json/query",

    "GOTModule/config/ApplicationConfig",
    "GOTModule/utils/ResourceLib",
    "GOTModule/utils/Utility",
    "esri/tasks/PrintTask",
    "esri/tasks/PrintParameters"
], function (declare, _WidgetBase, _TemplatedMixin, template, dom, domConstruct, domClass, domStyle, domAttr, lang, array, on, jsonQuery,
    ApplicationConfig, ResourceLib, Utility, PrintTask, PrintParameters) {

    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,

        constructor: function (params) { },
        postCreate: function () {
            this.wgLabel.innerHTML = ResourceLib.text("menu-tools-printmap-header");
            on(this.btnPrintmap, "click", lang.hitch(this, function () {
                this._printMap();
            }));
        },

        /* Event from ToolPanel */
        /* Flag : is First Load this widget ? */
        isFirstLoad: true,
        /* On widget display on screen */
        onOpen: function () {
            
            //First Load Operation
            if (this.isFirstLoad) {
                this.isFirstLoad = false;
            }
        },

        _printMap: function () {
            this.onAction({ key: "print-map" });
        },

        /* On toolpanel close */
        onClose: function () { },

        /* Send Action to other control */
        onAction: function (obj) { /* attach event */ }
    });
});
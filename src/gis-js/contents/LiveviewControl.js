﻿define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",

    "dojo/text!./templates/LiveviewControl.html",

    "dojo/dom",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/dom-attr",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/on",
    "dojox/json/query",

    "GOTModule/config/ApplicationConfig",
    "GOTModule/utils/ResourceLib",
    "GOTModule/utils/Utility",
    "vendors/iqtech/swfobject.js"
    
    //"vendors/hikvision/jsWebControl-1.0.0.min.js"
], function (declare, _WidgetBase, _TemplatedMixin, template, dom, domConstruct, domClass, domStyle, domAttr, lang, array, on, jsonQuery,
    ApplicationConfig, ResourceLib, Utility) {

    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,
        ctrl: null,

        _lstWndPanel: [],

        _WebControl: null,

        _hikVisionConfig: {
            webControl1: null,
            webControl2: null,
            webControl3: null
        },
        _resultCammer:null,
 

        _hikVisionServices: {
            login: "",
            getAreaByPage: "",
            getCameraByPage: "",
            getRealStreamUrl: "",
            getEncodeDevice: ""
        },
      

        constructor: function (params) {},
        postCreate: function () {},

        /* Event from ToolPanel */
        /* Flag : is First Load this widget ? */
        isFirstLoad: true,
        /* On widget display on screen */
        onOpen: function (param) {
            this.resultCamera = param;
            this.divVideoPanelHeader.innerHTML = ResourceLib.text("FleetMonitoring_Live_Video");; //"View on map";
            this.showCamara()
        },
       
        showCamara: function (){
            swfobject.removeSWF("divWnd0")
            swfobject.removeSWF("divWnd1")
            swfobject.removeSWF("divWnd2")

            for(let n = 1 ; n <= this.resultCamera.length ; n++ ){
                this.onContentLoading(true);
                var Wnd0 = document.getElementById("divWnd0");
                var Wnd1 = document.getElementById("divWnd1");
                var Wnd2 = document.getElementById("divWnd2");
                if(!Wnd0){
                    let d2 = document.createElement("div");
                    d2.setAttribute("id", "divWnd0");
					document.getElementById("divTitle0").innerHTML = "";

                    let d = document.createElement("div");
                    d.setAttribute("id", "divWnd0");
					document.getElementById("windowe0").appendChild(d);
                }

                if(!Wnd1){
                    let d2 = document.createElement("div");
                    d2.setAttribute("id", "divWnd0");
                    document.getElementById("divTitle1").innerHTML = "";
                    
                    let d = document.createElement("div");
					d.setAttribute("id", "divWnd1");
					document.getElementById("windowe1").appendChild(d);
                }

                if(!Wnd2){
                    let d2 = document.createElement("div");
                    d2.setAttribute("id", "divWnd0");
                    document.getElementById("divTitle2").innerHTML = "";
                    
                    let d = document.createElement("div");
					d.setAttribute("id", "divWnd2");
					document.getElementById("windowe2").appendChild(d);
                }
                
                if(this.resultCamera[(n-1)].brand === 3){
                    var dfdInitPlugin  = this._initPlayer("divWnd"+(n-1),this.resultCamera[(n-1)].cameraConfig);
                    $.when(dfdInitPlugin).done(lang.hitch(this, function (resInitPlugin) {
                        this._playCamera((n-1),
                        this.resultCamera[(n-1)].devices[0],
                        this.resultCamera[(n-1)].vehicleModelDisplayText,
                        this.resultCamera[(n-1)].sessionId,
                        this.resultCamera[(n-1)].cameraConfig)
                    })).done(lang.hitch(this, function(){
                        this.onContentLoading(false);
                    }))
                }
                this.onContentLoading(false);
            }
        },
        _initPlayer: function (windowId,config) {
            var dfdinitplayer = $.Deferred();
            var cameraConfig = config.split("|")
            var windowWidth = $( window ).width();
            var widthPlugin = null;
            var heightPlugin = null;
            if(windowWidth <= 1366){
                heightPlugin = 150
                widthPlugin = 250
            }else{
                heightPlugin = 250
                widthPlugin = 265
            }
            try {
                //Video plug-in init param
                var params = {
                    allowFullscreen: "true",
                    allowScriptAccess: "always",
                    bgcolor: "#FFFFFF",
                    wmode: "transparent"
                };

                //Init flash
                swfobject.embedSWF("vendors/iqtech/player.swf", windowId, widthPlugin, heightPlugin, "9.0.0", null, null, params, null);

                setTimeout(lang.hitch(this,function(){
                    //Setting the language video widget
                    swfobject.getObjectById(windowId).setLanguage("vendors/iqtech/en.xml");
                    //First of all windows created
                    swfobject.getObjectById(windowId).setWindowNum(36);
                    //Re-configure the current number of windows
                    swfobject.getObjectById(windowId).setWindowNum(4);
                    //Set the video plug-in server
                    let arrConfig = cameraConfig[2].split('//');
                    
                    swfobject.getObjectById(windowId).setServerInfo(arrConfig[1], "6605");
                    isInitFinished = true;

                    
                    dfdinitplayer.resolve({
                        mass:"Suss"
                    })
                }), 2000)
                
            } catch (ex) {
                console.log(ex);
            }
            return dfdinitplayer;
        },

        _playCamera: function (windowId,dataCamera,nameCamera,sessionId,config) {
            if (!isInitFinished) {
                return;
            }
            var cameraConfig = config.split("|")
            this["divTitle" + windowId].innerHTML = nameCamera;
            on(this["divTitle" + windowId],"click",lang.hitch(this, function(){
                let result = {
                    sessionId:sessionId,
                    iqtechId:dataCamera.did,
                    title:nameCamera,
                    cameraConfig: cameraConfig[2]
                }
                     localStorage.dataIQTech = JSON.stringify(result);
                     this.onAction({ key: "open-live-view-iqtech", param: result });
                //window.open("LiveviewPage2.html", "liveViewPage");
            }));

           // $("#divTitle"+windowId).html("<a onclick='alert("+dataCamera.vid+")'>"+dataCamera.vid+"</a>");
            // swfobject.getObjectById("divWnd"+windowId).setBufferTime(0, 1);
            // swfobject.getObjectById("divWnd"+windowId).setBufferTimeMax(0, 2);
            //Stop Video
            //swfobject.getObjectById("divWnd"+index).stopVideo(0);
            //Set the video window title
            swfobject.getObjectById("divWnd"+windowId).setVideoInfo(0,"");
            for (let n = 0; n <= 3; n++) {
                //Play Video
                
                swfobject.getObjectById("divWnd"+windowId).startVideo(n, sessionId, dataCamera.did, n, 1, true);
            }
        },

        _ajaxPost: function (url, data, successCallback) {
            $.ajax({
                url: url,
                data: data,
                type: 'post',
                success: successCallback
            });
        },

      
        /* On toolpanel close */
        onClose: function () {
     
                swfobject.removeSWF("divWnd0")
                swfobject.removeSWF("divWnd1")
                swfobject.removeSWF("divWnd2")
        },

        /* Send Action to other control */
        onAction: function (obj) { /* attach event */ },

        onShowPanel: function(){
            this.showCamara();
        },

        /* Send State to ToolsPanel */
        onHasActiveContent: function (hasActive) { /* attach event */ 

        },
        /* Show Loadinng Panel */
        onContentLoading: function (isLoad) { }
    });
});
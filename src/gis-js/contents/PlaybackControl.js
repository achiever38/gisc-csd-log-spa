﻿define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",

    "dojo/text!./templates/PlaybackControl.html",

    "dojo/dom",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/dom-attr",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/_base/event",
    "dojo/on",
    "dojox/json/query",

    "esri/geometry/Point",
    "esri/geometry/Polyline",
    "esri/geometry/Extent",
    "esri/geometry/webMercatorUtils",

    "GOTModule/config/ApplicationConfig",
    "GOTModule/utils/ResourceLib",
    "GOTModule/utils/Utility",
    "GOTModule/utils/VehiclePlayback",

    "BowerModule/jquery/src/jquery",
    "BowerModule/jRange/jquery.range"
], function (declare, _WidgetBase, _TemplatedMixin, template, dom, domConstruct, domClass, domStyle, domAttr, lang, array, dojoEvent, on, jsonQuery, Point, PolyLine, Extent, webMercatorUtils,
    ApplicationConfig, ResourceLib, Utility, VehiclePlayback, $) {
    
    Utility.loadWidgetCSS(require.toUrl("./gis-js/libs/jRange/jquery.range.css"));

    const PB_MODE = { ANIMATE: "ANIMATE", FOOTPRINT: "FOOTPRINT", FULLY: "FULLY" };

    let map = ApplicationConfig.mapObject;
    let mapManager = ApplicationConfig.mapManager;
    //let maxLevel = ApplicationConfig.animationConfig.maxPlaybackZoomLevel;

    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,
        pbControl: null,

        constructor: function (params) { },
        postCreate: function () {
            on(this.btnPlay, "click", lang.hitch(this, "_btnPlayClick"));
            on(this.btnPause, "click", lang.hitch(this, "_btnPauseClick"));
            //on(this.btnStop, "click", lang.hitch(this, "_btnStopClick"));

            require([
                "dojo/text!./gis-js/images/svg/btn_playback_play.svg",
                "dojo/text!./gis-js/images/svg/btn_playback_pause.svg"], lang.hitch(this, function (svgPlay, svgPause) {
                    domConstruct.place(domConstruct.toDom(svgPlay), this.btnPlay, "last");
                    domConstruct.place(domConstruct.toDom(svgPause), this.btnPause, "last");
            }));
        },

        tempPauseState: null,

        /* Event from ToolPanel */
        /* Flag : is First Load this widget ? */
        isFirstLoad: true,
        /* On widget display on screen */
        onOpen: function (param) {

            //First Load Operation
            let returnPBData = mapManager.animatePlayback(param);
            let totalBarLength = ApplicationConfig.animationConfig.fps * ApplicationConfig.animationConfig.animateTime * (returnPBData.playback.length - 1);

            if (this.isFirstLoad) {
                this.isFirstLoad = false;

                this.pbControl = new VehiclePlayback();

                on(this.pbControl, "VehicleMove", lang.hitch(this, "_onVehicleMove"));
                on(this.pbControl, "VehicleEndAnimate", lang.hitch(this, "_onAnimateEnd"));
                on(this.pbControl, "ChangeView", lang.hitch(this, "_onChangeViewExtent"));
                on(this.pbControl, "AttributeChanges", lang.hitch(this, "_onAttributeChanges"));

                $(this.inpSlider).jRange({
                    from: 0,
                    to: totalBarLength - 1,
                    width: 215,
                    step: 1,
                    format: '%s',
                    showLabels: false,
                    showScale: false,
                    snap: false,
                    onstatechange: lang.hitch(this, function (data) {
                        //console.log("onstatechange", data);
                    }),

                    ondragstart: lang.hitch(this, function () {

                        //this.tempPauseState = this.pbControl.isPause;

                        //if (this.pbControl.isPause != null && !this.pbControl.isPause)
                        this.pbControl.pauseAnimatePlayback();
                        domClass.remove(this.btnPlay, "hide");
                        domClass.add(this.btnPause, "hide");
                    }),

                    ondragend: lang.hitch(this, function (data) {
                        console.log(data);
                        this.pbControl.stepIndex(Number(data));
                        //if (this.pbControl.isPause != null && this.pbControl.isPause) {
                        //    //this.tempPauseState = null;
                        //    this.pbControl.resumeAnimatePlayback();
                        //}
                    }),

                    onbarclicked: lang.hitch(this, function (data) {
                        this.pbControl.stepIndex(Number(data));
                    })

                });

                $(this.inpSlider).jRange("setValue", '0');
            }
            else {

                $(this.inpSlider).jRange("updateRange", '0,' + (totalBarLength - 1), 0);
            }


            this.pbControl.stopAnimatePlayback();
            this.pbControl.setPlaybackData(returnPBData);
        },

        //clearPlaybackLayer: function () {
        //    let playbackCommandOption = ApplicationConfig.commandOptions["playback-command"];
        //    mapManager.clearLayers(playbackCommandOption);
        //},

        _btnPlayClick: function () {
            domClass.toggle(this.btnPlay, "hide");
            domClass.toggle(this.btnPause, "hide");

            if (this.pbControl.isPause != null && this.pbControl.isPause) {
                this.pbControl.resumeAnimatePlayback();
            }
            else {
                this.onAction({ key: "close-playback-widget", param: {} });
                this.pbControl.startAnimatePlayback();
            }
        },

        _btnPauseClick: function () {
            domClass.toggle(this.btnPlay, "hide");
            domClass.toggle(this.btnPause, "hide");

            this.pbControl.pauseAnimatePlayback();
        },

        _btnStopClick: function () {
            this.pbControl.stopAnimatePlayback();
        },

        _onAnimateEnd: function () {
            domClass.remove(this.btnPlay, "hide");
            domClass.add(this.btnPause, "hide");
        },

        _onVehicleMove: function (param) {
            $(this.inpSlider).jRange('setValue', '' + param.index);
        },

        _onChangeViewExtent: function (evt) {
            var path = new PolyLine(evt);
            var ext = path.getExtent().expand(3);

            // === TODO ===
            // 1.แปลง extent ของ map เป็น 4326 ( lalt, lon )
            // 2.สร้าง extent ใหม่ จาก evt[1] โดยให้ evt[1] เป็น center
            // 3.เพิ่ม check ว่า extent ใหม่ว่า path ที่จะไป contains ใน extent ใหม่หรือไม่
            // 3.1 ถ้าไม่ เปลี่ยน extent เป็นของ ext เหมือนเดิม
            // 3.2 ถ้าใช่ pan ไปยัง center ของ extent ใหม่


            // 1.แปลง extent ของ map เป็น 4326 ( lalt, lon )
            var geoMapExt = webMercatorUtils.webMercatorToGeographic(map.extent);

            // 1.1 หา diff ของ xmax - xmin และ ymax - ymin
            var currMapWidth = geoMapExt.getWidth(); //เวลา + - ให้หาร 2 ก่อนเพราะจาก fn จะได้ระยะจาก xmax - xmin เลย
            var currMapHeight = geoMapExt.getHeight(); //เวลา + - ให้หาร 2 ก่อนเพราะจาก fn จะได้ระยะจาก xmax - xmin เลย

            //2.สร้าง extent ใหม่ จาก evt[1] โดยให้ evt[1] เป็น center
            var newXMin = evt[1][0] - (currMapWidth / 2);
            var newXMax = evt[1][0] + (currMapWidth / 2);
            var newYMin = evt[1][1] - (currMapHeight / 2);
            var newYMax = evt[1][1] + (currMapHeight / 2);
            var nextExt = new Extent({
                xmin: newXMin,
                ymin: newYMin,
                xmax: newXMax,
                ymax: newYMax,
                spatialReference: { wkid: 4326 }
            });

            if (!map.extent.contains(path.getExtent())) {
                if (nextExt.contains(path.getExtent())) {
                    // 3.2 ถ้าใช่ pan ไปยัง center ของ extent ใหม่
                    map.centerAt(nextExt.getCenter());
                }
                else {
                    // 3.1 ถ้าไม่ เปลี่ยน extent เป็นของ ext เหมือนเดิม
                    map.setExtent(ext);
                }
            }
            else
            {
                //ไม่เปลี่ยน ext
            }

            //if (!map.extent.contains(path.getExtent())) {
            //    map.setExtent(ext);
            //}
        },

        _onAttributeChanges: function (evt) {
            //console.log(evt);

            let status = evt["STATUS"];

            domConstruct.empty(this.divListStat);

            
            if (status.length > 0) {

                let divLocation = domConstruct.create("div", { class: "div-status-blog" }, this.divListStat, "last");
                let divLocationContainer = domConstruct.create("div", { class: "div-location-container" }, divLocation, "last");
                let divLocationSVG = domConstruct.create("div", { class: "div-svg-icon-status" }, divLocationContainer, "last");

                require([
                "dojo/text!./gis-js/images/svg/icon_location_playback_info.svg"], lang.hitch(this, function (svgRaw) {
                    let svgIcon = domConstruct.toDom(svgRaw);
                    domConstruct.place(svgIcon, divLocationSVG, "last");
                 }));

                let divLocationText = domConstruct.create("div", { class: "div-location-text", innerHTML: evt.LOCATION }, divLocationContainer, "last");
            }

            let divBlock = null;
            array.forEach(status, lang.hitch(this, function (statItem, i) {

                if (i % 2 == 0) {
                    divBlock = domConstruct.create("div", { class: "div-status-blog" }, this.divListStat, "last");
                }

                let divContainer = domConstruct.create("div", { class: "div-icon-container", title: statItem.title || "" }, divBlock, "last");

                let divIcon = domConstruct.create("div", { class: "div-svg-icon-status " + statItem.cssClass }, divContainer, "last");
                let svg = domConstruct.toDom(statItem.icon);

                let divText = domConstruct.create("div", { class: "div-svg-icon-status-text", innerHTML: statItem.text || "", title: statItem.text || "" }, divContainer, "last");

                domConstruct.place(svg, divIcon, "last");
            }));

        },

        /* On toolpanel close */
        onClose: function () {
            //console.log("on close PB");
            $(this.inpSlider).jRange("setValue", '0');
            this.pbControl.stopAnimatePlayback();
            mapManager.clearLayers([ApplicationConfig.commandOptions["playback-command"].layer.animate]);
            //this.onAction({ key: "show-all-trackvehicle", param: {} });
            this.onAction({ key: "clear-animate-playback" });


        },

        /* Send Action to other control */
        onAction: function (obj) { /* attach event */ }
    });
});
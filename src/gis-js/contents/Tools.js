﻿define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",

    "dojo/text!./templates/Tools.html",

    "dojo/dom",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/dom-attr",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/on",
    "dojox/json/query",

    "GOTModule/controls/ListItemContainer",

    "GOTModule/config/ApplicationConfig",
    "GOTModule/utils/ResourceLib",
    "GOTModule/utils/Utility",
    "./tools/Measurement",
    "./tools/ClearGraphics",
    "./tools/PrintMap"
], function (declare, _WidgetBase, _TemplatedMixin, template, dom, domConstruct, domClass, domStyle, domAttr, lang, array, on, jsonQuery,
    ListItemContainer, ApplicationConfig, ResourceLib, Utility, Measurement, ClearGraphics, PrintMap) {

    var wgMeasurement;

    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,

        constructor: function (params) { },
        postCreate: function () {

            wgMeasurement = new Measurement();
            var wgClearGraphics = new ClearGraphics();
            var wgPrintMap = new PrintMap();

            on(wgMeasurement, "Action", lang.hitch(this, function (obj) {
                this.onAction(obj);
            }));

            on(wgClearGraphics, "Action", lang.hitch(this, function (obj) {
                wgMeasurement.clearActiveState();
                this.onAction(obj);
            }));

            on(wgPrintMap, "Action", lang.hitch(this, function (obj) {
                this.onAction(obj);
            }));

            this.divToolsContent.appendChild(wgMeasurement.domNode)
            this.divToolsContent.appendChild(wgClearGraphics.domNode)
            this.divToolsContent.appendChild(wgPrintMap.domNode)
        },

        clearMeasurement: function () {
            wgMeasurement.clearMeasurement();
        },

        /* Event from ToolPanel */
        /* Flag : is First Load this widget ? */
        isFirstLoad: true,
        /* On widget display on screen */
        onOpen: function () {

            //First Load Operation
            if (this.isFirstLoad) {
                this.isFirstLoad = false;
            }
        },

        /* On toolpanel close */
        onClose: function () {
            wgMeasurement.onClose();
        },

        /* Send Action to other control */
        onAction: function (obj) { /* attach event */ }
    });
});
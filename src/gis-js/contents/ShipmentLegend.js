﻿define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",

    "dojo/text!./templates/ShipmentLegend.html",

    "dojo/dom",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/dom-attr",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/on",
    "dojox/json/query",

    "GOTModule/controls/ListItemContainer",

    "GOTModule/config/ApplicationConfig",
    "GOTModule/utils/ResourceLib",
    "GOTModule/utils/Utility"
], function (declare, _WidgetBase, _TemplatedMixin, template, dom, domConstruct, domClass, domStyle, domAttr, lang, array, on, jsonQuery,
    ListItemContainer, ApplicationConfig, ResourceLib, Utility) {

    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,
        ctrl: null,
        toggleList: [],

        constructor: function (params) { },
        postCreate: function () { },

        /* Event from ToolPanel */
        /* Flag : is First Load this widget ? */
        isFirstLoad: true,
        /* On widget display on screen */
        onOpen: function (param) {
            
            if(!param || Object.keys(param).length == 0){
                return false;
            }

            this.divViewOnMapHeader.innerHTML = param.vehicleLicense; //"View on map";
            this.spnJobCode.innerHTML = param.jobCode;
            this.spnJobName.innerHTML = param.jobName;
            this.spnDriverName.innerHTML = (param.driverName == "" || param.driverName == null) ? "-" : param.driverName;
            //First Load Operation
            if (this.isFirstLoad) {
                this.isFirstLoad = false;

                //this.spnVehicleName.innerHTML = param.vehicleLicense;
                this.spnHeaderJobCode.innerHTML = ResourceLib.text("wg-shipmentlegend-jobcode");
                this.spnHeaderJobName.innerHTML = ResourceLib.text("wg-shipmentlegend-jobname");
                this.spnHeaderDriverName.innerHTML = ResourceLib.text("wg-shipmentlegend-driver");
                this.spnHeaderShowWaypoint.innerHTML = ResourceLib.text("wg-shipmentlegend-pininfo-header");

                this.ctrl = new ListItemContainer({ selectedMode: "toggle", type: "checked", useHighlight: false });
                domConstruct.place(this.ctrl.domNode, this.divList, "last");

                let lstConfig = [];
                ApplicationConfig.viewonmapConfig.forEach(lang.hitch(this, function (itm) {
                    lstConfig.push({
                        key: itm.key,
                        text: ResourceLib.text(itm.text),
                        icon: itm.icon,
                        selected: itm.selected
                    });
                    if(itm.selected){

                        this.toggleList.push(itm.key);
                    }
                }));
                this.ctrl.createItem(lstConfig);
                on(this.ctrl, "ItemClick", lang.hitch(this, "_ListClickHandler"));
                for (var key in ApplicationConfig.waypointConfig.status) {

                    var wpConfig = ApplicationConfig.waypointConfig.status[key];

                    var divPinItem = domConstruct.create("div", {}, this.divPinList);
                    domClass.add(divPinItem, "view-on-map-pinitem");
                    domConstruct.create("img", { src: wpConfig.url }, divPinItem);
                    domConstruct.create("span", { innerHTML: ResourceLib.text(wpConfig.key) }, divPinItem);
                }
            }
            else {
                if (ApplicationConfig.isResetFilterViewOnMap) {
                    this.resetFilter();
                    ApplicationConfig.isResetFilterViewOnMap = false;
                }
            }
        },

        _ListClickHandler: function (obj) {

            if (obj.isSelected) {
                if(this.toggleList.indexOf(obj.key) == -1){
                    this.toggleList.push(obj.key);
                }
            }
            else {
                this.toggleList.splice(this.toggleList.indexOf(obj.key), 1);
            }

            this.onAction({ key: "toggle-view-on-map", param: { toggle: this.toggleList } });
            this.onHasActiveContent(this.ctrl.getCountSelected());
        },

        resetFilter: function () {
            this.ctrl.setSelectedIndex(0);
            this.ctrl.setSelectedIndex(1);
            this.ctrl.setSelectedIndex(2);
            this.ctrl.setSelectedIndex(3,false);
            this.toggleList = ["plan", "actual", "waiting"]//,"poiarea"];
        },

        /* On toolpanel close */
        onClose: function () {
            //this.onAction({ key: "close-viewonmap", param: {} });
            //this.onAction({ key: "show-all-trackvehicle", param: {} });
        },

        /* Send Action to other control */
        onAction: function (obj) { /* attach event */ },

        /* Send State to ToolsPanel */
        onHasActiveContent: function (hasActive) {/* attach event */ }
    });
});
﻿function GOTPlayerWindows(uniqueID) {
    this.playCamera = playCamera;
    this._uniqueID = uniqueID;
    this.initOneWindow = initOneWindow;
    this.initFourWindow = initFourWindow;

}

// function cameraWindows(cameraId, targetId) {
//         var tagVideo = document.createElement("video");
//         $(tagVideo).attr({
//             'id': cameraId,
//             'preload': 'auto',
//             'data-setup': '{}',
//             'controls': true
//         });
//         $(tagVideo).addClass('video-js');
//         $(tagVideo).appendTo('#' + targetId);
    
// }

// function playcamera(windowsId, URL) {
//     var options = {
//         autoplay: 'muted',
//         height: 250,
//         width: 300
//     };
//     var tagSource = document.createElement("source");

//     $(tagSource).attr({
//         'src': URL,
//         'type': 'application/x-mpegURL'
//     });

//     $(tagSource).appendTo('#' + windowsId);

//     videojs('#' + windowsId, options);
// }

// function oneWindows() {
//     var tagVideo = document.createElement("video");
//         $(tagVideo).attr({
//             'id': 'onevideo',
//             'preload': 'auto',
//             'data-setup': '{}',
//             'controls': true
//         });
//         $(tagVideo).addClass('video-js');
//         // $(tagSource).appendTo(tagVideo);
//         $(tagVideo).appendTo('#' + targetId);
//         //videojs('my-video'+n, options);   

// }

// function fourWindows(targetId,subId,URL) {
//     var tagDiv = document.createElement("div");
//     var tagColumn1 = document.createElement("div");
//     var tagColumn2 = document.createElement("div");

//     $(tagDiv).attr({
//         'id': 'fourWindows'+subId
//     })
//     $(tagColumn1).attr({
//         'id': 'column1'+subId
//     })
//     $(tagColumn2).attr({
//         'id': 'column2'+subId
//     })
//     $(tagColumn1).css("display", "flex");
//     $(tagColumn2).css("display", "flex");
//     $(tagColumn1).appendTo(tagDiv);
//     $(tagColumn2).appendTo(tagDiv);
//     $(tagDiv).appendTo('#' + targetId);


//     cameraWindows("four"+subId+1, "column1"+subId)
//     cameraWindows("four"+subId+2, "column1"+subId)
//     cameraWindows("four"+subId+3, "column2"+subId)
//     cameraWindows("four"+subId+4, "column2"+subId)

//     playcamera("four"+subId+1,URL.camera0)
//     playcamera("four"+subId+2,URL.camera1)
//     playcamera("four"+subId+3,URL.camera2)
//     playcamera("four"+subId+4,URL.camera3)



// }



function initOneWindow(targetId){
    var divWindow0 = document.createElement("video");
    $(divWindow0).attr({
        'id': "video_" + this._uniqueID + "_0",
        'preload': 'auto',
        'data-setup': '{}',
        'controls': true
    });
   // divWindow0.id = "video_" + this._uniqueID + "_0";
   $(divWindow0).addClass('video-js');
   $(divWindow0).appendTo('#' + targetId);

}

function initFourWindow(targetId){
    var tagDiv = document.createElement("div");
    var tagColumn1 = document.createElement("div");
    var tagColumn2 = document.createElement("div");

    $(tagDiv).attr({
        'id': 'fourWindows_'+this._uniqueID
    })
    $(tagColumn1).attr({
        'id': 'column_1_'+this._uniqueID
    })
    $(tagColumn2).attr({
        'id': 'column_2_'+this._uniqueID
    })
    $(tagColumn1).css("display", "flex");
    $(tagColumn2).css("display", "flex");
   


    var divWindow0 = document.createElement("video");
    $(divWindow0).attr({
        'id': "video_" + this._uniqueID + "_0",
        'preload': 'auto',
        'data-setup': '{}',
        'controls': true
    });
    $(divWindow0).addClass('video-js');
    //divWindow0.id = "video_" + this._uniqueID + "_0";
    var divWindow1 = document.createElement("video");
    $(divWindow1).attr({
        'id': "video_" + this._uniqueID + "_1",
        'preload': 'auto',
        'data-setup': '{}',
        'controls': true
    });
    $(divWindow1).addClass('video-js');
    //divWindow1.id = "video_" + this._uniqueID + "_1";
    var divWindow2 = document.createElement("video");
    $(divWindow2).attr({
        'id': "video_" + this._uniqueID + "_2",
        'preload': 'auto',
        'data-setup': '{}',
        'controls': true
    });
    $(divWindow2).addClass('video-js');
    //divWindow2.id = "video_" + this._uniqueID + "_2";
    var divWindow3 = document.createElement("video");
    $(divWindow3).attr({
        'id': "video_" + this._uniqueID + "_3",
        'preload': 'auto',
        'data-setup': '{}',
        'controls': true
    });
    $(divWindow3).addClass('video-js');
    //divWindow3.id = "video_" + this._uniqueID + "_3";

    $(divWindow0).appendTo(tagColumn1);
    $(divWindow1).appendTo(tagColumn1);
    $(divWindow2).appendTo(tagColumn2);
    $(divWindow3).appendTo(tagColumn2);

    $(tagColumn1).appendTo(tagDiv);
    $(tagColumn2).appendTo(tagDiv);
    $(tagDiv).appendTo('#' + targetId);
}

function playCamera(windowIndex,URL){
    var options = {
        autoplay: 'muted',
        height: 250,
        width: 300
    };

    var tagSource = document.createElement("source");
    $(tagSource).attr({
        'src': URL,
        'type': 'application/x-mpegURL'
    });
    $(tagSource).appendTo("#video_" + this._uniqueID + "_" + windowIndex);
    videojs("video_" + this._uniqueID + "_" + windowIndex, options);
    
}
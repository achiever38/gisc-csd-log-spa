import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./dropdown.html";
import "select2";

/**
 * 
 * 
 * @class Dropdown
 * @extends {ControlBase}
 */
class Dropdown extends ControlBase {
    /**
     * Creates an instance of Dropdown.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.options = this.ensureObservable(params.options);
        this.optionsText = this.ensureNonObservable(params.optionsText);
        this.optionsCaption = this.ensureObservable(params.optionsCaption);
        this.value = this.ensureObservable(params.value, this.optionsCaption);
        this.type = this.ensureNonObservable(params.type, 'default');
        this.multiple = ko.observable();
        this.filterVisible = ko.observable(false);
        this.isDropdownFocus = ko.observable(false);
        this.cssFocusClass = ko.pureComputed(() => {
            // Your pure compute logic
            return this.isDropdownFocus() ? 'focus' : '';
        });

        this.disableState = ko.pureComputed(() => {
            return this.enable() === false ? "disabled" : "";
        });

        this.optionsAfterRender = (option, item) => {
            return option.value = item;
        }

        switch (this.type) {
            case "default":
                this.multiple(false);
                this.filterVisible(false);
                break;
            case "multiple":
                this.multiple(true);
                this.filterVisible(false);
                break;
            case "filter":
                this.filterVisible(true);
                break;
        }

        this.watchChange = this.ensureNonObservable(params.watchChange, false);

        this.staticVal = this.ensureNonObservable(params.value);

        /**
         * Check selected text
         * @returns
         */
        this.selectedText = ko.pureComputed(function () {
            var item = this.value();
            if (!item) {
                //return optionsCaption
                return this.optionsCaption();
            } else {
                //return selected value
                return item[this.optionsText];
            }
        }, this);
    }

    onDomReady() {
        var self = this;
        if(self.type === "filter"){
            //config select2
            var configSelect2 = {
                "language": {
                    "noResults": function(){
                        return self.i18n('Common_DataNotFound')();
                    }
                },
                placeholder: self.optionsCaption(),
            }
            //call function select2
            $("#" + self.id).select2(configSelect2).on('select2:open', function (e) {
                $('.select2-search input').prop('focus',false);
            });
        }

        var $el = $("#" + self.id).parent().find('span.app-dropdown-current');
        var $parent = $("#" + self.id).parent();

        if (this.watchChange) {

            this.value.subscribe(function (newValue) {

                if (newValue != self.staticVal) {
                    $el.addClass("watch-change");
                    $parent.addClass("watch-change");
                }
                else {
                    $el.removeClass("watch-change");
                    $parent.removeClass("watch-change");
                }
            });
        }
    }

    onLoad() {}
    onUnload() {
        this.options = null;
        this.optionsCaption = null;
        this.value = null;
        this.multiple = null;
        this.isDropdownFocus = null;
    }
}

export default {
    viewModel: Dropdown,
    template: templateMarkup
};
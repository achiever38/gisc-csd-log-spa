import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./window.html";
import WidgetNavigation from "../../gisc-chrome/shell/widgetNavigation";
import "kendo.all.min";

class WindowUI extends ControlBase {

    /**
     * Creates an instance of Window.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.widgetService = WidgetNavigation.getInstance();
        this.widgetParams = params || {};
        this.widgetParams.widgetInstance = this;
        this.componentName = this.ensureObservable(params.componentName);
        this.componentOptions = (params.componentOptions || {});
        this.componentInstance = ko.observable(null); // will be inject by factory.
        this.id = this.ensureNonObservable(params.widgetOptions.id, this.id);
        this.title = this.ensureNonObservable(params.widgetOptions.title, "");
        this.modal = this.ensureNonObservable(params.widgetOptions.modal, false);
        this.draggable = this.ensureNonObservable(params.widgetOptions.draggable, true);
        this.resizable = this.ensureNonObservable(params.widgetOptions.resizable, false);
        this.width = this.ensureNonObservable(params.widgetOptions.width, "70%");
        this.height = this.ensureNonObservable(params.widgetOptions.height, "230");
        this.left = this.ensureNonObservable(params.widgetOptions.left, "4.65%");
        this.bottom = this.ensureNonObservable(params.widgetOptions.bottom, "2px");
        this.minimize = this.ensureNonObservable(params.widgetOptions.minimize, true);
        this.maximize = this.ensureNonObservable(params.widgetOptions.maximize, false);
        this.onClose = this.ensureFunction(params.widgetOptions.onClose, ()=>{});
        
        
        this.isBusy = ko.observable(false);
        this.resizeHandler = null;
        this.customActions = this.ensureNonObservable(params.widgetOptions.customActions, []);
    }

    /**
     * Call component instance event handler.
     * 
     * @param {any} e
     */
    onCustomActionClick(e) {
        // Raise event aggregator.
        var currentComponentInst = this.componentInstance();
        if(currentComponentInst && currentComponentInst.onCustomActionClick){
            // Try to call custom click menu.
            currentComponentInst.onCustomActionClick(e);
        }
    }

    setPosition(windowWrapper) {
        var margin = 2;
        var windowPosition = windowWrapper.offset(),
        shouldOverridePosition = false,
        top = windowPosition.top,
        left = windowPosition.left,
        right = left + windowWrapper.outerWidth(),
        bottom = top + windowWrapper.outerHeight();
        
        if(left < 0){
            left = 0;
        }

        if((right + margin) > $(window).width()){
            left = $(window).width() - windowWrapper.outerWidth() - margin;
            left = (left < 0) ? 0 : left;
        }

        if((bottom + margin) > $(window).height()){
            top = $(window).height() - windowWrapper.outerHeight() - margin;
            top = (top < 0) ? 0 : top;
        }

        $("#" + this.id).closest(".k-window").css({top: top, left: left});

    }

    onDomReady () {
        var self = this;
        // Default teleriks icons are https://docs.telerik.com/kendo-ui/styles-and-layout/icons-web#actions
        var actionList = ["Close"];
        if(this.maximize){
            actionList.unshift("Maximize");
        }
        if(this.minimize){
            actionList.unshift("Minimize");
        }
        if(!_.isEmpty(self.customActions)){
            actionList = _.concat(self.customActions, actionList);
        }

        self.widget = $("#" + self.id).kendoWindow({
            width: this.width,
            height: this.height,
            actions: actionList,
            title: self.title,
            visible: false,
            modal: self.modal,
            draggable: self.draggable,
            resizable: self.resizable,
            deactivate: function(){
                this.destroy();
                self.widgetService.deactivateWiget(self.widgetParams);
            },
            dragstart: function() {
                $("#kWindowModal").show();
            },
            dragend: function() {
                $("#kWindowModal").hide();
                this.wrapper.css({
                    bottom: "auto"
                });

                var windowWrapper = self.widget.wrapper
                self.setPosition(windowWrapper);
            },
            resize: function() {
                clearTimeout(self.resizeHandler);
                self.resizeHandler = setTimeout(()=> {
                    $("#kWindowModal").hide();
                }, 200);
                $("#kWindowModal").show();
            },
            activate: function(){
                //self.widget.wrapper.css({top: "auto", bottom: 10});
                $(".k-widget .k-resize-handle").mousedown(function( e ) {
                    clearTimeout(self.resizeHandler);
                    self.resizeHandler = setTimeout(() => {
                        $("#kWindowModal").hide();
                    }, 200);
                    $("#kWindowModal").show();
                });
            },
            close: function(){
                self.widget = null;
                self.onClose();
            }
        }).data("kendoWindow").center().open();

        // Bind custom action if exists.
        if(!_.isEmpty(self.customActions)) {
            // Build selectors.
            var selectorList = [];
            self.customActions.forEach(ca => {
                selectorList.push(".k-i-" + ca.toLowerCase());
            });
            self.widget.wrapper.find(_.join(selectorList)).click(function(e) {
                e.preventDefault();
                self.onCustomActionClick(e);
            });
        }

        // Startup at bottom by default.
        $("#" + self.id).closest(".k-window").css({
            top: "auto",
            bottom: self.bottom,
            left: self.left
        });

        if(!this.maximize){
            self.widget.wrapper.children('.k-window-titlebar:first-child')
            .dblclick(function (e) {                           
                e.preventDefault();
                return false;
            });
        }
        
    }

    onLoad() {

    }

    onUnload() {

    }
    
    close(){
        if (this.widget){
            this.widget.close();
        }
    }
    onClose(){     
        this.onClose();
    }

}

export default {
    viewModel: WindowUI,
    template: templateMarkup
};
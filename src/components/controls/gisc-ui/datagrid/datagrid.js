import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./datagrid.html";
import Logger from "../../../../app/frameworks/core/logger";
import Utility from "../../../../app/frameworks/core/utility";
import "kendo.all.min";
import DataGridColumns from "./datagridColumns";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import WebRequestUserApiCore from "../../../../app/frameworks/data/apicore/webRequestUser";
import EventAggregator from "../../../../app/frameworks/core/eventAggregator";
import checkboxCheckedTemplateMarkup from "text!../../../../components/svgs/checkbox-checked-grid.html";
import { Constants, Enums, EntityAssociation} from "../../../../app/frameworks/constant/apiConstant";
/**
 * Telerik report integration.
 * 
 * @class ReportViewerControl
 * @extends {ControlBase}
 */
class DataGridControl extends ControlBase {
    /**
     * Creates an instance of ReportViewerControl.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.grid = null;
        this.id = this.ensureNonObservable(params.id, this.id);
        this.rowId = this.ensureNonObservable(params.rowId, "id");
        this.source = this.ensureObservable(params.source, []);
        this.columns = this.ensureObservable(params.columns, []);
        this.editable = this.ensureNonObservable(params.editable, false);
        this.sortable = this.ensureNonObservable(params.sortable, true);
        this.reorderable = this.ensureNonObservable(params.reorderable, false);
        this.groupable = this.ensureNonObservable(params.groupable, false);
        this.resizable = this.ensureNonObservable(params.resizable, true);
        this.filterable = this.ensureNonObservable(params.filterable, false);
        this.columnMenu = this.ensureNonObservable(params.columnMenu, false);
        this.toolbar = this.ensureObservable(params.toolbar, []);
        this.showRowSelector = this.ensureNonObservable(params.showRowSelector, false);
        this.selectedRows = this.ensureObservable(params.selectedRows, []);
        this.selectedRowsWithId = this.ensureObservable(params.selectedRowsWithId, []);
        this.onSelectingRow = this.ensureFunction(params.onSelectingRow, function(){});
        this.pageable = this.ensureNonObservable(params.pageable, true);
        this.pageSize = this.ensureNonObservable(params.pageSize, 20);
        this.pageSizeOptions = this.ensureNonObservable(params.pageSizeOptions, [10, 20, 50, 100]);
        this.height = this.ensureNonObservable(params.height, 0);
        this.onDatasourceRequestRead = this.ensureFunction(params.onDatasourceRequestRead, null);
        this.onDatasourceRequestUpdate = this.ensureFunction(params.onDatasourceRequestUpdate, null);
        this.onDetailInit = this.ensureNonObservable(params.onDetailInit, null);
        this.hiddenRefresh = this.ensureNonObservable(params.hiddenRefresh,false);
        this._eventAggregator = new EventAggregator();
        this.dataGridCollection = ko.observableArray(WebConfig.userSession.currentDataGridState);
        // can set remember datagrid ["columns", "page", "pageSize", "sort", "filter", "group"]
        this.rememberDataGridState = this.ensureNonObservable(params.rememberDataGridState, []);
        this.currentDataSource = this.ensureObservable(params.currentDataSource, []);
        //onClick column data grid expand
        this.detailInitOnClickColumns = this.ensureNonObservable(params.detailInitOnClickColumns, []);
        //for totol and set font bold last reccord
        this.totalRecord = this.ensureNonObservable(params.totalRecord, false);
        this.canExpandDataGrid = this.ensureNonObservable(params.canExpandDataGrid, null);
   
        this.statusText = ko.observable();
        this.statusType = ko.observable();
        this.statusVisible = ko.observable(false);
        this.effectiveIcon = ko.pureComputed(() => {
            switch(this.statusType()) {
                case "fail":
                    return "svg-ic-statusbar-fail";
                default:
                    return "svg-ic-statusbar";
            }
        });
        //for refresh datagrid set data in checkbox = []
        this.tempDataCheckBox = ko.observableArray([]);
        //select current row
        this.isSelectingRow = ko.observableArray([]);
        //index hide column menu
        this.columnMenuIndex = ko.observableArray([]);
        //index disable column resize
        this.columnResizeIndex = ko.observableArray([]);
        
        
        //read on data source (webrequest)
        this.onDatasourceRequestInternalRead = (request) => {
            if(this.hiddenRefresh){
                $("div.k-loading-mask").remove();
            }
            
            this.onDatasourceRequestRead(request.data)
            .done((result) => {
                //set default grid
                this.resetGrid();
                //setTimeout for reload height grid 100% 
                setTimeout(() => {
                    if(this.grid && result.totalRecords){
                        if(this.grid.dataSource && this.grid.dataSource.page() > result.currentPage){
                            this.grid.dataSource.page(result.currentPage);
                        }
                    }
                    request.success(result);
                }, 0);
            });
        }

        //update on data source (webrequest)
        this.onDatasourceRequestInternalUpdate = (request) => {
            this.onDatasourceRequestUpdate(request.data);
            $("div.k-loading-mask").remove();
        }

        this._eventAggregator.subscribe(this.id, (eventObj) => {
            if(!_.isEmpty(this.grid)){
                if(eventObj.eventName == "refresh"){
                    this.grid.dataSource.read();
                } else if (eventObj.eventName === "updateCheckboxSelectAll") {
                    this._dispatchEventUpdateCheckboxSelectAll(eventObj.message);
                } 
            }
        });

        this.onCloumnChange = (e) => {
            var self = this;
            var field = e.column.field;
            var hidden = e.column.hidden;
            var fDataGrid, fDataGridColumn, dataGridColumns, fColumns = null;
            var indexDataGrid, indexCloumn = 0;
            
            //if set remember eq columns then save to user preference
            fColumns = _.filter(self.rememberDataGridState, function(v){ return v == "columns" });
            if(fColumns.length > 0){
                fDataGrid = _.filter(self.dataGridCollection(), {id:self.id});
                //if not found then push new data grid id
                if(fDataGrid.length === 0){
                    self.dataGridCollection().push({
                        id:self.id,
                        columns:[{
                            field: field,
                            hidden: hidden
                        }]
                    });
                }else{
                    indexDataGrid = _.findIndex(self.dataGridCollection(), {id:self.id});
                    indexCloumn = _.findIndex(self.dataGridCollection()[indexDataGrid].columns, {field:field});
                    //if not found then push new column
                    if(indexCloumn === -1){
                        self.dataGridCollection()[indexDataGrid].columns.push({
                            field: field,
                            hidden: hidden
                        })
                    }else{
                        self.dataGridCollection()[indexDataGrid].columns[indexCloumn].hidden = hidden;
                    }

                }

                //update state to database
                var state =
                [
                    {
                        key: Enums.ModelData.UserPreferenceKey.DataGridState,
                        value: JSON.stringify(self.dataGridCollection()),
                        infoStatus: Enums.InfoStatus.Update
                    }
                ];
                WebRequestUserApiCore.getInstance().updateUserPreferences(state)
                .done(()=>{
                    Logger.info("Complete update profile.", self.dataGridCollection());
                    WebConfig.userSession.currentDataGridState = self.dataGridCollection();
                })
                .fail(() => {
                    Logger.error("Fail update profile.")
                });

                
                
            }
            
        }


    }

    close(){
        this.statusVisible(false);
    }


    onDomReady () {

        // console.log("this.columns indatagrid>>>",this.columns())
        var self = this;
        var $el = $("#" + self.id);
        var grid = $el.kendoGrid({
            dataSource: this.getApiDataSource(),
            detailInit: this.onDetailInit,
            columns: this.getColumns(),
            pageable: (this.pageable) ? {
                pageSizes: this.pageSizeOptions,
                refresh: false,
                buttonCount: 5,
                messages: {
                    display: "{0:#,##} - {1:#,##} of {2:#,##} items"
                }
            } : false,
            // persistSelection: false,
            // selectable: "row",
            height: (this.height) ? this.height : this.getHeight(),
            editable: {
                update: this.editable,
                createAt: "bottom"
            },
            sortable: this.sortable ,
            noRecords: {
                template: this.i18n("M111")()
            },
            reorderable: this.reorderable ,
            groupable: this.groupable ,
            resizable: this.resizable ,
            filterable: (this.filterable) ? {
                extra: false,
                operators: {
                    string: {
                        contains: "Contains",
                        eq: "Is equal to"
                    },
                    date: {
                        eq: "Is equal to",
                    },
                    number: {
                        contains: "Contains",
                        eq: "Is equal to"
                    }
                }
            } : this.filterable,
            columnMenu: this.columnMenu,
            //for resize mobile or ipad
            mobile: true,
            columnShow: this.onCloumnChange.bind(this),
            columnHide: this.onCloumnChange.bind(this),
            toolbar: this.toolbar(),
            // change: this.onChangeDataGrid.bind(this),
            dataBound: this.onDataBound.bind(this), 
          });
        
        self.grid = grid.data("kendoGrid");
        
        //load current data grid
        this.getCurrentDataGrid();

        //set function onclick
        $el.on("click", "[data-grid='table-column-cell-onclick']", function (e) {
            var grid = $(e.target).closest('.k-grid').data("kendoGrid");
            var row = $(e.target).closest("tr");
            var colIdx = $("th", row).index(this);
            var row_data = [];
            var checkHeader = false;
            //check onclick header or body
            if(colIdx != -1){
                colIdx = colIdx;
                checkHeader = true;
                _.forEach(grid._data, (item) => {
                    row_data.push({
                        id:item.id
                    });
                });
            }else{
                colIdx = $("td", row).index(this);
                checkHeader = false;
                row_data = grid.dataItem(row).toJSON();
                //for fix detailInit in grid
                colIdx = (typeof self.onDetailInit == "function") ? colIdx - 1 : colIdx;
            }
            var fieldName = grid.columns[colIdx].field;
            fieldName = (checkHeader) ? fieldName + "Header" : fieldName;
            
            var mappedEvent = self.getRegisteredColumnEvent(fieldName);
            if(!_.isNil(mappedEvent)) {
                if(mappedEvent.eventType === "click") {
                    mappedEvent.eventAction(row_data, e);
                }
            }
        });


        //set function onclick in grid expand
        $el.on("click", "[data-grid='table-column-cell-onclick-expand']", function (e) {
            var grid = $(e.target).closest('.k-grid').data("kendoGrid");
            var row = $(e.target).closest("tr");
            var row_data = grid.dataItem(row).toJSON();
            var colIdx = $(this).closest("td").index();
            var parents = $(this).closest("td").parents();
            var kGrid = parents.find(".k-detail-row").find(".k-grid");
            var kGridHeader = kGrid.find("th[data-index='"+ colIdx +"']");
            var titleName = kGridHeader.attr("data-title");
            // var fieldName = grid.columns[colIdx].field;
             _.forEach(self.detailInitOnClickColumns, (val) => {
                var title = encodeURIComponent(val.title);
                self.registerColumnEvents(title, "click", val.onClick);
            });
            
            var mappedEvent = self.getRegisteredColumnEvent(encodeURIComponent(titleName));
            if(!_.isNil(mappedEvent)) {
                if(mappedEvent.eventType === "click") {
                    mappedEvent.eventAction(row_data, e);
                }
            }
        });
        
        //set selector all in header grid
        $el.on("click", "[data-grid='isRowSelectorAll'] input[type='checkbox']", function (e) {
            e = window.event;
            var checked = $(e.target).context.checked;      
            var grid = $(e.target).closest('.k-grid').data("kendoGrid");
            var dataItem = [];
            var checkBox = $el.find("[data-grid='table-column-cell-rowselector'] input[type='checkbox']");

            if(checked){
                checkBox.prop('checked', checked);
                _.forEach(grid._data, (item) => {
                    dataItem.push(item.toJSON());
                });
            }else{
                checkBox.prop('checked', false);
                dataItem = [];
            }
            
            self.tempDataCheckBox(dataItem);
            self.selectedRows(self.tempDataCheckBox());
            self.selectedRowsWithId({id: self.id, items: self.selectedRows()});
        });

        //set selector in body grid
        $el.on("click", "[data-grid='table-column-cell-rowselector'] input[type='checkbox']", function (e) {
            e = window.event;
            var checked = $(e.target).context.checked;      
            var grid = $(e.target).closest('.k-grid').data("kendoGrid");
            var dataItem = self.tempDataCheckBox();
            var row = $(e.target).closest("tr");
            row.removeClass("k-state-selected");
            var row_data = grid.dataItem(row).toJSON();
    
            // if check is all then input checkBoxAll is equal check else input checkBoxAll is equal uncheck
            var isCheck = $el.find("[data-grid='table-column-cell-rowselector'] input[type='checkbox']:checked").length;
            var checkBox = $el.find("[data-grid='table-column-cell-rowselector'] input[type='checkbox']").length;
            
            if(isCheck === checkBox){
                $el.find("[data-grid='isRowSelectorAll'] input[type='checkbox']").prop("checked", true);
            }else{
                $el.find("[data-grid='isRowSelectorAll'] input[type='checkbox']").prop("checked", false);
            }
            if(checked){
                dataItem.push(row_data);
            }else{
                dataItem = dataItem.filter(function( obj ) {
                    return obj[self.rowId] !== row_data[self.rowId];
                });
            }
            self.tempDataCheckBox(dataItem);
            self.selectedRows(self.tempDataCheckBox());  
            self.selectedRowsWithId({id: self.id, items: self.selectedRows()});
        });

        //set select row in grid
        $el.on("click", "td[role='gridcell']",function (e) {
            let dgCell = $(e.target).attr("data-grid");
            //click only cell emty not have event
            if(!dgCell){
                

                let grid = $(e.target).closest('.k-grid').data("kendoGrid");
                $el.find(".k-grid-content tr.k-state-selected").removeClass('k-state-selected');
                let row = $(e.target).closest("tr");
                let col = null;
                let row_data = null;
                //คืน colum feild
                if(grid){
                    col = grid.columns[$(e.target).closest("td").index()].field;
                    grid.select(row);
                    row_data = grid.dataItem(row).toJSON();
                }
                //row.addClass("k-state-selected");
                self.onSelectingRow(row_data,col);
                //set curent row selected
                self.isSelectingRow(row_data);
            }
        });

        //set checkbox all in header grid
        $el.on("click", "[data-grid='isCheckBoxAll'] input[type='checkbox']", function (e) {

            var grid = $(e.target).closest('.k-grid').data("kendoGrid");
            var colIdx = $(this).closest("th").index();
            var colCheckBox = colIdx + 1;
            var fieldName = grid.columns[colIdx].field;
            var data = [];

            _.forEach(grid._data, (item) => {
                data.push({
                    id:item.id,
                    value: this.checked
                });
            });
            
            $el.find("[data-grid='table-column-cell-checkbox']:nth-child(" + colCheckBox + ") input").prop("checked", this.checked);  //select the inputs and [un]check it

            var mappedEvent = self.getRegisteredColumnEvent(fieldName);
            if(!_.isNil(mappedEvent)) {
                if(mappedEvent.eventType === "click") {
                    mappedEvent.eventAction(data);
                }
            }

            
        });

        //set checkbox in body grid
        $el.on("click", "[data-grid='table-column-cell-checkbox'] input[type='checkbox']", function (e) {
            //e = window.event;
            var checked = $(e.target).context.checked;   
            var grid = $(e.target).closest('.k-grid').data("kendoGrid");
            var row = $(e.target).closest("tr");
            //row.removeClass("k-state-selected");
            var colIdx = $(this).closest("td").index();
            var colCheckBox = colIdx + 1;
            var fieldName = grid.columns[colIdx].field;
            var row_data = grid.dataItem(row).toJSON();
            var data = null;

            if(Object.keys(row_data).length > 0){
                data = [{
                    id:row_data[self.rowId],
                    value: checked
                }];
            }
            
            var mappedEvent = self.getRegisteredColumnEvent(fieldName);
            if(!_.isNil(mappedEvent)) {
                if(mappedEvent.eventType === "click") {
                    mappedEvent.eventAction(data);
                }
            }

            // if check is all then input checkBoxAll is equal check else input checkBoxAll is equal uncheck
            var isCheck = $el.find("[data-grid='table-column-cell-checkbox']:nth-child(" + colCheckBox + ") input[type='checkbox']:checked").length;
            var checkBox = $el.find("[data-grid='table-column-cell-checkbox']:nth-child(" + colCheckBox + ") input[type='checkbox']").length;

            if(isCheck === checkBox){
                $el.find("th[data-index='" + colIdx + "'] input[type='checkbox']").prop("checked", true);
            }else{
                $el.find("th[data-index='" + colIdx + "'] input[type='checkbox']").prop("checked", false);
            }
        });

        //get all dataSource from dataGrid
        $el.on('blur', 'input', function (e) {
            self.currentDataSource(self.grid.dataSource.data().toJSON());
        });
        // }).on("mousedown", ".k-grid-cancel-changes", function (e) {
        //     self.grid.cancelChanges();
        //     self.currentDataSource(self.grid.dataSource.data().toJSON());
        // }); 

        //reload datagrid
        self.source.subscribe(function(dataSource) {
            for (var key in dataSource) {
                if (dataSource.hasOwnProperty(key)) {          
                    self.grid.dataSource.transport.options[key].url = dataSource[key][0];
                    self.grid.dataSource.transport.options[key].data = dataSource[key][1];
                }
            }
            //set default grid
            self.resetGrid();
            self.grid.dataSource.read();
        });

    }

    getApiDataSource(){

        //if want to create = create
        //if want to update = update
        //if want to delete = destroy

        var self = this;
        var initialLoad = true;
        var dataSource = [];
        var transport = {};

        if(Object.keys(self.source()).length > 0){
            for (var key in self.source()) {
                if (self.source().hasOwnProperty(key)) {
                    if(key == "read"){
                        transport[key] = {
                            url: self.source()[key][0],
                            data: self.source()[key][1],
                            method: (key == "update") ? "PUT" : "POST",
                            dataType: "json",
                            contentType: "application/json",
                            xhrFields: { withCredentials: true },
                        }
                    }else{
                        transport[key] = {
                            url: self.source()[key][0],
                            data: self.source()[key][1],
                            method: (key == "update") ? "PUT" : "POST",
                            dataType: "json",
                            contentType: "application/json",
                            xhrFields: { withCredentials: true },
                            complete: function(e) {
                                self.grid.dataSource.read();
                                if(e.status >= 300) {
                                    self.resetGrid(true);
                                }else{
                                    self.resetGrid();
                                }
                                
                            }
                        }
                    }
                }
            }
        }


        transport.parameterMap = function(options, operation) {
            // self.statusVisible(false);
            options.PageProperty = WebConfig.userSession.id;
            return JSON.stringify(options);
        }

        dataSource = new kendo.data.DataSource({
            transport: transport,
            error: function (e) {
                var formattedErrorMsg = Utility.stringFormat(e.xhr.responseJSON.message, e.xhr.responseJSON.dataDetails);
                self.displayError(formattedErrorMsg);
                self.resetGrid(true);
            },
            requestStart: function () {
                // if (initialLoad)
                    // kendo.ui.progress($("#" + self.id), true);
            },
            requestEnd: function () {
                // if(initialLoad)
                    // kendo.ui.progress($("#" + self.id), false);
                //     initialLoad = false;
            },
        //   batch: true,
            pageSize: (this.pageable) ? this.pageSize : 0,
            scrollable: true,
            serverPaging: true,
            serverSorting: true,
            serverGrouping: true,
            serverFiltering: true,
        //   autoSync: true,
            schema: {
                model: {
                    id: this.rowId,
                    fields: this.getSchemaFields(),
                },
                data: function(r) {
                    return r.items;
                },
                total: function(r) { 
                    return r.totalRecords;
                }
            }
        });

        if(Object.keys(self.source()).length == 0){
            dataSource.transport.read = self.onDatasourceRequestInternalRead;
            dataSource.transport.update = self.onDatasourceRequestInternalUpdate;
        }
        
        return dataSource;
    }

    getSchemaFields(){
        var fileds = {};
        this.columns().forEach(function(val) {
            fileds[val.data] = {
                type: Utility.extractParam((val.type == "text") ? "string" : val.type, "string"),
                editable: Utility.extractParam(val.editable, false),
                validation: Utility.extractParam(val.validation, {}),
                from: val.data
            };
        });
        return fileds;
    }

    getColumns(){
        var self = this;
        var columns = [];
        var colIndex = 0;
        if(self.showRowSelector){
            //if showRowSelector = true then set start colIndex = 1 and columnResizeIndex push column 0 (cannot resize) 
            colIndex = 1;
            self.columnResizeIndex.push({
                id:0
            });
        }

        this.columns().forEach(function(val) {
            //create column
            var column;

            //set column index for remove columnMenu
            if(val.columnMenu === false){
                self.columnMenuIndex.push({
                    id:colIndex
                });
            }
            if(val.resizable === false){
                self.columnResizeIndex.push({
                    id:colIndex
                });
            }

            colIndex++;

            //set column selector if type = rowselector
            if(val.type == "rowselector" && !self.showRowSelector){
                column = {
                    field: Utility.extractParam(val.data, ""),
                    headerTemplate: Utility.extractParam(val.headerTemplate, self.getTemplateCheckBox().headerTemplate),
                    template: function(data){ return self.getTemplateCheckBox(data, val.data).template; },
                    // template: self.getTemplateCheckBox().template,
                    title: Utility.extractPossibleI18NParam(val.title, ""),
                    width: Utility.extractParam(val.width, "40px"),
                    filterable: false,
                    sortable: false,
                    menu: false,
                    attributes: {
                        "data-grid": "table-column-cell-rowselector",
                    }
                };
            }else if(val.type == "checkbox"){
                var hTemplate = null;

                //check enableSelectAll
                if(val.enableSelectAll){
                    //if have headerTemplate render headerTemplate + checkbox else render checkbox
                    if(val.headerTemplate){
                        hTemplate = Utility.extractParam(val.headerTemplate + self.getTemplateCheckBox("","",val.type).headerTemplate, self.getTemplateCheckBox("","",val.type).headerTemplate);
                    }else{
                        hTemplate = Utility.extractParam(self.getTemplateCheckBox("","",val.type).headerTemplate, self.getTemplateCheckBox("","",val.type).headerTemplate);
                    }
                }else{
                    hTemplate = Utility.extractParam(val.headerTemplate, self.getTemplateCheckBox("","",val.type).headerTemplate)
                }

                column = {
                    field: Utility.extractParam(val.data, ""),
                    headerTemplate: hTemplate,
                    template: function(data){ return self.getTemplateCheckBox(data, val.data, val.type).template; },
                    title: Utility.extractPossibleI18NParam(val.title, ""),
                    width: Utility.extractParam(val.width, "40px"),
                    filterable: Utility.extractParam(val.filterable, true),
                    sortable: Utility.extractParam(val.sortable, true),
                    menu: Utility.extractParam(val.columnMenuItem, false),
                    attributes: {
                        "data-grid": "table-column-cell-checkbox",
                    }
                };
                self.registerColumnEvents(val.data, "click", val.onDataChanged );
            }else{

                // replace text in array command
                if(typeof val.command != "undefined" && val.command.length > 0){
                    _.forEach(val.command, (obj) => {
                        obj.text = Utility.extractPossibleI18NParam(obj.text, "");
                    });
                }

                column = {
                    command: Utility.extractParam(val.command),
                    field: Utility.extractParam(val.data),
                    headerTemplate: Utility.extractParam(val.headerTemplate, Utility.extractPossibleI18NParam(val.title, "")),
                    title: Utility.extractPossibleI18NParam(val.title, ""),
                    width: Utility.extractParam(val.width),
                    editor: Utility.extractParam(val.editor),
                    format: Utility.extractParam(val.format),
                    filterable: Utility.extractParam(val.filterable, true),
                    sortable: Utility.extractParam(val.sortable, true),
                    menu: Utility.extractParam(val.columnMenuItem, true),
                    hidden: Utility.extractParam(val.hidden, false),
                    //template for engine, movement, gps, gsm ...
                    template: (DataGridColumns.findFn(val.template).length === 1) ? function(data){ return DataGridColumns.getColumn(val.template, data[val.data]); } : val.template
                    
                };
            }

            //set thbody column attributes onclick
            switch (typeof val.onHeaderClick) {
                case "function" : {
                //set header column
                    column.headerAttributes = {
                        "data-grid": "table-column-cell-onclick",
                        class: " k-link"
                    }
                    self.registerColumnEvents(val.data+"Header", "click", val.onHeaderClick); 
                }
            }

            //set thbody column attributes onclick
            switch (typeof val.onClick) {
                case "function" : {

                    if(typeof val.className == "string"){

                        column.attributes = {
                            "data-grid": "table-column-cell-onclick",
                            class: val.className + " k-link"
                        }
                        self.registerColumnEvents(val.data, "click", val.onClick);

                    }else{
                        //set thbody column
                        column.attributes = {
                            "data-grid": "table-column-cell-onclick",
                            class: " k-link"
                        }
                        self.registerColumnEvents(val.data, "click", val.onClick);
                    }

                    break;
                }
            }
            
            //set column attributes class
            switch (typeof val.className) {
                case "string": {

                    //set thbody column attributes onclick
                    if (typeof val.onClick == "function") {

                        column.attributes = {
                            "data-grid": "table-column-cell-onclick",
                            class: val.className + " k-link"
                        }
                        self.registerColumnEvents(val.data, "click", val.onClick);

                    }else{

                        //set thbody column
                        column.attributes = {
                            class: val.className
                        }

                    }
                    
                    break;
                }
            }

            //set column attributes class
            switch (typeof val.editable) {
                case "boolean": {
                    //set thbody column
                    column.attributes = {
                        "data-editable": "true"
                    }
                }
            }

            if(val.columns){
                // for generate column merge
                column.columns = self.getColumnsMerge(val.columns);
            }

            columns.push(column);
        });

        //create check box
        if(self.showRowSelector){
            columns.unshift({
                field: "", 
                headerTemplate: self.getTemplateCheckBox().headerTemplate,
                template: self.getTemplateCheckBox().template,
                width: "40px",
                menu: false,
                attributes: {
                    "data-grid": "table-column-cell-rowselector",
                }
            });
        }
        
        return columns;
    }


    getColumnsMerge (obj = [])
    {
        var self = this;
        var columns = [];
        if(obj.length > 0){
            _.forEach(obj, function(val) {
                
                //create column
                var column;
                //set column selector if type = rowselector
                if(val.type == "rowselector" && !self.showRowSelector){
                    column = {
                        field: Utility.extractParam(val.data, ""),
                        headerTemplate: Utility.extractParam(val.headerTemplate, self.getTemplateCheckBox().headerTemplate),
                        template: function(data){ return self.getTemplateCheckBox(data, val.data).template; },
                        // template: self.getTemplateCheckBox().template,
                        title: Utility.extractPossibleI18NParam(val.title, ""),
                        width: Utility.extractParam(val.width, "40px"),
                        filterable: false,
                        sortable: false,
                        menu: false,
                        attributes: {
                            "data-grid": "table-column-cell-rowselector",
                        }
                    };
                }else if(val.type == "checkbox"){
                    var hTemplate = null;

                    //check enableSelectAll
                    if(val.enableSelectAll){
                        //if have headerTemplate render headerTemplate + checkbox else render checkbox
                        if(val.headerTemplate){
                            hTemplate = Utility.extractParam(val.headerTemplate + self.getTemplateCheckBox("","",val.type).headerTemplate, self.getTemplateCheckBox("","",val.type).headerTemplate);
                        }else{
                            hTemplate = Utility.extractParam(self.getTemplateCheckBox("","",val.type).headerTemplate, self.getTemplateCheckBox("","",val.type).headerTemplate);
                        }
                    }else{
                        hTemplate = Utility.extractParam(val.headerTemplate, self.getTemplateCheckBox("","",val.type).headerTemplate)
                    }

                    column = {
                        field: Utility.extractParam(val.data, ""),
                        headerTemplate: hTemplate,
                        template: function(data){ return self.getTemplateCheckBox(data, val.data, val.type).template; },
                        title: Utility.extractPossibleI18NParam(val.title, ""),
                        width: Utility.extractParam(val.width, "40px"),
                        filterable: Utility.extractParam(val.filterable, true),
                        sortable: Utility.extractParam(val.sortable, true),
                        menu: Utility.extractParam(val.columnMenuItem, false),
                        attributes: {
                            "data-grid": "table-column-cell-checkbox",
                        }
                    };
                    self.registerColumnEvents(val.data, "click", val.onDataChanged );
                }else{
                    
                    column = {
                        field: Utility.extractParam(val.data),
                        headerTemplate: Utility.extractParam(val.headerTemplate, Utility.extractPossibleI18NParam(val.title, "")),
                        title: Utility.extractPossibleI18NParam(val.title, ""),
                        width: Utility.extractParam(val.width),
                        editor: Utility.extractParam(val.editor),
                        format: Utility.extractParam(val.format),
                        filterable: Utility.extractParam(val.filterable, true),
                        sortable: Utility.extractParam(val.sortable, true),
                        menu: Utility.extractParam(val.columnMenuItem, true),
                        hidden: Utility.extractParam(val.hidden, false),
                        //template for engine, movement, gps, gsm ...
                        template: (DataGridColumns.findFn(val.template).length === 1) ? function(data){ return DataGridColumns.getColumn(val.template, data[val.data]); } : val.template
                        
                    };
                }
                
                //set thbody column attributes onclick
                switch (typeof val.onHeaderClick) {
                    case "function" : {
                    //set header column
                        column.headerAttributes = {
                            "data-grid": "table-column-cell-onclick",
                            class: " k-link"
                        }
                        self.registerColumnEvents(val.data+"Header", "click", val.onHeaderClick); 
                    }
                }

                //set thbody column attributes onclick
                switch (typeof val.onClick) {
                    case "function" : {
                        //set thbody column
                        column.attributes = {
                            "data-grid": "table-column-cell-onclick",
                            class: " k-link"
                        }
                        self.registerColumnEvents(val.data, "click", val.onClick);
                    }
                }
                
                //set column attributes class
                switch (typeof val.className) {
                    case "string": {
                        //set header column
                        column.headerAttributes = {
                            class: val.className
                        }
                        //set thbody column
                        column.attributes = {
                            class: val.className
                        }
                    }
                }
                

                if(val.columns){
                    // for generate column merge
                    column.columns = self.getColumnsMerge(val.columns);
                }

                columns.push(column);
            });
        }
        
        return columns;
    }
    
    onChangeDataGrid(e){

        // var grid = e.sender;
        // var selectRow = grid.select();

        // var dataItem = [];
        // selectRow.each(function(e) {
        //     dataItem.push(grid.dataItem(this).toJSON());
        // });

        // this.onSelectingRow(dataItem);
    }

    onDataBound(e)  
    {   
        var kendoGrid = e.sender;
        var $el = $("#" + this.id);
        //set autoFit Column 
        if(kendoGrid != null){
            var i = (this.showRowSelector) ? 1 : 0;
            for (i; i < this.columns().length; i++) {
                //check width if set width not set auto fit column
                if(!this.columns()[i].width){
                    kendoGrid.autoFitColumn(i);
                }
            } 
        }
        
        //set selected row if change page
        var isSelectedRow = (this.isSelectingRow()) ? this.isSelectingRow() : {};
        if(Object.keys(isSelectedRow).length){
            var data = kendoGrid.dataSource.data();
            var selectedRow = null;
            var selectedDataItem = null;
            for(var i = 0; i < data.length; i++){
                if(data[i][this.rowId] == isSelectedRow[this.rowId] && data[i][this.rowId] && isSelectedRow[this.rowId]){
                    var select = kendoGrid.tbody.find('tr[data-uid="' +  data[i].uid + '"]');
                    kendoGrid.select(select);
                    selectedRow = kendoGrid.select();
                    selectedDataItem = kendoGrid.dataItem(selectedRow);
                }
            }
        }

        // if check is all then input checkBoxAll is equal check else input checkBoxAll is equal uncheck
        var th = $el.find("th");
        var colIndex = [];
        //find column index checkbox in grid
        if(th.length > 0){
            _.forEach(th, (item) => {
                var checkbox = $(item).find("input[type='checkbox']");
                if(checkbox.length > 0){
                    colIndex.push($(item).attr('data-index'));
                }
            });
        }
        
        //set checkbox all
        if(colIndex.length > 0){
            var col = 0;
            _.forEach(colIndex, (index) => {
                col = parseInt(index) + 1;
                var isCheck = $el.find("td:nth-child(" + col + ") input[type='checkbox']:checked").length;
                var checkBox =  $el.find("td:nth-child(" + col + ") input[type='checkbox']").length;
                
                if(isCheck === checkBox && isCheck > 0){
                    $el.find("th[data-index='" + index + "'] input[type='checkbox']").prop("checked", true);
                }else{
                    $el.find("th[data-index='" + index + "'] input[type='checkbox']").prop("checked", false);
                }
            });
        }

        //for total record
        if(this.totalRecord)
        {
            $el.find("tr:last").find("td").css("font-weight","bold");
        }
        
        //set disable column menu
        this.disableColumnMenu(kendoGrid);
        //set disable column resize
        this.disableColumnResize(kendoGrid);

        //set disable column expand of data grid
        if(this.canExpandDataGrid){
            this.disableColumnExpand(kendoGrid);
        }
        
        //get all dataSource from dataGrid by event cancel, delete, addrow(row emty)
        this.currentDataSource(kendoGrid.dataSource.data().toJSON());

    }

    registerColumnEvents(columnId, eventType, eventAction) {
        // Lazy init 
        if(_.isNil(this.columnEventMap)) {
            this.columnEventMap = new Map();
        }

        this.columnEventMap.set(columnId + this.id, { 
            eventType: eventType,
            eventAction: eventAction
        });
    }

    getRegisteredColumnEvent(columnId) {
        if(_.isNil(this.columnEventMap) || _.isNil(columnId)) return null;
        return this.columnEventMap.get(columnId + this.id);
    }

        /**
     * Display popup status error
     * @public
     * @param {any} text
     */
    displayError(text) {
        this.statusText(this.ensureNonObservable(text));
        this.statusType("fail");
        this.statusVisible(true);
    }

    resetGrid(error = false){
        this.tempDataCheckBox([]);
        this.selectedRows([]);
        this.statusVisible(error);
  
        
    }

    disableColumnMenu(grid){
        //remove column menu if columnMenu eq false
        if(grid != null){
            _.forEach(this.columnMenuIndex(), (index) => {
                grid.thead.find("[data-index='" + index.id + "']").css("vertical-align", "middle");
                grid.thead.find("[data-index='" + index.id + "']>.k-header-column-menu").remove();
                grid.thead.find("[data-index='" + index.id + "']>.k-link").css("margin-right", 0);
            });
        }
    }

    disableColumnResize(grid){
        var self = this;
        if(grid != null){
            grid.resizable.bind("start", function(e) {
                _.forEach(self.columnResizeIndex(), (index) => {
                    if ($(e.currentTarget).data("th").data("index") == index.id) {
                        e.preventDefault();
                        setTimeout(function(){
                            grid.wrapper.removeClass("k-grid-column-resizing");
                            $(document.body).add("#" + self.id + " .k-grid th").css("cursor", "");
                            ("#" + self.id + " .k-grid th").css("cursor", "pointer");
                            ("#" + self.id + " .k-grid th>a").css("cursor", "pointer");
                        });
                    }
                });
            });
        }
    }

    disableColumnExpand(grid){
        var dataSource = grid.dataSource.data();
        var self = this;
        $("#" + this.id).find('tr.k-master-row').each(function() {
            let row = $(this);
            let data = _.find(dataSource, (item)=>{
                return item.uid == row.data('uid');
            });
            if(_.size(data)){
                let field = self.canExpandDataGrid;
                let enable = (data[field]) ? true : false;
                if(enable){
                    row.find("td a[aria-label='Expand']").remove();
                }
            }
        });
    }

    getCurrentDataGrid(){

        var fColumns = _.filter(this.rememberDataGridState, function(v){ return v == "columns" });
        if(fColumns.length > 0){
            //get option column default
            var columns = this.grid.options.columns;
            //find id in collection (user preference)
            var fDatagrid = _.find(this.dataGridCollection(), { id : this.id });
            if(fDatagrid != undefined){
                _.forEach(columns, function(value) {
                    //get column hide in collection and replace to default column
                    let fColumn = _.find(fDatagrid.columns, { field : value.field });
                    if(fColumn != undefined){
                        value.hidden =  fColumn.hidden;
                    }
                });
                //set hide column to default column
                this.grid.options.columns = columns;
                this.grid.destroy();
                $("#" + this.id).empty().kendoGrid(this.grid.options);
            }
        }
        
    }

    getHeight() {
        var kendoGrid = $("div#" + this.id);
        var height = 0;
        var paddingTop = 0;
        var paddingBottom = 10;
        if(kendoGrid.length > 0)
        {
            var bladeContent = kendoGrid.parents().closest("div.app-blade-content");
            if(bladeContent.lengthss === 0)
            {
                bladeContent = $("div.app-blade-content");
            }
            var topBladeContent = bladeContent.offset().top;
            var topKendoGrid = kendoGrid.offset().top;
            var distanceContentLayoutToKendoGrid = topKendoGrid - topBladeContent;
            
            var height = parseInt(bladeContent.height() - distanceContentLayoutToKendoGrid);
            
            //set parents padding bottom 0
            var componentContainer = kendoGrid.parents().closest("div.component-container");
            if(componentContainer.length > 0)
            {
                _.forEach(componentContainer, (container) => {
                    $(container).css("padding-bottom", "0");
                });
            }

            //if height < blade layout content/2 set grid height = blade layout content
            if(height < parseInt(bladeContent.height() / 2))
            {
                height = bladeContent.height();
            }

            // console.log("bladeContentLayout => " + bladeContent.height());
            // console.log("topContentLayout => " + topBladeContent);
            // console.log("topKendoGrid => " + topKendoGrid);
            // console.log("distanceContentLayoutToKendoGrid => " + distanceContentLayoutToKendoGrid);
            // console.log("height => " + height);
        }
        height = parseInt(height - paddingTop - paddingBottom);

        return height;
    }

    getTemplateCheckBox(data, field, type){
        var checkbox = {};
        var checked = "";
        var rowData = Utility.extractParam(data, []);
        var rowField = Utility.extractParam(field, "");
        var type = Utility.extractParam(type, "rowSelector");

        if(type == "rowSelector"){
            //add data to observable
            if(rowData[rowField]){
                checked = "checked";
                this.tempDataCheckBox.push(rowData.toJSON());
            }

            checkbox.headerTemplate = '\
            <div class="app-checkBox-contain-dt left" data-grid="isRowSelectorAll">\
                <label class="app-checkBox-control-dt control-checkbox-dt">\
                    <input type="checkbox" class="dt-row-selector" />\
                    <div class="app-control-dt-indicator app-input">' + checkboxCheckedTemplateMarkup + '</div>\
                </label>\
            </div>';

        }else{

            if(rowData[rowField]){
                checked = "checked";
            }

            checkbox.headerTemplate = '\
            <div class="app-checkBox-contain-dt left" data-grid="isCheckBoxAll">\
                <label class="app-checkBox-control-dt control-checkbox-dt">\
                    <input type="checkbox" class="dt-row-selector" />\
                    <div class="app-control-dt-indicator app-input">' + checkboxCheckedTemplateMarkup + '</div>\
                </label>\
            </div>';
            
        }

        checkbox.template = '\
        <div class="app-checkBox-contain-dt left">\
            <label class="app-checkBox-control-dt control-checkbox-dt">\
                <input type="checkbox" class="dt-row-selector"';
                if (data) {
                    checkbox.template += checked;
                }
                checkbox.template += ' />\
                <div class="app-control-dt-indicator app-input">' + checkboxCheckedTemplateMarkup + '</div>\
            </label>\
        </div>';
        
        return checkbox;
    }

    onUnload () {

        this.grid = null;
        this._eventAggregator.destroy();

        // Clear mapping event from screen, if any
        if(!_.isNil(this.columnEventMap)) {
            //IE 11 does not support for of loop.
            this.columnEventMap.forEach(function (item, key, mapObj) {
                item.eventAction = null;
            });
            this.columnEventMap = null;
        }

        var $dataGridNode = $('#' + this.id);
        if ($dataGridNode) {
            $dataGridNode.unbind();
        }
    }

    // Update Datatable Checkbox SelectAll base on options
    _dispatchEventUpdateCheckboxSelectAll(data = null) {
        if(data) {
            var extendData = $.extend({ columnIndex: 0, checked: false }, data);
            var $dataGridNode = $('#' + this.id);
            var $chk = $("th:eq(" + extendData.columnIndex + ") input:checkbox.dt-row-selector", $dataGridNode);
            $chk.prop("checked", extendData.checked);
            $chk.trigger("click");
        } 
    }
}

export default {
    viewModel: DataGridControl,
    template: templateMarkup
};
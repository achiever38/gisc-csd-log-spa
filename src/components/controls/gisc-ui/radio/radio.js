import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./radio.html";
/**
 * 
 * 
 * @class CheckBox
 * @extends {ControlBase}
 */
class Radio extends ControlBase {
    /**
     * Creates an instance of CheckBox.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.text = params.text;
        this.checked = this.ensureObservable(params.checked ,false);
        this.isRadioFocus = ko.observable();
        this.value = this.ensureObservable(params.value ,"giscValue");
        this.cssFocusClass = ko.pureComputed(()=> {
            // Your pure compute logic
            return this.isRadioFocus() ? 'focus' : '';
        });
    }

    onLoad() {}
    onUnload() {
        this.checked = null;
    }
}

export default {
    viewModel: Radio,
    template: templateMarkup
};
import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./textBox.html";
import "jquery-alphanum";
import Utility from "../../../../app/frameworks/core/Utility";

/**
 *
 *
 * @class TextBox
 * @extends {ControlBase}
 */
class TextBox extends ControlBase {
    /**
     * Creates an instance of TextBox.
     *
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.placeholder = this.ensureObservable(params.placeholder);
        this.value = this.ensureObservable(params.value);
        this.type = this.ensureNonObservable(params.type, "text");
        this.maxlength = this.ensureObservable(params.maxlength, 100);
        this.filter = this.ensureNonObservable(params.filter);
        this.immediateValueUpdate = this.ensureNonObservable(params.immediateValueUpdate, false);
        this.addDblQuote = this.ensureNonObservable(params.addDblQuote);

        this.watchChange = this.ensureNonObservable(params.watchChange, false);

        this.staticVal = this.ensureNonObservable(params.value);
        
    }
    /**
     * 
     * 
     * @param {any} element
     * 
     * @memberOf TextBox
     */
    afterRender(element) {
        var self = this;
        var $el = $("#" + self.id);

        if (this.watchChange) {

            this.value.subscribe(function (newValue) {

                if (newValue !== self.staticVal) {
                    $el.addClass("watch-change");
                }
                else {
                    $el.removeClass("watch-change");
                }
            });
        }


        var allowPlus, allowMinus, allowThouSep, allowDecSep;

        if (this.filter) {
            switch (this.filter.type) {
                case 'numeric':
                    if (this.filter.numericType === "int") {
                        allowPlus = false;
                        allowThouSep = false;
                        allowDecSep = false;
                    }

                    allowMinus = this.filter.allowNegative ? true : false;
                    $el.numeric({
                        allowPlus: allowPlus,
                        allowMinus: allowMinus,
                        allowThouSep: allowThouSep,
                        allowDecSep: allowDecSep,
                        maxDecimalPlaces: this.filter.maxDecimalPlaces,
                        maxPreDecimalPlaces: this.filter.maxPreDecimalPlaces,
                        max: this.filter.max,
                        min: this.filter.min
                    });

                    this._valueSubscribe = this.value.subscribe((newValue) => {

                        if (newValue) {
                            var n = Number(newValue);

                            // Compare minimum value.
                            if (n < this.filter.min) {
                                n = this.filter.min;
                            }

                            // Apply format if user setting padding digits.
                            if(this.filter.maxPrefixDigits) {
                                n = Utility.padDigits(n, this.filter.maxPrefixDigits);
                            }

                            this.value(n);
                        }
                    });
                    break;

                case 'alphanumeric':
                    $el.alphanum({
                        allowSpace: false
                    });
                    break;
                case 'regex':
                    var pattern = this.filter.pattern;
                    if(this.addDblQuote)
                    {
                        let tmp = [...pattern];
                        tmp.splice(tmp.length - 1,0,"\"");
                        pattern = tmp.join('');
    
                    }

                    var regex = new RegExp(pattern);
                    $el.bind('input', function (event) {
                        var latestValue = $(this).val();

                        if(!_.isEmpty(latestValue)){
                            // Loop through chars for simulate limit keypress with regular expression.
                            var correctChars = "";

                            _.each(latestValue, (c, i) => {
                                if (regex.test(c)) {
                                    correctChars += c;
                                }
                            });

                            // Update correct value to textbox, invalid chars will be removed.
                            $(this).val(correctChars);
                        }
                        return false;
                    });

                    break;
            }
        }
        
        //To set value in case user use  Copy and Paste.
        $el.blur(() => {
            if (this.value() != $el.val()) {
                this.value($el.val());
            }
        });
    }

    onUnload() {
        var $el = $("#" + this.id);
        $el.unbind();
        this.placeholder = null;
        this.value = null;
        this.maxlength = null;
    }
}

export default {
viewModel: TextBox,
    template: templateMarkup
};
import ko from "knockout";
import templateMarkup from "text!./chartBar.html";
import ControlBase from "../../controlbase";
import ChartJs from "chart";


class ChartBar extends ControlBase {
    constructor(params) {
        super(params);
         // Properties

        this.data = this.ensureObservable(params.data);
        this.valueField = this.ensureNonObservable(params.value);
        this.colorField = this.ensureNonObservable(params.color);

        this.maxScale =  ko.pureComputed(()=> {
            var sum = 0;
            _.forEach(this.data(), (v)=> {
                var value = this.ensureNonObservable(v[this.valueField]);
                sum = sum+value;
            });
            return sum;
        });

        this.option = {
            responsive: true, 
            maintainAspectRatio: false,
            tooltips: {
                enabled: false
            },
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero: true,
                        fontSize:11,
                        max: this.maxScale(),
                    },
                    display:false,
                    stacked: true
                }],
                yAxes: [{
                    categoryPercentage: 1.0,
                    display:false,
                    stacked: true
                }]
            },
            legend:{
                display:false
            }
        };


        this.chartInstance = null;
    }

    afterRender(element) {
        this.createChart(this.data());
    }

    createChart(val) {
        var self = this;
        var ctx = $("#" + self.id);

        try {

             if (!this.chartInstance) {
                this.chartInstance = new ChartJs(ctx, {
                    type: 'horizontalBar',
                    data: {
                        labels: ["a"],
                        datasets: []
                    },
                    options: this.option
                });
            }
            _.forEach(val, (v)=> {       
                var value = this.ensureNonObservable(v[this.valueField]);
                var color = this.ensureNonObservable(v[this.colorField]);

                var obj = {
                    data: [value],
                    backgroundColor: [color]
                }
                this.chartInstance.data.datasets.push(obj);
                
            });
            this.chartInstance.update();
            
        } catch (error) {
            
        }
    }

    onUnload() {
        this.chartInstance.destroy();
        this.data = null;
    }
}

export default {
    viewModel: ChartBar,
    template: templateMarkup
};

﻿import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./shipmentGrid.html";
import Logger from "../../../../app/frameworks/core/logger";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import { Enums } from "../../../../app/frameworks/constant/apiConstant";
import * as BladeDialog from "../../../../app/frameworks/constant/bladeDialog";
import DefaultSorting from "../../../../app/frameworks/constant/defaultSorting";
import WebRequestEnumResource from "../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestShipment from "../../../../app/frameworks/data/apitrackingcore/webRequestShipment";
import ScreenHelper from "../../../portals/screenhelper";
import moment from "moment";
import EventAggregator from "../../../../app/frameworks/core/eventAggregator";

class ShipmentGridControl extends ControlBase {
   
    constructor(params) {
        super(params);
        this.isShipmentTemplateChange = ko.observable(params.isShipmentTemplateChange);

        this.mode = this.ensureObservable(params.mode);
        this.data = this.ensureObservable(params.data);
        this._data = ko.observableArray(_.clone(this.data()));
        this.typedatagrid = this.ensureObservable(params.typedatagrid);
        this.isVisible = ko.observable(false);
        this.deliveryTypeList = this.ensureObservable(params.deliveryTypeList);
        this.startDateTime = this.ensureObservable(params.startDateTime);
        this.jobDetail = this.ensureObservable(params.jobDetail);
        this.autoCompleteList = this.ensureFunction(params.autoCompleteList, null);
        this.routeData = this.ensureFunction(params.routeData, null);
        this.getShipmentJobWaypointInfoLst = this.ensureFunction(params.getShipmentJobWaypointInfoLst);
        this.viewOnMap = this.ensureFunction(params.viewOnMap, function () {});
        this.defaultValue = this.ensureObservable(params.defaultValue);
        this.isShipmentGridEdit = this.ensureObservable(params.isShipmentGridEdit);
        this.shipmentGridChange = this.ensureObservable(params.shipmentGridChange);
        this.isFindBestSequenceLoading = this.ensureObservable(params.isFindBestSequenceLoading, false)
        this.isLatLonDup = ko.observable(false);
        this.isDup1 = ko.observable(false);
        this.isDup2 = ko.observable(false);
        this.routeInfosLst = ko.observableArray([]);
        this.lstUpdateData = ko.observableArray([]);
        this.totalWayPoint = ko.observable(0);
        this.bladeId = this.ensureObservable(params.bladeId);
        this.textViewOnMap = this.i18n("Common_ViewOnMap")();
        this.textFindBestSequence = this.i18n("Common_FindBestSequence")();
        this.businessUnit = this.ensureObservable(params.businessUnit);
        this.selectedBusinessUnit = ko.observable();
        this.enableWaypoint = ko.observable(true);
        this.selfControl = this.ensureObservable(params.selfControl);
        this.selfControl(this);
        this.tempJobDetail = _.clone(this.jobDetail());

        this.startShipmentDepart = ko.observable(Enums.ModelData.AccessType.Depart);
        this.startShipmentArrival = ko.observable(Enums.ModelData.AccessType.Arrival);
        this.isRadioStartShipment = ko.observable(Enums.ModelData.AccessType.Depart);

        this.isLastWaypoint = ko.observable(false);

        this.closeShipmentArrival = ko.observable(Enums.ModelData.AccessType.Arrival);
        this.closeShipmentDepart = ko.observable(Enums.ModelData.AccessType.Depart);

        this.startShipmentAccessType = ko.observableArray([]);
        this.selectedStartShipmentAccessType = ko.observable();

        this.closeShipmentAccessType = ko.observableArray([]);
        this.selectedCloseShipmentAccessType = ko.observable();

        this.startShipmentDepart = ko.observable(Enums.ModelData.AccessType.Depart);
        this.startShipmentArrival = ko.observable(Enums.ModelData.AccessType.Arrival);
        this.selectedStartShipmentAccessType = ko.observable(Enums.ModelData.AccessType.Depart);

        this.isLastWaypoint = ko.observable(false);

        this.closeShipmentArrival = ko.observable(Enums.ModelData.AccessType.Arrival);
        this.closeShipmentDepart = ko.observable(Enums.ModelData.AccessType.Depart);
        this.selectedCloseShipmentAccessType = ko.observable(Enums.ModelData.AccessType.Arrival);

        this.isRadioTimeCalculation = ko.observable("Auto");
        this.isDateTimePicker = ko.observable(false);
        this.tblRowDateTime = ko.observable("80px");
        this.enableBestSequence = ko.observable(false);
        this._eventAggregator = new EventAggregator();

        this.showTbl = ko.observable(true);
        this._startDateTime = ko.observable(this.startDateTime());

        this.getAutoCompleteList = (request, response) => {
            
            var filter = {
                companyId: WebConfig.userSession.currentCompanyId,
                businessUnitId: this.selectedBusinessUnit(),
                name: request.term
            }
            
            this.autoCompleteList(filter, response)
        }

        this.getDataRoute = (filter, response) => {
            this.routeData(filter, response);
        }

        this.isLastWaypoint.subscribe((state) => {

            let model = Object.assign({}, this.jobDetail());
            model.isTerminal = state;
            // model.closeJob = Enums.ModelData.AccessType.Arrival;
            this.jobDetail(model);
            if(state){
                this.selectedCloseShipmentAccessType(ScreenHelper.findOptionByProperty(this.closeShipmentAccessType, "value", Enums.ModelData.AccessType.Arrival));
            }
        })

        this.startDateTime.subscribe((dateTime) => {

            this._startDateTime(dateTime);
            this.displayDateTime()
        });
        
        if (this.mode() == "update") {
            this.lstUpdateData.replaceAll(params.data());
            this.cancel();
            this.isLastWaypoint(this.jobDetail().isTerminal);
            this.showTbl(true);
            if(_.size(params.data()) > 3){
                this.enableBestSequence(true);
            }else{
                this.enableBestSequence(false);
            }
        } else {
            if (this.data()['length'] == 0) {
                let newData = new Array();
                for (var i = 0; i < 2; i++) {
                    newData.push(this.createShipmentJobWaypointInfo(i));
                }
                this.data(newData);
                this.displayDateTime();
            }
        }  
        
        if(this.typedatagrid() == 'template'){
            this.isVisible(false)
        }
        
        //set default bu
        if (this.businessUnit() && this.isShipmentGridEdit()){
            this.enableWaypoint(true);
            this.selectedBusinessUnit(this.businessUnit());
        }

        //subscribe bu from page
        this.businessUnit.subscribe((data)=>{
            this.mode("changeBusinessUnit");
            //this.clearData();
            if(data){
                //this.enableWaypoint(true);
                if(data.value){
                    this.selectedBusinessUnit(data.value);
                }else{  
                    this.selectedBusinessUnit(data);
                }
            }else{
                //this.enableWaypoint(false);
                this.selectedBusinessUnit(null);
            }
        });
        
        this.selectedStartShipmentAccessType.ignorePokeSubscribe((state) => {
            let model = Object.assign({}, this.jobDetail());
            if(state){
                model.startJob = state.value
            }
            let index = 0;
            let dataLst = this.data()[index];
            this.displayDateTime();
            this.shipmentGridChange(true);

            this.jobDetail(model);
            this.updateList();
        })

        this.selectedCloseShipmentAccessType.ignorePokeSubscribe((state) => {
            let model = Object.assign({}, this.jobDetail());
            if(state){
                model.closeJob = state.value;
                if(state.value == Enums.ModelData.AccessType.Depart){
                    this.isLastWaypoint(false);
                }
            }
            let index = this.data()['length'] - 1;
            let dataLst = this.data()[index];

            this.displayDateTime();
            this.shipmentGridChange(true);

            this.jobDetail(model);
            this.updateList();
        });

        this.isRadioTimeCalculation.subscribe((data)=>{
            if(data=="Custom"){
                this.tblRowDateTime("150px");
                this.isDateTimePicker(true);
                this.enableBestSequence(false);
            }else{
                this.tblRowDateTime("80px");
                this.isDateTimePicker(false);
                this.onAutoCalcWaypoint();
                if(_.size(this.data()) > 3){
                    this.enableBestSequence(true);
                }else{
                    this.enableBestSequence(false);
                }
            }
            this.displayDateTime();
        });

        this._eventAggregator.subscribe("gisc-ui-shipment-grid-confirm-dialog-find", (eventObj) => {
            if(eventObj.eventName == "find"){
                this.isFindBestSequenceLoading(true);
                let dt = this.setObjDateTime(this.startDateTime().date, this.startDateTime().time);
                let filter = {
                    planStart: dt,
                    routeOption: eventObj.message.routeOption,
                    preserveFirstStop: eventObj.message.preserveFirstStop,
                    preserveLastStop: eventObj.message.preserveLastStop,
                    jobWayPointInfos: this.data()
                }
                this.webRequestShipment.findBestSequence(filter).done((res)=>{
                    var list = new Array();
                    _.forEach(res, (item, index)=>{
                        item.arrivalId = "arrival" + index;
                        item.departId = "depart" + index;
                        list.push(this.setPropertyForUpdate(item, index));
                    });
                    this.data.replaceAll(list);
                    // this.setObserLst();
                    this.displayDateTime();
                    this.isFindBestSequenceLoading(false);
                }).fail((err)=>{
                    console.log(err);
                    this.isFindBestSequenceLoading(false);
                });
            }
        });

    }

    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * Get WebRequest specific for Shipment module in Web API access.
     * @readonly
     */
    get webRequestShipment() {
        return WebRequestShipment.getInstance();
    }

    afterRender(element) {
       
        $(".forDisabled").click((e)=> {
            this.disabledHeaderBlade();
        });
        let self = this;

        var dfd = $.Deferred();
        let accessTypeFilter = { 
            companyId: WebConfig.userSession.currentCompanyId, 
            types: Enums.ModelData.EnumResourceType.AccessType,
            sortingColumns: DefaultSorting.EnumResource
        }
        let d1 = this.webRequestEnumResource.listEnumResource(accessTypeFilter);
        $.when(d1).done((r1) => {
            this.startShipmentAccessType(r1.items);
            this.closeShipmentAccessType(r1.items);
            //select by default
            this.selectedStartShipmentAccessType(ScreenHelper.findOptionByProperty(this.startShipmentAccessType, "value", this.tempJobDetail.startJob));
            this.selectedCloseShipmentAccessType(ScreenHelper.findOptionByProperty(this.closeShipmentAccessType, "value", this.tempJobDetail.closeJob));

            this.setDefaultDeliveryType();
            
            // this.data.subscribe((params)=>{
                
            //     this.displayDateTime();
            //     // this.setPropertyForUpdate(params);
            //     // this.updateList(0);
            //     this.shipmentGridChange(true);
            // });
            dfd.resolve();
        });

        //for update data
        this.data.subscribe((res)=>{
            this.displayDateTime();
            this.setPropertyForUpdate(res);
            this.updateList();
            this.shipmentGridChange(true);
            if(_.size(res) > 3){
                this.enableBestSequence(true);
            }else{
                this.enableBestSequence(false);
            }
        });

        return dfd;

    }
    
    onAutoCalcWaypoint(){
        _.forEach(this.data(), (item, index)=>{
            this.checkCallWaypoint(item, 1);
            this.checkCallWaypoint(this.data()[index + 1], 2);
        });
    }

    onUnload() {
    }

    disabledHeaderBlade() {
        //var bladeId = "#" + this.bladeId();
        //var elem = $(bladeId + " a.app-commandBar-item");
        //for (let index = 0; index < elem.length; index++) {
        //    let title = elem[index].title;
        //    switch (title) {
        //        case this.i18n('Transportation_Mngm_Shipment_Finish')():
        //        case this.i18n('Common_Cancel')():
        //            $(elem[index]).removeClass("app-has-hover");
        //            $(elem[index]).attr("style", "pointer-events: none;color:#dbdbdb;");
        //            $(elem[index].children["0"].children).attr("style", "fill:#dbdbdb;");
        //            break;
        //        default:
        //            break;
        //    }
        //}
    }

    setDefaultDeliveryType(){
        _.map(this.data(), (item, index)=>{
            if(item.deliveryType === undefined){
                if(index === 0){
                    item.deliveryType = Enums.ModelData.DeliveryType.Pickup;
                }else{
                    item.deliveryType = Enums.ModelData.DeliveryType.Deliver;
                }
            }
            return item;
        });
    }

    addRow(params) {

        let isValidate = false;
        
        for(let i = 0 ;i < this.data().length ; i++){

            let lst = this.data()[i];

            if (lst.waypointVal() == "" ||
                lst.waypointVal() == null) {
                isValidate = true
                lst.waypointVal("ticker")
                lst.waypointVal(null)
            }

        }
        // for (let i in this.data()) {
            
        //     let lst = this.data()[i]

        //     if (lst.waypointVal() == "" ||
        //         lst.waypointVal() == null) {
        //         isValidate = true
        //         lst.waypointVal("ticker")
        //         lst.waypointVal(null)
        //     }
        // }

        if (!isValidate) {
            
            let lastInd = this.data().length - 1;
            let currData = this.data();
            let newData = currData;
            newData.push(this.createShipmentJobWaypointInfo(lastInd + 1));
            this.data(newData);
            this.setDefaultDeliveryType();
            // if(this.data()[lastInd - 1].stop() == 0) {
                
            //     this.data()[lastInd - 1].stop(this.defaultValue().deliveryTime)
            // }
        }

    }

    cancel() {
        let wayPoint = _.filter(this.data(), (obj)=>{
            return obj.waypointVal && obj.waypointVal();
        });

        var oWayPoint = new Array();
        var nWayPoint = new Array();

        //old waypoint Val
        _.forEach(this._data(), (item)=>{
            if(item){
                oWayPoint.push(item.jobWaypointName);
            }
        });

        //new waypoint Val
        _.forEach(this.data(), (item)=>{
            if(item.waypointVal && item.waypointVal()){
                nWayPoint.push(item.waypointVal());
            }
        });

        //compare old waypoint and new waypoint
        let cp = _.isEqual(oWayPoint, nWayPoint);
        let changeWayPoint = true;
        
        if(!cp && _.size(wayPoint)){
            changeWayPoint = true;
        }else{
            changeWayPoint = false;
        }
        
        if(changeWayPoint){
            this.showMessageBox(null, this.i18n("M245")(), BladeDialog.DIALOG_YESNO).done((button) => {
                switch(button) {
                    case BladeDialog.BUTTON_YES:
                        this.clearData();
                        break;
                }
            });
        }else{
            this.clearData();
        }
        
    }

    clearData(){
        this.data.replaceAll([]);
        if (this.mode() == 'create') {
            let newData = new Array();
            for (var i = 0; i < 2; i++) {
                newData.push(this.createShipmentJobWaypointInfo(i));
            }
            this.data(newData);
        }else if(this.mode() == 'changeBusinessUnit'){
            let newData = new Array();
            for (var i = 0; i < 2; i++) {
                newData.push(this.createShipmentJobWaypointInfo(i));
            }
            this.data(newData);
        } else {
            let lst = [];
            this.totalWayPoint(this.lstUpdateData().length);
            for (let i in this.lstUpdateData()) {
                lst.push(this.setPropertyForUpdate(this.lstUpdateData()[i], i));
            }
            
            this.data.replaceAll(lst.slice());
            this.setObserLst();
            this.displayDateTime();
        }
        this.isLatLonDup(false);
        this.setDefaultDeliveryType();
    }

    genereateId(str, id) {
        return `${str}${id}`;
    }

    onDeleteRow(index) {
        if (this.isShipmentGridEdit()) {
            if (this.data().length > 2) {
                let lst = this.data.splice(index, 1);
                if(index != 0) {
                    this.isDup1(false);
                    this.isDup2(false);
                    this.checkCallWaypoint(this.data()[index], 1);
                    this.checkCallWaypoint(this.data()[index + 1], 2);
                }
                this.updateList();
            }
        } else {}
    }

    createShipmentJobWaypointInfo(index = 0) {
        var items = this.data();
        var emptyShipment = {
            indexLst: ko.observable(null),
            stop: ko.observable(0),
            travel: ko.observable(0),
            distance: ko.observable(0),
            radius: this.defaultValue().radius,
            radiusDisplay: ko.observable(this.defaultValue().radius),
            minPark: ko.observable(this.defaultValue().insideWaypointMinTime),
            maxPark: ko.observable(this.defaultValue().insideWaypointMaxTime),
            waypoint: ko.observable(null),
            waypointVal: ko.observable(null),
            requestWaypoint: this.getAutoCompleteList,
            dType: ko.observable(null),
            arrival: ko.observable("-"),
            depart: ko.observable("-"),
            isStartArrival: ko.observable(false),
            isEnabled: ko.observable(true),
            jobWaypointRouteInfos: [],
            jobWaypointStatus: 1,
            planStopTime: 0,
            planDriveTime: 0,
            planDistance: 0,
            shipmentToAddressDisplay: ko.observable("-"),
            customerAddress: "",
            poiCheckInType: 2,
            infoStatus: Enums.InfoStatus.Add,
            imgUrl: this.resolveUrl("~/images/icon_customroute_remove.svg"),
            displayTotalDOs: '-',
            jobwayPointStatus: ko.observable('-'),
            dTypeId: null,
            arrivalId: ko.observable("arrival" + index),
            departId: ko.observable("depart" + index),
            arrivalMinDate: this.startDateTime().date,
            arrivalEnable: ko.observable(true),
            departEnable: ko.observable(true),
            tempPlanStopTime: 0,
        };

        emptyShipment.waypointVal.extend({
            required: true
        });
        emptyShipment.stop.extend({
            min: {
                params: 0,
                message: this.i18n("M138")(),
                onlyIf: function () {
                    return emptyShipment.stop() < 0;
                },
            },
            required: true
        });
        emptyShipment.travel.extend({
            min: {
                params: 0,
                message: this.i18n("M138")(),
                onlyIf: function () {
                    return emptyShipment.travel() < 0;
                },
            },
            required: true
        });
        emptyShipment.distance.extend({
            required: true
        });
        emptyShipment.radiusDisplay.extend({
            required: true
        });

        emptyShipment.minPark.extend({
            max: {
                params: emptyShipment.maxPark,
                message: this.i18n("M242")()
            },
            required: true
        });

        emptyShipment.maxPark.extend({
            min: {
                params: emptyShipment.minPark,
                message: this.i18n("M248")()
            },
            required: true
        });

        // emptyShipment.arrival.extend({
        //     min: {
        //         params: 0,
        //         message: this.i18n("M138")(),
        //         onlyIf: function () {
        //             return emptyShipment.stop() < 0;
        //         },
        //     },
        //     required: true
        // });

        // emptyShipment.depart.extend({
        //     min: {
        //         params: 0,
        //         message: this.i18n("M138")(),
        //         onlyIf: function () {
        //             return emptyShipment.stop() < 0;
        //         },
        //     },
        //     required: true
        // });

        emptyShipment.radiusDisplay.extend({
            trackChange: true
        })

        emptyShipment.indexLst.subscribe((ind) => {
           
            // this.updateList(ind);
            this.displayDateTime();
            emptyShipment.dTypeId = ("dType" + ind);
        })

        emptyShipment.waypoint.subscribe((data) => {
            
            if (data != null) {
                
                emptyShipment.jobWaypointName = emptyShipment.waypoint().item.name;
                emptyShipment.jobWaypointCode = emptyShipment.waypoint().item.code;
                emptyShipment.customPoiId = emptyShipment.waypoint().item.id;
                emptyShipment.shipmentToAddressDisplay(emptyShipment.waypoint().item.locationInfo);
                emptyShipment.customerAddress = emptyShipment.waypoint().item.locationInfo;
                emptyShipment.insideWaypointMinTime = emptyShipment.minPark();
                emptyShipment.insideWaypointMaxTime = emptyShipment.maxPark();

                let radius = (emptyShipment.waypoint().item.radius) ? emptyShipment.waypoint().item.radius : this.defaultValue().radius;
                emptyShipment.radiusDisplay(radius);

                this.isDup1(false);
                this.isDup2(false);
                this.checkCallWaypoint(emptyShipment, 1);
                this.checkCallWaypoint(this.data()[emptyShipment.indexLst() + 1], 2);
            } else {
            
                emptyShipment.shipmentToAddressDisplay('-');
                emptyShipment.travel(0);
                emptyShipment.distance(0);
            }
        });

        emptyShipment.waypointVal.subscribe((val) => {
            
            if (val == "") {
                emptyShipment.waypoint(null)
            }
            this.shipmentGridChange(true);
        })

        emptyShipment.stop.subscribe((value) => {
           
          
            if (value == "") {
                emptyShipment.stop(0)
            }
            emptyShipment.planStopTime = emptyShipment.stop();
            this.displayDateTime();
            this.shipmentGridChange(true);
        })

        emptyShipment.travel.subscribe((value) => {
            
            if (value == "") {
                emptyShipment.travel(0)
            }
            emptyShipment.planDriveTime = emptyShipment.travel();
            this.displayDateTime();
            this.shipmentGridChange(true);
        })

        emptyShipment.distance.subscribe((value) => {
    
            if (value == "") {
                emptyShipment.distance(0)
            }
            emptyShipment.planDistance = emptyShipment.distance();
            this.displayDateTime();
            this.shipmentGridChange(true);
        })

        emptyShipment.radiusDisplay.subscribe((value) => {

            if (value == "") {
                emptyShipment.radiusDisplay(0)
            }
            emptyShipment.radius = emptyShipment.radiusDisplay();
            this.shipmentGridChange(true);
        })

        emptyShipment.minPark.subscribe((value) => {

            if (value == "") {
                emptyShipment.minPark(0)
            }
            
            emptyShipment.insideWaypointMinTime = emptyShipment.minPark();
            this.shipmentGridChange(true);
        });

        emptyShipment.maxPark.subscribe((value) => {
     
            if (value == "") {
                emptyShipment.maxPark(0)
            }
            emptyShipment.insideWaypointMaxTime = emptyShipment.maxPark();
            this.shipmentGridChange(true);
        });

        emptyShipment.dType.ignorePokeSubscribe((data) => {
            emptyShipment.deliveryType = 0;
            if(emptyShipment.dType() && emptyShipment.dType().value){
                emptyShipment.deliveryType = emptyShipment.dType().value;
            }
            
            if(emptyShipment.dTypeId){
                let rowNumber = emptyShipment.dTypeId.replace("dType", "");
                this.onChangeDeliveryType(rowNumber);
            }
            
            // emptyShipment.deliveryType = emptyShipment.dType() == undefined ? 0 : emptyShipment.dType().value;
        });

        emptyShipment.arrival.ignorePokeSubscribe((value) => {
            
            let rowNumber = emptyShipment.arrivalId().replace("arrival", "");
            if(parseInt(rowNumber)){
                let currData = this.data()[rowNumber];
                let prevData = this.data()[rowNumber - 1];
                let df = this.dateDiff(prevData.depart(), currData.arrival());
                prevData.travel(df.minutes);
            }
        });
        
        emptyShipment.depart.ignorePokeSubscribe((value) => {
            
            let rowNumber = emptyShipment.departId().replace("depart", "");
            
            if(this.selectedStartShipmentAccessType().value == Enums.ModelData.AccessType.Arrival || parseInt(rowNumber)){
                let currData = this.data()[rowNumber];
                let df = this.dateDiff(currData.arrival(), currData.depart());
                
                emptyShipment.stop(df.minutes);
            }
        });

        this.data[emptyShipment.indexLst] = emptyShipment
        this.shipmentGridChange(true);
       
        return emptyShipment;
    }

    updateList(dataLst, index) {
        
        var lastIndex = this.data()['length'] - 1;

        this.data().filter((item) => {
            if (this.data().indexOf(item) == 0) {
                item.isEnabled(true);
                item.arrivalEnable(false);
                
                if (item.deliveryType == undefined) {
                    let selected = this.deliveryTypeList().filter((lst) => {
                        if (lst.value == Enums.ModelData.DeliveryType.Pickup) {
                            return item
                        }
                    })
                    item.dType.poke(selected[0])
                } else {
                    let selected = this.deliveryTypeList().filter((lst) => {
                        if (lst.value == item.deliveryType) {
                            return item
                        }
                    })
                    item.dType.poke(selected[0])
                }
                
                if (this.jobDetail().startJob == Enums.ModelData.AccessType.Arrival) {
                    item.departEnable(true);
                }else{
                    item.departEnable(false);
                }

            } else {
                item.isEnabled(true);
                item.isStartArrival(true);
                item.departEnable(true);
                if (item.deliveryType == undefined) {
                    let selected = this.deliveryTypeList().filter((lst) => {
                        if (lst.value == Enums.ModelData.DeliveryType.Deliver) {
                            return item
                        }
                    })
                    item.dType.poke(selected[0])
                } else {
                    let selected = this.deliveryTypeList().filter((lst) => {
                        if (lst.value == item.deliveryType) {
                            return item
                        }
                    })
                    item.dType.poke(selected[0])
                }
            }

            if (this.data().indexOf(item) == 0 && (this.selectedStartShipmentAccessType() && this.selectedStartShipmentAccessType().value == Enums.ModelData.AccessType.Depart)) {
                item.stop(0);
                item.isStartArrival(false);
            }
            
            if (this.data().indexOf(item) == 0 && (this.selectedStartShipmentAccessType() && this.selectedStartShipmentAccessType().value == Enums.ModelData.AccessType.Arrival)) {
                item.isStartArrival(true);
                if(item.stop() == 0) {
                    var dType = item.dType() && item.dType().value;
                    switch(dType){
                        case Enums.ModelData.DeliveryType.Both:
                            item.stop(this.defaultValue().pickUpDeliveryTime);
                        break;
                        case Enums.ModelData.DeliveryType.Deliver:
                            item.stop(this.defaultValue().deliveryTime);
                        break;
                        case Enums.ModelData.DeliveryType.Pickup:
                            item.stop(this.defaultValue().pickUpTime);
                        break;
                        default:
                            item.stop(this.defaultValue().stopTime);
                        break;
                    }
                }
            }

            if (this.data().indexOf(item) == lastIndex) {
                item.isEnabled(false);
                item.isStartArrival(false);
                item.travel(0);
                item.distance(0);
                item.jobWaypointRouteInfos = [];
            }
            
            if (this.data().indexOf(item) == lastIndex && (this.selectedCloseShipmentAccessType() && this.selectedCloseShipmentAccessType().value == Enums.ModelData.AccessType.Arrival)) {
                item.isStartArrival(false);
                item.stop(0);
                // item.stop(item.tempPlanStopTime);
                item.departEnable(false);
            }

            if (this.data().indexOf(item) == lastIndex && (this.selectedCloseShipmentAccessType() && this.selectedCloseShipmentAccessType().value == Enums.ModelData.AccessType.Depart)) {
                item.isStartArrival(true);
                item.departEnable(true);

                if(item.stop() == 0) {
                    var dType = item.dType() && item.dType().value;

                    switch(dType){
                        case Enums.ModelData.DeliveryType.Both:
                            item.stop(this.defaultValue().pickUpDeliveryTime);
                        break;
                        case Enums.ModelData.DeliveryType.Deliver:
                            item.stop(this.defaultValue().deliveryTime);
                        break;
                        case Enums.ModelData.DeliveryType.Pickup:
                            item.stop(this.defaultValue().pickUpTime);
                        break;
                        default:
                            item.stop(this.defaultValue().stopTime);
                        break;
                    }
                }
            }

            if((this.data().indexOf(item) != 0 && this.data().indexOf(item) != lastIndex) && item.stop() == 0){
                item.stop(this.defaultValue().deliveryTime);
            }

            if (!this.isShipmentGridEdit()) {
                item.isEnabled(this.isShipmentGridEdit())
                item.isStartArrival(this.isShipmentGridEdit())
            }

            item.planStopTime = item.stop();
        })
    }

    displayDateTime() {    
        var objDateTime = this.setObjDateTime(this.startDateTime().date, this.startDateTime().time);
        var dataLst = this.data();
        var isDisplay = true;
       
        if(this.startDateTime().date == null || this.startDateTime().time == '') {
            isDisplay = false;
        } else {
            isDisplay = true;
        }
        dataLst.map((item) => {
            
            if (dataLst.indexOf(item) == 0) {
                item.arrivalDate = this.setObjDateTime(objDateTime)
                item.departDate = this.setObjDateTime(this.addMinutes(objDateTime, item.stop()))
                objDateTime = item.departDate;
                item.arrival.poke(!isDisplay ? "-" : this.setFormatTime(item.arrivalDate));
                item.depart.poke(!isDisplay ? "-" : this.setFormatTime(item.departDate));
                item.planIncomingDate = item.arrivalDate;
                item.planOutgoingDate = item.departDate;
            } else {
                let totalTimeToAdd = (parseInt(dataLst[dataLst.indexOf(item) - 1].travel()))
                item.arrivalDate = this.setObjDateTime(this.addMinutes(objDateTime, totalTimeToAdd));
                item.departDate = this.setObjDateTime(this.addMinutes(item.arrivalDate, item.stop()));
                objDateTime = item.departDate;
                item.arrival.poke(!isDisplay ? "-" : this.setFormatTime(item.arrivalDate));
                item.depart.poke(!isDisplay ? "-" : this.setFormatTime(item.departDate));
                item.planIncomingDate = item.arrivalDate;
                item.planOutgoingDate = item.departDate;
            }
        })
    }

    checkCallWaypoint(listDetail, times) {
        var lstData = this.data();
        if (listDetail != undefined &&
            listDetail.waypoint() != null) {
            var index = listDetail.indexLst();
            var filter = {};
            var startPoint = null;
            var endPoint = null;
            var routeData = null;
            var isUpdateRowIndex = null;
            var _startDateTime = this._startDateTime();
            var _startDate = new Date(_startDateTime.date);
            var sDate = _startDate.toDateString() + " " + _startDateTime.time;

            if (index == 0) {
                this.data()[index].lat = lstData[index].waypoint().item.latitude;
                this.data()[index].lon = lstData[index].waypoint().item.longitude;
            }

            if (lstData[index - 1] != undefined) { // match กับตัวด้านบน
                startPoint = lstData[index - 1].waypoint() == undefined ? null : lstData[index - 1].waypoint().item;
                endPoint = lstData[index].waypoint() == undefined ? null : lstData[index].waypoint().item;
                if (endPoint !== null) {

                    if(startPoint.id == endPoint.id) {
                        // this.isLatLonDup(true)
                        this.isLatLonDup(false)
                        return
                    }

                    this.data()[index].lat = endPoint.latitude;
                    this.data()[index].lon = endPoint.longitude;
                }
                isUpdateRowIndex = index - 1;
            } else if (lstData[index + 1] != undefined) { // match กับตัวด้านล่าง
                if (startPoint != null) {

                    if(startPoint.id == endPoint.id) {
                        // this.isLatLonDup(true)
                        this.isLatLonDup(false)
                        return
                    }

                    startPoint = lstData[index].waypoint() == undefined ? null : lstData[index].waypoint().item;
                    endPoint = lstData[index + 1].waypoint() == undefined ? null : lstData[index + 1].waypoint().item;
                    this.data()[index].lat = startPoint.latitude;
                    this.data()[index].lon = startPoint.longitude;
                    isUpdateRowIndex = index;
                }
            } else {}

            if (startPoint !== null &&
                endPoint !== null) {
                filter = {
                    stops: [{
                        name: startPoint.name,
                        lat: startPoint.latitude,
                        lon: startPoint.longitude
                    }, {
                        name: endPoint.name,
                        lat: endPoint.latitude,
                        lon: endPoint.longitude
                    }],
                    routeMode: 4,
                    routeOption: 1,
                    startDate: sDate,
                    lang: WebConfig.userSession.currentUserLanguage == "en-US" ? "E" : "L"
                };

                this.getDataRoute(filter, (res) => {
                    routeData = JSON.parse(res);
                    let routeLst = routeData.results.route[0];
                    let jobWaypointRouteInfos = [];
                    let travel = routeData.results.totalTime;
                    let distance = (routeData.results.totalLength / 1000);

                    this.data()[isUpdateRowIndex].travel(Math.round(travel));
                    this.data()[isUpdateRowIndex].distance(distance.toFixed(2));

                    if (routeLst != undefined) {
                        // Update Req ล่าสุด คือ ไม้ต้องส่ง Route ไปให้ Service แล้ว
                        //ใช้ข้อมูลนี้ในการวาด View on Map
                        for (let i = 0; i < routeLst.length; i++) {
                            jobWaypointRouteInfos.push({
                                longitude: routeLst[i][0],
                                latitude: routeLst[i][1]
                            })
                        }

                        this.data()[isUpdateRowIndex].jobWaypointRouteInfos = jobWaypointRouteInfos;
                        this.checkWaypointDup(times);
                    } else {
                        // this.isLatLonDup(true)
                        this.isLatLonDup(false)
                    }

                    this.data()[isUpdateRowIndex].isLatLonDup = this.isLatLonDup();
                    this.updateList();
                })
            } else {}
        }

        if (listDetail == undefined) {
            this.checkWaypointDup(times);
        }
    }

    checkWaypointDup(times) {
        this.isLatLonDup(false)
        let isDup = false;
        for (let index = 0; index < this.data().length - 1; index++) {
            let routeLst = this.data()[index].customPoiId;
            let nextRouteLst = this.data()[index + 1].customPoiId;
            if (routeLst == nextRouteLst) {
                isDup = true
            }
        }

        switch (times) {
            case 1:
                this.isDup1(isDup)
                break;
            case 2:
                this.isDup2(isDup)
                break;
            default:
                break;
        }

        if (this.isDup1() || this.isDup2()) {
            // this.isLatLonDup(true)
            this.isLatLonDup(false)
        } else {
            this.isLatLonDup(false)
        }
    }

    setObserLst() {
        for(let i=0 ;i<this.data().length;i++){
            let lst = this.data()[i];
            lst.indexLst = ko.observable(i);
            lst.dTypeId = 'dType' + lst.indexLst();
            
            //lst.requestWaypoint = this.getAutoCompleteList;
            lst.InfoStatus = Enums.InfoStatus.Original;

            lst.waypointVal.extend({
                required: true
            });
            lst.stop.extend({
                min: {
                    params: 0,
                    message: this.i18n("M138")(),
                    onlyIf: function () {
                        return lst.stop() < 0;
                    },
                },
                required: true
            });
            lst.travel.extend({
                min: {
                    params: 0,
                    message: this.i18n("M138")(),
                    onlyIf: function () {
                        return lst.travel() < 0;
                    },
                },
                required: true
            });
            lst.distance.extend({
                required: true
            });
            lst.radiusDisplay.extend({
                required: true
            });

            lst.radiusDisplay.extend({
                trackChange: true
            });

            lst.minPark.extend({
                max: {
                    params: lst.maxPark,
                    message: this.i18n("M242")()
                },
                required: true
            });
    
            lst.maxPark.extend({
                min: {
                    params: lst.minPark,
                    message: this.i18n("M248")()
                },
                required: true
            });

            lst.indexLst.subscribe((ind) => {
                // this.updateList(ind);
                this.displayDateTime();
            })

            lst.waypointVal.subscribe((val) => {

                if (val == "") {
                    lst.waypoint(null)
                }
                this.disabledHeaderBlade();
            })

            lst.waypoint.subscribe((data) => {
                
                if (data != null) {
                    lst.jobWaypointName = lst.waypoint().item.name;
                    lst.jobWaypointCode = lst.waypoint().item.code;
                    lst.customPoiId = lst.waypoint().item.id;
                    lst.shipmentToAddressDisplay(lst.waypoint().item.locationInfo);
                    lst.customerAddress = lst.waypoint().item.locationInfo;
                    lst.insideWaypointMinTime = lst.minPark();
                    lst.insideWaypointMaxTime = lst.maxPark();

                    let radius = (lst.waypoint().item.radius) ? lst.waypoint().item.radius : this.defaultValue().radius;
                    lst.radiusDisplay(radius);

                    this.isDup1(false);
                    this.isDup2(false);
                    this.checkCallWaypoint(lst, 1);
                    this.checkCallWaypoint(this.data()[(parseInt(i) + 1)], 2);
                } else {
                    lst.shipmentToAddressDisplay('-');
                    lst.travel(0);
                    lst.distance(0);
                }
                this.disabledHeaderBlade();
            })

            lst.stop.subscribe((value) => {

                if (value == "") {
                    lst.stop(0);
                }
                lst.planStopTime = lst.stop();
                this.displayDateTime();
                this.shipmentGridChange(true);
                this.disabledHeaderBlade();
            })

            lst.travel.subscribe((value) => {
          
                if (value == "") {
                    lst.travel(0);
                }
                lst.planDriveTime = lst.travel();
                this.displayDateTime();
                this.shipmentGridChange(true);
                this.disabledHeaderBlade();
            })

            lst.distance.subscribe((value) => {
   
                if (value == "") {
                    lst.distance(0);
                }
                lst.planDistance = lst.distance();
                this.displayDateTime();
                this.shipmentGridChange(true);
                this.disabledHeaderBlade();
            })

            lst.radiusDisplay.subscribe((value) => {
           
                if (value == "") {
                    lst.radiusDisplay(0);
                }
                lst.radius = lst.radiusDisplay();
                this.shipmentGridChange(true);
                this.disabledHeaderBlade();
            })

            lst.minPark.subscribe((value) => {
    
                if (value == "") {
                    lst.minPark(0);
                }
                lst.insideWaypointMinTime = lst.minPark();
                this.shipmentGridChange(true);
                this.disabledHeaderBlade();
            })

            lst.maxPark.subscribe((value) => {
              
                if (value == "") {
                    lst.maxPark(0);
                }
                lst.insideWaypointMaxTime = lst.maxPark();
                this.shipmentGridChange(true);
                this.disabledHeaderBlade();
            })

            // lst.dType.ignorePokeSubscribe((data) => {
            //     this.onChangeDeliveryType();
            //     lst.deliveryType = lst.dType() == undefined ? 0 : lst.dType().value;
            //     $("#" + lst.dTypeId).click(()=> {
            //         this.disabledHeaderBlade();
            //     });
            // })
            lst.dType.ignorePokeSubscribe((data) => {
                lst.deliveryType = 0;
                if(lst.dType() && lst.dType().value){
                    lst.deliveryType = lst.dType().value;
                }
                
                if(lst.dTypeId){
                    let rowNumber = lst.dTypeId.replace("dType", "");
                    this.onChangeDeliveryType(rowNumber);
                }
                
            });
    
            lst.arrival.ignorePokeSubscribe((value) => {
                
                let rowNumber = lst.arrivalId().replace("arrival", "");
                if(parseInt(rowNumber)){
                    let currData = this.data()[rowNumber];
                    let prevData = this.data()[rowNumber - 1];
                    let df = this.dateDiff(prevData.depart(), currData.arrival());
                    prevData.travel(df.minutes);
                }
            });
            
            lst.depart.ignorePokeSubscribe((value) => {
                
                let rowNumber = lst.departId().replace("depart", "");
                
                if(this.selectedStartShipmentAccessType().value == Enums.ModelData.AccessType.Arrival || parseInt(rowNumber)){
                    let currData = this.data()[rowNumber];
                    let df = this.dateDiff(currData.arrival(), currData.depart());
                    
                    lst.stop(df.minutes);
                }
            });
            this.data[i] = lst;
            this.shipmentGridChange(true);

        }
        
        this.updateList();
    }

    setPropertyForUpdate(data, index) {
        let isEnabled = true;
        let isStartArrival = true;
        let arrivalEnable = true;
        let lastIndex = this.totalWayPoint() - 1;

        if (index == 0) {
            arrivalEnable = false;
        }

        if (index == lastIndex) {
            isEnabled = false;
        }

        this.isLastWaypoint(this.jobDetail().isTerminal);

        if (this.jobDetail().startJob == Enums.ModelData.AccessType.Depart &&
            index == 0) {
            isStartArrival = false;
        }

        if (this.jobDetail().startJob == Enums.ModelData.AccessType.Arrival &&
            index == 0) {
            isStartArrival = true;
        }

        this.selectedStartShipmentAccessType(ScreenHelper.findOptionByProperty(this.startShipmentAccessType, "value", this.jobDetail().startJob));
        this.selectedCloseShipmentAccessType(ScreenHelper.findOptionByProperty(this.closeShipmentAccessType, "value", this.jobDetail().closeJob));


        data.indexLst = ko.observable(index);
        data.imgUrl = this.resolveUrl("~/images/icon_customroute_remove.svg");
        data.stop = ko.observable(data.planStopTime);
        data.travel = ko.observable(data.planDriveTime);
        data.distance = ko.observable(data.planDistance);
        data.radiusDisplay = ko.observable(data.radius);
        data.minPark = ko.observable(data.insideWaypointMinTime);
        data.maxPark = ko.observable(data.insideWaypointMaxTime);
        data.waypoint = ko.observable({
            item: {
                label: data.jobWaypointName,
                name: data.jobWaypointName,
                latitude: data.lat,
                longitude: data.lon,
                id:data.customPoiId
            }
        });
        // data.requestWaypoint = null;
        data.dType = ko.observable(data.deliveryType);
        data.arrival = ko.observable(data.formatArrivalDate);
        data.depart = ko.observable(data.formatDepartDate);
        data.isStartArrival = ko.observable(isStartArrival);
        data.isEnabled = ko.observable(isEnabled);
        data.shipmentToAddressDisplay = ko.observable(data.customerAddress);
        data.waypointVal = ko.observable(data.jobWaypointName);
        data.totalDOs = data.totalDOs;
        data.displayTotalDOs = data.totalDOs?data.totalDOs:null;
        data.jobwayPointStatus = ko.observable(data.jobWaypointStatusDisplayName);
        data.InfoStatus = Enums.InfoStatus.Update;
        data.assignOrder = data.assignOrder;
        data.requestWaypoint = this.getAutoCompleteList;
        data.dTypeId = null;
        data.arrivalEnable = ko.observable(arrivalEnable);
        data.departEnable = ko.observable(isStartArrival);
        data.arrivalId = ko.observable(data.arrivalId);
        data.departId = ko.observable(data.departId);
        data.arrivalMinDate = this.startDateTime().date;
        data.tempPlanStopTime = data.planStopTime;

        
        this.setObserLst();
        return data;
    }

    setFormatTime(objDateTime) {
 
        var newDateTime = "";
        if (this.isRadioTimeCalculation() == "Auto" && objDateTime) {
            let d = new Date(objDateTime);
            let year = d.getFullYear().toString();
            let month = (d.getMonth() + 1).toString();
            let day = d.getDate().toString();
            let hour = d.getHours().toString();
            let minute = d.getMinutes().toString();

            month = month.length > 1 ? month : "0" + month;
            day = day.length > 1 ? day : "0" + day;
            minute = minute.length > 1 ? minute : "0" + minute;
            newDateTime = day + "/" + month + "/" + year + "<br/>" + hour + ":" + minute;


        } else {
            // newDateTime = new Date(objDateTime);
            let d = new Date(objDateTime);
            let year = d.getFullYear().toString();
            let month = (d.getMonth() + 1).toString();
            let day = d.getDate().toString();
            let hour = d.getHours().toString();
            let minute = d.getMinutes().toString();

            month = month.length > 1 ? month : "0" + month;
            day = day.length > 1 ? day : "0" + day;
            minute = minute.length > 1 ? minute : "0" + minute;
            newDateTime = day + "/" + month + "/" + year + " " + hour + ":" + minute;
        }

        return newDateTime;
    }

    setObjDateTime(dateObj, timeObj) {
        var newDateTime = "";

        if (dateObj && timeObj) {
            let d = new Date(dateObj);
            let year = d.getFullYear().toString();
            let month = (d.getMonth() + 1).toString();
            let day = d.getDate().toString();

            month = month.length > 1 ? month : "0" + month;
            day = day.length > 1 ? day : "0" + day;
            newDateTime = year + "-" + month + "-" + day + "T" + timeObj + ":00";
        } else {
            let d = new Date(dateObj);
            let year = d.getFullYear().toString();
            let month = (d.getMonth() + 1).toString();
            let day = d.getDate().toString();
            let hour = d.getHours().toString();
            let minute = d.getMinutes().toString();

            month = month.length > 1 ? month : "0" + month;
            day = day.length > 1 ? day : "0" + day;
            hour = hour.length > 1 ? hour : "0" + hour;
            minute = minute.length > 1 ? minute : "0" + minute;
            newDateTime = year + "-" + month + "-" + day + "T" + hour + ":" + minute + ":00";
        }

        return newDateTime;
    }

    addMinutes(currentDate, minutes) {
    
        var result = new Date(currentDate);
        result.setMinutes(parseInt(result.getMinutes()) + (parseInt(minutes)));
        return result
    }

    minusMinutes(currentDate, minutes) {
      
        var result = new Date(currentDate);
        result.setMinutes(parseInt(result.getMinutes()) - parseInt(minutes));
        return result
    }

    get Constant() {

        return {
            ARRIVAL: 1,
            DEPART: 2
        }
    }

    onChangeDeliveryType(rowNumber) {
        
        // let stopT = (lst.planStopTime && firstLoad) ? lst.planStopTime : df.minutes;
        // lst.stop(stopT);
        // firstLoad = false;

        var dataLst = [this.data()[rowNumber]];
        _.forEach(dataLst, (item, index)=>{
            if(_.size(item)){
                let last = _.size(this.data()) - 1;
                let pickUpDeliveryTime = 0;
                let deliveryTime = 0;
                let pickUpTime = 0;
                let stopTime = 0;

                //first row
                if(rowNumber == 0 && (this.selectedStartShipmentAccessType() && this.selectedStartShipmentAccessType().value == Enums.ModelData.AccessType.Arrival)){
                    pickUpDeliveryTime = this.defaultValue().pickUpDeliveryTime;
                    deliveryTime = this.defaultValue().deliveryTime;
                    pickUpTime = this.defaultValue().pickUpTime;
                    stopTime = this.defaultValue().stopTime;
                }
                
                //last row
                if(rowNumber == last && (this.selectedCloseShipmentAccessType() && this.selectedCloseShipmentAccessType().value == Enums.ModelData.AccessType.Depart)){
                    pickUpDeliveryTime = this.defaultValue().pickUpDeliveryTime;
                    deliveryTime = this.defaultValue().deliveryTime;
                    pickUpTime = this.defaultValue().pickUpTime;
                    stopTime = this.defaultValue().stopTime;
                }

                //other row
                if(rowNumber != 0 && rowNumber != last){
                    pickUpDeliveryTime = this.defaultValue().pickUpDeliveryTime;
                    deliveryTime = this.defaultValue().deliveryTime;
                    pickUpTime = this.defaultValue().pickUpTime;
                    stopTime = this.defaultValue().stopTime;
                }
                
                var dType = item.dType() && item.dType().value;
                
                switch(dType){
                    case Enums.ModelData.DeliveryType.Both:
                        item.stop(pickUpDeliveryTime);
                    break;
                    case Enums.ModelData.DeliveryType.Deliver:
                        item.stop(deliveryTime);
                    break;
                    case Enums.ModelData.DeliveryType.Pickup:
                        item.stop(pickUpTime);
                    break;
                    default:
                        item.stop(stopTime);
                    break;
                }
            }
        });
    }

    dateDiff(sDate, eDate){
        let sDateTimeArray = sDate.split(" ");
        let sDateArray = sDateTimeArray[0].split("/");
        let eDateTimeArray = eDate.split(" ");
        let eDateArray = eDateTimeArray[0].split("/");
        
        let start = moment(new Date(sDateArray[2] + "/" + sDateArray[1] + "/" + sDateArray[0] + " " + sDateTimeArray[1])); //begin date
        let end = moment(new Date(eDateArray[2] + "/" + eDateArray[1] + "/" + eDateArray[0] + " " + eDateTimeArray[1])); // end date
        // let start = moment(new Date(sDate)); //begin date
        // let end = moment(new Date(eDate)); // end date
        var duration = {};
        duration.minutes = end.diff(start, 'minutes'); // 44700
        duration.hours = end.diff(start, 'hours') // 745
        duration.days = end.diff(start, 'days') // 31
        duration.weeks = end.diff(start, 'weeks') // 4
        
        return duration;
    }

    findBestSequence(){

        var isValidate = new Array();
        _.forEach(this.data(), (item)=>{
            if(!item.waypointVal() || !item.waypoint()){
                isValidate.push(true);
            }
        });

        let validationModelShipmentGrid = _.filter(isValidate, (val)=>{
            return val;
        });

        if(_.size(validationModelShipmentGrid) === 0){
            this.widget('gisc-ui-shipment-grid-confirm-dialog', null, {
                title: this.i18n("Transportation_Mngm_Shipment_Select_BestSequence")(),
                modal: true,
                resizable: false,
                minimize: false,
                target: "gisc-ui-shipment-grid-confirm-dialog",
                width: "320",
                height: "238",
                id: 'gisc-ui-shipment-grid-confirm-dialog',
                left: "40%",
                bottom: "35%"
            });
        }
    }
}

export default {
    viewModel: ShipmentGridControl,
    template: templateMarkup
};
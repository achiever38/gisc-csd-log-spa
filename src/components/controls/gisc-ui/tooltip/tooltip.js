import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./tooltip.html";

/**
 * 
 * 
 * @class Tooltip
 * @extends {ControlBase}
 */
class Tooltip extends ControlBase {

    /**
     * Creates an instance of Tooltip.
     * 
     * @param {any} params
     * 
     * @memberOf Tooltip
     */
    constructor(params) {
        super(params);
        this.text = this.ensureObservable(params.text, "");
        this.isSelected = ko.observable(false);

        this.instructionIcon = this.resolveUrl("~/images/ic-info.svg");

        //Add css styles to fixed position of Tooltip
        this.subscribeForIsSelected = this.isSelected.subscribe((b) => {
            var $el = $('#' + this.id);
            var offset = $el.offset();
            $el.find('.app-balloon').css({
                top: offset.top - 10,
                left: offset.left + 13
            });
        });
    }

    _onBladeScrollHandler() {

    }

    _onBladeScroll() {
        var $el = $("#" + this.id+">.app-balloon-icon");
        $el.blur();
    }

    /**
     * 
     * Detect mouse scroll event after DOM element is rendered to page.
     * 
     * @memberOf Tooltip
     */
    onDomReady() {
        
        this._onBladeScrollHandler = this._onBladeScroll.bind(this);
        //Detect mouse scroll event.
        $('.app-blade-content').on("scroll", this._onBladeScrollHandler);
        $('#main-panorama').on("scroll", this._onBladeScrollHandler);
    }

    onLoad(isFirstLoad) {}

    /**
     * 
     * Cleanup jquery event when dom is removed.
     * 
     * @memberOf Tooltip
     */
    onUnload() {
        var self = this;
        var $el = $("#" + self.id);
        $el.unbind();
        $('.app-blade-content').off("scroll", this._onBladeScrollHandler);
        $('#main-panorama').off("scroll", this._onBladeScrollHandler);

        this.text = null;
        this.isSelected = null;

    }
}

export default {
    viewModel: Tooltip,
    template: templateMarkup
};
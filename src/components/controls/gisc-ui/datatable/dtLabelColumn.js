import ko from "knockout";
import DTBaseColumn from './dtBaseColumn';
import Utility from "../../../../app/frameworks/core/utility";

/**
 * Label as DataTable Column
 * 
 * @class DTLabelColumn
 * @extends {DTBaseColumn}
 */
class DTLabelColumn extends DTBaseColumn {
    
    /**
     * Creates an instance of DTLabelColumn.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.cellTitle = Utility.extractParam(params.cellTitle);

        var self = this;
        this.render = function(data, type, row) {
            if(!_.isNil(self.cellTitle)) {
                return '<span title="' + row[self.cellTitle] + '">' + data + '</span>';
            }
            return data;
        }
    }
}

export default DTLabelColumn;
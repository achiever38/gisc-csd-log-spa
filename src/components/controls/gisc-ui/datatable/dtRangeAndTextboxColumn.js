import ko from "knockout";
import DTBaseColumn from './dtBaseColumn';
import Utility from "../../../../app/frameworks/core/utility";

/**
 * Editable range text box as DataTable column
 * 
 * @class DTRangeColumn
 * @extends {DTBaseColumn}
 */
class DTRangeColumn extends DTBaseColumn {

    /**
     * Creates an instance of DTRangeColumn.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        // Specific property.
        var minProp = params.dataMin;
        var maxProp = params.dataMax;
        var rangeEnd = params.rangeEnd;
        var rangeBegin = params.rangeBegin;
        var validationData = params.validationData;
        var disableSpan = params.disableSpan;
        var maxLengthVal = params.maxlength;
        var isMorethanEqual = params.isMorethanEqual;
        // Must define render function here instead of ES6 public method.
        var self = this;
        this.render = function(data, type, row) {
            // Extract min-max value.
            var minVal = Utility.getProperty(row, minProp);
            var maxVal = Utility.getProperty(row, maxProp);
            var txtBoxValBegin = Utility.getProperty(row, rangeBegin);
            var txtBoxValEnd = Utility.getProperty(row, rangeEnd);
            var disableSpanVal = Utility.getProperty(row, disableSpan);
            let spanBegin = '';
            let spanEnd = '';
            if (txtBoxValBegin != undefined && txtBoxValEnd == undefined) {

                if (!disableSpanVal) {
                    spanBegin = '<span style="font-size: larger;margin-left:-15px"> >= </span>';
                    spanEnd = '<span style="font-size: larger;"> % </span>';
                }
               
                let node = '<input type="text" class="dt-textbox-range-readonly" ' +
                    'data-target-field="' + self.data + '" ' +
                    'value="' + txtBoxValBegin + '" style="width:20%" ' + (self.maxLength ? ' maxlength="' + 10 + '" ' : '') + ' readonly />';
                //+ ' onkeyup="this.value=this.value.replace(/' + regex + '/g, \'\')" />';


                ///  let node = '<gisc-ui-textbox params="value:' + renderableDataText +', filter:{type :numeric, numericType:float "/>'
                return spanBegin + node + spanEnd;
            }
            else if (txtBoxValEnd != undefined && txtBoxValBegin == undefined) {

                spanBegin = isMorethanEqual == true ? '<span style="font-size: larger;"> >= </span>' : '<span style="font-size: larger;"> < </span>';
                if (!disableSpanVal) {
                    spanEnd = '<span style="font-size: larger;"> % </span>';
                }
                let node ='<input type="text" class="dt-textbox-range-readonly" ' +
                    'data-target-field="' + self.data + '" ' +
                    'value="' + txtBoxValEnd + '" style="width:20%" ' + (self.maxLength ? ' maxlength="' + 10 + '" ' : '') + ' readonly />';
                //+ ' onkeyup="this.value=this.value.replace(/' + regex + '/g, \'\')" />';


                ///  let node = '<gisc-ui-textbox params="value:' + renderableDataText +', filter:{type :numeric, numericType:float "/>'
                return spanBegin + node + spanEnd;
            }

            //else if (txtBoxScoreBegin) {
            //    let node = '<input type="text" class="dt-textbox-range" ' +
            //        'data-target-field="' + self.data + '" ' +
            //        'value="' + txtBoxScoreBegin + '" style="width:30%" ' + (self.maxLength ? ' maxlength="' + 10 + '" ' : '') + ' readonly />';
            //    return node;
            //}

            //else if (txtBoxScoreEnd != undefined) {
            //    let node = '<input type="text" class="dt-textbox-range" ' +
            //        'data-target-field="' + self.data + '" ' +
            //        'value="' + txtBoxScoreEnd + '" style="width:30%" ' + (self.maxLength ? ' maxlength="' + 10 + '" ' : '') + ' readonly />';
            //    return node;
            //}

            //else if (txtBoxScore) {
            //    let node = '<input type="text" class="dt-textbox-range" ' +
            //        'data-target-field="' + self.data + '" ' +
            //        'value="' + txtBoxScore + '" style="width:30%" ' + (self.maxLength ? ' maxlength="' + 10 + '" ' : '') + ' />';
            //    return node;
            //}

            else {
                // Verify validation error message.
                var hasValidation = false;
                var validationMessage = "";
                var validationCssClass = "hidden";

                if (!_.isNil(validationData)) {
                    hasValidation = true;
                    validationMessage = Utility.getProperty(row, validationData);

                    if (!_.isEmpty(validationMessage)) {
                        // Remove hidden class for visible message.
                        validationCssClass = "";
                    }
                }

                // Return composed HTML code.
                let spanRangeBegin = isMorethanEqual == true ? "<span class=\"dt-rangebox-separator\">>=</span>" : "<span class=\"dt-rangebox-separator\"><</span>" ; 
                let spanRangeEnd = isMorethanEqual == true ? "<span class=\"dt-rangebox-separator\"><=</span>" : "<span class=\"dt-rangebox-separator\">>=</span>" ; 
                
                return spanRangeBegin+self.getTextBoxHtml(minProp, minVal, hasValidation, true, maxLengthVal) +spanRangeEnd +
                    self.getTextBoxHtml(maxProp, maxVal, hasValidation, false, maxLengthVal) +
                    `<span class="ko-inline-msg-err ${validationCssClass}">${validationMessage}</span>`;
            }
           
        };
    }

    /**
     * Prepare textbox HTML.
     * 
     * @param {any} targetField
     * @param {any} value
     * @param {boolean} [hasValidation=false]
     * @returns
     * 
     * @memberOf DTRangeColumn
     */
    getTextBoxHtml(targetField, value, hasValidation = false, disable = false, maxLength = 3) {
        let strReadOnly = disable == false ? '' : 'readonly';
        let cssReadOnly = disable == false ? '' : 'cursor: not-allowed;background-color: #dbdbdb;';
        let classRangebox = maxLength != 3 ? 'dt-rangebox-advance-rule-fuel' : 'dt-rangebox dt-rangebox-advance-rule-speed';
        let widthVal = maxLength != 3 ? 'width:15%;' : 'width:10%;';

        return `<input type="text" style="${widthVal}${cssReadOnly}text-align: center;" class="${classRangebox}" maxlength = ${maxLength} data-validation="${hasValidation}"
                 data-target-field="${targetField}" value="${value}" ${strReadOnly}
                />`;
    }
}

export default DTRangeColumn;

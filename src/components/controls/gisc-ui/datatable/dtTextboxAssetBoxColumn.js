﻿import ko from "knockout";
import DTBaseColumn from './dtBaseColumn';
import Utility from "../../../../app/frameworks/core/utility";
import {Enums} from "../../../../app/frameworks/constant/apiConstant";

/**
 * Editable textbox as DataTable column
 * 
 * @class DTTextboxColumn
 * @extends {DTBaseColumn}
 */
class DTTextboxAssetBoxColumn extends DTBaseColumn {
    
    /**
     * Creates an instance of DTTextboxColumn.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);     
        this.maxLength = Utility.extractParam(params.maxlength);
        
        // Must define render function here instead of ES6 public method.
        var self = this;
        this.render = function(data, type, row) {
            //let enableVal = Utility.getProperty(row, enable);
            let validationMessage = Utility.getProperty(row, "errorAnalogVal");
            let renderableDataText = data;
            let dataType = Utility.getProperty(row, "eventDataType");
            let isSupportAnalog = Utility.getProperty(row, "isSupportAnalog");
            let eventModelDataType = Utility.getProperty(row, "eventModelDataType");

            var validationCssClass = "hidden";
            let node = "";
            if(_.isNil(renderableDataText)) {
                renderableDataText = "";
            }

            if (!_.isNil(validationMessage)) {
                validationCssClass = "";

            }

            if (eventModelDataType == Enums.ModelData.EventDataType.Analog && dataType == Enums.ModelData.EventDataType.Digital 
                && isSupportAnalog) 
            {
                node = '<input type="text" class="dt-textbox" ' +
                    'data-target-field="' + self.data + '" ' +
                    'value="' + renderableDataText + '"' + (self.maxLength ? ' maxlength="' + self.maxLength + '" ' : '') + ' style="width:40%;text-align: center;" />';
                 //+ ' onkeyup="this.value=this.value.replace(/'+ self.regex +'/g, \'\')" />';


              node = "<span> >= </span>"+ node 
              +`<span style="padding-left: 10px;" class="ko-inline-msg-err ${validationCssClass}">${validationMessage}</span>`;

            }
            return node;
        }
    }
}

export default DTTextboxAssetBoxColumn;
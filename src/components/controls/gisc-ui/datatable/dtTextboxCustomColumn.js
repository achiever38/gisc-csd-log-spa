﻿import ko from "knockout";
import DTBaseColumn from './dtBaseColumn';
import Utility from "../../../../app/frameworks/core/utility";

/**
 * Editable textbox as DataTable column
 * 
 * @class DTTextboxColumn
 * @extends {DTBaseColumn}
 */
class dtTextboxCustomColumn extends DTBaseColumn {
    
    /**
     * Creates an instance of DTTextboxColumn.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);     
        this.maxLength = Utility.extractParam(params.maxlength);
        let enable = params.enable;
        let validationData = params.validationData;
        let showVirtualError = params.showVirtualError;
        // Must define render function here instead of ES6 public method.
        var self = this;
        this.render = function(data, type, row) {
            let enableVal = Utility.getProperty(row, enable);
            let showVirtualErrorVal = Utility.getProperty(row, showVirtualError);
            let validationMessage = Utility.getProperty(row, validationData);
            let renderableDataText = data;
            //let validationCssClass = "hidden";
            var validationCssClass = "hidden";
            var styleTextColor = "";
            let node = "";
            if(_.isNil(renderableDataText)) {
                renderableDataText = "";
            }

            if (!_.isNil(validationMessage)) {
                validationCssClass = "";

            }
            else if (showVirtualErrorVal) {
                validationCssClass = "";
                styleTextColor = `style="opacity: 0.0"`;
            }

            if (enableVal) {
                node = '<input type="text" class="dt-textbox" ' +
                    'data-target-field="' + self.data + '" ' +
                    'value="' + renderableDataText + '"' + (self.maxLength ? ' maxlength="' + self.maxLength + '" ' : '') + ' style="width:20%;text-align: center;" />';
                 //+ ' onkeyup="this.value=this.value.replace(/'+ self.regex +'/g, \'\')" />';


              ///  let node = '<gisc-ui-textbox params="value:' + renderableDataText +', filter:{type :numeric, numericType:float "/>'
            }
            else {
                node = '<input type="text" class="dt-textbox-range-readonly" ' +
                    'data-target-field="' + self.data + '" ' +
                    'value="' + renderableDataText + '"' + (self.maxLength ? ' maxlength="' + self.maxLength + '" ' : '') + "readonly" + ' style="width:20%" />';
                 //+ ' onkeyup="this.value=this.value.replace(/'+ self.regex +'/g, \'\')" />';


              ///  let node = '<gisc-ui-textbox params="value:' + renderableDataText +', filter:{type :numeric, numericType:float "/>'
            }
            node += `<span ${styleTextColor} class="ko-inline-msg-err ${validationCssClass}">${validationMessage}</span>`;
            return node;
        }
    }
}

export default dtTextboxCustomColumn;
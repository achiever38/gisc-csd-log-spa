import ko from "knockout";
import DTBaseColumn from './dtBaseColumn';
import Utility from "../../../../app/frameworks/core/utility";

class DTImageButtonColumn extends DTBaseColumn {

    /**
     * Creates an instance of DTImageButtonColumn.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        // Force this column to non orderable/searchable
        this.searchable = false;
        this.orderable = false;
        this.columnId = Utility.extractParam(params.columnId, "imbCol");
        this.onClick = Utility.extractParam(params.onClick, null);

        // Bridge to gisc DataTable
        this.table.registerColumnEvents(this.columnId, "click", this.onClick);

        var self = this;
        this.render = function (data, type, row) {
            let node = '<div class="dt-img-button" data-column-id="';
            node += self.columnId;
            node += '">';
            
            if(data) {
                node += '<img src="';
                node += data;
                node += '"/>';
            }

            node += "</div>"
            // console.log("--DTIBC--");
            // console.log(row);

            // Desperate try...
            // $()

            return node;
        };
    }
}

export default DTImageButtonColumn;
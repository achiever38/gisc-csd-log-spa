import ko from "knockout";
import DTBaseColumn from './dtBaseColumn';
import Utility from "../../../../app/frameworks/core/utility";
import checkboxCheckedTemplateMarkup from "text!../../../svgs/checkbox-checked.html";

/**
 * Editable checkbox as DataTable column
 * 
 * @class DTCheckboxColumn
 * @extends {DTBaseColumn}
 */
class DTCheckboxColumn extends DTBaseColumn {

    /**
     * Creates an instance of DTCheckboxColumn.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        // Force this column to non orderable/searchable
        this.searchable = false;
        this.orderable = false;

        this.enableProp = Utility.extractParam(params.enable, null);
        this.disableProp = Utility.extractParam(params.disable, null);

        this.enableSelectAll = Utility.extractParam(params.enableSelectAll, false);
        this.className = "dt-body-center";
        this.redrawOnDataChange = Utility.extractParam(params.redrawOnDataChange, false);

        if(this.enableSelectAll){
            var originalTitle = this.title;
            this.title = '<span>'+originalTitle+'</span>\
                    <div class="app-checkBox-contain-dt left">\
                    <label class="app-checkBox-control-dt control-checkbox-dt">\
                    <input type="checkbox" class="dt-checkbox-master" \
                    data-target-field="' + this.data + '" \
                    data-column-index="' + this.columnIndexInternal + '" />\
                     <div class="app-control-dt-indicator app-input">' + checkboxCheckedTemplateMarkup + '</div>\
                    </label>\
                    </div>';
        }

        // Must define render function here instead of ES6 public method.
        var self = this;
        this.render = function (data, type, row) {
            let node = '<div class="app-checkBox-contain-dt">\
                <label class="app-checkBox-control-dt control-checkbox-dt">\
                <input type="checkbox" class="dt-checkbox" ' +
                'data-column-index="' + self.columnIndexInternal + '"' +
                'data-target-field="' + self.data + '"' +
                'data-force-redraw="' + self.redrawOnDataChange + '"';
            if (data) {
                node += ' checked';
            }
            if (!_.isNil(row[self.enableProp]) && !row[self.enableProp]) {
                node += ' disabled="disabled"';
            } else if (!_.isNil(row[self.disableProp]) && row[self.disableProp]) {
                node += ' disabled="disabled"';
            }
            node += ' />\
            <div class="app-control-dt-indicator app-input">' + checkboxCheckedTemplateMarkup + '</div>\
            </label>\
            </div>';
            return node;
        }
    }
}

export default DTCheckboxColumn;
import ko from "knockout";
import "jquery";
import "datatables.net-responsive";
import "./datatablePipeline";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./datatable.html";
import Logger from "../../../../app/frameworks/core/logger";
import ErrorBase from "../../../../app/frameworks/core/errorBase";
import Utility from "../../../../app/frameworks/core/utility";
import DTLabelColumn from "./dtLabelColumn";
import DTCheckboxColumn from "./dtCheckboxColumn";
import DTTextboxColumn from "./dtTextboxColumn";
import DTDropdownColumn from "./dtDropdownColumn";
import DTColorHexColumn from "./dtColorHexColumn";
import DTBooleanColumn from "./dtBooleanColumn";
import DTHierarchyColumn from "./dtHierarchyColumn";
import DTImageButtonColumn from "./dtImageButtonColumn";
import DTCustomColumn from "./dtCustomColumn";
import DTRangeColumn from "./dtRangeColumn";
import DTLinkColumn from "./dtLinkColumn";
import DTRegexTextboxColumn from "./dtRegexTextboxColumn";
import DTTextboxColumnOne from "./dtTextboxColumnOne";
import DTRangeAndTextboxColumn from "./dtRangeAndTextboxColumn";
import DTTextboxCustomColumn from "./dtTextboxCustomColumn";
import EventAggregator from "../../../../app/frameworks/core/eventAggregator";
import closeTemplateMarkup from "text!../../../svgs/blade-close.html";
import checkboxCheckedTemplateMarkup from "text!../../../svgs/checkbox-checked.html";
import UIConstants from "../../../../app/frameworks/constant/uiConstant";
import "jquery-alphanum";
import DTTextboxAssetBoxColumn from "./dtTextboxAssetBoxColumn";

/**
 * DataTable is using for displaying tabular data
 * 
 * @class DataTable
 * @extends {ControlBase}
 */
class DataTable extends ControlBase {

    /**
     * Creates an instance of DataTable.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        // Core
        this.id = this.ensureNonObservable(params.id, this.id);
        this.datasource = this.ensureObservable(params.datasource, null);
        this.columns = this.ensureNonObservable(params.columns, []);
        this.customColumns = this.ensureObservable(params.customColumns, null); // Support case dynamic column
        this.selectedRow = this.ensureObservable(params.selectedRow, null); // FIXME: now it is mutual exclusive with selectedRows
        this.selectedRows = this.ensureObservable(params.selectedRows, []);
        this.removable = this.ensureNonObservable(params.removable, null);
        this.moveable = this.ensureNonObservable(params.moveable, false);
        this.ordering = this.ensureNonObservable(params.ordering, true);
        this.order = this.ensureObservable(params.order, [[0, "asc"]]);
        this.orderOriginal = _.cloneDeep(this.order());//null; // order orignal state.
        this.autoWidth = this.ensureNonObservable(params.autoWidth, true);
        this.errMode = this.ensureNonObservable(params.errMode, UIConstants.DataTableErrorMode.Alert); // https://datatables.net/reference/option/%24.fn.dataTable.ext.errMode
        this.serverSide = this.ensureNonObservable(params.serverSide, false);
        this.onDatasourceRequest = this.ensureFunction(params.onDatasourceRequest, null);
        this.onRemovable = this.ensureFunction(params.onRemovable, () => { });
        this.totalRecords = ko.observable(0); // Internal counting from server.
        this.datasourceServerSide = ko.observableArray([]);
        this.cacheLastRequest = null;
        this.cacheLastPageSize = null;
        this.forceRefresh = false;

        this.preventRefreshEvtAggr = this.ensureNonObservable(params.preventRefreshEvtAggr, false);

        this.onDatasourceRequestInternal = (request) => {
            // Handle internal datasource manipulate.
            var dfd = $.Deferred();
            //console.log("onDatasourceRequestInternal begin", request.draw);
            var cleanCache = false;
            var pendingUpdateViewportSize = false;
            var originalViewportSize = 20;
            var originalPageIndex = 0;

            // Add logic for reset UI state when user changes
            // Sorting, Search, PageSize will re-start view to first page.
            // Load more will maintain internal datasource for each page manually.

            if(this.cacheLastRequest !== null){
                // Verify sorting or pageSize was changed or not.
                if (this.forceRefresh ||
                    JSON.stringify( request.order )   !== JSON.stringify( this.cacheLastRequest.order ) ||
                    JSON.stringify( request.columns ) !== JSON.stringify( this.cacheLastRequest.columns ) ||
                    JSON.stringify( request.search )  !== JSON.stringify( this.cacheLastRequest.search ) ||
                    this.pageSize() !== this.cacheLastPageSize) {
                        cleanCache = true;

                        // Page length will be synchronized with current pageSize.
                        originalViewportSize = this.pageSize();
                        originalPageIndex = this.pageIndex();

                        var currentViewportSize = (originalPageIndex + 1) * originalViewportSize;
                        if(currentViewportSize !== originalViewportSize){
                            // Track restore point of page size.
                            pendingUpdateViewportSize = true;

                            // Apply new size to observable pagesize.
                            this.pageSize.poke(currentViewportSize);
                            //console.log("expand page size to", currentViewportSize, " now pagesize is", this.pageSize());
                        }

                        // Reset page index to first page.
                        this.pageIndex(0);
                }
            }
            else {
                // First time request then clean cache.
                cleanCache = true;
            }

            // Clear force refresh flag.
            this.forceRefresh = false;

            // Store the request for checking next time around
            this.cacheLastRequest = $.extend( true, {}, request );
            this.cacheLastPageSize = this.pageSize();

            // Invoke caller method.
            this.onDatasourceRequest(request)
            .done((items, totalRecords) => {
                //console.log("onDatasourceRequestInternal done", request.draw);
                // Update total record for first page.
                if(cleanCache){
                    if(_.isNumber(totalRecords)){
                        this.totalRecords(totalRecords);
                    }
                    this.datasourceServerSide.replaceAll(items);
                    cleanCache = false;
                    //console.log("overwrite datasource.");
                }
                else {
                    // For load more page request we need to append new datasource to latest page.
                    var mergedItems = _.union(this.datasourceServerSide(), items, (row) => {return row[this.rowId];});
                    this.datasourceServerSide.replaceAll(mergedItems);
                    //console.log("append datasource.");
                }

                // Expect Json is return object based on standard
                // https://datatables.net/manual/server-side
                // {
                //     draw: 1, //	integer	The draw counter that this object is a response to - from the draw parameter sent as part of the data request. Note that it is strongly recommended for security reasons that you cast this parameter to an integer, rather than simply echoing back to the client what it sent in the draw parameter, in order to prevent Cross Site Scripting (XSS) attacks.
                //     recordsTotal: 10, //	integer	Total records, before filtering (i.e. the total number of records in the database)
                //     recordsFiltered: 10, //	integer	Total records, after filtering (i.e. the total number of records after filtering has been applied - not just the number of records being returned for this page of data).
                //     data: [], //	array	The data to be displayed in the table. This is an array of data source objects, one for each row, which will be used by DataTables. Note that this parameter's name can be changed using the ajax option's dataSrc property.
                //     error: "" //	string	Optional: If an error occurs during the running of the server-side processing script, you can inform the user of this error by passing back the error message to be displayed using this parameter. Do not include if there is no error.
                // }
                var tableDataObject = {
                    draw: request.draw,
                    data: this.datasourceServerSide()
                };

                // Passing response to pipeline.
                dfd.resolve(tableDataObject);
            })
            .fail((error) => {
                // Prepare error response.
                var tableDataObject = {
                    draw: request.draw,
                    data: [],
                    recordsTotal: 0,
                    error: error.toString()
                };
                drd.resolve(tableDataObject);
            })
            .always(() => {
                //console.log("onDatasourceRequestInternal always", request.draw);
                // Restore viewport change.
                if(pendingUpdateViewportSize){
                    //console.log("restore pagesize from", this.pageSize(), originalViewportSize, "and page index from", this.pageIndex(), originalPageIndex );
                    this.pageSize.poke(originalViewportSize);
                    this.pageIndex(originalPageIndex);

                    // Resotre default value.
                    pendingUpdateViewportSize = false;
                    originalViewportSize = -1;
                    originalPageIndex = 0;
                }
            });

            return dfd;
        };

        // State
        this.selectable = this.ensureNonObservable(params.selectable, false);
        this.showFilter = this.ensureNonObservable(params.showFilter, false);
        this.filter = this.ensureObservable(params.filter, null);
        this.retainSelectedRowAfterReplaceDataSource = this.ensureObservable(params.retainSelectedRowAfterReplaceDataSource, false);
        this.recentChangedRowIds = this.ensureObservable(params.recentChangedRowIds, []);
        this.readonly = this.ensureNonObservable(params.readonly, false);

        // Internal
        this.dtInstance = null;
        this.dtInstanceOptions = null;
        this.dtInstanceFilterClear = null;
        this.rowId = this.ensureNonObservable(params.rowId, "id");
        this.masterRowSelector = null;
        this.masterCheckboxes = null;
        this._eventAggregator = new EventAggregator();
        this._suspendDSArrayChanged = false;

        // Events
        this.onDataSourceChanged = this.ensureFunction(params.onDataSourceChanged);
        this.onSelectingRow = this.ensureFunction(params.onSelectingRow);
        this.onSortingColumn = this.ensureFunction(params.onSortingColumn);
        this.onTableReady = this.ensureFunction(params.onTableReady);
        this.onError = this.ensureFunction(params.onError);

        // Computional
        this.showRowSelector = this.ensureNonObservable(params.showRowSelector, false);
        if (this.showRowSelector) {
            this.selectable = true;
        }
        this.rowSelectorType = this.ensureNonObservable(params.rowSelectorType, "multiple");
        this.shouldDisplayClearFilterAction = ko.pureComputed(() => {
            return !_.isEmpty(this.filter());
        });

        // i18n injectable text
        this.filterPlaceholder = this.ensureNonObservable(params.filterPlaceholder, "Filter");

        // Internal subscribe to trigger $DT visual
        this._filterSubscribe = this.filter.subscribe((text) => {
            if(this.serverSide){
                // Do nothing because datatable will retrive datasource from server.
                // If we call draw method again it will impact invoke multiple ajax calls.
                // So in server mode will readonly filter, not force re-render.
            }
            else {
                // In client side mode we need to keep compatibility of original feature two way refresh.
                // Screen can update filter and datatable will re-draw automatically.
                this.dtInstance.search(text).draw();
            }
        });
        this._selectedRowSubscribe = this.selectedRow.subscribe((row) => {
            // Reset row selection
            if (!row) {
                this.dtInstance.$("tr.selected").removeClass("selected");
            } else {
                // Only resetRecentChangedUIState when selected row is not null.
                this.resetRecentChangedUIState();
            }
        });
        this._recentChangedRowIdsSubscribe = this.recentChangedRowIds.subscribe((rowIds) => {
            this.resetRecentChangedUIState();

            // Set recent-changed state
            if(rowIds.length > 0) {
                this.registerRecentChangedRows(rowIds);
            }
        });

        this._orderSubscribe = this.order.subscribe((newValue) => {
            this.resetRecentChangedUIState();
            this.dtInstanceOptions.order = newValue;
            this.dtInstance.order(newValue).draw();
        });

        // Waiting for dispatch event
            this._eventAggregator.subscribe(this.id, (eventObj)=>{
                if(eventObj.eventName === "refresh") {
                    this._dispatchEventRefresh(eventObj.message);
                } else if(eventObj.eventName === "reset") {
                    this._dispatchEventRefresh(eventObj.message, true);
                } else if(eventObj.eventName === "setColumnTitle") {
                    this._dispatchEventSetColumnTitle(eventObj.message);
                } else if(eventObj.eventName === "saveTableScrollPosition") {
                    this._dispatchEventSaveTableScrollPosition(eventObj.message);
                } else if (eventObj.eventName === "loadTableScrollPosition") {
                    this._dispatchEventLoadTableScrollPosition(eventObj.message);
                } else if (eventObj.eventName === "updateCheckboxSelectAll") {
                    this._dispatchEventUpdateCheckboxSelectAll(eventObj.message);
                } 
            });

        // LoadMore - pageSize and pageIndex is loaded from ViewState, if not specified
        this.loadMore = this.ensureNonObservable(params.loadMore, false);
        this.onLoadMore = this.ensureFunction(params.onLoadMore);
        this.pageSize = this.ensureObservable(params.pageSize, 20);

        // In ServerSide mode we need to expand pageSize which not in availalbe list temporary.
        // So we need to decoupling pageSize for dropdown list bidning
        this.pageSizeInternal = ko.observable(this.pageSize());
        this._pageSizeInternalChange = this.pageSizeInternal.subscribe((newValue) => {
            this.pageSize(newValue);
        });
        this.pageIndex = this.ensureObservable(params.pageIndex, 0);
        this.visibleRowsId = [];
        this.visibleRowsCount = ko.observable(0);

        this.formatRecordCount = ko.pureComputed(() => {
            var total = !_.isNil(this.datasource()) ? this.datasource().length : 0;
            if(this.serverSide) {
                total = this.totalRecords();
            }

            var formatVisibleRowsCount = Utility.numberFormat(this.visibleRowsCount());
            var formatTotal = Utility.numberFormat(total);
            return Utility.stringFormat(this.i18n("Common_RecordCount")(), formatVisibleRowsCount, formatTotal);
        });
        this.shouldDisplayLoadMore = ko.pureComputed(() => {
            var total = !_.isNil(this.datasource()) ? this.datasource().length : 0;
            if(this.serverSide) {
                total = this.totalRecords();
            }
            var currentSize = (this.pageIndex() + 1) * this.pageSize();
            return currentSize < total;
        });
        this.shouldDisplayTopPane = ko.pureComputed(() => {
            // var total = !_.isNil(this.datasource()) ? this.datasource().length : 0;
            // return this.loadMore && (total > 0);

            // As of 30355 - always show top pane when loadMore == true
            return this.loadMore;
        });
        this.availablePageSizes = ko.observableArray([ 20, 50, 100]);

        this._pageSizeSubscribe = this.pageSize.ignorePokeSubscribe((newSize) => {
            if(!_.isNil(this.dtInstance)) {
                // Reset UI when page size change effect sorting issue.
                //if(!this.serverSide){
                    this._resetDTInstanceUIState();
                //}
                this._updateVirtualPageSize();
            }
        });

        // use for save/load current scroll position of table
        this.tableScrollPosition = null;
    }

    // ---- Life Cycle ----

    /**
     * Called from gisc-ui-component when controls is loaded
     * @public
     * @param {any} isFirstLoad
     */
    onLoad(isFirstLoad) {
    }

    /**
     * Called from gisc-ui-component when controls is removed from DOM.
     * @public
     */
    onUnload() {
        this.dispose();
    }

    /**
     * Called from gisc-ui-component to pass ViewState to this control.
     * 
     * @param {any} state
     */
    onLoadViewState(state) {
        this.pageSize(state.pageSize || 20);
        this.pageIndex(state.pageIndex || 0);
        // Loading order if available.
        if(!_.isEmpty(state.orderOriginal)) {
            // Using previous store state.
            this.orderOriginal = state.orderOriginal;
        }
        else {
            // Using default value from screen by clone order without references.
            this.orderOriginal = _.cloneDeep(this.order());
        }

        if(this.serverSide) {
            if(state.hasHibernate === true){
                this.forceRefresh = true;
                this.cacheLastRequest = state.cacheLastRequest;
            }
        }
    }

    /**
     * Called from gisc-ui-component to save ViewState for this control.
     * 
     * @param {any} state
     */
    onSaveViewState(state) {
        state.pageSize = this.pageSize();
        state.pageIndex = this.pageIndex();
        state.orderOriginal = this.orderOriginal;

        if(this.serverSide) {
            state.hasHibernate = true;
            state.cacheLastRequest = this.cacheLastRequest;
        }
    }

    /*
     * Called from template when controls is rendered.
     * @public
     */
    onDomReady() {
        let self = this;

        if(!self.serverSide){
            // Observe changes form binding datasource.
            // This requires ko.observableArray.fn.subscribeArrayChanged()
            this.datasource.subscribeArrayChanged(
                function(addedItems) {
                    if(self._suspendDSArrayChanged) return;

                    self.dtInstance.rows.add(addedItems).draw();
                },
                function(deletedItems) {
                    if(self._suspendDSArrayChanged) return;

                    deletedItems.forEach((deletedItem) => {
                        // Primary key is the value of rowId property, in most case is "id" of bound item in DS
                        var pk = deletedItem[self.rowId];
                        var rowDom = self.dtInstance.row("#" + pk);
                        rowDom.remove();
                    });
                    self.dtInstance.draw();
                },
                function(latestValue) {
                    if(self._suspendDSArrayChanged) return;

                    // When loadMore is enabled, the data need to be filtered based on pageIndex and pageSize
                    if(self.loadMore) {
                        self._updateVirtualPageSize();
                    }

                    if ((self.retainSelectedRowAfterReplaceDataSource() || self.showRowSelector) && self.selectable) {
                        if (!self._hasAnySelectedRowInUIState()) {
                            self.registerSelectedRows(_.isNil(self.selectedRow()) ? [] : [self.selectedRow()]);
                        }
                    }
                }
            );
        }

        var $dataTableNode = $('#' + this.id);

        // Prepare options
        let dtOptions = {
            // working with optimizeScrollableHeader
            scrollX: true, // Auto scrolling.
            sScrollX: "100%",
            sScrollXInner: "99.80%", 
            // Cannot set to 100% because inner content always overflow in non-webkit browsers.
            // Using this value works with small blade size.
            deferRender: true,
            autoWidth: this.autoWidth, // default true.
            order : this.order(),
            ordering: this.ordering,
            bFilter: this.showFilter,
            bLengthChange: false,
            info: false,
            paging: false,
            search: {
                search: self.filter()
            },
            initComplete: function(settings, json) {
                // DOM Manipulation: filter
                let filterInputSelector = "input[aria-controls=" + self.id + "]";
                let filterInput = $(filterInputSelector);
                if (filterInput.length > 0) {
                    $(filterInput).attr("type", "text");

                    // Append clear search action
                    let $filterContainer = $(filterInput).parents(".dataTables_filter");
                    $filterContainer
                        .css("position", "relative")
                        .append('<div class="dt-filter-clear"><i class="gisc-ui-icon" aria-hidden="true">' + closeTemplateMarkup + '</div>')
                        .on("click", ".dt-filter-clear", function(e) { 
                            e.stopPropagation();
                            // Direct redraw table for both client and server mode.
                            self.dtInstance.search("").draw();
                        });

                    // Store clear filter reference for future use.
                    self.dtInstanceFilterClear = $filterContainer.find(".dt-filter-clear");
                    $(self.dtInstanceFilterClear).prop("title", self.i18n("Common_Clear")());

                    // Init state based on self.filter
                    if(!self.shouldDisplayClearFilterAction()) {
                        $(self.dtInstanceFilterClear).css("display", "none");
                    }
                }

                // DOM Manipulation: top pane
                if(self.loadMore && filterInput.length) {
                    let $oFilterContainer = $(filterInput).parents(".dataTables_filter");
                    let $oTopPaneContainer = $oFilterContainer.parents(".dataTables_wrapper").siblings(".dt-top-pane");

                    $oFilterContainer
                        .wrap('<div class="dataTables_filter_wrapper"></div>')
                        .wrap('<div class="dataTables_filter_main"></div>');

                    $oFilterContainer
                        .parents(".dataTables_filter_wrapper")
                        .find(".dataTables_filter_main")
                        .after($oTopPaneContainer);

                    $oTopPaneContainer
                        .find(".dt-page-size")
                        .unwrap();
                }

                // Register toggle select/deselect all event
                self.masterRowSelector = $dataTableNode.parents(".dataTables_scroll").find(".dataTables_scrollHead input:checkbox.dt-row-selector-master");
                if (self.masterRowSelector) {
                    self.masterRowSelector.on("click", function(e) {
                        if (this.checked) {
                            $dataTableNode.find("tbody input:checkbox.dt-row-selector:not(:checked)").trigger('click');
                        } else {
                            $dataTableNode.find("tbody input:checkbox.dt-row-selector:checked").trigger('click');
                        }
                        e.stopPropagation();
                    });
                }

                self.masterCheckboxes = $dataTableNode.parents(".dataTables_scroll").find(".dataTables_scrollHead input:checkbox.dt-checkbox-master");
                if (self.masterCheckboxes) {
                    // For first load checked Master when all chidren is checked.
                    self.masterCheckboxes.each((i, chk)=>{
                        var $chk = $(chk);
                        var targetField = $chk.attr("data-target-field");
                        var checkedCount = 0;
                        var datasource = self.datasource();
                        _.forEach(datasource, (item) => {
                            var value = null;
                            if (ko.isObservable(item[targetField])) {
                                value = item[targetField]();
                            } else {
                                value = item[targetField];
                            }

                            if (value) {
                                checkedCount++;
                            } 
                        });

                        if(datasource.length != 0 && checkedCount != 0 && datasource.length === checkedCount){
                            $chk.prop("checked", true);
                        }
                        else{
                            $chk.prop("checked", false);
                        }

                    });
                    // For binding on click Master. Check/Unchecked all children in same column.
                    self.masterCheckboxes.on("click", function(e) {
                        var $masterChk = $(this);
                        var targetField = $masterChk.attr("data-target-field");
                        var datasource = self.datasource();
                        _.forEach(datasource, (item) => {
                            if ($masterChk.is(":checked")) {
                                if (ko.isObservable(item[targetField])) {
                                    item[targetField](true);
                                } else {
                                    item[targetField] = true;
                                }
                            }
                            else {
                                if (ko.isObservable(item[targetField])) {
                                    item[targetField](false);
                                } else {
                                    item[targetField] = false;
                                }
                            }
                        });

                        var fnGetCellValueForMasterCheckbox = (elm) => {
                            var rowId = $(elm).parents("tr").attr("id");
                            // Side effect from invalidate row and force redraw
                            if (rowId === undefined) return;

                            var targetField = $(elm).attr("data-target-field");
                            var item = self.getItemFromDataSource(rowId);

                            var value = null;
                            if (ko.isObservable(item[targetField])) {
                                value = item[targetField]();
                            } else {
                                value = item[targetField];
                            }

                            return value;
                        }

                        var fnCheckMasterForMasterCheckbox = (columnIndex) => {
                            var $dataTableNode = $('#' + self.id);
                            var tbodyRef = $dataTableNode.find("tbody");
                            var checkBoxes =  $("input.dt-checkbox[data-column-index=" + columnIndex + "]", tbodyRef);

                            var checkedCount = 0;
                            checkBoxes.each((i, chk) => {
                                var value = fnGetCellValueForMasterCheckbox(chk);
                                if (value) {
                                    checkedCount++;
                                }
                            });

                            var $inner =  $dataTableNode.parents(".dataTables_scroll").find(".dataTables_scrollHeadInner");
                            var masterCheckbox = $inner.find("input.dt-checkbox-master[data-column-index=" + columnIndex + "]");
                            if(checkBoxes.length != 0 && checkedCount != 0 && checkBoxes.length === checkedCount){
                                masterCheckbox.prop("checked", true);
                            }
                            else{
                                masterCheckbox.prop("checked", false);
                            }
                        };

                        // Trigger DS changed event
                        if (self.onDataSourceChanged) {
                            var response = self.onDataSourceChanged(null, targetField, { isMasterChecked: $masterChk.is(":checked"), datasource: self.datasource() });
                            if(response && response.state){
                                //debugger;
                                response.done((obj)=>{
                                    _.forEach(obj.refreshColumnIndex, (columnIndex) => {
                                        fnCheckMasterForMasterCheckbox(columnIndex);
                                    });
                                    
                                });
                            }
                        }

                        self.dtInstance.rows().invalidate('data').draw(false);

                        e.stopPropagation();
                    });
                }

                // Set pre-state: selectedRow
                if ((self.selectedRow() !== undefined) && self.selectedRow()) {
                    self.registerSelectedRows([self.selectedRow()]);
                }

                // Set pre-state: selectedRows
                if ((self.selectedRows() !== undefined) && self.selectedRows()) {
                    self.registerSelectedRows(self.selectedRows());
                }

                // Set pre-state: recentChangedRowIds
                if((self.recentChangedRowIds() !== undefined) && self.recentChangedRowIds().length > 0) {
                    self.registerRecentChangedRows(self.recentChangedRowIds());
                }

                if(_.isFunction(self.onTableReady)) {
                   self.onTableReady(); // TODO: add details in API
                }

                self.optimizeScrollableHeader(1000);
            },
            rowCallback: function(row, data, index) {
                // Check if row has already bind once.
                if(row.rowCallbackProcessed === true) {
                    return;
                } else {
                    // Turn on flag and process other fields.
                    row.rowCallbackProcessed = true;
                }

                if(self.readonly) {
                    $(row).addClass("no-highlight");
                }

                // Bind onClick event to RowSelector (Multiple)
                $(row).on("click", "input.dt-row-selector.multiple", function(e) {
                    // Stop event bubbling in tbody > tr
                    e.stopPropagation();
                }).on("change", "input.dt-row-selector.multiple", function(e) {
                    

                    var rowRef = $(this).parents("tr");
                    var rowId = rowRef.attr("id");

                    var isAppendSelection = false;

                    if (rowRef.hasClass("selected")) {
                        rowRef.removeClass("selected");
                        rowRef.removeClass("no-highlight");
                    } else {
                        rowRef.addClass("selected");
                        rowRef.addClass("no-highlight");
                        isAppendSelection = true;
                    }

                    // Trigger changes on selectedRows
                    var targetRow = ko.utils.arrayFilter(self.datasource(), function(row) {
                        return row[self.rowId].toString() === rowId;
                    });

                    if (targetRow.length) {
                        if (isAppendSelection) {
                            self.selectedRows.push(targetRow[0]);
                        } else {
                            // Change the equity check to be more losely couple.
                            // self.selectedRows.remove(targetRow[0]);
                            self.selectedRows.remove((item) => {
                                return item[self.rowId] === targetRow[0][self.rowId];
                            });
                        }

                        // Update masterRowSelector, if neccessary
                        self.changeMasterRowSelectorState(self.selectedRows());
                    }
                });

                // Bind onClick event to RowSelector (Single)
                $(row).on("click", "input.dt-row-selector.single", function(e) {
                    _.each($dataTableNode.find("tbody input:checkbox.dt-row-selector:checked"), (cb)=> {
                        if(e.target !== cb) {
                            $(cb)
                            .prop("checked", false)
                            .parents("tr")
                                .removeClass("selected")
                                .removeClass("no-highlight");
                        }
                    });

                    // Stop event bubbling in tbody > tr
                    e.stopPropagation();
                }).on("change", "input.dt-row-selector.single", function(e) {
                    var rowId = $(this).parents("tr").attr("id");
                    var rowRef = $(this).parents("tr");

                    // Apply selected state to row.
                    var checkedState = $(this).prop("checked");

                    if(checkedState) {
                        rowRef.addClass("selected");
                        rowRef.addClass("no-highlight");
                    }
                    else {
                        rowRef.removeClass("selected");
                        rowRef.removeClass("no-highlight");
                    }

                    // Trigger changes on selectedRows
                    var targetRow = ko.utils.arrayFilter(self.datasource(), function(row) {
                        return row[self.rowId].toString() === rowId;
                    });

                    if (targetRow.length) {
                        self.selectedRows.removeAll();
                        self.selectedRows.push(targetRow[0]);
                        // self.selectedRows.replaceAll([targetRow[0]]);
                    }
                });

                // Fix: DTCheckbox style, the wrapping element intercept onclick in higer level
                // and propogate event to TR:clicked, cause the row selection event to trigger.
                $(row).on("click", ".app-control-dt-indicator.app-input", function(e) {
                    e.stopPropagation();
                });

                // Bind onClick event to DTCheckboxColumn
                $(row).on("click", "input.dt-checkbox", function(e) {
                    // Stop event bubbling in tbody > tr
                    e.stopPropagation();
                }).on("change", "input.dt-checkbox", function(e) {
                    var objId = $(this).parents("tr").attr("id");

                    // Side effect from invalidate row and force redraw
                    if (objId === undefined) return;

                    var targetField = $(this).attr("data-target-field");
                    var isChecked = $(this).prop("checked");
                    var item = self.getItemFromDataSource(objId);

                    if (ko.isObservable(item[targetField])) {
                        item[targetField](isChecked);
                    } else {
                        item[targetField] = isChecked;
                    }

                    var fnGetCellValue = (elm) => {
                        var rowId = $(elm).parents("tr").attr("id");
                        // Side effect from invalidate row and force redraw
                        if (rowId === undefined) return;

                        var targetField = $(elm).attr("data-target-field");
                        var item = self.getItemFromDataSource(rowId);

                        var value = null;
                        if (ko.isObservable(item[targetField])) {
                            value = item[targetField]();
                        } else {
                            value = item[targetField];
                        }

                        return value;
                    }

                    var fnCheckMaster = (columnIndex) => {
                        var $dataTableNode = $('#' + self.id);
                        var tbodyRef = $(this).parents("tbody");
                        var checkBoxes =  $("input.dt-checkbox[data-column-index=" + columnIndex + "]", tbodyRef);

                        var checkedCount = 0;
                        checkBoxes.each((i, chk) => {
                            var value = fnGetCellValue(chk);
                            if (value) {
                                checkedCount++;
                            }
                        });

                        var $inner =  $dataTableNode.parents(".dataTables_scroll").find(".dataTables_scrollHeadInner");
                        var masterCheckbox = $inner.find("input.dt-checkbox-master[data-column-index=" + columnIndex + "]");
                        if(checkBoxes.length != 0 && checkedCount != 0 && checkBoxes.length === checkedCount){
                            masterCheckbox.prop("checked", true);
                        }
                        else{
                            masterCheckbox.prop("checked", false);
                        }
                    };

                    var columnIndex = $(this).attr("data-column-index");
                    if( !_.isEmpty(columnIndex)){
                         fnCheckMaster(columnIndex);
                    }

                    // Trigger DS changed event
                    if (self.onDataSourceChanged) {
                        var response = self.onDataSourceChanged(item, targetField);
                        if(response && response.state){
                            response.done((obj)=>{
                                _.forEach(obj.refreshColumnIndex, (columnIndex) => {
                                    fnCheckMaster(columnIndex);
                                });
                            });
                        }
                    }
                    
                    // Force redraw if neccessary
                    var forceRedrawOnChange = $(this).attr("data-force-redraw") === "true";
                    if (forceRedrawOnChange) {
                        self.dtInstance.row("#" + objId).invalidate().draw();
                    }
                });

                // Bind onClick event to DTTextboxColumn
                $(row).on("click", "input.dt-textbox", function(e) {
                    // Stop event bubbling in tbody > tr
                    e.stopPropagation();
                }).on("change", "input.dt-textbox", function(e) {
                    var objId = $(this).parents("tr").attr("id");
                    var targetField = $(this).attr("data-target-field");
                    var targetValue = $(this).val();
                    var item = self.getItemFromDataSource(objId);

                    if (ko.isObservable(item[targetField])) {
                        item[targetField](targetValue);
                    } else {
                        item[targetField] = targetValue;
                    }

                    // Trigger DS changed event
                    if (self.onDataSourceChanged) {
                        self.onDataSourceChanged(item);
                    }
                });

                // Bind onClick event to DTRangeColumn
                $(row).on("click", "input.dt-rangebox", function(e) {
                    // Stop event bubbling in tbody > tr
                    e.stopPropagation();
                })
                .on("keypress", "input.dt-rangebox", function(e) {
                    var currentKey = e.key;
                    var isValidNumber = true;
                    // Firefox will send key as string for example <- is ArrowLeft, ArrowRight
                    // So we will check special keycode then using regex.
                    switch(e.keyCode) {
                        case 8: // back
                        case 9: // tab
                        case 46: // deleted
                        case 35: // end
                        case 36: // home
                        case 37: // left arrow
                        case 38: // up arrow
                        case 39: // right arrow
                        case 40: // down arrow
                            // allow automatically.
                            break;
                        default:
                            // allow only number validation.
                            if(!(/^\d+$/.test(e.key))) {
                                isValidNumber = false;
                            }
                            break;
                    }

                    if(!isValidNumber) {
                        e.preventDefault();
                        e.stopPropagation();
                    }
                })
                .on("change", "input.dt-rangebox", function(e) {
                    var $txt = $(this);
                    var objId = $txt.parents("tr").attr("id");
                    var targetField = $txt.attr("data-target-field");
                    var targetValue = parseInt($txt.val());

                    if(isNaN(targetValue) || targetValue < 0) {
                        targetValue = 0;
                        $txt.val(targetValue);
                    }

                    var item = self.getItemFromDataSource(objId);

                    Utility.setProperty(item, targetField, targetValue);

                    // Trigger DS changed event
                    if (self.onDataSourceChanged) {
                        self.onDataSourceChanged(item);

                        // Checking for valiation.
                        if($txt.attr("data-validation") === "true") {
                            self.dtInstance.rows().invalidate('data').draw(false);
                        }
                    }
                });


                // Bind onClick event to textbox
                $(row).on("click", "input.dt-textboxone", function (e) {
                    // Stop event bubbling in tbody > tr
                    e.stopPropagation();
                })
                    .on("keypress", "input.dt-textboxone", function (e) {
                        var currentKey = e.key;
                        var isValidNumber = true;
                        // Firefox will send key as string for example <- is ArrowLeft, ArrowRight
                        // So we will check special keycode then using regex.
                        // switch(e.keyCode) {
                        //     case 8: // back
                        //     case 9: // tab
                        //     case 46: // deleted
                        //     case 35: // end
                        //     case 36: // home
                        //     case 37: // left arrow
                        //     case 38: // up arrow
                        //     case 39: // right arrow
                        //     case 40: // down arrow
                        //         // allow automatically.
                        //         break;
                        //     default:
                        //         // allow only number validation.
                        //         if(!(/^\d+$/.test(e.key))) {
                        //             isValidNumber = false;
                        //         }
                        //         break;
                        // }

                        switch (e.keyCode) {
                            case e.keyCode >= 48: // back
                            case e.keyCode <= 57: // tab

                                break;
                            default:
                                // allow only number validation.
                                if (!(/^\d+$/.test(e.key))) {
                                    isValidNumber = false;
                                }
                                break;
                        }



                        if (!isValidNumber) {
                            e.preventDefault();
                            e.stopPropagation();
                        }
                    })
                    .on("change", "input.dt-textboxone", function (e) {
                        var $txt = $(this);
                        var objId = $txt.parents("tr").attr("id");
                        var targetField = $txt.attr("data-target-field");
                        var targetValue = parseInt($txt.val());

                        //console.log($txt);

                        // console.log(objId);
                        // console.log(targetField);
                        // console.log(targetValue);

                        if (isNaN(targetValue) || targetValue <= 0) {
                            targetValue = 1;
                            $txt.val(targetValue);
                        }

                        var item = self.getItemFromDataSource(objId);

                        Utility.setProperty(item, targetField, targetValue);

                        // Trigger DS changed event
                        if (self.onDataSourceChanged) {
                            self.onDataSourceChanged(item);

                            // Checking for valiation.
                            if ($txt.val() <= 0) {
                                self.dtInstance.rows().invalidate('data').draw(true);
                            }
                        }
                    });
                $(row).on("keypress", "input.dt-rangebox-advance-rule", function (e) {
                    //var currentKey = e.key;
                    var isValidNumber = true;
                    // Firefox will send key as string for example <- is ArrowLeft, ArrowRight
                    // So we will check special keycode then using regex.
                     //switch(e.keyCode) {
                     //    case 8: // back
                     //    case 9: // tab
                     //    case 46: // deleted
                     //    case 35: // end
                     //    case 36: // home
                     //    case 37: // left arrow
                     //    case 38: // up arrow
                     //    case 39: // right arrow
                     //    case 40: // down arrow
                     //        // allow automatically.
                     //        break;
                     //    default:
                     //        // allow only number validation.
                     //        if(!(/^\d+$/.test(e.key))) {
                     //            isValidNumber = false;
                     //        }
                     //        break;
                     //}

                    switch (e.keyCode) {
                        case e.keyCode >= 48: // back
                        case e.keyCode <= 57: // tab

                            break;
                        default:
                            // allow only number validation.
                            if (!(/^\d+$/.test(e.key))) {
                                isValidNumber = false;
                            }
                            break;
                    }



                    if (!isValidNumber) {
                        e.preventDefault();
                        e.stopPropagation();
                    }
                });
                

                // Bind onClick event to DTDropdownColumn
                $(row).on("click", ".dt-dropdown-container", function(e) {
                    // Stop event bubbling in tbody > tr
                    e.stopPropagation();
                }).on("change", "select.dt-dropdown", function(e) {
                    var $dd = $(this);
                    var objId = $dd.parents("tr").attr("id");
                    var targetField = $dd.attr("data-target-field");
                    var targetValue = $dd.val();
                    var targetValueText = $dd.find(":selected").text();
                    var item = self.getItemFromDataSource(objId);

                    // Update label ui.
                    $dd.siblings(".app-dropdown-current").text(targetValueText);

                    // Update binding orignal datasource.
                    if (ko.isObservable(item[targetField])) {
                        item[targetField](targetValue);
                    } else {
                        item[targetField] = targetValue;
                    }

                    // Trigger DS changed event
                    if (self.onDataSourceChanged) {
                        self.onDataSourceChanged(item);

                        // Checking for valiation.
                        if($dd.attr("data-validation") === "true") {
                            self.dtInstance.rows().invalidate('data').draw(false);
                        }
                    }
                });

                // Bind onClick event to interal dt-row-remover
                $(row).on("click", ".dt-row-remover", function(e) {
                    // Stop event bubbling in tbody > tr
                    e.stopPropagation();

                    // Now we have to find objectId using rowId
                    // Then find target row on $dtInstance and remove via DT api
                    // --> the best way is to change this.datasource() then trigger change
                    // --> which will automatically go to this.datasource.subscribeArrayChanged

                    var rowId = $(this).parents("tr").attr("id");
                    var targetRows = ko.utils.arrayFilter(self.datasource(), function(row) {
                        return row[self.rowId].toString() === rowId;
                    });

                    if (targetRows.length > 0) {
                        self.datasource.remove(targetRows[0]);
                    }

                    // use for event when remove row data.
                    self.onRemovable(rowId);
                });

                // Bind onClick event to internal dt-row-moveup and dt-row-movedown
                $(row).on("click", ".dt-row-moveup", function(e) {
                    // Stop event bubbling in tbody > tr
                    e.stopPropagation();

                    var i =  self.datasource.indexOf(data);

                    if (i >= 1) {
                        if(i===index){
                            var array = self.datasource();
                            self.datasource.splice(i - 1, 2, array[i], array[i - 1]);
                        }
                    }

                    self.dtInstance.clear();
                    self.dtInstance.rows.add(self.datasource()).draw();
                });
                $(row).on("click", ".dt-row-movedown", function(e) {
                    // Stop event bubbling in tbody > tr
                    e.stopPropagation();

                    var i =  self.datasource.indexOf(data);
                    var array = self.datasource();

                    if (i < array.length - 1) {
                        if(i===index){
                            self.datasource.splice(i, 2, array[i + 1], array[i]);
                        }
                    }

                    self.dtInstance.clear();
                    self.dtInstance.rows.add(self.datasource()).draw();
                });

                // Bind onClick event to DTImageButtonColumn
                $(row).on("click", ".dt-img-button", function(e) {
                    // Stop event bubbling in tbody > tr
                    e.stopPropagation();

                    var objId = $(this).parents("tr").attr("id");
                    var columnId = $(this).attr("data-column-id");
                    var item = self.getItemFromDataSource(objId);

                    var mappedEvent = self.getRegisteredColumnEvent(columnId);
                    if(!_.isNil(mappedEvent)) {
                        if(mappedEvent.eventType === "click") {
                            mappedEvent.eventAction(item);
                        }
                    }
                    
                    $($dataTableNode).find("tbody tr.selected").removeClass("selected");
                    $(row).addClass("selected");
                });

                $(row).on("click", ".dt-link-column", function(e) {
                    // Stop event bubbling in tbody > tr
                    e.stopPropagation();

                    var objId = $(this).parents("tr").attr("id");
                    var columnId = $(this).attr("data-column-id");
                    var item = self.getItemFromDataSource(objId);

                    var mappedEvent = self.getRegisteredColumnEvent(columnId);
                    if(!_.isNil(mappedEvent)) {
                        if(mappedEvent.eventType === "click") {
                            mappedEvent.eventAction(item);
                        }
                    }

                    $($dataTableNode).find("tbody tr.selected").removeClass("selected");
                    $(row).addClass("selected");

                });

                // Bind onClick event to DTRangeColumn
                $(row).on("click", "input.dt-rangebox-advance-rule-fuel", function (e) {
                    // Stop event bubbling in tbody > tr
                    e.stopPropagation();
                }).on("change", "input.dt-rangebox-advance-rule-fuel", function (e) {
                        var $txt = $(this);
                        var objId = $txt.parents("tr").attr("id");
                        var targetField = $txt.attr("data-target-field");
                        var targetValue = parseFloat($txt.val());

                        if (isNaN(targetValue)) {
                            targetValue = "";
                            $txt.val(targetValue);
                        }

                        var item = self.getItemFromDataSource(objId);

                        Utility.setProperty(item, targetField, targetValue);

                        // Trigger DS changed event
                        if (self.onDataSourceChanged) {
                            self.onDataSourceChanged(item);

                            // Checking for valiation.
                            if ($txt.attr("data-validation") === "true") {
                                self.dtInstance.rows().invalidate('data').draw(false);
                            }
                        }
                    });
            },
            drawCallback: function(settings) {
                let api = this.api();

                let vrs = $(api.table().node()).find("tbody tr[role='row']");
                let vrCount = !_.isNil(vrs) ? vrs.length : 0;

                self.visibleRowsCount(vrCount);

                // Keep visible rows id
                self.visibleRowsId = [];
                vrs.each((index, row) => {
                    self.visibleRowsId.push(row.id);
                });

                self.changeMasterRowSelectorState(self.selectedRows());
            },
            rowId: this.rowId
        };

        // if customColumns defined, use customColumns else use columns from template
        // Parse columns to support extra column type, i.e. checkbox, colorhexcode, etc...
        dtOptions.columns = this.customColumns() ? this.parseColumnsParam(this.customColumns()) : this.parseColumnsParam(this.columns);

        // Detect client vs server mode.
        if(this.serverSide){
            // Initialize server mode parameters.
            dtOptions.processing = true;
            dtOptions.serverSide = true;
            dtOptions.deferLoading = 0; // Use this option for prevent loading data blocking rendered.
            dtOptions.ajax = $.fn.dataTable.pipeline({
                onDatasourceRequest: this.onDatasourceRequestInternal,
                pages: 0 // number of pages to cache
            });
        }
        else {
            // Initialize client mode with passing datasource parameters.
            dtOptions.data = this.datasource();
        }

        // i18n
        dtOptions.language = {
            search: "",
            searchPlaceholder: Utility.extractPossibleI18NParam(this.filterPlaceholder),
            zeroRecords: this.i18n("M110")(),
            emptyTable: this.i18n("M111")()
        };

        // Calculate if it should disable sorting even if page does not said it directly.
        if (this.showRowSelector) {
            this.insertRowSelectorColumn(dtOptions);

            var shouldDisableSort = true;
            for (let i = 0; i < dtOptions.columns.length; i++) {
                var orderable = Utility.extractParam(dtOptions.columns[i].orderable, true);
                if (orderable) {
                    shouldDisableSort = false;
                    break;
                }
            }
            if (shouldDisableSort) {
                dtOptions.aaSorting = []; // Disable inital sorting on first column
            }
        }

        if (this.moveable) {
            this.insertMoveItemColumn(dtOptions);
        }

        if (!_.isNil(this.removable)) {
            this.insertRemoveItemColumn(dtOptions);
        }

        // Disable built-in paging when loadMore == true
        if(this.loadMore) {
            dtOptions.paging = true;
            dtOptions.lengthChange = false;
            dtOptions.pageLength = this._getVirtualPageSize();
        }

        $.fn.dataTable.ext.errMode = this.errMode;
        // handle error if errMode is none. see references from https://datatables.net/reference/event/error
        if (this.errMode === UIConstants.DataTableErrorMode.None) {
            $dataTableNode.on( 'error.dt', ( e, settings, techNote, message ) => {
                if (_.isFunction(self.onError)) {
                    self.onError(e, settings, techNote, message);
                }
                else {
                    Logger.log('An error has been reported by DataTables: ', message);
                }
            });
        }

        // Create instance.
        let datatable = $dataTableNode.DataTable(dtOptions);
        self.dtInstance = datatable;
        self.dtInstanceOptions = _.cloneDeep(dtOptions);

        // For server side mode, it initialized with deferLoading
        // After create instance we need to draw it manually.
        // Call draw method will trigger ajax request.
        if(this.serverSide){
            datatable.draw();
        }

        // State:
        // Update filter
        datatable.on('search.dt', () => {
            self.filter(datatable.search());
            self.resetRecentChangedUIState();

            // Modify state based on self.filter
            if(!self.shouldDisplayClearFilterAction()) {
                $(self.dtInstanceFilterClear).css("display", "none");
            } else {
                $(self.dtInstanceFilterClear).css("display", "block");
            }
        });

        // State:
        // Order changed
        datatable.on("order.dt", (e, settings, sortInternal, newSort) => {
            self.resetRecentChangedUIState();
        });

        // Optimize layout when scrolling occured.
        datatable.on('draw.dt', function () {
            // Calling resize method when datatable has been re-draw.
            $dataTableNode.resize();

            // Render recent-changes for server side mode because
            // Datasource doesn't prepared before initialized control.
            if(self.serverSide){
                // Set pre-state: selectedRow
                if ((self.selectedRow() !== undefined) && self.selectedRow()) {
                    self.registerSelectedRows([self.selectedRow()]);
                }

                // Set pre-state: selectedRows
                if ((self.selectedRows() !== undefined) && self.selectedRows()) {
                    self.registerSelectedRows(self.selectedRows());
                }

                // Set pre-state: recentChangedRowIds
                if((self.recentChangedRowIds() !== undefined) && self.recentChangedRowIds().length > 0) {
                    self.registerRecentChangedRows(self.recentChangedRowIds());
                }
            }
        });

        $dataTableNode.parents('section').on('blade.resize', function() {
            // Calling resize method when blade is resize.
            //$dataTableNode.resize();
            datatable.columns.adjust().draw();
        });

        // State:
        // Styling selectable row(s)
        if (this.selectable) {
            $dataTableNode.find("tbody").on("click", "tr", function(e) {
                var $row = $(this);
                // Ignore if cell has "skip-parent-click" class, this is an workaround of
                // event bubling problem TR > TD click on TD but TR process first.
                if(e.target && $(e.target).hasClass("skip-parent-click")) return;

                // Ignore if table is filtered with empty search result.
                if ($row.find("td:first-child").hasClass("dataTables_empty")) return;

                // Single row selection
                var rowId = $row.attr("id");
                var targetRow = self.getItemFromDataSource(rowId);

                // Check with guard event, if selecting is allowed
                var shouldCheckGuard = _.isFunction(self.onSelectingRow);
                if(shouldCheckGuard) {
                    if (targetRow) {
                        var selectedRowResponse = self.onSelectingRow(targetRow);
                        // Verify if response is defer then handle selected state.
                        if(selectedRowResponse && selectedRowResponse.state){
                            selectedRowResponse.done(() => {
                                if ($row.hasClass("selected")) {
                                    $row.removeClass("selected");
                                } else {
                                    self.dtInstance.$("tr.selected").removeClass("selected");
                                }

                                $row.addClass("selected");

                                // Trigger changes on selectedRow
                                // rowId always is an id property of TR, however self.rowId can be any prop on bound JSON.
                                if (targetRow) {
                                    self.selectedRow(targetRow);
                                }
                            });
                        }
                        // For other types do not process any logic.
                    }
                }
            });
        }
    }

    // ---- Internal ----

    optimizeScrollableHeader(firstExecutionDelay = 50) {
        var $dataTableNode = $('#' + this.id);
        $dataTableNode.resize();

        var resizeRef;
        var resizeHeaderFn = () => {
            var bodyWidth = $dataTableNode.width();
            var $headerContainer = $dataTableNode.parents(".dataTables_scroll").find(".dataTables_scrollHeadInner");
            var headerWidth = $headerContainer.width();

            if(bodyWidth !== headerWidth) {
                $headerContainer.width(bodyWidth).find("table").width(bodyWidth);
                Logger.log('optimize', bodyWidth, headerWidth);

                // Re-checking because there is some delay after first sorting.
                setTimeout(resizeHeaderFn, 1000);
            }
            else {
                Logger.log('no need to optimize');
            }
        };

        resizeRef = setTimeout(resizeHeaderFn, firstExecutionDelay);
    }

    registerColumnEvents(columnId, eventType, eventAction) {
        // Lazy init 
        if(_.isNil(this.columnEventMap)) {
            this.columnEventMap = new Map();
        }

        this.columnEventMap.set(columnId, { 
            eventType: eventType,
            eventAction: eventAction
        });

    }

    getRegisteredColumnEvent(columnId) {
        if(_.isNil(this.columnEventMap) || _.isNil(columnId)) return null;

        return this.columnEventMap.get(columnId);
    }

    // ---- Private ----

    getItemFromDataSource(rowId) {
        var self = this;
        var currentObservableDS = self.serverSide ? this.datasourceServerSide : this.datasource;

        let targetRow = currentObservableDS().filter(function(row) {
            return row[self.rowId].toString() === rowId;
        });

        if (targetRow.length) {
            return targetRow[0];
        }

        return null;
    }

    parseColumnsParam(columnsParam) {
        var self = this;
        var dtColumns = [];
        var columnIndex = 0;
        // 1. Check type, if type is not defined OR type is label, follow through
        // 2. If type is checkbox 
        columnsParam.forEach(function(columnP) {
            if (columnP.hasOwnProperty('type')) {

                // Inject self to column definition
                columnP.table = self;
                columnP.columnIndexInternal = columnIndex;
                let columnDef = null;

                switch(columnP.type) {
                    case "label":
                        columnDef = new DTLabelColumn(columnP); 
                        break;
                    case "checkbox":
                        columnDef = new DTCheckboxColumn(columnP); 
                        break;
                    case "textbox":
                        columnDef = new DTTextboxColumn(columnP);
                        break;
                    case "dropdown":
                        columnDef = new DTDropdownColumn(columnP);
                        break;
                    case "colorhex":
                        columnDef = new DTColorHexColumn(columnP);
                        break;
                    case "boolean":
                        columnDef = new DTBooleanColumn(columnP);
                        break;
                    case "hierarchy":
                        columnDef = new DTHierarchyColumn(columnP);
                        break;
                    case "image-button":
                        columnDef = new DTImageButtonColumn(columnP);
                        break;
                    case "custom":
                        columnDef = new DTCustomColumn(columnP);
                        break;
                    case "range":
                        columnDef = new DTRangeColumn(columnP); 
                        break;
                    case "link":
                        columnDef = new DTLinkColumn(columnP);
                        break;
                    case "regex":
                        columnDef = new DTRegexTextboxColumn(columnP);
                        break;
                    case "textboxone":
                        columnDef = new DTTextboxColumnOne(columnP);
                        break;
                    case "rangeandtextbox":
                        columnDef = new DTRangeAndTextboxColumn(columnP); 
                        break;
                    case "textboxcustom":
                        columnDef = new DTTextboxCustomColumn(columnP);
                        break;
                    case "textboxAssetBox":
                        columnDef = new DTTextboxAssetBoxColumn(columnP);
                        break;
                }

                // Verify compatible with serverSide mode.
                if(self.serverSide){
                    switch(columnP.type){
                        case "label":
                        case "image-button":
                        case "custom":
                        case "boolean":
                        case "colorhex":
                            // Works with server side mode.
                            break;
                        default:
                            Logger.warn(columnP.type, "does not compatible with server side mode.");
                            break;
                    }
                }

                // Append column to datatable option.
                dtColumns.push(columnDef);
            } else {
                // It is Original Datatable field, do nothing
                dtColumns.push(columnP);
            }
            columnIndex++;
        });
        // return columns collection that DataTableJS understand
        return dtColumns;
    }

    insertRowSelectorColumn(dtOptions) {
        var rsCol = {
            searchable: false,
            orderable: false,
            width: "1%",
            className: "dt-body-center"
        };

        // Switch by rowSelectorType
        if(this.rowSelectorType === "single") {
            rsCol.title = "";
            rsCol.render = function(data, type, full, meta) {
                let node = '<div class="app-checkBox-contain-dt left">\
                    <label class="app-checkBox-control-dt control-checkbox-dt">\
                    <input type="checkbox" class="dt-row-selector single"';
                if (data) {
                    node += ' checked';
                }
                node += ' />\
                <div class="app-control-dt-indicator app-input">' + checkboxCheckedTemplateMarkup + '</div>\
                </label>\
                </div>';
                return node;
            }
        } else if(this.rowSelectorType === "multiple") {
            rsCol.title = '<div class="app-checkBox-contain-dt left">\
                    <label class="app-checkBox-control-dt control-checkbox-dt">\
                    <input type="checkbox" class="dt-row-selector-master" />\
                     <div class="app-control-dt-indicator app-input">' + checkboxCheckedTemplateMarkup + '</div>\
                    </label>\
                    </div>';
            rsCol.render = function(data, type, full, meta) {
                let node = '<div class="app-checkBox-contain-dt left">\
                    <label class="app-checkBox-control-dt control-checkbox-dt">\
                    <input type="checkbox" class="dt-row-selector multiple"';
                if (data) {
                    node += ' checked';
                }
                node += ' />\
                <div class="app-control-dt-indicator app-input">' + checkboxCheckedTemplateMarkup + '</div>\
                </label>\
                </div>';
                return node;
            }
        }

        dtOptions.columns.unshift(rsCol);
    }

    changeMasterRowSelectorState(selectedRows) {
        if (this.masterRowSelector == null) return;
        
        // Rules:
        // If visible rows = 0, then deselect and disable it.
        // If visible rows > 0, then enable it.
        //      Continue check if there is at least 1 unselected row in visible rows then checked = false, else checked = true

        if (this.visibleRowsId.length === 0) {
            this.masterRowSelector.prop("checked", false);
            this.masterRowSelector.prop("disabled", true);
        }
        else {
            this.masterRowSelector.prop("disabled", false);
            var selectedRowsId = _.map(selectedRows, (item) => {
                return item[this.rowId].toString();
            });

            // find visible items which is not in selected items
            var visibleUnselectedRowsId = _.filter(this.visibleRowsId, (o) => {
                return _.indexOf(selectedRowsId, o) === -1;
            });

            // if there is at least 1 unselected item
            if (visibleUnselectedRowsId.length > 0) {
                this.masterRowSelector.prop("checked", false);
            }
            else {
                this.masterRowSelector.prop("checked", true);
            }
        }
    }

    insertRemoveItemColumn(dtOptions) {
        var visibleData = _.isBoolean(this.removable) ? null : this.removable.visible;
        var removeItemCol = {
            searchable: false,
            orderable: false,
            width: "10%",
            className: "dt-body-right",
            title: "",
            render: function(data, type, full, meta) {
                let visible = _.isNil(visibleData) ? true : full[visibleData];
                if(visible) {
                    return '<div class="dt-row-remover"><i class="gisc-ui-icon" aria-hidden="true">' + closeTemplateMarkup + '</div>';
                }
                return '';
            }
        };
        dtOptions.columns.push(removeItemCol);
    }

    insertMoveItemColumn(dtOptions) {
        var self = this;
        var removeItemCol = {
            searchable: false,
            orderable: false,
            width: "50px",
            className: "dt-body-center",
            title: "",
            render: function(data, type, full, meta) {
                var moveupUrl = self.resolveUrl("~/images/icon_customroute_moveup.svg");
                var movedownUrl = self.resolveUrl("~/images/icon_customroute_movedown.svg");
                return '<img src="' + moveupUrl + '" style="cursor:pointer" class="dt-row-moveup"><img src="' + movedownUrl + '" class="dt-row-movedown" style="cursor:pointer">';

            }
        };
        dtOptions.columns.push(removeItemCol);
    }
    
    /**
     * Register selectedRow, selectedRows state
     * @private 
     * @param {array} selectedRows
     */
    registerSelectedRows(selectedRows) {
        var $dataTableNode = $('#' + this.id);
        var self = this;
        selectedRows.forEach(function(selectedRow) {
            if (selectedRow) {
                $dataTableNode.find("tbody tr").each(function() {
                    // rowId always is an id property of TR, however self.rowId can be any prop on bound JSON.
                    var rowId = $(this).attr("id");
                    if (selectedRow[self.rowId].toString() === rowId) {
                        $(this).addClass("selected");
                        // Also add no-highlight if it is selection from row-selector
                        if(self.showRowSelector) {
                            $(this).addClass("no-highlight");
                        }
                        // Also register the dt-row-selector, if any
                        $(this).find("input:checkbox.dt-row-selector").prop("checked", true);
                    }
                });
            }
        });

        // Update masterRowSelector, if neccessary
        self.changeMasterRowSelectorState(selectedRows);
    }

    registerRecentChangedRows(rowIds = []) {
        if((rowIds === undefined) || (rowIds.length === 0)){
            return;
        }

        var $dataTableNode = $('#' + this.id);
        rowIds.forEach(function(rowId) {
            let selector = "tr[id=" + rowId + "]";
            $dataTableNode.find(selector).addClass("recent-changed");
        }, this);
    }

    resetRecentChangedUIState() {
        this.dtInstance.$("tr.recent-changed").removeClass("recent-changed");
    }

    _hasAnySelectedRowInUIState() {
        var $dataTableNode = $('#' + this.id);
        var result = false;
        var allRows = $dataTableNode.find("tbody tr");
        for (let i = 0; i < allRows.length; i++) {
            var row = allRows[i];
            result = $(row).hasClass("selected");
            if (result) break;
        }
        return result;
    }

    /**
     * Destroy datatable instance and unregister any DOM event subscribed by this controls.
     * @private
     */
    dispose() {
        this._eventAggregator.destroy();

        if (this.dtInstance) {
            this.dtInstance.destroy(true);
        }
        if(this._filterSubscribe) {
            this._filterSubscribe.dispose();
        }
        if (this._selectedRowSubscribe) {
            this._selectedRowSubscribe.dispose();
        }
        if(this._recentChangedRowIdsSubscribe) {
            this._recentChangedRowIdsSubscribe.dispose();
        }
        if(this._orderSubscribe) {
            this._orderSubscribe.dispose();
        }
        if(this._pageSizeInternalChange ) {
            this._pageSizeInternalChange.dispose();
        }
        if(this.onDataSourceChanged) {
            this.onDataSourceChanged = null;
        }
        if(this.onSelectingRow) {
            this.onSelectingRow = null;
        }
        if(this.onSortingColumn) {
            this.onSortingColumn = null;
        }

        // Clear mapping event from screen, if any
        if(!_.isNil(this.columnEventMap)) {
            //IE 11 does not support for of loop.
            this.columnEventMap.forEach(function (item, key, mapObj) {
                item.eventAction = null;
            });
            this.columnEventMap = null;
        }

        var $dataTableNode = $('#' + this.id);
        if ($dataTableNode) {
            $dataTableNode.unbind();
        }
    }

    // ---- Dispatch Events ----

    _dispatchEventRefresh(data = null, resetUIState = false) {
        if(resetUIState) {
            this._resetDTInstanceUIState();
        }

        if(this.serverSide) {
            // We need to flag datatable to force refresh in next request.
            this.forceRefresh = true;

            // Trigger datatable for load ajax datasource.
            this._updateVirtualPageSize();
        }
        else {
            // Mark transaction, to avoid KO subscribe overhead.
            this._suspendDSArrayChanged = true;

            //console.log('NOTE :: Check flag for avoid loadig data event to datascource property')

            if(!this.preventRefreshEvtAggr) {
                if(data) {
                        this.datasource(data);
                } else {
                    this.datasource(this.dtInstanceOptions.data);
                }
            }

            this._updateVirtualPageSize();
            this.dtInstance.clear().rows.add(this.datasource()).draw();
            this._suspendDSArrayChanged = false;
        }


        // Restore selected row, if any.
        if (this.retainSelectedRowAfterReplaceDataSource() && this.selectable) {
            if (!this._hasAnySelectedRowInUIState()) {
                this.registerSelectedRows(_.isNil(this.selectedRow()) ? [] : [this.selectedRow()]);
            }
        }
    }

    _dispatchEventSetColumnTitle(columnObjects) {
        // Note: From testing we found this.dtInstance may not assigned yet.
        // So to make sure use jQuery to find DT instance again in this context.
        var $dataTableNode = $('#' + this.id);
        if($dataTableNode) {
            var localDtInstance = $dataTableNode.dataTable();
            _.each(columnObjects, (col) => {
                $(localDtInstance.api().column(col.columnIndex).header()).contents().last().replaceWith(col.columnTitle);
            });
        }
    }

    // save current table scroll position
    _dispatchEventSaveTableScrollPosition() {
        var $dataTableNode = $('#' + this.id);
        if($dataTableNode) {
            var localDtInstance = $dataTableNode.dataTable();

            this.tableScrollPosition = {
                'top': $(localDtInstance.api().settings()[0].nScrollBody).scrollTop(),
                'left': $(localDtInstance.api().settings()[0].nScrollBody).scrollLeft()
            };
        }
    }

    // load blade scroll position
    _dispatchEventLoadTableScrollPosition() {
        var $dataTableNode = $('#' + this.id);
        if($dataTableNode) {
            var localDtInstance = $dataTableNode.dataTable();
            $(localDtInstance.api().settings()[0].nScrollBody).scrollTop(this.tableScrollPosition.top);
            $(localDtInstance.api().settings()[0].nScrollBody).scrollLeft(this.tableScrollPosition.left);
            this.tableScrollPosition = null;
        }
    }

    // Update Datatable Checkbox SelectAll base on options
    _dispatchEventUpdateCheckboxSelectAll(data = null) {
        if(data) {
            var extendData = $.extend({ columnIndex: 0, checked: false }, data);
            var $dataTableNode = $('#' + this.id);
            var $dataTableParent = $dataTableNode.parent().parent();
            var $dataTablesScrollHead = $(".dataTables_scrollHead", $dataTableParent);
            var $chk = $("th:eq(" + extendData.columnIndex + ") input:checkbox.dt-checkbox-master", $dataTablesScrollHead);
            $chk.prop("checked", extendData.checked);
            $chk.trigger("click");
        } 
    }

    // ---- Load More ----

    _getVirtualPageSize() {
        return (this.pageIndex() + 1) * this.pageSize();
    }

    _updateVirtualPageSize() {
        var size = this._getVirtualPageSize();
        this.dtInstance.page.len(size).draw();
    }

    // // FIXME: Not use this anymore -- use paging WITH native datatablejs
    // _prepareDataSource(originalDS) {
    //     if(this.loadMore) {
    //         // Filter, use pageIndex (start with 0) and number of Load Mode clicked.
    //         var currentSize = (this.pageIndex() + 1) * this.pageSize();
    //         return _.take(originalDS, currentSize);
    //     } else {
    //         return originalDS;
    //     }
    // }

    // // FIXME: Not use this anymore -- use paging WITH native datatablejs
    // _updateDataSourceAndRedraw() {
    //     // TODO: Get New DS from onLoadMore callback.
    //     var updateDS = this._prepareDataSource(this.datasource());

    //     // Update dtInstance based on latest DS
    //     this.dtInstance.clear();
    //     this.dtInstance.rows.add(updateDS).draw();
    // }

    _resetDTInstanceUIState() {
        // Reset page index to first page.
        this.pageIndex(0);

        // Warning we cannot set order in serverside mode because it will break sorting
        // There is no solution now so we skip this block on serverSide mode.
        if(!this.serverSide) {
            this.dtInstance.order(_.cloneDeep(this.orderOriginal));
        }

        // Clear current filter text.
        this.dtInstance.search("");
    }

    onLoadMoreClick() {
        var previousIndex = this.pageIndex();
        this.pageIndex(previousIndex + 1);

        if(this.onLoadMore) {
            this.onLoadMore(this.pageIndex());
        }

        this._updateVirtualPageSize();
    }
}

/**
 * Error when parameters set on gisc-ui-datatable is invalid
 * 
 * @class DataTableParameterInvalidError
 * @private
 * @extends {ErrorBase}
 */
class DataTableParameterInvalidError extends ErrorBase {
    constructor(invalidParamName, reason) {
        super(invalidParamName);
        this.reason = reason;
    }
}

/**
 * Validate parameter for DataTable instance
 * 
 * @private 
 * @param {any} params
 */
function validateDataTableParameters(params) {
    // If selectable BUT there is no rowId, throw error
    if (params.selectable && (params.rowId === "")) {
        throw new DataTableParameterInvalidError("rowId",
            "params.selectable is set to true, expect params.rowId to be specified");
    }
    if (params.filter && !params.showFilter) {
        throw new DataTableParameterInvalidError("filter",
            "params.filter is set, expect params.showFilter to true");
    }
}

export default {
    viewModel: {
        createViewModel: function(params, componentInfo) {

            // Guard against invalid combination of parameters
            validateDataTableParameters(params);

            return new DataTable(params);
        }
    },
    template: templateMarkup
};
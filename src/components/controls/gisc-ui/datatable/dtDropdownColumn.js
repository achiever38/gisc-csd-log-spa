import ko from "knockout";
import DTBaseColumn from './dtBaseColumn';

/**
 * Editable dropdown list as DataTable column
 * 
 * @class DTDropdownColumn
 * @extends {DTBaseColumn}
 */
class DTDropdownColumn extends DTBaseColumn {
    /**
     * Creates an instance of DTDropdownColumn.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        // Specific property for this column.
        var dataField = params.data;
        var optionsData = params.optionsData;
        var optionsText = params.optionsText;
        var optionsValue = params.optionsValue;
        var optionsCaption = params.optionsCaption;
        var validationData = params.validationData;


        // Must define render function here instead of ES6 public method.
        var self = this;
        this.render = function(data, type, row) {
            //console.log(params, data, type, row);
            var dataText = data;
            var optionsTagHtmls = [];

            // Adding blank option when optionsCaption has been set.s
            if(!_.isNil(optionsCaption)) {
                optionsTagHtmls.push(`<option value=\"\">${optionsCaption}</option>`);
            }

            // Parse datasource if define option reflection field.
            if(optionsData && optionsText && optionsValue) {
                var dataSourceArray = row[optionsData];
                if(dataSourceArray && dataSourceArray.length) {
                    dataSourceArray.forEach((item) => {
                        var optionValueAttr = item[optionsValue];
                        var optionTextAttr = item[optionsText];
                        var selectedAttr = "";
                        if(optionValueAttr == data) {
                            selectedAttr = " selected";
                            dataText = optionTextAttr;
                        }
                        optionsTagHtmls.push(`<option value=\"${optionValueAttr}\"${selectedAttr}>${optionTextAttr}</option>`);
                    });
                }
            }

            // Verify validation error message.
            var hasValidation = false;
            var validationMessage = "";
            var validationCssClass = "hidden";
            if(!_.isNil(validationData)) {
                hasValidation = true;
                validationMessage = row[validationData];

                if(validationMessage && validationMessage !== "") {
                    // remove hidden class for visible message.
                     validationCssClass = "";
                }
            }

            // Build full select HTML content.
            var renderableDataText = dataText;
            if(_.isNil(renderableDataText)) {
                renderableDataText = "";
            }
            var optionHtmlResult = optionsTagHtmls.join("");
            var template = 
            `<div class="app-dropdown dt-dropdown-container" style="width:${self.width}">
                    <div class="app-dropdown-container app-dropdown-wrapper app-input">
                        <span class="app-dropdown-current">${renderableDataText}</span>
                        <span class="app-dropdown-arrow-normal"></span>
                        <select class="app-dropdown-select dt-dropdown" data-target-field="${dataField}" data-validation="${hasValidation}" data-validation-field="${validationData}">${optionHtmlResult}</select>
                    </div>
            </div>
            <span class="ko-inline-msg-err ${validationCssClass}">${validationMessage}</span>`;

            return template;
        };
    }

    /**
     * Handle when user change value in dropdown.
     * 
     * @static
     */
    static onValueChange() {

    }
}

export default DTDropdownColumn;
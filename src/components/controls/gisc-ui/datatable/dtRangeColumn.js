import ko from "knockout";
import DTBaseColumn from './dtBaseColumn';
import Utility from "../../../../app/frameworks/core/utility";

/**
 * Editable range text box as DataTable column
 * 
 * @class DTRangeColumn
 * @extends {DTBaseColumn}
 */
class DTRangeColumn extends DTBaseColumn {

    /**
     * Creates an instance of DTRangeColumn.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        // Specific property.
        var minProp = params.dataMin;
        var maxProp = params.dataMax;
        var validationData = params.validationData;

        // Must define render function here instead of ES6 public method.
        var self = this;
        this.render = function(data, type, row) {
            // Extract min-max value.
            var minVal = Utility.getProperty(row, minProp);
            var maxVal = Utility.getProperty(row, maxProp);

            // Verify validation error message.
            var hasValidation = false;
            var validationMessage = "";
            var validationCssClass = "hidden";

            if(!_.isNil(validationData)) {
                hasValidation = true;
                validationMessage = Utility.getProperty(row, validationData);

                if(!_.isEmpty(validationMessage)) {
                    // Remove hidden class for visible message.
                     validationCssClass = "";
                }
            }

            // Return composed HTML code.
            return self.getTextBoxHtml(minProp, minVal, hasValidation) +
                "<span class=\"dt-rangebox-separator\">-</span>" + 
                self.getTextBoxHtml(maxProp, maxVal, hasValidation) +
                `<span class="ko-inline-msg-err ${validationCssClass}">${validationMessage}</span>`;
        };
    }

    /**
     * Prepare textbox HTML.
     * 
     * @param {any} targetField
     * @param {any} value
     * @param {boolean} [hasValidation=false]
     * @returns
     * 
     * @memberOf DTRangeColumn
     */
    getTextBoxHtml(targetField, value, hasValidation = false) {
        return `<input type="text" class="dt-rangebox" data-validation="${hasValidation}" data-target-field="${targetField}" value="${value}" />`;
    }
}

export default DTRangeColumn;
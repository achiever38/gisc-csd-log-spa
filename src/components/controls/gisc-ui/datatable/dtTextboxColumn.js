import ko from "knockout";
import DTBaseColumn from './dtBaseColumn';
import Utility from "../../../../app/frameworks/core/utility";

/**
 * Editable textbox as DataTable column
 * 
 * @class DTTextboxColumn
 * @extends {DTBaseColumn}
 */
class DTTextboxColumn extends DTBaseColumn {
    
    /**
     * Creates an instance of DTTextboxColumn.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.maxLength = Utility.extractParam(params.maxlength);

        // Must define render function here instead of ES6 public method.
        var self = this;
        this.render = function(data, type, row) {
            let renderableDataText = data;
            if(_.isNil(renderableDataText)) {
                renderableDataText = "";
            }
            let node = '<input type="text" class="dt-textbox" ' + 
                'data-target-field="' + self.data + '" ' +
                'value="' + renderableDataText +'"'+ (self.maxLength ? ' maxlength="'+ self.maxLength +'" ' : '')
                + '/>';
            return node;
        }
    }
}

export default DTTextboxColumn;
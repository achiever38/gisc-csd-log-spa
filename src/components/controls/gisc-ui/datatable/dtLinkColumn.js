﻿import ko from "knockout";
import DTBaseColumn from './dtBaseColumn';
import Utility from "../../../../app/frameworks/core/utility";

/**
 * Label as DataTable Column
 * 
 * @class DTLabelColumn
 * @extends {DTBaseColumn}
 */
class DTLinkColumn extends DTBaseColumn {
    
    /**
     * Creates an instance of DTLabelColumn.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.columnId = Utility.extractParam(params.columnId, "imbCol");
        this.onClick = Utility.extractParam(params.onClick, null);

        // Bridge to gisc DataTable
        this.table.registerColumnEvents(this.columnId, "click", this.onClick);

        var self = this;
        this.render = function(data, type, row) {
            let node = '<div class="dt-link-column" data-column-id="';
            node += self.columnId +'">';
            node += data + '</div>';

            return node;
        }
    }
}

export default DTLinkColumn;
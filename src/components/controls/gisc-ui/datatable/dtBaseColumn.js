import ko from "knockout";
import Utility from "../../../../app/frameworks/core/utility";

/**
 * Base class for all columns specific to gisc-ui-datatable
 * 
 * @class DTBaseColumn
 */
class DTBaseColumn {
    
    /**
     * Creates an instance of DTBaseColumn.
     * 
     * @param {any} params
     */
    constructor(params) {
        this.table = params.table; // requires

        this.columnIndexInternal = Utility.extractParam(params.columnIndexInternal, 0);
        this.type = Utility.extractParam(params.type);
        this.title = Utility.extractPossibleI18NParam(params.title, "");
        this.data = Utility.extractParam(params.data);
        this.visible = Utility.extractParam(params.visible, true);
        this.searchable = Utility.extractParam(params.searchable, false);
        this.orderable = Utility.extractParam(params.orderable, true);
        this.orderData = Utility.extractParam(params.orderData, null);
        this.sType = Utility.extractParam(params.sType, "string"); // string, numeric, date and html (which will strip HTML tags before sorting)
        this.className = Utility.extractParam(params.className, null);

        if(_.isNil(this.orderData)) {
            delete this.orderData;
        }

        if(_.isNil(this.className)) {
            delete this.className;
        }

        if(!_.isNil(params.width)) {
            this.width = params.width;
        }

        // Must define render function here instead of ES6 public method.
        this.render = function(data, type, row) {
            return data;
        }
    }
}

export default DTBaseColumn;
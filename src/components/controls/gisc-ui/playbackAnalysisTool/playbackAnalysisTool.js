﻿import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./playbackAnalysisTool.html";
import Logger from "../../../../app/frameworks/core/logger";
import "am4core";
import "am4charts";
import "am4themes_animated";

class PlaybackAnalysisTool extends ControlBase {
	constructor(params) {
		super(params);
		this.id = this.ensureNonObservable(this.id);
		this.xyChartId = this.id + "-xy-chart";
		this.ganttChartId = this.id + "-gantt-chart";
		this.data = this.ensureObservable(params.data, []);
		this.onPlaybackAnalystToolZoom = this.ensureFunction(params.onPlaybackAnalystToolZoom, () => {});
		this.onClickEvent = this.ensureFunction(params.onClickEvent, () => {});
		
		this.listenerEvent = this.ensureObservable(params.listenerEvent, {});
		this.enableChart = this.ensureObservable(params.enableChart, "gantt");
		this.isGanttChart = ko.observable(true);
		this.chart = null;
		this.xAxis = null;
		this.currChart = ko.observable("gantt");
	}

	afterRender(element) {
		// Themes begin
		am4core.useTheme(am4themes_animated);
		//first init gannt chart
		this.createGanttChart();
		// this.enableChart.subscribe((data)=>{
			
		// 	if(data.enableChart == "xy"){
		// 		this.isGanttChart(false);
		// 		this.createXYChart();
		// 		this.chart.events.on("ready", ()=> {
		// 			this.zoomToDates(data);
		// 		});
		// 	}else if(data.enableChart == "gantt"){
		// 		this.isGanttChart(true);
		// 		this.createGanttChart();
		// 		this.chart.events.on("ready", ()=> {
		// 			this.zoomToDates(data);
		// 		});
				
		// 	}
		// });
		
		this.listenerEvent.subscribe((data)=>{
			if(data.enableChart == "xy"){
				this.isGanttChart(false);
				this.createXYChart();
				this.zoomToDates(data);
				this.currChart("xy");
				
				// this.chart.events.on("ready", ()=> {
				// 	this.zoomToDates(data);
				// });
			}else if(data.enableChart == "gantt"){
				this.isGanttChart(true);
				//when current chart is xy create gantt chart
				if(this.currChart() == "xy"){
					this.createGanttChart();
					this.currChart("gantt");
				}
				this.chart.events.on("ready", ()=> {
					this.zoomToDates(data);
				});
				this.zoomToDates(data);
			}
		});
		
	}

	zoomToDates(data){
		if(data.mode == "zoomOut"){
			this.xAxis.zoom({start:0,end:1});
		}else{
			this.xAxis.zoomToDates(
				data.dateTime.startDate,
				data.dateTime.endDate
			);
		}
	}

	createXYChart() {
		// Create chart instance
		this.chart = am4core.create(this.xyChartId, am4charts.XYChart);

		// Create Data
		let newData = this.data()["features"];
		this.chart.data = newData;
	
		// Create axes
		this.xAxis = this.chart.xAxes.push( new am4charts.DateAxis() );
		this.xAxis.renderer.minGridDistance = 60;
		this.xAxis.dataFields.date = "dateTime";


		
		let yAxis = this.chart.yAxes.push( new am4charts.CategoryAxis() );
		yAxis.dataFields.category = "featureName";
		yAxis.renderer.minGridDistance = 1;
		yAxis.renderer.inversed = true;
		

		// Create series
		var series = this.chart.series.push(new am4charts.ColumnSeries());
		series.dataFields.categoryY = "featureName";
		series.dataFields.dateX = "dateTime";
		series.dataFields.valueX = "dateTime";
		series.dataFields.relatesFeature = "relatesFeature";
		series.dataFields.formatDateTime = "formatDateTime";
		series.dataFields.businessUnitName = "businessUnitName";
		series.dataFields.driverName = "driverName";
		series.dataFields.engine = "engine";
		series.dataFields.gpsStatus = "gpsStatus";
		series.dataFields.movement = "movement";
		series.dataFields.speed = "speed";
		series.dataFields.latitude = "latitude";
		series.dataFields.longitude = "longitude";
		series.strokeOpacity = 0;
		series.cursorTooltipEnabled = false;
		series.columns.template.disabled = true;
		// series.sequencedInterpolation = true;
		// series.columns.template.propertyFields.fill = "color";
        // series.columns.template.propertyFields.stroke = "color";
		// series.columns.template.strokeOpacity = 1;
		// series.columns.template.height = 0.5;
		// series.columns.template.width = am4core.percent(100);

		// var bullet = series.bullets.push(new am4charts.Bullet());
		// var square = bullet.createChild(am4core.Rectangle);
		// square.width = 2;
		// square.height = 2;

		var bullet = series.bullets.push(new am4charts.Bullet());
		bullet.propertyFields.fill = "color";
		bullet.stroke = "rgba(255, 0, 0, 0)";
		var square = bullet.createChild(am4core.Rectangle);
		square.width = 3;
		square.height = 10;
		square.horizontalCenter = "middle";
		square.verticalCenter = "middle";

		// var rectangle = bullet.createChild(am4core.Rectangle);
		// rectangle.width = 10;
		// rectangle.height = 10;
		// // rectangle.rotation = 45;
		// // rectangle.stroke = am4core.color("#fff");


		// series.heatRules.push({
		// 	target: rectangle,
		// 	min: 1,
		// 	// max: 10,
		// 	// property: "scale"
		// });
		
		// bullet.tooltipHTML = "";
		bullet.adapter.add("tooltipHTML", (text, target, key) => {
			let html = "";
			if(target.dataItem){
				let field = target.dataItem;
				let listFeature = field.relatesFeature;
				if(!_.isNil(field.formatDateTime)){
					html = `<div style="width:200px; padding:2px">`;
					html += `
						<div>
							<span style="color:#000">${ field.formatDateTime }</span>
						</div>
						<div>
							<img style="width:32px;float:left;margin-right:15px" src="${ this.getImage("movement", field.movement) }"/>
							<img style="width:32px;float:left;margin-right:15px" src="${ this.getImage("engine", field.engine) }"/>
							<img style="width:32px;float:left;margin-right:15px" src="${ this.getImage("gpsStatus", field.gpsStatus) }"/>
						</div>
						<br />
						<br />
						<div>
							<span style="float:left; color:#000">${ this.i18n("Common_BusinessUnit")() }</span>
							<span style="float:right; color:#000">${ field.businessUnitName }</span>
						</div>
						<br />
						<div>
							<span style="float:left; color:#000">${ this.i18n("Common_Driver")() }</span>
							<span style="float:right; color:#000">${ (field.driverName) ? field.driverName : "-" }</span>
						</div>
						<br />
						<div>
							<span style="float:left; color:#000">${ this.i18n("Common_Speed")() }</span>
							<span style="float:right; color:#000">${ field.speed }</span>
						</div>
						<br />
					`;
					if(_.size(listFeature)){
						// html += `<div>`;
						_.forEach(listFeature, (name) => {
							html += `
									<span style="float:left; color:#000">${ name }</span>
									<span style="float:right; color:#000">${ this.i18n("Common_Open")() }</span>
									<br />
									`;
						});
						// html += `</div>`;
					}
					html += `</div>`;
				}
				
			}
			return html;
		});

		bullet.events.on("hit", (event) => {
			let data = {
				latitude: event.target.dataItem.latitude,
				longitude: event.target.dataItem.longitude
			}
			this.onClickEvent(data);
		});

		series.tooltip.getFillFromObject = false;
		series.tooltip.background.fill = am4core.color("#FFFFFF");
		

		this.chart.zoomOutButton.disabled = true;

		// Add scrollbars
		//chart.scrollbarX = new am4core.Scrollbar();
		//chart.scrollbarY = new am4core.Scrollbar();

		// Add cursor
		// chart.cursor = new am4charts.XYCursor();
		// chart.cursor.behavior = "zoomXY";

		// var scrollbarX = new am4charts.XYChartScrollbar();
		// scrollbarX.series.push(series);
		// chart.scrollbarX = scrollbarX;

		// Pre-zoom the chart
		// this.chart.events.on("ready", ()=> {
		// 	this.listenerEvent.subscribe((data)=>{

		// 		if(data.mode == "zoomOut"){
		// 			this.xAxis.zoom({start:0,end:1});
		// 		}else{
		// 			this.xAxis.zoomToDates(
		// 				data.dateTime.startDate,
		// 				data.dateTime.endDate
		// 			);
		// 		}
				
		// 	})
		// });

	}

	createGanttChart() {
		
		this.chart = am4core.create(this.ganttChartId, am4charts.XYChart);
		this.chart.hiddenState.properties.opacity = 0; 

		let newData = this.data()["featurePeriods"];
		this.chart.data = newData;

		var yAxis = this.chart.yAxes.push(new am4charts.CategoryAxis());
		yAxis.dataFields.category = "name";
		yAxis.renderer.minGridDistance = 1;
		// yAxis.renderer.grid.template.location = 0;
		yAxis.renderer.inversed = true;
		// yAxis.renderer.grid.template.opacity = 0;

		this.xAxis = this.chart.xAxes.push(new am4charts.DateAxis());
		this.xAxis.renderer.minGridDistance = 60;
		
		this.xAxis.renderer.grid.template.paddingTop = 0;
		this.xAxis.renderer.grid.template.paddingBottom = 0;
		this.xAxis.renderer.grid.template.marginTop = 0;
		this.xAxis.renderer.grid.template.marginBottom = 0;
		

		var series = this.chart.series.push(new am4charts.ColumnSeries());
		// series.columns.template.width = am4core.percent(80);
		series.columns.template.minHeight = 10;
		series.columns.template.height = 10;
		series.columns.template.tooltipText = "{name}: {formatStartDateTime} - {formatEndDateTime}";
		series.tooltip.getFillFromObject = false;
        series.tooltip.background.fill = am4core.color("#FFFFFF");
		series.tooltip.label.fill = am4core.color("#000000");
		
		series.dataFields.openDateX = "startDateTime";
		series.dataFields.dateX = "endDateTime";
		series.dataFields.categoryY = "name";
		series.dataFields.formatStartDateTime = "formatStartDateTime";
		series.dataFields.formatEndDateTime = "formatEndDateTime";
		series.columns.template.propertyFields.fill = "color"; // get color from data
		series.columns.template.propertyFields.stroke = "color";
		series.columns.template.strokeOpacity = 1;
		this.chart.zoomOutButton.disabled = true;
	}

	getImage(type, status){
		//type = engine, gpsStatus, movement
		//status = 1, 2, 3, 4
		let imgPath = this.resolveUrl("~/gis-js/images/");
		let vehiclePath = imgPath + "/vehicle_status/";
		let fullPath = "";
		
		switch(type){
			case "engine":
				if(status == "1"){
					fullPath = vehiclePath + "icon_nearest_assets_engine_on@3x.png";
				}else{
					fullPath = vehiclePath + "icon_nearest_assets_engine_off@3x.png";
				}
				break;
			case "gpsStatus":
				if(status == "1"){
					fullPath = vehiclePath + "icon_nearest_assets_gsm_bad@3x.png";
				}else if(status == "2"){
					fullPath = vehiclePath + "icon_nearest_assets_gsm_medium@3x.png";
				}else{
					fullPath = vehiclePath + "icon_nearest_assets_gsm_good@3x.png";
				}
				break;
			case "movement":
				if(status == "1"){
					fullPath = imgPath + "icon_vehicle_move.png";
				}else if(status == "2"){
					fullPath = imgPath + "icon_vehicle_stop.png";
				}else if(status == "3"){
					fullPath = imgPath + "icon_vehicle_park.png";
				}else{
					fullPath = imgPath + "icon_vehicle_park_engine_on.png";
				}
				break;
		}
		return fullPath;
	}
}

export default {
	viewModel: PlaybackAnalysisTool,
	template: templateMarkup
};
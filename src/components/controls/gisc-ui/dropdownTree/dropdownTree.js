import ko from "knockout";
import templateMarkup from "text!./dropdownTree.html";
import ControlBase from "../../controlbase";

class DropdownTree extends ControlBase {
    constructor(params) {
        super(params);

        // Properties
        this.options = this.ensureObservable(params.options);
        this.optionsID = this.ensureNonObservable(params.optionsID);
        this.optionsText = this.ensureNonObservable(params.optionsText);
        this.optionsParent = this.ensureNonObservable(params.optionsParent);
        this.optionsChildren = this.ensureNonObservable(params.optionsChildren);
        this.disableNodes = this.ensureObservable(params.disableNodes);
        this.mode = this.ensureNonObservable(params.mode, "view");
        this.autoSelection = this.ensureObservable(params.autoSelection, true);
        this.zIndex = this.ensureObservable(params.zIndex, '5');

        //รับ obj ที่เป็น i18n มา  และใช้แบบ observable
        this.optionsCaption = params.optionsCaption==undefined ||params.optionsCaption==" " ?ko.observable(null):params.optionsCaption;
        
        this.value = this.ensureObservable(params.value);
        this.name = ko.observable(null);

        this.placeholderFilterText = ko.observable(this.i18n("Common_Search")());

        this.onClickEventRef = null;
        this.clearSelectedItemRef = null;

        this.zIndex = this.ensureNonObservable(params.zIndex, null);


        this.isForcedSetValue = false;

        this.arrayValue = ko.observableArray();
        this.arrayValueSubscribe = this.arrayValue.subscribe((newValue) => {


            if (newValue) {
                var n = newValue;
                var n2 ;
                if (this.mode === 'update') {
                    this.hasSelected(true);
                  
                    if (n.length < 2) {
                        this.name(this.findNodeName(this.options(), n[0]));
                    } else {
                        n2 = n.filter(item => item!=0) //ไม่ต้องการ 0
                        this.name(n2.length + " " + this.i18n("Common_Selected")());
                    }

                    this.isForcedSetValue = true;
                    if (Array.isArray(n)) {
                        this.value(n);
                    }
                    else {
                        this.value(n[0]);
                        
                    }
                    //
                } else {

                    this.hasSelected(true);
                    this.name(this.findNodeName(this.options(), n[0]));
                    this.value(n[0]);
                }
            }

        });
        this.visibleCloseBtn = ko.observable();
        this.selectedValueSubscribe = this.value.subscribe((newValue) => {

            if (!this.isForcedSetValue){
                //Enable Binding Value in the Subscribe Event (Outside Onload Event)
                if (typeof newValue === 'object') {
                    if (newValue != null && newValue.value != undefined) {
                        this.arrayValue([newValue.value]);
                    }
                    else if (Array.isArray(newValue)) {
                        this.arrayValue(newValue);
                    }
                    else if (newValue == null) {
                        this.arrayValue("");
                        this.name(null);
                    }
                    this.isDropdownOpened(false);
                }
                else {
                    if (!newValue) {
                        this.arrayValue("");
                        this.name(null);
                        this.visibleCloseBtn(false);
                    } else {
                        this.visibleCloseBtn(true);
                    }
                    this.isDropdownOpened(true);
                }

                
            }
            else {
                this.isForcedSetValue = false;
            }

        });


        this.isDropdownOpened = ko.observable(false);
        this.hasSelected = ko.observable();

        this._enableSubscribe = this.enable.subscribe((value) => {
            var $el = $("#" + this.id);
            var $clear = $(".app-remove-icon" + "." + this.id);
            if (value) {
                $el.on('click', this.onClickEventRef);
                $clear.on("click", this.clearSelectedItemRef);
            } else {
                $el.off('click', this.onClickEventRef);
                $clear.off("click", this.clearSelectedItemRef);
            }
        });

        //ถ้าส่ง All มาจะแสดงตรงนี้ optionsCaption
        this.name.subscribe((res)=>{
           if(!this.optionsCaption()) return
            if(res === undefined || res === null){
                this.name(this.optionsCaption())
            }
        });



        this.isFocus = ko.observable(false);
        this.subFocus = this.isFocus.subscribe((newValue)=> {
            // new valule change logic
            if(!this.enable()){
                this.isFocus(false);
            }
        });

    }

    findNodeName(object, id) {
        var name;
        _.each(object, (obj) => {
            var nodeID = this.ensureNonObservable(obj[this.optionsID]);
            if (nodeID == id) {
                name = this.ensureNonObservable(obj[this.optionsText]);
                // console.log("Found at parent: ",name);
            } else {
                var child = obj[this.optionsChildren];
                if (child && child.length > 0 && !name) {
                    name = this.findNodeName(child, id);
                    // console.log("Found at child: ",name);
                }
            }
        });
        return name;
    }

    onClick(e) {
        //Check if click input search will not close dropdown
        var filterId = (e.target) ? e.target.id : null;
        if(e.type === "click" || e.charCode === 32){
            if(this.hasSelected() === true){
                //for dropdown treeview checkbox
                if(this.mode === 'update'){
                    this.isDropdownOpened(true);
                }else{
                    this.isDropdownOpened(false);
                }
                this.hasSelected(false);
            }
            else{
                // //click input search
                if(filterId === this.id + "FilterText"){
                    this.isDropdownOpened(this.isDropdownOpened());
                }else{
                    this.isDropdownOpened(!this.isDropdownOpened());
                }        
            }
            
        }
        
    }

    clearSelectedItem(e) {
        this.value(null);
        this.isDropdownOpened(false);
        e.stopPropagation();
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var h = !this.value() ? false : true;
        this.visibleCloseBtn(h);
        this.name(this.findNodeName(this.options(), this.value()));
        if (this.value()) {
            if (Array.isArray(this.value())) {
                this.arrayValue(this.value());
            }
            else {
                this.arrayValue([this.value()]);
            }
            
        }
    }

    onDomReady() {
        // if(!this.enable()) return;

        var self = this;

        this.onClickEventRef = this.onClick.bind(this);
        this.clearSelectedItemRef = this.clearSelectedItem.bind(this);

        if (this.enable()) {
            var $el = $("#" + self.id);
            var $clear = $(".app-remove-icon" + "." + self.id);
            $el.on('click keypress', this.onClickEventRef);
            $clear.on("click", this.clearSelectedItemRef);

            $el.focusin((e)=>{
                self.hasSelected(false);
            });
            $el.focusout((e) => {
                // Check if the original click comes from children or outside DOM tree.
                var isInnerElement = $el.find(e.relatedTarget).length > 0;
                var selectedId = (e.relatedTarget) ? e.relatedTarget.id : null;

                //fix dropdown is open 2 times
                if(selectedId == self.id) {
                    self.isDropdownOpened(true);
                }

                if(!isInnerElement && selectedId != self.id) {
                    self.isDropdownOpened(false);
                }
            });
        }

        $("#" + self.id +"FilterText").keyup(function (e) {
            var filterText = $(this).val().toLowerCase();
            if(filterText.length !== ""){
                $(this).parent().find("li").hide();
                $(this).parent().find(".jstree-anchor").each(function() {
                    let text = $(this).text().toLowerCase();
                    if(text.match(filterText)) {
                        $(this).show();
                    }   
                });
                $(this).parent().find(".jstree-anchor").each(function() {
                    let text = $(this).text().toLowerCase();
                    if(text.match(filterText)) {
                        $(this).parents("ul, li").each(function () {
                            $(this).show();
                        });
                    }   
                }); 
            }
            else {
                //if not filterText null reset treeview
                $(this).parent().find("li").show();
            }
        }).on('search', function(e) {
            //clear value and tree view
            $(this).val(null);
            $(this).parent().find("li").show();
            e.stopPropagation();
        });
        
    }

    onUnload() {
        var $el = $("#" + self.id);
        var $clear = $(".app-remove-icon" + "." + this.id);

        this._enableSubscribe.dispose();
        this.arrayValueSubscribe.dispose();

        $el.unbind();
        $clear.unbind();
        this.value = null;
        this.options = null;
        this.hasSelected = null;
    }
}

export default {
    viewModel: DropdownTree,
    template: templateMarkup
};
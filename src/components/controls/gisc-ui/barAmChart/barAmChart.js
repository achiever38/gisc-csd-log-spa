﻿import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./barAmChart.html";
import "amcharts";
import "amcharts-serial"

class BarAmChart extends ControlBase {
    constructor(params) {
        super(params);
        
        this.data = this.ensureObservable(params.data);
        this.nameChart = this.ensureObservable(params.nameChart);
        this.valueAxesGuide = this.ensureObservable(params.valueAxesGuide, null);
        this.valueAxesCustomMaximum = this.ensureObservable(params.valueAxesGuide, false);
        this.formatBalloonText = this.ensureObservable(params.formatBalloonText, null);    
        this.categoryLabelRotateAngle = this.ensureObservable(params.categoryLabelRotateAngle, 0);
        this.showChartScrollbar = this.ensureObservable(params.showChartScrollbar, false);
        this.disabledZoomOut = this.ensureObservable(params.disabledZoomOut, false);
        this.initialZoom = this.ensureObservable(params.initialZoom, null);
        this.hilightClickItem = this.ensureObservable(params.hilightClickItem, false);
        this.hilightColorClickItem = this.ensureObservable(params.hilightColorClickItem, "#FF0000");
        this.fixedColumnWidth = this.ensureObservable(params.fixedColumnWidth, null);
        this.isCategoryAxisLabelTruncate = this.ensureObservable(params.isCategoryAxisLabelTruncate, false);
        this.textontopchart = this.ensureObservable(params.textontopchart,null)
        this.isShowBallonText = this.ensureObservable(params.isShowBallonText, false);
        this.chartTitle = this.ensureObservable(params.chartTitle,null)
        this.categoryTitle = this.ensureObservable(params.categoryTitle,null)
       //console.log("AmchartData",this.data())

        this.onClickBarChart = this.ensureFunction(params.onClickBarChart, null);
       

        this.barChart = null;

        this.animation = this.ensureObservable(params.animation, true);

        this.previousClickObject = null;
        this.previousClickObjectColor = null;
    }

    afterRender(element) {
        this.checkData()  
        this.data.subscribe(()=> {
            this.checkData()  
        })

        this.nameChart.subscribe(() => {
            this.checkData()  
        })
    }

    checkData() {
        var self = this;
        if(this.data() != undefined) {
            if(this.data()['length'] != 0) {
                this.createChart();
            } else {
                $("#" + self.id).html(this.i18n('Common_DataNotFound')())
            }         
        }
    }

    createChart() {
        var self = this;

        if (this.hilightClickItem()){
            for (var i = 0; i < this.data().length; i++){
                this.data()[i]["lineColor"] = "transparent";
            }
        }

        
        var chart = {
            "type": "serial",
            "dataProvider": this.data(),
            "valueAxes": [{
              "gridColor": "#FFFFFF",
              "gridAlpha": 0.2,
              "dashLength": 0,
              "minimum": 0,
              "titleFontSize":10,
              "title": this.nameChart()
            }],
            // "allLabels":[{
            //     "text": this.chartTitle(),
            //     "bold": true,
            //     "align": "center",
            // }],
            "gridAboveGraphs": true,
            "startDuration": 1,
            "graphs": [{
                "balloonText": "<b>[[category]]: [[value]]</b>",
                "fillColorsField": "color",
                "labelText": "[[labelText]]",//"[[percent]]% ([[value]])",
                "fillAlphas": 0.8,
              //"lineColorField": "lineColor",
              //"lineThickness": 5,
              //"bulletSize": 50,
              //"customBulletField": "bullet",
              "type": "column",
              "valueField": "value",
              "showBalloon":this.isShowBallonText() ,
            //   "labelRotation":50,
              "fontSize":10
            }],
            "chartCursor": {
              "categoryBalloonEnabled": false,
              "cursorAlpha": 0,
              "zoomable": false
            },
            "categoryField": "name",
            "categoryAxis": {
                "gridPosition": "start",
                "gridAlpha": 0,
                "autoRotateAngle": this.categoryLabelRotateAngle(),
                "autoRotateCount": 1,
                // "title": this.nameChart()
            },
        };

        if (this.isCategoryAxisLabelTruncate()){

            chart.categoryAxis = {
                "gridPosition": "start",
                "gridAlpha": 0,
                "autoRotateAngle": this.categoryLabelRotateAngle(),
                "autoRotateCount": 1,
                "labelFunction":(valueText) =>{
                    return valueText.length > 15? valueText.substring(0, 15) + '...':valueText
                  }
            }

        }

        // this.chartTitle
        if(this.categoryTitle() != null)
        {
            chart.categoryAxis.title = this.categoryTitle();
            chart.categoryAxis.titleFontSize= 10;
        }
        if(this.chartTitle() != null){
            chart.allLabels = [{
                "text": this.chartTitle(),
                "bold": true,
                "align": "center",
            }]
            
        }
        if (this.hilightClickItem()){
            chart.graphs[0]["lineColorField"] = "lineColor";
            chart.graphs[0]["lineThickness"] = 5;
        }

        
        var maxVal = this.getMaxVal(this.data(), "value");
        chart.valueAxes[0].maximum = maxVal+5
        if (maxVal <= 5){
            chart.valueAxes[0].maximum = 6;
            chart.valueAxes[0].autoGridCount = false;
        }
       
        if (this.fixedColumnWidth()) {
            chart.graphs[0]["fixedColumnWidth"] = this.fixedColumnWidth();
        }

        if(!this.animation()){
            chart.startDuration = 0
        }

        if(this.formatBalloonText() != null || this.formatBalloonText() != undefined) {
            chart.graphs = [{
                "fillColorsField": "color",
                "balloonText": this.formatBalloonText(),
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "type": "column",
                "labelText": this.textontopchart(),
                "valueField": "value"
              }]
        }
        
        if (this.valueAxesGuide()) {
            for (let i = 0; i < chart.valueAxes.length; i++){
                chart.valueAxes[i].guides = this.valueAxesGuide()[i];
            }
        }

        if (this.valueAxesCustomMaximum()){
            chart.valueAxes[0].maximum = 100;
        }


        if (this.showChartScrollbar()) {
            chart.chartScrollbar = {
                "graph": "scrollbar",
                "graphType": "column",
                "resizeEnabled": true,
                "scrollbarHeight": 30
            }

            chart.graphs.push({
                "id": "scrollbar",
                // "balloonText": "<b>[[category]]: [[value]]</b>",
                "showBalloon":false,
                "valueField": "value",
                "lineAlpha": 0
            });
        }

   

        if (this.disabledZoomOut()) {
            chart.zoomOutButtonImageSize = 0;
            chart.zoomOutButtonPadding = 0;
            chart.zoomOutText = '';
        }

        this.barChart = AmCharts.makeChart(self.id, chart);

        var self = this;
        this.barChart.addListener('clickGraphItem',  (e) => {
            if (self.hilightClickItem()) {
                if (self.previousClickObject) {
                    self.previousClickObject.dataContext.lineColor = "transparent";
                }

                e.item.dataContext.lineColor = self.hilightColorClickItem();
                self.previousClickObject = e.item;


                var startIndex = e.chart.startIndex;
                var endIndex = e.chart.endIndex;

                e.chart.validateData();
                e.chart.zoomToIndexes(startIndex, endIndex);

            }      

           // console.log("clickGraph",e);
            //e.item.dataContext.lineColor = "#FF0000";
            //e.item.dataContext.bullet = "https://image.ibb.co/m3WqqT/Arrow_down.png"
           //.chart.validateData();
            
            self.onClickBarChart(e);
        });


        if (this.isCategoryAxisLabelTruncate()) {
            this.barChart.categoryAxis.addListener("rollOverItem", (event) => {
                event.target.setAttr("cursor", "default");
                event.chart.balloon.followCursor(true);
                event.chart.balloon.showBalloon(event.serialDataItem.dataContext.name);
            });

            this.barChart.categoryAxis.addListener("rollOutItem", (event) => {
                event.chart.balloon.hide();
            });
        }

        if (this.initialZoom()){
            this.barChart.zoomToIndexes(this.initialZoom()[0], this.initialZoom()[1]);
        }


        // barChart.addListener('rollOverSlice', function(e) {
        //     barChart.clickSlice(e.dataItem.index);
        // });

        // barChart.addListener('rollOutSlice', function(e) {
        //     barChart.clickSlice(e.dataItem.index);
        // });

        // barChart.addListener('clickSlice', function(e) {
            
        //     if(e.event != undefined) { // check mouseClick
        //         self.onClickPieChart(e);
        //         e.chart.validateData();
        //     } else { }
            
        // });
    }

    zoomToIndexes(startIndex, endIndex) {
        this.barChart.zoomToIndexes(startIndex, endIndex);
    }

    onLoad() {

    }

    onUnload() {
        
    }
    getMaxVal(arr, prop) { //get max value in json array
        let max;
        for (let i=0 ; i<arr.length ; i++) {
            if (!max || parseInt(arr[i][prop]) > parseInt(max[prop]))
                max = arr[i];
        }
        return max.value;
    }
}

export default {
viewModel: BarAmChart,
    template: templateMarkup
};

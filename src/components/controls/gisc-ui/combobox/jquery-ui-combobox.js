import "jquery"
import "jquery-ui";

// Define $.combobox() only once.
// This jq-ui widget is to be used by gisc-ui-combobox controls.
if (!$.combobox) {
    $(function() {
        $.widget("gisc.combobox", {
            _create: function() {
                this.id = this.element.attr("id");
                this.wrapper = $("<div>")
                    .attr('id', this.id)
                    .addClass("app-combobox")
                    .addClass("app-combobox-wrapper")
                    .addClass("app-input")
                    .insertAfter(this.element);

                this.element.hide();
                this._createAutocomplete();
                this._createShowAllButton();


            },

            _createAutocomplete: function() {
                var selected = this.element.children(":selected"),
                    value = selected.val() ? selected.text() : "";

                var inputPlaceHolder = $(this.element).attr("data-input-placeholder");
                var isElementDisabled = $(this.element).is(":disabled");

                this.input = $("<input>")
                    .appendTo(this.wrapper)
                    .val(value)
                    .attr("title", "")
                    .attr("type", "text")
                    .addClass("app-combobox-input")
                    .autocomplete({
                        delay: 0,
                        minLength: 0,
                        appendTo: "div[id = "+this.id+"]",
                        source: $.proxy(this, "_source")
                    });
                if (inputPlaceHolder) {
                    this.input.attr("placeholder", inputPlaceHolder);
                    this.input.val("")
                    this.element.val("");
                    this.input.autocomplete("instance").term = "";
                }
                if (isElementDisabled) {
                    this.input.prop("disabled", true);
                }

                this._on(this.input, {
                    autocompleteselect: function(event, ui) {
                        ui.item.option.selected = true;
                        this._trigger("select", event, {
                            item: ui.item.option
                        });
                        // Need to manually trigger this event to chain to Knockout
                        this.element.change();
                    },

                    autocompletechange: "_removeIfInvalid"
                });
            },

            _createShowAllButton: function() {
                var input = this.input,
                    wasOpen = false;
                var isElementDisabled = $(this.element).is(":disabled");

                $("<a>")
                    .attr("tabIndex", -1)
                    .addClass("app-combobox-arrow-normal")
                    .text("")
                    .attr("title", "Show All Items")
                    .appendTo(this.wrapper)
                    .on("mousedown", function() {
                        wasOpen = input.autocomplete("widget").is(":visible");
                    })
                    .on("click", function() {
                        if (isElementDisabled) return;

                        input.trigger("focus");

                        // Close if already visible
                        if (wasOpen) {
                            return;
                        }

                        // Pass empty string as value to search for, displaying all results
                        input.autocomplete("search", "");
                    });
            },

            _source: function(request, response) {
                var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                response(this.element.children("option").map(function() {
                    var text = $(this).text();
                    if (this.value && (!request.term || matcher.test(text)))
                        return {
                            label: text,
                            value: text,
                            option: this
                        };
                }));
            },

            _removeIfInvalid: function(event, ui) {

                // Selected an item, nothing to do
                if (ui.item) {
                    return;
                }

                // Search for a match (case-insensitive)
                var value = this.input.val(),
                    valueLowerCase = value.toLowerCase(),
                    valid = false;
                this.element.children("option").each(function() {
                    if ($(this).text().toLowerCase() === valueLowerCase) {
                        this.selected = valid = true;
                        return false;
                    }
                });

                // Found a match, nothing to do
                if (valid) {
                    return;
                }

                // Remove invalid value
                this.input
                    .val("")
                this.element.val("");

                this.input.autocomplete("instance").term = "";
            },

            _destroy: function() {
                this.wrapper.remove();
                this.element.show();
            }
        });
    });
}
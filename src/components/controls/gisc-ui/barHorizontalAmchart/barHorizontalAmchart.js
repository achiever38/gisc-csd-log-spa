import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./barHorizontalAmchart.html";
import "amcharts";
import "amcharts-serial"

class BarHorizontalAmchart extends ControlBase {
    constructor(params) {
        super(params);

        this.data = this.ensureObservable(params.data);
        this.nameChart = this.ensureObservable(params.nameChart);
        this.showChartScrollbar = this.ensureObservable(params.showChartScrollbar, false);
        this.initialZoom = this.ensureObservable(params.initialZoom, null);
        this.fixedColumnWidth = this.ensureObservable(params.fixedColumnWidth, null);
        this.isCategoryAxisLabelTruncate = this.ensureObservable(params.isCategoryAxisLabelTruncate, false);
        this.onClickHorizontalChart = this.ensureFunction(params.onClickHorizontalChart, null);
        this.valueAxesLabelShowPercentage = this.ensureObservable(params.valueAxesLabelShowPercentage, false);
        this.chartLegendData = this.ensureObservable(params.chartLegendData, null);
        this.chartLegendPosition = this.ensureObservable(params.chartLegendPosition, "bottom");
        this.horizontalAmchart = null;
        this.valueFieldName = this.ensureObservable(params.valueFieldName, 'value')

    }

    afterRender(element) {

        this.checkData()
        this.data.subscribe(() => {
            this.checkData()
        })

        this.nameChart.subscribe(() => {
            this.checkData()
        })
    }

    checkData() {
        var self = this;
        if (this.data() != undefined) {
            if (this.data()['length'] != 0) {
                this.createChart();
            } else {
                
                $("#" + self.id).html(this.i18n('Common_DataNotFound')())
            }
        }
    }

    createChart() {
        console.log("inCreateChart", this.data())
        var self = this;


        var chart = {
            "type": "serial",
            "dataProvider": this.data(),
            "categoryField": "name",
            "rotate": true,
            "startDuration": 1,
            "categoryAxis": {
                "gridPosition": "start"
            },
            // "chartCursor": {
            //     "enabled": true
            // },
            "trendLines": [],
            "graphs": [
                {
                    "balloonText": "<b>Pass:</b><span style='font-size:12px; color:grey;'> [[value]]% ([[score]])</span>",
                    // "balloonText": "<b>Pass:</b><span style='font-size:12px; color:grey;'> [[value]]% ([[valueForDisplay]])</span>",
                    "fillAlphas": 1,
                    "type": "column",
                    "valueField": this.valueFieldName(),
                    "fillColorsField": "color",
                    "labelPosition": "middle",
                    "labelText": "[[value]]%",
                    "fontSize": 15,
                    "color": "white"
                }
            ],
            "guides": [],
            "valueAxes": [
                {
                    "id": "ValueAxis-1",
                    "title": "",
                    "position": "top",
                    "autoGridCount": false,
                    "gridCount": 10,
                    "maximum": 100,
                    "minimum": 0     
                }
            ],
            "allLabels": [],
            "balloon": {}
            
        };

        if (this.fixedColumnWidth()) {
            chart.graphs[0]["fixedColumnWidth"] = this.fixedColumnWidth();
        }

        if (this.showChartScrollbar()) {
            chart.chartScrollbar = {
                "graph": "scrollbar",
                "graphType": "column",
                "resizeEnabled": true,
                "scrollbarHeight": 30
            }

            chart.graphs.push({
                "id": "scrollbar",
                "showBalloon": false,
                "valueField": this.valueFieldName(),
                "lineAlpha": 0
            });
        }

        if (this.valueAxesLabelShowPercentage()){
            chart.valueAxes[0].labelFunction = (valueText) =>{
                //console.log("valueText",valueText);
                return valueText + "%";
            }
        }

        if (this.isCategoryAxisLabelTruncate()){

            chart.categoryAxis = {
                "gridPosition": "start",
                "labelRotation": 45,
                // "gridAlpha": 0,
                // "autoRotateAngle": this.categoryLabelRotateAngle(),
                // "autoRotateCount": 1,
                "labelFunction":(valueText) =>{
                    return valueText.length > 15? valueText.substring(0, 15) + '...':valueText
                  }
            }

        }
        if(this.chartLegendData() != null || this.chartLegendData() != undefined) {
            chart.legend = {
                "position":this.chartLegendPosition(),
                "data":this.chartLegendData()
              }
        }




        this.horizontalAmchart = AmCharts.makeChart(self.id, chart);

        var self = this;

        this.horizontalAmchart.addListener('clickGraphItem', (e) => {

            self.onClickHorizontalChart(e);
        });

        if (this.initialZoom()) {
            this.horizontalAmchart.zoomToIndexes(this.initialZoom()[0], this.initialZoom()[1]);
        }


        if (this.isCategoryAxisLabelTruncate()) {
            this.horizontalAmchart.categoryAxis.addListener("rollOverItem", (event) => {
                event.target.setAttr("cursor", "default");
                event.chart.balloon.followCursor(true);
                event.chart.balloon.showBalloon(event.serialDataItem.dataContext.name);
            });

            this.horizontalAmchart.categoryAxis.addListener("rollOutItem", (event) => {
                event.chart.balloon.hide();
            });
        }
    }

   
    zoomToIndexes(startIndex, endIndex) {
        this.horizontalAmchart.zoomToIndexes(startIndex, endIndex);
    }

    onLoad() {


    }

    onUnload() {

    }

}

export default {
    viewModel: BarHorizontalAmchart,
    template: templateMarkup
};

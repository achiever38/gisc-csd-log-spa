
class FileUploadModel {
    constructor(){
        this.name = "";
        this.extension = "";
        this.type = "";
        this.size = 0;
        this.imageWidth = 0;
        this.imageHeight = 0;
        this.errorMessage = "";
    }
}

export default FileUploadModel;
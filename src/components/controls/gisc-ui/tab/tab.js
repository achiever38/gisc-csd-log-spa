﻿import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./tab.html";
import EventAggregator from "../../../../app/frameworks/core/eventAggregator";

/**
 * Handle tab interface for page.
 * @class Tab
 * @extends {ControlBase}
 */
class Tab extends ControlBase {
    /**
     * Creates an instance of Tab.
     * @public
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.viewModel = null; //params.viewModel;
        this.isViewModelInjected = ko.observable(false);
        this.items = ko.observableArray();
        this.selectedIndex = this.ensureObservable(params.selectedIndex, 0);
        this._eventAggregator = new EventAggregator();
    }

    /**
     * Hook from template when render node is completed
     * @public
     * @param {any} element
     * @param {any} parent
     */
    afterRender(element, parent) {
        this.viewModel = parent;

        // Parse item binding.
        this.items().forEach(function(item) {
            item.dataBind(parent);
        });

        this.isViewModelInjected(true);
    }

    /**
     * Handle switch tab from user inter action.
     * @public
     * @param {any} tabItem
     * @returns
     */
    onTabClick(tabItem) {
        // Return self click.
        if (tabItem.isSelected()) {
            return;
        }

        // Deselect all items.
        this.items().forEach(function(element) {
            if (element !== tabItem) {
                element.isSelected(false);
            } else {
                element.isSelected(true);
            }
        });

        // Update selected index.
        this.selectedIndex(this.items.indexOf(tabItem));

       // console.log('NOTE :: DataTable in TabControl EVT Refresh Trigger');
        // Update Datatable within tab content.
        $("#" + tabItem.id).find(".dt-storage").each((index, elm) => {
            var controlId = $(elm).val();
            this._eventAggregator.publish(controlId, { 
                eventName: "refresh",
                message: null
            });
        });
    }

    /**
     * Handle loading event.
     */
    onLoad() {}

    /**
     * Cleanup resource when dom is removed from page.
     */
    onUnload(){
        this._eventAggregator.destroy();
        this.items().forEach(function(element) {
            element.onUnload();
        });
    }
}

/**
 * TabItem class for render tab contents.
 * 
 * @class TabItem
 * @extends {ControlBase}
 */
class TabItem extends ControlBase {
    constructor(params) {
        super(params);
        this.element = params.element;
        this.text = this.ensureObservable(params.text, "");
        this.templateNodes = params.templateNodes;
        this.isSelected = ko.observable(false);
        this.bindingExpression = "";
        this.cssClasses = ko.pureComputed(() => {
            var cssClassString = "app-text-default";

            if (this.isSelected()) {
                cssClassString = "app-text-default ext-hubs-selectedtab";
            }

            return cssClassString;
        });
    }
    dataBind(viewModel) {
        var rawParameters = this.getBindingFromElement(this.element, viewModel);
        this.text = this.ensureObservable(rawParameters.text, "");
        this.visible = this.ensureObservable(rawParameters.visible, true);
    }
    onUnload(){
        this.element = null;
        this.text = null;
        this.isSelected = null;
    }
}

export default {
    viewModel: {
        createViewModel: function(params, componentInfo) {
            var vm = new Tab(params);
            // Parsing item templates node for each item.
            var i = 0;

            componentInfo.templateNodes.forEach(function(element) {
                switch (element.nodeName) {
                    case "ITEM":
                        var defaultTabItemId = vm.id + "_tab" + (i+1);
                        var newTabItem = new TabItem({
                            id: defaultTabItemId,
                            text: "Tab" + i,
                            templateNodes: element.children,
                            element: element
                        });

                        // Auto select first tab.
                        if (i === vm.selectedIndex()) {
                            newTabItem.isSelected(true);
                        }
                        vm.items.push(newTabItem);
                        i++;
                        break;
                }
            }, this);

            return vm;
        }
    },
    template: templateMarkup
};
import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./crumbtail.html";
import Logger from "../../../../app/frameworks/core/logger";
import BreadCrumb from "./collapsible-crumbtail";

/**
 * Handle top navigation menu.
 * 
 * @class Crumbtail
 * @extends {ControlBase}
 */
class Crumbtail extends ControlBase {
    /**
     * Creates an instance of Crumbtail.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.journey = this.ensureObservable(params.journey, null);
        this.engine = null;
    }
    onDomReady() {
        //this.engine = new BreadCrumb(document.getElementById(this.id));
    }
    onLoad(isFirstLoad) {}
    onUnload() {
        //this.engine.dispose();
    }
}

export default {
    viewModel: Crumbtail,
    template: templateMarkup
};
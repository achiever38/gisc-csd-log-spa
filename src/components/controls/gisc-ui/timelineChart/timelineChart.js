﻿import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./timelineChart.html";
import Logger from "../../../../app/frameworks/core/logger";
import "amcharts";
import "amcharts-serial";
import "amcharts-amstock";

class TimelineChart extends ControlBase {
    constructor(params) {
        super(params);
        this.data = this.ensureObservable(params.data);
        this.graphId = ko.observable(null);
        this.vehicleName = ko.observable(null);
        this.mainColor = ko.observable("#20af80");
        this.onTimelineClick = this.ensureFunction(params.onTimelineClick, null);
    }

    afterRender(element) {
        if(this.data()["graphs"] != undefined) {
            this.graphId(this.makeid());   
            this.vehicleName(this.data()["displayTitle"]);
            this.createChart();
        }        
    }

    onUnload() {
        
    }

    generateChartData() {
        var UTC =  this.data().isIos?"+07:00":"";

        var reChartData = this.data()["graphs"];
        for (var i = 0; i < reChartData.length; i++) {
            var objDate = new Date(reChartData[i]["date"]+UTC);
            reChartData[i]["date"] = objDate;

            if(i == (reChartData.length - 1) || i == (reChartData.length - 2)) {
                reChartData[i]["color"] = "transparent";
            }

            if(reChartData[i]["volume"] != 1){
                reChartData[i]["volume"] = 1;
            }

        }

        return reChartData;
    }

    generateChartEvent() {
        
        var reChartEvent = this.data()["events"];
        for (var i = 0; i < reChartEvent.length; i++) {
            var objDate = new Date(reChartEvent[i]["date"]);
            reChartEvent[i]["date"] = objDate;
            reChartEvent[i]["graph"] = this.graphId();      
            reChartEvent[i]["description"] = this.generateDescriptionEvent(reChartEvent[i]["imagePath"], reChartEvent[i]["description"]);
            reChartEvent[i]["color"] = reChartEvent[i]["fontColor"];
        }

        return reChartEvent;
    }

    generateDescriptionEvent(img, des) {
        var result = null;
        var image = null;
        var description = null;

        if(img){
            image = "<img src='" + this.resolveUrl(img) + "'>";
        }

        if(des){
            description = des;
        }
        
        if(image || description){
            result = "<div align='center'>";
            result += image;
            result += (image)?"<br/>":"";
            result += description;
            result += "</div>";
        }

        return result;
    }

    createChart() {
        let chartData = this.generateChartData();
        let chartEvent = this.generateChartEvent();
        let self = this;
        let chart = {
            type: "stock",
            categoryAxesSettings: {
                minPeriod: "fff",
                maxSeries: 0
            },

            dataSets: [{
                fieldMappings: [{
                    fromField: "speed",
                    toField: "speed"
                },
                    {
                        fromField: "volume",
                        toField: "volume"
                    }
                ],

                dataProvider: chartData,
                categoryField: "date",
                stockEvents: chartEvent,
                color: this.mainColor()
            }],

            panels: [{
                showCategoryAxis: false,
                title: "Speed",
                percentHeight: 70,

                stockGraphs: [{
                    id: this.graphId(),
                    valueField: "speed",
                    //type: "smoothedLine",
                    type: "line",
                    bullet: "none"
                }],
                
                stockLegend: {
                    valueTextRegular: " ",
                    markerType: "none"
                }
            },
                {
                    title: " ",
                    percentHeight: 30,
                    stockGraphs: [{
                        valueField: "volume",
                        lineColorField: "color",
                        fillColorsField: "color",
                        type: "line",
                        cornerRadiusTop: 2,
                        fillAlphas: 1,
                        showBalloon: false
                    }],
                    valueAxes: [{
                        maximum: 1,
                        labelsEnabled: false
                    }],
                    stockLegend: {
                        valueTextRegular: " ",
                        markerType: "none"
                    }
                }
            ],

            chartScrollbarSettings: {
                graph: this.graphId(),
                usePeriod: "10mm",
                position: "top",
                color: "#000"
            },

            chartCursorSettings: {
                valueBalloonsEnabled: true,
                graphBulletSize: 1,
                valueLineBalloonEnabled: true,
                valueLineEnabled: true,
                valueLineAlpha: 0.5  
            },

            periodSelector: {
                selectFromStart: true,
                position: "top",
                dateFormat: "YYYY-MM-DD JJ:NN",
                inputFieldWidth: 150,
                periods: [{
                    period: "hh",
                    count: 1,
                    label: "1 hour"
                },
                    {
                        period: "hh",
                        count: 2,
                        label: "2 hours"
                    },
                    {
                        period: "hh",
                        count: 4,                        
                        label: "4 hour"
                    },
                    {
                        period: "hh",
                        count: 8,
                        label: "8 hours"
                    },
                    {
                        period: "hh",
                        count: 10,
                        label: "10 hours"
                    },
                    {
                        period: "hh",
                        count: 12,
                        label: "12 hours"
                    },
                    {
                        period: "hh",
                        count: 24,
                        label: "24 hours"
                    },
                    {
                        period: "MAX",
                        selected: true,
                        label: "MAX"
                    }
                ]
            },

            panelsSettings: {
                usePrefixes: true
            },
            listeners: [{
                event: "clickStockEvent",
                method: (eventData) => {
                    self.onTimelineClick(eventData["eventObject"])                    
                }
            }]
        };
        let amChart = AmCharts.makeChart(self.id, chart);
    }

    makeid() {
        var text = "G";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 10; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }
}

export default {
viewModel: TimelineChart,
    template: templateMarkup
};

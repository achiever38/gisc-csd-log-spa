import ko from "knockout";
import "jquery";
import templateMarkup from "text!./listView.html";
import ControlBase from "../../controlbase";
import Logger from "../../../../app/frameworks/core/logger";

/**
 * ListView controls, similar to jQuery mobile in term of API
 * 
 * @class ListView
 * @extends {ControlBase}
 */
class ListView extends ControlBase {
    
    /**
     * Creates an instance of ListView.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.items = ko.observableArray();
        this.selectedIndex = this.ensureObservable(params.selectedIndex, null);
        this.additionalCssClass = this.ensureObservable(params.additionalCssClass, null);
        this.listViewClass = ko.pureComputed(() => {
            return this.additionalCssClass() ? "gisc-ui-listview ".concat(this.additionalCssClass()) : "gisc-ui-listview";
        });

        // Selecting/Selected events
        this.onItemSelected = this.ensureFunction(params.onItemSelected);
        this.onItemSelecting = this.ensureFunction(params.onItemSelecting);

        // Clear selected item if selectedIndex is set to null
        this._selectedIndexSubscribeRef = this.selectedIndex.subscribe((newIndex) => {
            if(newIndex === null) {
                this.items().forEach(function(element) {
                    element.isSelected(false);
                });
            }
        });
    }
    
    /**
     * Hook from template when render node is completed
     * @public 
     * @param {any} element
     */
    afterRender(parent) {
        // Parse item binding.
        this.items().forEach(function(item) {
            item.dataBind(parent);
        });
    }

    /**
     * Called from gisc-ui-component when controls is loaded
     * @public
     * @param {any} isFirstLoad
     */
    onLoad(isFirstLoad) {}

    /**
     * Called from gisc-ui-component when controls is removed from DOM.
     * @public
     */
    onUnload() {
        // dispose unused event/resources
        this.items().forEach(function(element) {
            element.onUnload();
        });
        if(this._selectedIndexSubscribeRef) {
            this._selectedIndexSubscribeRef.dispose();
        }
    }

    /**
     * Handle when ListItem is clicked
     * 
     * @param {any} listItem
     * @private
     * @returns
     */
    onItemClick(listItem) {

        var shouldCheckGuard = _.isFunction(this.onItemSelecting);
        var shouldSendEventOut = _.isFunction(this.onItemSelected);

        if(shouldCheckGuard) {
            var selectedRowResponse = this.onItemSelecting(listItem.extras);

            // Verify if response is defer then handle selected state.
            // For other types do not process any logic.
            if(selectedRowResponse && selectedRowResponse.state) {
                selectedRowResponse.done(() => {
                    // Deselect all items.
                    this.items().forEach(function(element) {
                        if (element !== listItem) {
                            element.isSelected(false);
                        } else {
                            element.isSelected(true);
                        }
                    });

                    // Update selected index.
                    this.selectedIndex(this.items.indexOf(listItem));

                    // Send event out
                    if(shouldSendEventOut) {
                        this.onItemSelected(listItem.extras);
                    }
                });
            }
        } else {
            // Deselect all items.
            this.items().forEach(function(element) {
                if (element !== listItem) {
                    element.isSelected(false);
                } else {
                    element.isSelected(true);
                }
            });

            // Update selected index.
            this.selectedIndex(this.items.indexOf(listItem));

            // Send event out
            if(shouldSendEventOut) {
                this.onItemSelected(listItem.extras);
            }
        }

    }

}

/**
 * ListItem used by ListView control
 * 
 * @class ListItem
 * @extends {ControlBase}
 * @private
 */
class ListItem extends ControlBase {
    constructor(params) {
        super(params);
        this.element = params.element;
        this.text = this.ensureObservable(params.text, "");
        this.isSelected = ko.observable(false);
        this.templateNodes = params.templateNodes;
        this.extras = this.ensureNonObservable(params.extras, null);
        this.faIcon = this.ensureObservable(params.faIcon, null);
        this.isRequired = this.ensureObservable(params.isRequired, false);
        this.isShowNavIcon = this.ensureObservable(params.isShowNavIcon, true);

        this.displayText = ko.pureComputed(() => {
            var result = "";
            if(this.faIcon()) {
                result += "<i class='fa " + this.faIcon() + "' aria-hidden='true'></i>";
            }
            result += this.text();
            if(this.isRequired()) {
                result += "<span class='gisc-ui-required'>*</span>";
            }
            return result;
        });

        this.cssClasses = ko.pureComputed(() => {
            var cssClassString = "";

            if (this.isSelected()) {
                cssClassString = "selected";
            }

            return cssClassString;
        });
    }
    onLoad(isFirstLoad) {
    }
    onUnload(){
        this.element = null;
        this.selectedIndex = null;
        this.additionalCssClass = null;
    }
    dataBind(viewModel) {
        if(this.element) {
            var rawParameters = this.getBindingFromElement(this.element, viewModel);
            var textValue = ko.isObservable(rawParameters.text) ? rawParameters.text() : rawParameters.text;
            this.text(textValue);
            this.faIcon(rawParameters.faIcon);
            this.isRequired(rawParameters.isRequired);
            this.isShowNavIcon(rawParameters.isShowNavIcon || true);
            this.extras = rawParameters.extras;
        }
    }
}

export default {
    viewModel: {
        createViewModel: function(params, componentInfo) {
            var vm = new ListView(params);
            var i = 0;

            if(params.datasource) {
                // Using dynamic datasource when passing from screen.
                params.datasource().forEach((item)=> {
                    var defaultListItemId = vm.id + "_li" + (i+1);
                    var newListItem = new ListItem({
                        id: defaultListItemId,
                        text: vm.ensureNonObservable(item[params.dataTextfield], "List_Item_" + i),
                        isShowNavIcon: vm.ensureObservable(params.isShowNavIcon, true),
                        templateNodes: null,
                        element: null,
                        extras: item
                    });

                    // Set selected index
                    if (i === vm.selectedIndex()) {
                        newListItem.isSelected(true);
                    }

                    vm.items.push(newListItem);
                    i++;
                })
            }
            else {
                // Using static template parsing.
                // Parsing item templates node for each item.
                componentInfo.templateNodes.forEach(function(element) {
                    switch (element.nodeName) {
                        case "ITEM":
                            var defaultListItemId = vm.id + "_li" + (i+1);
                            var newListItem = new ListItem({
                                id: defaultListItemId,
                                text: "List_Item_" + i,
                                templateNodes: element.children,
                                element: element
                            });

                            // Set selected index
                            if (i === vm.selectedIndex()) {
                                newListItem.isSelected(true);
                            }

                            vm.items.push(newListItem);
                            i++;
                            break;
                        }
                    }, this);
                }

                return vm;
        }
    },
    template: templateMarkup
};
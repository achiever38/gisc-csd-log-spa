﻿import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./lineChart.html";
import Logger from "../../../../app/frameworks/core/logger";
import "amcharts";
import "amcharts-serial";

class LineChart extends ControlBase {
    constructor(params) {
        super(params);
        this.data = this.ensureObservable(params.data);
        this.percentOnTimeLineColor = ko.observable(null);
        this.isDisplay = ko.observable(true);
    }

    afterRender(element) {
        this.checkData()
        this.data.subscribe(()=> {
            this.checkData()
        })      
    }

    checkData() {
        var self = this;
        if(this.data() != undefined) {
            if(this.data()['graphData']['length'] != 0) {
                this.createChart();     
            } else {
                $("#" + self.id).html(this.i18n('Common_DataNotFound')())
            }            
        } 
    }

    createGraphData() {
        var graphsData = this.data()["graphConfig"];
        for (let i = 0; i < graphsData.length; i++) {
            graphsData[i]["bulletBorderThickness"] = 1
            graphsData[i]["hideBulletsCount"] = 30
            graphsData[i]["fillAlphas"] = 0
            graphsData[i]["bullet"] = graphsData[i]["valueField"] == "percentOnTime" ? "square" : "round"
            graphsData[i]["valueAxis"] = graphsData[i]["valueField"] == "percentOnTime" ? "shipmentTotal" : "shipment"
            
            if(graphsData[i]["valueField"] == "percentOnTime") {
                graphsData[i].balloonText = "[[value]]%"
                this.percentOnTimeLineColor(graphsData[i].lineColor)
            }
        }

        return graphsData
    }

    createGuides() {
        let guide = [{
            value: 0,
            label: "0%",
            tickLength: 5,
            lineAlpha: .15
        }, {
            value: 10,
            label: "10%",
            tickLength: 5,
            lineAlpha: .15
        }, {
            value: 20,
            label: "20%",
            tickLength: 5,
            lineAlpha: .15
        }, {
            value: 30,
            label: "30%",
            tickLength: 5,
            lineAlpha: .15
        }, {
            value: 40,
            label: "40%",
            tickLength: 5,
            lineAlpha: .15
        }, {
            value: 50,
            label: "50%",
            tickLength: 5,
            lineAlpha: .15
        }, {
            value: 60,
            label: "60%",
            tickLength: 5,
            lineAlpha: .15
        }, {
            value: 70,
            label: "70%",
            tickLength: 5,
            lineAlpha: .15
        }, {
            value: 80,
            label: "80%",
            tickLength: 5,
            lineAlpha: .15
        }, {
            value: 90,
            label: "90%",
            tickLength: 5,
            lineAlpha: .15
        }, {
            value: 100,
            label: "100%",
            tickLength: 5,
            lineAlpha: .15
        }];

        return guide;
    }

    generateChartData() {
        var chartData = [];
        //var firstDate = new Date();
        //firstDate.setDate(firstDate.getDate() - 100);

        //var shipmentOnTime = 3500;
        //var shipmentLate = 3600;
        //var ShipmentCancelled = 3700;
        //var PercentOnTime = 0;
        //var shipmentTotal = 3700;

        //for (var i = 0; i < 20; i++) {
        //    var newDate = new Date(firstDate);
        //    newDate.setDate(newDate.getDate() + i);

        //    shipmentOnTime += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 100);
        //    shipmentLate += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 100);
        //    ShipmentCancelled += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 100);
        //    PercentOnTime = Math.round((Math.random() * 100));
        //    shipmentTotal += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 100);

        //    chartData.push({
        //        date: newDate,
        //        shipmentOnTime,
        //        shipmentLate,
        //        ShipmentCancelled,
        //        shipmentTotal,
        //        PercentOnTime
        //    });
        //}

        chartData = this.data()["graphData"];

        return chartData;
    }

    createChart() {
        var axisGuides = this.createGuides();
        var chartData = this.generateChartData();
        var graphsData = this.createGraphData();
        var self = this;
        var chart = {
            type: "serial",
            legend: {
                useGraphSettings: true
            },
            dataProvider: chartData,
            synchronizeGrid: true,
            valueAxes: [{
                id: "shipment",
                axisColor: "red",
                axisThickness: 2,
                position: "left",
                title: this.i18n('Transportation_Mngm_Dashboard_Shipment_Volume')(),
                //autoGridCount: false,
                //gridCount: 10
            }, {
                id: "shipmentTotal",
                axisColor: this.percentOnTimeLineColor(),
                axisThickness: 2,
                position: "right",
                title: this.i18n('Transportation_Mngm_Dashboard_Percent')(),
                maximum: 100,
                minimum: 0,
                labelsEnabled: false,
                guides: axisGuides
            }],
            graphs: graphsData,
            chartScrollbar: {
                updateOnReleaseOnly: true,
                scrollbarHeight: 40,
                autoGridCount: true,
                color: "#000"
            },
            chartCursor: {
                cursorPosition: "mouse"
            },
            categoryField: "date",
            categoryAxis: {
                parseDates: true,
                axisColor: "#DADADA",
                minorGridEnabled: true
            }
        };

        let lineChart = AmCharts.makeChart(self.id, chart);
    }   

    onUnload() {
        
    }
}

export default {
viewModel: LineChart,
    template: templateMarkup
};

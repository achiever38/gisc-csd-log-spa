﻿import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./donutChart.html";
import "amcharts";
import "amcharts-donut";

class DonutChart extends ControlBase {
    constructor(params) {
        super(params);
        this.data = this.ensureObservable(params.data);
        this.centerLabel = this.ensureObservable(params.centerLabel);
        this.formatLabelText = this.ensureObservable(params.formatLabelText, "[[title]]: [[value]]");
        this.formatBalloonText = this.ensureObservable(params.formatBalloonText, "[[title]]: [[percents]]%");
        this.labelRadius = this.ensureObservable(params.labelRadius);
        this.title = this.ensureObservable(params.title);
        this.detailInfo = this.ensureObservable(params.detailInfo);
        this.legendPosition = this.ensureObservable(params.legendPosition);
        this.legendWidth = this.ensureObservable(params.legendWidth, 0);
        this.showEntries = this.ensureObservable(params.showEntries);
        this.onClickPieChart = this.ensureFunction(params.onClickPieChart, null);
        this.animation = this.ensureObservable(params.animation, true);
        this.labelTextColor = this.ensureObservable(params.labelTextColor, null)
        this.fontSizeBusiness = this.ensureObservable(params.fontSizeBusiness, 15);
        this.allLabels = this.ensureObservable(params.allLabelsdata)
        this.pullOutRadiusdata = this.ensureObservable(params.pullOutRadiusdata, 0);
        // วงข้างใน donutChart
        this.innerRadius = this.ensureObservable(params.innerRadius, 100);
        // ความห่างของข้อความในกราฟ
        this.labelRadius = this.ensureObservable(params.labelRadius, -10);
        // ความห่างของกราฟจากบน-ล่าง
        this.pieY = this.ensureObservable(params.paddingbottom, 55);
        // property อื่นๆ
        this.propertyGraph = this.ensureObservable(params.propertyGraph)
    }

    afterRender(element) {
        this.checkData()
        this.data.subscribe(() => {
            this.checkData()
        })

        this.centerLabel.subscribe(() => {
            this.checkData()
        })
    }

    checkData() {
        var self = this;
        if (this.data() != undefined) {
            if (this.data()['length'] != 0) {
                this.createChart();
            } else {
                $("#" + self.id).html(this.i18n('Common_DataNotFound')())
            }
        } else {
            $("#" + self.id).html(this.i18n('Common_DataNotFound')())
        }
    }

    createChart() {
        var self = this;
        var chart = {
            type: "pie",
            // dataProvider: [{
            //    name: "Waiting",
            //    value: 25,
            //    color: "red"
            // },
            // {
            //    name: "Start",
            //    value: 50,
            //    color: "#000"
            // },
            // {
            //    name: "On The Way",
            //    value: 5,
            //    color: "yellow"
            // },
            // {
            //    name: "Inside Terminal",
            //    value: 60,
            //    color: "pink"
            // },
            // {
            //    name: "Going to Terminal",
            //    value: 100,
            //    color: "blue"
            // },
            // {
            //    name: "Cancelled",
            //    value: 5,
            //    color: "gold"
            // }],
            dataProvider: this.data(),
            valueField: "value",
            titleField: "text",
            colorField: "color",
            color: this.labelTextColor(),
            innerRadius: this.innerRadius() + "%",
            //pullOutRadius: this.pullOutRadiusdata()+"%",
            pieY: this.pieY() + "%",
            //startDuration: 0,
            labelRadius: this.labelRadius(),
            allLabels: [{
                    y: "45%",
                    align: "center",
                    size: 25,
                    bold: true,
                    text: this.data()['total'],
                    color: "#555"
                },
                {
                    y: "50%",
                    align: "center",
                    size: 15,
                    text: this.i18n('Transportation_Mngm_Shipment_Title')(),
                    color: "#555"
                }
            ],
            labelText: this.formatLabelText(),
            balloonText: this.formatBalloonText()
        };



        if (!this.animation()) {
            chart.startDuration = 0
        }
        if (this.allLabels() != null || this.allLabels() != undefined) {
            chart.allLabels = this.allLabels()
        }

        if (this.centerLabel() != null || this.centerLabel() != undefined) {
            // ค่าเลขตรงกลาง

            chart.allLabels = [{
                y: this.title() == null ? "40%" : "50%",
                align: "center",
                size: 16,
                bold: true,
                text: this.centerLabel(),
                color: "#616161"
            }]

        }
        // if(this.formatLabelText() != null || this.formatLabelText() != undefined) {
        //     chart.labelText = this.formatLabelText()
        // }

        // if(this.formatBalloonText() != null || this.formatBalloonText() != undefined) {
        //     chart.balloonText = this.formatBalloonText()
        // }

        // if(this.labelRadius() != null || this.labelRadius() != undefined) {
        //     chart.labelRadius = this.labelRadius();
        // }
        if (this.title() != null || this.title() != undefined) {
            let title = []

            for (let i = 1; i <= 7; i++) {
                title.push({
                    text: ""
                })
            }
            title.push({
                text: this.title(),
                color: "#616161",
                size: this.fontSizeBusiness()
            })

            chart.titles = title
        }

        if (this.showEntries() != null || this.showEntries() != undefined) {
            chart.titleField = "name",
                chart.legend = {
                    showEntries: this.showEntries()
                }
        }

        if (this.legendPosition() != null || this.legendPosition() != undefined) {


            chart.titleField = "name",

                chart.pullOutRadius = 0, //เมื่อมี legend ให้ปิด animation pullout
                chart.pullOutDuration = 0

            chart.legend = {
                position: this.legendPosition(),
                enabled: true,
                valueAlign: "left",
                valueWidth: this.legendWidth(),
            }
        }

        if (this.pullOutRadiusdata() != null || this.pullOutRadiusdata() != undefined) {
            chart.pullOutRadius = this.pullOutRadiusdata() + "%";
        }
        if (this.propertyGraph() != null || this.propertyGraph() != undefined) {
            // property ที่ส่งมาพวก margin 0 มาต่อกัน
            let property = this.propertyGraph()[0]
            Object.assign(chart, property)
        }



        let donutChart = AmCharts.makeChart(self.id, chart);

        donutChart.addListener('rollOverSlice', function (e) {
            if (e.event.target.nodeName == "rect") { // ดักเมื่อ mouseover ที่ legend จะไม่ให้ animation ทำงาน
                return
            }
            donutChart.clickSlice(e.dataItem.index);
        });

        donutChart.addListener('rollOutSlice', function (e) {
            if (e.event.target.nodeName == "rect") { // ดักเมื่อ mouseover ที่ legend จะไม่ให้ animation ทำงาน
                return
            }
            donutChart.clickSlice(e.dataItem.index);
        });

        donutChart.addListener('clickSlice', function (e) {

            if (e.event != undefined) { // check mouseClick
                if (self.detailInfo() != null) {
                    e.detailInfo = self.detailInfo();
                }
                self.onClickPieChart(e);

                //if นี้เพื่อเช็คว่าหากมี legend มาไม่ต้อง validateData ใหม่(ถ้ามี legend มาจะไม่เข้า if นี้)
                if (self.legendPosition() == null || self.legendPosition() == undefined) {
                    e.chart.validateData();
                }

            } else {}

        });
    }

    onUnload() {

    }
}

export default {
    viewModel: DonutChart,
    template: templateMarkup
};
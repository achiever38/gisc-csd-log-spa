import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./editor.html";
import "kendo.all.min";

/**
 * 
 * Using for multi-line text input control
 * @class TextArea
 * @extends {ControlBase}
 */
class Editor extends ControlBase {
    /**
     * Creates an instance of TextArea.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        
        let defaultTools = [
            "bold",
            "italic",
            "underline",
            "strikethrough",
            "justifyLeft",
            "justifyCenter",
            "justifyRight",
            "justifyFull",
            "insertUnorderedList",
            "insertOrderedList",
            "indent",
            "outdent",
            "createLink",
            "unlink",
            "insertImage",
            "insertFile",
            "subscript",
            "superscript",
            "tableWizard",
            "createTable",
            "addRowAbove",
            "addRowBelow",
            "addColumnLeft",
            "addColumnRight",
            "deleteRow",
            "deleteColumn",
            // "viewHtml",
            "formatting",
            "cleanFormatting",
            // "fontName",
            "fontSize",
            "foreColor",
            "backColor",
            "print"
        ];

        this.id = this.ensureNonObservable(params.id, this.id);
        this.value = this.ensureObservable(params.value, null);
        this.tools = this.ensureObservable(params.tools, defaultTools);
        this.enable = this.ensureNonObservable(params.enable, true);
        
    }

    onDomReady() {
        var self = this;
        let editor = $("#" + self.id);
        editor.kendoEditor({
            tools: self.tools()
        });

        editor.closest(".k-editor").height(self.getHeight());

        //set readonly
        $(editor.data().kendoEditor.body).attr('contenteditable',self.enable);

        var kendoEditor = editor.data("kendoEditor");

        self.value.subscribe(function(value) {
            kendoEditor.value(self.value());
        });

        $(kendoEditor.body).blur(function(){
            self.value(kendoEditor.value());
        });

    }

    getHeight() {
        // var topPadding = 10;
        var btmPadding = 40;
        var tools = 100;
        var header, btmHeader, blade, btmBlade, topKendoUI, distanceHeaderAndKendoUI, kendoUIHeight, height = 0;
        // get bottom header
        header = $("header.app-blade-header");
        btmHeader = header.offset().top + header.height();
        //get bottom blade
        blade = $("div.app-blade-content-container");
        btmBlade = blade.offset().top + blade.height();
        //get top kendo ui header
        topKendoUI = $("gisc-ui-editor").offset().top;
        //calculator distance header and kendo ui
        distanceHeaderAndKendoUI = topKendoUI - btmHeader;
        //calculator kendo ui height
        kendoUIHeight = btmBlade - (btmHeader  + distanceHeaderAndKendoUI + btmPadding + tools);
        //set kendo ui height
        height = (kendoUIHeight < 195) ? 195 : kendoUIHeight;

        return height;
    }


    onLoad() {
        
    }

    onUnload() {
        this.value = null;
    }
}

export default {
    viewModel: Editor,
    template: templateMarkup
};
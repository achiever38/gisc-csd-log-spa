﻿import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./barStackAmChart.html";
import "amcharts";
import "amcharts-serial"

class BarStackAmChart extends ControlBase {
    constructor(params) {
        super(params);
        this.data = this.ensureObservable(params.data);
        this.nameChart = this.ensureObservable(params.nameChart);
        this.showChartScrollbar = this.ensureObservable(params.showChartScrollbar, false);
        this.initialZoom = this.ensureObservable(params.initialZoom, null);
        this.onClickStackChart = this.ensureFunction(params.onClickStackChart, null);
        this.fixedColumnWidth = this.ensureObservable(params.fixedColumnWidth, null);
        this.isCategoryAxisLabelTruncate = this.ensureObservable(params.isCategoryAxisLabelTruncate, false);


        this.height = this.ensureObservable(params.height, '100%');


        this.stackAmChart = null;

    }


    afterRender(element) {
        this.checkData()
        this.data.subscribe(() => {
            this.checkData()
        })

        this.nameChart.subscribe(() => {
            this.checkData()
        })
    }

    checkData() {
        var self = this;
        if (this.data() != undefined) {
            if (this.data()['length'] != 0) {
                this.createChart();
            } else {
                $("#" + self.id).html(this.i18n('Common_DataNotFound')())
            }
        }
    }

    createChart() {
        var self = this;

        // var chart = {
        //     "type": "serial",
        //     "theme": "light",

        //     "dataProvider": this.data(),
        //     "valueAxes": [{
        //         "stackType": "100%",
        //         "axisAlpha": 0,
        //         "gridAlpha": 0,
        //         "labelsEnabled": false,
        //         "position": "left"
        //     }],
        //     "graphs":[{
        //         "balloonText": "[[title]], [[category]]<br><span style='font-size:14px;'><b>[[value]]</b> ([[percents]]%)</span>",
        //         "fillAlphas": 0.9,
        //         "fontSize": 11,
        //         "labelText": "[[percents]]%",
        //         "lineAlpha": 0.5,
        //         "fillColorsField": "color1",
        //         "title": "Shipment",
        //         "type": "column",
        //         "valueField": "value1"
        //     },{
        //         "balloonText": "[[title]], [[category]]<br><span style='font-size:14px;'><b>[[value]]</b> ([[percents]]%)</span>",
        //         "fillAlphas": 0.9,
        //         "fontSize": 11,
        //         "labelText": "[[percents]]%",
        //         "lineAlpha": 0.5,
        //         "fillColorsField": "color2",
        //         "title": "No Shipment",
        //         "type": "column",
        //         "valueField": "value2"
        //     }],
        //     "marginTop": 30,
        //     "marginRight": 0,
        //     "marginLeft": 0,
        //     "marginBottom": 40,
        //     "autoMargins": false,
        //     "categoryField": "businessUnitName",
        //     "categoryAxis": {
        //         "gridPosition": "start",
        //         "axisAlpha": 0,
        //         "gridAlpha": 0,
        //         "labelRotation": 90
        //     },
        //     "export": {
        //         "enabled": true
        //     }
        // };

        var chart = {
            "type": "serial",
            "dataProvider": this.data(),
            "categoryField": "name",
            "startDuration": 1,
            "fontSize": 14,
            // "handDrawScatter": 6,
            "categoryAxis": {
                "gridPosition": "start",
                "labelRotation": 45
                
            },
            "trendLines": [],
            "graphs": [
                {
                    "balloonText": "[[title]], [[category]]<br><span style='font-size:14px;'><b>[[value]]</b> ([[percents]]%)</span>",
                    "fillAlphas": 1,
                    "labelText": "[[percents]]%",
                    // "labelText": "ei",
                    // "id": "AmGraph-1",
                    "fontSize": 10,
                    // "labelRotation": 270,
                    "title": "No Shipment",
                    "type": "column",
                    "fillColorsField": "color1",
                    "valueField": "value1"
                },
                {
                    "balloonText": "[[title]], [[category]]<br><span style='font-size:14px;'><b>[[value]]</b> ([[percents]]%)</span>",
                    "fillAlphas": 1,
                    "labelText": "[[percents]]%",
                    // "labelText": "ei",
                    // "id": "AmGraph-2",
                    "fontSize": 10,
                    "title": "Shipment Assigned",
                    
                    "type": "column",
                    "fillColorsField": "color2",
                    "valueField": "value2"
                }],
            "guides": [],
            "valueAxes": [
                {
                    // "id": "ValueAxis-1",
                    "stackType": "100%",
                    "gridColor": "#FFFFFF",
                    "gridAlpha": 0.2,
                    "dashLength": 0,
                    "minimum": 0,
                    "title": "(%)"
                    //"maximum": 2000
                    // "title": "eiei"
                }
            ],
            // "height":262
            // "legend": {
            //     "enabled": true,
            //     "useGraphSettings": true
            // }
        }

        // console.log("this.fixedColumnWidth()", this.fixedColumnWidth())
        if (this.fixedColumnWidth()) {
            chart.graphs[0]["fixedColumnWidth"] = this.fixedColumnWidth();
            chart.graphs[1]["fixedColumnWidth"] = this.fixedColumnWidth();
        }
        if (this.showChartScrollbar()) {
            chart.chartScrollbar = {
                "graph": "scrollbar",
                "graphType": "column",
                "resizeEnabled": true,
                "scrollbarHeight": 30
            }
    
        }
        if (this.isCategoryAxisLabelTruncate()){

            chart.categoryAxis = {
                "gridPosition": "start",
                "labelRotation": 45,
                // "gridAlpha": 0,
                // "autoRotateAngle": this.categoryLabelRotateAngle(),
                // "autoRotateCount": 1,
                "labelFunction":(valueText) =>{
                    return valueText.length > 15? valueText.substring(0, 15) + '...':valueText
                  }
            }

        }


        this.stackAmChart = AmCharts.makeChart(self.id, chart);

        // console.log("this.initialZoom()",this.initialZoom())
        if (this.initialZoom()) {
            this.stackAmChart.zoomToIndexes(this.initialZoom()[0], this.initialZoom()[1]);
        }

        this.stackAmChart.addListener('clickGraphItem', (e) => {
            self.onClickStackChart(e);
        });

        if (this.isCategoryAxisLabelTruncate()) {
            this.stackAmChart.categoryAxis.addListener("rollOverItem", (event) => {
                event.target.setAttr("cursor", "default");
                event.chart.balloon.followCursor(true);
                event.chart.balloon.showBalloon(event.serialDataItem.dataContext.name);
            });

            this.stackAmChart.categoryAxis.addListener("rollOutItem", (event) => {
                event.chart.balloon.hide();
            });
        }
    }
    // zoomToIndexes(startIndex, endIndex) {
    //     this.stackAmChart.zoomToIndexes(startIndex, endIndex);
    // }

    onLoad() {

    }

    onUnload() {

    }
}


export default {
    viewModel: BarStackAmChart,
    template: templateMarkup
};

import ko from "knockout";
import "jquery";
import "ms-dropdown";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./ImageCombobox.html";


/**
 * 
 * @class ImageCombobox
 * @extends {ControlBase}
 */
class ImageCombobox extends ControlBase {
    constructor(params) {
       
        super(params);
    
        this.id = this.ensureNonObservable(params.id, this.id);
        this.value = this.ensureObservable(params.value);
        this.options= this.ensureNonObservable(params.options);
        this.optionsCaption = this.ensureObservable(params.optionsCaption);

    }

    afterRender(element) {

        var self = this;
        var $el = $(element);// $("#" + self.id);
        var selectedValue=0;
        
        if(self.optionsCaption()!=null){
           self.options.unshift({value:"", text:self.optionsCaption()});
  
        }
        let option = {
            byJson: {
                data: self.options
            }, //if you want to create by json data
            mainCSS: 'dd',
            visibleRows: 7,
            rowHeight: 0,
            showIcon: true,
            zIndex: 9999,
            useSprite: false,
            animStyle: 'slideDown',
            event: 'click', //it can be mouseover 
            openDirection: 'auto', //auto || alwaysUp || alwaysDown
            enableCheckbox: false, //this needs to multiple or it will set element to multiple
            checkboxNameSuffix: '_mscheck',
            append: '',
            prepend: '',
            reverseMode: true, //it will update the msdropdown UI/value if you update the original dropdown - will be usefull if are using knockout.js or playing with original dropdown
            roundedCorner: true, //to have rounded corner
            enableAutoFilter: true, //to enable autofilter
            on: {
                create: null,
                open: null,
                close: null,
                add: null,
                remove: null,
                change: function(value){
                    //console.log("change,value",value);
                    self.value(value);
                },
                blur: null,
                click: null,
                dblclick: null,
                mousemove: null,
                mouseover: null,
                mouseout: null,
                focus: null,
                mousedown: null,
                mouseup: null
            }
        }
        var oDropdown = $el.msDropdown(option).data("dd");
        if(self.value() && self.value!=undefined && self.options.length>0){
            ko.utils.arrayForEach(self.options, function(items,index) {
                if(items.value===self.value().value){
                    oDropdown.set("selectedIndex",index);
                }
            });

        }else {
          //  this.value(self.options[0]);
        }
        this._changeValueSubscribe = this.value.subscribe((newvalue) => {
            if(self.value() && self.value!=undefined && self.options.length>0){
                ko.utils.arrayForEach(self.options, function(items,index) {
                    if(items.value===newvalue.value){
                        oDropdown.set("selectedIndex",index);
                    }
                });

            }
        
        });
    }
    onUnload() {
        this.value = null;
    }
}

    export default {
viewModel: ImageCombobox,
    template: templateMarkup
};
import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./gallery.html";
import * as PhotoSwipe from 'photoswipe';
import * as PhotoSwipeUI_Default from 'photoswipe-ui';

// import "photoswipe";
// import "photoswipe-ui";



/**
 * 
 * Using for multi-line text input control
 * @class TextArea
 * @extends {ControlBase}
 */
class Gallery extends ControlBase {
    /**
     * Creates an instance of TextArea.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        
        this.id = this.ensureNonObservable(params.id, this.id);
        this.source = this.ensureNonObservable(params.source, null);
        this.width = this.ensureNonObservable(params.width, "170px");
        this.height = this.ensureNonObservable(params.height, "85px");
        this.description = this.ensureNonObservable(params.description, null);
        // var fileName = this.src.split("/").pop();
        // this.fileName(fileName);
        this.angle = ko.observable(0);
        this.oldPage = ko.observable();
        this.fullScreen = this.ensureNonObservable(params.fullScreen, true);
      
    }

    onDomReady() {
        var self = this;
        
        // execute above function
        this.initPhotoSwipeFromDOM(".gisc-gallery");
        
    }

    onClickRotate(e) {
        if(this.source.length > 0) {
            var pageOf = $('#' + this.id + " .pswp__counter").text();
            var currPage = pageOf.split(" / ").shift();
            if(!_.isNil(currPage)){
                var objImage = _.find(this.source, function(val, id){
                    return (id == (currPage-1)) ? val : null;
                });
                //for resset angle if switch page
                if(currPage != this.oldPage()){
                    this.angle(0);
                }
                var sum = this.angle() + 90;
                this.angle(sum);
                var img = $("img.pswp__img[src='" + objImage.filePath + "']:not(.pswp__img--placeholder)");
                img.css('transform','rotate(' + this.angle() + 'deg)');
            }
            this.oldPage(currPage);
        }

        // var pageOf = $('#' + this.id + " .pswp__counter").text();
        // var currPage = pageOf.split(" / ").shift();
        
        // var objImage = _.find(this.source, function(val, id){
        //     return (id == (currPage-1)) ? val : null;
        // });
        
        // var img = $("img.pswp__img[src='" + objImage.filePath + "']:not(.pswp__img--placeholder)");
        // img.css('transform','rotate(' + angle + 'deg)');
        // console.log(eGallery);
        // console.log(eZoom);
        // var test = _.find(eGallery)
        // var angle = this.angle() + 90;
        
        // $('#' + this.id).css('transform','rotate(' + angle + 'deg)');
    }

    initPhotoSwipeFromDOM(gallerySelector) {
        // loop through all gallery elements and bind events
        var galleryElements = document.querySelectorAll( gallerySelector );

        for(var i = 0, l = galleryElements.length; i < l; i++) {
            galleryElements[i].setAttribute('data-pswp-uid', i+1);
            galleryElements[i].onclick = this.onThumbnailsClick.bind(this);
        }

        // Parse URL and open gallery if it contains #&pid=3&gid=1
        var hashData = this.photoswipeParseHash();
        if(hashData.pid && hashData.gid) {
            this.openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
        }
    }

    

    // parse slide data (url, title, size ...) from DOM elements 
    // (children of gallerySelector)
    parseThumbnailElements(el) {
        var thumbElements = el.childNodes,
            numNodes = thumbElements.length,
            items = [],
            figureEl,
            linkEl,
            size,
            item,
            imgSize = new Image(),
            imgWidth = 0,
            imgHeight = 0,
            src = null;

        for(var i = 0; i < numNodes; i++) {

            figureEl = thumbElements[i]; // <figure> element

            // include only element nodes 
            if(figureEl.nodeType !== 1) {
                continue;
            }

            linkEl = figureEl.children[0]; // <a> element
            src = linkEl.getAttribute('href');

            imgSize.src = src;
            imgWidth = imgSize.width;
            imgHeight = imgSize.height;

            // size = linkEl.getAttribute('data-size').split('x');
            
            // create slide object
            item = {
                src: src,
                w: parseInt(imgWidth, 10),
                h: parseInt(imgHeight, 10)
            };



            if(figureEl.children.length > 1) {
                // <figcaption> content
                item.title = figureEl.children[1].innerHTML; 
            }

            if(linkEl.children.length > 0) {
                // <img> thumbnail element, retrieving thumbnail url
                item.msrc = linkEl.children[0].getAttribute('src');
            } 

            item.el = figureEl; // save link to element for getThumbBoundsFn
            items.push(item);
        }

        return items;
    }

    // find nearest parent element
    closest(el, fn) {
        return el && ( fn(el) ? el : this.closest(el.parentNode, fn) );
    };

    // triggers when user clicks on thumbnail
    onThumbnailsClick(e) {
        e = e || window.event;
        e.preventDefault ? e.preventDefault() : e.returnValue = false;

        var eTarget = e.target || e.srcElement;

        // find root element of slide
        var clickedListItem = this.closest(eTarget, function(el) {
            return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
        });

        if(!clickedListItem) {
            return;
        }

        // find index of clicked item by looping through all child nodes
        // alternatively, you may define index via data- attribute
        var clickedGallery = clickedListItem.parentNode,
            childNodes = clickedListItem.parentNode.childNodes,
            numChildNodes = childNodes.length,
            nodeIndex = 0,
            index;

        for (var i = 0; i < numChildNodes; i++) {
            if(childNodes[i].nodeType !== 1) { 
                continue; 
            }

            if(childNodes[i] === clickedListItem) {
                index = nodeIndex;
                break;
            }
            nodeIndex++;
        }



        if(index >= 0) {
            // open PhotoSwipe if valid index found
            this.openPhotoSwipe( index, clickedGallery );
        }
        return false;
    }

    // parse picture index and gallery index from URL (#&pid=1&gid=2)
    photoswipeParseHash() {
        var hash = window.location.hash.substring(1),
        params = {};

        if(hash.length < 5) {
            return params;
        }

        var vars = hash.split('&');
        for (var i = 0; i < vars.length; i++) {
            if(!vars[i]) {
                continue;
            }
            var pair = vars[i].split('=');  
            if(pair.length < 2) {
                continue;
            }           
            params[pair[0]] = pair[1];
        }

        if(params.gid) {
            params.gid = parseInt(params.gid, 10);
        }

        return params;
    }

    openPhotoSwipe(index, galleryElement, disableAnimation, fromURL) {
        var pswpElement = document.querySelectorAll('.pswp')[0],
            gallery,
            options,
            items;

        items = this.parseThumbnailElements(galleryElement);

        // define options (if needed)
        options = {

            // define gallery index (for URL)
            galleryUID: galleryElement.getAttribute('data-pswp-uid'),

            getThumbBoundsFn: function(index) {
                // See Options -> getThumbBoundsFn section of documentation for more info
                var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                    pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                    rect = thumbnail.getBoundingClientRect(); 

                return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
            }

        };

        // PhotoSwipe opened from URL
        if(fromURL) {
            if(options.galleryPIDs) {
                // parse real index when custom PIDs are used 
                // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                for(var j = 0; j < items.length; j++) {
                    if(items[j].pid == index) {
                        options.index = j;
                        break;
                    }
                }
            } else {
                // in URL indexes start from 1
                options.index = parseInt(index, 10) - 1;
            }
        } else {
            options.index = parseInt(index, 10);
        }

        // exit if index not found
        if( isNaN(options.index) ) {
            return;
        }

        if(disableAnimation) {
            options.showAnimationDuration = 0;
        }

        options.shareButtons = [
            {id:'download', label:'Download image', url:'{{raw_image_url}}', download:true}
        ];

        options.fullscreenEl = this.fullScreen;
        // Pass data to PhotoSwipe and initialize it
        gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();
    }

    

    onLoad() {
        
    }

    onUnload() {
        this.value = null;
    }
}

export default {
    viewModel: Gallery,
    template: templateMarkup
};
﻿import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./routedragdrop.html";
import Logger from "../../../../app/frameworks/core/logger";
import WebRequestCustomPOI from "../../../../app/frameworks/data/apitrackingcore/webRequestCustomPOI";

//import "jquery-ui-touch-punch";

import "gridstack";
import "gridstack-jQueryUI";
import Utility from "../../../../app/frameworks/core/utility";

class RouteDragDrop extends ControlBase {
    constructor(params) {
        super(params);

        this.data = this.ensureObservable(params.data);

        this.isDevice = ko.pureComputed(function(){
            return Utility.isTouchDeviceSupported();
        }, this);
        
        this.id = this.ensureNonObservable(params.id || 'RouteDragDrop-CTRL');
        this.textClick = this.ensureFunction(params.textClick);

        this.routeParams = this.ensureObservable(params.routeParams);

        this.routeParams.replaceAll(params.data());

        //this.onSelectedItem = this.ensureFunction(params.onSelectedItem, null);
    }

    get webRequestCustomPOI() {
        return WebRequestCustomPOI.getInstance();
    }

    afterRender(element) {
        let self = this;
        let options = {
                width:12,
                cellHeight: 44,
                verticalMargin: 10,
                alwaysShowResizeHandle: false,
                disableResize: true,
                //height: 2,
                removable :false
        };

        $('#' + this.id).gridstack(options);

        //let grid = $('.grid-stack').data('gridstack');

        $('#' + this.id).unbind("change");

        $('#' + this.id).on('change', (e, items) => {

            let grid = $('.grid-stack').data('gridstack');
            
            this.changeIndex(grid.grid.nodes);
        });

        this.data.subscribe((newVal) => {
            //$('#' + this.id).gridstack(options);
            let grid = $('.grid-stack').data('gridstack');

            grid.removeAll();

            ko.utils.arrayForEach(newVal, function (itm, ind) {
                grid.makeWidget($('#gs-item-'+ind)[0]);
            });

            //let grid = $('.grid-stack').data('gridstack');
        }, null, 'change');

        //let grid = $('.grid-stack').data('gridstack');

        //setInterval(() => {
        //    let el = $('<div class="grid-stack-item" data-gs-width="12" data-gs-height="1"><div class="grid-stack-item-content"></div></div>');
        //    grid.addWidget(el);
        //}, 1000)
    }

    //textClick(index)
    //{
    //    console.log(this, index);
    //}

    onSelectedItem(self, index, evt, ui) {
        //console.log(ui);

        let lat = ui.item.lat;
        let lon = ui.item.lon;
        let name = ui.item.label;

        let lstObj = self.routeParams().slice();

        lstObj[index()].name(name);
        lstObj[index()].lat = lat;
        lstObj[index()].lon = lon;

        self.data.replaceAll(lstObj);
    }

    stepUp(oldIndex)
    {
        if(oldIndex == 0)
            return;
        else
        {
            let lst = this.routeParams().slice();

            let b = lst[oldIndex];
            lst[oldIndex] = lst[oldIndex - 1];
            lst[oldIndex - 1] = b;

            this.data.replaceAll(lst);
        }
    }

    stepDown(oldIndex)
    {
        if(oldIndex >= (this.routeParams().length - 1))
            return;
        else
        {
            let lst = this.routeParams().slice();

            let b = lst[oldIndex];
            lst[oldIndex] = lst[oldIndex + 1];
            lst[oldIndex + 1] = b;

            this.data.replaceAll(lst);
        }
    }

    onDatasourceRequestPOI(request, response)
    {
        let filter = { name: request["term"]};
        
        WebRequestCustomPOI.getInstance().autoComplete(filter).done((data) => {

            response($.map(data, (item) => {
                return {
                    label: item.name,
                    value: item.name,
                    lat: item.latitude,
                    lon: item.longitude,
                    color: item.color
                };
            }));
        });
    }


    getWaterMarkText(ind)
    {
        let text = "";
        if (ind == 0)
            text = "Start";
        else
            text = "End";
        
        return text;
    }

    changeIndex(items)
    {
        let self = this;
        if (items) {
            //console.log(items.length, this.data().length);

            if (items.length != this.data().length)
                return;

            //console.log("changed :: ", this.data());
            let newArr = [];
            ko.utils.arrayForEach(items, (itm, ind) => {

                //console.log(itm.el[0], ind);
                //console.log(itm.el[0].getAttribute("index"), self.data());

                newArr.push(self.data()[itm.el[0].getAttribute("index")]);

                $(itm.el[0]).find("img").attr("src", self.getPin(ind));
                $(itm.el[0]).attr("index", ind);
            });

            //$(".img-route-position").each(function (ind, ele) {
            //    console.log(ele, ind);
            //    ele.setAttribute("src", self.getPin(ind));
            //});

            this.routeParams.replaceAll(newArr);
        }
    }

    deleteNode(index)
    {
        let count = this.routeParams().length - 1;

        let stops = [];
        let newIndex = 0;
        ko.utils.arrayForEach(this.routeParams(), function (itm, ind) {
            if (ind == index) {
                //ตรง เอาออก

                if (index == 0) {
                    if (count + 1 <= 2)
                        stops.push({ index: newIndex, name: ko.observable(""), lat: null, lon: null });
                    else
                    { }
                }
                else if (index == count) {
                    if (count + 1 <= 2)
                        stops.push({ index: newIndex, name: ko.observable(""), lat: null, lon: null });
                    else
                    { }
                }
                else { }
            }
            else {
                //ไม่ตรง ใส่กลับ
                stops.push({ index: newIndex,name: itm.name, lat: itm.lat, lon: itm.lon });
            }

            newIndex++;
        });

        this.data.replaceAll(stops);
    }

    getPin(index) {
        let urlPic = "~/images/";
        if (index === (this.data().length - 1)) {
            urlPic += "pin_destinationE.png";
        }
        else if (index === 0) {
            urlPic += "pin_destinationB.png";
        }
        else {
            urlPic += "pin_poi_" + (index + 1).toString() + ".png";
        }

        return this.resolveUrl(urlPic);
    }

    onUnload() {

    }
}

export default {
    viewModel: RouteDragDrop,
    template: templateMarkup
};

import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./autocomplete.html";
import "jquery-ui";


/**
 *
 *
 * @class TextBox
 * @extends {ControlBase}
 */
class Autocomplete extends ControlBase {
    /**
     * Creates an instance of TextBox.
     *
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.placeholder = this.ensureObservable(params.placeholder);
        this.value = this.ensureObservable(params.value);
        this.maxlength = this.ensureObservable(params.maxlength, 100);
        this.type = this.ensureObservable(params.type, 'search');
        this.minLength = this.ensureNonObservable(params.minLength, 3);
        this.onDatasourceRequest = this.ensureFunction(params.onDatasourceRequest, []);
        this.onSelectedItem = this.ensureFunction(params.onSelectedItems, this._onSelectedItem.bind(this));

        this.selectedValue = this.ensureObservable(params.selectedValue, null);

        this.watchChange = this.ensureNonObservable(params.watchChange, false);

        this.staticVal = this.ensureNonObservable(params.selectedValue);

        this.onKeypress = this.ensureFunction(params.onKeypress, this._onKeypress.bind(this));

    }

    _onSelectedItem(event, ui) {     
        this.selectedValue(ui.item);
        // this.value(ui.item.label);
        this.value(`"${ui.item.label}"`);
    }

    _onKeypress(d,e){
        e.keyCode === 13 && console.log(this.value()); 
        return true;
    };


    onDomReady() {
        var self = this;
        var $el = $("#" + this.id);
        $el.autocomplete({
            source: this.onDatasourceRequest,
            minLength: this.minLength,
            select: this.onSelectedItem,
            search: () => {
                $el.addClass('k-loading-image');
                $el.css({
                    "background": "no-repeat right center",
                    "background-size": "20px 20px",
                    "background-color": "white",
                    "position": "unset"
                });
            },
            response: () => {
                $el.removeClass('k-loading-image');
                $el.css({
                    "background": "",
                    "background-size": "",
                    "background-color": "",
                    "position": ""
                });
            }
        }).autocomplete('instance')._renderItem = ( ul, item ) => {
            return $("<li></li>")
                    .append('<div style="color:' + item.color + '">' + item.label + '</div>')
                    .appendTo(ul);
        };

        if (this.watchChange) {

            this.selectedValue.subscribe(function (newValue) {

                if (newValue !== self.staticVal) {
                    $el.addClass("watch-change");
                }
                else {
                    $el.removeClass("watch-change");
                }
            });
        }

    }

    onUnload() {
        var $el = null;
    }


}

export default {
viewModel: Autocomplete,
    template: templateMarkup
};
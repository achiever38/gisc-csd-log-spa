﻿import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./calendar.html";
import "jquery";
import "moment";
import "full-calendar";
import Logger from "../../../../app/frameworks/core/logger";

class Calendar extends ControlBase {
    constructor(params) {
        super(params);
        this.selected = this.ensureFunction(params.selected);
        this.data = params.data;

        var today = new Date();
        var day = ("0" + (today.getDate())).slice(-2);;
        var month = ("0" + (today.getMonth() + 1)).slice(-2);
        var year = today.getFullYear();
       
      
        this.currentdate = year + "-" + month + "-" + day;        
     
        this.test1 = [
            {
                data:{appointmentId:36},
                formatStart:"26 สิงหาคม 2560 15:30:00",
                start:"2017-08-26T15:30:00",
                title:"1กค-9856↵ทดสอบ MA Plan"
            },
          {
              data:{appointmentId:36},
              formatStart:"26 สิงหาคม 2560 17:30:00",
              start:"2017-08-26T17:30:00",
              title:"1กค-9856↵ทดสอบ MA Plan"
          },
              {
                  data:{appointmentId:36},
                  formatStart:"26 สิงหาคม 2560 15:30:00",
                  start:"2017-08-26T15:30:00",
                  title:"1กค-9856↵ทดสอบ MA Plan"
              },
               {
                      data:{appointmentId:36},
                      formatStart:"26 สิงหาคม 2560 17:30:00",
                      start:"2017-08-26T17:30:00",
                      title:"1กค-9856↵ทดสอบ MA Plan"
               },
                  {
                      data:{appointmentId:36},
                      formatStart:"26 สิงหาคม 2560 17:30:00",
                      start:"2017-08-26T17:30:00",
                      title:"1กค-9856↵ทดสอบ MA Plan"
                  },
  
               
                             
                                  
                            
        ]
    }

    //Test 1 Appointment On time
    afterRender(){
        setTimeout(() => {
            var initialLocaleCode = 'en';
            var self = this;
            let calendarHeight = $(".div-dashboard-col").css("height").split("px");
            calendarHeight = parseInt(calendarHeight[0] - 70);
            this.data.subscribe(function(newValue) {
                // alert("The person's new name is " + newValue);
                //$('.fc-view-container .fc-view > table').css({
                //    'width': '99%',
                //    'margin': 'auto'
                //});
                //$('.fc-view-container').css('width', '99%');

                //------  Subscribe --------
                $("#" + self.id).fullCalendar('removeEvents');
                setTimeout(() => {
                    $("#" + self.id).fullCalendar('addEventSource',newValue);
               

                    var ArrCheck = [];
                    if(newValue.length > 1){
           
                        //หาเวลาตั้งต้น
                        for(var i = 0 ;i < newValue.length;i++){
                    
                            var checkCount =  0;
                            for(var c = 0; c < newValue.length;c++){

                                if(newValue[c].start === newValue[i].start ){

                                    checkCount += 1;
                                }
                            }
               
                            ArrCheck.push(checkCount);
                            // console.log("จำนวนในช่วงเวลานั้น",checkCount);
                    
                        }
        

                       // console.log("Arrcheck new",ArrCheck);
                        var max = Math.max(...ArrCheck);
                     //   console.log("Count  max new",max);

                        if(max >= 10){
                            $('.fc-view-container .fc-view > table').css({
                                'width': '200%',
                                'margin': 'auto'
                            });

                            $('.fc-view-container .fc-view').css({
                                'overflow-x': 'scroll'
                            });
                        }
                        else if(max >= 5){
                            $('.fc-view-container .fc-view > table').css({
                                'width': '150%',
                                'margin': 'auto'
                            });

                            $('.fc-view-container .fc-view').css({
                                'overflow-x': 'scroll'
                            });
                        }
                        else if(max >= 3){

                            $('.fc-view-container .fc-view > table').css({
                                'width': '130%',
                                'margin': 'auto'
                            });

                            $('.fc-view-container .fc-view').css({
                                'overflow-x': 'scroll'
                            });
           
                        }
                        else
                        {
                            $('.fc-view-container .fc-view > table').css({
                                'width': '99%',
                                'margin': 'auto'
                            });
                        }
                    
                    }else{
                        $('.fc-view-container .fc-view > table').css({
                            'width': '99%',
                            'margin': 'auto'
                        });
            
              
                    }
          
                    $('.fc-view-container').css('width', '99%');

                
                }, 500);
            });
           
            //------ End Subscribe --------


            //-------- Create Calendar --------
            $("#" + self.id).fullCalendar({
                header: {
                    left: '',
                    center: 'title',
                    right: ''   
                },
                height: calendarHeight,
                defaultDate: this.currentdate,
                defaultView: 'agendaDay',
                defaultTimedEventDuration: '01:30:00',
                locale: initialLocaleCode,
                events:  this.data(),         
                eventColor: 'rgb(27, 169, 128)',

                eventClick: function (calData) {

                  
                    var appointment = { id: calData.data.appointmentId };
              
                    this.selected(appointment);

                }.bind(this),

                eventMouseover: function (event, element) {
                    
                }
            });

          //  console.log("Data Calendar",this.data());
   
         

      
            //$('.fc-view-container .fc-view > table').css({
            //    'width': '99%',
            //    'margin': 'auto'
            //});
            //$('.fc-view-container').css('width', '99%');


            var ArrCheck = [];
            if(this.data().length > 1){
           
                //หาเวลาตั้งต้น
                for(var i = 0 ;i < this.data().length;i++){
                    
                    var checkCount =  0;
                    for(var c = 0; c < this.data().length;c++){

                        if(this.data()[c].start === this.data()[i].start ){

                            checkCount += 1;
                        }
                    }
               
                    ArrCheck.push(checkCount);
          
                    
                }
        

              //  console.log("Arrcheck",ArrCheck);
                var max = Math.max(...ArrCheck);
               // console.log("Count  max",max);


                if(max >= 10){
                    $('.fc-view-container .fc-view > table').css({
                        'width': '250%',
                        'margin': 'auto'
                    });

                    $('.fc-view-container .fc-view').css({
                        'overflow-x': 'scroll'
                    });
                }
                else if(max >= 5){
                    $('.fc-view-container .fc-view > table').css({
                        'width': '180%',
                        'margin': 'auto'
                    });

                    $('.fc-view-container .fc-view').css({
                        'overflow-x': 'scroll'
                    });
                }
                else if(max >= 3){

                    $('.fc-view-container .fc-view > table').css({
                        'width': '130%',
                        'margin': 'auto'
                    });

                    $('.fc-view-container .fc-view').css({
                        'overflow-x': 'scroll'
                    });
           
                }
                else
                {
                    $('.fc-view-container .fc-view > table').css({
                        'width': '99%',
                        'margin': 'auto'
                    });
                }
            }else{
                $('.fc-view-container .fc-view > table').css({
                    'width': '99%',
                    'margin': 'auto'
                });
              
            }

            $('.fc-view-container').css('width', '99%');




            //---------------ตัวสำเร็จ----------------
            //        $('.fc-view-container .fc-view > table').css({
            //            'width': '99%',
            //            'margin': 'auto'
            //        });
            //        $('.fc-view-container').css('width', '99%');

            //        var ArrCheck = [];
            //        if(this.data().length > 1){
           
            //            //หาเวลาตั้งต้น
            //            for(var i = 0 ;i < this.data().length;i++){
                    
            //                var checkCount =  0;
            //                for(var c = 0; c < this.data().length;c++){

            //                    if(this.data()[c].start === this.data()[i].start ){

            //                        checkCount += 1;
            //                    }
            //                }
               
            //                ArrCheck.push(checkCount);
            //               // console.log("จำนวนในช่วงเวลานั้น",checkCount);
                    
            //            }
        

            //            console.log("Arrcheck",ArrCheck);
            //            var max = Math.max(...ArrCheck);
            //            console.log("Count  max",max);


            //            //if(checkCount >= 3){

            //            //    // console.log("ช่วงเวลานั้นเกิน 3",checkCount);
            //            //    $('.fc-view-container .fc-view > table').css({
            //            //        'width': '120%',
            //            //        'margin': 'auto'
            //            //    });
            //            //    $('.fc-view-container .fc-view').css({
            //            //        'overflow-x': 'scroll'
            //            //    });
            //            //}

            //            if(checkCount >= 10){
            //                $('.fc-view-container .fc-view > table').css({
            //                    'width': '200%',
            //                    'margin': 'auto'
            //                });

            //                $('.fc-view-container .fc-view').css({
            //                    'overflow-x': 'scroll'
            //                });
            //            }
            //            else if(checkCount >= 5){
            //                $('.fc-view-container .fc-view > table').css({
            //                    'width': '150%',
            //                    'margin': 'auto'
            //                });

            //                $('.fc-view-container .fc-view').css({
            //                    'overflow-x': 'scroll'
            //                });
            //            }
            //            else if(checkCount >= 3){

            //                $('.fc-view-container .fc-view > table').css({
            //                    'width': '120%',
            //                    'margin': 'auto'
            //                });

            //                $('.fc-view-container .fc-view').css({
            //                    'overflow-x': 'scroll'
            //                });
           
            //            }
            //        }
            
        }, 500);


    
    }
}

export default {
    viewModel: Calendar,
    template: templateMarkup
};
import ko from "knockout";
import i18nextko from "knockout-i18next";
import Conductor from "./conductor";
import Logger from "../../../../app/frameworks/core/logger";
import MessageboxManager from "../messagebox/messageboxManager";
import * as BladeDialog from "../../../../app/frameworks/constant/bladeDialog";

/**
 * Journey contains blades for current working task.
 * @public
 * @class Journey
 * @extends {Conductor}
 */
class Journey extends Conductor {

    /**
     * Creates an instance of Journey.
     * @public
     */
    constructor() {
        super();
        this.allowMultipleActive = true;
        this.quickLaunch = ko.pureComputed(() => {
            var text = "";
            ko.utils.arrayForEach(this.items(), function(item) {
                text = text + item.title();
            });
            return text;
        }, this);
    }

    /**
     * Activate new blade to journey.
     * 
     * @public
     * @param {any} item
     * @param {any} opener
     */
    activate(item, opener) {
        if (!this.exists(item)) {
            // Find all successors refer to openner.
            var successors = this.successors(opener);

            if(successors.length){
                // Deactivate first one then next one will be close automatically.
                this.deactivate(successors[0], true, true, false);
            }
        }

        // Activate new blade to journey.
        super.activate(item);
    }
    
    /**
     * Deactivate blade and remove from journey.
     * 
     * @public
     * @param {any} item
     * @param {boolean} [forceClose=true]
     * @param {boolean} [includeSelf=true]
     */
    deactivate(item, forceClose = true, includeSelf = true, informParent = true) {
        var deleteableBlades = this.successors(item, includeSelf);
        var parentBlades = this.ancestors(item, false);

        // Real remove from base class.
        deleteableBlades.forEach(function(b) {
            // Clear out transition, to prevent continuous out-in combo
            b.nextTransitionInfo = null;
            super.deactivate(b, forceClose);
        }, this);

        // Inform parent, in this case only parent, not grand-parent
        // Expect max:1 per iteration (can be zero)
        if(informParent) {
            parentBlades.forEach(function(p) {
                p.onChildDeactivate(true);
            }, this);
        }

        // If no blade available then close journal itself.
        if (this.isEmpty()) {
            var parentNavigation = this.getParent(this);
            if(parentNavigation){
                Logger.log("Journey empty try cleanup.");
                parentNavigation.deactivate(this, true);
            }
        }
    }

    /**
     * Try deactivate the given item. if remove flag is false then use normal deactivation process.
     * Else check if item can be removed and confirm with message M100.
     * callbackBeforeDeactivation will trigger BEFORE the actual deactivation.
     * 
     * @param {any} item
     * @param {boolean} [remove=false]
     * @param {any} [callbackBeforeDeactivation=null]
     * @returns
     */
    tryDeactivate(item, remove = false, callbackBeforeDeactivation = null) {
        if(!remove) {
            this.deactivate(item, remove);
            return;
        }

        var meAnSuccessors = this.successors(item, true);
        var canClose = true;
        for(let i = 0; i < meAnSuccessors.length; i++) {
            canClose = canClose && meAnSuccessors[i].canClose();
            if(!canClose) break;
        }

        if(canClose) {
            if(callbackBeforeDeactivation) callbackBeforeDeactivation();
            this.deactivate(item, true);
        } else {
            MessageboxManager.getInstance().showMessageBox("", i18nextko.t("M100")(), BladeDialog.DIALOG_OKCANCEL).done((response)=> {
                if(BladeDialog.BUTTON_OK === response) {
                    if(callbackBeforeDeactivation) callbackBeforeDeactivation();
                    this.deactivate(item, true);
                }
            });
        }
    }

    /**
     * Check all item belong to Journey if all canClose.
     * 
     * @returns true if all canClose, else false.
     */
    canRemove() {
        var canRemove = true;
        for(let i = 0; i < this.items().length; i++) {
            canRemove = canRemove && this.items()[i].canClose();
            if(!canRemove) break;
        }
        return canRemove;
    }

    /**
     * Find blade by id, null if not exists
     * 
     * @public
     * @param {any} itemId
     * @returns {Blade} target blade
     */
    findItemById(itemId) {
        var item = null;
        for(let i = 0; i < this.items().length; i++) {
            var current = this.items()[i];
            if(current.id == itemId) {
                item = current;
                break;
            }
        }
        return item;
    }
    
    /**
     * Life cycle: trigger when Journey is activated.
     * @public
     * @param {boolean} isFirstInitialize
     */
    onActivate(isFirstInitialize){
        Logger.log("Journey onActivate", this.id, isFirstInitialize);
    }
    
    /**
     * Life cycle: trigger when Journey is deactived.
     * @public
     */
    onDeactivate(){
        Logger.log("Journey onDeactivate", this.id);
    }

    /**
     * Is this journey active
     * 
     * @public
     * @returns true if active, false otherwise.
     */
    isActive() {
        var that = this.parent.instance.activeItem();
        return _.isEqual(this, that);
    }
}

export default Journey;
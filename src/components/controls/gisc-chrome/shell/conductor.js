import ko from "knockout";
import ObjectBase from "../../../../app/frameworks/core/objectBase";

/**
 * Conductor is a data structor which conduct collection of item
 * The collection can have one item active at a time by default.
 * This behavior can be changed by set allowMultipleActive to true.
 * 
 * Conductor manage life cycle of collection item by telling when
 * the state changed {active, inactive}. 
 * 
 * @class Conductor
 * @extends {ObjectBase}
 */
class Conductor extends ObjectBase {
    
    /**
     * Creates an instance of Conductor.
     */
    constructor() {
        super();
        this.allowMultipleActive = false;
        this.items = ko.observableArray();
        this.activeItem = ko.observable(null);
        this.hasActiveItem = ko.pureComputed(()=>{
            return this.activeItem() !== null;
        }, this);
        this.count = ko.pureComputed(() => {
            return this.items().length;
        }, this);
        this.isEmpty = ko.pureComputed(() => {
            return this.count() === 0;
        }, this);
        this.nonActiveItems = ko.pureComputed(()=> {
            var result = [];
            var activeItem = this.activeItem();
            this.items().forEach(function(element) {
                if(element !== activeItem){
                    result.push(element);
                }
            }, this);
            return result;
        });
    }

    /**
     * Check if given item is in the Conductor
     * @public
     * @param {any} item
     * @returns
     */
    exists(item) {
        return this.items.indexOf(item) !== -1;
    }

    /**
     * Get all successors from given item, if includeReference is set to true
     * then including self.
     * @public
     * @param {any} item
     * @param {boolean} [includeReference=false]
     * @returns {array}
     */
    successors(item, includeReference = false) {
        var successors = [];

        if (this.exists(item)) {

            var startIndex = this.items.indexOf(item);
            if(!includeReference){
                // Moving to next children.
                startIndex += 1;
            }

            // Verify startIndex should not exceed maximum array length.
            startIndex = Math.min(startIndex, this.count());

            // Extract array from slice method.
            successors = this.items().slice(startIndex);
        }

        return successors;
    }

    /**
     * Get all ancestors from given item, if includeAllAncestors is set to false
     * then return only one level parent.
     * @public
     * @param {any} item
     * @param {boolean} [includeAllAncestors=true]
     * @returns {array}
     */
    ancestors(item, includeAllAncestors = true) {
        var ancestors = [];

        if (this.exists(item)) {
            var stopIndex = this.items.indexOf(item);
            var startIndex = includeAllAncestors ? 0 : stopIndex - 1;

            // Verify stopIndex should not exceed maximum array length.
            stopIndex = Math.min(stopIndex, this.count());

            // Extract array from slice method.
            ancestors = this.items().slice(startIndex, stopIndex);
        }

        return ancestors;
    }

    /**
     * 
     * @public
     * @param {any} item
     * @returns
     */
    getParent(item) {
        if (item.parent && item.parent.instance) {
            return item.parent.instance;
        }
        return null;
    }

    /**
     * 
     * @public
     * @param {any} item
     */
    setParent(item) {
        item.parent = function() {}; // Prevent KO serialize unnecessary property.
        item.parent.instance = this;
    }

    /**
     * Acvitate gien item, set it to this.activeItem
     * @public
     * @param {any} item
     */
    activate(item) {
        var isNewlyCreate = false;

        // Verify item exists if not append to collection.
        if (!this.exists(item)) {
            this.items.push(item);
            this.setParent(item);
            isNewlyCreate = true;
        }

        // Calling deactivate current item before activate new one.
        if (!this.allowMultipleActive) {
            var currentActive = this.activeItem();
            if (currentActive && (currentActive !== item)) {
                this.deactivateItem(currentActive);
            }
        }

        // Update active item for current conductor.
        this.activateItem(item, isNewlyCreate);
    }
    
    /**
     * Activate item internally
     * @private
     * @param {any} item
     * @param {any} isNewlyCreate
     */
    activateItem(item, isNewlyCreate) {
        if (item.onActivate) {
            item.onActivate(isNewlyCreate);
        }

        // Set current active item.
        this.activeItem(item);
    }
    
    /**
     * Deactivate item internally
     * @private
     * @param {any} item
     * @param {boolean} [remove=false]
     */
    deactivateItem(item, remove = false) {
        // Execute event handler onDeactivate execute before adding.
        if (item.onDeactivate) {
            item.onDeactivate(remove);
        }

        // Check for deactivate child too.
        if(ko.isObservable(item.items)){
            item.items().forEach(function(element) {
                this.deactivateItem(element, remove);
            }, this);
        }
    }
    
    /**
     * Remove item internally
     * @private
     * @param {any} item
     */
    removeItem(item){
        // Cleanup parent reference.
        if (item.parent){
            item.parent.instance = null;
            item.parent = null;
        }

        // Remove from array.
        this.items.remove(item);
    }
    
    /**
     * Deactivate given item, if remove flag is set then 
     * remove that item after deactivation process finish.
     * 
     * @public
     * @param {any} item
     * @param {boolean} [remove=false]
     * @returns
     */
    deactivate(item, remove = false) {
        // Check if item is null then skip it.
        if (!item) {
            return;
        }

        // Execute event handler onDeactivate execute before adding.
        this.deactivateItem(item, remove);

        // If current active is same item then set null.
        if(item === this.activeItem()){
            this.activeItem(null);
        }

        // If remove flag turn on then remove from array.
        if (remove) {
            // Cleanup parent reference.
            this.removeItem(item);
        }
    }
}

export default Conductor;
import "jquery";
import "hammerjs";
import ko from "knockout";
import ObjectBase from "../../../../app/frameworks/core/objectBase";
import Logger from "../../../../app/frameworks/core/logger";
import gc from "../../../../app/frameworks/core/gc";
import VisualEffect from "../../../../app/frameworks/core/visualEffect";
import Utility from "../../../../app/frameworks/core/utility";
import * as CSSContant from "../../../../app/frameworks/constant/stylesheet";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeType from "../../../../app/frameworks/constant/bladeType";
import FocusManager from "../../../../app/frameworks/core/focusManager";

/**
 * Sample class document - change me!
 * 
 * @class Blade
 * @extends {ObjectBase}
 */
class Blade extends ObjectBase {

    /**
     * Creates an instance of Blade.
     * 
     * @param {any} componentName
     * @param {any} [options=null]
     */
    constructor(componentName, options = null) {
        super();
        this.componentName = componentName || "";
        this.componentOptions = options;
        this.componentInstance = ko.observable(null);
        this.title = ko.pureComputed(this.computeTitle, this);
        this.description = ko.pureComputed(this.computeDescription, this);
        this.isLoaded = ko.pureComputed(this.computeIsLoaded, this);
        this.bladeSize = ko.pureComputed(this.computeBladeSize, this);
        this.bladeMargin = ko.pureComputed(this.computeBladeMargin, this);
        this.canMinimize = ko.observable(true);
        this.canMaximize = ko.pureComputed(this.computeBladeCanMaximize, this);
        this.isMinimized = ko.observable(false);
        this.isMaximized = ko.observable(false);
        this.isBusy = ko.observable(false);
        this.bladeLayoutCss = ko.pureComputed(this.onCalculateLayoutCSS, this);
        this.bladeSizeCss = ko.pureComputed(this.onCalculateBladeCSS, this);
        this.bladeType = ko.pureComputed(this.computeBladeType, this);
        this._hammerInstance = null;
        this.onDragEnd = null;
        this.statusType = ko.observable();
        this.statusText = ko.observable();
        this.statusVisible = ko.observable(false);
        this.bladeEnableQuickSearch = ko.pureComputed(this.computeBladeEnableQuickSearch, this);;

        this.nextTransitionInfo = null;

        this.isError = ko.pureComputed(this.computeBladeIsError, this);
        this.errorMessage = ko.pureComputed(this.computeBladeErrorMessage, this);

        this._viewState = {};
    }
    computeBladeSize() {
        return this.componentInstance() ? this.componentInstance().bladeSize : BladeSize.Small;
    }
    computeBladeCanMaximize() {
        return this.componentInstance() ? this.componentInstance().bladeCanMaximize : true;
    }
    computeBladeMargin() {
        return this.componentInstance() ? this.componentInstance().bladeMargin : BladeMargin.Normal;
    }
    computeIsLoaded() {
        return this.componentInstance() ? this.componentInstance().isLoaded() : false;
    }
    computeTitle() {
        return this.componentInstance() ? this.componentInstance().bladeTitle() : "";
    }
    computeDescription() {
        return this.componentInstance() ? this.componentInstance().bladeDescription() : "";
    }
    computeBladeIsError() {
        return this.componentInstance() ? this.componentInstance().isError() : false;
    }
    computeBladeErrorMessage() {
        return this.componentInstance() ? this.componentInstance().errorMessage() : "";
    }
    computeBladeType() {
        return this.componentInstance() ? this.componentInstance().bladeType : BladeType.Standard;
    }
    computeBladeEnableQuickSearch() {
        return this.componentInstance() ? this.componentInstance().bladeEnableQuickSearch : false;
    }

    onActivate(isFirstInitialize) {
        Logger.log(" Blade onActivate", this.id, isFirstInitialize);
    }
    onDeactivate(isClosing) {
        Logger.log(" Blade onDeactivate", this.id, isClosing);
        if (isClosing) {
            var isJourneyActive = this.parent.instance.isActive();
            if (isJourneyActive && this.nextTransitionInfo) {
                // console.log("< blade.onDeactivate(true)");
                this.transitionOut(this.nextTransitionInfo);
                this.nextTransitionInfo = null;
            }
            this.dispose();
        }
    }
    onChildDeactivate(isClosing) {
        if(isClosing) {
            if (this.componentInstance().onChildScreenClosed) {
                this.componentInstance().onChildScreenClosed();
            }
        }
    }
    onCalculateLayoutCSS() {
        var bladeLayoutCssClass = [
            "app-blade-stacklayout",
            "app-stacklayout-vertical",
            "app-stacklayout"
        ];

        if(this.isBusy()) {
            bladeLayoutCssClass.push(CSSContant.APP_BLADE_LAYOUT_STATIC);
        }

        return bladeLayoutCssClass.join(" ");
    }
    onCalculateBladeCSS() {
        var bladeSizeCssClass = [];
        switch (this.bladeSize()) {
            case BladeSize.XSmall:
                bladeSizeCssClass.push(CSSContant.APP_BLADE_SIZE_XSMALL);
                break;
            case BladeSize.Small:
                bladeSizeCssClass.push(CSSContant.APP_BLADE_SIZE_SMALL);
                break;
            case BladeSize.Medium:
                bladeSizeCssClass.push(CSSContant.APP_BLADE_SIZE_MEDIUM);
                break;
            case BladeSize.XMedium:
                bladeSizeCssClass.push(CSSContant.APP_BLADE_SIZE_XMEDIUM);
                break;
            case BladeSize.Large:
                bladeSizeCssClass.push(CSSContant.APP_BLADE_SIZE_LARGE);
                break;
            case BladeSize.XLarge:
                bladeSizeCssClass.push(CSSContant.APP_BLADE_SIZE_XLARGE);
                break;
            case BladeSize.XLarge_A0:
                bladeSizeCssClass.push(CSSContant.APP_BLADE_SIZE_XLARGE_A0);
                break;
            case BladeSize.XLarge_A1:
                bladeSizeCssClass.push(CSSContant.APP_BLADE_SIZE_XLARGE_A1);
                break;
            case BladeSize.XLarge_A2:
                bladeSizeCssClass.push(CSSContant.APP_BLADE_SIZE_XLARGE_A2);
                break;
            case BladeSize.XLarge_Report:
                bladeSizeCssClass.push(CSSContant.APP_BLADE_SIZE_XLARGE_REPORT);
                break;
        }

        switch (this.bladeMargin()) {
            case BladeMargin.Narrow:
                if (!this.isError()) {
                    // Do not allow narrow layout for error page.
                    bladeSizeCssClass.push(CSSContant.APP_BLADE_MARGIN_NARROW);
                }
                break;
        }

        switch (this.bladeType()){
            case BladeType.Compact:
                bladeSizeCssClass.push(CSSContant.APP_BLADE_TYPE_COMPACT);
                break;
        }

        // Append minimized state.
        if (this.isMinimized()) {
            bladeSizeCssClass.push(CSSContant.APP_BLADE_MINIMIZED);
        } else if (this.isMaximized()) {
            bladeSizeCssClass.push(CSSContant.APP_BLADE_MAXIMIZED);
        }

        return bladeSizeCssClass.join(" ");
    }

    /**
     * Handle when control has already render in page. 
     * 
     * @param {any} element
     */
    afterRender(element) {
        // At this method this refers to blade not component so we need to use utility class to detect device.
        if(!Utility.isTouchDeviceSupported()) {
            // Handle pan header by hammer js.
            var $bladeHeader = $(element).find(".app-blade-header");
            var $appPortalContent = $(".app-portal-content");
            var hammertime = new Hammer.Manager($bladeHeader.get(0), {
                recognizers: [
                    [Hammer.Pan, {
                        direction: Hammer.DIRECTION_HORIZONTAL
                    }],
                ]
            });

            var ACT_DE = "dragend";
            var ACT_PS = "panstart";
            var lastAction = "";
            var allowPan = false;
            var lastDeltaX = 0;
            var excludeDrag = "INPUT";

            // Dynamic registration for detect panend event.
            this.onDragEnd = (e) => {
                lastAction = ACT_DE;
            };

            // Register drag end event on body, this will let us know the last drag performed.
            $(document.body).on("dragend", this.onDragEnd);

            // Initalize hammer's panstart event, skip if immediate from dragend.
            hammertime.on("panstart", function(ev) {
                if(lastAction == ACT_DE) {
                    allowPan = false;
                }
                else {
                    allowPan = true;
                }

                if(ev.target.tagName.toUpperCase() === excludeDrag.toUpperCase()){
                    allowPan = false;
                }

                lastAction = ACT_PS;
                if(!allowPan) {
                    return;
                }

                $(document.body).addClass(CSSContant.APP_PANNING_CURSOR);
                $bladeHeader.addClass(CSSContant.APP_PANNING_CURSOR);
            });

            // Initiailize hammer's panmove event, sync horizontal blade movement.
            hammertime.on("panmove", function(ev) {
                if(!allowPan) {
                    return;
                }

                var diff = (lastDeltaX - ev.deltaX);
                lastDeltaX = ev.deltaX;
                $appPortalContent.scrollLeft($appPortalContent.scrollLeft() + diff);
            });

            // Initialize hammer's panend event, restore default pointer.
            hammertime.on("panend", function(ev) {
                if(!allowPan) {
                    return;
                }

                lastDeltaX = 0;
                $(document.body).removeClass(CSSContant.APP_PANNING_CURSOR);
                $bladeHeader.removeClass(CSSContant.APP_PANNING_CURSOR);
            });

            // Store element for destroy.
            this._hammerInstance = hammertime;
        }

        // Show transition visual effect if there is nextTransitionInfo
        if (this.nextTransitionInfo) {
            this.transitionIn(this.nextTransitionInfo);
        }
    }
    
    /**
     * Show transition In effect
     * 
     * @private
     * @param {any} transitionInfo
     */
    transitionIn(transitionInfo) {
        // console.log("< blade.transitionIn");
        VisualEffect.getInstance().snapToBlade(this.id, transitionInfo.ancestorIds);
    }

    /**
     * Show transition Out effect
     * 
     * @private
     * @param {any} transitionInfo
     */
    transitionOut(transitionInfo) {
        // console.log("< blade.transitionOut");
        var shifted = transitionInfo.ancestorIds.shift();
        VisualEffect.getInstance().snapToBlade(shifted, transitionInfo.ancestorIds);
    }

    /**
     * User maximize blade.
     * @public
     */
    maximize() {
        this.isMaximized(!this.isMaximized());
        this.isMinimized(false);

        // Show transition visual effect if there is nextTransitionInfo
        if (this.nextTransitionInfo) {
            this.transitionIn(this.nextTransitionInfo);
        }

        // Raise event for blade resize.
        $("#" + this.id).trigger("blade.resize");

        // Notify focus manager because HTML prevent bubble.
        FocusManager.getInstance().handleActiveElementChanged($("#" + this.id).get(0));
    }

    /**
     * User restore blade.
     * @public
     */
    restore() {
        if (this.isMinimized()) {
            this.isMinimized(false);

            // Raise event for blade resize.
            $("#" + this.id).trigger("blade.resize");
        }

        // Notify focus manager because HTML prevent bubble.
        FocusManager.getInstance().handleActiveElementChanged($("#" + this.id).get(0));

        return true;
    }

    /**
     * User minimize blade.
     * @public
     */
    minimize() {
        this.isMinimized(true);
        this.isMaximized(false);

        // Notify focus manager because HTML prevent bubble.
        FocusManager.getInstance().handleActiveElementChanged($("#" + this.id).get(0));
    }

    /**
     * User close blade manually from close button.
     * @public
     */
    close() {
        // Call Journey.tryDeactivate() which check against guard function.
        // If the deactivation process is about to continue, callback in last param
        // is to display visual effect BEFORE the deactivation finish.
        this.parent.instance.tryDeactivate(this, true, ()=>{
            if (this.nextTransitionInfo) {
                // console.log("< blade.close()");
                this.transitionOut(this.nextTransitionInfo);
                this.nextTransitionInfo = null;
            }
        });

        // Notify focus manager because HTML prevent bubble.
        FocusManager.getInstance().handleActiveElementChanged($("#" + this.id).get(0));
    }

    /**
     * Guard method to check if the Blade can be closed.
     * 
     * @returns
     */
    canClose() {
        var canClose = true;
        // return this.componentInstance().canClose();
        if(this.componentInstance()){
            canClose = this.componentInstance().canClose();
        }
        return canClose;
    }

    /**
     * Handle GC when user close blade instance.
     */
    dispose() {
        if (this._hammerInstance) {
            this._hammerInstance.destroy();
            this._hammerInstance = null;
            $(document.body).off("dragend", this.onDragEnd);
        }

        if (this.nextTransitionInfo) {
            this.nextTransitionInfo = null;
        }

        if (this.componentInstance() && this.componentInstance()._onViewModelDisposing) {
            this.componentInstance()._onViewModelDisposing();
        }
    }

    /** -- Blade Storage -- **/
    
    /**
     * Used by gisc-component internally to provide "BladeStorage" which is
     * transient storage exist only when blade is present.
     * 
     * @param {any} key
     * @returns {object} bag object to store anything.
     */
    getItem(key) {
        return this._viewState[key];
    }
    
    /**
     * Used by gisc-component internally to provide "BladeStorage" which is
     * transient storage exist only when blade is present.
     * 
     * @param {any} key
     * @param {any} value
     */
    setItem(key, value) {
        this._viewState[key] = value;
    }
}

export default Blade;

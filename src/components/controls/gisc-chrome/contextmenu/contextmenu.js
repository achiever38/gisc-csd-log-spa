import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./contextmenu.html";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import Singleton from "../../../../app/frameworks/core/Singleton";
import FocusManager from "../../../../app/frameworks/core/focusManager";
import ContextMenuManager from "./contextmenuManager";

/**
 * Display system context menu.
 * 
 * @class ContextMenuControl
 * @extends {ControlBase}
 */
class ContextMenuControl extends ControlBase {
    /**
     * Creates an instance of Company Logo.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.manager = ContextMenuManager.getInstance();
        this.visible = this.manager.visible;
        this.items = ContextMenuManager.getInstance().items;
        this.offsetTop = ContextMenuManager.getInstance().offsetTop;
        this.offsetLeft = ContextMenuManager.getInstance().offsetLeft;
        this.cssClass = ko.pureComputed(() => {
            var cssClass = "app-commands-contextMenu app-contextMenu app-popup";
            if(this.visible()) {
                cssClass += " app-contextMenu-active";
            }
            return cssClass;
        });
    }

    /**
     * Get context menu control instance.
     * 
     * @param {any} params
     * @returns {ContextMenuControl}
     */
    static getInstance(params) {
        return Singleton.getInstance("contextMenuCtrl", new ContextMenuControl(params));
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {}

    /**
     * Handle when user clicks outside element.
     */
    onDomReady() {
        var $el = $("#" + this.id);
        FocusManager.getInstance().registerFocusElement(this.id, $el.get(0), (activeElement) => {
            if(!this.visible()) return;
            this.visible(false);
        });

        // Hide context menu when user scroll blade or screen.
        $("#main-panorama, .app-blade-content").on("scroll", function(){
            FocusManager.getInstance().handleActiveElementChanged(this);
        });
    }

    /**
     * Handle when user has been clicked on some context item.
     * @param {any} item
     */
    onItemClick(item) {
        if(this.manager.onItemClickHandler) {
            this.manager.onItemClickHandler.resolve(item);
        }
        // Hide context menu.
        this.visible(false);
    }
}

export default {
    viewModel: ContextMenuControl,
    template: templateMarkup
};
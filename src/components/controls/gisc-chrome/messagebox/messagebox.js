import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./messagebox.html";
import Logger from "../../../../app/frameworks/core/logger";
import MessageboxManager from "./messageboxManager";
import * as BladeDialog from "../../../../app/frameworks/constant/bladeDialog";

/**
 * Handle top navigation menu.
 * 
 * @class MessageBoxControl
 * @extends {ControlBase}
 */
class MessageBoxControl extends ControlBase {
    /**
     * Creates an instance of MessageBoxControl.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        // Hide by default.
        this.visible(false);

        // Binding to dialog members.
        this.dialogTitle = ko.observable();
        this.dialogMessage = ko.observable();
        this.dialogType = ko.observable();

        this.dialogList = [];
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {}
    /**
     * @lifecycle Called when view dom is ready to use.
     */
    onDomReady() {}
     /**
     * @lifecycle Called when dom is removed from page.
     */
    onUnload() {
        // This control is only one instance on page so donot need to handle unload.
    }

    /**
     * Handle when dialog was clicked.
     * 
     * @param {any} button
     */
    onDialogClick(button) {
        //find current dialog (last dialog in list) and resolve deferred
        var currentDialog = this.dialogList.pop();
        currentDialog.deferred.resolve(button);

        if (this.dialogList.length === 0) {
            // hide dialog if this is the last dialog
            this.visible(false);
        }
        else {
            // find next last dialog to display
            var nextDialog = this.dialogList[this.dialogList.length - 1];
            // Configured dialog params.
            this._setDialogParams(nextDialog.title, nextDialog.message, nextDialog.type);
        }
    }

    /**
     * Invoke command from component base.
     * 
     * @param {any} title
     * @param {any} message
     * @param {any} type
     * 
     * @memberOf MessageBoxControl
     */
    showMessageBox (title, message, type = "ok") {

        var currentDeferred = $.Deferred();

        // add dialog with deferred and params in list
        this.dialogList.push({
            deferred: currentDeferred,
            title: title,
            message: message,
            type: type
        });

        // Configured dialog params.
        this._setDialogParams(title, message, type);

        // Show messagebox on screen.
        this.visible(true);

        return currentDeferred;
    }

    
    /**
     * Configure dialog params
     * @param {any} title
     * @param {any} message
     * @param {any} type
     */
    _setDialogParams (title, message, type) {
        this.dialogTitle(title);
        this.dialogMessage(this.ensureNonObservable(message, ""));
        this.dialogType(type);
    }
}

export default {
    viewModel: {
        createViewModel: function(params, componentInfo) {
            // - 'params' is an object whose key/value pairs are the parameters
            //   passed from the component binding or custom element
            // - 'componentInfo.element' is the element the component is being
            //   injected into. When createViewModel is called, the template has
            //   already been injected into this element, but isn't yet bound.
            // - 'componentInfo.templateNodes' is an array containing any DOM
            //   nodes that have been supplied to the component. See below.
 
            // Return the desired view model instance, e.g.:
            return MessageboxManager.ensureInstance(new MessageBoxControl(params));
        }
    },
    template: templateMarkup
};
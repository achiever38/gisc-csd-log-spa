﻿import "jquery";
import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./workingMap.html";
import Logger from "../../../../app/frameworks/core/logger";
import WorkingMapManager from "./workingMapManager"
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import FocusManager from "../../../../app/frameworks/core/focusManager";
import EventAggregator from "../../../../app/frameworks/core/eventAggregator";
import * as EventAggregatorConstant from "../../../../app/frameworks/constant/eventAggregator";

/**
 * Handle top navigation menu.
 * 
 * @class MapControl
 * @extends {ControlBase}
 */
class WorkingMapControl extends ControlBase {
    constructor(params) {
        super(params);

        this.data = this.ensureObservable(params.data);
        this.mapEndpoint = this.resolveUrl(WebConfig.appSettings.workingMapUrl);
        this.mapManager = WorkingMapManager.getInstance();

        // Register event subscribe.
        // this._eventAggregator = new EventAggregator();
        // this._eventAggregator.subscribe(EventAggregatorConstant.MAP_COMMAND_COMPLETE, this.onMapComplete.bind(this));
    }

    onDomReady() {
        this.mapManager.initialize(this.id, this.mapEndpoint);

        //Detect click event on iframe to manage Menu bar lost focus.
        var $el = $("#" + this.id);
        $el.load(function () {
            $(this).contents().find("body").on('click', (e) => {
                FocusManager.getInstance().handleActiveElementChanged(e.currentTarget);
            });
        });
    }

    onMapComplete(data){
        // console.log("data",data);

        // switch (data.command)
        // {
        //     case "wm-map-api-ready":
        //         this.mapManager.setStartupResource(WebConfig.userSession);
        //         break;
        //     case "wm-set-startup-resources":
        //         console.log("mamam");
        //         //WorkingMapManager.getInstance().drawPolygonFromPolyLine();
        //     break;
        //     default:
        //         break;
        // }
    }

    /**
    * @lifecycle Called when dom is removed from page.
    */
   onUnload() {
        this.mapManager.destroy();

        var $el = $("#"+this.id);
        $el.unbind();
    }
}

export default {
    viewModel: WorkingMapControl,
    template: templateMarkup
}
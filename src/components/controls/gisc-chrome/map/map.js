import "jquery";
import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./map.html";
import Logger from "../../../../app/frameworks/core/logger";
import MapManager from "./mapManager";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import FocusManager from "../../../../app/frameworks/core/focusManager";
import EventAggregator from "../../../../app/frameworks/core/eventAggregator";
import * as EventAggregatorConstant from "../../../../app/frameworks/constant/eventAggregator";

import WidgetNavigation from "../shell/widgetNavigation";

/**
 * Handle top navigation menu.
 * 
 * @class MapControl
 * @extends {ControlBase}
 */
class MapControl extends ControlBase {
    /**
     * Creates an instance of MapControl.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.mapEndpoint = this.resolveUrl(WebConfig.appSettings.mapUrl);
        this.mapManager = MapManager.getInstance();
        this.visible(WebConfig.userSession.currentPortal === "company-workspace");
        // Register event subscribe.
        this._eventAggregator = new EventAggregator();
        this._eventAggregator.subscribe(EventAggregatorConstant.MAP_COMMAND_COMPLETE, this.onMapComplete.bind(this));

        this.widgetService = WidgetNavigation.getInstance();
    }

    /**
     * Navigate to target moduleName on a new Window
     * @public
     * @param {any} moduleName
     * @param {any} [options=null]
     */
    widget(componentName, options = null, widgetOptions = null) {
        this.widgetService.widget(componentName, options, widgetOptions);
    }

    onMapComplete(data) {

        switch (data.command)
        {
            case "map-api-ready":
                //console.log("WebConfig>>",WebConfig.companySettings)
                this.mapManager.setStartupResource(WebConfig.userSession, WebConfig.companySettings);
                break;
                
            case "open-vehicle-info-window":
                this.widget('cw-geo-fencing-vehicle-info-widget', data.result.attributes, {
                    title: data.result.attributes["TITLE"],
                    modal: false,
                    resizable: false,
                    target: "cw-geo-fencing-vehicle-info-widget",
                    width: "450px",
                    height: "400px",
                    id: 'cw-geo-fencing-vehicle-info-widget'
                })
                break;

            case "open-playback-footprint-window":
                this.widget('cw-fleet-monotoring-playback-info-widget', data.result.attributes, {
                    title: data.result.attributes["TITLE"],
                    modal: false,
                    resizable: false,
                    target: "cw-fleet-monotoring-playback-info-widget",
                    width: "450px",
                    height: "400px",
                    id: 'cw-fleet-monotoring-playback-info-widget'
                })
                break;

            case "close-vehicle-info-window":
                //$("#cw-geo-fencing-vehicle-info-widget").data("kendoWindow").close();
                break;
            case "close-playback-footprint-window":
                //$("#cw-fleet-monotoring-playback-info-widget").data("kendoWindow").close();
                break;
            case "open-vehicle-status-window":
                this.widget('cw-fleet-monitoring-common-vehicle-status-widget', data.result.attributes, {
                    title: this.i18n("Vehicle_Status_Title")(),
                    modal: false,
                    resizable: false,
                    target: "cw-fleet-monitoring-common-vehicle-status-widget",
                    width: "650px",
                    height: "285px",
                    id: 'cw-fleet-monitoring-common-vehicle-status-widget',
                    left: "25%",
                    bottom: "35%"
                });
                break;
            default:
                break;
        }
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {}
    /**
     * @lifecycle Called when view dom is ready to use.
     */
    onDomReady() {
        if(this.visible()){
            this.mapManager.initialize(this.id, this.mapEndpoint);
        }
        
        //Detect click event on iframe to manage Menu bar lost focus.
        var $el = $("#"+this.id);
        $el.load(function(){
            $(this).contents().find("body").on('click', (e)=>{ 
                FocusManager.getInstance().handleActiveElementChanged(e.currentTarget);
            });
        });
    }
    /**
    * @lifecycle Called when dom is removed from page.
    */
    onUnload() {
        if(this.visible()){
            this.mapManager.destroy();
        }

        var $el = $("#"+this.id);
        $el.unbind();

    }
}

export default {
viewModel: MapControl,
    template: templateMarkup
};
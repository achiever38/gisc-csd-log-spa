import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./usermenu.html";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import {Enums} from "../../../../app/frameworks/constant/apiConstant";
import {DropMenu} from "../shell/shellComponents";
import APICoreWebRequestUser from "../../../../app/frameworks/data/apicore/webRequestUser";
import APITrackingCoreWebRequestUser from "../../../../app/frameworks/data/apitrackingcore/webRequestUser";
import Utility from "../../../../app/frameworks/core/utility";
import EventAggregator from "../../../../app/frameworks/core/eventAggregator";
import * as EventAggregatorConstant from "../../../../app/frameworks/constant/eventAggregator";
import AuthenticationManager from "../authenticationManager/authenticationManager";
import FocusManager from "../../../../app/frameworks/core/focusManager";

/**
 * Handle top user navigation menu.
 * 
 * @class UserMenuControl
 * @extends {ControlBase}
 */
class UserMenuControl extends ControlBase {
    /**
     * Creates an instance of UserMenuControl.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.navigationService = params.navigationService;
        this.isShowJourney = params.isShowJourney;
        this.currentUser = WebConfig.userSession;
        this.userMenu = new DropMenu(this.i18n("Common_UserMenu"));
        this.items = ko.observableArray();
        // this.greeting = Utility.stringFormat(this.i18n("Common_Greeting")(), WebConfig.userSession.fullname);
        this.greeting = ko.observable(this.formatGreetingMessage(WebConfig.userSession.fullname));
        this._eventAggregator = new EventAggregator();
        this._eventAggregator.subscribe(EventAggregatorConstant.CURRENT_USERNAME_CHANGED, (username) => {
            this.greeting(this.formatGreetingMessage(username));

            if(!WebConfig.appSettings.isDebug){
                var ajaxOption  = {
                    url: this.resolveUrl(WebConfig.appSettings.accountUpdateUrl),
                    type: "post",
                    crossDomain: true,
                    xhrFields: { // Force jQuery send cookie request.
                        withCredentials: true
                    }
                };
                //Ajax to update cookie authentication
                $.ajax(ajaxOption).done((r) => {});
            }
        });

        // refresh menu items when needed.
        this._eventAggregator.subscribe(EventAggregatorConstant.REFRESH_USER_MENU, () => {
            this.refreshDatasource();
        });
    }

    formatGreetingMessage(username) {
        return Utility.stringFormat(this.i18n("Common_Greeting")(), username);
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        this.refreshDatasource();
    }
    /**
     * Handle user menu click.
     * 
     */
    onUserMenuClick (menuItem) {
        var allowDefaultLinkBehavior = true;

        switch(menuItem.id){
            case "companyPortal":
                if(WebConfig.userSession.accessibleCompanyCount !== 1) {
                    // Open blade for switch company portal if user has multiple accessible company.
                    // If user include zero accessible will display blank.
                    this.navigationService.navigate("bo-company-select");
                    this.isShowJourney(true);
                    allowDefaultLinkBehavior = false;
                }
                else {
                    // Open user portal in new tab with differrent url patterns.
                    var portalUrl = this.resolveUrl(WebConfig.appSettings.companyWorkspaceUrl);

                    // Replace company id parameters to URL.
                    portalUrl = Utility.stringFormat(portalUrl, WebConfig.userSession.accessibleFirstCompanyId);
                    window.open(portalUrl);

                    // Open same as select company page.
                    allowDefaultLinkBehavior = false;
                }
                break;

            case "userPreferences":
                // Open blade for user perferences.
                this.navigationService.navigate("cw-user-preference");
                this.isShowJourney(true);
                allowDefaultLinkBehavior = false;
                break;

            case "about":
                // Open blade for about.
                this.navigationService.navigate("bo-about");
                this.isShowJourney(true);
                allowDefaultLinkBehavior = false;
                break;

            case "logout":
                // Calling signout to api servers.
                AuthenticationManager.getInstance().signOut();

                // Signout MVC.
                allowDefaultLinkBehavior = false;
                break;
        }
        // Always hide current user expanded menu.
        this.userMenu.isActive(false);

        return allowDefaultLinkBehavior;
    }

    onDomReady() {
        var $el = $("#" + this.userMenu.id);

        FocusManager.getInstance().registerFocusElement(this.userMenu.id, $el.get(0), () => {
            if(!this.userMenu.isActive()) return;
            this.userMenu.isActive(false);

        });
    }

    
    /**
     * Refresh Menu Items
     */
    refreshDatasource() {
        this.items.removeAll();

        // Adding menu items based on user permissions.
        var isBackOfficeUser = (WebConfig.userSession.userType == Enums.UserType.BackOffice);

        // Adding back office portal menu for back office users.
        if(isBackOfficeUser &&
            WebConfig.userSession.currentPortal !== "back-office") {
            this.items.push({
                title: this.i18n("User_BackOfficePortal"),
                href: this.resolveUrl(WebConfig.appSettings.backOfficeUrl),
                target: "_backOffice"
            });
        }

        // Adding company portal menu if backoffice users has accessible companies.
        if(isBackOfficeUser && 
            WebConfig.userSession.currentPortal === "back-office") {
            this.items.push({
                id: "companyPortal",
                title: this.i18n("User_CompanyPortal"),
                href: "javascript: void(0);",
                target: ""
            });
        }

        //Do not display "Change Password" menu if AuthenticationType is ActiveDirectory.
        if(WebConfig.userSession.authenticationType != Enums.ModelData.AuthenticationType.ActiveDirectory){
            // Adding Change password menu item.
            this.items.push({
                title: this.i18n("Common_ChangePassword"),
                href: this.resolveUrl(WebConfig.appSettings.changePasswordUrl),
                target: "_cpwd"
            });
        }

        // Adding Preferrence menu for company portal only, all users.
        if(WebConfig.userSession.currentPortal === "company-workspace") {
            this.items.push({
                id: "userPreferences",
                title: this.i18n("User_Preferences"),
                href: "javascript: void(0);",
                target: ""
            });
        }

        // Adding About menu for all users.
        this.items.push({
            id: "about",
            title: this.i18n("Common_About"),
            href: "javascript: void(0);",
            target: ""
        });

        // Adding Logout menu for all users.
        this.items.push({
            id: "logout",
            title: this.i18n("Common_Logout"),
            href: "javascript: void(0);",
            target: ""
        });
    }
}

export default {
    viewModel: UserMenuControl,
    template: templateMarkup
};
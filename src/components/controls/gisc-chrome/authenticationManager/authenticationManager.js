import ControlBase from "../../controlbase";
import EventAggregator from "../../../../app/frameworks/core/eventAggregator";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import Singleton from "../../../../app/frameworks/core/singleton";
import Logger from "../../../../app/frameworks/core/logger";
import Utility from "../../../../app/frameworks/core/utility";
import WebRequestUserApiCore from "../../../../app/frameworks/data/apicore/webRequestUser";
import WebRequestUserApiTrackingCore from "../../../../app/frameworks/data/apitrackingcore/webRequestUser";
import webRequestUserApiTrackingReport from "../../../../app/frameworks/data/apitrackingreport/webRequestUser";
import WebRequestUserSignalR from "../../../../app/frameworks/data/signalr/webRequestUser";
import MessageboxManager from "../messagebox/messageboxManager";
import * as BladeDialog from "../../../../app/frameworks/constant/bladeDialog";
import * as EventAggregatorConstant from "../../../../app/frameworks/constant/eventAggregator";

class AuthenticationManager extends ControlBase {
    constructor(params){
        super(params);
        this._eventAggregator = new EventAggregator();
        this._authTimerRef = null;
        this._nextSync = null;
        this._isValidating = false;
        this._syncIntervalSeconds = 60;
        this._slidingWindowMinutes = Math.floor(WebConfig.appSettings.authTimeout / 2);
        this.webRequestUserApiCore = WebRequestUserApiCore.getInstance();
        this.webRequestUserApiTrackingCore = WebRequestUserApiTrackingCore.getInstance();
        this.webRequestUserApiTrackingReport = webRequestUserApiTrackingReport.getInstance();
        this.webRequestUserSignalR = WebRequestUserSignalR.getInstance();
    }

    /**
     * Get instance of authenticataion manager.
     * 
     * @returns AuthenticationManager
     */
    static getInstance() {
        return Singleton.getInstance("authenticationManager", new AuthenticationManager());
    }
    /**
     * Setup next validation time.
     */
    makeSchedule() {
        // Calculate half life for authentication check.
        this._nextSync = Utility.addMinutes(new Date(), this._slidingWindowMinutes);
        Logger.info("AuthenticationManager", "makeSchedule within", this._slidingWindowMinutes, "minutes", "to", this._nextSync);
    }
    /**
     * Start timer for auth check for authentication when user passing half life of asp.net auth cookie.
     */
    startAuthenticationSync() {
        this.makeSchedule();

        // Clear previous one.
        clearInterval(this._authTimerRef);

        // Start new timer ref every 1 seconds.
        this._authTimerRef = setInterval(this.verifyTimeout.bind(this), this._syncIntervalSeconds * 1000);
        Logger.info("AuthenticationManager", "setInterval", this._syncIntervalSeconds, "sec");
    }
    /**
     * Verify authenticataion timeout.
     */
    verifyTimeout() {
        var currentDate = new Date();
        if(currentDate > this._nextSync) {
            if(this._isValidating) {
                Logger.info("AuthenticationManager", "Skip validating because previous task is in progress.");
                return;
            }
            else {
                Logger.info("AuthenticationManager", "Start validation.");
            }

            this._isValidating = true;

            // Need to verify with service by check to api/core and api/tracking core.
            $.when( // Add all API verification here, don't forget to add checking logic in done callback.
                this.webRequestUserApiCore.isAutheticated(),
                this.webRequestUserApiTrackingCore.isAutheticated(),
                // NOTE: As discussed with N'BOM we skip authentication because
                // issue of Telerik's report viewer doesn't send authentication cookie.
                // if it supports send cookie then enable below service.
                this.webRequestUserApiTrackingReport.isAutheticated(),
                this.webRequestUserSignalR.isAutheticated()
            )
            .done(( // Add variables of authentication checked above.
                isApiCoreAuthenticated,
                isApiTrackingCoreAuthenticated,
                // NOTE: As discussed with N'BOM we skip authentication because
                // issue of Telerik's report viewer doesn't send authentication cookie.
                // if it supports send cookie then enable below service.
                isApiReportAuthenticated,
                isSignalRAuthenticated) => {

                // If all API still valid then make next schedule check.
                if( isApiCoreAuthenticated &&
                    isApiTrackingCoreAuthenticated &&
                    // NOTE: As discussed with N'BOM we skip authentication because
                    // issue of Telerik's report viewer doesn't send authentication cookie.
                    // if it supports send cookie then enable below service.
                    isApiReportAuthenticated &&
                    isSignalRAuthenticated) {
                    // Display logging for debug.
                    Logger.info("AuthenticationManager", "All APIs are authenticated.");

                    // Schedule in next time.
                    this.makeSchedule();
                }
                else {
                    clearInterval(this._authTimerRef);
                    // Display error message to user for sign out.
                    MessageboxManager.getInstance().showMessageBox("", this.i18n("M093"), BladeDialog.DIALOG_OK).done(() => {
                        this.signOut();
                    });
                }
            })
            .fail(() => {
                // Network error authentication manager will retry next time faster.
                Logger.info("AuthenticationManager", "Network connection fail try again within next time.");
            })
            .always(() => {
                Logger.info("AuthenticationManager", "Finish validation.");
                this._isValidating = false;
            });
        }
        else {
            // Skip verify within sliding windows time.
            Logger.info("AuthenticationManager", "Skip validation (within sliding window).");
        }
    }
    /**
     * Signout user from system and web service api session.
     * 
     * @static
     */
    signOut() {
        // notify sign out event for display shell.
        this._eventAggregator.publish(EventAggregatorConstant.SHOW_LOADING, true);

        // Calling service.
        $.when(
            this.webRequestUserApiCore.logout(),
            this.webRequestUserApiTrackingCore.logout(),
            this.webRequestUserApiTrackingReport.logout(),
            this.webRequestUserSignalR.logout()
        ).always(() => {
            window.location.assign(this.resolveUrl(WebConfig.appSettings.signOutUrl));
        });
    }
}

export default AuthenticationManager;

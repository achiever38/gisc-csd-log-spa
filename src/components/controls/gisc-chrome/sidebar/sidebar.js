import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./sidebar.html";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import EventAggregator from "../../../../app/frameworks/core/eventAggregator";
import * as EventAggregatorConstant from "../../../../app/frameworks/constant/eventAggregator";
import {Constants} from "../../../../app/frameworks/constant/apiConstant";
import MapManager from "../map/mapManager";

// Local constants.
const HUB_DEVELOPER = 999;
const HUB_BO_HOME = 101;
const HUB_BO_SUBSCRIPTION = 102;
const HUB_BO_COMPANIES = 103;
const HUB_BO_USERS = 104;
const HUB_BO_ASSETS = 105;
const HUB_BO_ANNOUNCEMENTS = 106;
const HUB_BO_CONFIGURATIONS = 107;
const HUB_BO_REPORTS = 108;
const HUB_BO_REPORTSEMAIL = 109;
const HUB_BO_TICKETMANAGEMENT = 110;
const HUB_CA_HOME = 201;
const HUB_CA_CONTRACTS = 202;
const HUB_CA_BU = 203;
const HUB_CA_GROUPS = 204;
const HUB_CA_USERS = 205;
const HUB_CA_ASSETS = 206;
const HUB_CA_CONFIGURATIONS = 207;
const HUB_CA_DRIVERPERF = 208;
const HUB_CA_REPORTS = 209;
const HUB_CA_DASHBOARD = 210;
const HUB_CW_GLOBALSEARCH = 301;
const HUB_CW_MAP = 302;
const HUB_CW_FLEETMONITORING = 303;
const HUB_CW_GEOFENCING = 304;
const HUB_CW_SHIPMENTS = 306;
const HUB_CW_REPORTS = 307;
const HUB_CA_VENDOR = 308;
const HUB_CW_BOMS = 309;
const HUB_CW_AUTOMAILREPORT = 310;

/**
 * Handle top navigation menu.
 * 
 * @class SidebarControl
 * @extends {ControlBase}
 */
class SidebarControl extends ControlBase {

    /**
     * Creates an instance of SidebarControl.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this._eventAggregator = new EventAggregator();
        this.navigationService = params.navigationService;
        this.isShowJourney = this.ensureObservable(params.isShowJourney, true);
        this.navigations = ko.observableArray();
        this.isExpanded = ko.observable(false);
        this.title = ko.pureComputed(this.computedTitle, this);
        this.containerCssClass = ko.pureComputed(this.computeContainerCssClass, this);
        this.activeMenu = ko.pureComputed(this.computeActiveMenu, this).extend({ notify: 'always', rateLimit: 1, method: "notifyWhenChangesStop" });


        // Register event subscribe.
        this._eventAggregator.subscribe(EventAggregatorConstant.MAP_COMMAND_COMPLETE, this.onMapComplete.bind(this));

    }
    /**
     * Calculate title of hamburger button.
     */
    computedTitle() {
        return this.isExpanded() ? 
                this.i18n("Common_HideMenu")():
                this.i18n("Common_ShowMenu")();
    }
    /**
     * Calculate CSS class name for sidebar.
     */
    computeContainerCssClass() {
        var cssClass = "app-sidebar app-trim";

        if(!this.isExpanded()) {
            cssClass += " app-sidebar-is-collapsed";
        }

        if(this.navigationService.activeItem() === null) {
            cssClass += " has-shadow";
        }

        return cssClass;
    }
    /**
     * Calculate active menu when user change blade, switch journey.
     * 
     * @returns {string}
     */
    computeActiveMenu() {
        var activeFirstBladeComponent = "";

        if(this.navigationService.activeItem() &&
            this.navigationService.activeItem().items().length) {
            activeFirstBladeComponent = (this.navigationService.activeItem().items()[0].componentName);
        }

        // Using jQuery to toggle active menu css class.
        // Currently we are using convension based of component name with prefix.

        $(".app-sidebar-item-link").each((i, item) => {
            var $menuItem = $(item);
            var menuComponent = $menuItem.attr("data-component");

            // Always reset class.
            $menuItem.removeClass("active");

            switch(activeFirstBladeComponent) {
                case "bo-company-select":
                case "cw-user-preference":
                    // Skip setting active items.
                    break;
                default:
                    if(_.isEmpty(activeFirstBladeComponent)) {
                        // No displayable journey.
                        // Try to select map when user is on company workspace portal.
                        if (menuComponent === "") {
                            $menuItem.addClass("active").trigger("focus");
                        }
                    }
                    else {
                        // Displayable journey try to map active, when menu if not empty.
                        if (!_.isEmpty(menuComponent) &&  _.startsWith(activeFirstBladeComponent, menuComponent)) {
                            $menuItem.addClass("active").trigger("focus");
                        }
                    }
                    break;
            }
        });

        return activeFirstBladeComponent;
    }
    /**
     * Get component name for each menu item.
     * 
     * @param {string} data
     */
    computeRootComponent(data) {
        return data.routeOption !== null ? data.routeOption.page: '';
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        var navOptions = [];
        switch (WebConfig.userSession.currentPortal) {
            case "company-workspace":
                navOptions = [
                {
                    id: HUB_CW_GLOBALSEARCH,
                    title: this.i18n("Menu_GlobalSearch"),
                    icon: this.i18n("svg-menu-search"),
                    permissions: [Constants.Permission.GlobalSearch],
                    routeOption: { page: "cw-global-search", permission: "GlobalSearch" }
                },
                {
                    id: HUB_CW_MAP,
                    title: this.i18n("Menu_Map"),
                    icon: this.i18n("svg-menu-map"),
                    permissions: [],
                    routeOption: null
                }, {
                    id: HUB_CW_FLEETMONITORING,
                    title: this.i18n("Menu_FleetMonitoring"),
                    icon: this.i18n("svg-menu-monitoring"),
                    permissions: [Constants.Permission.FleetMonitoring],
                    routeOption: { page: "cw-fleet-monitoring", permission: "FleetMonitoring" }
                }, {
                    id: HUB_CW_GEOFENCING,
                    title: this.i18n("Menu_GeoFencing"),
                    icon: this.i18n("svg-menu-geofencing"),
                    permissions: [Constants.Permission.GeoFencing],
                    routeOption: { page: "cw-geo-fencing", permission: "GeoFencing" }
                }, 
                {
                    id: HUB_CW_SHIPMENTS,
                    title: this.i18n("Transportation_Mngm_Title"),
                    icon: this.i18n("svg-icon-transportation"),
                    permissions: [Constants.Permission.Shipment],
                    routeOption: { page: "cw-shipment", permission: "SHM001" }
                },
                // {
                //     id: HUB_CW_REPORTS,
                //     title: this.i18n("Menu_Reports"),
                //     icon: this.i18n("svg-menu-reports"),
                //     permissions: [Constants.Permission.Reports_Company]
                // }
                 {
                    id: HUB_CW_REPORTS,
                    title: this.i18n("Menu_Reports"),
                    icon: this.i18n("svg-icon-report"),
                    permissions: [Constants.Permission.Reports],
                    routeOption: { page: "cw-report", permission: "Report" }
                },
                {
                    id: HUB_CA_DASHBOARD,
                    title: this.i18n("Dashboard_DashboardTitlePage"),
                    icon: this.i18n("svg-menu-dashboard"),
                    permissions: [Constants.Permission.Dashboard],
                    routeOption: { page: "cw-dashboard", permission: "Dashboard" }
                },
                {
                    id: HUB_CW_BOMS,
                    title: "BOMs",
                    icon: this.i18n("svg-menu-boms"),
                    permissions: [Constants.Permission.BOMs],
                    routeOption: { page: "cw-boms-menu", permission: "BOMs" }
                },
                {
                    id: HUB_CW_AUTOMAILREPORT,
                    title: this.i18n("Menu_Auto_Mail_Report"),
                    icon: this.i18n("svg-icon-auto-mail-report"),
                    permissions: [Constants.Permission.AUTOMAILREPORT],
                    routeOption: {
                        page: "cw-automail-report-manage-list", permission: "AUTOMAILREPORT" }
                }
                 
                ];
                break;
            case "company-admin":
                navOptions = [{
                    id: HUB_CA_BU,
                    title: this.i18n("Menu_BusinessUnits"),
                    icon: this.i18n("svg-menu-businessunits"),
                    permissions: [Constants.Permission.BusinessUnitManagement],
                    routeOption: { page: "ca-business-unit-menu", permission: "BusinessUnitManagement" }
                },{
                    id: HUB_CA_GROUPS,
                    title: this.i18n("Menu_Groups"),
                    icon: this.i18n("svg-menu-groups"),
                    permissions: [Constants.Permission.GroupManagement],
                    routeOption: { page: "ca-group", permission: "GroupManagement" }
                },{
                    id: HUB_CA_USERS,
                    title: this.i18n("Menu_Users"),
                    icon: this.i18n("svg-menu-users"),
                    permissions: [Constants.Permission.UserManagement_Company],
                    routeOption: { page: "ca-user", permission: "UserManagement_Company" }
                },{
                    id: HUB_CA_ASSETS,
                    title: this.i18n("Menu_Assets"),
                    icon: this.i18n("svg-menu-assets"),
                    permissions: [Constants.Permission.Asset],
                    routeOption: { page: "ca-asset", permission: "Asset" }
                },
                {
                    id: HUB_CA_DRIVERPERF,
                    title: this.i18n("Menu_DriverPerformanceRules"),
                    icon: this.i18n("svg-menu-driver-performance"),
                    permissions: [Constants.Permission.DriverPerformanceRule],
                    routeOption: { page: "ca-driver-performance-rule-menu", permission: "DriverPerformanceRule" }
                },
                //{
                //    id: HUB_CA_VENDOR,
                //    title: this.i18n("Menu_VendorManagement"),
                //    icon: this.i18n("svg-icon-vendor"),
                //    permissions: [Constants.Permission.VendorManagement],
                //    routeOption: { page: "ca-vendor-management"}
                //},
                {
                    id: HUB_CA_REPORTS,
                    title: this.i18n("Menu_Reports"),
                    icon: this.i18n("svg-icon-report"),
                     //permissions: [Constants.Permission.Reports],
                    permissions: [],
                    routeOption: { page: "ca-report", permission: "Vendor" }
                },
                {
                    id: HUB_CA_CONFIGURATIONS,
                    title: this.i18n("Menu_Configurations"),
                    icon: this.i18n("svg-menu-configurations"),
                    permissions: [Constants.Permission.Configuration_Company],
                    routeOption: { page: "ca-configuration", permission: "Configuration_Company" }
                }];
                break;
            case "back-office":
                navOptions = [
                // {
                //     id: HUB_BO_HOME,
                //     title: this.i18n("Menu_Home"),
                //     icon: this.i18n("svg-menu-home"),
                //     permissions: []
                // }, 
                {
                    id: HUB_BO_SUBSCRIPTION,
                    title: this.i18n("Menu_Subscriptions"),
                    icon: this.i18n("svg-menu-subscriptions"),
                    permissions: [Constants.Permission.SubscriptionManagement],
                    routeOption: { page: "bo-subscription-and-group", permission: "SubscriptionManagement" }
                }, {
                    id: HUB_BO_COMPANIES,
                    title: this.i18n("Menu_Companies"),
                    icon: this.i18n("svg-menu-companies"),
                    permissions: [Constants.Permission.CompanyManagement],
                    routeOption: { page: "bo-company", permission: "CompanyManagement" }
                }, {
                    id: HUB_BO_USERS,
                    title: this.i18n("Menu_Users"),
                    icon: this.i18n("svg-menu-users"),
                    permissions: [Constants.Permission.UserManagement_BackOffice],
                    routeOption: { page: "bo-user", permission: "UserManagement_BackOffice" }
                }, {
                    id: HUB_BO_ASSETS,
                    title: this.i18n("Menu_Assets"),
                    icon: this.i18n("svg-menu-assets"),
                    permissions: [Constants.Permission.AssetManagement],
                    routeOption: { page: "bo-asset", permission: "AssetManagement" }
                }, 
                {
                    id: HUB_BO_ANNOUNCEMENTS,
                    title: this.i18n("Menu_Announcements"),
                    icon: this.i18n("svg-menu-announcements"),
                    permissions: [Constants.Permission.Announcement],
                    routeOption: { page: "bo-announcement", permission: "Announcement" }
                },
                {
                    id: HUB_BO_TICKETMANAGEMENT,
                    title: this.i18n("Menu_TicketManagement"),
                    icon: this.i18n("menu-ticket"),
                    permissions: [Constants.Permission.Ticket],
                    routeOption: { page: "bo-ticket-manage", permission: "" }
                },
                {
                    id: HUB_BO_REPORTSEMAIL,
                    title: this.i18n("Report"),
                    icon: this.i18n("svg-icon-report"),
                    permissions: [Constants.Permission.Reports_BackOffice],
                    routeOption: { page: "bo-report", permission: "Reports_BackOffice" }
                },
                {
                    id: HUB_BO_CONFIGURATIONS,
                    title: this.i18n("Menu_Configurations"),
                    icon: this.i18n("svg-menu-configurations"),
                    permissions: [Constants.Permission.Configuration_BackOffice],
                    routeOption: { page: "bo-configuration", permission: "Configuration_BackOffice" }
                },
                // ,
                // {
                //     id: HUB_BO_REPORTS,
                //     title: this.i18n("Menu_Reports"),
                //     icon: this.i18n("svg-menu-reports"),
                //     permissions: [Constants.Permission.Reports_BackOffice]
                // }
                ];
                break;
        }

        // Always append developer menu in debugger mode.
        if (WebConfig.appSettings.isDebug) {
            navOptions.push({
                id: HUB_DEVELOPER,
                title: this.i18n("Developer"),
                icon: this.i18n("svg-ic-setting"),
                permissions: [],
                routeOption: { page: "developerHome", permission: null }
            });
        }

        // Filters menu from permissions.
        var validNavOptions = [];

        navOptions.forEach(function (nav) {

            var isValidItem = false;
            if(WebConfig.userSession.hasPermission(nav.permissions)) {
                isValidItem = true;
            }

            if(isValidItem) {
                validNavOptions.push(nav);
            }

        }, this);

        // Adding to observable navigations.
        this.navigations(validNavOptions);

        // Navigate to the launch screen, this is dynamic based on user permission.
        var landingNavOption = (validNavOptions.length > 0) ? validNavOptions[0] : null;
        this.navigationService.navigateToInitialState(landingNavOption);
    }

    /**
     * Handle main navigation (left menu).
     * @public
     * @param {any} menu
     */


    onMapComplete(data) {

    }


    changeHub(menu) {

        // Inform other active menu.
        this._eventAggregator.publish(EventAggregatorConstant.DROP_MENU_CLICK, this.id);

        switch(menu.id) {
            case HUB_CW_MAP:
                // Show map then hide blades.
                MapManager.getInstance().setFollowTracking(true);
                MapManager.getInstance().disableClickAndHideMenu();
                this.navigationService.hideAllJourney();
                this.isShowJourney(false);
                break;
            default:
                MapManager.getInstance().enableClickAndHideMenu();
                this.navigationService.navigate(menu.routeOption.page);
                this.isShowJourney(true);
                break;
        }
    }
    /**
     * Toggle side bar menu (hamburger bar)
     */
    onToggleMenu() {
        this.isExpanded(!this.isExpanded());
    }
}

export default {
    viewModel: SidebarControl,
    template: templateMarkup
};

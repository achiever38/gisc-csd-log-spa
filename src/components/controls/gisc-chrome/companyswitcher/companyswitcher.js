import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./companyswitcher.html";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import Utility from "../../../../app/frameworks/core/utility";
import {DropMenu} from "../shell/shellComponents";
import {Enums} from "../../../../app/frameworks/constant/apiConstant";
import FocusManager from "../../../../app/frameworks/core/focusManager";

/**
 * Handle top navigation menu.
 * 
 * @class CompanySwitcherControl
 * @extends {ControlBase}
 */
class CompanySwitcherControl extends ControlBase {
    /**
     * Creates an instance of CompanySwitcherControl.
     * 
     * @param {any} params
     */
    constructor(params) {
            super(params);
            this.visible(WebConfig.userSession.currentPortal !== "back-office");
            this.companyMenu = new DropMenu(this.i18n("Common_SwitchMode"));
            this.items = ko.observableArray();
        }
        /**
         * @lifecycle Called when View is loaded.
         * @param {boolean} isFirstLoad true if ViewModel is first load
         */
    onLoad(isFirstLoad) {
        // Adding menu items based on user permissions.
        var isBackOfficeUser = (WebConfig.userSession.userType == Enums.UserType.BackOffice);

        switch (WebConfig.userSession.currentPortal) {
            case "company-admin":
                // Adding to company workspace switcher.
                var companyWorkspacePortalUrl = isBackOfficeUser ? WebConfig.appSettings.companyWorkspaceUrl :
                    WebConfig.appSettings.companyWorkspaceUrlForOwner;

                companyWorkspacePortalUrl = Utility.stringFormat(companyWorkspacePortalUrl,
                    WebConfig.userSession.currentCompanyId);

                this.items.push({
                    title: this.i18n("User_WorkspaceMode"),
                    href: this.resolveUrl(companyWorkspacePortalUrl),
                    target: "GISWorkspaceMode"
                });

                break;
            case "company-workspace":
                // Adding to company admin switcher if user has company admin right.
                if (WebConfig.userSession.portalModes.indexOf(Enums.ModelData.PortalMode.Admin) !== -1) {
                    var companyAdminPortalUrl = isBackOfficeUser ?
                        WebConfig.appSettings.companyAdminUrl :
                        WebConfig.appSettings.companyAdminUrlForOwner;

                    companyAdminPortalUrl = Utility.stringFormat(
                        companyAdminPortalUrl,
                        WebConfig.userSession.currentCompanyId);

                    this.items.push({
                        title: this.i18n("User_AdminMode"),
                        href: this.resolveUrl(companyAdminPortalUrl),
                        target: "GISAdminMode"
                    });
                } else {
                    // Hide menu when no access right to administration portal.
                    this.visible(false);
                }
                break;
        }
    }

    onDomReady() {

        if (!this.visible()) return;

        var $el = $("#" + this.companyMenu.id);

        FocusManager.getInstance().registerFocusElement(this.companyMenu.id, $el.get(0), () => {
            if (!this.companyMenu.isActive()) return;
            this.companyMenu.isActive(false);

        });


    }
}

export default {
    viewModel: CompanySwitcherControl,
    template: templateMarkup
};
import ko from "knockout";
import {DropMenu} from "../shell/shellComponents";

class NotificationMenu extends DropMenu {
    constructor(title, type, icon, observableItem, unreadCount) {
        super(title);
        this.type = type;
        this.icon = icon;
        this.items = observableItem;
        this.unReadCount = unreadCount;
        this.cssClass = ko.pureComputed(()=> {
            return this.type === 1 ? "app-notification-announcement" : "";
        });
    }
}

export default NotificationMenu;
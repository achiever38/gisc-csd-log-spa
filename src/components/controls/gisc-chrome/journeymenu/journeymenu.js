import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./journeymenu.html";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import {DropMenu} from "../shell/shellComponents";
import FocusManager from "../../../../app/frameworks/core/focusManager";

/**
 * Handle top navigation journey menu.
 * 
 * @class JourneyMenuControl
 * @extends {ControlBase}
 */
class JourneyMenuControl extends ControlBase {

    /**
     * Creates an instance of JourneyMenuControl.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.journeyMenu = new DropMenu(this.i18n("Common_SwitchTask"));
        this.navigationService = params.navigationService;
        this.isShowJourney = params.isShowJourney;
        this.recentJourney = ko.pureComputed(() => {
            return this.navigationService.nonActiveItems().splice(0).reverse();
        });

        this._$journeyMenuContent = null;
        this.journeyMenu.onClicked = (isActive) => {
            // console.log("JM onClicked... isActive =", isActive);
            if (isActive && this._$journeyMenuContent) {
                this._$journeyMenuContent.focus();
            }
        };
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {}

    /**
     * Change displayed journey to the target one.
     * @public
     * @param {Journey} journey - target Journey.
     */
    changeJourney(journey) {
        this.navigationService.changeJourney(journey);
        this.isShowJourney(true);
        this.journeyMenu.isActive(false);
    }

    /**
     * Remove target journey.
     * @public
     * @param {Journey} journey - target Journey.
     */
    removeJourney(journey) {
        this.navigationService.tryDeactivate(journey, true);
    }

    onDomReady() {

        var $el = $("#" + this.journeyMenu.id);

        FocusManager.getInstance().registerFocusElement(this.journeyMenu.id, $el.get(0), () => {
            if(!this.journeyMenu.isActive()) return;
            this.journeyMenu.isActive(false);
        });
    }
}

export default {
    viewModel: JourneyMenuControl,
    template: templateMarkup
};
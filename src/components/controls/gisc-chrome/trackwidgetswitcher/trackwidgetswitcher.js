import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./trackwidgetswitcher.html";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import Utility from "../../../../app/frameworks/core/utility";
import {DropMenu} from "../shell/shellComponents";
import {Enums} from "../../../../app/frameworks/constant/apiConstant";
import FocusManager from "../../../../app/frameworks/core/focusManager";
import {Constants} from "../../../../app/frameworks/constant/apiConstant";

/**
 * Handle top navigation menu.
 * 
 * @class TrackWidgetSwitcherControl
 * @extends {ControlBase}
 */
class TrackWidgetSwitcherControl extends ControlBase {
    /**
     * Creates an instance of TrackWidgetSwitcherControl.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        // Hide this item if user doesnt' have permission or in admin portal.
        var trackAvailable = WebConfig.userSession.hasPermission(Constants.Permission.TrackMonitoring)
            && WebConfig.userSession.currentPortal == "company-workspace";
        this.visible(trackAvailable);
        //this.visible(false);

        this.companyMenu = new DropMenu(this.i18n('FleetMonitoring_TrackMonitoring')());
        this.isActive = ko.observable(false);
    }

    /**
     * Open widget when user clicks on menu.
     */
    onClick()
    {
        // Open widget with target for unique open once.
        this.widget("cw-fleet-monitoring-widget", null, {
            id: 'trackingWidgetMonitoring',
            title: this.i18n('FleetMonitoring_TrackMonitoring')(),
            modal: false,
            resizable: true,
            target: "cw-fleet-monitoring-widget",
            customActions: ["all", "delete"],
            width: "95%",
            height: "230"
        });
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {

    }

    onDomReady() {
        // if (!this.visible()) return;
        // var $el = $("#" + this.companyMenu.id);
        // FocusManager.getInstance().registerFocusElement(this.companyMenu.id, $el.get(0), () => {
        //     if (!this.companyMenu.isActive()) return;
        //     this.companyMenu.isActive(false);
        // });
    }
}

export default {
    viewModel: TrackWidgetSwitcherControl,
    template: templateMarkup
};
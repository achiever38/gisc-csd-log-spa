import ko from "knockout";
import ComponentBase from "../componentBase";
import GC from "../../app/frameworks/core/gc";
import WidgetNavigation from "../controls/gisc-chrome/shell/widgetNavigation";

/**
 * Base class for control development.
 * 
 * @class ControlBase
 * @extends {ComponentBase}
 */
class ControlBase extends ComponentBase {
    /**
     * Creates an instance of ControlBase.
     * @public
     * @param {any} params
     */
    constructor(params) {
        // Ensure params
        if (!params) {
            params = {};
        }
        super(params);
        this.enable = this.ensureObservable(params.enable, true);
        this.visible = this.ensureObservable(params.visible, true);
        this.widgetService = WidgetNavigation.getInstance();
    }

    /**
     * Parse params binding for non-ko-components.
     *
     * @static
     * @param {any} element
     * @param {any} context
     * @returns
     */
    getBindingFromElement(element, context) {
        var result = {};
        var paramsAttribute = element.getAttribute("params");
        var nativeBindingProviderInstance = new ko.bindingProvider();
        if (paramsAttribute) {
            var bindingParams = nativeBindingProviderInstance["parseBindingsString"](
                paramsAttribute, {
                    $data: context
                }, element, {
                    "valueAccessors": true,
                    "bindingParams": true
                }
            );

            // This function is copied from ko.utils.objectMap.
            function objectMap(source, mapping) {
                if (!source)
                    return source;
                var target = {};
                for (var prop in source) {
                    if (source.hasOwnProperty(prop)) {
                        target[prop] = mapping(source[prop], prop, source);
                    }
                }
                return target;
            };

            var rawParamComputedValues = objectMap(bindingParams, function(paramValue, paramName) {
                return ko.computed(paramValue, null, {
                    disposeWhenNodeIsRemoved: element
                });
            });

            result = objectMap(rawParamComputedValues, function(paramValueComputed, paramName) {
                var paramValue = paramValueComputed.peek();
                // Does the evaluation of the parameter value unwrap any observables?
                if (!paramValueComputed.isActive()) {
                    // No it doesn"t, so there"s no need for any computed wrapper. Just pass through the supplied value directly.
                    // Example: "someVal: firstName, age: 123" (whether or not firstName is an observable/computed)
                    return paramValue;
                } else {
                    // Yes it does. Supply a computed property that unwraps both the outer (binding expression)
                    // level of observability, and any inner (resulting model value) level of observability.
                    // This means the component doesn"t have to worry about multiple unwrapping. If the value is a
                    // writable observable, the computed will also be writable and pass the value on to the observable.
                    return ko.computed({
                        /**
                         * 
                         * 
                         * @returns
                         */
                        "read": function() {
                            return ko.utils.unwrapObservable(paramValueComputed());
                        },
                        "write": ko.isWriteableObservable(paramValue) && function(value) {
                            paramValueComputed()(value);
                        },
                        disposeWhenNodeIsRemoved: elem
                    });
                }
            });
        }

        return result;
    }

    /**
     * Called when View is detached.
     * @public
     */
    onViewDetach() {
        super.onViewDetach();

        // Using GC to cleanup if exists.
        GC.dispose(this);
    }

    /**
     * Navigate to target moduleName on a new Window
     * @public
     * @param {any} moduleName
     * @param {any} [options=null]
     */
    widget(componentName, options = null, widgetOptions = null){
        this.widgetService.widget(componentName, options, widgetOptions);
    }
}

export default ControlBase;
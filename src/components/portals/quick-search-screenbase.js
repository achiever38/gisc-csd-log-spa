import ko from "knockout";
import ScreenBase from "./screenbase";

/**
 * Base class for screen which requires quick search feature
 * 
 * @class QuickSearchScreenBase
 * @extends {ScreenBase}
 */
class QuickSearchScreenBase extends ScreenBase {

    /**
     * Creates an instance of QuickSearchScreenBase.
     * @public
     * @param {any} params
     */
    constructor(params) {
        super(params);

        // Quick Search
        this.bladeEnableQuickSearch = false;
        this.quickSearchKeyword = ko.observable("");
        this.quickSearchPlaceHolder = this.i18n("PHSearchByLicenseOrDriver")();
    }

     /**
     * @lifecycle Handle when search on text box is clicked or enter.
     * @param {any} data
     * @param {any} event
     */
    onQuickSearchClick(data, event) {
        throw new Error("require override method onQuickSearchClick");
    }

    /**
     * @lifecycle web request global search autocomplete.
     * @param {any} request
     * @param {promise} response
     */
    onDataSourceQuickSearch(request, response) {
        throw new Error("require override method onQuickSearchClick");
    }
}

export default QuickSearchScreenBase;
import templateMarkup from "text!./labelPage.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";

/**
 * Sample usage of gisc-ui-label controls
 * 
 * @class LabelScreen
 * @extends {ScreenBase}
 */
class LabelScreen extends ScreenBase {
    /**
     * Creates an instance of LabelScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Medium;
        this.bladeTitle("Label (gisc-ui-label)");
        this.bladeDescription("src/components/portals/developer/label");
    }
}

export default {
    viewModel: ScreenBase.createFactory(LabelScreen),
    template: templateMarkup
};
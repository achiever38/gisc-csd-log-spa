import ko from "knockout";
import templateMarkup from "text!./home.html";
import ScreenBase from "../../screenbase";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";

class DeveloperHomeScreen extends ScreenBase {
    constructor(params){
        super(params);
        this.bladeTitle("Developer Homepage");

        this.bladeMargin = BladeMargin.Narrow;
        this.selectedIndex = ko.observable();
    }
    onLoad(isFirstLoad) {
    }
    goto (extras){
        this.navigate(extras.id);
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedIndex(null);
    }
}

export default {
    viewModel: ScreenBase.createFactory(DeveloperHomeScreen),
    template: templateMarkup
};
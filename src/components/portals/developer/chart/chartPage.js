import templateMarkup from "text!./chartPage.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import ko from "knockout";

/**
 * 
 * Constructor for an object
 * @class Jobs
 */
class Jobs {
    /**
     * Creates an instance of Jobs.
     * 
     * @param {any} status
     * @param {any} total
     * @param {any} color
     */
    constructor(id, status, total, color) {
        this.id = id;
        this.status = status;
        this.total = total;
        this.color = color;
    }
}

/**
 * 
 * 
 * @class ChartScreen
 * @extends {ScreenBase}
 */
class ChartScreen extends ScreenBase {
    /**
     * Creates an instance of ChartScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
            super(params);

            this.bladeSize = BladeSize.Medium;
            this.bladeTitle("Chart (gisc-ui-chart)");
            this.bladeDescription("src/components/portals/developer/chartPage");

            this.items = ko.observableArray([
                new Jobs(1,"Unassign", 9, "#b93e3d"),
                new Jobs(2,"Waiting", 15, "#b7b83f"),
                new Jobs(3, "Loading", 2, "#b9783f"),
                new Jobs(4, "On The Way", 20, "#84b761"),
                new Jobs(5, "Go To Terminal", 12, "#67b7dc")
            ]);

            window.test = this;
        }
        /**
         * 
         * Get index value from control when click on chart.
         * @param {any} index
         */
    selectedJob(index) {
        var newValue = {
            status: this.items()[index].status,
            total: this.items()[index].total
        }
        //Open new blade and display selected item detail.
        this.navigate("chartEndPoint", {
            previousScreenName: this.bladeTitle(),
            job: newValue
        });
    }
    onLoad(){}
    onUnload(){}
}

export default {
    viewModel: ScreenBase.createFactory(ChartScreen),
    template: templateMarkup
};
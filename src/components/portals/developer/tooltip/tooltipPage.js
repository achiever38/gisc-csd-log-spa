import templateMarkup from "text!./tooltipPage.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import ko from "knockout";

/**
 * 
 * 
 * @class Help
 * @extends {ScreenBase}
 */
class TooltipScreen extends ScreenBase {


    /**
     * Creates an instance of Help.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Medium;
        this.bladeTitle("Tooltip (gisc-ui-tooltip)");
        this.bladeDescription("src/components/portals/developer/tooltip");

        //Text to show in tooltip
        // this.text = ko.observable("Tooltip control is a pop-up box that appears when the user click the mouse over an element.Using similar to tooltips, supports below parameters.");
    }
}

export default {
    viewModel: ScreenBase.createFactory(TooltipScreen),
    template: templateMarkup
};
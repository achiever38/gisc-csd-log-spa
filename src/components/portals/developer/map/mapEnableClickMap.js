﻿import ko from "knockout";
import templateMarkup from "text!./mapEnableClickMap.html";
import ScreenBase from "../../screenbase";
import MapManager from "../../../controls/gisc-chrome/map/mapManager";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import * as EventAggregatorConstant from "../../../../app/frameworks/constant/eventAggregator";

class MapEnableClickMap extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Medium;
        this.bladeTitle("Enable Click Map ('enable-click-map')");
        this.bladeDescription("src/components/portals/developer/map/mapEnableClickMap");

        this.resultText = ko.observable();

        // Register event subscribe.
        this._eventAggregator.subscribe(EventAggregatorConstant.MAP_COMMAND_COMPLETE, this.onMapComplete.bind(this));
    }

    onMapComplete(data) {
        console.log(data);

        if(data.command == "enable-click-map" || data.command == "cancel-click-map" )
            this.resultText(JSON.stringify(data))
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {}
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}

    invokeCommand() {
        MapManager.getInstance().enableClickMap(this.id);
    }

    invokeCancelCommand(){
        MapManager.getInstance().cancelClickMap(this.id);
    }
}

export default {
    viewModel: ScreenBase.createFactory(MapEnableClickMap),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./mapDrawMultipleVehicle.html";
import ScreenBase from "../../screenbase";
import MapManager from "../../../controls/gisc-chrome/map/mapManager";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import * as EventAggregatorConstant from "../../../../app/frameworks/constant/eventAggregator";

class MapDrawMultipleVehicle extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Medium;
        this.bladeTitle("Draw Multiple Vehicles ('draw-multiple-vehicles')");
        this.bladeDescription("src/components/portals/developer/map/mapDrawMultipleVehicle");

        // Register event subscribe.
        this._eventAggregator.subscribe(EventAggregatorConstant.MAP_COMMAND_COMPLETE, this.onMapComplete.bind(this));
    }

    onMapComplete(data) {
        console.log(data);
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {}
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}

    invokeCommand() {
        var mockVehicles = [{
            location: {
                lat: 13.733114,
                lon: 100.530602
            },
            symbol: {
                url: 'https://map.nostramap.com/mobileweb/images/pin_markonmap.png',
                width: '35',
                height: '50',
                offset: {
                    x: 0,
                    y: 23.5
                },
                rotation: 0
            },
            attributes: null
        }, {
            location: {
                lat: 13.723114,
                lon: 100.530602
            },
            symbol: {
                url: 'https://map.nostramap.com/mobileweb/images/pin_markonmap.png',
                width: '35',
                height: '50',
                offset: {
                    x: 0,
                    y: 23.5
                },
                rotation: 0
            },
            attributes: null
        }];

        MapManager.getInstance().drawMultipleVehicles(this.id, mockVehicles);
    }
}

export default {
viewModel: ScreenBase.createFactory(MapDrawMultipleVehicle),
    template: templateMarkup
};
import ko from "knockout";
import templateMarkup from "text!./colorPickerPage.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
/**
 * Sample usage of gisc-ui-colorpicker controls
 * 
 * @class ColorPickerPage
 * @extends {ScreenBase}
 */
class ColorPickerScreen extends ScreenBase {
    /**
     * Creates an instance of ColorPickerPage.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.bladeTitle("ColorPicker (gisc-ui-colorpicker)");
        this.bladeDescription("src/components/portals/developer/colorpicker");
        this.bladeSize = BladeSize.Medium;
        this.myValue = ko.observable("#e83f3f");
    }
  
}

export default {
    viewModel: ScreenBase.createFactory(ColorPickerScreen),
    template: templateMarkup
};
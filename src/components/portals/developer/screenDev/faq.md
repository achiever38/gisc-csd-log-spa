## How to send data to other screens?

Normally data is passed from parent to child blade, put any object in the second parameter of
`navigate` method in View Model. For example:

``` js
// In Sender View Model
this.navigate("target-component", {id: 1, name: "test"});
```

This will pass object `{id: 1, name: "test"}` as `params` inside the target-component constructor.
You then able to obtain this information and set to self

``` js
// In Reciever View Model's constructor
this.objectId = params.id;
this.objectName = params.name;
```

You can use helper method `ensureNonObservable` and `ensureObservable` to make sure the value is in correct type

``` js
// In Reciever View Model's constructor
this.objectId = this.ensureNonObservable(params.id, -1);
this.objectName = this.ensureObservable(params.name);
```

## How to broadcast data to all paricipant screens?

There will be scenario when you need to send data to serveral screens, for example if blade structure is like this:

List > View > Update

When update action finish, it should update both View and List screens simultaneously. In order to achieve this there are 2 steps:
set up observing subject in reciever and broadcast subject with changed in sender.

### Setup observing subject

From the given scenario, List and View can setup observing with the following code

``` js
// In List and View screens VM
this.subscribeMessage("subject-changed", (whatChanged) => {
    // update its state with whatChanged.
});
```

Whenever Update broadcast message with `"subject-changed"` the reciever under the same journey will get it.

### Broadcast message

From the given scenario, Update can broadcast message when the operation is completed with 
the following code

``` js
// In Update screen VM
this.publishMessage("subject-changed", whatChanged);
```

Where `whatChanged` is an object to broadcast to whoever listening to `"subject-changed"` subject.
In this case it will be List and View screens VM.

This mechanism is a simple pub/sub pattern, however please note that only participants on the same journey
can communicate with each other. This is due to performance optimization.

## How to detect change on Observable?

Use Knockout `subscribe` on observable. For example

``` js
// In View Model
this.text = ko.observable();
this.textSubscribeRef = this.text.subscribe((newValue) => {
    // newValue is the change.
});
```

Please note that to prevent memory leak, we need to create property `textSubscribeRef` to hold reference 
This property will be deal with using Garbage Collector (the custom one that we made, not GC on browser), when
it is not in used any more.

## How to display dialogbox?

Before using dialogbox make sure to import constant under app/frameworks/constant/bladeDialog.

Specify dialogbox type by using constant below: 

* `DIALOG_YESNO` - with Yes/No buttons.
* `DIALOG_OKCANCEL` - with OK/Cancel buttons.
* `DIALOG_OK` - with only OK button.

Use `displayDialog` method in ScreenBase to display the dialog, for example:

``` js
// In View Model
import * as BladeDialog from "../../../../app/frameworks/constant/bladeDialog";

this.displayDialog("This is title", 
                    "This is content.", 
                    BladeDialog.DIALOG_YESNO, 
                    this.dialogAction.bind(this));
```

This will display the dialog that user can click `Yes` or `No` to verify something.
When the button was clicked, it will return a string value of button to `dialogAction` method 
that you can detect which button was clicked by compare the value to constant below:

* BUTTON_YES
* BUTTON_NO
* BUTTON_OK
* BUTTON_CANCEL

__Example:__
``` js
// In View Model
dialogAction(button) {
    switch (button) {
        case BladeDialog.BUTTON_YES:
            //To do some action.
            break;
        case BladeDialog.BUTTON_NO:
            //To do some action.
            break;
    }
}
```

The dialog will automatically close after your action is done.

> `ScreenBase.onActionClick` already handle cancel action, we stronly suggest to call base method when you
overide this one, then let the base method handle cancel action.

## How to display status bar?

Use status bar to show feedback messages for user actions such as Save data to Core API.

### Display fail status

To display alert messages when action fail by using `displayError` method in View Model. For example:

``` js
// In View Model
this.displayError("Unable to save data...");
```

### Display success status

To display alert messages when action success by using `displaySuccess` in View Model. For example:

``` js
// In View Model
this.displayError("Some context");
```

### Clear statusbar

Basically statusbar has close button which can close itself, 
but in somecase we might want to automatically close it by use `clearStatusBar` method in View Model. 

``` js
// In View Model
this.clearStatusBar();
```

## How to show loading indicator?

When perform long operation and/or calling API, screen should be blocked to prevent user from accidently
submit form multiple time. 

To block the screen (including action buttons, command buttons) call `this.isBusy(true)` in View Model.

``` js
// In View Model
this.isBusy(true);
```

Note that `isBusy` is an observable in ScreenBase. In scenario where API call may response error on the 
same blade as caller, make sure to reset `isBusy` to false __before__ display error on status bar. 
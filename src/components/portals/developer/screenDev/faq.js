import ko from "knockout";
import ScreenBase from "../../screenbase";
import templateMarkup from "text!./faq.html";
import contentMarkdown from "text!./faq.md";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";

class ScreenDevReadme extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeSize = BladeSize.Medium;
        this.bladeTitle("Frequently asked questions");
        this.bladeDescription("src/components/portals/developer/screenDev/faq");

        this.markdownText = ko.observable(contentMarkdown);
    }

    onLoad(isFirstLoad) {}
    onUnload() {}
}

export default {
    viewModel: ScreenBase.createFactory(ScreenDevReadme),
    template: templateMarkup
};
import ko from "knockout";
import ScreenBase from "../../screenbase";
import templateMarkup from "text!./readme.html";
import contentMarkdown from "text!./readme.md";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";

class ScreenDevReadme extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeSize = BladeSize.Medium;
        this.bladeTitle("Screen Development README");
        this.bladeDescription("src/components/portals/developer/screenDev/readme");

        this.markdownText = ko.observable(contentMarkdown);
    }

    onLoad(isFirstLoad) {}
    onUnload() {}
}

export default {
    viewModel: ScreenBase.createFactory(ScreenDevReadme),
    template: templateMarkup
};
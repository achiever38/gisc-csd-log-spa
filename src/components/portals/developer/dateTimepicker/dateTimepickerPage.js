import templateMarkup from "text!./dateTimePickerPage.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import Utility from '../../../../app/frameworks/core/utility';
import ko from "knockout";
import "jquery-ui";
import "jqueryui-timepicker-addon";
import moment from "moment";


class DateTimePickerScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Medium;
        this.bladeTitle("DateTimePicker (gisc-ui-datetime)");
        this.bladeDescription("src/components/portals/developer/dateTimePickerPage");


        window.pick = this;

        this.date = ko.observable("2016-08-01T08:47:33.383");
        // this.time = ko.observable("2016-08-01T18:47:33.383");
        this.time = ko.observable(new Date());
        // this.dateTime = ko.observable("2016-08-01T18:47:33.383");
        this.dateTime = ko.observable("2016-08-01T18:47:33.383");

        this.dtSetFormat = ko.observable(new Date());
        this.dateFormat = ko.observable("d/M/yyyy");
        this.timeFormat = ko.observable("h:mm:ss tt");

        //Set start and end date
        this.start = ko.observable("2016-10-20T00:00:00");
        this.end = ko.observable("2016-11-19T00:00:00");

        this.min = ko.observable();
        this.max = ko.observable();

       

        this.startDate = ko.observable("2016-10-20T00:00:00");
        this.endDate = ko.observable("2016-11-19T00:00:00");
        this.exceedDate  = ko.pureComputed(()=>{
            return Utility.addDays(this.startDate(),30);
        });
        

        this.formatSet = "yyyy-MM-dd";

        // this.dateTime2 = ko.observable();

        // this.startTime = ko.observable("2000-01-01T08:30:00");
        // this.endTime = ko.observable("2000-01-01T08:50:00");
        this.startTime = ko.observable();
        this.endTime = ko.observable();

        
    }

    onClear(){
        this.dateTime(null);
    }

    // setupExtend() {
    //     this.time.extend({
    //         trackChange: true
    //     });
    // }
    
    onLoad(isFirstLoad){
        if (!isFirstLoad) {
            return;
        }
    }
    onUnload() {}
}

export default {
    viewModel: ScreenBase.createFactory(DateTimePickerScreen),
    template: templateMarkup
};
import templateMarkup from "text!./textAreaPage.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import ko from "knockout";

/**
 * 
 * 
 * @class TextAreaScreen
 * @extends {ScreenBase}
 */
class TextAreaScreen extends ScreenBase {
    /**
     * Creates an instance of TextAreaScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Medium;
        this.bladeTitle("TextArea (gisc-ui-textarea)");
        this.bladeDescription("src/components/portals/developer/textArea");

        //Value for view
        this.example = ko.observable("Example");
        this.ex1 = ko.observable("Example1");
        this.ex2 = ko.observable("Example2");

        this.isEnable = ko.observable(false);
        this.isVisible = ko.observable(false);
    }

    onLoad(isFirstLoad) {}

    toggleEnable() {
        this.isEnable(!this.isEnable());
        console.log("this.isEnable = " + this.isEnable());
    }

    toggleVisible() {
        this.isVisible(!this.isVisible());
        console.log("this.isVisible = " + this.isVisible());
    }
}

export default {
    viewModel: ScreenBase.createFactory(TextAreaScreen),
    template: templateMarkup
};
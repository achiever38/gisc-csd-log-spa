import ko from "knockout";
import templateMarkup from "text!./loadMore.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import Utility from "../../../../app/frameworks/core/utility";

class DataTableLoadMore extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle("Load More");
        this.bladeDescription("src/components/portals/developer/datatable/loadMore");
        this.bladeSize = BladeSize.Medium;

        this.selectedItem = ko.observable();
        this.searchText = ko.observable();
        this.order = ko.observable([[ 0, "asc" ]]);

        // For load more
        // this.pageIndex = ko.observable(0);
        // this.pageSize = ko.observable(20);


        this.items = ko.observableArray(Utility.extractParam(params.ds, this.__generateRandomDatasource(100)));

        this._loadMoreHandler = (pageIndex) => {
            console.log("Screen: on load more = " + pageIndex);
        };
    }

    __generateRandomDatasource(size) {
        var positionPool = [ 
            "Professor", "Teacher", "Clergy", 
            "Philosopher", "Audiologist", "Chiropractor",
            "Dentist", "Dietitian", "Nurse",
            "Physician", "Podiatrist", "Psychologist",
            "Engineer", "Linguistic", "Lawyer" 
        ];
        var result = [];

        for(let i = 0; i < size; i++) {
            let item = {
                id: i+1,
                name: Utility.randomString(10),
                position: positionPool[_.random(0, 14)],
                age: _.random(18, 70)
            };
            result.push(item);
        }
        return result;
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {}

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(DataTableLoadMore),
    template: templateMarkup
};
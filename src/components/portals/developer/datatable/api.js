import ko from "knockout";
import ScreenBase from "../../screenbase";
import templateMarkup from "text!./api.html";
import contentMarkdown from "text!./api.md";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";

class DataTableAPI extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeSize = BladeSize.Large;
        this.bladeTitle("DataTable API");
        this.bladeDescription("src/components/portals/developer/datatable/api");

        this.markdownText = ko.observable(contentMarkdown);
    }
    onLoad(isFirstLoad) {}
}

export default {
    viewModel: ScreenBase.createFactory(DataTableAPI),
    template: templateMarkup
};
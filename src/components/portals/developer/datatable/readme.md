> DataTable controls is using for displaying tabular data, most of the entity list screen use this controls.

## Setup a Table

To use a `gisc-ui-datatable` declare it on view (html) like this.

``` xml
<gisc-ui-datatable params="
    datasource: items,
    columns: [
        { type: 'label', title: 'Name, data: 'name', searchable: true },
        { type: 'label', title: 'Position', data: 'position' },
        { type: 'label', title: 'Age', data: 'age' }
    ],
    order: order
    " />
```

There are 2 required parameters `datasource` and `columns`. 
* `datasource` is an Observable Array holding json object.
* `columns` is an array of column object, there are several types of column, the basic one is `label`
* `order` is an Observable object in form of [order option](https://datatables.net/reference/option/order)

In View Model, the datasource is look like this

``` js
this.items = ko.observableArray([
    { id: 1, name: "Victor", position: "Software Engineer", age: 22 },
    { id: 2, name: "Tiger", position: "System Architect", age: 33 },
    { id: 3, name: "Satou", position: "Accountant", age: 31 }
]);

this.order = ko.observable([[ 0, "asc" ]]);
```

### Data Projection

From an example above, each column access property of json object in datasource with `data` attribute.
This data projection allow losely coupling between datasource and the column displaying.

You can have multiple columns project on the same property of json object, then display it differently
using different column type.

### Column object

The following are basic parameters for column object:
* __Required__
  - `type` - Type of the column, see list of support column types below.
  - `data` - Property of json object to be projected to get data for displaying cell in that column type.
* __Optional__
  - `title` - Title of the column, render as text in the header's cell. This parameter support i18n in form of token, for example:
title:'i18n:User_Users'.
  - `searchable` - Whether this column is searchable when filter is active. Use in conjunction with `showFilter` parameter on DataTable.
  Default value is `false`.
  - `visible` - Whether this column is visible. Default value is `true`.
  - `orderable` - Whether this column is sortable. Default value is `true`.
  - `orderData` - When you want to sort column from different data, value can be integer as target column index, or array as target column indexes for multiple columns.
  - `width` - Define width of the column, this parameter can take any CSS value (3em, 20px, 20%, etc...)
  - `className` - CSS class name, apply to both th and td in target column.

## Column Types

DataTable supports the following column types:
* `boolean`
* `checkbox`
* `colorhex`
* `custom`
* ~~`hierarchy`~~
* `label`
* `textbox`

### Boolean Column

Use for rendering human readable true/false with boolean data projection.

__Example:__
``` xml
<!-- View -->
<gisc-ui-datatable params="
    datasource: items,
    columns: [
        { type: 'label', title: 'Name, data: 'name' },
        { type: 'boolean', title: 'Is Enabled', data: 'isEnabled' }
    ],
    order: order
    " />
```
``` js
// View Model
this.items = ko.observableArray([
    { id: 1, name: "Victor", age: 22, isEnabled: true },
    { id: 2, name: "Tiger", age: 33, isEnabled: true },
    { id: 3, name: "Satou", age: 31, isEnabled: false }
]);
this.order = ko.observable([[ 0, "asc" ]]);
```

### Checkbox Column

Use for rendering checkbox control with boolean data projection. 
Mostly use to edit data on the table inline. 

It can be used in conjunction with `onDataSourceChanged` event
on DataTable to take action when datasource change immediately.

__Example:__
``` xml
<!-- View -->
<gisc-ui-datatable params="
    datasource: items,
    columns: [
        { type: 'label', title: 'Name, data: 'name' },
        { type: 'checkbox', title: 'Is Enabled', data: 'isEnabled' }
    ],
    order: order
    " />
```
``` js
// View Model
this.items = ko.observableArray([
    { id: 1, name: "Victor", age: 22, isEnabled: true },
    { id: 2, name: "Tiger", age: 33, isEnabled: true },
    { id: 3, name: "Satou", age: 31, isEnabled: false }
]);
this.order = ko.observable([[ 0, "asc" ]]);
```

### ColorHex Column

Use for rendering color with color-hex code data projection.

__Example:__
``` xml
<!-- View -->
<gisc-ui-datatable params="
    datasource: items,
    columns: [
        { type: 'label', title: 'Name, data: 'name' },
        { type: 'colorhex', title: 'Color', data: 'hex' }
    ],
    order: order
    " />
```
``` js
// View Model
this.items = ko.observableArray([
    { id: 1, name: "Victor", age: 22, hex: "#ff0000" },
    { id: 2, name: "Tiger", age: 33, hex: "#00ff00" },
    { id: 3, name: "Satou", age: 31, hex: "#0000ff" }
]);
this.order = ko.observable([[ 0, "asc" ]]);
```

### Custom Column

Use for rendering anything (by html) with any data projection.

The custom column is difference than others since you have to tell 
the controls how to render by yourself using `render` attribute.

__Example:__
``` xml
<!-- View -->
<gisc-ui-datatable params="
    datasource: items,
    columns: [
        { type: 'label', title: 'Name, data: 'name' },
        { type: 'custom', title: 'Is Enabled', data: 'isEnabled', render: renderCustomColumn }
    ],
    order: order
    " />
```
``` js
// View Model
this.items = ko.observableArray([
    { id: 1, name: "Victor", age: 22, isEnabled: true },
    { id: 2, name: "Tiger", age: 33, isEnabled: true },
    { id: 3, name: "Satou", age: 31, isEnabled: false }
]);
this.order = ko.observable([[ 0, "asc" ]]);

// You can access the following in this context:
// - cell data with 'data' param, in this case it is either true or false.
// - type is jqueryDataTable param, ignore this.
// - row is TR DOM for this row, you can use jQuery to manipulate it here.
this.renderCustomColumn = (data, type, row) => {
    if(data) {
        return 'Yes';
    }
    return 'No';
};
```

### Label Column

Use for rendering text with string data projection. 
See an example from other column usage.

Also support cell's title with `cellTitle` property.

### Textbox Column

Use for rendering textbox control with string data projection. 
Mostly use to edit data on the table inline. 

It can be used in conjunction with `onDataSourceChanged` event
on DataTable to take action when datasource change immediately.

__Example:__
``` xml
<!-- View -->
<gisc-ui-datatable params="
    datasource: items,
    columns: [
        { type: 'label', title: 'Name, data: 'name', cellTitle: 'name' },
        { type: 'textbox', title: 'Age', data: 'age' }
    ],
    order: order
    " />
```
``` js
// View Model
this.items = ko.observableArray([
    { id: 1, name: "Victor", age: 22, isEnabled: true },
    { id: 2, name: "Tiger", age: 33, isEnabled: true },
    { id: 3, name: "Satou", age: 31, isEnabled: false }
]);
this.order = ko.observable([[ 0, "asc" ]]);
```

## Things you need to know

* `rowId` is not required when table is in readonly state, means there is no 
manipulation on datasource needed. However most of the time DataTable require change
on datasource, such as add/remove.
Whenever those requirement comes up, make sure to set rowId.
* The controls will try to set first `orderable` column as default sort (ascending).
If all columns are not orderable then sort for table will be disabled entirely.
* `selectedRow` and `selectedRows` are mutual exclusive, means you have to choose one.
  - When it is list of entity, most likely use `selectedRow`
  - When it is a table to choose multiple item and goes back to previous screen, most likely use `selectedRows`
* `onSelectingRow` event is to provide an opportunity to cancel row selection. This is most likely use on
list of entity screen, when selecting an item may not possible because previous item is in editing state.

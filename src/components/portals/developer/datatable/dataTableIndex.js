import ko from "knockout";
import templateMarkup from "text!./dataTableIndex.html";
import ScreenBase from "../../screenbase";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";

class DataTableIndexScreen extends ScreenBase {
    constructor(params){
        super(params);
        this.bladeTitle("DataTable (gisc-ui-datatable)");
        this.bladeDescription("src/components/portals/developer/datatable/dataTableIndex");

        this.bladeMargin = BladeMargin.Narrow;
        this.selectedIndex = ko.observable();
    }
    onLoad(isFirstLoad) {
    }
    goto (data){
        var pageName = data.id;
        this.navigate(pageName);
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedIndex(null);
    }
}

export default {
    viewModel: ScreenBase.createFactory(DataTableIndexScreen),
    template: templateMarkup
};
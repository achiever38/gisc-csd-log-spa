import ko from "knockout";
import templateMarkup from "text!./experimental.html";
import ScreenBase from "../../screenbase";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import Utility from "../../../../app/frameworks/core/utility";


class DataTableExperimental extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle("Experimental");
        this.bladeDescription("src/components/portals/developer/datatable/experimental");
        this.bladeMargin = BladeMargin.Narrow;

        this.selectedIndex = ko.observable();
        
        // For test various sizes
        this.dsSize = ko.observable(10000); // 10000
    }

    generateRandomDatasource(size) {
        var positionPool = [ 
            "Professor", "Teacher", "Clergy", 
            "Philosopher", "Audiologist", "Chiropractor",
            "Dentist", "Dietitian", "Nurse",
            "Physician", "Podiatrist", "Psychologist",
            "Engineer", "Linguistic", "Lawyer" 
        ];
        var colorPool = ['red', 'green', 'blue'];
        var result = [];

        for(let i = 0; i < size; i++) {
            let item = {
                id: i+1,
                name: Utility.randomString(10),
                position: positionPool[_.random(0, 14)],
                age: _.random(18, 70),
                registered: _.random(18, 70) > 40,
                icon: '../images/ic-search.svg',
                color: colorPool[_.random(0, 2)]
            };
            result.push(item);
        }
        return result;
    }

    goto(data){
        var pageName = data.id;

        this.isBusy(true);

        var sampleSize = !_.isNil(this.dsSize()) ? this.dsSize() : 100;
        var sampleDS = this.generateRandomDatasource(this.dsSize());
        this.navigate(pageName, { ds: sampleDS });

        this.isBusy(false);
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedIndex(null);
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {}

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(DataTableExperimental),
    template: templateMarkup
};
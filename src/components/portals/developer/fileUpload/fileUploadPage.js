import templateMarkup from "text!./fileUploadPage.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import Utility from "../../../../app/frameworks/core/utility";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import ko from "knockout";

class FileUploadScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Medium;
        this.bladeTitle("FileUpload (gisc-ui-fileupload)");
        this.bladeDescription("src/components/portals/developer/fileUpload");

        this.uploadUrl = Utility.resolveUrl(WebConfig.appSettings.apiCoreUrl, "~/api/media/upload");
        this.fileName = ko.observable("default file.bak");
        this.fileUrl = ko.observable("");
        this.selectedFile = ko.observable();
        this.formData = ko.pureComputed(()=> {
            var f =this.selectedFile();
            return {id: 1, timestap: 'xijfe'};
        });
    }
    onLoad(isFirstLoad) {}
    onBeforeUpload(isValidToSubmit) {
        console.log('onBeforeUpload');
    }
    onUploadSuccess(response) {
        this.fileUrl(response[0].fileUrl);
        console.log(this.uploadUrl, 'returns', response);
    }
    onUploadFail(error) {
        console.log(this.uploadUrl, 'fail with', error);
    }
    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        // Manual setup track change.
        this.selectedFile.extend({
            trackChange: true
        });

        // Manual setup validation rules
        this.selectedFile.extend({
            fileRequired: true,
            fileExtension: ['jpg', 'png'],
            fileSize: 1, // MB
            fileImageDimension: [500, 500]
        });

        this.validationModel = ko.validatedObservable({
            selectedFile: this.selectedFile
        });
    }
}

export default {
    viewModel: FileUploadScreen.createFactory(FileUploadScreen),
    template: templateMarkup
};
import {
    AssetIcons
} from "../../../app/frameworks/constant/svg";
import ScreenBase from "../screenbase";
import Utility from "../../../app/frameworks/core/utility";
import {
    Enums
} from "../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../app/frameworks/configuration/webConfiguration";
import Navigation from "../../controls/gisc-chrome/shell/navigation";
import MapManager from "../../controls/gisc-chrome/map/mapManager";
import Logger from "../../../app/frameworks/core/logger";

import svgVdo from "text!../../../components/svgs/ic_vdo.html";
import svgPicture from "text!../../../components/svgs/ic_viewevent.html";
import svgVdoDisable from "text!../../../components/svgs/ic_vdo_disable.html";

import HikvisionUtility from "../../../app/frameworks/core/hikvisionUtility";
import IQTechUtility from "../../../app/frameworks/core/iqtechUtility";
import DFITechUtility from "../../../app/frameworks/core/dfiUtility";
import EncryptUtility from "../../../app/frameworks/core/encryptUtility"
var isNewWidget = false;
class AssetUtility {


    static headerTemplateIcon() {
        return _.template('<div class="<%= iconCssClass %>" title="<%= title %>"><%= icon %></div>');
    }

    /**
     * 
     * @param {any} reportTemplateFields
     * @param {boolean} [isAlert=false] //Check if this method is calling by Alert page.
     * @param {integer} startIndex //for case we have additional columns before report temport template fields
     * @param {number} primaryMenuColumn // for datagrid menu builder.
     * @returns
     * 
     * @memberOf TableAssetScreenBase
     */
    static getColumnDefinitions(reportTemplateFields, isAlert = false, startIndex = 0, primaryMenuColumn = -1, objPage = null) {
        var columns = [];
        var index = startIndex; //use custom index instead of forEach index because we may skip some column e.g. Job Code, Job Status
        var headerTemplateIcon = this.headerTemplateIcon();
        var widthXXS = '95px';
        var widthXS = '120px';
        var widthS = '150px';
        var widthM = '200px';
        var widthL = '300px';
        var widthIcon = '55px';
        var widthAuto = 0;


        // columns.push({
        //     type: 'object',
        //     title: 'i18n:EventVideo_Col_Video',
        //     width: '300px',
        //     searchable: false,
        //     template: this.renderPlayVideoIcon,

        //     // enumColumnName: null,
        //     // headerTemplate: null,
        //     // columnMenu: false,
        //     // columnMenuItem: true,
        //     // sortData:null,
        //     data: "driverName",

        //     // onclick:this.onPlayVideo ,
        //     // onClick:function(){ console.log("eiei") } ,
        //     onClick: this.onPlayVideo,
        //     className: 'dg-body-center dg-svg-column',
        //     sortable: false

        // });



        _.forEach(reportTemplateFields, (field) => {
            var type = "object";
            var data = "";
            var orderable = true;
            var title = "";
            var headerTemplate = null;
            var sType = "string";
            var skipColumn = false;
            var width = widthS;
            var template = null;
            var onclick = null;
            var cssClass = null;
            var enumColumnName = null;
            var sortData = null;
            // if column is feature
            if (field.featureId) {
                //data = "trackLocationFeatureValues[" + field.featureId + "].formatValue";
                data = field.feature.code;
                title = field.feature.unitDisplayName ? Utility.stringFormat("{0} ({1})", field.displayName, field.feature.unitDisplayName) : field.displayName;
                template = this.renderFeatureValueColumn(field.featureId)
                orderable = false; //feature cannot be sort by default

                switch (field.feature.code) {
                    case "TrkVwEngine":
                        // data = "trackLocationFeatureValues[" + field.featureId + "].id";
                        data = field.feature.code;
                        headerTemplate = headerTemplateIcon({
                            'iconCssClass': 'icon-engine',
                            'title': title,
                            'icon': AssetIcons.IconEngineTemplateMarkup
                        });
                        template = this.renderEngineColumn;
                        width = widthIcon;
                        break;
                    case "TrkVwGate":
                        // data = "trackLocationFeatureValues[" + field.featureId + "].id";
                        data = field.feature.code;
                        headerTemplate = headerTemplateIcon({
                            'iconCssClass': 'icon-gate',
                            'title': title,
                            'icon': AssetIcons.IconGateTemplateMarkup
                        });
                        template = this.renderGateColumn;
                        width = widthIcon;
                        break;
                    case "TrkVwFuel":
                        headerTemplate = headerTemplateIcon({
                            'iconCssClass': 'icon-fuel',
                            'title': title,
                            'icon': AssetIcons.IconFuelTemplateMarkup
                        });
                        width = widthIcon;
                        break;
                    case "TrkVwTemp":
                        headerTemplate = headerTemplateIcon({
                            'iconCssClass': 'icon-temperature',
                            'title': title,
                            'icon': AssetIcons.IconTemparetureTemplateMarkup
                        });
                        width = widthIcon;
                        break;
                    case "TrkVwVehicleBattery":
                        headerTemplate = headerTemplateIcon({
                            'iconCssClass': 'icon-vehiclebattery',
                            'title': title,
                            'icon': AssetIcons.IconVehicleBatteryTemplateMarkup
                        });
                        width = widthIcon;
                        break;
                    case "TrkVwDeviceBattery":
                        headerTemplate = headerTemplateIcon({
                            'iconCssClass': 'icon-devicebattery',
                            'title': title,
                            'icon': AssetIcons.IconDeviceBatteryTemplateMarkup
                        });
                        width = widthIcon;
                        break;
                    case "TrkVwSOS":
                        // data = "trackLocationFeatureValues[" + field.featureId + "].valueDigital";
                        data = field.feature.code;
                        headerTemplate = headerTemplateIcon({
                            'iconCssClass': 'icon-sos',
                            'title': title,
                            'icon': AssetIcons.IconSosTemplateMarkup
                        });
                        template = this.renderSOSColumn;
                        width = widthIcon;
                        break;
                    case "TrkVwPower":
                        // data = "trackLocationFeatureValues[" + field.featureId + "].valueDigital";
                        data = field.feature.code;
                        headerTemplate = headerTemplateIcon({
                            'iconCssClass': 'icon-power',
                            'title': title,
                            'icon': AssetIcons.IconPowerTemplateMarkup
                        });
                        template = this.renderPowerColumn;
                        width = widthIcon;
                        break;
                    case "TrkVwDRVCardReader":
                        headerTemplate = headerTemplateIcon({
                            'iconCssClass': 'icon-drivercard',
                            'title': title,
                            'icon': AssetIcons.IconDriverCardTemplateMarkup
                        });
                        width = widthL;
                        break;
                    case "TrkVwPSGRCardReader":
                        headerTemplate = headerTemplateIcon({
                            'iconCssClass': 'icon-passengercard',
                            'title': title,
                            'icon': AssetIcons.IconPassengerCardTemplateMarkup
                        });
                        width = widthL;
                        break;
                    case "TrkVwMileage":
                        headerTemplate = headerTemplateIcon({
                            'iconCssClass': 'icon-mileage',
                            'title': title,
                            'icon': AssetIcons.IconMileageTemplateMarkup
                        });
                        width = widthIcon;
                        break;
                    default:
                        headerTemplate = headerTemplateIcon({
                            'iconCssClass': 'icon-customfeature',
                            'title': title,
                            'icon': AssetIcons.IconCustomFeatureTemplateMarkup
                        });
                        width = widthIcon;
                        break;
                }
            }
            // column is not feature
            else {
                data = Utility.toLowerCaseFirstLetter(field.mapPropertyName);
                sortData = data;
                title = field.unitSymbol ? Utility.stringFormat("{0} ({1})", field.displayName, field.unitSymbol) : field.displayName;
                switch (field.name) {
                    case "License":
                        width = widthXXS;
                        enumColumnName = Enums.SortingColumnName.VehicleLicense;
                        break;
                    case "ReferenceNo":
                        width = widthS;
                        enumColumnName = Enums.SortingColumnName.VehicleReferenceNo;
                        break;
                    case "Username":
                        width = widthS;
                        enumColumnName = Enums.SortingColumnName.Username;
                        break;
                    case "Email":
                        width = widthM;
                        enumColumnName = Enums.SortingColumnName.Email;
                        break;
                    case "EmployeeID":
                        width = widthS;
                        enumColumnName = Enums.SortingColumnName.DriverEmployeeId;
                        break;
                    case "Driver":
                        width = widthS;
                        data = "driverName"; // "driver";
                        sortData = data;
                        template = this.renderDriverColumn;
                        enumColumnName = Enums.SortingColumnName.DriverName;
                        break;
                    case "BusinessUnit":
                        width = widthS;
                        enumColumnName = Enums.SortingColumnName.BusinessUnitName;
                        break;
                    case "Radius":
                        enumColumnName = Enums.SortingColumnName.Radius;
                        sortData = "radius";
                        break;
                    case "Movement":
                        data = "movement";
                        sortData = data;
                        headerTemplate = headerTemplateIcon({
                            'iconCssClass': 'icon-movement',
                            'title': title,
                            'icon': AssetIcons.IconMovementTemplateMarkup
                        });
                        template = this.renderMovementColumn;
                        width = widthIcon;
                        enumColumnName = Enums.SortingColumnName.Movement;
                        break;
                    case "Speed":
                        headerTemplate = headerTemplateIcon({
                            'iconCssClass': 'icon-speed',
                            'title': title,
                            'icon': AssetIcons.IconSpeedTemplateMarkup
                        });
                        width = widthIcon;
                        enumColumnName = Enums.SortingColumnName.Speed;
                        sortData = "speed";
                        break;
                    case "Direction":
                        data = "directionType";
                        sortData = data;
                        headerTemplate = headerTemplateIcon({
                            'iconCssClass': 'icon-direction',
                            'title': title,
                            'icon': AssetIcons.IconDirectionTemplateMarkup
                        });
                        template = this.renderDirectionColumn;
                        width = widthIcon;
                        enumColumnName = Enums.SortingColumnName.DirectionType;
                        break;
                    case "GPSStatus":
                        data = "gpsStatus";
                        sortData = data;
                        headerTemplate = headerTemplateIcon({
                            'iconCssClass': 'icon-gps',
                            'title': title,
                            'icon': AssetIcons.IconGpsTemplateMarkup
                        });
                        template = this.renderGpsStatusColumn;
                        width = widthIcon;
                        enumColumnName = Enums.SortingColumnName.GpsStatus;
                        break;
                    case "GSMStatus":
                        data = "gsmStatus";
                        sortData = data;
                        headerTemplate = headerTemplateIcon({
                            'iconCssClass': 'icon-gsm',
                            'title': title,
                            'icon': AssetIcons.IconGsmTemplateMarkup
                        });
                        template = this.renderGsmStatusColumn;
                        width = widthIcon;
                        enumColumnName = Enums.SortingColumnName.GsmStatus;
                        break;
                    case "Altitude":
                        headerTemplate = headerTemplateIcon({
                            'iconCssClass': 'icon-altitude',
                            'title': title,
                            'icon': AssetIcons.IconAltitudeTemplateMarkup
                        });
                        width = widthIcon;
                        enumColumnName = Enums.SortingColumnName.Altitude;
                        sortData = "altitude";
                        break;
                    case "DLTStatus":
                        data = "dltStatus";
                        sortData = data;
                        headerTemplate = headerTemplateIcon({
                            'iconCssClass': 'icon-dltstatus',
                            'title': title,
                            'icon': AssetIcons.IconDltStatusTemplateMarkup
                        });
                        template = this.renderDltStatusColumn;
                        width = widthIcon;
                        enumColumnName = Enums.SortingColumnName.DltStatus;
                        break;
                    case "TotalEvent":
                        headerTemplate = headerTemplateIcon({
                            'iconCssClass': 'icon-event',
                            'title': title,
                            'icon': AssetIcons.IconEventTemplateMarkup
                        });
                        width = widthIcon;
                        enumColumnName = Enums.SortingColumnName.TotalAlerts;
                        sortData = "totalAlerts";
                        break;
                    case "CoDriver":
                        width = widthM;
                        orderable = false;
                        sortData = null;
                        break;
                    case "JobCode":
                    case "JobStatus":
                        skipColumn = true;
                        break;
                    case "Datetime":
                        width = widthXS;
                        enumColumnName = isAlert ? Enums.SortingColumnName.AlertDateTime : Enums.SortingColumnName.DateTime;
                        sortData = "dateTime";
                        template = isAlert ? this.renderAlertDateTimeColumn : this.renderDateTimeColumn;
                        break;
                    case "AlertDuration":
                        width = widthXXS;
                        sortData = null;
                        orderable = false;
                        break;
                    case "POI":
                        template = this.renderPoiIconColumn;
                        width = widthIcon;
                        enumColumnName = Enums.SortingColumnName.PoiIconName;
                        sortData = "poiIconName";
                        break;
                    case "Location":
                        width = widthL;
                        orderable = false;
                        template = this.renderLocationColumn;
                        cssClass = "truncate";
                        onclick = this.onLocationColumnClick;
                        sortData = null;
                        break;
                    case "AlertType":
                        width = widthM;
                        enumColumnName = Enums.SortingColumnName.AlertType;
                        sortData = "alertType";
                        break;
                    case 'IsDelay':
                        data = "isDelay";
                        sortData = data;
                        template = this.renderDelayColumn;
                        enumColumnName = Enums.SortingColumnName.IsDelay;
                        break;
                    case "PlayVideo":
                        width = '58px';
                        template = this.renderPlayVideoIcon;
                        onclick = (evt) => {
                            this.onPlayVideo(evt, objPage);
                        }
                        orderable = false;
                        // enumColumnName = Enums.SortingColumnName.AlertType;
                        // sortData = "alertType";
                        break;

                }
            }

            if (!skipColumn) {
                // Configured data grid primary menu column.
                var c = {
                    type: type,
                    title: title,
                    headerTemplate: headerTemplate,
                    width: width,
                    data: data,
                    sortable: orderable,
                    className: cssClass,
                    onClick: onclick,
                    template: template,
                    enumColumnName: enumColumnName,
                    sortData: sortData
                };

                // Perform primary column logic.
                if (primaryMenuColumn !== -1) {
                    if (primaryMenuColumn === c.enumColumnName) {
                        c.columnMenu = true;
                        c.columnMenuItem = false;
                    } else {
                        c.columnMenu = false;
                        c.columnMenuItem = true;
                    }
                }

                columns.push(c);
                index++;
            }
        });
        return columns;
    }

    static query(datasource, pageSize, pageStart, sortingColumns = []) {
        var dataExists = datasource && datasource.length;
        if (dataExists) {
            // Skip reference
            var cloneDS = _.take(datasource, datasource.length);

            // First step perform search.
            if (sortingColumns && sortingColumns.length) {
                // NOTE: Support only one level only.
                var sortConfig = sortingColumns[0];
                var sortField = sortConfig.field;
                var sortDir = sortConfig.dir;

                // Perform sorting.
                cloneDS.sort((a, b) => {
                    var valA = _.get(a, sortField);
                    var valB = _.get(b, sortField);
                    var compareResult = 0;
                    if (_.isNil(valA)) {
                        valA = "";
                    }
                    if (_.isNil(valB)) {
                        valB = "";
                    }
                    // Compare sort result.
                    if (valA > valB) {
                        compareResult = 1;
                    } else if (valA < valB) {
                        compareResult = -1;
                    }

                    // Multiply with direction.
                    return compareResult * (sortDir === "asc" ? 1 : -1);
                });
            }

            // Perform paginate.
            var pageEnd = Math.min(cloneDS.length, pageStart + pageSize);
            var currentPage = cloneDS.slice(pageStart, pageStart + pageSize);
            return currentPage;
        }

        return [];
    }

    static currentPage(datasource, page, pageSize) {
        let result = 0;
        let maxData = datasource.length;
        let maxPage = Math.ceil(maxData / pageSize);
        if(page > maxPage){
            result = maxPage
        }else{
            result = page
        }
        return result;
    }

    static filter(datasource, filter) {
        var field = (filter) && filter.filters[0].field;
        var operator = (filter) && filter.filters[0].operator;
        var value = (filter) && filter.filters[0].value;
        var newDS = new Array();
        if(_.size(datasource)){
            _.forEach(datasource, (item)=>{
                if(operator == "contains"){
                    if(item[field].indexOf(value) != -1){
                        newDS.push(item);
                    }
                }else{
                    if(item[field] == value){
                        newDS.push(item);
                    }
                }
                
            });
        }
        return newDS;
    }

    static renderMovementColumn(data) {
        if (data) {
            // console.log('data',data)
            // data.movementDisplayName = 'Park Engine On';
            // data.movement = 4;
            var cssClass = "move";
            var iconMovement = AssetIcons.IconMovementTemplateMarkup;
            switch (data.movement) {
                case Enums.ModelData.MovementType.Move:
                    cssClass = "move";
                    break;
                case Enums.ModelData.MovementType.Stop:
                    cssClass = "stop";
                    break;
                case Enums.ModelData.MovementType.Park:
                    cssClass = "park";
                    break;
                case Enums.ModelData.MovementType.ParkEngineOn:
                    cssClass = "parkEngineOn";
                    break;
                case Enums.ModelData.MovementType.MoveOver35Hrs:
                    iconMovement = AssetIcons.IconMovementOver35HrsTemplateMarkup;
                    break;
                case Enums.ModelData.MovementType.MoveOver4Hrs:
                    iconMovement = AssetIcons.IconMovementOver4HrsTemplateMarkup;
                    break;
                //default: // ถ้าไม่เข้า case ด้านบน แสดงว่า เป็น Over 3.5 , 4 Hrs
                //    let iconMoveOver = "";
                //    switch (data.movement) {
                //        case Enums.ModelData.MovementType.MoveOver35Hrs:
                //            iconMoveOver = '<div title="' + data.movementDisplayName + '">' + AssetIcons.IconMovementOver35HrsTemplateMarkup + '</div>';
                //            break;
                //        case Enums.ModelData.MovementType.MoveOver4Hrs:
                //            iconMoveOver = '<div title="' + data.movementDisplayName + '">' + AssetIcons.IconMovementOver4HrsTemplateMarkup + '</div>';
                //            break;
                //    }
                //    console.log(iconMoveOver);
                //    return iconMoveOver;
                //   //break;
            }
            let node = '<div title="' + data.movementDisplayName + '" class="icon-movement ' + cssClass + '">' + iconMovement + '</div>';
            return node;
        } else {
            return "";
        }
    }


    static renderDriverColumn(data) {
        return Utility.createDriverNameWithTelephoneTag(data.driverName, data.driverMobile);
    }

    static renderDirectionColumn(data) {
        if (data) {
            var cssClass = " ";
            var iconTemplateMarkup = null;
            switch (data.directionType) {
                case Enums.ModelData.DirectionType.N:
                    iconTemplateMarkup = AssetIcons.IconDirectionNTemplateMarkup;
                    cssClass += "n";
                    break;
                case Enums.ModelData.DirectionType.NE:
                    iconTemplateMarkup = AssetIcons.IconDirectionNETemplateMarkup;
                    cssClass += "ne";
                    break;
                case Enums.ModelData.DirectionType.E:
                    iconTemplateMarkup = AssetIcons.IconDirectionETemplateMarkup;
                    cssClass += "e";
                    break;
                case Enums.ModelData.DirectionType.SE:
                    iconTemplateMarkup = AssetIcons.IconDirectionSETemplateMarkup;
                    cssClass += "se";
                    break;
                case Enums.ModelData.DirectionType.S:
                    iconTemplateMarkup = AssetIcons.IconDirectionSTemplateMarkup;
                    cssClass += "s";
                    break;
                case Enums.ModelData.DirectionType.SW:
                    iconTemplateMarkup = AssetIcons.IconDirectionSWTemplateMarkup;
                    cssClass += "sw";
                    break;
                case Enums.ModelData.DirectionType.W:
                    iconTemplateMarkup = AssetIcons.IconDirectionWTemplateMarkup;
                    cssClass += "w";
                    break;
                case Enums.ModelData.DirectionType.NW:
                    iconTemplateMarkup = AssetIcons.IconDirectionNWTemplateMarkup;
                    cssClass += "nw";
                    break;
            }

            if (iconTemplateMarkup != null) {
                let node = '<div title="' + data.directionTypeDisplayName + '" class="icon-direction' + cssClass + '">' + iconTemplateMarkup + '</div>';
                return node;
            }
        }
        return "";
    }

    static renderGsmStatusColumn(data) {
        if (data) {
            var cssClass = " ";
            var iconTemplateMarkup = null;
            switch (data.gsmStatus) {
                case Enums.ModelData.GsmStatus.Bad:
                    iconTemplateMarkup = AssetIcons.IconGsmBadTemplateMarkup;
                    cssClass += "bad";
                    break;
                case Enums.ModelData.GsmStatus.Medium:
                    iconTemplateMarkup = AssetIcons.IconGsmMediumTemplateMarkup;
                    cssClass += "medium";
                    break;
                case Enums.ModelData.GsmStatus.Good:
                    iconTemplateMarkup = AssetIcons.IconGsmGoodTemplateMarkup;
                    cssClass += "good";
                    break;
            }

            if (iconTemplateMarkup != null) {
                let node = '<div title="' + data.gsmStatusDisplayName + '" class="icon-gsm' + cssClass + '">' + iconTemplateMarkup + '</div>';
                return node;
            }
        }

        return "";
    }

    static renderGpsStatusColumn(data) {
        if (data) {
            var cssClass = " ";
            switch (data.gpsStatus) {
                case Enums.ModelData.GpsStatus.Bad:
                    cssClass += "bad";
                    break;
                case Enums.ModelData.GpsStatus.Medium:
                    cssClass += "medium";
                    break;
                case Enums.ModelData.GpsStatus.Good:
                    cssClass += "good";
                    break;
            }

            let node = '<div title="' + data.gpsStatusDisplayName + '" class="icon-gps' + cssClass + '">' + AssetIcons.IconGpsTemplateMarkup + '</div>';
            return node;
        } else {
            return "";
        }
    }

    static renderDltStatusColumn(data) {
        if (data) {
            var cssClass = " ";
            var iconTemplateMarkup = null;
            switch (data.dltStatus) {
                case Enums.ModelData.DltStatus.Success:
                    cssClass += "success";
                    iconTemplateMarkup = AssetIcons.IconDltStatusSuccessTemplateMarkup;
                    break;
                case Enums.ModelData.DltStatus.Fail:
                    cssClass += "fail";
                    iconTemplateMarkup = AssetIcons.IconDltStatusFailTemplateMarkup;
                    break;
            }

            if (iconTemplateMarkup != null) {
                let node = '<div title="' + data.dltStatusDisplayName + '" class="icon-dltstatus' + cssClass + '">' + iconTemplateMarkup + '</div>';
                return node;
            }
        }
        return "";
    }

    static renderEngineColumn(data) {
        if (!_.isNil(data)) {
            var trackLocationFeatureValue = _.find(data.trackLocationFeatureValues, function (o) {
                return o.featureCode == "TrkVwEngine";
            });
            if (trackLocationFeatureValue) {
                var cssClass = " ";
                switch (trackLocationFeatureValue.valueDigital) {
                    case 0:
                        cssClass += "off";
                        break;
                    case 1:
                        cssClass += "on";
                        break;
                }

                let node = '<div title="' + trackLocationFeatureValue.formatValue + '" class="icon-engine' + cssClass + '">' + AssetIcons.IconEngineTemplateMarkup + '</div>';
                return node;
            }
        }

        return "";
    }

    static renderGateColumn(data) {
        if (!_.isNil(data)) {
            var trackLocationFeatureValue = _.find(data.trackLocationFeatureValues, function (o) {
                return o.featureCode == "TrkVwGate";
            });
            if (trackLocationFeatureValue) {
                var cssClass = " ";
                var iconTemplateMarkup = null;
                switch (trackLocationFeatureValue.valueDigital) {
                    case 0:
                        iconTemplateMarkup = AssetIcons.IconGateOffTemplateMarkup;
                        cssClass += "off";
                        break;
                    case 1:
                        iconTemplateMarkup = AssetIcons.IconGateOnTemplateMarkup;
                        cssClass += "on";
                        break;
                }

                if (iconTemplateMarkup != null) {
                    let node = '<div title="' + trackLocationFeatureValue.formatValue + '" class="icon-gate' + cssClass + '">' + iconTemplateMarkup + '</div>';
                    return node;
                }
            }
        }

        return "";
    }

    static renderSOSColumn(data) {
        if (!_.isNil(data)) {
            var trackLocationFeatureValue = _.find(data.trackLocationFeatureValues, function (o) {
                return o.featureCode == "TrkVwSOS";
            });
            if (trackLocationFeatureValue) {
                var cssClass = " ";
                var iconTemplateMarkup = null;
                switch (trackLocationFeatureValue.valueDigital) {
                    case 0:
                        iconTemplateMarkup = AssetIcons.IconSosTemplateMarkup;
                        cssClass += "off";
                        break;
                    case 1:
                        iconTemplateMarkup = AssetIcons.IconSosTemplateMarkup;
                        cssClass += "on";
                        break;
                }

                if (iconTemplateMarkup != null) {
                    let node = '<div title="' + trackLocationFeatureValue.formatValue + '" class="icon-sos' + cssClass + '">' + iconTemplateMarkup + '</div>';
                    return node;
                }
            }
        }
        return "";
    };

    static renderPowerColumn(data) {
        if (!_.isNil(data)) {
            var trackLocationFeatureValue = _.find(data.trackLocationFeatureValues, function (o) {
                return o.featureCode == "TrkVwPower";
            });
            if (trackLocationFeatureValue) {
                var cssClass = " ";
                var iconTemplateMarkup = null;
                switch (trackLocationFeatureValue.valueDigital) {
                    case 0:
                        iconTemplateMarkup = AssetIcons.IconPowerTemplateMarkup;
                        cssClass += "off";
                        break;
                    case 1:
                        iconTemplateMarkup = AssetIcons.IconPowerTemplateMarkup;
                        cssClass += "on";
                        break;
                }

                if (iconTemplateMarkup != null) {
                    let node = '<div title="' + trackLocationFeatureValue.formatValue + '" class="icon-power' + cssClass + '">' + iconTemplateMarkup + '</div>';
                    return node;
                }
            }
        }

        return "";
    }

    static renderPoiIconColumn(data) {
        if (data) {
            var poiIcons = WebConfig.fleetMonitoring.trackLocationPoiIcons();
            var poiIcon = poiIcons[data.poiIconId];

            let node = "";
            if (poiIcon) {
                node = '<img class="icon-poi" title="' + data.poiIconName + '" src="' + poiIcon + '" />';
            }
            return node;
        }
        return "";
    }

    static renderDelayColumn(data) {
        if (data && data.isDelay == true) {
            let node = '<div title="' + data.formatIsDelay + '" class="icon-delay">' + AssetIcons.IconDelayTemplateMarkup + '</div>';
            return node;
        } else {
            return "";
        }
    }

    static renderLocationColumn(data) {
        if (data) {
            var linkCssClass = "location skip-parent-click truncate";

            switch (data.locationDescriptionColor) {

                case Enums.ModelData.LocationDescriptionColor.Black:
                    linkCssClass += " system-poi";
                    break;
                case Enums.ModelData.LocationDescriptionColor.Green:
                    linkCssClass += " system-poi-green";
                    break;
                case Enums.ModelData.LocationDescriptionColor.Blue:
                    linkCssClass += " system-poi-blue";
                    break;
            }

            var node = '<a title="' + data.location + '" href="javascript:void(0)" class="' + linkCssClass + '">' + data.location + '</a>';
            return node;
        }
        return "";
    }

    static renderRemoveColumn(data) {
        var node = '<img class="icon-trackmonitoring-remove" src="../images/icon-trackmonitoring-remove.svg" />';
        return node;
    }

    static renderDateTimeColumn(data) {
        if (data) {
            let node = '<div title="' + data.formatDateTime + '" class="truncate ' + (data.isOverFleetDelayTime ? "over-fleet-delay-time" : "") + '">' + data.formatDateTime + '</div>';
            return node;
        } else {
            return "";
        }
    }

    static renderAlertDateTimeColumn(data) {
        if (data) {
            let node = '<div title="' + data.formatAlertDateTime + '" class="truncate ' + (data.isOverFleetDelayTime ? "over-fleet-delay-time" : "") + '">' + data.formatAlertDateTime + '</div>';
            return node;
        } else {
            return "";
        }
    }

    static renderPlayVideoIcon(data) {
       // console.log("data>>", data);

        var svg = '';
        if (data.alertDisplayType == Enums.ModelData.AlertDisplayType.Video) {
            svg = `<div class="PlayVideoStatus"> ${svgVdo} </div>`;
        }else if(data.alertDisplayType == Enums.ModelData.AlertDisplayType.Picture){
            svg = `<div class="PlayVideoStatus"> ${svgPicture} </div>`;
        }else if (data.deviceCode && data.deviceId) {

            if(data.mdvrBrand === 4){
                if(data.alertMediaInfo.length > 0){
                    svg = `<div class="PlayVideoStatus"> ${svgVdo} </div>`;
                }else{
                    svg = `<div class="PlayVideoStatus PlayVideoStatusDisable"> ${svgVdoDisable} </div>`;
                }
            }else{
                svg = `<div class="PlayVideoStatus"> ${svgVdo} </div>`;
            }
        } 

        return svg;
    }


    static onPlayVideo(params, objPage) {

        let hikvisionUtility = HikvisionUtility.getInstance();
        let iqtechUtility = IQTechUtility.getInstance();
        let dfiUtility = DFITechUtility.getInstance();
        let encryptUtility = EncryptUtility.getInstance();
        let filter;


        if(params.alertDisplayType == Enums.ModelData.AlertDisplayType.Video){
            let driverName = params.driverName == null || params.driverName == " " ? "-" :  params.driverName;
            let vehicleLicense = params.vehicleLicense == null || params.vehicleLicense == " " ? "-" :  params.vehicleLicense;
            
            let videoWidget = $('#cw-fleet-monitoring-common-video-widget');
            if(videoWidget.length){
                videoWidget.data("kendoWindow").close();
                setTimeout(()=>{
                    objPage.widget('cw-fleet-monitoring-common-video-widget', params, {
                        title: `${vehicleLicense}, ${driverName}`,
                        modal: false,
                        resizable: false,
                        minimize: false,
                        target: "cw-fleet-monitoring-common-video-widget",
                        width: "480",
                        height: "300",
                        id: 'cw-fleet-monitoring-common-video-widget',
                        left: "30%",
                        bottom: "20%"
                    });
                },500);
            }else{
                objPage.widget('cw-fleet-monitoring-common-video-widget', params, {
                    title: `${vehicleLicense}, ${driverName}`,
                    modal: false,
                    resizable: false,
                    minimize: false,
                    target: "cw-fleet-monitoring-common-video-widget",
                    width: "480",
                    height: "300",
                    id: 'cw-fleet-monitoring-common-video-widget',
                    left: "30%",
                    bottom: "20%"
                });
            }
            
        }else if(params.alertDisplayType == Enums.ModelData.AlertDisplayType.Picture){
            let driverName = params.driverName == null || params.driverName == " " ? "-" :  params.driverName;
            let vehicleLicense = params.vehicleLicense == null || params.vehicleLicense == " " ? "-" :  params.vehicleLicense;

            let pictureWidget = $('#cw-fleet-monitoring-common-picture-widget');
            if(pictureWidget.length){
                pictureWidget.data("kendoWindow").close();
                setTimeout(()=>{
                    objPage.widget('cw-fleet-monitoring-common-picture-widget', params, {
                        title: `${vehicleLicense}, ${driverName}`,
                        modal: false,
                        resizable: true,
                        minimize: false,
                        target: "cw-fleet-monitoring-common-picture-widget",
                        width: "480",
                        height: "300",
                        id: 'cw-fleet-monitoring-common-picture-widget',
                        left: "30%",
                        bottom: "20%"
                    });
                },500);
            }else{
                objPage.widget('cw-fleet-monitoring-common-picture-widget', params, {
                    title: `${vehicleLicense}, ${driverName}`,
                    modal: false,
                    resizable: true,
                    minimize: false,
                    target: "cw-fleet-monitoring-common-picture-widget",
                    width: "480",
                    height: "300",
                    id: 'cw-fleet-monitoring-common-picture-widget',
                    left: "30%",
                    bottom: "20%"
                });
            }

            
            
        }else if (params.mdvrBrand == Enums.ModelData.MDVRModel.HIKvision) {

            let vehicleInfos = { //ข้อมูลรถและคนขับสำหรับแสดงบน Title Video

                "vehicleLicense": params.vehicleLicense,
                "alertTypeDisplayName": params.alertTypeDisplayName,
                "formatAlertDateTime": params.formatAlertDateTime,
                "driverName": params.driverName

            }
           // console.log("Onplay Video HikVision", params);
            var dfd = $.Deferred();
            localStorage.removeItem("cameraInfos");
            localStorage.removeItem("vehicleInfos");

            filter = {
                "deviceId": params.deviceId,
                "startTime": params.startPreview,
                "endTime": params.endPreview,
                "recordType": 1 //Device
            }


            hikvisionUtility.playBack(filter).done((res) => {

                localStorage.cameraInfos = JSON.stringify(res.items);
                localStorage.vehicleInfos = JSON.stringify(vehicleInfos);

                dfd.resolve();

            }).fail((e) => {
                console.log(e);
            })

            window.open(WebConfig.appSettings.eventView, "EventviewPage");
            return dfd;


        } else if (params.mdvrBrand == Enums.ModelData.MDVRModel.IQTech) {

            var dfdEventVideo = $.Deferred();

            let startPreview = new Date(params.startPreview);
            let endPreview = new Date(params.endPreview);

            //decrypt Key
            let cameraConig = encryptUtility.decryption(params.mdvrServerKey)
            // let result = config.split("|")
  
            iqtechUtility.getEventVideo(params.deviceId, startPreview, endPreview, cameraConig).done((resVideo) => {

                //console.log("resVideo>>", resVideo)
                // console.log("isNewWidget", isNewWidget)
                if (isNewWidget) {
                    objPage.closeWidget();
                }

                resVideo.videoInfos = params; //add property to json
                resVideo.cameraConfig = cameraConig;


                //if null or "" set value to "-"
                let driverName = params.driverName == null || params.driverName == " " ? "-" :  params.driverName;
                let vehicleLicense = params.vehicleLicense == null || params.vehicleLicense == " " ? "-" :  params.vehicleLicense;
                

                objPage.widget('cw-fleet-monitoring-eventview-widget', resVideo, {
                    // title: this.i18n("FleetMonitoring_Playback_Timeline_Widget")(),
                    title: `${vehicleLicense}, ${driverName}`,
                    modal: false,
                    resizable: false,
                    minimize: false,
                    target: "cw-fleet-monitoring-eventview-widget",
                    width: "500",
                    height: "520",
                    id: 'cw-fleet-monitoring-eventview-widget',
                    left: "25%",
                    bottom: "10%"
                });

                isNewWidget = true;

                dfdEventVideo.resolve();
                // this.isBusy(false);

            }).fail((e) => {
                console.log(e);
            })

        } else if (params.mdvrBrand == Enums.ModelData.MDVRModel.DFI) {    
            var dfdEventVideo = $.Deferred();

            let startPreview = new Date(params.startPreview);
            let endPreview = new Date(params.endPreview);
            //decrypt Key
            let cameraConig = encryptUtility.decryption(params.mdvrServerKey);

 
            dfiUtility.getEventVideo(params.deviceId, startPreview, endPreview, cameraConig).done((resVideo) => {

                //console.log("resVideo>>", resVideo)
                // console.log("isNewWidget", isNewWidget)
                if (isNewWidget) {
                    objPage.closeWidget();
                }

                resVideo.videoInfos = params; //add property to json
                resVideo.cameraConfig = cameraConig;
                resVideo.eventvideo = params.alertMediaInfo;

                //if null or "" set value to "-"
                let driverName = params.driverName == null || params.driverName == " " ? "-" :  params.driverName;
                let vehicleLicense = params.vehicleLicense == null || params.vehicleLicense == " " ? "-" :  params.vehicleLicense;
                         
                objPage.widget('cw-fleet-monitoring-eventview-widget', resVideo, {
                    // title: this.i18n("FleetMonitoring_Playback_Timeline_Widget")(),
                    title: `${vehicleLicense}, ${driverName}`,
                    modal: false,
                    resizable: false,
                    minimize: false,
                    target: "cw-fleet-monitoring-eventview-widget",
                    width: "500",
                    height: "520",
                    id: 'cw-fleet-monitoring-eventview-widget',
                    left: "25%",
                    bottom: "10%"
                });

                isNewWidget = true;

                dfdEventVideo.resolve();
                // this.isBusy(false);

            }).fail((e) => {
                console.log(e);
            })

        }
        


    }


    static onLocationColumnClick(data, event) {
        Logger.info("onLocationColumnClick");
        var lat = data.latitude;
        var lon = data.longitude;
        var assetUtilityId = "asset-utility-id";
        var mapManager = MapManager.getInstance();
        mapManager.drawPointBuffer(
            assetUtilityId, {
                lat: lat,
                lon: lon
            },
            WebConfig.mapSettings.defaultRadius,
            WebConfig.mapSettings.defaultUnit,
            WebConfig.mapSettings.fillColor,
            WebConfig.mapSettings.borderColor,
            WebConfig.mapSettings.fillOpacity,
            WebConfig.mapSettings.borderOpacity);
        mapManager.zoomPoint(assetUtilityId, lat, lon, WebConfig.mapSettings.defaultZoom);
        Navigation.getInstance().minimizeAll(true);
    }

    static updateIconResult(vehicleIcons = {}, poiIcons = {}) {
        WebConfig.fleetMonitoring.trackLocationVehicleIcons(
            _.extend(WebConfig.fleetMonitoring.trackLocationVehicleIcons(), vehicleIcons)
        );

        WebConfig.fleetMonitoring.trackLocationPoiIcons(
            _.extend(WebConfig.fleetMonitoring.trackLocationPoiIcons(), poiIcons)
        );
    }

    static renderFeatureValueColumn(data) {
        var value = "# if(trackLocationFeatureValues[" + data + "].formatValue == null) { # ";
        value += "#= '' # ";
        value += "# }else{ # ";
        value += "#= trackLocationFeatureValues[" + data + "].formatValue # ";
        value += "# } # ";
        return value;
    }

}
export default AssetUtility;
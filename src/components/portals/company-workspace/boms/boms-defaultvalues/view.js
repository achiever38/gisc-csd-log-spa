﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import WebRequestBOMsDefaultValues from "../../../../../app/frameworks/data/apitrackingcore/WebRequestBOMsDefaultValues";
import ScreenBase from "../../../screenbase";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Enums, Constants } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";


/**
 * Display available subscription menu based on user permission.
 * @class MenuScreen
 * @extends {ScreenBase}
 */
class BOMsDefaultValuesView extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("BOMs_Default_Value_View")());
        this.bladeSize = BladeSize.Small;
        this.id = this.ensureNonObservable(params.id, 0);


        this.name = ko.observable();
        this.description = ko.observable();
        this.businessUnitName = ko.observable();
        this.travelMode = ko.observable();
        this.jobCate = ko.observable();
        this.late = ko.observable();
        this.maybeLate = ko.observable();



        this.subscribeMessage("cw-boms-defaultValue-changed", id => {
            this.getItemBomsDefaultValue(id);
        });

    }

    /**
     * Get WebRequest BOMS
     * @readonly
     */
    get webRequestBOMsDefaultValues() {
        return WebRequestBOMsDefaultValues.getInstance();
    }


    getItemBomsDefaultValue(id) {
        var dfd = $.Deferred();
        this.webRequestBOMsDefaultValues.getBOMsDefaultValues(id).done((res) => {
            this.name(res.name);
            this.description(res.description);
            this.businessUnitName(res.businessUnitName);
            this.travelMode(res.travelModeDisplayName);
            this.jobCate(res.jobCategoryDisplayName);
            this.late(res.late);
            this.maybeLate(res.mayBeLate);

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
            this.handleError(e);
        });
        return dfd;

    }

    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.UpdateBomsDefaultValue)) {
            commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.DeleteBomsDefaultValue)) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }

    onCommandClick(sender) {
        switch (sender.id) {
            case 'cmdUpdate':
                this.navigate("cw-boms-defaultvalues-manage", { id: this.id, mode: Screen.SCREEN_MODE_UPDATE });
                break;
            case 'cmdDelete':
                this.showMessageBox(null, this.i18n("M250")(),
                    BladeDialog.DIALOG_YESNO).done((button) => {
                        switch (button) {
                            case BladeDialog.BUTTON_YES:
                                this.isBusy(true);

                                this.webRequestBOMsDefaultValues.deleteBOMsDefaultValues(this.id).done(() => {
                                    this.isBusy(false);

                                    this.publishMessage("cw-boms-defaultValue-delete");
                                    this.close(true);
                                }).fail((e) => {
                                    this.handleError(e);
                                    this.isBusy(false);
                                });
                                break;
                        }
                    });
                break;
            default:
        }
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        
        this.getItemBomsDefaultValue(this.id);
    }
}

export default {
    viewModel: ScreenBase.createFactory(BOMsDefaultValuesView),
    template: templateMarkup
};
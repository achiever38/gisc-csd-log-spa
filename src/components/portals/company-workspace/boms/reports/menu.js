﻿import ko from "knockout";
import templateMarkup from "text!./menu.html";
import ScreenBase from "../../../screenbase";
import * as BladeMargin from "../../../../../app/frameworks/constant/bladeMargin";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import { Enums, Constants } from "../../../../../app/frameworks/constant/apiConstant";

/**
 * Display available subscription menu based on user permission.
 * @class MenuScreen
 * @extends {ScreenBase}
 */
class BOMsReportsMenuScreen extends ScreenBase {
    constructor(params){
        super(params);
        
        this.bladeTitle(this.i18n("BOMs_Reports")());
        this.bladeCanMaximize = false;
        this.bladeMargin = BladeMargin.Narrow;
        this.selectedIndex = ko.observable();
        
        this._selectingRowHandler = (item) => {
            if (item) {
                return this.navigate(item.id);
            }
            return false;
        };

        this.items = ko.observableArray([]);

    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {

        if(WebConfig.userSession.hasPermission(Constants.Permission.BOMsReportsSummaryRoundTrip)){
            this.items.push({
                text: this.i18n("BOMs_Reports_Summary_Round_Trip")(),
                id: 'cw-boms-reports-summary-round-trip-form'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.BOMsReportsRealtimePassengerOnBoard)){
            this.items.push({
                text: this.i18n("BOMs_Reports_Summary_Realtime_Passenger_On_Board")(),
                id: 'cw-boms-reports-realtime-passenger-on-board-form'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.BOMsReportsSummaryPassengerByRoute)){
            this.items.push({
                text: this.i18n("BOMs_Reports_Summary_Passenger_By_Route")(),
                id: 'cw-boms-reports-summary-passenger-by-route-form'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.BOMsReportsSummaryPassengerByZone)){
            this.items.push({
                text: this.i18n("BOMs_Reports_Summary_Passenger_By_Zone")(),
                id: 'cw-boms-reports-summary-passenger-by-zone-form'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.BOMsReportsBusUsageStatistics)){
            this.items.push({
                text: this.i18n("BOMs_Reports_Bus_Usage_Statistics")(),
                id: 'cw-boms-reports-bus-usage-statistics-form'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.BOMsReportsSummaryTransportation)){
            this.items.push({
                text: this.i18n("BOMs_Reports_Summary_Transportation")(),
                id: 'cw-boms-reports-summary-transportation-form'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.BOMsReportsMonthlySummartTransportation)){
            this.items.push({
                text: this.i18n("BOMs_Reports_Monthly_Summary_Transportation")(),
                id: 'cw-boms-reports-monthly-summary-transportation-form'
            });
        }

        // if(WebConfig.userSession.hasPermission(Constants.Permission.BOMsReportsMonthlySummartTransportation)){
            this.items.push({
                text: this.i18n("BOMs_Reports_Summary_Passenger_By_Zone_And_Vehicle_Type")(),
                id: 'cw-boms-reports-summary-passenger-by-zone-and-vehicle-type-form'
            });
        // }
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedIndex(null);
    }
    /**
     * Navigate to inner screens.
     * @param {any} extras
     */
    goto (extras){
        // Passing report names to next page.
        //  this.navigate("bo-report-and-group-ovserspeed", {reportSource: extras});
        this.navigate(extras.id,extras.name);
    }
}

export default {
    viewModel: ScreenBase.createFactory(BOMsReportsMenuScreen),
    template: templateMarkup
};

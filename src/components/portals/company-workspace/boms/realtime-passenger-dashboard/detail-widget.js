﻿import ko from "knockout";
import ScreenBaseWidget from "../../../screenbasewidget";
import templateMarkup from "text!./detail-widget.html";
import WebRequestBOMsDefaultValues from "../../../../../app/frameworks/data/apitrackingcore/webRequestBOMsDefaultValues";

class BOMsRealtimePassengerDashboardDetailWidget extends ScreenBaseWidget {
    constructor(params) {
        super(params);
        this.columns = ko.observableArray([]);
        this.data = ko.observableArray([]);
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
 
        if (!isFirstLoad) {
            return;
        }

        this.columns(this.generateColumn());

    }

    /**
     * Get WebRequestRealtimePassenger
     * @readonly
     */
    get webRequestBOMsDefaultValues() {
        return WebRequestBOMsDefaultValues.getInstance();
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }

    /**
     * Generate Column
     * 
     * @param {any} res
     * @returns
     */
    generateColumn() {
        let columns = [{
                type: "text",
                title: this.i18n('BOMs_Reports_Plant')(),
                data: "businessUnitName",
                width: "80px"
            },
            {
                type: "text",
                title: this.i18n('BOMs_Reports_ShiftMode')(),
                data: "jobCategoryDisplayName",
                width: "150px"
            },
            {
                type: "text",
                title: this.i18n('BOMsDefaultValue_Late')(),
                data: "late",
                width: "90px"
            },
            {
                type: "text",
                title: this.i18n('BOMsDefaultValue_MaybeLate')(),
                data: "mayBeLate",
                width: "100px",
                template: (data) => {
                    return `${data.mayBeLate} ${this.i18n('BOMs_DefaultValue_To')()} ${data.late}`
                }
            },
        ];
        return columns;
    }

    /**
     * On Datasource Request Read
     * 
     * @param {any} res
     * @returns
     */
    onDatasourceRequestRead(gridOption) {
        var dfd = $.Deferred();
        
        this.webRequestBOMsDefaultValues.listBOMsDefaultValues(gridOption).done((data) => {
             dfd.resolve({
                items: data["items"],
                totalRecords: data["totalRecords"],
                currentPage: (data["currentPage"]) ? data["currentPage"] : 1
            });
        }).fail(err => {
            console.log(err);
        });
        return dfd;
    }
}

export default {
    viewModel: ScreenBaseWidget.createFactory(BOMsRealtimePassengerDashboardDetailWidget),
    template: templateMarkup
};
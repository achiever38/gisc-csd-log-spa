import ko from "knockout";
import templateMarkup from "text!./search-result.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import GenerateDataGridExpand from "../../generateDataGridExpand";
import WebRequestRealtimePassenger from "../../../../../app/frameworks/data/apitrackingcore/WebRequestRealtimePassenger";
import WebRequestShipment from "../../../../../app/frameworks/data/apitrackingcore/webRequestShipment";
import AssetUtility from "../../asset-utility";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";
import AssetInfo from "../../asset-info";
import Utility from "../../../../../app/frameworks/core/utility";
import * as EventAggregator from "../../../../../app/frameworks/constant/eventAggregator";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";

/**
 * Display available subscription menu based on user permission.
 * @class MenuScreen
 * @extends {ScreenBase}
 */
class BOMsRealtimePassengerDashboardSearchResult extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("BOMs_Realtime_Passenger_Dashboard")());
        this.bladeIsMaximized = true;

        this.params = this.ensureNonObservable(params);
        this.filter = ko.observable();
        this.filter({
            businessUnitId: this.params.businessUnitId,
            travelMode: this.params.travelMode,
            shift: this.params.shiftMode,
            keyword: this.params.keyword,
        });

        this.statusDetail = ko.observable();
        this.vehicleTxt = ko.observable(this.i18n('BOMs_Realtime_Passenger_Dashboard_Vehicle')());
        this.employeeTxt = ko.observable(this.i18n('BOMs_Realtime_Passenger_Dashboard_Employee')());

        this.onDetailClickColumns = ko.observableArray([]);
        this.onDetailClickColumns(
            GenerateDataGridExpand.getOnClickColumns(this.generateColumnDetail())
        );

        this.pageSizeOptions = ko.observableArray([10, 20, 50, 100]);

        this.renderStatusDisplay = (data) => {
            let statusDisplayName = (data.statusDisplay) ? data.statusDisplay : "";
            if (data != null || data != undefined) {
                var node = '<div style="color:' + data.statusColour + ';">' + statusDisplayName + '</div>';

                return node;
            } else {
                return "";
            }
        };

        this.renderActualStartTime = (data) => {
            let actualStartTimeDisplayName = (data.actualStartTime) ? data.actualStartTime : "";
            if (data != null || data != undefined) {
                var node = '<div style="color:' + data.actualStartColour + ';">' + actualStartTimeDisplayName + '</div>';

                return node;
            } else {
                return "";
            }
        };

        this.renderActualEndTime = (data) => {
            let actualEndTimeDisplayName = (data.actualEndTime) ? data.actualEndTime : "";
            if (data != null || data != undefined) {
                var node = '<div style="color:' + data.actualFinishColour + ';">' + actualEndTimeDisplayName + '</div>';

                return node;
            } else {
                return "";
            }
        };

        this.clickViewOnMap = (data) => { 
            this.isBusy(true);
            var viewOnMapQuery = this.webRequestShipment.getDataViewOnMap(data.id);
            $.when(viewOnMapQuery).done(res => {
                this.isBusy(false);
                this.prepareViewOnMapData(res);
            });
        };

        //Map Subscribe
        this._eventAggregator.subscribe(
            EventAggregator.MAP_COMMAND_COMPLETE,
            info => {
                switch (info.command) {
                    case "open-shipment-legend":
                        this.minimizeAll();
                        break;
                    default:
                        break;
                }
            }
          );


    }

    /**
     * Get WebRequestRealtimePassenger
     * @readonly
     */
    get webRequestRealtimePassenger() {
        return WebRequestRealtimePassenger.getInstance();
    }

    /**
     * Get WebRequestShipment
     * @readonly
     */
    get webRequestShipment() {
        return WebRequestShipment.getInstance();
    }


    onDatasourceRequestRead(gridOption) {
        var dfd = $.Deferred();
        var filter = Object.assign({}, this.filter(), gridOption);
        
        if (this.filter().keyword != null || this.filter().keyword != undefined) {
            this.webRequestRealtimePassenger.listVehicle(filter).done((data) => {

                let countAllVehicle = (_.size(data["items"])) ? data["items"][0].countAllVehicle : 0 ;
                let countAllEmployee = (_.size(data["items"])) ? data["items"][0].countAllEmployee : 0 ;

                if(_.size(data["items"])){
                    data["items"].shift();
                }

                let statusVehicleDetail = Utility.stringFormat(this.vehicleTxt(), countAllVehicle);
                let statusEmpDetail = Utility.stringFormat(this.employeeTxt(), countAllEmployee);
                this.statusDetail(`( ${statusVehicleDetail} / ${statusEmpDetail} )`);
                dfd.resolve({
                    items: data["items"],
                    totalRecords: data["totalRecords"],
                    currentPage: data["currentPage"]
                });
            }).fail(err => {
                console.log(err);
            });
        } else {
            dfd.resolve({
                items: [],
                totalRecords: 1,
                currentPage: 1
            });
        }



        return dfd;
    }

    generateColumnDetail() {
        var self = this;
        var columnsDetail = [{
                type: "text",
                width: "60px",
                title: this.i18n('BOMs_Realtime_Passenger_Dashboard_No')(),
                data: "rowNo",
                className: "dg-body-right"
            },
            {
                type: "text",
                width: "",
                title: this.i18n('BOMs_Realtime_Passenger_Dashboard_EmployeeNo')(),
                data: "employeeId"
            },
            {
                type: "text",
                width: "",
                title: this.i18n('BOMs_Realtime_Passenger_Dashboard_FullName')(),
                data: "fullName"
            },
            {
                type: "text",
                title: this.i18n('BOMs_Realtime_Passenger_Dashboard_Department')(),
                data: "divisionName"
            },
            {
                type: "text",
                title: this.i18n('BOMs_Realtime_Passenger_Dashboard_BusinessUnit')(),
                data: "departmentName"
            },
            {
                type: "text",
                title: this.i18n('BOMs_Realtime_Passenger_Dashboard_ProductionLine')(),
                data: "lineName"
            },
            {
                type: "text",
                title: this.i18n('BOMs_Realtime_Passenger_Dashboard_Register')(),
                data: "reserve"
            },
            {
                type: "text",
                title: this.i18n('BOMs_Realtime_Passenger_Dashboard_WaypointStart')(),
                data: "locationSwipe"
            },
            {
                type: "text",
                title: this.i18n('BOMs_Realtime_Passenger_Dashboard_SwipingCardTime')(),
                data: "dateSwipeFormat"
            }
        ];
        return columnsDetail;
    }

    onDetailInit(evt) {

        var self = this;
        var empId = evt.data.id;
        console.log(evt);

        this.webRequestRealtimePassenger.listEmployee(empId)
            .done(response => {
                console.log(response);
                //self.formatDetailData(response);
                $("<div id='" + self.id + empId + "'/>")
                    .appendTo(evt.detailCell)
                    .kendoGrid({
                        dataSource: response.items,
                        //dataSource: data.Items,
                        noRecords: {
                            template: self.i18n("M111")()
                        },
                        pageable: GenerateDataGridExpand.getPageable(false),
                        // set column of kendo grid
                        columns: GenerateDataGridExpand.getColumns(
                            self.generateColumnDetail()
                        )
                    });
            })
            .fail(e => {
                this.handleError(e);
            })
            .always(() => {
                this.isBusy(false);
            });
    }

    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdReport", this.i18n("Common_Export")(), "svg-cmd-export"));
    }

    onCommandClick(sender) {

        switch (sender.id) {
            case "cmdReport":
                this.webRequestRealtimePassenger.exportFile(this.filter())
                .done(response => {
                    ScreenHelper.downloadFile(response.fileUrl);
                });
                break;
        }
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        MapManager.getInstance().closeViewOnMap();
        MapManager.getInstance().showAllTrackVehicle();
    }

    prepareViewOnMapData(result) {

        result.driverName = result.driverName.trim();

        var viewonmapData = {
            panel: {
                jobName: _.isNil(result.jobName) ? "-" : result.jobName,
                driverName: _.isNil(result.driverName) ? "-" : result.driverName,
                jobCode: _.isNil(result.jobCode) ? "-" : result.jobCode,
                jobStatusDisplayName: _.isNil(result.jobStatusDisplayName) ? "-" : result.jobStatusDisplayName,
                vehicleLicense: _.isNil(result.vehicleLicense) ? "-" : result.vehicleLicense,
            },
            actual: {
                pin: [],
                route: []
            },
            plan: {
                pin: [],
                route: []
            },
            waiting: {
                pin: [],
                route: []
            },
            vehicle: []
        };

        _.forEach(result.shipmentPlanInfos, (planInfo) => {
            var pinItem = {
                longitude: planInfo.longitude,
                latitude: planInfo.latitude,
                attributes: {
                    jobWaypointStatus: planInfo.jobWaypointStatus,
                    jobWaypointName: planInfo.jobWaypointName,
                    info: [
                        {
                            "title": this.i18n("waypoint-info-status")(),
                            "key": "jobWaypointStatusDisplayName",
                            "text": planInfo.jobWaypointStatusDisplayName || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-planIncomingDate")(),
                            "key": "formatPlanIncomingDate",
                            "text": planInfo.formatPlanIncomingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-actualIncomingDate")(),
                            "key": "formatActualIncomingDate",
                            "text": planInfo.formatActualIncomingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-actualOutgoingDate")(),
                            "key": "formatActualOutgoingDate",
                            "text": planInfo.formatActualOutgoingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-remark")(),
                            "key": "jobDescription",
                            "text": planInfo.jobDescription || '-'
                        }
                    ]
                }
            };
            viewonmapData.plan.pin.push(pinItem);
            viewonmapData.plan.route = viewonmapData.plan.route.concat(planInfo.shipmentLocations);
        });

        _.forEach(result.shipmentActualInfos, (actualInfo) => {
            var pinItem = {
                longitude: actualInfo.longitude,
                latitude: actualInfo.latitude,
                attributes: {
                    jobWaypointStatus: actualInfo.jobWaypointStatus,
                    jobWaypointName: actualInfo.jobWaypointName,
                    info: [
                        {
                            "title": this.i18n("waypoint-info-status")(),
                            "key": "jobWaypointStatusDisplayName",
                            "text": actualInfo.jobWaypointStatusDisplayName || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-planIncomingDate")(),
                            "key": "formatPlanIncomingDate",
                            "text": actualInfo.formatPlanIncomingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-actualIncomingDate")(),
                            "key": "formatActualIncomingDate",
                            "text": actualInfo.formatActualIncomingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-actualOutgoingDate")(),
                            "key": "formatActualOutgoingDate",
                            "text": actualInfo.formatActualOutgoingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-remark")(),
                            "key": "jobDescription",
                            "text": actualInfo.jobDescription || '-'
                        }
                    ]
                }
            };
         //   console.log("viewonmapData.actual.route",viewonmapData.actual.route);
            viewonmapData.actual.pin.push(pinItem);
            viewonmapData.actual.route = viewonmapData.actual.route.concat(actualInfo.shipmentActualRouteInfos);
        });

        //waitingLocataionInfos
        if(_.size(result.waitingLocataionInfos.items)){
            

            var route = new Array();
            let arrSize = _.size(result.waitingLocataionInfos.items);
            let beginLat = result.waitingLocataionInfos.items[0].latitude;
            let beginLon = result.waitingLocataionInfos.items[0].longitude;
            let beginAttributes = this.generateAttributes(result.waitingLocataionInfos.items[0]);
            let endLat = result.waitingLocataionInfos.items[arrSize-1].latitude;
            let endLon = result.waitingLocataionInfos.items[arrSize-1].longitude;
            let endAttributes = this.generateAttributes(result.waitingLocataionInfos.items[arrSize-1]);

            _.forEach(result.waitingLocataionInfos.items, (item) => {
                route.push({
                    lat: item.latitude,
                    lon: item.longitude,
                    attributes: this.generateAttributes(item),
                    direction: item.direction,
                    movement: item.movement
                });
            });

            viewonmapData.waiting.pin = [
                {
                    latitude: beginLat,
                    longitude: beginLon,
                    attributes: beginAttributes,
                },
                {
                    latitude: endLat,
                    longitude: endLon,
                    attributes: endAttributes,
                }
            ];
            viewonmapData.waiting.route = route;
            
        }

        
        

        var directionConst = ["N", "NE", "E", "SE", "S", "SW", "W", "NW"];
        _.forEach(result.locationInfo, (locationInfo) => {

            AssetInfo.getStatuses(locationInfo,WebConfig.userSession);
            var locationInfoStatus = AssetInfo.getLocationInfoStatus(locationInfo);
            var shipmentInfo = AssetInfo.getShipmentInfo(locationInfo, locationInfoStatus);

            locationInfo.vehicleLicense = _.isNil(locationInfo.vehicleLicense) ? "-" : locationInfo.vehicleLicense;
            
            var vehicleItem = {
                longitude: locationInfo.longitude,
                latitude: locationInfo.latitude,
                attributes: {
                    "TITLE": locationInfo.vehicleLicense,
                    "SHIPMENTINFO": shipmentInfo
                }
            };

            var vehicleIcon = result.vehicleIconPath[locationInfo.vehicleIconId];
            var movementImageUrl = "";
            _.forEach(result.vehicleIconPath[locationInfo.vehicleIconId], (vehicleIcon) => {
                if (vehicleIcon.movementType == locationInfo.movement) {
                    movementImageUrl = vehicleIcon.imageUrl + "/" + directionConst[locationInfo.directionType - 1] + ".png"
                }
            });
            vehicleItem.movementImageUrl = Utility.resolveUrl(movementImageUrl);
            viewonmapData.vehicle.push(vehicleItem);
        });

        MapManager.getInstance().clear();
        MapManager.getInstance().openShipmentLegend({ data: viewonmapData });
    }

    /**
     * Create attributes object.
     * 
     * @param {any} data
     * @returns
     * 
     * @memberOf PlaybackTimelineScreen
     */
    generateAttributes(data) {
        var isVehicle = data.vehicleId ? true : false;
        var datID = isVehicle ? "V-" + data.vehicleId + "-PB" : "D-" + data.deliveryManId + "-PB";
        var title = isVehicle ? data.vehicleLicense : data.username;
        var businessUnitName = "";

        // Detect business unit name based on vehicle or delivery men.
        if (isVehicle) {
            businessUnitName = _.map(_.filter(this.selectedVehicles, {
                id: data.vehicleId
            }), "businessUnitName");
        } else {
            businessUnitName = _.map(_.filter(this.selectedDeliveryMen, {
                id: data.deliveryManId
            }), "businessUnitName");
        }

        // Compose map attribute object.
        var attr = {
            ID: datID,
            TITLE: title,
            BOX_ID: data.vehicleId ? data.boxSerialNo : data.email,
            DRIVER_NAME: data.driverName,
            DEPT: businessUnitName,
            DATE: data.formatDateTime,
            TIME: Utility.getOnlyTime(data.dateTime, "HH:mm"),
            PARK_TIME: data.parkDuration,
            PARK_IDLE_TIME: data.idleTime,
            LOCATION: data.location,
            RAW: data,
            STATUS: AssetInfo.getStatuses(data, WebConfig.userSession),
            IS_DELAY: data.isDelay
        };

        attr.SHIPMENTINFO = AssetInfo.getShipmentInfo(data, attr.STATUS);

        return attr;
    }

}

export default {
    viewModel: ScreenBase.createFactory(BOMsRealtimePassengerDashboardSearchResult),
    template: templateMarkup
};
import ko from "knockout";
import templateMarkup from "text!./form.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import { Enums } from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestShipmentCategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestShipmentCategory"

class BOMsRealtimePassengerDashboardForm extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeSize = BladeSize.Small;
        this.bladeTitle(this.i18n('Report_Filter')());

        this.plant = ko.observableArray([]);
        this.selectedPlant = ko.observable();
        this.travelMode = ko.observableArray([]);
        this.selectedTravelMode = ko.observable();
        this.shiftMode = ko.observableArray([]);
        this.selectedShiftMode = ko.observable();

        this.selectedPlant.subscribe((ids)=>{
            this.shiftMode([]);
            if(_.size(ids)){
                this.isBusy(true);
                this.webRequestShipmentCategory.listShipmentCategory({
                    businessUnitIds: ids,
                    enable: true
                }).done((res)=>{
                    this.isBusy(false);
                    var i = 1;
                    _.forEach(res.items, (item)=>{
                        item.parent = 0;
                    });
                    res.items.unshift({id: 0, name: this.i18n("Common_CheckAll")(), parent: "#"});
                    this.shiftMode(res.items);
                });
            }
        });

        
    }

    setupExtend() {

    }

    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * Get WebRequest specific for BusinessUnit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
     * Get WebRequest specific for Shipment Category module in Web API access.
     * @readonly
     */
    get webRequestShipmentCategory() {
        return WebRequestShipmentCategory.getInstance();
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        
        var dfd = $.Deferred();
        let businessUnitFilter = {
            companyId: WebConfig.userSession.currentCompanyId
        }
        
        let travelModeFilter = { 
            companyId: WebConfig.userSession.currentCompanyId, 
            types: Enums.ModelData.EnumResourceType.TravelMode,
            sortingColumns: DefaultSorting.EnumResource
        }
        
        let d1 = this.webRequestBusinessUnit.listBusinessUnitParentSummary(businessUnitFilter);
        let d2 = this.webRequestEnumResource.listEnumResource(travelModeFilter);
        $.when(d1, d2).done((r1, r2) => {
            this.plant(r1.items);
            this.travelMode(r2.items);
            dfd.resolve();
        });

        return dfd;

    }

    buildActionBar(actions){
        actions.push(this.createAction("actPreview", this.i18n('Report_Preview')()));
    }

    onActionClick(sender) {

        if(sender.id === "actPreview") {
            var BUIds = (_.size(this.selectedPlant())) ? this.selectedPlant() : [0];
            var travelModeId = (_.size(this.selectedTravelMode())) ? this.selectedTravelMode().value : 0;
            var shiftModeId = (_.size(this.selectedShiftMode())) ? this.selectedShiftMode() : [0];
            var listBUDisplayName = new Array();
            var listTravelModeDisplayName = new Array();
            let businessUnitDisplayName = null;
            let travelModeDisplayName = null;
            let shiftModeDisplayName = null;

            let listBU = _.filter(this.plant(), function(o) { return o.id; });
            _.forEach(listBU, (item)=>{
                listBUDisplayName.push(item.name);
            });

            // let listTravelMode = _.filter(this.travelMode(), function(o) { return o.id; });
            _.forEach(this.travelMode(), (item)=>{
                listTravelModeDisplayName.push(item.displayName);
            });
            
            businessUnitDisplayName = (_.size(listBUDisplayName)) ? _.join(listBUDisplayName, ', ') : null;
            travelModeDisplayName = (_.size(listTravelModeDisplayName)) ? _.join(listTravelModeDisplayName, ', ') : null;
            shiftModeDisplayName = null;

            var filter = {
                businessUnitIds : BUIds,
                travelModeIds : travelModeId,
                shiftModeIds : shiftModeId,
                businessUnitDisplayName: businessUnitDisplayName,
                travelModeDisplayName: travelModeDisplayName,
                shiftModeDisplayName: shiftModeDisplayName,
            }
            this.navigate("cw-boms-realtime-passenger-dashboard-list", filter);
        } 
    }
}

export default {
    viewModel: ScreenBase.createFactory(BOMsRealtimePassengerDashboardForm),
    template: templateMarkup
};
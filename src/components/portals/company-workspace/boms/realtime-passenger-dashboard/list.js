import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import * as BladeMargin from "../../../../../app/frameworks/constant/bladeMargin";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import ScreenHelper from "../../../screenhelper";
import GenerateDataGridExpand from "../../generateDataGridExpand";
import WebRequestRealtimePassenger from "../../../../../app/frameworks/data/apitrackingcore/WebRequestRealtimePassenger";
import { Constants, Enums } from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestShipment from "../../../../../app/frameworks/data/apitrackingcore/webRequestShipment";
import AssetUtility from "../../asset-utility";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";
import AssetInfo from "../../asset-info";
import Utility from "../../../../../app/frameworks/core/utility";
import * as EventAggregator from "../../../../../app/frameworks/constant/eventAggregator";

/**
 * Display available subscription menu based on user permission.
 * @class MenuScreen
 * @extends {ScreenBase}
 */
class BOMsRealtimePassengerDashboardList extends ScreenBase {
    constructor(params) {
        super(params);

        this.dataResult = this.ensureNonObservable(params);
        this.businessUnitIds = this.ensureObservable(this.dataResult.businessUnitIds, 0);
        this.travelModeIds = this.ensureObservable(this.dataResult.travelModeIds, 0);
        this.shiftModeIds = this.ensureObservable(this.dataResult.shiftModeIds, 0);

        this.plantDisplayName = this.ensureObservable(this.dataResult.businessUnitDisplayName);
        this.travelModeDisplayName = this.ensureObservable(this.dataResult.travelModeDisplayName);
        this.shiftModeDisplayName = this.ensureObservable(this.dataResult.shiftModeDisplayName);
        
        this.bladeTitle(this.i18n("BOMs_Realtime_Passenger_Dashboard")());
        this.bladeIsMaximized = true;
        this.numberOfBusTxt = ko.observable(this.i18n('BOMs_Realtime_Passenger_Dashboard_Legend_Txt')());

        this.items = ko.observableArray([]);
        this.driverRule = ko.observableArray([]);

        this.isAutoRefresh = ko.observable(true);
        //minute WebConfig.appSettings.realTimePassengerAutoRefreshTime
        this.autoRefreshTime = this.ensureNonObservable((1000 * 60) * 5);
        this.timeOutHandler = null;
        this.dashBoardHeadTxt = ko.observable(this.i18n('BOMs_Realtime_Passenger_Dashboard_Today_Statistic')());
        this.autoRefreshTxt = ko.observable(this.i18n('Common_Auto_Refresh')());
        this.lastSyncTxt = ko.observable(this.i18n('Common_LastSynce')());
        this.lastSync = ko.observable("01/09/2018 06:50:00");
        this.lastSyncDisplay = ko.observable();
        this.status = ko.observable(1);
        this.statusDisplayName =  ko.observable(null);
        this.statusDetail = ko.observable();
        
        this.vehicleTxt = ko.observable(this.i18n('BOMs_Realtime_Passenger_Dashboard_Vehicle')());
        this.employeeTxt = ko.observable(this.i18n('BOMs_Realtime_Passenger_Dashboard_Employee')());

        this.iconDetail = this.resolveUrl("~/images/ic-info.svg");

        this.realtimePassengerStatus = ko.observable(Enums.ModelData.RealtimePassengerStatus.Late);
        this.jobStatus = ko.observable();

        /////////////// Bus ///////////////////////////////

        // this.busLateNum = ko.observable(0);
        // this.busLateTxt = ko.observable(this.i18n('BOMs_Realtime_Passenger_Dashboard_Late')());
        // this.busMaybeLateNum = ko.observable(0);
        // this.busMaybeLateTxt = ko.observable(this.i18n('BOMs_Realtime_Passenger_Dashboard_Maybe_Late')());
        // this.busNotTravelNum = ko.observable(0);
        // this.busNotTravelTxt = ko.observable(this.i18n('BOMs_Realtime_Passenger_Dashboard_Not_Travel')());
        // this.busOnTheWayNum = ko.observable(0);
        // this.busOnTheWayTxt = ko.observable(this.i18n('BOMs_Realtime_Passenger_Dashboard_On_The_Way')());
        // this.busWaitForFactoryNum = ko.observable(0);
        // this.busWaitForFactoryTxt = ko.observable(this.i18n('BOMs_Realtime_Passenger_Dashboard_Wait_For_Factory')());
        // this.busOnFactoryNum = ko.observable(0);
        // this.busOnFactoryTxt = ko.observable(this.i18n('BOMs_Realtime_Passenger_Dashboard_Finish')());
        // this.busAllNum = ko.observable(0);
        // this.busAllTxt = ko.observable(this.i18n('BOMs_Realtime_Passenger_Dashboard_All')());
        this.busLateNum = ko.observable(0);
        this.busLateTxt = ko.observable("-");
        this.busMaybeLateNum = ko.observable(0);
        this.busMaybeLateTxt = ko.observable("-");
        this.busNotTravelNum = ko.observable(0);
        this.busNotTravelTxt = ko.observable("-");
        this.busOnTheWayNum = ko.observable(0);
        this.busOnTheWayTxt = ko.observable("-");
        this.busWaitForFactoryNum = ko.observable(0);
        this.busWaitForFactoryTxt = ko.observable("-");
        this.busOnFactoryNum = ko.observable(0);
        this.busOnFactoryTxt = ko.observable("-");
        this.busAllNum = ko.observable(0);
        this.busAllTxt = ko.observable("-");

        //////////////// User ///////////////////////////////
        this.userLateNum = ko.observable(0);
        this.userMaybeLateNum = ko.observable(0);
        this.userNotTravelNum = ko.observable(0);
        this.userOnTheWayNum = ko.observable(0);
        this.userWaitForFactoryNum = ko.observable(0);
        this.userOnFactoryNum = ko.observable(0);
        this.userAllNum = ko.observable(0);


        ////////////////// Box Color //////////////////////////
        this.lateBoxColor = ko.observable('#E53935');
        this.maybeLateBoxColor = ko.observable('#FF8F00');
        this.notTravelBoxColor = ko.observable('#a6a6a6');
        this.onTheWayBoxColor = ko.observable('#33CCFF');
        this.waitForFactoryBoxColor = ko.observable('#0054d0');
        this.onFactoryBoxColor = ko.observable('#00AA8D');
        this.allBoxColor = ko.observable('#f2f2f2');

        /// FILTER ///
        this.filter = {
            businessUnitId: _.toString(this.businessUnitIds()),
            travelMode: _.toString(this.travelModeIds()),
            shift: _.toString(this.shiftModeIds()),
            realtimePassengerStatus: Enums.ModelData.RealtimePassengerStatus.Late
        };

        //////////////////////////////
        this.subscribeMessage("toyota-realtime-search", info => {
            // this.filter(info);
            // console.log("info", info);
        });

        this.onDetailClickColumns = ko.observableArray([]);
        this.onDetailClickColumns(
            GenerateDataGridExpand.getOnClickColumns(this.generateColumnDetail())
        );

        this.pageSizeOptions = ko.observableArray([10, 20, 50, 100]);

        ////// title table /////
        this.titleNo = ko.observable(this.i18n('BOMs_Realtime_Passenger_Dashboard_No')());
        this.titleJobNo = ko.observable(this.i18n('BOMs_Realtime_Passenger_Dashboard_JobNo')());
        this.titleLicense = ko.observable(this.i18n('BOMs_Realtime_Passenger_Dashboard_License')());
        this.titleNumDriver = ko.observable(this.i18n('BOMs_Realtime_Passenger_Dashboard_Num_Driver')());
        this.titleStatus = ko.observable(this.i18n('BOMs_Realtime_Passenger_Dashboard_Status')());
        this.titleStatusInVehicle = ko.observable(this.i18n('BOMs_Realtime_Passenger_Dashboard_Status_In_Vehicle')());
        this.titleInWaypoint = ko.observable(this.i18n('BOMs_Realtime_Passenger_Dashboard_In_Waypoint')());
        this.titleDistance = ko.observable(this.i18n('BOMs_Realtime_Passenger_Dashboard_Distance')());
        this.titleEstmOutgDate = ko.observable(this.i18n('BOMs_Realtime_Passenger_Dashboard_EstmOutgDate')());
        this.titleStartDateTime = ko.observable(this.i18n('BOMs_Realtime_Passenger_Dashboard_StartDateTime')());
        this.titlePlanFinish = ko.observable(this.i18n('BOMs_Realtime_Passenger_Dashboard_PlanFinish')());
        this.titleActFinished = ko.observable(this.i18n('BOMs_Realtime_Passenger_Dashboard_ActFinished')());
        this.titleLocDesc = ko.observable(this.i18n('BOMs_Realtime_Passenger_Dashboard_LocDesc')());
        

        this.isAutoRefresh.subscribe(isChecked => {
            this.onAutoRefresh(isChecked);
        });


        this.apiDataSource = ko.observableArray([]);
        this.busImgUrl = ko.observable();
        this.passengerImgUrl = ko.observable();

        
        this.busImgUrl("../../../../../images/ic_bus.png");
        this.passengerImgUrl("../../../../../images/ic_passenger.png");

        this.renderStatusDisplay = (data) => {
            let statusDisplayName = (data.statusDisplay) ? data.statusDisplay : "";
            if (data != null || data != undefined) {
                var node = '<div style="color:' + data.statusColour + ';">' + statusDisplayName + '</div>';

                return node;
            } else {
                return "";
            }
        };

        this.renderActualStartTime = (data) => {
            let actualStartTimeDisplayName = (data.actualStartTime) ? data.actualStartTime : "";
            if (data != null || data != undefined) {
                var node = '<div style="color:' + data.actualStartColour + ';">' + actualStartTimeDisplayName + '</div>';

                return node;
            } else {
                return "";
            }
        };

        this.renderActualEndTime = (data) => {
            let actualEndTimeDisplayName = (data.actualEndTime) ? data.actualEndTime : "";
            if (data != null || data != undefined) {
                var node = '<div style="color:' + data.actualFinishColour + ';">' + actualEndTimeDisplayName + '</div>';

                return node;
            } else {
                return "";
            }
        };

        this.clickViewOnMap = (data) => { 
            this.isBusy(true);
            var viewOnMapQuery = this.webRequestShipment.getDataViewOnMap(data.id);
            $.when(viewOnMapQuery).done(res => {
                this.isBusy(false);
                this.prepareViewOnMapData(res);
            });
        };

        //Map Subscribe
        this._eventAggregator.subscribe(
            EventAggregator.MAP_COMMAND_COMPLETE,
            info => {
                switch (info.command) {
                    case "open-shipment-legend":
                        this.minimizeAll();
                        break;
                    default:
                        break;
                }
            }
          );

    }

    /**
     * Get WebRequestRealtimePassenger
     * @readonly
     */
    get webRequestRealtimePassenger() {
        return WebRequestRealtimePassenger.getInstance();
    }

    /**
     * Get WebRequestShipment
     * @readonly
     */
    get webRequestShipment() {
        return WebRequestShipment.getInstance();
    }
    

    onDomReady(){
        $(`#lateBox`).css('background-color', `#e53935`); //Set Defalt Highlight on latebox
        // $(".k-header.k-grid-toolbar").hide();
    }

    onClickIconDetail(){
        this.widget("cw-boms-realtime-passenger-dashboard-detail-widget", null, {
            id: 'realtimePassengerDashboardDetailWidget',
            title: "",
            modal: false,
            resizable: false,
            target: "cw-boms-realtime-passenger-dashboard-detail-widget",
            width: "575",
            height: "213",
            left: "30%",
            bottom: "20%"
        });
    }

    onClickBox(boxName, boxId, data, e) {

        $('#lateBox').css('background-color', '');
        $('#maybelateBox').css('background-color', '');
        $('#notTravelBox').css('background-color', '');
        $('#onTheWayBox').css('background-color', '');
        $('#waitForFactoryBox').css('background-color', '');
        $('#finish').css('background-color', '');
        $('#allBox').css('background-color', '');
        var borderColor = "#e53935";


        if(boxId == 1){
            borderColor = "#e53935";
            this.realtimePassengerStatus(Enums.ModelData.RealtimePassengerStatus.Late);
            this.jobStatus(null);
        }else if(boxId == 2){
            borderColor = "#ff8f00";
            this.realtimePassengerStatus(Enums.ModelData.RealtimePassengerStatus.MaybeLate);
            this.jobStatus(null);
        }else if(boxId == 3){
            borderColor = "#a6a6a6";
            this.realtimePassengerStatus(null);
            this.jobStatus(Enums.JobStatus.Waiting);
        }else if(boxId == 4){
            borderColor = "#33ccff";
            this.realtimePassengerStatus(null);
            this.jobStatus(Enums.JobStatus.OnTheWay);
        }else if(boxId == 5){
            borderColor = "#0054d0";
            this.realtimePassengerStatus(null);
            this.jobStatus(Enums.JobStatus.GoingToTerminal);
        }else if(boxId == 6){
            borderColor = "#00aa8d";
            this.realtimePassengerStatus(null);
            this.jobStatus(Enums.JobStatus.Finished);
        }else{
            borderColor = "#323b3a";
            this.realtimePassengerStatus(null);
            this.jobStatus(null);
        }

        // let boxIdName = boxName;
        $(`#${boxName}`).css('background-color', borderColor);
        
        if(this.businessUnitIds() == null || this.businessUnitIds() == undefined){
           return
        }

        this.filter.realtimePassengerStatus = this.realtimePassengerStatus();
        this.filter.jobStatus = this.jobStatus();

        // this.onAutoRefresh(true);
        this.status(boxId);
        this.setStatusDisplay(boxId);
        this.getDataDashboard();
        this.refreshShipmentList();
    }
    setStatusDisplay(boxId) {
        let boxStatusTxt;
        let busNum;
        let userNum;
        if (boxId == 1) {
            boxStatusTxt = this.busLateTxt();
            busNum = this.busLateNum();
            userNum = this.userLateNum();
        } else if (boxId == 2) {
            boxStatusTxt = this.busMaybeLateTxt();
            busNum = this.busMaybeLateNum();
            userNum = this.userMaybeLateNum();

        } else if (boxId == 3) {
            boxStatusTxt = this.busNotTravelTxt();
            busNum = this.busNotTravelNum();
            userNum = this.userNotTravelNum();

        } else if (boxId == 4) {
            boxStatusTxt = this.busOnTheWayTxt();
            busNum = this.busOnTheWayNum();
            userNum = this.userOnTheWayNum();
        } else if (boxId == 5) {
            boxStatusTxt = this.busWaitForFactoryTxt();
            busNum = this.busWaitForFactoryNum();
            userNum = this.userWaitForFactoryNum();
        } else if (boxId == 6) {
            boxStatusTxt = this.busOnFactoryTxt();
            busNum = this.busOnFactoryNum();
            userNum = this.userOnFactoryNum();
        } else if (boxId == 0) {
            boxStatusTxt = this.busAllTxt();
            busNum = this.busAllNum();
            userNum = this.userAllNum();
        }
        this.statusDisplayName(boxStatusTxt);
        let statusVehicleDetail = Utility.stringFormat(this.vehicleTxt(), busNum);
        let statusEmpDetail = Utility.stringFormat(this.employeeTxt(), userNum);
        this.statusDetail(`( ${statusVehicleDetail} / ${statusEmpDetail} )`);
        // this.statusDetail(`( ${this.vehicleTxt()} ${busNum} คัน / ${this.employeeTxt()} ${userNum} คน )`);
    }


    onDatasourceRequestRead(gridOption) {
        var dfd = $.Deferred();
        var filter = Object.assign({}, this.filter, gridOption);
        if(this.businessUnitIds() == null || this.businessUnitIds() == undefined){
            filter = {}
        }
        this.webRequestRealtimePassenger.listVehicle(filter).done((data) => {
            if(_.size(data["items"])){
                data["items"].shift();
            }
            dfd.resolve({
                items: data["items"],
                totalRecords: data["totalRecords"],
                currentPage: data["currentPage"]
            });
        }).fail(err => {
            console.log(err);
        });
        return dfd;
    }

    refreshShipmentList() {
        this.dispatchEvent("dgBOMsDashboard", "refresh");
    }


    generateColumnDetail() {
        var self = this;
        var columnsDetail = [{
                type: "text",
                width: "60px",
                title: this.i18n('BOMs_Realtime_Passenger_Dashboard_No')(),
                data: "rowNo",
                className: "dg-body-right"
            },
            {
                type: "text",
                width: "",
                title: this.i18n('BOMs_Realtime_Passenger_Dashboard_EmployeeNo')(),
                data: "employeeId"
            },
            {
                type: "text",
                width: "",
                title: this.i18n('BOMs_Realtime_Passenger_Dashboard_FullName')(),
                data: "fullName"
            },
            {
                type: "text",
                title: this.i18n('BOMs_Realtime_Passenger_Dashboard_Department')(),
                data: "divisionName"
            },
            {
                type: "text",
                title: this.i18n('BOMs_Realtime_Passenger_Dashboard_BusinessUnit')(),
                data: "departmentName"
            },
            {
                type: "text",
                title: this.i18n('BOMs_Realtime_Passenger_Dashboard_ProductionLine')(),
                data: "lineName"
            },
            {
                type: "text",
                title: this.i18n('BOMs_Realtime_Passenger_Dashboard_Register')(),
                data: "reserve"
            },
            {
                type: "text",
                title: this.i18n('BOMs_Realtime_Passenger_Dashboard_WaypointStart')(),
                data: "locationSwipe"
            },
            {
                type: "text",
                title: this.i18n('BOMs_Realtime_Passenger_Dashboard_SwipingCardTime')(),
                data: "dateSwipeFormat"
            }
        ];
        return columnsDetail;
    }

    onDetailInit(evt) {

        var self = this;
        var empId = evt.data.id;

        this.webRequestRealtimePassenger.listEmployee(empId)
            .done(response => {
                //self.formatDetailData(response);
                $("<div id='" + self.id + empId + "'/>")
                    .appendTo(evt.detailCell)
                    .kendoGrid({
                        dataSource: response.items,
                        //dataSource: data.Items,
                        noRecords: {
                            template: self.i18n("M111")()
                        },
                        pageable: GenerateDataGridExpand.getPageable(false),
                        // set column of kendo grid
                        columns: GenerateDataGridExpand.getColumns(
                            self.generateColumnDetail()
                        )
                    });
            })
            .fail(e => {
                this.handleError(e);
            })
            .always(() => {
                this.isBusy(false);
            });
    }

    buildCommandBar(commands) {
        // this.createCommand("actPreview", this.i18n('TE_Preview')());
        commands.push(this.createCommand("cmdSearch", this.i18n("Common_Search")(), "svg-cmd-search"));
        commands.push(this.createCommand("cmdExport", this.i18n("Common_Export")(), "svg-cmd-export"));
        commands.push(this.createCommand("cmdRefresh", this.i18n("Common_Refresh")(), "svg-cmd-refresh"));
        // commands.push(this.createCommand("cmdReport", this.i18n("Common_Export")(), "svg-cmd-add"));
    }

    onCommandClick(sender) {

        var filter = this.filter;

        switch (sender.id) {
            case "cmdSearch":
                this.navigate("cw-boms-realtime-passenger-dashboard-search", {
                    businessUnitIds: this.businessUnitIds(),
                    travelModeIds: this.travelModeIds(),
                    shiftModeIds: this.shiftModeIds(),
                    realtimePassengerStatus: this.filter.realtimePassengerStatus,
                    jobStatus: this.filter.jobStatus
                });
                break;
            case "cmdExport":

                if(this.businessUnitIds() == null || this.businessUnitIds() == undefined){
                    filter = {}
                 }

                this.webRequestRealtimePassenger.exportFile(filter)
                .done(response => {
                    ScreenHelper.downloadFile(response.fileUrl);
                });
                // this.createPolygon();
                break;
            case "cmdRefresh":
                this.refreshShipmentList();
                this.getDataDashboard();
                break;
        }
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        //set auto refresh by default
        this.getDataDashboard();
        this.onAutoRefresh(true);
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        clearInterval(this.timeOutHandler);
        MapManager.getInstance().closeViewOnMap();
        MapManager.getInstance().showAllTrackVehicle();

    }

    getDataDashboard(){
        var dfd = $.Deferred();
        let filter = _.clone(this.filter);

        if (this.businessUnitIds() == null) {
            filter = {}
        }
        filter.realtimePassengerStatus = null;
        filter.jobStatus = null;
        this.webRequestRealtimePassenger.listDashboard(filter).done((data) => {

        if (_.size(data.items)) {
            this.busLateNum(data.items[1].vehicle);
            this.userLateNum(data.items[1].employee);
            this.busLateTxt(data.items[1].statusDisplay);

            this.busMaybeLateNum(data.items[2].vehicle);
            this.userMaybeLateNum(data.items[2].employee);
            this.busMaybeLateTxt(data.items[2].statusDisplay);

            this.busNotTravelNum(data.items[3].vehicle);
            this.userNotTravelNum(data.items[3].employee);
            this.busNotTravelTxt(data.items[3].statusDisplay);

            this.busOnTheWayNum(data.items[4].vehicle);
            this.userOnTheWayNum(data.items[4].employee);
            this.busOnTheWayTxt(data.items[4].statusDisplay);

            this.busWaitForFactoryNum(data.items[5].vehicle);
            this.userWaitForFactoryNum(data.items[5].employee);
            this.busWaitForFactoryTxt(data.items[5].statusDisplay);

            this.busOnFactoryNum(data.items[6].vehicle);
            this.userOnFactoryNum(data.items[6].employee);
            this.busOnFactoryTxt(data.items[6].statusDisplay);

            this.busAllNum(data.items[7].vehicle);
            this.userAllNum(data.items[7].employee);
            this.busAllTxt(data.items[7].statusDisplay);
            
            this.lastSync(data.formatTimestamp);
            this.lastSyncDisplay(`(${this.lastSyncTxt()} ${this.lastSync()})`);
            this.setStatusDisplay(this.status()); //for set status display

            this.plantDisplayName(data.items[0].businessUnitName);
            this.travelModeDisplayName(data.items[0].travelMode);
            this.shiftModeDisplayName(data.items[0].shift);
        }
            dfd.resolve();
        }).fail(err => {
            console.log(err);
        });
        return dfd;
    }

    onAutoRefresh(checked) {
        if (checked) {
            this.timeOutHandler = setInterval(() => {
                this.refreshShipmentList();
                this.getDataDashboard();
            }, this.autoRefreshTime);
        } else {
            clearInterval(this.timeOutHandler);
        }

    }

    prepareViewOnMapData(result) {

        result.driverName = result.driverName.trim();

        var viewonmapData = {
            panel: {
                jobName: _.isNil(result.jobName) ? "-" : result.jobName,
                driverName: _.isNil(result.driverName) ? "-" : result.driverName,
                jobCode: _.isNil(result.jobCode) ? "-" : result.jobCode,
                jobStatusDisplayName: _.isNil(result.jobStatusDisplayName) ? "-" : result.jobStatusDisplayName,
                vehicleLicense: _.isNil(result.vehicleLicense) ? "-" : result.vehicleLicense,
            },
            actual: {
                pin: [],
                route: []
            },
            plan: {
                pin: [],
                route: []
            },
            waiting: {
                pin: [],
                route: []
            },
            vehicle: []
        };

        _.forEach(result.shipmentPlanInfos, (planInfo) => {
            var pinItem = {
                longitude: planInfo.longitude,
                latitude: planInfo.latitude,
                attributes: {
                    jobWaypointStatus: planInfo.jobWaypointStatus,
                    jobWaypointName: planInfo.jobWaypointName,
                    info: [
                        {
                            "title": this.i18n("waypoint-info-status")(),
                            "key": "jobWaypointStatusDisplayName",
                            "text": planInfo.jobWaypointStatusDisplayName || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-planIncomingDate")(),
                            "key": "formatPlanIncomingDate",
                            "text": planInfo.formatPlanIncomingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-actualIncomingDate")(),
                            "key": "formatActualIncomingDate",
                            "text": planInfo.formatActualIncomingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-actualOutgoingDate")(),
                            "key": "formatActualOutgoingDate",
                            "text": planInfo.formatActualOutgoingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-remark")(),
                            "key": "jobDescription",
                            "text": planInfo.jobDescription || '-'
                        }
                    ]
                }
            };
            viewonmapData.plan.pin.push(pinItem);
            viewonmapData.plan.route = viewonmapData.plan.route.concat(planInfo.shipmentLocations);
        });

        _.forEach(result.shipmentActualInfos, (actualInfo) => {
            var pinItem = {
                longitude: actualInfo.longitude,
                latitude: actualInfo.latitude,
                attributes: {
                    jobWaypointStatus: actualInfo.jobWaypointStatus,
                    jobWaypointName: actualInfo.jobWaypointName,
                    info: [
                        {
                            "title": this.i18n("waypoint-info-status")(),
                            "key": "jobWaypointStatusDisplayName",
                            "text": actualInfo.jobWaypointStatusDisplayName || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-planIncomingDate")(),
                            "key": "formatPlanIncomingDate",
                            "text": actualInfo.formatPlanIncomingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-actualIncomingDate")(),
                            "key": "formatActualIncomingDate",
                            "text": actualInfo.formatActualIncomingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-actualOutgoingDate")(),
                            "key": "formatActualOutgoingDate",
                            "text": actualInfo.formatActualOutgoingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-remark")(),
                            "key": "jobDescription",
                            "text": actualInfo.jobDescription || '-'
                        }
                    ]
                }
            };
         //   console.log("viewonmapData.actual.route",viewonmapData.actual.route);
            viewonmapData.actual.pin.push(pinItem);
            viewonmapData.actual.route = viewonmapData.actual.route.concat(actualInfo.shipmentActualRouteInfos);
        });

        //waitingLocataionInfos
        if(_.size(result.waitingLocataionInfos.items)){
            

            var route = new Array();
            let arrSize = _.size(result.waitingLocataionInfos.items);
            let beginLat = result.waitingLocataionInfos.items[0].latitude;
            let beginLon = result.waitingLocataionInfos.items[0].longitude;
            let beginAttributes = this.generateAttributes(result.waitingLocataionInfos.items[0]);
            let endLat = result.waitingLocataionInfos.items[arrSize-1].latitude;
            let endLon = result.waitingLocataionInfos.items[arrSize-1].longitude;
            let endAttributes = this.generateAttributes(result.waitingLocataionInfos.items[arrSize-1]);

            _.forEach(result.waitingLocataionInfos.items, (item) => {
                route.push({
                    lat: item.latitude,
                    lon: item.longitude,
                    attributes: this.generateAttributes(item),
                    direction: item.direction,
                    movement: item.movement
                });
            });

            viewonmapData.waiting.pin = [
                {
                    latitude: beginLat,
                    longitude: beginLon,
                    attributes: beginAttributes,
                },
                {
                    latitude: endLat,
                    longitude: endLon,
                    attributes: endAttributes,
                }
            ];
            viewonmapData.waiting.route = route;
            
        }

        
        

        var directionConst = ["N", "NE", "E", "SE", "S", "SW", "W", "NW"];
        _.forEach(result.locationInfo, (locationInfo) => {

            AssetInfo.getStatuses(locationInfo,WebConfig.userSession);
            var locationInfoStatus = AssetInfo.getLocationInfoStatus(locationInfo);
            var shipmentInfo = AssetInfo.getShipmentInfo(locationInfo, locationInfoStatus);

            locationInfo.vehicleLicense = _.isNil(locationInfo.vehicleLicense) ? "-" : locationInfo.vehicleLicense;
            
            var vehicleItem = {
                longitude: locationInfo.longitude,
                latitude: locationInfo.latitude,
                attributes: {
                    "TITLE": locationInfo.vehicleLicense,
                    "SHIPMENTINFO": shipmentInfo
                }
            };

            var vehicleIcon = result.vehicleIconPath[locationInfo.vehicleIconId];
            var movementImageUrl = "";
            _.forEach(result.vehicleIconPath[locationInfo.vehicleIconId], (vehicleIcon) => {
                if (vehicleIcon.movementType == locationInfo.movement) {
                    movementImageUrl = vehicleIcon.imageUrl + "/" + directionConst[locationInfo.directionType - 1] + ".png"
                }
            });
            vehicleItem.movementImageUrl = Utility.resolveUrl(movementImageUrl);
            viewonmapData.vehicle.push(vehicleItem);
        });

        MapManager.getInstance().clear();
        MapManager.getInstance().openShipmentLegend({ data: viewonmapData });
    }

    /**
     * Create attributes object.
     * 
     * @param {any} data
     * @returns
     * 
     * @memberOf PlaybackTimelineScreen
     */
    generateAttributes(data) {
        var isVehicle = data.vehicleId ? true : false;
        var datID = isVehicle ? "V-" + data.vehicleId + "-PB" : "D-" + data.deliveryManId + "-PB";
        var title = isVehicle ? data.vehicleLicense : data.username;
        var businessUnitName = "";

        // Detect business unit name based on vehicle or delivery men.
        if (isVehicle) {
            businessUnitName = _.map(_.filter(this.selectedVehicles, {
                id: data.vehicleId
            }), "businessUnitName");
        } else {
            businessUnitName = _.map(_.filter(this.selectedDeliveryMen, {
                id: data.deliveryManId
            }), "businessUnitName");
        }

        // Compose map attribute object.
        var attr = {
            ID: datID,
            TITLE: title,
            BOX_ID: data.vehicleId ? data.boxSerialNo : data.email,
            DRIVER_NAME: data.driverName,
            DEPT: businessUnitName,
            DATE: data.formatDateTime,
            TIME: Utility.getOnlyTime(data.dateTime, "HH:mm"),
            PARK_TIME: data.parkDuration,
            PARK_IDLE_TIME: data.idleTime,
            LOCATION: data.location,
            RAW: data,
            STATUS: AssetInfo.getStatuses(data, WebConfig.userSession),
            IS_DELAY: data.isDelay
        };

        attr.SHIPMENTINFO = AssetInfo.getShipmentInfo(data, attr.STATUS);

        return attr;
    }

}

export default {
    viewModel: ScreenBase.createFactory(BOMsRealtimePassengerDashboardList),
    template: templateMarkup
};
import ko from "knockout";
import ScreenBase from "../../../screenbase";
import templateMarkup from "text!./search.html";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebRequestBOMs from "../../../../../app/frameworks/data/apitrackingcore/webRequestBOMs";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestRealtimePassenger from "../../../../../app/frameworks/data/apitrackingcore/WebRequestRealtimePassenger";
import WebRequestGlobalSearch from "../../../../../app/frameworks/data/apitrackingcore/webrequestGlobalSearch";

class BOMsRealtimePassengerDashboardSearch extends ScreenBase {
    constructor(params) {
        super(params);
        this.businessUnitIds = this.ensureNonObservable(params.businessUnitIds);
        this.travelModeIds = this.ensureNonObservable(params.travelModeIds);
        this.shiftModeIds = this.ensureNonObservable(params.shiftModeIds);
        this.bladeSize = BladeSize.Small;
        this.bladeTitle(this.i18n('Common_Search')());
        this.filter = ko.observable();
        this.keyword = ko.observable();
        this.validationKeyword = ko.observable(false);
        /*Auto Complete */
        this.requestSearch = (request, response) => {
            if (request.term){
                var filter = {
                    keyword: request.term,
                }
                this.webRequestRealtimePassenger.autoComplete(filter).done((data) => {
                    response( $.map( data, (item) => {
                        return {
                            label: item,
                            value: '"' + item + '"'
                        };
                    }));
                }).fail((e)=>{
                    this.handleError(e);
                });    
            }
            /*End Auto Complete */
        }

        this.keyword.subscribe((res) => {
            if (res.length > 2) {
                this.validationKeyword(true);
            }else{
                this.validationKeyword(false);
            }
        });

    }

    /**
     * Get WebRequest BOMS
     * @readonly
     */
    get webRequestBOMs() {
        return WebRequestBOMs.getInstance();
    }

    /**
     * Get WebRequestRealtimePassenger
     * @readonly
     */
    get webRequestRealtimePassenger() {
        return WebRequestRealtimePassenger.getInstance();
    }

    /**
     * Get WebRequest specific for GlobalSearch module in Web API access.
     * @readonly
     */
    get webRequestGlobalSearch() {
        return WebRequestGlobalSearch.getInstance();
    }

    setupExtend() {
        // this.keyword.extend({ 
        //     required: true,
        //     minLength: 3
        // });
        // this.validationModel = ko.validatedObservable({
        //     keyword: this.keyword,
        // });
    }

    onLoad(isFirstLoad) {
    }

    buildActionBar(actions) {
        actions.push(this.createAction("actSearch", this.i18n('Common_Search')(), "default", true, this.validationKeyword));
    }

    onActionClick(sender) {
        // if (!this.validationModel.isValid()) {
        //     this.validationModel.errors.showAllMessages();
        //     return;
        // }
        if (sender.id === "actSearch") {
            this.filter({
                businessUnitId: _.toString(this.businessUnitIds),
                travelMode: _.toString(this.travelModeIds),
                shiftMode: _.toString(this.shiftModeIds),
                keyword: this.keyword()
            });
            var filter = this.filter();
            this.navigate("cw-boms-realtime-passenger-dashboard-search-result", filter);
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(BOMsRealtimePassengerDashboardSearch),
    template: templateMarkup
};
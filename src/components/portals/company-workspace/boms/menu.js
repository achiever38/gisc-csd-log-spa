﻿import ko from "knockout";
import templateMarkup from "text!./menu.html";
import ScreenBase from "../../screenbase";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import { Enums, Constants } from "../../../../app/frameworks/constant/apiConstant";

/**
 * Display available subscription menu based on user permission.
 * @class MenuScreen
 * @extends {ScreenBase}
 */
class BOMsMenuScreen extends ScreenBase {
    constructor(params){
        super(params);
        
        this.bladeTitle(this.i18n("Menu_BOMs")());
        this.bladeCanMaximize = false;
        this.bladeMargin = BladeMargin.Narrow;
        this.selectedIndex = ko.observable();
        
        this._selectingRowHandler = (item) => {
            if (item) {
                return this.navigate(item.id);
            }
            return false;
        };

        this.items = ko.observableArray([]);

    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {

        if(WebConfig.userSession.hasPermission(Constants.Permission.BOMsRealtimePassengerDashboard)){
            this.items.push({
                text: this.i18n("BOMs_Realtime_Passenger_Dashboard")(),
                id: 'cw-boms-realtime-passenger-dashboard-form'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.BOMsReports)){
            this.items.push({
                text: this.i18n("BOMs_Reports")(),
                id: 'cw-boms-reports'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.BomsDefaultValue)){
            this.items.push({
                text: this.i18n("BOMs_DefaultValues")(),
                id: 'cw-boms-defaultvalues-list'
            });
        }
        

        

        

        
        


    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedIndex(null);
    }
    /**
     * Navigate to inner screens.
     * @param {any} extras
     */
    goto (extras){
        // Passing report names to next page.
        //  this.navigate("bo-report-and-group-ovserspeed", {reportSource: extras});
        this.navigate(extras.id,extras.name);
    }
}

export default {
    viewModel: ScreenBase.createFactory(BOMsMenuScreen),
    template: templateMarkup
};

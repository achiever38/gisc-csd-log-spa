﻿import ko from "knockout";
import templateMarkup from "text!./menu.html";
import ScreenBase from "../../screenbase";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import {Constants} from "../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";

/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class ShipmentMainMenuScreen extends ScreenBase {

    /**
     * Creates an instance of Shipment Main Screen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Transportation_Mngm_Title")());

        this.bladeMargin = BladeMargin.Narrow;
        this.bladeSize = BladeSize.Small;

        this.items = ko.observableArray([]);
        this.selectedIndex = ko.observable();

        this._selectingRowHandler = (item) => {
            if (item) {
                return this.navigate(item.page);
            }
            return false;
        };
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // TODO: Verify access right to specific items
        if (!isFirstLoad) {
            return;
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.ShipmentDashboard)) {
            this.items.push({
                text: this.i18n('Transportation_Mngm_Dashboard_Title')(),
                page: 'cw-shipment-dashboard'
            });
        }


        if (WebConfig.userSession.hasPermission(Constants.Permission.ShipmentTimeline)) {
            this.items.push({
                text: this.i18n('Transportation_Mngm_ShipmentTimeline_Title')(),
                page: 'cw-shipment-timeline'
            });
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.ShipmentManagement)) {
            this.items.push({
                text: this.i18n('Transportation_Mngm_Shipment_Title')(),
                page: 'cw-shipment-list'
            });
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.ShipmentPOSearch)) {
            this.items.push({
                text: this.i18n('Transportation_Mngm_POSearch_Title')(),
                page: 'cw-shipment-posearch'
            });
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.ShipmentDefaultValue)) {
            this.items.push({
                text: this.i18n('Transportation_Mngm_Default_Value_Title')(),
                page: 'cw-shipment-default-value'
            });
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.ShipmentTemplatesManagement)) {
            this.items.push({
                text: this.i18n('Transportation_Mngm_Template_Title')(),
                page: 'cw-shipment-templates-list'
            });
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.ShipmentCategory)) {
            this.items.push({
                text: this.i18n('ShipmentCategory_Title')(),
                page: 'cw-shipment-category'
            });
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.ShipmentZone)) {
            this.items.push({
                text: this.i18n('ShipmentZone_Title')(),
                page: 'cw-shipment-zone'
            });
        }
                
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedIndex(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}
}

export default {
    viewModel: ScreenBase.createFactory(ShipmentMainMenuScreen),
    template: templateMarkup
};
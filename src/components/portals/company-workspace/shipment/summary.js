﻿import ko from "knockout";
import templateMarkup from "text!./summary.html";
import ScreenBase from "../../screenbase";
import ScreenHelper from "../../screenhelper";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import { Constants } from "../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import WebRequestShipment from "../../../../app/frameworks/data/apitrackingcore/webRequestShipment";
import Utility from "../../../../app/frameworks/core/utility";


/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class ShipmentSummaryScreen extends ScreenBase {

    /**
     * Creates an instance of ShipmentSummaryScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Transportation_Mngm_Shipment_Summary")());
        this.isAutoRefresh = ko.observable(false);
        this.timeOutHandler = this.ensureNonObservable(null);
        this.txtRefreshTime = ko.observable("");
        this.bladeIsMaximized = true;
        this.bladeSize = BladeSize.XLarge_A1;
        this.txtRefresh = ko.pureComputed(() => {
        
            return (
                this.i18n("Transportation_Mngm_Shipment_AutoRefresh")() +
                " (" +
                this.i18n("Vehicle_MA_LastSync")() +
                " " +
                this.formatTimestamp() +
                ")"
            );
        });
        // Tap ShipmentInfo
        
        this.shipmentId = params.shipmentId;//159950  //  test Id = 94976;
        this.txtStatus              = ko.observable("");
        this.txtShipmentName        = ko.observable("");
        this.txtShipmentStartDate   = ko.observable("");
        this.txtBusinessUnit        = ko.observable("");
        this.txtDriver              = ko.observable("");
        this.txtDeliveryMan         = ko.observable("");
        this.txtShipmentCode        = ko.observable("");
        this.txtShipmentTemplate    = ko.observable("");
        this.txtShipmentWaitingTime = ko.observable("");
        this.txtVehicle             = ko.observable("");
        this.txtShipmentTrail       = ko.observable("");
        this.txtDescription         = ko.observable("");

        this.txtFuelFromDistance        = ko.observable("");
        this.txtAvgSpeed                = ko.observable("");
        this.txtInsideOfWaypointStop    = ko.observable("");
        this.txtOverSpeed               = ko.observable("");
        this.txtFuelFromSensor          = ko.observable("");
        this.txtSummaryStopTable        = ko.observable("");
        this.txtOutsideOfWaypointStop   = ko.observable("");
        this.txtDecRapid                = ko.observable("");
        this.txtMaxSpeed                = ko.observable("");
        this.txtMove                    = ko.observable("");
        this.txtMaxTemperature          = ko.observable("");
        this.txtAccRapid                = ko.observable("");
        this.txtLowSpeed                = ko.observable("");
        this.txtStop                    = ko.observable("");
        this.txtLowTemperature          = ko.observable("");
        this.txtjobAutoFinish           = ko.observable("");
        this.txtDuration                = ko.observable("");
        this.txtlateShipmentRole        = ko.observable("");

        this.txtFinishCause             = ko.observable("");
        this.txtFinishBy                = ko.observable("");
        this.txtCancelCause             = ko.observable("");
        this.txtCancelBy                = ko.observable("");

        this.txtStartShipment = ko.observable("");
        this.txtCloseShipment = ko.observable();
        this.txtCreatedDate = ko.observable();
        this.txtCreatedBy = ko.observable();
        this.txtLastUpdateDate = ko.observable();
        this.txtLastUpdateBy = ko.observable();
        this.txtShipmentReferenceNo = ko.observable();
        this.txtShipmentFinishCancelCause = ko.observable();
        this.txtShipmentVehicleBU = ko.observable();
        this.txtShipmentDriverBU = ko.observable();
        this.txtShipmentFinishCancelBy = ko.observable();
        this.txtShipmentCloseDate = ko.observable();
        this.txtlateWaypoint = ko.observable();

        this.txtZone = ko.observable("");
        this.txtTravelMode = ko.observable("");
        this.txtShipmentCategory = ko.observable("");

        this.formatTimestamp = ko.observable("");
        this.txtSwerveDriver = ko.observable("");
        this.shipmentDetailData = ko.observable(null);

        this.txtFormatFuelFromPlanDistance = ko.observable();
        this.txtStopEngineOn = ko.observable();
        this.txtAvgTemperature = ko.observable();
        // Sample properties.
        
        this.selectedIndex = ko.observable(0);
        

        // Sample2 properties.
        this.tab2Visible = ko.observable(true);

        this.coDriverLst = ko.observableArray([]);
        this.isCoDriver = ko.observable(false);
        this.porterLst = ko.observableArray([]);
        this.isPorter = ko.observable(false);
        this.isAutoRefresh.subscribe(isChecked => {
            this.onAutoRefresh();
        });

        this.vehicleLogingList           = ko.observableArray([]);
        this.isVehicleLoging            = ko.observable(false);

        this.renderShipmentColorData = (data) => {
            if (!_.isNil(data)) {
                var html = ""
                html += '<span style="color:' + data.jobWaypointNameColor + ';">' + data.jobWaypointName + '</span>';
                return html;
            } else {
                return "";
            }
        }
        this.visibleAlert = ko.observable(false);
        this.alertInfos = ko.observableArray([
            //{ alert1: "alert1", value1: 1, alert2: "alert2", value2: 2, alert3: "alert3", value3: 3, alert4: "alert4", value4: 4, alert5: null, value5: " " }, //mock Data
        ]);

    }
    onClick() { }
    onToggleTab() {
        this.tab2Visible(!this.tab2Visible());
    }
    

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // TODO: Verify access right to specific items
        if (!isFirstLoad) {
            return;
        }

        
        
        this.getDataShipmentSummary();
        
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() { 
        this.isAutoRefresh(false);
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) { }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(
            this.createCommand(
                "cmdExport",
                this.i18n("Common_Export")(),
                "svg-cmd-export"
            )
        );
        commands.push(
            this.createCommand(
                "cmdRefresh",
                this.i18n("Common_Refresh")(),
                "svg-cmd-refresh"
            )
        );
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdExport":
                this.exportShipment();
                break;
            case "cmdRefresh":
                this.getDataShipmentSummary();
                //this.onAutoRefresh(true);
                break;
            default:
                break;
        }
    }

    onDatasourceRequestRead(gridOption) {
        var dfd = $.Deferred();
        if (this.shipmentDetailData() != null) {
            var filter = Object.assign({}, this.shipmentDetailData(), gridOption);
            dfd.resolve({
                items: filter.item,
                totalRecords: filter.item.length
            });

            
        } else {
            dfd.resolve({
                items:[],
                totalRecords: 0
            });
        }
        return dfd;
    }

    refreshShipmentList() {
        this.dispatchEvent("dgShipmentSummary", "refresh");
    }

    formatData(response) {
        
        var data = [];
        for (let i = 0; i < response.length; i++) {
            var dataItem = response[i]; 
            dataItem.actionDisplayName = dataItem.actionDisplayName == null ? "-" : dataItem.actionDisplayName;
            dataItem.formatFuelFromDistance = dataItem.formatFuelFromDistance == null ? "-" : dataItem.formatFuelFromDistance;
            dataItem.formatFuelFromSensor = dataItem.formatFuelFromSensor == null ? "-" : dataItem.formatFuelFromSensor;
            dataItem.formatMileage = dataItem.formatMileage == null ? "-" : dataItem.formatMileage;
            dataItem.formatTemperatureArrive = dataItem.formatTemperatureArrive == null ? "-" : dataItem.formatTemperatureArrive;
            dataItem.formatTemperatureDepart = dataItem.formatTemperatureDepart == null ? "-" : dataItem.formatTemperatureDepart;
            dataItem.formatAvgTemperatureInside = dataItem.formatAvgTemperatureInside == null ? "-" : dataItem.formatAvgTemperatureInside;
            dataItem.formatAvgTemperatureOutside = dataItem.formatAvgTemperatureOutside == null ? "-" : dataItem.formatAvgTemperatureOutside;
            dataItem.orderCode = dataItem.orderCode == null ? "-" : dataItem.orderCode;
            dataItem.formatPlanArriveDate = dataItem.formatPlanArriveDate == null ? " " : dataItem.formatPlanArriveDate;
            dataItem.formatPlanDepartDate = dataItem.formatPlanDepartDate == null ? " " : dataItem.formatPlanDepartDate;
            dataItem.formatActualArriveDate = dataItem.formatActualArriveDate == null ? " " : dataItem.formatActualArriveDate;
            dataItem.formatActualDepartDate = dataItem.formatActualDepartDate == null ? " " : dataItem.formatActualDepartDate;
            dataItem.formatActualOrder = dataItem.formatActualOrder == null ? " " : dataItem.formatActualOrder;
            dataItem.formatActualDistance = dataItem.formatActualDistance == null ? "-" : dataItem.formatActualDistance;
            dataItem.formatDistanceInWaypoint = dataItem.formatDistanceInWaypoint == null ? "-" : dataItem.formatDistanceInWaypoint;
            dataItem.formatAvgSpeedOutWaypoint = dataItem.formatAvgSpeedOutWaypoint == null ? "-" : dataItem.formatAvgSpeedOutWaypoint;
            dataItem.swerveTime = dataItem.swerveTime == null ? "-" : dataItem.swerveTime;


            dataItem.waypointData = {
                color: dataItem.jobWaypointNameColor,
                text: dataItem.jobWaypointStatusDisplayName
            };

            dataItem.planlArriveData = {
                color: dataItem.planColor,
                text: dataItem.formatPlanArriveDate
            };

            dataItem.planlDepartData = {
                color: dataItem.planColor,
                text: dataItem.formatPlanDepartDate
            };

            dataItem.planlStopData = {
                color: dataItem.planColor,
                text: dataItem.planStopTime
            };

            dataItem.planlTravelData = {
                color: dataItem.planColor,
                text: dataItem.planTravelTime
            };

            dataItem.planlTripData = {
                color: dataItem.planColor,
                text: dataItem.planTripTime
            };

            dataItem.planlDistanceData = {
                color: dataItem.planColor,
                text: dataItem.formatPlanDistance
            };

            dataItem.actualArriveOrderData = {
                color: dataItem.actualColor,
                text: dataItem.formatActualOrder
            };
            dataItem.actualArriveData = {
                color: dataItem.actualColor,
                text: dataItem.formatActualArriveDate
            };
            dataItem.actualDepartData = {
                color: dataItem.actualColor,
                text: dataItem.formatActualDepartDate
            };
            dataItem.actualStopData = {
                color: dataItem.actualColor,
                text: dataItem.actualTimeInsideTime
            };
            dataItem.actualTravelData = {
                color: dataItem.actualColor,
                text: dataItem.actualTravelTime
            };
            dataItem.actualTripData = {
                color: dataItem.actualColor,
                text: dataItem.actualTripTime
            };
            dataItem.actualDistance = {
                color: dataItem.actualColor,
                text: dataItem.formatActualDistance
            };

            dataItem.distanceInWaypoint = {
                color: dataItem.actualColor,
                text: dataItem.formatDistanceInWaypoint
            };

            dataItem.avgSpeedOutWaypoint = {
                color: dataItem.actualColor,
                text: dataItem.formatAvgSpeedOutWaypoint
            };

            if (response.length == i + 1) {
                //console.log("Total ", dataItem)
                dataItem.formatTemperatureArrive = dataItem.formatTemperatureArrive == "-" ? " " : dataItem.formatTemperatureArrive;
                dataItem.formatTemperatureDepart = dataItem.formatTemperatureDepart == "-" ? " " : dataItem.formatTemperatureDepart;
                dataItem.formatAvgTemperatureInside = dataItem.formatAvgTemperatureInside == "-" ? " " : dataItem.formatAvgTemperatureInside;
                dataItem.formatAvgTemperatureOutside = dataItem.formatAvgTemperatureOutside == "-" ? " " : dataItem.formatAvgTemperatureOutside;
                dataItem.formatMileage = dataItem.formatMileage == "-" ? " " : dataItem.formatMileage;
                dataItem.orderCode = dataItem.orderCode == "-" ? " " : dataItem.orderCode;
                dataItem.actionDisplayName = dataItem.actionDisplayName == "-" ? " " : dataItem.actionDisplayName;
            }

            data.push(dataItem);
        }
        return data;
    }

    exportShipment() {
        var dfdExportShipment = this.webRequestShipment.getExportShipmentSummary(this.shipmentId);

        this.isBusy(true);
        $.when(dfdExportShipment).done(response => {
            this.isBusy(false);
            ScreenHelper.downloadFile(response.fileUrl);
        });
    }

    getDataShipmentSummary() {
        this.isBusy(true);
        var dfd = $.Deferred();
        var userSummary = this.shipmentId ? this.webRequestShipment.getShipmentSummary(this.shipmentId) : null;
        $.when(userSummary).done((res) => {
            if(res != null) {
                this.formatTimestamp(res.formatLastSyncDate);
                this.txtStatus(res.formatPlanStartDate);

                this.txtStatus(res.shipmentStatusDisplayName);
                this.txtShipmentName(res.shipmentName);
                this.txtShipmentStartDate(res.formatPlanStartDate);
                this.txtBusinessUnit(res.businessUnitName);
                this.txtDriver(res.driverName);
                this.txtDeliveryMan(res.deliveryManName);
                this.txtShipmentCode(res.shipmentCode);
                this.txtShipmentTemplate(res.shipmentTemplateName);
                this.txtShipmentWaitingTime(res.waitingTime);
                this.txtVehicle(res.vehicleLicense);
                this.txtShipmentTrail(res.vehicleTrailLicense);
                this.txtFinishCause(res.finishCause == null ? "-" : res.finishCause);
                this.txtFinishBy(res.finishBy == null ? "-" : res.finishBy);
                this.txtCancelCause(res.cancelCause == null ? "-" : res.cancelCause);
                this.txtCancelBy(res.cancelBy == null ? "-" : res.cancelBy);
                
                this.txtDescription(res.shipmentDescription == null ? res.shipmentDescription : Utility.convertUserTagToHTML(res.shipmentDescription));

                this.txtSwerveDriver(res.swerveTime);
                this.txtFuelFromDistance(res.formatFuelFromDistance);
                this.txtAvgSpeed(res.avgSpeed);
                this.txtInsideOfWaypointStop(res.insideWaypointStopTime);
                this.txtOverSpeed(res.overSpeedTime);
                this.txtFuelFromSensor(res.fuelFromSensor);
                this.txtSummaryStopTable(res.stopTime);
                this.txtOutsideOfWaypointStop(res.outsideWaypointStopTime);
                this.txtDecRapid(res.decRapidTime);
                this.txtMaxSpeed(res.maxSpeed);
                this.txtMove(res.moveDuration);
                this.txtMaxTemperature(res.maxTemperature);
                this.txtAccRapid(res.accRapidTime);
                this.txtLowSpeed(res.minSpeed);
                this.txtStop(res.stopDuration);
                this.txtLowTemperature(res.minTemperature);
                this.txtjobAutoFinish(res.jobAutoFinishDisplayName);
                this.txtDuration(res.jobAutoFinishDuration);
                this.txtlateShipmentRole(res.lateShipmentRoleDisplayName == null ? "-" : res.lateShipmentRoleDisplayName);

                this.txtStartShipment(res.startJobTypeDisplayName);
                this.txtCloseShipment(res.closeJobTypeDisplayName);
                this.txtCreatedDate(res.formatCreateDate);
                this.txtCreatedBy(res.createBy);
                this.txtLastUpdateDate(res.formatUpdateDate);
                this.txtLastUpdateBy(res.updateBy);
                this.txtShipmentReferenceNo(res.refNo);
                this.txtShipmentFinishCancelCause(res.finishCause ? res.finishCause : res.cancelCause);
                this.txtShipmentVehicleBU(res.vehicleBusinessUnitName);
                this.txtShipmentDriverBU(res.driverBusinessUnitName);
                this.txtShipmentFinishCancelBy(res.finishBy ? res.finishBy : res.cancelBy);
                this.txtShipmentCloseDate(res.formatPlanEndDate);
                this.txtlateWaypoint(res.formatIsTerminal);

                this.txtZone(res.jobRegionName);
                this.txtTravelMode(res.travelModeDisplayName);
                this.txtShipmentCategory(res.jobCategoryName);

                this.txtFormatFuelFromPlanDistance(res.formatFuelFromPlanDistance);
                this.txtStopEngineOn(res.idleDuration);
                this.txtAvgTemperature(res.avgTemperature);

                this.shipmentDetailData({ item: this.formatData(res.waypointList) });                
                for(let i=0;i<res.coDriverList.length;i++) {
                    this.coDriverLst().push(res.coDriverList[i]);
                }
                
                for(let i=0;i<res.poterList.length;i++) {
                    this.porterLst().push(res.poterList[i]);
                }
                for(let i=0;i<res.vehicleLogInfos.length;i++) {
                    this.vehicleLogingList().push(res.vehicleLogInfos[i]);
                }

                if(this.coDriverLst().length != 0) {
                    this.isCoDriver(true)
                }

                if(this.porterLst().length != 0) {
                    this.isPorter(true)
                }

                if(this.vehicleLogingList().length != 0){
                    this.isVehicleLoging(true)
                }

                if (_.size(res.shipmentAlertInfos) > 0) {
                    this.alertInfos(res.shipmentAlertInfos);
                    this.visibleAlert(true);
                }

                this.refreshShipmentList();
            }            
            dfd.resolve();
        }).fail((e)=> {
            this.handleError(e);
        }).always(()=> {
            this.isBusy(false);
        });

        return dfd;
    }

    onAutoRefresh(reCall) {
        if (reCall) {
            clearTimeout(this.timeOutHandler);
            this.onAutoRefresh();
            return;
        }

        if (this.isAutoRefresh()) {
            this.timeOutHandler = setTimeout(() => {
                this.getDataShipmentSummary();
                this.onAutoRefresh();
            }, WebConfig.appSettings.shipmentAutoRefreshTime);
        } else {
            clearTimeout(this.timeOutHandler);
        }
    }

    get webRequestShipment() {
        return WebRequestShipment.getInstance();
    }
}

export default {
viewModel: ScreenBase.createFactory(ShipmentSummaryScreen),
    template: templateMarkup
};
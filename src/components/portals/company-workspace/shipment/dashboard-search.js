﻿import ko from "knockout";
import templateMarkup from "text!./dashboard-search.html";
import ScreenBase from "../../screenbase";
import ScreenHelper from "../../screenhelper";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import { Enums, Constants } from "../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../app/frameworks/core/utility";

import WebRequestBusinessUnit from "../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestCategory from "../../../../app/frameworks/data/apitrackingcore/webRequestCategory";
import WebRequestShipment from "../../../../app/frameworks/data/apitrackingcore/webRequestShipment";

/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class DashboardSearchScreen extends ScreenBase {

    /**
     * Creates an instance of ShipmentPOSearchScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Common_Search")());
        this.bladeSize = BladeSize.Small;

        this.isToday = ko.observable(0);
        this.isHistorical = ko.observable(1);

        this.isState = ko.observable();
        this.checkState(params.isState);

        //date format
        this.dateFormat = WebConfig.companySettings.shortDateFormat;

        //value
        this.region = ko.observable();
        this.businessUnit = ko.observable();
        this.includeSubTS = ko.observable(false);
        this.vehicleCategory = ko.observable();

        //value of historical
        this.year = ko.observable();
        this.quarter = ko.observable();
        this.month = ko.observable();
        this.dateFrom = ko.observable();
        this.dateTo = ko.observable();

        //ddl data source
        this.regionList = ko.observableArray([]);
        this.businessUnitList = ko.observableArray([]);
        this.vehicleCategoryList = ko.observableArray([]);

        //ddl data source of historical
        this.yearList = ko.observableArray([]);
        this.quarterList = ko.observableArray([]);
        this.monthList = ko.observableArray([]);

        this.maxDate = ko.observable();
        this.minDate = ko.observable();

        this.ddEnabled = ko.observable(false);
        this.isOnChange = false

        this.subscribeMessage("cw-shipment-dashboard-search-swap-content", (filter) => {
            this.checkState(filter)          
        });
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // TODO: Verify access right to specific items
        if (!isFirstLoad) {            
            return;
        }     
        
        this.setDefaultRangeDate()
        this.createYearList()
        this.createQuarterList()
        this.createMonthList() 

        this.dateFrom.subscribe((val)=> {
            if(!val) {
                $("#dtDateFrom").val("")
                return
            }

            let dateFrom = new Date(val);
            let dateTo = new Date(this.dateTo());
            let rangeDateFrom = Utility.addYears(dateFrom, 1)

            $("#dtDateFrom").focus((e)=> {
                this.isOnChange = true
            })

            if(dateFrom > dateTo) {
                this.isOnChange = false
                this.dateTo(dateFrom)
                $("#dtDateTo").val(this.setFormatDisplayDateTime(new Date(dateFrom)))
            }

            if(this.isOnChange) {  
                if((rangeDateFrom < dateTo) && (rangeDateFrom.getFullYear() != '0001')) {
                    this.dateTo(rangeDateFrom)
                    $("#dtDateTo").val(this.setFormatDisplayDateTime(new Date(rangeDateFrom)))
                } else {
                    this.yearList()[0]['isReset'] = true                
                    this.year(this.yearList()[0])

                    this.dateFrom(dateFrom)
                    this.dateTo(dateTo)
                    $("#dtDateFrom").val(this.setFormatDisplayDateTime(new Date(dateFrom)))                
                    $("#dtDateTo").val(this.setFormatDisplayDateTime(new Date(dateTo)))
                }                
                this.ddEnabled(false)  
            }
        })

        this.dateTo.subscribe((val)=> {
            if(!val) {
                $("#dtDateTo").val("")
                return
            }

            let dateFrom = new Date(this.dateFrom());
            let dateTo = new Date(val);
            let rangeDateFrom = Utility.addYears(dateFrom, 1)

            $("#dtDateTo").focus((e)=> {
                this.isOnChange = true
            })
            
            if(dateTo < dateFrom) {
                this.isOnChange = false
                this.dateFrom(dateTo)
                $("#dtDateFrom").val(this.setFormatDisplayDateTime(new Date(dateTo)))
            }

            if(this.isOnChange) {
                if((rangeDateFrom < dateTo) && (rangeDateFrom.getFullYear() != '0001')) {
                    let rangeDate = Utility.minusYears(dateTo, 1)
                    this.dateFrom(rangeDate)
                    $("#dtDateFrom").val(this.setFormatDisplayDateTime(new Date(rangeDate)))   
                } else {
                    this.yearList()[0]['isReset'] = true
                    this.year(this.yearList()[0])
                
                    this.dateFrom(dateFrom)
                    this.dateTo(dateTo)
                    $("#dtDateFrom").val(this.setFormatDisplayDateTime(new Date(dateFrom)))                
                    $("#dtDateTo").val(this.setFormatDisplayDateTime(new Date(dateTo)))
                }                
                this.ddEnabled(false)  
            }
        })

        this.year.subscribe((val) => {  
            this.isOnChange = false
            this.quarter(this.quarterList()[0])
            this.month(this.monthList()[0])    
            var year = val['value']   

            if (val['value'] == "") {   
                this.setDefaultRangeDate()  
                this.ddEnabled(false)   
            } else {
                let maxDate = new Date()
                let minDate = new Date()
                let maxDateDay = 1;
                let maxDateMonth = 11;
                let minDateDay = 1;
                let minDateMonth = 0;

                if(year == this.maxDate().getFullYear()) {
                    maxDateDay = this.maxDate().getDate()
                    maxDateMonth = this.maxDate().getMonth()

                }

                if(year == this.minDate().getFullYear()) {
                    minDateDay = this.minDate().getDate()
                    minDateMonth = this.minDate().getMonth()
                }
                
                maxDate.setDate(maxDateDay-1)
                maxDate.setMonth(maxDateMonth)           
                maxDate.setFullYear(year)

                minDate.setDate(minDateDay)
                minDate.setMonth(minDateMonth)
                minDate.setFullYear(year)
                
                this.dateFrom(minDate)
                this.dateTo(maxDate)

                $("#dtDateFrom").val(this.setFormatDisplayDateTime(new Date(minDate)))
                $("#dtDateTo").val(this.setFormatDisplayDateTime(new Date(maxDate)))

                this.ddEnabled(true)
            }
        })

        this.quarter.subscribe((val) => {
            this.isOnChange = false
            let quarter = val['value'];
            let year = this.year()["value"];
            let maxDate = new Date()
            let minDate = new Date()
            minDate.setDate(1)    
            maxDate.setDate(1) 
            minDate.setFullYear(year)
            maxDate.setFullYear(year)
            if(quarter != '') {
                this.month(this.monthList()[0])
            }

            switch(quarter) {
                case 1 :                 
                    minDate.setMonth(0)
                    maxDate.setMonth(2 + 1) 
                    break;
                case 2 :
                    minDate.setMonth(3)
                    maxDate.setMonth(5 + 1) 
                    break;
                case 3 :
                    minDate.setMonth(6)
                    maxDate.setMonth(8 + 1) 
                    break;
                case 4 :
                    minDate.setMonth(9)
                    maxDate.setMonth(11 + 1)  
                    break;
                default : // ไม่ได้เลือก 
                    minDate.setMonth(0)                    
                    maxDate.setMonth(11 + 1)  
                    break;
            }

            maxDate.setDate(maxDate.getDate() - 1)   

            this.dateFrom(minDate)
            this.dateTo(maxDate)

            $("#dtDateFrom").val(this.setFormatDisplayDateTime(new Date(minDate)))
            $("#dtDateTo").val(this.setFormatDisplayDateTime(new Date(maxDate)))

        })

        this.month.subscribe((val) => {
            this.isOnChange = false
            let txtMonth = val['text']
            let month = val['value']
            let year = this.year()['value']
            let maxDate = new Date()
            let minDate = new Date()
            minDate.setDate(1)
            maxDate.setDate(1)
            minDate.setFullYear(year)
            maxDate.setFullYear(year) 

            if(txtMonth == '') { // ไม่ได้เลือก 
                minDate.setMonth(0)                    
                maxDate.setMonth(11 + 1)  
            } else {
                minDate.setMonth(month)
                maxDate.setMonth(month + 1)   
                this.quarter(this.quarterList()[0])
            }
            
            maxDate.setDate(maxDate.getDate() - 1)

            this.dateFrom(minDate)
            this.dateTo(maxDate)

            $("#dtDateFrom").val(this.setFormatDisplayDateTime(new Date(minDate)))
            $("#dtDateTo").val(this.setFormatDisplayDateTime(new Date(maxDate)))

        })

        var dfd = $.Deferred();

        var filterCategoryVehicle = {
            categoryId:Enums.ModelData.CategoryType.Vehicle,
        };

        var dfdRegion = null;
        var dfdBusinessUnit = null;
        var dfdVehicleCategory = null;

        /* Process : Business Unit */
        var businessUnitFilter = {
            companyId: WebConfig.userSession.currentCompanyId
        }

        dfdRegion = this.webRequestShipment.listRegionSummary().done((response) => {
            this.regionList(response["items"]);
            this.region(ScreenHelper.findOptionByProperty(this.regionList, "id"));
        });

        dfdBusinessUnit = this.webRequestBusinessUnit.listBusinessUnitSummary(businessUnitFilter).done((response) => {
            this.businessUnitList(response["items"]);
            this.businessUnit(ScreenHelper.findOptionByProperty(this.businessUnitList, "id"));
        });

        dfdVehicleCategory = this.webRequestCategory.listCategory(filterCategoryVehicle).done((response) => {            
            response.items.unshift({
                name: this.i18n('Report_All')(),
                id: 0
            });
            
            this.vehicleCategoryList(response["items"]);
            this.vehicleCategory(ScreenHelper.findOptionByProperty(this.vehicleCategoryList, "id"));
        });

        $.when(dfdRegion, dfdBusinessUnit, dfdVehicleCategory).done(() => {
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    setDefaultRangeDate() {
        let maxDate = new Date()
        maxDate.setDate(31)
        maxDate.setMonth(11)
        this.maxDate(maxDate);
        this.minDate(Utility.minusYears(new Date(), 3));   

        this.dateFrom(this.setDefaultDate('start'));
        this.dateTo(this.setDefaultDate('end'));
        $("#dtDateFrom").val(this.setFormatDisplayDateTime(new Date(this.dateFrom())))
        $("#dtDateTo").val(this.setFormatDisplayDateTime(new Date(this.dateTo())))

    }

    createYearList() {
        let objDate = new Date();
        let currentYear = objDate.getFullYear();

        this.yearList.push({ text: '', value: '' })

        for(let i=0;i<=3;i++) {
            let displayText = (currentYear - i).toString()
            this.yearList.push({ text: displayText, value: displayText })
        }
    }

    createQuarterList() {
        this.quarterList.push({ text: '', value: '' })
        this.quarterList.push({ text: "Q1", value: 1 })
        this.quarterList.push({ text: "Q2", value: 2 })
        this.quarterList.push({ text: "Q3", value: 3 })
        this.quarterList.push({ text: "Q4", value: 4 })
    }

    createMonthList(quarter) {
        let selectedLst = [];
        let monthLst = [];
        monthLst.push({ text: '', value: '' })
        monthLst.push({ text: this.i18n('Common_MonthJan')(), value: 0 })
        monthLst.push({ text: this.i18n('Common_MonthFeb')(), value: 1 })
        monthLst.push({ text: this.i18n('Common_MonthMar')(), value: 2 })
        monthLst.push({ text: this.i18n('Common_MonthApr')(), value: 3 })
        monthLst.push({ text: this.i18n('Common_MonthMay')(), value: 4 })
        monthLst.push({ text: this.i18n('Common_MonthJun')(), value: 5 })
        monthLst.push({ text: this.i18n('Common_MonthJul')(), value: 6 })
        monthLst.push({ text: this.i18n('Common_MonthAug')(), value: 7 })
        monthLst.push({ text: this.i18n('Common_MonthSep')(), value: 8 })
        monthLst.push({ text: this.i18n('Common_MonthOct')(), value: 9 })
        monthLst.push({ text: this.i18n('Common_MonthNov')(), value: 10 })
        monthLst.push({ text: this.i18n('Common_MonthDec')(), value: 11 })
        selectedLst = monthLst.filter((items)=> {
            switch(quarter) {
                case 1 :
                    return (items.value == 0
                        || items.value == 1
                        || items.value == 2) 
                    break;
                case 2 :
                    return (items.value == 3
                        || items.value == 4
                        || items.value == 5) 
                    break;
                case 3 :
                    return (items.value == 6
                        || items.value == 7
                        || items.value == 8) 
                    break;
                case 4 :
                    return (items.value == 9
                        || items.value == 10
                        || items.value == 11) 
                    break;
                default :
                    return items
                    break;
            }
        })

        this.monthList(selectedLst);
    }

    checkState(state) {
        switch(state) {
            case this.isToday():
                this.isState(false);
                break;
            case this.isHistorical():
                this.isState(true);
                break;
            default :
                this.isState(false);
                break;
        }
    }

    setFormatDisplayDateTime(objDate) {
        let displayDate = "";
        let d = objDate;
        let year = d.getFullYear().toString();
        let month = (d.getMonth() + 1).toString();
        let day = d.getDate().toString();

        displayDate = day + "/" + month + "/" + year;

        return displayDate
    }

    setupExtend() {

    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() { }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Search")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdClear", this.i18n("Common_Clear")(), "svg-cmd-clear"));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        switch(sender.id) {
            case "actSave" :
                this.submitSearchFilter();
                break;
            default :
                break;
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch(sender.id) {
            case "cmdClear":
                this.clearFrom()
                break;
            default:
                break;
        }
    }

    clearFrom() {        
        this.region(null);
        this.businessUnit(null);
        this.includeSubTS(false);
        this.vehicleCategory(this.vehicleCategoryList()[0]);

        this.year(this.yearList()[0]);
        this.quarter(this.quarterList()[0]);
        this.month(this.monthList()[0]);
        this.setDefaultRangeDate()
    }

    setDefaultDate(info) {
        let objDate = new Date();
        switch(info) {
            case 'start':
                if(objDate.getDate() == 1) {
                    objDate = Utility.minusMonths(objDate, 1)
                }
                objDate.setDate(1);
                break;
            case 'end' :
                objDate = Utility.minusDays(new Date(), 1)
                break;
            default :
                break;
        }

        return objDate
    }

    submitSearchFilter() {
        let filter = {}
        let subBU = []

        filter.regionIds = this.region() == null ? null : this.region()
        filter.businessUnitIds = this.businessUnit() == null ? null : this.businessUnit()
        filter.vehicleCategoryId = this.vehicleCategory()['id'] == null ? null : this.vehicleCategory()['id']
        

        if(this.isState() == this.isHistorical()) {
            filter.shipmentFromDate = this.dateFrom() == undefined ? null : this.dateFrom();
            filter.shipmentToDate = this.dateTo() == undefined ? null : this.dateTo();
        }
        this.publishMessage("cw-shipment-dashboard-search", filter);
    }

    get webRequestCategory() {
        return WebRequestCategory.getInstance();
    }
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    get webRequestShipment() {
        return WebRequestShipment.getInstance();
    }
}

export default {
viewModel: ScreenBase.createFactory(DashboardSearchScreen),
    template: templateMarkup
};
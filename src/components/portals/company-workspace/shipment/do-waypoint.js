﻿import ko from "knockout";
import templateMarkup from "text!./do-waypoint.html";
import ScreenBase from "../../screenbase";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import { Constants } from "../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import WebRequestShipment from "../../../../app/frameworks/data/apitrackingcore/webRequestShipment";
//import WebRequestCompany from "../../../../app/frameworks/data/apitrackingcore/webrequestCompany";

class DOWaypoint extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Transportation_Mngm_Waypoint_Delivery_Order_Title")());

        this.bladeMargin = BladeMargin.Normal;
        this.bladeSize = BladeSize.XMedium;


        this.waypointId = this.ensureNonObservable(params.waypointId, 0);


        //Hard Code Test 62378


        this.txtWPName = ko.observable("-");
        this.txtDOCount = ko.observable("0");

        this.txtCustomerName = ko.observable("-");
        this.txtShipmentCode = ko.observable("-");
        this.txtShipmentName = ko.observable("-");
        this.txtVehicle = ko.observable("-");
        this.txtDriver = ko.observable("-");
        this.txtDeliveryMan = ko.observable("-");
        this.txtShipTo = ko.observable("-");

        this.pinIconSource = ko.observable("-");
        this.shouldShowDeliveryMan = ko.observable(false);
        this.doListData = ko.observable(null);


        //// Set custom column title with DataTable
        //this.onVehicleMaintenancePlanListTableReady = () => {
        //    if (!_.isEmpty(this.distanceUnit())) {


        //        this.dispatchEvent("dtVehicleMaintenancePlanList2", "setColumnTitle", [{
        //            columnIndex: 6,
        //            columnTitle: this.i18n("Vehicle_MA_Common_AccDistance", [this.distanceUnit()])
        //        }]);
        //    }
        //};
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {

        if (!isFirstLoad) {
            return;
        }


        var dfd = $.Deferred();
        //var dfdCompanySettings = this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId).done((response) => {

        //    console.log("Company Setting", response);
        //    //this.distanceUnit(response.distanceUnitSymbol);
        //});

        if (this.waypointId != null && this.waypointId != 0) {
            var dfdShipmentDO = this.webRequestShipment.getShipmentDeliveryOrder(this.waypointId)
                .done(response => {
                    //console.log("response wp dolist", response);

                    this.txtWPName(!_.isNil(response.jobWaypointName) ? response.jobWaypointName : "-");
                    this.txtDOCount(response.numberOfOrder);

                    this.txtCustomerName(!_.isNil(response.customerName) ? response.customerName : "-");
                    this.txtShipmentCode(!_.isNil(response.shipmentCode) ? response.shipmentCode : "-");
                    this.txtShipmentName(!_.isNil(response.shipmentName) ? response.shipmentName : "-");
                    this.txtVehicle(!_.isNil(response.vehicleLicense) ? response.vehicleLicense : "-");
                    this.txtDriver(!_.isNil(response.driverName) ? response.driverName : "-");


                    if (response.deliveryManName == "-" || _.isNil(response.deliveryManName)){
                        this.shouldShowDeliveryMan(false);
                    }
                    else {
                        this.shouldShowDeliveryMan(true);
                        this.txtDeliveryMan(response.deliveryManName);
                    }
                   

                    this.txtShipTo(!_.isNil(response.shipToAddress) ? response.shipToAddress : "-" );
                    

                    this.doListData({ item: response.doList });   


                    this.pinIconSource(response.icon.name);

                    this.refreshDOList();
                })
                .fail(e => {
                    this.handleError(e);
                })
                .always(() => {
                    dfd.resolve();
                    this.isBusy(false);
                });


            //$.when(dfdCompanySettings, dfdShipmentDO).done(() => {
            //    dfd.resolve();
            //}).fail((e) => {
            //    dfd.reject(e);
            //});
        }
        else {
            dfd.resolve();
        }

        return dfd;
    }

    onDatasourceRequestRead(gridOption) {
        var dfd = $.Deferred();
        if (this.doListData() != null) {
            var filter = Object.assign({}, this.doListData(), gridOption);
            dfd.resolve({
                items: filter.item,
                totalRecords: filter.item.length
            });


        } else {
            dfd.resolve({
                items: [],
                totalRecords: 0
            });
        }
        return dfd;
    }

    refreshDOList() {
        this.dispatchEvent("dgDeliveryOrderList", "refresh");
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {

    }

    get webRequestShipment() {
        return WebRequestShipment.getInstance();
    }

    //    /**
    // * Get WebRequest specific for Company module in Web API access.
    // * @readonly
    // */
    //get webRequestCompany() {
    //    return WebRequestCompany.getInstance();
    //}
}

export default {
    viewModel: ScreenBase.createFactory(DOWaypoint),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../screenbase";
import ScreenHelper from "../../screenhelper";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import { Constants, Enums } from "../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import WebRequestShipment from "../../../../app/frameworks/data/apitrackingcore/webRequestShipment";
import WebRequestUser from "../../../../app/frameworks/data/apicore/webRequestUser";
import AssetInfo from "../asset-info";
import AssetUtility from "../asset-utility";
import Utility from "../../../../app/frameworks/core/utility";
import GenerateDataGridExpand from "../generateDataGridExpand";
import * as BladeDialog from "../../../../app/frameworks/constant/bladeDialog";

import * as EventAggregator from "../../../../app/frameworks/constant/eventAggregator";
import MapManager from "../../../controls/gisc-chrome/map/mapManager";

/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class ShipmentListScreen extends ScreenBase {
    /**
       * Creates an instance of GeoFencingMenuScreen.
       * 
       * @param {any} params
       */
    constructor(params) {

        super(params);
        this.statusDashboard = ko.observable(params.status);
        this.bladeTitle(this.i18n("Transportation_Mngm_Shipment_Title")()); 

        this.bladeSize = BladeSize.XLarge_A1;
        this.bladeIsMaximized = true;
        this.apiDataSource = ko.observableArray([]);
        this.filter = ko.observable();
        this.filterDetail = ko.observable();
        this.selectedIndex = ko.observable();
        this.isAutoRefresh = ko.observable(false);

        this.txtRefreshTime = ko.observable("");
        this.txtRefresh = ko.pureComputed(() => {
            return (
              this.i18n("Transportation_Mngm_Shipment_AutoRefresh")() +
              " (" +
              this.i18n("Vehicle_MA_LastSync")() +
              " " +
              this.txtRefreshTime() +
              ")"
            );
        });

        //Defaultวันปัจจุบัน
        this.filter({
            shipmentFromDate: this.setFormatDateTime(new Date(), "00:00:00"),
            shipmentToDate: this.setFormatDateTime(new Date(), "23:59:59")
        });

        ////ถ้าไม่มีข้อมูลในวันปัจจุบัน ให้มาแก้ใช้ตรงนี้แล้วเลือกวันที่เอาเอง
        //this.filter({
        //    "shipmentFromDate": "2018-02-16T00:00:00",
        //    "shipmentToDate": "2018-02-16T23:59:59"
        //});

        this.dataSource = ko.observable();
        this.isHiddenDeliveryMan = false;

        this.timeOutHandler = this.ensureNonObservable(null);

        this._selectingRowHandler = item => {
            if (item) {
                return this.navigate(item.page);
            }
            return false;
        };

        this.isAutoRefresh.subscribe(isChecked => {
            this.onAutoRefresh();
        });

        //Page Subscribe
        this.subscribeMessage("cw-shipment-search", info => {
            this.filter(info);
            this.refreshShipmentList();
        });
        this.subscribeMessage("cw-shipment-manage-delete", info => {
            this.refreshShipmentList();
        });
        this.subscribeMessage("cw-shipment-manage-Cancel", info => {
            this.refreshShipmentList();
        });
        this.subscribeMessage("cw-shipment-import-updated", info => {
            this.refreshShipmentList();
        });
        this.subscribeMessage("cw-shipment-import-with-template-updated", info => {
            this.refreshShipmentList();
        });
        this.subscribeMessage("cw-shipment-manage-finish", info => {
            this.refreshShipmentList();
        });
        this.subscribeMessage("cw-shipment-manage-refresh", info => {
            this.refreshShipmentList();
        });
        this.onDetailClickColumns = ko.observableArray([]);
        this.onDetailClickColumns(
            GenerateDataGridExpand.getOnClickColumns(this.generateColumnDetail())
        );


        //Map Subscribe
        this._eventAggregator.subscribe(
          EventAggregator.MAP_COMMAND_COMPLETE,
          info => {
              switch (info.command) {
                  case "open-shipment-legend":
                      this.minimizeAll();
                      break;
                  default:
                      break;
              }
          }
        );

        this.clickOpenShipmentSummary = (data) => {           
            this.navigate("cw-shipment-summary", { shipmentId: data.shipmentId });
        };

        this.clickViewOnMap = (data) => { 
            this.isBusy(true);
            var viewOnMapQuery = this.webRequestShipment.getDataViewOnMap(data.shipmentId);
            $.when(viewOnMapQuery).done(res => {
                this.isBusy(false);
                this.prepareViewOnMapData(res);
            });
        };

        this.onDeleteShipment = (data)=> {
            let shipmentStatusId = data.shipmentStatusId
            switch(parseInt(shipmentStatusId)) {
                case Enums.JobStatus.Unassigned:
                case Enums.JobStatus.Waiting:
                    this.showMessageBox(null, this.i18n("M194")(),
                        BladeDialog.DIALOG_YESNO).done((button) => {
                            switch (button) {
                                case BladeDialog.BUTTON_YES:
                                    this.isBusy(true);
                                    this.webRequestShipment.deleteShipment(data.shipmentId).done(() => {
                                        this.isBusy(false);
                                        this.refreshShipmentList();
                                    }).fail((e) => {
                                        this.handleError(e);
                                    }).always(() => {
                                        this.isBusy(false);
                                    });
                                break;
                            }
                        });

                    
                    break;
                default:
                    break;
            }
        }

        this.onUpdateShipment = (data) => {
            let filter = {
                shipmentId: data.shipmentId,
                shipmentStatusId: data.shipmentStatusId
            }
            
            switch (parseInt(data.shipmentStatusId)) {
                case Enums.JobStatus.Finished:
                case Enums.JobStatus.FinishedLate:
                case Enums.JobStatus.FinishedIncomplete:
                case Enums.JobStatus.FinishedLateIncomplete:
                case Enums.JobStatus.Cancelled:
                    break;
                default:
                    this.navigate("cw-shipment-manage", {
                        filter: filter
                    });
                    break;
            }
        }

        //check Permission 

        this.isPermissionDelete = false;
        this.isPermissionUpdate = false;
        if (WebConfig.userSession.hasPermission(Constants.Permission.ShipmentDelete)){
            this.isPermissionDelete = true;
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.ShipmentUpdate)) {
            this.isPermissionUpdate = true;
        }

        this.isPermissionLastWaypoint = false;
        if (WebConfig.userSession.hasPermission(Constants.Permission.ShipmentLastWaypoint)) {
            this.isPermissionLastWaypoint = true;
        }
        //this.isPermissionDelete = ko.observable(WebConfig.userSession.hasPermission(Constants.Permission.ShipmentDelete));
        //this.isPermissionUpdate = ko.observable(WebConfig.userSession.hasPermission(Constants.Permission.ShipmentUpdate));
        var self = this;
        this.renderShipmentWayPoint = (data) => {
            if (!_.isNil(data)) {  
                var node = null;
                if(data.waypointName == null) {
                    node = "-";
                } else {
                    var img = data.iconName != null ? '<img style="width:16px;" src="'+ data.iconName +'"/>' : '';    
                    var eta = data.formatETA == null ? '-' : data.formatETA;
                    var ne = data.formatNE == null ? '-' : data.formatNE;
                    var html = img + data.waypointName + '<br/>'
                        + 'ETA ' + eta + '<br/>'
                        + self.i18n("Transportation_Mngm_Shipment_Remain")() + " " + ne + " " + self.i18n("Transportation_Mngm_Shipment_KMS")();
    
                    node = '<div style="color:'+data.etaColor+';">' + html + '</div>';
                }
                return node; 
            }
            else {
                return "";
            }
        };
    
    }

    /**
       * @lifecycle Called when View is loaded.
       * @param {boolean} isFirstLoad true if ViewModel is first load
       */
    onLoad(isFirstLoad) {
        // TODO: Verify access right to specific items
        if (!isFirstLoad) {
            return;
        }

        this.getDataOnDashboard();


        var userSummaryFilter = {
            CompanyId: WebConfig.userSession.currentCompanyId,
            isAssociatedWithMobileService: true
        };
        var userSummary = this.webRequestUser.listUserSummary(userSummaryFilter);
        var dfd = $.Deferred();

        $.when(userSummary).done(res => {
            if (res.items.length <= 0) {
                this.isHiddenDeliveryMan = true;
            }
            this.refreshShipmentList();
            dfd.resolve();
        });

        return dfd;
    }

    // renderShipmentWayPoint(data) {
    //     if (!_.isNil(data)) {  
    //         var node = null;
    //         if(data.waypointName == null) {
    //             node = this.i18n("Transportation_Mngm_Shipment_Title")();
    //         } else {
    //             var img = data.iconName != null ? '<img style="width:16px;" src="'+ data.iconName +'"/>' : '';    
    //             var eta = data.formatETA == null ? '-' : data.formatETA;
    //             var ne = data.formatNE == null ? '-' : data.formatNE;
    //             var html = img + data.waypointName + '<br/>'
    //                 + 'ETA ' + eta + '<br/>'
    //                 + 'NE ' + ne;

    //             node = '<div style="color:'+data.etaColor+';">' + html + '</div>';
    //         }

            
    //         return node; 
    //     }
    //     else {
    //         return "";
    //     }
    // };

    onDatasourceRequestRead(gridOption) {
        var dfd = $.Deferred();

        var filter = Object.assign({}, this.filter(), gridOption);

        this.isBusy(true);
        this.webRequestShipment
          .listShipmentSummary(filter)
          .done(response => {
        
              var formatData = this.formatData(response);
              this.txtRefreshTime(response.formatTimestamp);

              for(let index in formatData["items"]) {
                let item = formatData["items"][index];
                item.shipmentStatusIdUpdate = item.shipmentStatusId;
              }
              
              dfd.resolve({
                  items: formatData["items"],
                  totalRecords: response["totalRecords"],
                  currentPage : response["currentPage"]
              });
          })
          .fail(e => {
              this.handleError(e);
          })
          .always(() => {
              this.isBusy(false);
              this.onAutoRefresh(true);
          });

        return dfd;
    }

    onDetailInit(evt) {
        var self = this;
        var shipmentID = evt.data.shipmentId;
        this.webRequestShipment
          .getShipmentDetail(evt.data.shipmentId)
          .done(response => {
              self.formatDetailData(response);
              $("<div id='" + self.id + shipmentID + "'/>")
                .appendTo(evt.detailCell)
                .kendoGrid({
                    dataSource: response,
                    noRecords: {
                        template: self.i18n("M111")()
                    },
                    pageable: GenerateDataGridExpand.getPageable(false),
                    // set column of kendo grid
                    columns: GenerateDataGridExpand.getColumns(
                        self.generateColumnDetail()
                    )
                });
          })
          .fail(e => {
              this.handleError(e);
          })
          .always(() => {
              this.isBusy(false);
          });
    }

    generateColumnDetail() {
        var self = this;
        var columnsDetail = [
            {
                type: "text",
                width: "15px",
                title: this.i18n("Transportation_Mngm_Shipment_Order")(),
                data: "order",
                className: "dg-body-right"
            },
            {
                type: "object",
                width: "40px",
                title: this.i18n("Transportation_Mngm_Shipment_Waypoint_Status")(),
                template: this.renderWaypointStatus
            },
            {
                type: "object",
                width: "50px",
                title: this.i18n("Transportation_Mngm_Shipment_Waypoint")(),
                template: this.renderWaypoint
            },
            {
                type: "text",
                title: this.i18n("Transportation_Mngm_Shipment_Plan")(),
                columns: [
                    {
                        type: "object",
                        width: "30px",
                        title: this.i18n("Transportation_Mngm_Shipment_Arrive")(),
                        template: this.renderPlanArriveDate
                    },
                    {
                        type: "object",
                        width: "30px",
                        title: this.i18n("Transportation_Mngm_Shipment_Depart")(),
                        template: this.renderPlanDepartDate
                    },
                    {
                        type: "object",
                        width: "30px",
                        title: this.i18n("Transportation_Mngm_Shipment_Summary_Service_Time")(),
                        template: this.renderPlanStop
                    }
                ]
            },
            {
                type: "text",
                title: this.i18n("Transportation_Mngm_Shipment_Actual")(),
                columns: [
                    {
                        type: "object",
                        width: "30px",
                        title: this.i18n("Transportation_Mngm_Shipment_Arrive")(),
                        template: this.renderActualArriveDate
                    },
                    {
                        type: "object",
                        width: "30px",
                        title: this.i18n("Transportation_Mngm_Shipment_Depart")(),
                        template: this.renderActualDepartDate
                    },
                    {
                        type: "object",
                        width: "30px",
                        title: this.i18n("Transportation_Mngm_Shipment_Summary_Service_Time")(),
                        template: this.renderActualStop
                    }
                ]
            },
            {
                type: "text",
                width: "200px",
                title: this.i18n("Transportation_Mngm_Shipment_Delivery_Order")(),
                data: "doDetail",
                onClick: function (e) {

                    let params = {
                        waypointId: e.id
                        //shipmentId: data.shipmentId,
                        //shipmentStatusId: data.shipmentStatusId
                    }
                    self.navigate("cw-shipment-list-do-waypoint", params);
                }
            }
        ];
        return columnsDetail;
    }

    onAutoRefresh(reCall) {
        if (reCall) {
            clearTimeout(this.timeOutHandler);
            this.onAutoRefresh();
            return;
        }

        if (this.isAutoRefresh()) {
            this.timeOutHandler = setTimeout(() => {
                this.refreshShipmentList();
                this.onAutoRefresh();
            }, WebConfig.appSettings.shipmentAutoRefreshTime);
        } else {
            clearTimeout(this.timeOutHandler);
        }
    }

    prepareViewOnMapData(result) {

        result.driverName = result.driverName.trim();

        var viewonmapData = {
            panel: {
                jobName: _.isNil(result.jobName) ? "-" : result.jobName,
                driverName: _.isNil(result.driverName) ? "-" : result.driverName,
                jobCode: _.isNil(result.jobCode) ? "-" : result.jobCode,
                jobStatusDisplayName: _.isNil(result.jobStatusDisplayName) ? "-" : result.jobStatusDisplayName,
                vehicleLicense: _.isNil(result.vehicleLicense) ? "-" : result.vehicleLicense,
                
            },
            menu: {
                //disable clear layer
                disableClearGraphic: true,
                //for set menu show or hide
                show: true,
                //for identify menu
                key: "shipment-legend"
            },
            actual: {
                pin: [],
                route: []
            },
            plan: {
                pin: [],
                route: []
            },
            waiting: {
                pin: [],
                route: []
            },
            vehicle: []
        };

        _.forEach(result.shipmentPlanInfos, (planInfo) => {
            var pinItem = {
                longitude: planInfo.longitude,
                latitude: planInfo.latitude,
                attributes: {
                    areaInfo: planInfo.areaInfo,
                    radius:planInfo.radius,
                    //areaInfo:[
                    //    {
                    //        points:"[[[100.55816292599839,13.71647823230567],[100.55735040315894,13.719139427571614],[100.55563301209422,13.71964534343447],[100.5572106951386,13.720544954362351],[100.55558178907002,13.72137537060368],[100.55564883800605,13.723079424904089],[100.55711720641287,13.72314739113497],[100.55645418000948,13.72186717424234],[100.55805831992888,13.721123289342666],[100.55837122066904,13.719265235623677],[100.56020133190252,13.71884833612533],[100.5594762815935,13.7169318264386],[100.55816292599839,13.71647823230567]],[[100.55890220543682,13.717812660795945],[100.55912357032466,13.71850472646356],[100.55912357032466,13.71850472646356],[100.55890220543682,13.717812660795945]]]"
                    //    ,type:{
                    //        symbolBorderColor: "46,191,36,0.4",
                    //        symbolFillColor: "46,191,36,0.2"
                    //    }
                    //    ,name:"Waypoint"
                    //    }],
                    jobWaypointStatus: planInfo.jobWaypointStatus,
                    jobWaypointName: planInfo.jobWaypointName,
                    info: [
                        {
                            "title": this.i18n("waypoint-info-status")(),
                            "key": "jobWaypointStatusDisplayName",
                            "text": planInfo.jobWaypointStatusDisplayName || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-planIncomingDate")(),
                            "key": "formatPlanIncomingDate",
                            "text": planInfo.formatPlanIncomingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-actualIncomingDate")(),
                            "key": "formatActualIncomingDate",
                            "text": planInfo.formatActualIncomingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-actualOutgoingDate")(),
                            "key": "formatActualOutgoingDate",
                            "text": planInfo.formatActualOutgoingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-remark")(),
                            "key": "jobDescription",
                            "text": planInfo.jobDescription || '-'
                        }
                    ]
                }
            };
            viewonmapData.plan.pin.push(pinItem);
            viewonmapData.plan.route = viewonmapData.plan.route.concat(planInfo.shipmentLocations);
        });

        _.forEach(result.shipmentActualInfos, (actualInfo) => {
            var pinItem = {
                longitude: actualInfo.longitude,
                latitude: actualInfo.latitude,
                attributes: {
                    jobWaypointStatus: actualInfo.jobWaypointStatus,
                    jobWaypointName: actualInfo.jobWaypointName,
                    info: [
                        {
                            "title": this.i18n("waypoint-info-status")(),
                            "key": "jobWaypointStatusDisplayName",
                            "text": actualInfo.jobWaypointStatusDisplayName || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-planIncomingDate")(),
                            "key": "formatPlanIncomingDate",
                            "text": actualInfo.formatPlanIncomingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-actualIncomingDate")(),
                            "key": "formatActualIncomingDate",
                            "text": actualInfo.formatActualIncomingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-actualOutgoingDate")(),
                            "key": "formatActualOutgoingDate",
                            "text": actualInfo.formatActualOutgoingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-remark")(),
                            "key": "jobDescription",
                            "text": actualInfo.jobDescription || '-'
                        }
                    ]
                }
            };
         //   console.log("viewonmapData.actual.route",viewonmapData.actual.route);
            viewonmapData.actual.pin.push(pinItem);
            viewonmapData.actual.route = viewonmapData.actual.route.concat(actualInfo.shipmentActualRouteInfos);
        });

        //waitingLocataionInfos
        if(_.size(result.waitingLocataionInfos.items)){
            

            var route = new Array();
            let arrSize = _.size(result.waitingLocataionInfos.items);
            let beginLat = result.waitingLocataionInfos.items[0].latitude;
            let beginLon = result.waitingLocataionInfos.items[0].longitude;
            let beginAttributes = this.generateAttributes(result.waitingLocataionInfos.items[0]);
            let endLat = result.waitingLocataionInfos.items[arrSize-1].latitude;
            let endLon = result.waitingLocataionInfos.items[arrSize-1].longitude;
            let endAttributes = this.generateAttributes(result.waitingLocataionInfos.items[arrSize-1]);

            _.forEach(result.waitingLocataionInfos.items, (item) => {
                route.push({
                    lat: item.latitude,
                    lon: item.longitude,
                    attributes: this.generateAttributes(item),
                    direction: item.direction,
                    movement: item.movement
                });
            });

            viewonmapData.waiting.pin = [
                {
                    latitude: beginLat,
                    longitude: beginLon,
                    attributes: beginAttributes,
                },
                {
                    latitude: endLat,
                    longitude: endLon,
                    attributes: endAttributes,
                }
            ];
            viewonmapData.waiting.route = route;
            
        }

        
        

        var directionConst = ["N", "NE", "E", "SE", "S", "SW", "W", "NW"];
        _.forEach(result.locationInfo, (locationInfo) => {

            AssetInfo.getStatuses(locationInfo,WebConfig.userSession);
            var locationInfoStatus = AssetInfo.getLocationInfoStatus(locationInfo);
            var shipmentInfo = AssetInfo.getShipmentInfo(locationInfo, locationInfoStatus);

            locationInfo.vehicleLicense = _.isNil(locationInfo.vehicleLicense) ? "-" : locationInfo.vehicleLicense;
            
            var vehicleItem = {
                longitude: locationInfo.longitude,
                latitude: locationInfo.latitude,
                attributes: {
                    "TITLE": locationInfo.vehicleLicense,
                    "SHIPMENTINFO": shipmentInfo
                }
            };

            var vehicleIcon = result.vehicleIconPath[locationInfo.vehicleIconId];
            var movementImageUrl = "";
            _.forEach(result.vehicleIconPath[locationInfo.vehicleIconId], (vehicleIcon) => {
                if (vehicleIcon.movementType == locationInfo.movement) {
                    movementImageUrl = vehicleIcon.imageUrl + "/" + directionConst[locationInfo.directionType - 1] + ".png"
                }
            });
            vehicleItem.movementImageUrl = Utility.resolveUrl(movementImageUrl);
            viewonmapData.vehicle.push(vehicleItem);
        });

        // //check visible route
        // if(viewonmapData.actual.route.length){
        //     viewonmapData.panel.visible.push("actual");
        // }

        // if(viewonmapData.plan.route.length){
        //     viewonmapData.panel.visible.push("plan");
        // }

        // if(viewonmapData.waiting.route.length){
        //     viewonmapData.panel.visible.push("waiting");
        // }

        MapManager.getInstance().clear();
        MapManager.getInstance().openShipmentLegend({ data: viewonmapData });
    }

    refreshShipmentList() {
        this.dispatchEvent("dgShipmentLst", "refresh");
    }

    setFormatDateTime(dateObj, time) {
        var newDateTime = "";

        if (dateObj) {
            if (typeof dateObj.getMonth === "function") {
                let d = new Date();
                let year = d.getFullYear().toString();
                let month = (d.getMonth() + 1).toString();
                let day = d.getDate().toString();

                month = month.length > 1 ? month : "0" + month;
                day = day.length > 1 ? day : "0" + day;
                newDateTime = year + "-" + month + "-" + day + "T" + time;
            } else {
                let newDate = dateObj.split("T")[0] + "T";
                let newTime = (dateObj.split("T")[1] = time);
                newDateTime = newDate + newTime;
            }
        }
        return newDateTime;
    }

    formatData(response) {
        var data = {
            items: []
        };
        for (let i = 0; i < response.items.length; i++) {
            var dataItem = response.items[i];

            dataItem.formatWaypoint = {
                waypointName: dataItem.waypointName,
                formatETA: dataItem.formatETA,
                formatNE: dataItem.formatNE,
                iconName: dataItem.icon.name,
                etaColor: dataItem.etaColor
            };

            dataItem.formatDisplayStartDate = {
                formatPlanStartDate: dataItem.formatPlanStartDate,
                formatActualStartDate: dataItem.formatActualStartDate,
                startLateColor: dataItem.startLateColor
            };

            dataItem.formatDisplayEndDate = {
                formatPlanEndDate: dataItem.formatPlanEndDate,
                formatActualEndDate: dataItem.formatActualEndDate,
                endLateColor: dataItem.endLateColor
            };

            dataItem.driverNameAndMobile = {
                driverName: dataItem.driverName, driverMobile: dataItem.driverMobile
            };

            data.items.push(dataItem);
        }
        return data;
    }

    formatDetailData(response) {
        for (var i = 0; i < response.length; i++) {
            response[i].renderShipmentDetailColumn = this.renderShipmentDetailColumn;
            //response[i].renderActual = this.renderActual;
            //var colorWaypoint = response[i].isCancelWaypoint == true ? "red" : "black";

            //response[i].displayJobWaypointStatusDisplayName = {
            //    text: response[i].jobWaypointStatusDisplayName,
            //    color: colorWaypoint
            //}

            //response[i].displayJobWaypointName = {
            //    text: response[i].jobWaypointName,
            //    color: colorWaypoint
            //}

            //response[i].displayFormatPlanArriveDate = {
            //    text: response[i].formatPlanArriveDate,
            //    color: colorWaypoint
            //}

            //response[i].displayFormatPlanDepartDate = {
            //    text: response[i].formatPlanDepartDate,
            //    color: colorWaypoint
            //}

            //response[i].displayPlanStop = {
            //    text: response[i].planStop,
            //    color: response[i].isCancelWaypoint == true ? "black" : "red"
            //}

            //response[i].displayActualArriveDate = {
            //    text: response[i].formatActualArriveDate,
            //    color : response[i].actualColor
            //}

            //response[i].displayActualDepartDate = {
            //    text: response[i].formatActualDepartDate,
            //    color: response[i].actualColor
            //}

            //response[i].displayActualStop = {
            //    text: response[i].actualStop,
            //    color: response[i].actualColor
            //}
        }
    }

    renderWaypointStatus(data) {
        return data.renderShipmentDetailColumn({
            text: data.jobWaypointStatusDisplayName,
            color: data.jobWaypointNameColor
        });
    }

    renderWaypoint(data) {
        return data.renderShipmentDetailColumn({
            text: data.jobWaypointName,
            color: data.jobWaypointNameColor
        });
    }

    renderPlanArriveDate(data) {
        return data.renderShipmentDetailColumn({
            text: data.formatPlanArriveDate,
            color: data.planColor
        });
    }

    renderPlanDepartDate(data) {
        return data.renderShipmentDetailColumn({
            text: data.formatPlanDepartDate,
            color: data.planColor
        });
    }

    renderPlanStop(data) {
        return data.renderShipmentDetailColumn({
            text: data.planStopTime,
            color: data.planColor
        });
    }

    renderActualArriveDate(data) {
        return data.renderShipmentDetailColumn({
            text: data.formatActualArriveDate,
            color: data.actualColor
        });
    }

    renderActualDepartDate(data) {
        return data.renderShipmentDetailColumn({
            text: data.formatActualDepartDate,
            color: data.actualColor
        });
    }

    renderActualStop(data) {
        return data.renderShipmentDetailColumn({
            text: data.actualTimeInsideTime,
            color: data.actualColor
        });
    }

    renderShipmentDetailColumn(data) {
        var renderColumn = "";
        if (data.text != "" && data.text != null) {
            renderColumn =
              "<span style='color:" + data.color + ";'>" + data.text + "</span>";
        }
        return renderColumn;
    }

    //renderActual(data) {
    //    var renderColumn = "";
    //    if (data.text != "" && data.text != null) {
    //        var color = data.color
    //        renderColumn = "<span style='color:" + color + ";'>" + data.text + "</span>";
    //    }
    //    return renderColumn;
    //}

    exportShipment() {
        this.showMessageBox(null, this.i18n("M128")(), BladeDialog.DIALOG_YESNO).done((button) => {
            switch (button) {
                case BladeDialog.BUTTON_YES:
                    this.isBusy(true);
                    this.webRequestShipment.exportShipment(this.filter()).done((response) => {
                        this.isBusy(false);
                        ScreenHelper.downloadFile(response.fileUrl);
                    }).fail((e) => {
                        this.isBusy(false);
                        this.handleError(e);
                    });
                    
                break;
            }
        });
    }

    exportShipmentDetail() {
        this.showMessageBox(null, this.i18n("M128")(), BladeDialog.DIALOG_YESNO).done((button) => {
            switch (button) {
                case BladeDialog.BUTTON_YES:
                    this.isBusy(true);
                    this.webRequestShipment.exportShipmentDetail(this.filter()).done((response)=> {
                        this.isBusy(false);
                        ScreenHelper.downloadFile(response.fileUrl);
                    }).fail((e) => {
                        this.isBusy(false);
                        this.handleError(e);
                    });                    
                break;
            }
        });
    }

    /**
       * Clear reference from DOM for garbage collection.
       * @lifecycle Called when View is unloaded.
       */
    onUnload() {
        this.isAutoRefresh(false);
        let propMenu = {
            data: {},
            menu: {
                //disable clear layer
                disableClearGraphic: false,
                //for set menu show or hide
                show: false,
                //for identify menu
                key: "shipment-legend"
            },
        }
        MapManager.getInstance().clear(this.id);
        // MapManager.getInstance().clearGraphicsId("lyr-draw-command");
        MapManager.getInstance().closeViewOnMap(propMenu);
        MapManager.getInstance().showAllTrackVehicle();

    }
    /**
       * @lifecycle Called when adjacent child (only one level) is closed
       */
    onChildScreenClosed() {
        this.selectedIndex(null);
    }

    /**
       * @lifecycle Build available action button collection.
       * @param {any} actions
       */
    buildActionBar(actions) {}

    /**
       * @lifecycle Build available commands collection.
       * @param {any} commands
       */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.ShipmentCreate)) {
            commands.push(
                this.createCommand(
                    "cmdCreate",
                    this.i18n("Common_Create")(),
                    "svg-cmd-add"
                )
            );
        }
        
        commands.push(
          this.createCommand(
            "cmdFilter",
            this.i18n("Common_Search")(),
            "svg-cmd-search"
          )
        );

        //พี่อี้ดบอกว่า Import ให้ใช้Permission เดียวกับCreate
        if (WebConfig.userSession.hasPermission(Constants.Permission.ShipmentCreate)) {
            commands.push(
                this.createCommand(
                    "cmdImport",
                    this.i18n("Common_Import")(),
                    "svg-cmd-import"
                )
            );
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.ShipmentCreate)) {
            commands.push(
                this.createCommand(
                    "cmdImportWithTemplate",
                    this.i18n("Transportation_Mngm_Import_With_Template")(),
                    "svg-cmd-import"
                )
            );
        }
   
        commands.push(
          this.createCommand(
            "cmdExport",
            this.i18n("Transportation_Mngm_Export_Shipment")(),
            "svg-cmd-export"
          )
        );
        commands.push(
          this.createCommand(
            "cmdExportWithDetail",
            this.i18n("Transportation_Mngm_Export_ShipmentDetail")(),
            "svg-cmd-export"
          )
        );
        commands.push(
          this.createCommand(
            "cmdRefresh",
            this.i18n("Common_Refresh")(),
            "svg-cmd-refresh"
          )
        );
       
    }

    /**
       * @lifecycle Handle when button on ActionBar is clicked.
       * @param {any} commands
       */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
       * @lifecycle Hangle when command on CommandBar is clicked.
       * @param {any} sender
       */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdExport":
                this.exportShipment();
                break;
            case "cmdExportWithDetail":
                this.exportShipmentDetail();
                break;
            case "cmdFilter":
                this.navigate("cw-shipment-list-search", {});
                break;
            case "cmdRefresh":
                this.refreshShipmentList();
                this.onAutoRefresh(true);
                break;
            case "cmdCreate":
                this.navigate("cw-shipment-manage", {});
                break;
            case "cmdImport":
                this.navigate("cw-shipment-import", {});
                break;
            case "cmdImportWithTemplate":
                this.navigate("cw-shipment-import-with-template", {});
                break;
            default:
                break;
        }
    }

    getDataOnDashboard() {
        if (this.statusDashboard() != undefined) {
            this.filter(this.statusDashboard());
            if(this.statusDashboard().isHistorical) {
                this.filter()['shipmentFromDate'] = this.statusDashboard().dateFrom;
                this.filter()['shipmentToDate'] = this.statusDashboard().dateTo;
            } else {
                this.filter()['shipmentFromDate'] = this.setFormatDateTime(new Date(), "00:00:00");
                this.filter()['shipmentToDate'] = this.setFormatDateTime(new Date(), "23:59:59");                
            }
            this.refreshShipmentList();
        }
        

    }

    /**
     * Create attributes object.
     * 
     * @param {any} data
     * @returns
     * 
     * @memberOf PlaybackTimelineScreen
     */
    generateAttributes(data) {
        var isVehicle = data.vehicleId ? true : false;
        var datID = isVehicle ? "V-" + data.vehicleId + "-PB" : "D-" + data.deliveryManId + "-PB";
        var title = isVehicle ? data.vehicleLicense : data.username;
        var businessUnitName = "";

        // Detect business unit name based on vehicle or delivery men.
        if (isVehicle) {
            businessUnitName = _.map(_.filter(this.selectedVehicles, {
                id: data.vehicleId
            }), "businessUnitName");
        } else {
            businessUnitName = _.map(_.filter(this.selectedDeliveryMen, {
                id: data.deliveryManId
            }), "businessUnitName");
        }

        // Compose map attribute object.
        var attr = {
            ID: datID,
            TITLE: title,
            BOX_ID: data.vehicleId ? data.boxSerialNo : data.email,
            DRIVER_NAME: data.driverName,
            DEPT: businessUnitName,
            DATE: data.formatDateTime,
            TIME: Utility.getOnlyTime(data.dateTime, "HH:mm"),
            PARK_TIME: data.parkDuration,
            PARK_IDLE_TIME: data.idleTime,
            LOCATION: data.location,
            RAW: data,
            STATUS: AssetInfo.getStatuses(data, WebConfig.userSession),
            IS_DELAY: data.isDelay
        };

        attr.SHIPMENTINFO = AssetInfo.getShipmentInfo(data, attr.STATUS);

        return attr;
    }

    get webRequestShipment() {
        return WebRequestShipment.getInstance();
    }

    get webRequestUser() {
        return WebRequestUser.getInstance();
    }
}

export default {
viewModel: ScreenBase.createFactory(ShipmentListScreen),
    template: templateMarkup
};

﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import Utility from "../../../../../app/frameworks/core/utility";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestShipmentZone from "../../../../../app/frameworks/data/apitrackingcore/WebRequestShipmentZone";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import { Enums, Constants } from "../../../../../app/frameworks/constant/apiConstant";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";

/**
 * Display available subscription menu based on user permission.
 * @class MenuScreen
 * @extends {ScreenBase}
 */
class ShipmentZoneManage extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeSize = BladeSize.Small;
        this.mode = this.ensureNonObservable(params.mode);
        this.id = this.ensureNonObservable(params.id, 0);

        this.enableCode = ko.observable();
        switch (params.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.enableCode(true);
                this.bladeTitle(this.i18n("ShipmentZone_Create")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.enableCode(false);
                this.bladeTitle(this.i18n("ShipmentZone_Update")());
                break;
        }
        this.code = ko.observable();
        this.name = ko.observable();
        this.enableOptions = ScreenHelper.createYesNoObservableArray();
        this.enableJobZone = ko.observable(ScreenHelper.findOptionByProperty(this.enableOptions, "value", true));

        this.order = ko.observable([[1, "asc"]]);
        this.filterText = ko.observable("");
        this.recentChangedRowIds = ko.observableArray([]);


    }

    /**
     * Get WebRequest Shipment Category
     * @readonly
     */
    get webRequestShipmentZone() {
        return WebRequestShipmentZone.getInstance();
    }

    buildCommandBar(commands) {
       
    }

    onCommandClick(sender) {
       
    }

    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }


    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            var model = this.generateModel();
            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.isBusy(true);
                    this.webRequestShipmentZone.createShipmentZone(model).done((r) => {
                        this.publishMessage("cw-shipment-zone-changed", r.id);
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.isBusy(true);
                    this.webRequestShipmentZone.updateShipmentZone(model).done(() => {
                        this.publishMessage("cw-shipment-zone-changed", this.id);
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });
                    break;
            }
        }
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var dfd = $.Deferred();
        this._originalAccessibleBusinessUnits = [];

        if (this.mode == Screen.SCREEN_MODE_UPDATE) {
            var w1 = this.webRequestShipmentZone.getShipmentZone(this.id);
            $.when(w1).done((res) => {
                if (res) {
                    this.code(res.code);
                    this.name(res.name);
                    this.enableJobZone(ScreenHelper.findOptionByProperty(this.enableOptions, "value", res.enable));
                }
                dfd.resolve();
            }).fail((e) => {
                dfd.reject();
                this.handleError(e);
            });
        }
        else {
            dfd.resolve();
        }

        return dfd;
    }

    setupExtend() {
        this.code.extend({
            trackChange: true
        });

        this.name.extend({
            trackChange: true
        });
        this.enableJobZone.extend({
            trackChange: true
        });
       

        //Required
        this.code.extend({
            required: true
        });

        this.name.extend({
            required: true
        });
        this.enableJobZone.extend({
            required: true
        });


        this.code.extend({
            serverValidate: {
                params: "Code",
                message: this.i18n("M010")()
            }
        });

        this.name.extend({
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });


        this.validationModel = ko.validatedObservable({
            name: this.name,
            code: this.code,
        });

    }




    generateModel() {

        let model = {
            Name: this.name(),
            code: this.code(),
            enable: this.enableJobZone().value,
            Id: this.id
        };

        return model;
    }
}

export default {
    viewModel: ScreenBase.createFactory(ShipmentZoneManage),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import { Constants } from "../../../../../app/frameworks/constant/apiConstant";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import WebRequestShipmentZone from "../../../../../app/frameworks/data/apitrackingcore/WebRequestShipmentZone";
import ScreenBase from "../../../screenbase";


/**
 * Display available subscription menu based on user permission.
 * @class MenuScreen
 * @extends {ScreenBase}
 */
class ShipmentZoneList extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("ShipmentZone_ShipmentZoneList")());
        this.bladeSize = BladeSize.XMedium;

        this.jobZoneItem = ko.observableArray([]);
        this.order = ko.observable([[1, "asc"]]);
        this.filterText = ko.observable("");
        this.recentChangedRowIds = ko.observableArray([]);

        this._selectingRowHandler = (data) => {
            return this.navigate("cw-shipment-zone-view", { id: data.id });
        };


        this.subscribeMessage("cw-shipment-zone-changed", id => {
            this.webRequestShipmentZone.listShipmentZone().done((res) => {
                this.jobZoneItem.replaceAll(res.items);
                if (id) {
                    this.recentChangedRowIds.replaceAll([id]);
                }
            }).fail((e) => {
                this.handleError(e);
            });
        });

        this.subscribeMessage("cw-shipment-zone-delete", () => {
            return this.loadFindSummary();
        });

    }

    /**
     * Get WebRequest Category
     * @readonly
     */
    get webRequestShipmentZone() {
        return WebRequestShipmentZone.getInstance();
    }


    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.ShipmentZoneCreate)) {
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }

    }

    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdCreate":
                this.navigate("cw-shipment-zone-manage", { mode: Screen.SCREEN_MODE_CREATE });
                break;
        }
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        return this.loadFindSummary();

    }

    loadFindSummary() {
        var dfd = $.Deferred();
        this.webRequestShipmentZone.listShipmentZone().done((res) => {
            this.jobZoneItem(res.items);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
}

export default {
    viewModel: ScreenBase.createFactory(ShipmentZoneList),
    template: templateMarkup
};
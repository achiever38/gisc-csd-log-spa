﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import { Constants } from "../../../../../app/frameworks/constant/apiConstant";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import WebRequestShipmentZone from "../../../../../app/frameworks/data/apitrackingcore/WebRequestShipmentZone";


/**
 * Display available subscription menu based on user permission.
 * @class MenuScreen
 * @extends {ScreenBase}
 */
class ShipmentZoneView extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeSize = BladeSize.Small;
        this.id = this.ensureNonObservable(params.id,0);

        this.bladeTitle(this.i18n("ShipmentZone_View")());

        this.code = ko.observable();
        this.name = ko.observable();
        this.enableJobZone = ko.observable();
        this.updateDate = ko.observable();
        this.updateBy = ko.observable();


        this.subscribeMessage("cw-shipment-zone-changed", id => {
            return this.getShipmentZone(id);
        });
    }

    /**
     * Get WebRequest Shipment Category
     * @readonly
     */
    get webRequestShipmentZone() {
        return WebRequestShipmentZone.getInstance();
    }


    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.ShipmentZoneUpdate)) {
            commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.ShipmentZoneDelete)) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }

    onCommandClick(sender) {
        switch (sender.id) {
            case 'cmdUpdate':
                this.navigate("cw-shipment-zone-manage", { id: this.id, mode: Screen.SCREEN_MODE_UPDATE });
                break;
            case 'cmdDelete':
                this.showMessageBox(null, this.i18n("M238")(),
                    BladeDialog.DIALOG_YESNO).done((button) => {
                        switch (button) {
                            case BladeDialog.BUTTON_YES:
                                this.isBusy(true);

                                this.webRequestShipmentZone.deleteShipmentZone(this.id).done(() => {
                                    this.isBusy(false);

                                    this.publishMessage("cw-shipment-zone-delete");
                                    this.close(true);
                                }).fail((e) => {
                                    this.handleError(e);
                                    this.isBusy(false);
                                });
                                break;
                        }
                    });
                break;
            default:
        }
    }

    buildActionBar(actions) {
        
    }

    onActionClick(sender) {
        
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        return this.getShipmentZone(this.id);
    }

    getShipmentZone(id) {

        var dfd = $.Deferred();
        this.webRequestShipmentZone.getShipmentZone(this.id).done((res) => {
            if (res) {
                this.code(res.code);
                this.name(res.name);
                this.enableJobZone(res.formatEnable);
                this.updateDate(res.formatUpdateDate);
                this.updateBy(res.updateBy);
            }
            dfd.resolve();
        }).fail((e) => {
            dfd.reject();
            this.handleError(e);
        });
 
        return dfd;
    }

    setupExtend() {
    }

}

export default {
    viewModel: ScreenBase.createFactory(ShipmentZoneView),
    template: templateMarkup
};
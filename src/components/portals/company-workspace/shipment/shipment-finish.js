﻿import ko from "knockout";
import templateMarkup from "text!./shipment-finish.html";
import ScreenBase from "../../screenbase";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import { Constants, EntityAssociation, Enums } from "../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import WebRequestShipment from "../../../../app/frameworks/data/apitrackingcore/webRequestShipment";
import WebRequestEnumResource from "../../../../app/frameworks/data/apicore/webRequestEnumResource";

class ShipmentFinish extends ScreenBase {

    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Transportation_Mngm_Finish_Shipment_Title")());

        this.bladeMargin = BladeMargin.Normal;
        this.bladeSize = BladeSize.XLarge_A1;
        this.bladeIsMaximized = true;

        //this.shipmentId = this.ensureNonObservable(params.shipmentId, 24989);
        this.shipmentId = this.ensureNonObservable(params.shipmentId);

        this.OtherReason = this.ensureNonObservable(6);
        this.ShipmentWpCancelled = this.ensureNonObservable(Enums.JobWaypointStatus.Cancelled);

        /* List Array */
        this.lstCancelReasons = ko.observableArray([]);
        this.shipmentStatusList = ko.observableArray([]);
        this.mainReasons = ko.observableArray([]);
        this.lstIncompleteWaypoint = ko.observableArray([]);

        /* Observable Params */
        this.mainReason = ko.observable();
        this.mainReasonDetail = ko.observable();

        /* Display Condition */
        this.isShowMainOtherReason = ko.pureComputed(function() {
            return this.mainReason() && this.mainReason().id == this.OtherReason;
        }, this);

        this.subscribeMessage("cw-shipment-disabled-cancelfinish", () => {
            this.close(true);
        });
    }

    setupExtend() {
        let self = this;

        this.mainReason.extend({ trackChange: true });
        this.mainReasonDetail.extend({ trackChange: true });

        this.mainReason.extend({ required: true });
        this.mainReasonDetail.extend({
            required: {
                onlyIf: function () {
                    return self.mainReason() && self.mainReason().id == self.OtherReason;
                }
            }
        });

        this.validationModel = ko.validatedObservable({
            finishReason: this.mainReason,
            finishReasonDetail: this.mainReasonDetail
        });
    }
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    get webRequestShipment()
    { return WebRequestShipment.getInstance(); }

    //get OtherReason() { return 6; }

    //get ShipmentWpCancelled() { return 12; }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad)
            return;

        let self = this;
        let filter = { companyId: WebConfig.userSession.currentCompanyId };

        this.webRequestShipment.finishCauseList(filter).done((res) => {
            this.mainReasons.replaceAll(res)
        });

        let dfd1 = this.webRequestShipment.cancelCauseList(filter);
        //    .done((res) => {
        //    this.lstCancelReasons.replaceAll(res)
        //});

        var waypointStatusFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            types: [Enums.ModelData.EnumResourceType.JobWaypointStatus],
            values: [
                Enums.JobWaypointStatus.Finished,
                Enums.JobWaypointStatus.FinishedLate,
                Enums.JobWaypointStatus.FinishedIncomplete,
                Enums.JobWaypointStatus.FinishedLateIncomplete,
                Enums.JobWaypointStatus.Cancelled
             ]
        }

        let dfd2 = this.webRequestEnumResource.listEnumResource(waypointStatusFilter);
        //    .done((response) => {
        //    this.shipmentStatusList(response["items"]);
        //});

        $.when(dfd1, dfd2).done((res1, res2) => {

            let lstCancelReasons = res1;
            let waypointStatusList = res2["items"];

            this.webRequestShipment.getIncompleteWaypointShipment(this.shipmentId).done((res) => {
                let lstData = [];

                ko.utils.arrayForEach(res, function (itm) {
                    let data = {
                        wpId: itm.id,
                        name: ko.observable(itm.jobWaypointName),
                        reason: ko.observable(null),
                        planEndDate: ko.observable(itm.formatPlanDepartDate),
                        cancelReason: ko.observable(null),
                        otherCancelDetail: ko.observable(null),
                        actualEndDate: ko.observable(itm.planDepartDate),
                        actualEndTime: ko.observable(itm.planDepartDate.split('T')[1]),
                        shipmentStatusList: ko.observableArray(waypointStatusList),
                        lstCancelReasons: ko.observableArray(lstCancelReasons)
                    };

                    data.reason.extend({ required: true });
                    data.actualEndDate.extend({ required: true });
                    data.actualEndTime.extend({ required: true });

                    data.cancelReason.extend({
                        required: {
                            onlyIf: function () {
                                return data.reason() && (data.reason().value == self.ShipmentWpCancelled)
                            }
                        }
                    });

                    data.otherCancelDetail.extend({
                        required: {
                            onlyIf: function () {
                                return data.cancelReason() && (data.cancelReason().value == self.OtherReason)
                            }
                        }
                    });

                    lstData.push(data);
                });

                this.lstIncompleteWaypoint.replaceAll(lstData);

            }).fail((err) => {

            });


        });
    }

    checkReason(reason) {
        return reason && (reason.value == this.ShipmentWpCancelled)
    }

    checkCancelReason(cancelReason) {
        return cancelReason && (cancelReason.id == this.OtherReason)
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() { }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
            else {
                let validate = {};

                ko.utils.arrayForEach(this.lstIncompleteWaypoint(), function (itm, ind) {
                    validate[`reason_${ind}`] = itm.reason;
                    validate[`actualEndDate_${ind}`] = itm.actualEndDate;
                    validate[`actualEndTime_${ind}`] = itm.actualEndTime;
                    validate[`cancelReason_${ind}`] = itm.cancelReason;
                    validate[`otherCancelDetail_${ind}`] = itm.otherCancelDetail;
                });
                let validateKo = ko.validatedObservable(validate);

                if (!validateKo.isValid()) {
                    validateKo.errors.showAllMessages();
                    return;
                }

                this.isBusy(true);

                //จัด parameter สำหรับ save
                let finish_cause = "";
                let wp = [];

                if (this.isShowMainOtherReason()) { finish_cause = `${this.mainReason().cause}-${this.mainReasonDetail()}`; }
                else { finish_cause = this.mainReason().cause;}

                ko.utils.arrayForEach(this.lstIncompleteWaypoint(), function (itm, ind) {
                    let obj = {
                        jobWaypointId: itm.wpId,
                        status: itm.reason().value,
                        endDateTime: "",
                        cause: ""
                    }

                    if (this.checkCancelReason(itm.cancelReason())) { obj.cause = `${itm.cancelReason().cause}-${itm.otherCancelDetail()}`; }
                    else { obj.cause = itm.cancelReason(); }

                    let endDate = itm.actualEndDate().split('T')[0];
                    let endTime = itm.actualEndTime();

                    obj.endDateTime = `${endDate}T${endTime}`;

                    wp.push(obj);
                }.bind(this));

                let paramSave = {
                    jobId: this.shipmentId,
                    waypoints: wp,
                    cause: finish_cause
                };

                this.webRequestShipment.saveFinishShipment(paramSave).done(() => {
                    this.publishMessage("cw-shipment-manage-finish");
                    this.isBusy(false);
                    this.close(true);
                }).fail((e) => {
                    this.handleError(e);
                    this.isBusy(false);
                }).always(() => {
                    this.isBusy(false);
                });
            }
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {

    }
}

export default {
    viewModel: ScreenBase.createFactory(ShipmentFinish),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import Utility from "../../../../../app/frameworks/core/utility";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestShipmentCategory from "../../../../../app/frameworks/data/apitrackingcore/WebRequestShipmentCategory";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";

/**
 * Display available subscription menu based on user permission.
 * @class MenuScreen
 * @extends {ScreenBase}
 */
class ShipmentCategoryManage extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeSize = BladeSize.Small;
        this.mode = this.ensureNonObservable(params.mode);
        this.id = this.ensureNonObservable(params.id,0);

        this.enableCode = ko.observable();
        switch (params.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.enableCode(true);
                this.bladeTitle(this.i18n("ShipmentCategory_Create")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.enableCode(false);
                this.bladeTitle(this.i18n("ShipmentCategory_Update")());
                break;
        }
        this.code = ko.observable();
        this.name = ko.observable();
        this.enableOptions = ScreenHelper.createYesNoObservableArray();
        this.enableJobCate = ko.observable(ScreenHelper.findOptionByProperty(this.enableOptions, "value", true)); 
        this.jobCateItem = ko.observableArray([]);
        this.description = ko.observable();
        this.lstBu = ko.observableArray([]);
        this.accessibleBusinessUnits = ko.observableArray([]);
        this.orderAccessibleBusinessUnits = ko.observable([
            [1, "asc"]
        ]);

        this.order = ko.observable([[1, "asc"]]);
        this.filterText = ko.observable("");
        this.selectedDefaultValue = ko.observable(null);
        this.recentChangedRowIds = ko.observableArray([]);


        // Observe change about Accessible Business Units
        this.subscribeMessage("cw-shipment-category-accessible-business-unit-selected", (selectedBusinessUnits) => {
            this.lstBu(selectedBusinessUnits);
            var cloneObj = _.clone(selectedBusinessUnits);
            var mappedSelectedBUs = _.filter(cloneObj, (bu) => {
                return bu.id > 0;
                //if (bu.id > 0) {
                //    let mbu = bu;
                //    mbu.businessUnitId = bu.id;
                //    mbu.businessUnitName = bu.name;
                //    mbu.businessUnitPath = bu.businessUnitPath;
                //    mbu.canManage = true;

                //    return mbu;
                //}
            });
          
            this.accessibleBusinessUnits.replaceAll(mappedSelectedBUs);
        });
    }

    /**
     * Get WebRequest Shipment Category
     * @readonly
     */
    get webRequestShipmentCategory() {
        return WebRequestShipmentCategory.getInstance();
    }

    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    buildCommandBar(commands) {
       
    }

    onCommandClick(sender) {
 
    }

    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }


    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            var model = this.generateModel();
            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.isBusy(true);
                    this.webRequestShipmentCategory.createShipmentCategory(model, true).done((r) => {
                        this.publishMessage("cw-shipment-cate-changed", r.id);
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.isBusy(true);
                    this.webRequestShipmentCategory.updateShipmentCategory(model, true).done(() => {
                        this.publishMessage("cw-shipment-cate-changed", this.id);
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });
                    break;
            }
        }
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var dfd = $.Deferred();
        this._originalAccessibleBusinessUnits = [];

        if (this.mode == Screen.SCREEN_MODE_UPDATE) {
            var w1 = this.webRequestShipmentCategory.getShipmentCategory(this.id); 
            var w2 = this.webRequestBusinessUnit.listBusinessUnitParentSummary({ companyId: WebConfig.userSession.currentCompanyId });
            $.when(w1,w2).done((res,res2) => {
                if (res) {
                    this.code(res.code);
                    this.name(res.name);
                    this.enableJobCate(ScreenHelper.findOptionByProperty(this.enableOptions, "value", res.enable));
                    this.description(res.description);
                    this.accessibleBusinessUnits(res.shipmentCategoryAccessibleInfos);

                    let cloneCateAcc = _.clone(res.shipmentCategoryAccessibleInfos);
                    if (cloneCateAcc.length > 0 && cloneCateAcc.length == res2.items.length-1) {
                        cloneCateAcc.unshift({ id: 0,name : "All"});
                    }
                    this.lstBu(cloneCateAcc); // use to accessible Bu
                    this._originalAccessibleBusinessUnits = _.cloneDeep(res.shipmentCategoryAccessibleInfos);
                }
                    dfd.resolve();
                }).fail((e) => {
                    dfd.reject();
                    this.handleError(e);
                });;
            }
            else {
                dfd.resolve();
            }

            return dfd;
    }

    setupExtend() {
        this.code.extend({
            trackChange: true
        });

        this.name.extend({
            trackChange: true
        });
        this.enableJobCate.extend({
            trackChange: true
        });
        this.accessibleBusinessUnits.extend({
            trackArrayChange: true
        });
        this.description.extend({
            trackChange: true
        });

        //Required
        this.code.extend({
            required: true
        });

        this.name.extend({
            required: true
        });
        this.enableJobCate.extend({
            required: true
        });
        this.accessibleBusinessUnits.extend({
            arrayRequired: true
        });


        this.code.extend({
            serverValidate: {
                params: "Code",
                message: this.i18n("M010")()
            }
        });

        this.name.extend({
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });


        this.validationModel = ko.validatedObservable({
            name: this.name,
            code: this.code,
            description: this.description,
            accessibleBusinessUnits: this.accessibleBusinessUnits,
        });

    }


   

    addAccessibleBusinessUnit() {
        var selectedBusinessUnitIds = this.lstBu().map((obj) => {
            return _.toString(obj.id);
        });
        this.navigate("cw-shipment-category-manage-accessible-business-unit-select", {
            mode: this.mode,
            selectedBusinessUnitIds: selectedBusinessUnitIds
        });
    }


    generateModel() {

        let model = {
            Name: this.name(),
            code: this.code(),
            description: this.description(),
            enable: this.enableJobCate().value,
            Id: this.id
        };
        model.shipmentCategoryAccessibleInfos = Utility.generateArrayWithInfoStatus(
            this._originalAccessibleBusinessUnits,
            this.accessibleBusinessUnits()
        );

        return model;
    }
}

export default {
    viewModel: ScreenBase.createFactory(ShipmentCategoryManage),
    template: templateMarkup
};
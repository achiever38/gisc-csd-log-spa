﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import ScreenBase from "../../screenbase";
import ScreenHelper from "../../screenhelper";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../app/frameworks/constant/bladeDialog";
import { Constants, Enums, EntityAssociation } from "../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../app/frameworks/core/utility";

import WebRequestBusinessUnit from "../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestEnumResource from "../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestVehicle from "../../../../app/frameworks/data/apitrackingcore/webRequestVehicle";
import WebRequestShipment from "../../../../app/frameworks/data/apitrackingcore/webRequestShipment";
import WebRequestDriver from "../../../../app/frameworks/data/apitrackingcore/webRequestDriver";
import DefaultSorting from "../../../../app/frameworks/constant/defaultSorting";
import WebRequestShipmentCategory from "../../../../app/frameworks/data/apitrackingcore/webRequestShipmentCategory";

/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class ShipmentListSearchScreen extends ScreenBase {

    /**
     * Creates an instance of ShipmentListSearchScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Common_Search")());
        this.bladeSize = BladeSize.Small;

        let currentDate = new Date();

        //date format
        this.dateFormat = WebConfig.companySettings.shortDateFormat;

        //ddl data source
        this.businessUnitList = ko.observableArray([]);
        this.shipmentStatusList = ko.observableArray([]);
        this.regionList = ko.observableArray([]);

        //filter variable
        this.businessUnit = ko.observable(null);
        this.includeSubBU = ko.observable(false);
        this.shipmentStatus = ko.observable(null);
        this.region = ko.observable([]);

        this.vehicle = ko.observableArray([]);
        this.driver = ko.observableArray([]);
        this.zone = ko.observableArray([]);
        this.travelMode = ko.observableArray([]);
        this.shipmentCategory = ko.observableArray([]);

        this.selectedVehicle = ko.observable();
        this.selectedDriver = ko.observable();
        this.selectedZone = ko.observable();
        this.selectedTravelMode = ko.observable();
        this.selectedShipmentCategory = ko.observable();

        this.txtShipmentCode = ko.observable();
        this.txtTemplateName = ko.observable();
        this.txtShipmentName = ko.observable();

        this.txtDOCode = ko.observable();
        this.txtDOName = ko.observable();
        this.txtCreateBy = ko.observable();

        //datetime
        var addDay = 30;
        var addMonth = 3;
        this.maxDateStart = ko.observable();
        this.minDateEnd = ko.observable();        

        this.shipmentStartDate = ko.observable(currentDate);
        this.shipmentEndDate = ko.observable(currentDate);
        this.shipmentStartTime = ko.observable('00:00');
        this.shipmentEndTime = ko.observable('23:59');

        this.minDateShipment = ko.observable(Utility.minusMonths(new Date(),addMonth));
        this.maxDateShipment = ko.observable(Utility.addMonths(new Date(),addMonth));
        
        this.minCreateDate = ko.observable(Utility.minusMonths(new Date(),addMonth));
        this.maxCreateDate = ko.observable(Utility.addMonths(new Date()));

        this.shipmentCreatedStartDate = ko.observable();
        this.shipmentCreatedEndDate = ko.observable();
        this.shipmentCreatedStartTime = ko.observable();
        this.shipmentCreatedEndTime = ko.observable();      

        this.minDODate = ko.observable(Utility.minusMonths(new Date(),addMonth));
        this.maxDODate = ko.observable(Utility.addMonths(new Date(),addMonth));

        this.shipmentDODateFrom = ko.observable();
        this.shipmentDODateTo = ko.observable();

        this.dayList = ko.observableArray([]);
        this.valueDay = ko.observable();

        this.businessUnitDriver = ko.observable();
        this.businessUnitVehicle = ko.observable();

        /*Auto Complete ShipmentCode*/
        this.onDatasourceRequestShipmentCode = (request, response) => {
            var filter = {
                shipmentName: this.txtShipmentName(),
                shipmentCode: request.term,
                doCode: this.txtDOCode(),
                doName: this.txtDOName(),
                createBy: this.txtCreateBy(),
                shipmentFromDate: this.setFormatDateTime(this.shipmentStartDate(), this.shipmentStartTime()),
                shipmentToDate: this.setFormatDateTime(this.shipmentEndDate(), this.shipmentEndTime())
            }
            this.webRequestShipment.autocompleteShipmentCode(filter).done((data) => {
                response( $.map( data, (item) => {
                    return {
                        label: item.shipmentCode,
                        value: item.shipmentCode
                    };
                }));
            }).fail(e => this.handleError(e));
        }
        /*End Auto Complete ShipmentCode*/

        /*Auto Complete ShipmentName*/
        this.onDatasourceRequestShipmentName = (request, response) => {
            var filter = {
                shipmentName: request.term,
                shipmentCode: this.txtShipmentCode(),
                doCode: this.txtDOCode(),
                doName: this.txtDOName(),
                createBy: this.txtCreateBy(),
                shipmentFromDate: this.setFormatDateTime(this.shipmentStartDate(), this.shipmentStartTime()),
                shipmentToDate: this.setFormatDateTime(this.shipmentEndDate(), this.shipmentEndTime())
            }
            this.webRequestShipment.autocompleteShipmentName(filter).done((data) => {
                response( $.map( data, (item) => {
                    return {
                        label: item.shipmentName,
                        value: item.shipmentName
                    };
                }));
            });
        }
        /*End Auto Complete ShipmentName*/

        /*Auto Complete DOCode*/
        this.onDatasourceRequestDOCode = (request, response) => {
            var filter = {
                shipmentName: this.txtShipmentName(),
                shipmentCode: this.txtShipmentCode(),
                doCode: request.term,
                doName: this.txtDOName(),
                createBy: this.txtCreateBy(),
                shipmentFromDate: this.setFormatDateTime(this.shipmentStartDate(), this.shipmentStartTime()),
                shipmentToDate: this.setFormatDateTime(this.shipmentEndDate(), this.shipmentEndTime())
            }
            this.webRequestShipment.autocompleteShipmentDOCode(filter).done((data) => {
                response( $.map( data, (item) => {
                    return {
                        label: item.doCode,
                        value: item.doCode
                    };
                }));
            });
        }
        /*End Auto Complete DOCode*/

        /*Auto Complete DOName*/
        this.onDatasourceRequestDOName = (request, response) => {
            var filter = {
                shipmentName: this.txtShipmentName(),
                shipmentCode: this.txtShipmentCode(),
                doCode: this.txtDOCode(),
                doName: request.term,
                createBy: this.txtCreateBy(),
                shipmentFromDate: this.setFormatDateTime(this.shipmentStartDate(), this.shipmentStartTime()),
                shipmentToDate: this.setFormatDateTime(this.shipmentEndDate(), this.shipmentEndTime())
            }
            this.webRequestShipment.autocompleteShipmentDOName(filter).done((data) => {
                response( $.map( data, (item) => {
                    return {
                        label: item.doName,
                        value: item.doName
                    };
                }));
            });
        }
        /*End Auto Complete DOName*/

        /*Auto Complete CreateBy*/
        this.onDatasourceRequestCreateBy = (request, response) => {
            var filter = {
                shipmentName: this.txtShipmentName(),
                shipmentCode: this.txtShipmentCode(),
                doCode: this.txtDOCode(),
                doName: this.txtDOName(),
                createBy: request.term,
                shipmentFromDate: this.setFormatDateTime(this.shipmentStartDate(), this.shipmentStartTime()),
                shipmentToDate: this.setFormatDateTime(this.shipmentEndDate(), this.shipmentEndTime())
            }
            this.webRequestShipment.autocompleteShipmentCreateBy(filter).done((data) => {
                response( $.map( data, (item) => {
                    return {
                        label: item.createBy,
                        value: item.createBy
                    };
                }));
            });
        }
        /*End Auto Complete CreateBy*/

        /*Auto Complete TemplateName*/
        this.onDatasourceRequestTemplateName = (request, response) => {
            var filter = {
                businessUnitIds: this.businessUnit(),
                vehicleIds: this.selectedVehicle(),
                regionIds: this.region(),
                travelModes: this.selectedTravelMode(),
                jobCategoryIds: this.selectedShipmentCategory(),
                templateName: request.term
            }
            this.webRequestShipment.autocompleteShipmentTemplate(filter).done((data) => {
                response( $.map( data, (item) => {
                    return {
                        label: item,
                        value: item
                    };
                }));
            });
        }
        /*End Auto Complete TemplateName*/
    }

    /* Web Request */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }
    get webRequestVehicle() {
        return WebRequestVehicle.getInstance();
    }
    get webRequestShipment() {
        return WebRequestShipment.getInstance();
    }
    get webRequestDriver() {
        return WebRequestDriver.getInstance();
    }
    get webRequestShipmentCategory() {
        return WebRequestShipmentCategory.getInstance();
    }
    /* End Web Request */

    setShipmentDateMaxMin(dateTime) {
        if(dateTime == undefined) {
            this.minDateShipment(Utility.minusMonths(new Date(), 3));
            this.maxDateShipment(Utility.addMonths(new Date(), 3));            
        }  else {
            this.minDateShipment(new Date(dateTime));
            this.maxDateShipment(new Date(dateTime));
        }  
    }

    setCreateDateMaxMin(dateTime) {
        if(dateTime == undefined) {
            this.minCreateDate(Utility.minusMonths(new Date(), 3));
            this.maxCreateDate(Utility.addMonths(new Date()));
        } else {
            this.minCreateDate(new Date(dateTime));
            this.maxCreateDate(new Date(dateTime));
        }
    }

    setDODateMaxMix(dateTime) {
        if(dateTime == undefined) {
            this.minDODate(Utility.minusMonths(new Date(), 3));
            this.maxDODate(Utility.addMonths(new Date(), 3));
        } else {
            this.minDODate(new Date(dateTime));
            this.maxDODate(new Date(dateTime));
        }
    }

    /* End Web Request */

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // TODO: Verify access right to specific items
        if (!isFirstLoad) {
            return;
        }

        // StartDate condition
        this.shipmentStartDate.subscribe((dateTime) => {
            let startDate = new Date(dateTime);
            let endDate = new Date(this.shipmentEndDate());
            if(startDate > endDate) {
                this.setShipmentDateMaxMin(dateTime);
                this.setShipmentDateMaxMin();
            }

            if((startDate.getMonth() - endDate.getMonth()) != 0) {
                this.maxDateShipment(Utility.addMonths(dateTime, 1));
                this.setShipmentDateMaxMin();
            }
        });

        // EndDate condition
        this.shipmentEndDate.subscribe((dateTime) => {
            let startDate = new Date(this.shipmentStartDate());
            let endDate = new Date(dateTime);
            if(endDate < startDate) {
                this.setShipmentDateMaxMin(dateTime);
                this.setShipmentDateMaxMin();
            }

            if((startDate.getMonth() - endDate.getMonth()) != 0) {
                this.minDateShipment(Utility.minusMonths(dateTime, 1));
                this.setShipmentDateMaxMin();
            }
        });

        // CreateDateStart condition
        this.shipmentCreatedStartDate.subscribe((dateTime) => {
            let startDate = new Date(dateTime);
            let endDate = new Date(this.shipmentCreatedEndDate());

            if(dateTime == null) {
                return
            }

            if(startDate > endDate) {
                this.setCreateDateMaxMin(dateTime);
                this.setCreateDateMaxMin();
            }

            if((startDate.getMonth() - endDate.getMonth()) != 0) {
                this.maxCreateDate(Utility.addMonths(dateTime, 1));
                this.setCreateDateMaxMin();
            }
        });

        // CreateDateEnd condition
        this.shipmentCreatedEndDate.subscribe((dateTime) => {
            let startDate = new Date(this.shipmentCreatedStartDate());
            let endDate = new Date(dateTime);

            if(dateTime == null) {
                return
            }

            if(endDate < startDate) {
                this.setCreateDateMaxMin(dateTime);
                this.setCreateDateMaxMin();
            }

            if((startDate.getMonth() - endDate.getMonth()) != 0) {
                this.minCreateDate(Utility.minusMonths(dateTime, 1));
                this.setCreateDateMaxMin();
            }
        });

        // CreateDateEnd cindition
        this.shipmentCreatedEndDate.subscribe((dateTime) => {
        
        });

        // DODateFrom condition
        this.shipmentDODateFrom.subscribe((dateTime) => {
            let startDate = new Date(dateTime);
            let endDate = new Date(this.shipmentDODateTo());
            if(startDate > endDate) {
                this.setDODateMaxMix(dateTime);
                this.setDODateMaxMix();
            }

            if((startDate.getMonth() - endDate.getMonth()) != 0) {
                this.maxDODate(Utility.addMonths(dateTime, 1));
                this.setDODateMaxMix();
            }
        });

        // DODateTo condition
        this.shipmentDODateTo.subscribe((dateTime) => {
            let startDate = new Date(this.shipmentDODateFrom());
            let endDate = new Date(dateTime);
            if(endDate < startDate) {
                this.setDODateMaxMix(dateTime);
                this.setDODateMaxMix();
            }

            if((startDate.getMonth() - endDate.getMonth()) != 0) {
                this.minDODate(Utility.minusMonths(dateTime, 1));
                this.setDODateMaxMix();
            }
        });

        var dfd = $.Deferred();

        var dfdBusinessUnit = null;
        var dfdVehicle = null;
        var dfdShipmentStatus = null;
        var dfdRegion = null;
        var dfdSearchbyDay = null;

        /* Process : Business Unit */
        var businessUnitFilter = {
            companyId: WebConfig.userSession.currentCompanyId
        }

        dfdBusinessUnit = this.webRequestBusinessUnit.listBusinessUnitSummary(businessUnitFilter).done((response) => {
            this.businessUnitList(response["items"]);
            this.businessUnit(ScreenHelper.findOptionByProperty(this.businessUnitList, "id"));
        });
        /* End Process : Business Unit */

        /* Process : Shipment Status */
        var shipmentStatusFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            types: [Enums.ModelData.EnumResourceType.JobStatus]
        }

        dfdShipmentStatus = this.webRequestEnumResource.listEnumResource(shipmentStatusFilter).done((response) => {
            this.shipmentStatusList(response["items"]);
            this.shipmentStatus(ScreenHelper.findOptionByProperty(this.shipmentStatusList, "value"));
        });

        dfdRegion = this.webRequestShipment.listRegionSummary().done((response) => {
            this.regionList(response["items"]);
            this.region(ScreenHelper.findOptionByProperty(this.regionList, "id"));
        });

        let filterSearchbyDay = {
            companyId: WebConfig.userSession.currentCompanyId,
            types: [Enums.ModelData.EnumResourceType.Day]
        }
        dfdSearchbyDay = this.webRequestEnumResource.listEnumResource(filterSearchbyDay).done((response)=>{
            this.dayList(response["items"]);
            this.valueDay(ScreenHelper.findOptionByProperty(this.dayList, "value"));
        });

        /* End Process : Shipment Status */

        /* Subscribe on Business Unit Change */
        // Bu Shipment
        this.businessUnit.subscribe((businessUnitIds) => {
            this.shipmentCategory([]);

            if(_.size(businessUnitIds)){
                let shipmentCateFilter = {
                    businessUnitIds: businessUnitIds
                }
                let d3 = this.webRequestShipmentCategory.listShipmentCategory(shipmentCateFilter);

                this.isBusy(true);
                $.when(d3).done((r3)=>{
                    r3.items.unshift({id: 0, name: this.i18n("Common_CheckAll")(), parent: "#"});
                    _.forEach(r3.items, (item)=>{
                        if(item.id){
                            item.parent = 0;
                        }
                    });
                    this.shipmentCategory(r3.items);
                    this.isBusy(false);
                });
            }
        });

        // Bu Vehicle
        this.businessUnitVehicle.subscribe((businessUnitIds) => {
            this.vehicle([]);

            if(_.size(businessUnitIds)){
                let vehicleFilter = {
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: businessUnitIds,
                    sortingColumns: DefaultSorting.Vehicle
                }
                let d1 = this.webRequestVehicle.listVehicleSummary(vehicleFilter);

                this.isBusy(true);
                $.when(d1).done((r1)=>{
                    r1.items.unshift({id: 0, license: this.i18n("Common_CheckAll")(), parent: "#"});
                    _.forEach(r1.items, (item)=>{
                        if(item.id){
                            item.parent = 0;
                        }
                    });
                    this.vehicle(r1.items);
                    this.isBusy(false);
                });
            }
        });

        // Bu Driver
        this.businessUnitDriver.subscribe((businessUnitIds) => {
            this.driver([]);

            if(_.size(businessUnitIds)){
                let driverFilter = {
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: businessUnitIds,
                    sortingColumns: DefaultSorting.Driver
                }
                let d2 = this.webRequestDriver.listDriverSummary(driverFilter);

                this.isBusy(true);
                $.when(d2).done((r2)=>{
                    r2.items.unshift({id: 0, displayName: this.i18n("Common_CheckAll")(), parent: "#"});
                    _.forEach(r2.items, (item)=>{
                        if(item.id){
                            item.displayName = item.firstName + " " + item.lastName;
                            item.parent = 0;
                        }
                    });
                    this.driver(r2.items);
                    this.isBusy(false);
                });
            }
        });
        /* End Subscribe Business Unit Change */

        let travelModeFilter = { 
            companyId: WebConfig.userSession.currentCompanyId, 
            types: Enums.ModelData.EnumResourceType.TravelMode,
            sortingColumns: DefaultSorting.EnumResource
        }

        let dfdTravelMode = this.webRequestEnumResource.listEnumResource(travelModeFilter);

        /* Check State All Process */
        $.when(dfdBusinessUnit, dfdShipmentStatus, dfdTravelMode).done((r1, r2, r3) => {
            this.travelMode(r3.items);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    setupExtend() {

        //this.businessUnit.extend({ trackChange: true });;
        //this.includeSubBU.extend({ trackChange: true });
        //this.vehicle.extend({ trackChange: true });
        //this.shipmentStatus.extend({ trackChange: true });

        //this.txtShipmentCode.extend({ trackChange: true });
        //this.txtShipmentName.extend({ trackChange: true });

        //this.txtDOCode.extend({ trackChange: true });
        //this.txtDOName.extend({ trackChange: true });
        //this.txtCreateBy.extend({ trackChange: true });

        //this.shipmentDODateFrom.extend({ trackChange: true });
        //this.shipmentDODateTo.extend({ trackChange: true });

        //this.shipmentStartDate.extend({ trackChange: true });
        //this.shipmentEndDate.extend({ trackChange: true });
        //this.shipmentStartTime.extend({ trackChange: true });
        //this.shipmentEndTime.extend({ trackChange: true });

        // this.shipmentStartDate.extend({ required: true });
        // this.shipmentEndDate.extend({ required: true });
        // this.shipmentStartTime.extend({ required: true });
        // this.shipmentEndTime.extend({ required: true });

        this.selectedVehicle.extend({
            required: {
                onlyIf: () => {
                    return _.size(this.businessUnitVehicle())
                }
            }
        });

        this.selectedDriver.extend({
            required: {
                onlyIf: () => {
                    return _.size(this.businessUnitDriver())
                }
            }
        });

        this.shipmentStartDate.extend({ 
            required: {
                onlyIf: () => {
                    let condition = (_.isNil(this.shipmentStartDate())) && (this.txtShipmentCode() == "" || this.txtShipmentCode() == undefined)
                    let condition2 = !(this.txtShipmentCode() == "" || this.txtShipmentCode() == undefined) 
                                    && (!_.isNil(this.shipmentStartDate()) 
                                    || !_.isNil(this.shipmentEndDate()) 
                                    || !this.shipmentStartTime() == "" 
                                    || !this.shipmentEndTime() == "");
                    
                    return condition || condition2;
                }
            }
        });

        this.shipmentEndDate.extend({ 
            required: {
                onlyIf: () => {
                    let condition = (_.isNil(this.shipmentEndDate())) && (this.txtShipmentCode() == "" || this.txtShipmentCode() == undefined)
                    let condition2 = !(this.txtShipmentCode() == "" || this.txtShipmentCode() == undefined) 
                                    && (!_.isNil(this.shipmentStartDate()) 
                                    || !_.isNil(this.shipmentEndDate()) 
                                    || !this.shipmentStartTime() == "" 
                                    || !this.shipmentEndTime() == "");
                    
                    return condition || condition2;
                }
            }
        });

        this.shipmentStartTime.extend({ 
            required: {
                onlyIf: () => {
                    let condition = (this.shipmentStartTime() == "") && (this.txtShipmentCode() == "" || this.txtShipmentCode() == undefined)
                    let condition2 = !(this.txtShipmentCode() == "" || this.txtShipmentCode() == undefined) 
                                    && (!_.isNil(this.shipmentStartDate()) 
                                    || !_.isNil(this.shipmentEndDate()) 
                                    || !this.shipmentStartTime() == "" 
                                    || !this.shipmentEndTime() == "");
                    
                    return condition || condition2;
                }
            }
        });

        this.shipmentEndTime.extend({ 
            required: {
                onlyIf: () => {
                    let condition = (this.shipmentEndTime() == "") && (this.txtShipmentCode() == "" || this.txtShipmentCode() == undefined)
                    let condition2 = !(this.txtShipmentCode() == "" || this.txtShipmentCode() == undefined) 
                                    && (!_.isNil(this.shipmentStartDate()) 
                                    || !_.isNil(this.shipmentEndDate()) 
                                    || !this.shipmentStartTime() == "" 
                                    || !this.shipmentEndTime() == "");
                    
                    return condition || condition2;
                }
            }
        });

        this.shipmentCreatedStartDate.extend({
            required: {
                onlyIf: () => {
                    return !(_.isNil(this.shipmentCreatedStartTime()) 
                        || this.shipmentCreatedStartTime() == "" 
                        || this.shipmentCreatedStartTime() == null
                        || this.shipmentCreatedStartTime() == undefined)
                }
            }
        })

        this.shipmentCreatedEndDate.extend({
            required: {
                onlyIf: () => {
                    return !(_.isNil(this.shipmentCreatedEndTime()) 
                        || this.shipmentCreatedEndTime() == "" 
                        || this.shipmentCreatedEndTime() == null
                        || this.shipmentCreatedEndTime() == undefined)
                }
            }
        })

        let paramsValidation = {
            shipmentStartDate: this.shipmentStartDate,
            shipmentEndDate: this.shipmentEndDate,
            shipmentStartTime: this.shipmentStartTime,
            shipmentEndTime: this.shipmentEndTime,
            shipmentCreatedStartDate: this.shipmentCreatedStartDate,
            shipmentCreatedEndDate: this.shipmentCreatedEndDate,
            selectedVehicle: this.selectedVehicle,
            selectedDriver: this.selectedDriver
        }

        this.validationModel = ko.validatedObservable(paramsValidation);
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() { }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Search")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdClear", this.i18n("Common_Clear")(), "svg-cmd-clear"));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.submitSearchFilter();
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdClear") {
            this.clearSearchFilter();
        }
    }
    /* start autocomplete section */
    queryShipmentCode(filter) {
        return this.webRequestShipment.autocompleteShipmentCode(filter);
    }

    queryShipmentName(filter) {
        return this.webRequestShipment.autocompleteShipmentName(filter);
    }

    queryDOCode(filter) {
        return this.webRequestShipment.autocompleteShipmentDOCode(filter);
    }

    queryDOName(filter) {
        return this.webRequestShipment.autocompleteShipmentDOName(filter);
    }

    queryShipmentCreateBy(filter) {
        return this.webRequestShipment.autocompleteShipmentCreateBy(filter);
    }
    /* end autocomplete section */

    getSelectedShipmentStatus()
    {
        var selectedShipment = this.shipmentStatus();

        if (!selectedShipment)
            return selectedShipment;

        var shipmentStatus = this.shipmentStatusList().filter((item) => {
            return selectedShipment.map((itm) => { return parseInt(itm); }).indexOf(item.id) >= 0;
        });

        return shipmentStatus.map(itm => { return itm.value });
    }
    getSelectedDay()
    {
        var selectDay = this.valueDay();

        if (!selectDay)
            return selectDay;

        var searchbyday = this.dayList().filter((item) => {
            return selectDay.map((itm) => { return parseInt(itm); }).indexOf(item.id) >= 0;
        });

        return searchbyday.map(itm => { return itm.value });
    }

    setFormatDateTime(dateObj, timeObj, defaultTime) {

        var newDateTime = "";

        if (dateObj && timeObj) {

            if (typeof (dateObj.getMonth) === 'function') {

                let d = new Date();
                let year = d.getFullYear().toString();
                let month = (d.getMonth() + 1).toString();
                let day = d.getDate().toString();

                month = month.length > 1 ? month : "0" + month;
                day = day.length > 1 ? day : "0" + day;
                newDateTime = year + "-" + month + "-" + day + "T" + timeObj;
            } else {
                let newDate = dateObj.split("T")[0] + "T";
                let newTime = dateObj.split("T")[1] = timeObj + ":00";
                newDateTime = newDate + newTime;
            }

        }
        else if (dateObj && defaultTime) {
            if (typeof (dateObj.getMonth) === 'function') {

                let d = new Date();
                let year = d.getFullYear().toString();
                let month = (d.getMonth() + 1).toString();
                let day = d.getDate().toString();

                month = month.length > 1 ? month : "0" + month;
                day = day.length > 1 ? day : "0" + day;
                newDateTime = year + "-" + month + "-" + day + "T" + defaultTime;
            } else {
                let newDate = dateObj.split("T")[0] + "T";
                let newTime = dateObj.split("T")[1] = defaultTime;
                newDateTime = newDate + newTime;
            }
        }
        else
        { }

        return newDateTime;
    }

    validate()
    {
        return true;
    }

    submitSearchFilter() {

        var filter = {};

        var tavelModes = new Array();
        _.forEach(this.selectedTravelMode(), (id) =>{
            let obj = _.find(this.travelMode(), (item)=>{
                return item.id == id
            });
            tavelModes.push(obj.value);
        });

        filter.businessUnitIds = this.businessUnit();
        filter.vehicleIds = this.selectedVehicle();
        filter.driverIds = this.selectedDriver();
        filter.regionIds = this.region();
        filter.travelModes = tavelModes;
        filter.jobCategoryIds = this.selectedShipmentCategory();

        filter.shipmentStatusIds = this.getSelectedShipmentStatus();
        filter.shipmentFromDate = this.setFormatDateTime(this.shipmentStartDate(), this.shipmentStartTime());
        filter.shipmentToDate = this.setFormatDateTime(this.shipmentEndDate(), this.shipmentEndTime());
        filter.shipmentCreateFrom = this.setFormatDateTime(this.shipmentCreatedStartDate(), this.shipmentCreatedStartTime(), "00:00:00");
        filter.shipmentCreateTo = this.setFormatDateTime(this.shipmentCreatedEndDate(), this.shipmentCreatedEndTime(), "23:59:59");
        filter.shipmentCode = this.txtShipmentCode();
        filter.templateName = this.txtTemplateName();
        filter.shipmentName = this.txtShipmentName();
        filter.doDateFrom = this.shipmentDODateFrom();
        filter.doDateTo = this.shipmentDODateTo();
        filter.doCode = this.txtDOCode();
        filter.doName = this.txtDOName();
        filter.createBy = this.txtCreateBy();
        filter.dayIds = this.getSelectedDay();

        // console.log("businessUnitIds", filter.businessUnitIds);
        // console.log("filter", filter);
        this.publishMessage("cw-shipment-search", filter);
    }

    clearSearchFilter() {
        let currentDate = new Date();

        this.businessUnit(null);
        this.includeSubBU(false);
        this.vehicle(null);

        //clear ddl
        this.vehicle([]);

        this.shipmentStatus(null);

        this.txtShipmentCode(null);
        this.txtShipmentName(null);
        this.txtTemplateName(null);

        this.txtDOCode(null);
        this.txtDOName(null);
        this.txtCreateBy(null);

        //this.shipmentStartDate(currentDate);
        //this.shipmentEndDate(currentDate);
        //this.shipmentStartTime('00:00');
        //this.shipmentEndTime('23:59');

        this.shipmentCreatedStartDate(null);
        this.shipmentCreatedEndDate(null);
        this.shipmentCreatedStartTime(null);
        this.shipmentCreatedEndTime(null);   

        this.shipmentDODateFrom(null);
        this.shipmentDODateTo(null);

        this.selectedShipmentCategory(null);
        this.region(null);
        this.selectedTravelMode(null);
        this.valueDay(null);
    }
}

export default {
viewModel: ScreenBase.createFactory(ShipmentListSearchScreen),
    template: templateMarkup
};
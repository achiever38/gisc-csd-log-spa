﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import {
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestShipment from "../../../../../app/frameworks/data/apitrackingcore/webRequestShipment";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicle";
import WebRequestDriver from "../../../../../app/frameworks/data/apitrackingcore/webRequestDriver";
import WebRequestCustomPOI from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomPOI";
import WebRequestMap from "../../../../../app/frameworks/data/apitrackingcore/webRequestMap";
import WebRequestShipmentCategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestShipmentCategory"
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebRequestShipmentZone from "../../../../../app/frameworks/data/apitrackingcore/webRequestShipmentZone";

class ShipmentTemplateManage extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle("Shipment Templates List");
        this.bladeSize = BladeSize.XLarge_A1;
        this.bladeIsMaximized = true;


        this.selectedIndex = ko.observable(0);

        this.txtDisable = ko.observable(true);
        this.isVisibleDuration = ko.observable(false);
        this.isShipmentGridEdit = ko.observable(false);
        this.shipmentGridChange = ko.observable(false);

        this.isLatLonDup = ko.observable(false);


        this.ddBusinessUnitList = ko.observableArray([]);
        this.ddVehiclelist = ko.observableArray([]);
        this.ddDriverlist = ko.observableArray([]);
        this.ddDelivertManlist = ko.observableArray([]);
        this.ddTraillelist = ko.observableArray([]);
        this.ddEnumJobAutoFinish = ko.observableArray([]);
        this.ddLastShipmentRole = ko.observableArray([]);
        this.ddcoDriverList = ko.observableArray([]);

        this.valueTemplateCode = ko.observable();
        this.valueTemplateName = ko.observable();
        this.valueShipmentName = ko.observable();
        this.valueBusinessUnit = ko.observable();
        this.valueVehicle = ko.observable();
        this.valueDriver = ko.observable();
        this.valueDelivertMan = ko.observable();
        this.valueWaitingTime = ko.observable();
        this.valueTrail = ko.observable();
        this.valueEnumJobAutoFinish = ko.observable();
        this.valueDuration = ko.observable();
        this.valueLastShipmentRole = ko.observable();
        this.valueDescription = ko.observable();
        this.valueStartTime = ko.observable(this.checkTime());
        this.valueStartDate = ko.observable(new Date());


        this.txtStartDate = ko.observable(new Date());
        this.txtStartTime = ko.observable(this.checkTime());


        this.dfDuration = ko.observable();
        this.dflastShipmentRole = ko.observable();
        this.dfValuejobAutoFinish = ko.observable();


        // Tab Assign AdditionalDriver
        this.customRouteImage = this.resolveUrl('~/images/icon_customroute_remove.svg');
        this.list = ko.observableArray([]);
        this.selectedItem = ko.observable();
        this.cancelDriver = ko.observable();
        this.resultCodriver = ko.observableArray([]);
        this.cancelCodriver = ko.observableArray([]);

        // Table waypoint
        this.mode = ko.observable('create');
        this.typedatagrid = ko.observable('template');
        this.shipmentTemplateData = ko.observable({});
        this.shipmentJobWaypointInfoLst = ko.observableArray([]);
        this.deliveryTypeList = ko.observableArray([]);
        this.startDateTime = ko.pureComputed(({
            read: () => {
                return ({
                    date: this.txtStartDate(),
                    time: this.valueStartTime()
                });
            }
        }));
        this.jobDetail = ko.observable({
            startJob: 2,
            closeJob: 1,
            isTerminal: false
        })
        this.defaultValue = ko.observable({});
        this.bladeId = ko.observable(null);
        this.totalWayPoint = ko.observable();
        this.isBindingData = this.ensureNonObservable(false);

        this.zone = ko.observableArray([]);
        this.selectedZone = ko.observable();
        this.travelMode = ko.observableArray([]);
        this.selectedTravelMode = ko.observable();
        this.shipmentCategory = ko.observableArray([]);
        this.selectedShipmentCategory = ko.observable();

        this.isFindBestSequenceLoading = ko.observable(false);


        this.vechicleBUList = ko.observableArray([]);
        this.vechicleBU = ko.observable();
        this.driverBUList = ko.observableArray([]);
        this.driverBU = ko.observable();


        if (params.shipmentTemplateId == undefined) {
            this.txtDisable(true);
            this.isShipmentGridEdit(true);
            this.bladeTitle(this.i18n("Transportation_Mngm_Template_Create")());
            this.mode('create');
        } else {
            this.shipmentId = params.shipmentTemplateId;
            this.isShipmentGridEdit(true)
            this.isBindingData = true;
            this.bladeTitle(this.i18n("Transportation_Mngm_Template_Update")());
            this.mode('update');
        }

        this.isFindBestSequenceLoading.subscribe((res)=>{
            if(res){
                this.isBusy(true);
            }else{
                this.isBusy(false);
            }
        });


    }

    setupExtend() {

        
        this.valueTemplateCode.extend({
            trackChange: true
        });

        this.valueTemplateName.extend({
            trackChange: true
        });

        this.valueBusinessUnit.extend({
            trackChange: true
        });

        this.valueShipmentName.extend({
            trackChange: true
        });
        
        this.valueTemplateCode.extend({
            required: true
        });

        this.valueTemplateName.extend({
            required: true
        });
        this.valueBusinessUnit.extend({
            required: true
        });
        
        this.valueShipmentName.extend({
            required: true
        });

        this.valueTemplateName.extend({
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });

        this.valueTemplateCode.extend({
            serverValidate: {
                params: "Code",
                message: this.i18n("M010")()
            }
        });


        this.validationModel = ko.validatedObservable({
            valueTemplateCode: this.valueTemplateCode,
            templateName: this.valueTemplateName,
            businessUnit: this.valueBusinessUnit,
            shipmentName: this.valueShipmentName
        });
    }
    get webRequestMap() {
        return WebRequestMap.getInstance();
    }
    get webRequestCustomPOI() {
        return WebRequestCustomPOI.getInstance();
    }
    get webRequestShipment() {
        return WebRequestShipment.getInstance();
    }

    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    get webRequestVehicle() {
        return WebRequestVehicle.getInstance();
    }

    get webRequestDriver() {
        return WebRequestDriver.getInstance();
    }

    get webRequestShipmentCategory() {
        return WebRequestShipmentCategory.getInstance();
    }

    get webRequestShipmentZone() {
        return WebRequestShipmentZone.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {

        if (!isFirstLoad)
            return;

        this.bladeId($(this)[0]._blade.id);


        let dfd = $.Deferred();
        let dfdAllDone = $.Deferred();
        let dfdShipmentTemplate;
        let companyId = WebConfig.userSession.currentCompanyId;
        let dfdBusinessUnit = this.webRequestBusinessUnit.listBusinessUnitSummary({
            companyId: companyId
        });
        let dfdDefaultValue = this.webRequestShipment.getDefaultValue();
        let dfdEnumJobAutoFinish = this.webRequestEnumResource.listEnumResource({
            companyId: companyId,
            types: [Enums.ModelData.EnumResourceType.JobAutoFinish]
        });
        let dfdLastShipmentRole = this.webRequestEnumResource.listEnumResource({
            companyId: companyId,
            types: [Enums.ModelData.EnumResourceType.LastShipmentRole]
        });
        let dfdDeliveryType = this.webRequestEnumResource.listEnumResource({
            companyId: companyId,
            types: [Enums.ModelData.EnumResourceType.DeliveryType]
        });

        if (this.shipmentId != null) {
            dfdShipmentTemplate = this.webRequestShipment.getShipmentTemplate(this.shipmentId);
        } else {
            dfdShipmentTemplate = $.Deferred();
            dfdShipmentTemplate.resolve();
        }

        let dfdZone = this.webRequestShipmentZone.listShipmentZone({enable: true});

        let travelModeFilter = { 
            companyId: WebConfig.userSession.currentCompanyId, 
            types: Enums.ModelData.EnumResourceType.TravelMode,
            sortingColumns: DefaultSorting.EnumResource
        }
        
        let dfdTravelMode = this.webRequestEnumResource.listEnumResource(travelModeFilter);

        $.when(dfdBusinessUnit, dfdDefaultValue, dfdEnumJobAutoFinish, dfdLastShipmentRole, dfdDeliveryType, dfdShipmentTemplate, dfdZone, dfdTravelMode).done((
            responBusinessUnit,
            responDefaultValue,
            responEnumJobAutoFinish,
            reaponLastShipmentRole,
            responDeliveryType,
            responShipmentTemplate,
            responseZone,
            responseTravelMode) => {

            this.ddBusinessUnitList(responBusinessUnit.items);
            this.vechicleBUList(responBusinessUnit.items);
            this.driverBUList(responBusinessUnit.items);
            this.ddEnumJobAutoFinish(responEnumJobAutoFinish.items)
            this.ddLastShipmentRole(reaponLastShipmentRole.items)
            this.deliveryTypeList(responDeliveryType.items);

            //ค่า Defaul Value
            this.defaultValue(responDefaultValue);
            this.dfDuration(responDefaultValue.duration);
            this.dflastShipmentRole(responDefaultValue.lastShipmentRole)
            this.dfValuejobAutoFinish(responDefaultValue.jobAutoFinish);


            this.valueEnumJobAutoFinish(ScreenHelper.findOptionByProperty(this.ddEnumJobAutoFinish, "value", responDefaultValue.jobAutoFinish));
            this.valueLastShipmentRole(ScreenHelper.findOptionByProperty(this.ddLastShipmentRole, "value", responDefaultValue.lateShipmentRole));
            this.valueDuration(responDefaultValue.duration);
            // this.valueStartTime(`${this.valueStartDate().getHours()}:${this.valueStartDate().getMinutes()}`);
            this.valueStartTime(this.valueStartDate().getHours() + ":" + this.valueStartDate().getMinutes());
            this.zone(responseZone.items);
            this.travelMode(responseTravelMode.items);
            
            //Update Shipment
            if (responShipmentTemplate) {
                let lst = [];

                this.shipmentTemplateData(responShipmentTemplate);
                this.valueBusinessUnit(responShipmentTemplate.businessUnitId.toString());
                if(responShipmentTemplate.vehicleBusinessUnitId){
                    this.vechicleBU(responShipmentTemplate.vehicleBusinessUnitId.toString())
                }
                if(responShipmentTemplate.driverBusinessUnitId){
                    this.driverBU(responShipmentTemplate.driverBusinessUnitId.toString())
                }
                this.valueTemplateCode(responShipmentTemplate.code);
                this.valueShipmentName(responShipmentTemplate.jobName);
                this.valueTemplateName(responShipmentTemplate.name);
                this.valueWaitingTime(responShipmentTemplate.waitingTime);
                this.valueDescription(responShipmentTemplate.description);
                this.valueDelivertMan(responShipmentTemplate.deliveryManId);
                this.valueStartTime(responShipmentTemplate.planStartTime);
                this.valueEnumJobAutoFinish(ScreenHelper.findOptionByProperty(this.ddEnumJobAutoFinish, "value", responShipmentTemplate.jobAutoFinish));
                this.valueLastShipmentRole(ScreenHelper.findOptionByProperty(this.ddLastShipmentRole, "value", responShipmentTemplate.lateShipmentRole));
                this.valueDuration(responShipmentTemplate.jobAutoFinishDuration);

                this.selectedZone(ScreenHelper.findOptionByProperty(this.zone, "id", responShipmentTemplate.jobRegionId));
                this.selectedTravelMode(ScreenHelper.findOptionByProperty(this.travelMode, "value", responShipmentTemplate.travelMode));
                
                let dfdShipmentCategory = this.webRequestShipmentCategory.listShipmentCategory({businessUnitIds: [responShipmentTemplate.businessUnitId],enable: true});
                dfdShipmentCategory.done((resShipmentCategory)=>{
                    this.shipmentCategory(resShipmentCategory.items);
                    this.selectedShipmentCategory(ScreenHelper.findOptionByProperty(this.shipmentCategory, "id", responShipmentTemplate.jobCategoryId));
                });

                this.jobDetail({
                    startJob: responShipmentTemplate.startJob,
                    closeJob: responShipmentTemplate.closeJob,
                    isTerminal: responShipmentTemplate.isTerminal
                });

                this.resultCodriver.replaceAll(this.formatNameDriver(responShipmentTemplate.coDriverInfos));
                this.cancelCodriver.replaceAll(this.formatNameDriver(responShipmentTemplate.coDriverInfos));

                this.totalWayPoint(responShipmentTemplate.jobWaypointTemplatesInfos.length);
                for (let i in responShipmentTemplate.jobWaypointTemplatesInfos) {
                    responShipmentTemplate.jobWaypointTemplatesInfos[i].arrivalId = "arrival" + i;
                    responShipmentTemplate.jobWaypointTemplatesInfos[i].departId = "depart" + i;
                    lst.push(responShipmentTemplate.jobWaypointTemplatesInfos[i]);
                }

                
                var dfdVehicle = responShipmentTemplate.vehicleBusinessUnitId?this.webRequestVehicle.listVehicleSummary({
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: [responShipmentTemplate.vehicleBusinessUnitId],
                    vehicleTypes: [
                        Enums.ModelData.VehicleType.Car,
                        Enums.ModelData.VehicleType.Truck,
                        Enums.ModelData.VehicleType.Trailer,
                        Enums.ModelData.VehicleType.PassengerCar
                    ]
                }):null;
                var dfdTrail = responShipmentTemplate.vehicleBusinessUnitId?this.webRequestVehicle.listVehicleSummary({
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: [responShipmentTemplate.vehicleBusinessUnitId],
                    vehicleTypes: [
                        Enums.ModelData.VehicleType.Trail
                    ]
                }):null;
                var dfddriverlist = responShipmentTemplate.driverBusinessUnitId?this.webRequestDriver.driverSummarylist({
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: responShipmentTemplate.driverBusinessUnitId
                }):null;

                $.when(dfdTrail, dfdVehicle, dfddriverlist)
                .done((responseTrail, responseVehicle, responsedriverlist) => {

                    if(responsedriverlist){
                        this.ddDriverlist.replaceAll(this.formatNameDriver(responsedriverlist.items));
                        this.valueDriver(ScreenHelper.findOptionByProperty(this.ddDriverlist, "id", responShipmentTemplate.driverId));
                        this.cancelDriver(ScreenHelper.findOptionByProperty(this.ddDriverlist, "id", responShipmentTemplate.driverId));
                    }
                    if(responseVehicle){
                        this.ddVehiclelist.replaceAll(responseVehicle.items);
                        this.valueVehicle(ScreenHelper.findOptionByProperty(this.ddVehiclelist, "id", responShipmentTemplate.vehicleId));
                    }
                    
                    if(responseTrail){
                        this.ddTraillelist.replaceAll(responseTrail.items);
                        this.valueTrail(ScreenHelper.findOptionByProperty(this.ddTraillelist, "id", responShipmentTemplate.trailId));
                    }


                    this.isBindingData = false;
                    this.coDriveris = true;
                    this.setCoDriver();
                    this.shipmentJobWaypointInfoLst.replaceAll(lst)
                    dfd.resolve();
                });
                dfdAllDone.resolve();
            } else {
                //Tab CoDriver
                this.setCoDriver();
                dfd.resolve();
            }
        }).fail((e) => {
            dfd.reject(e);
        }).always(() => {

        });
        this.vechicleBU.subscribe((businessUnitId) => {
            if (!this.isBindingData) {
                this.valueVehicle(null);
                // this.valueDriver(null);

                // this.ddDriverlist.replaceAll([]);
                this.ddTraillelist.replaceAll([]);
                this.ddVehiclelist.replaceAll([]);
                // this.coDriverList.replaceAll([]);
                // this.shipmentCategory.replaceAll([]);

                if(businessUnitId){
                    let companyId = WebConfig.userSession.currentCompanyId;
                    let dfdVehicle = this.webRequestVehicle.listVehicleSummary({
                        companyId: companyId,
                        businessUnitIds: [businessUnitId],
                        vehicleTypes: [
                            Enums.ModelData.VehicleType.Car,
                            Enums.ModelData.VehicleType.Truck,
                            Enums.ModelData.VehicleType.Trailer,
                            Enums.ModelData.VehicleType.PassengerCar
                        ]
                    });

                    let dfdTrail = this.webRequestVehicle.listVehicleSummary({
                        companyId: companyId,
                        businessUnitIds: [businessUnitId],
                        vehicleTypes: [
                            Enums.ModelData.VehicleType.Trail
                        ]
                    });

                    // let dfddriverlist = this.webRequestDriver.driverSummarylist({
                    //     companyId: companyId,
                    //     businessUnitIds: businessUnitId
                    // });

                    // let dfdShipmentCategory = this.webRequestShipmentCategory.listShipmentCategory({
                    //     businessUnitIds: [businessUnitId],
                    //     enable: true
                    // });

                    $.when(dfdTrail, dfdVehicle)
                    .done((responseTrail, responseVehicle) => {
                        // this.ddDriverlist.replaceAll(this.formatNameDriver(responsedriverlist.items));
                        this.ddVehiclelist.replaceAll(responseVehicle.items);
                        this.ddTraillelist.replaceAll(responseTrail.items);
                        // this.shipmentCategory.replaceAll(resShipmentCategory.items);
                        dfdTrail.resolve();
                        dfdVehicle.resolve();
                        // dfddriverlist.resolve();
                    })
                }
            }
        });
        this.driverBU.subscribe((businessUnitId) => {

            if (!this.isBindingData) {
                // this.valueVehicle(null);
                this.valueDriver(null);

                this.ddDriverlist.replaceAll([]);
                this.coDriverList.replaceAll([]);
                // this.ddVehiclelist.replaceAll([]);
                // this.ddTraillelist.replaceAll([]);
                // this.shipmentCategory.replaceAll([]);

                if(businessUnitId){
                    let companyId = WebConfig.userSession.currentCompanyId;
                    // let dfdVehicle = this.webRequestVehicle.listVehicleSummary({
                    //     companyId: companyId,
                    //     businessUnitIds: [businessUnitId],
                    //     vehicleTypes: [
                    //         Enums.ModelData.VehicleType.Car,
                    //         Enums.ModelData.VehicleType.Truck,
                    //         Enums.ModelData.VehicleType.Trailer,
                    //         Enums.ModelData.VehicleType.PassengerCar
                    //     ]
                    // });

                    // let dfdTrail = this.webRequestVehicle.listVehicleSummary({
                    //     companyId: companyId,
                    //     businessUnitIds: [businessUnitId],
                    //     vehicleTypes: [
                    //         Enums.ModelData.VehicleType.Trail
                    //     ]
                    // });

                    let dfddriverlist = this.webRequestDriver.driverSummarylist({
                        companyId: companyId,
                        businessUnitIds: businessUnitId
                    });

                    // let dfdShipmentCategory = this.webRequestShipmentCategory.listShipmentCategory({
                    //     businessUnitIds: [businessUnitId],
                    //     enable: true
                    // });

                    $.when(dfddriverlist)
                    .done((responsedriverlist) => {
                        this.ddDriverlist.replaceAll(this.formatNameDriver(responsedriverlist.items));
                        // this.ddVehiclelist.replaceAll(responseVehicle.items);
                        // this.ddTraillelist.replaceAll(responseTrail.items);
                        // this.shipmentCategory.replaceAll(resShipmentCategory.items);
                        // dfdTrail.resolve();
                        // dfdVehicle.resolve();
                        dfddriverlist.resolve();
                    })
                }
            }
        });
        this.valueBusinessUnit.subscribe((businessUnitId) => {
            if (!this.isBindingData) {
                // this.valueVehicle(null);
                // this.valueDriver(null);

                // this.ddDriverlist.replaceAll([]);
                // this.ddVehiclelist.replaceAll([]);
                // this.ddTraillelist.replaceAll([]);
                // this.coDriverList.replaceAll([]);
                this.shipmentCategory.replaceAll([]);

                if(businessUnitId){
                    let companyId = WebConfig.userSession.currentCompanyId;
                    // let dfdVehicle = this.webRequestVehicle.listVehicleSummary({
                    //     companyId: companyId,
                    //     businessUnitIds: [businessUnitId],
                    //     vehicleTypes: [
                    //         Enums.ModelData.VehicleType.Car,
                    //         Enums.ModelData.VehicleType.Truck,
                    //         Enums.ModelData.VehicleType.Trailer,
                    //         Enums.ModelData.VehicleType.PassengerCar
                    //     ]
                    // });

                    // let dfdTrail = this.webRequestVehicle.listVehicleSummary({
                    //     companyId: companyId,
                    //     businessUnitIds: [businessUnitId],
                    //     vehicleTypes: [
                    //         Enums.ModelData.VehicleType.Trail
                    //     ]
                    // });

                    // let dfddriverlist = this.webRequestDriver.driverSummarylist({
                    //     companyId: companyId,
                    //     businessUnitIds: businessUnitId
                    // });

                    let dfdShipmentCategory = this.webRequestShipmentCategory.listShipmentCategory({
                        businessUnitIds: [businessUnitId],
                        enable: true
                    });

                    $.when( dfdShipmentCategory)
                    .done((resShipmentCategory) => {
                        // this.ddDriverlist.replaceAll(this.formatNameDriver(responsedriverlist.items));
                        // this.ddVehiclelist.replaceAll(responseVehicle.items);
                        // this.ddTraillelist.replaceAll(responseTrail.items);
                        this.shipmentCategory.replaceAll(resShipmentCategory.items);
                        // dfdTrail.resolve();
                        // dfdVehicle.resolve();
                        dfdShipmentCategory.resolve();
                    })
                }
            }
        });

        let val = this.valueDriver.subscribe(() => {
            if (!this.isBindingData) {
                //console.log(typeof this.coDriverList);

                this.coDriverList.replaceAll([]);
            }
        })

        this.valueEnumJobAutoFinish.subscribe((closeShipment) => {
            if (closeShipment != null) {
                switch (closeShipment.value) {
                    case Enums.ModelData.JobAutoFinish.AfterPlanEnd:
                        this.valueDuration(this.dfDuration());
                        this.isVisibleDuration(true);
                        break;
                    default:
                        this.isVisibleDuration(false);
                }
            } else {
                this.valueEnumJobAutoFinish(this.dfValuejobAutoFinish());
            }
        })
        this.valueLastShipmentRole.subscribe((lastShipment) => {
            if (lastShipment == null) {
                this.valueLastShipmentRole(this.dflastShipmentRole());
            }
        })
        return dfd;
    }
    checkTime() {
        var dateTime = new Date();
        dateTime = this.addMinutes(dateTime, 30);
        var hour = dateTime.getHours();
        var min = dateTime.getMinutes();
        dateTime = this.check(hour) + ":" + this.check(min);
        return dateTime

    }
    check(time) {
        time = parseInt(time);
        if (time < 10) {
            time = "0" + time
        };
        return time;
    }

    //autocomplete for table waypoint
    autoCompleteList(filter, response) {

        this.webRequestCustomPOI.autoCompleteForShipment(filter).done((data) => {
            response($.map(data, (item) => {
                return {
                    label: item.name,
                    value: item.name,
                    item: item
                };
            }));
        });
    }
    routeData(filter, response) {
        this.isBusy(true);
        this.webRequestMap.solveRoute(filter).done((res) => {
            this.isBusy(false);
            return response(res)
        }).fail((e) => {
            this.isBusy(false);
        });
    }
    viewOnMap() {

        let route = [];
        let wp = [];
        let pics = [];
        let urlPic = "~/images/"

        ko.utils.arrayForEach(this.shipmentJobWaypointInfoLst(), (itm, ind) => {
            if (!_.isNull(itm.jobWaypointRouteInfos)) {
                itm.jobWaypointRouteInfos.map((itm) => {
                    route.push([itm.longitude, itm.latitude]);
                });
            }
            //{ route = route.concat(); }

            pics.push(this.resolveUrl(`${urlPic}pin_poi_${ind + 1}.png`));

            wp.push({
                name: itm.jobWaypointName,
                lat: itm.lat,
                lon: itm.lon
            });
        });

        if (route.length > 0)
            this.drawRoute(wp, pics, [route]);
    }
    drawRoute(stops, pics, path, color = "#d91515", width = 5, opacity = 1, attributes = null) {
        MapManager.getInstance().clear(this.id);
        MapManager.getInstance().drawOneRoute(this.id, stops, pics, path, color, width, opacity, attributes);
        MapManager.getInstance().setFollowTracking(false);
        this.minimizeAll();
    }
    isDisabledHeadereBtn(form) {
        var bladeId = "#" + this.bladeId();
        var elem = $(bladeId + " a.app-commandBar-item");

        for (let index = 0; index < elem.length; index++) {
            let title = elem[index].title;
            switch (title) {
                case this.i18n('Transportation_Mngm_Shipment_Finish')():
                case this.i18n('Common_Cancel')():
                    $(elem[index]).removeClass("app-has-hover");
                    $(elem[index]).attr("style", "pointer-events: none;color:#dbdbdb;");
                    $(elem[index].children["0"].children).attr("style", "fill:#dbdbdb;");
                    break;
                default:
                    break;
            }
        }

        this.publishMessage("cw-shipment-disabled-cancelfinish", {});
    }

    isEnabledHeaderBtn() {
        var bladeId = "#" + $(this)[0]._blade.id;
        var classElemFinish = bladeId + " .app-commandBar-itemList>.app-br-default:nth-of-type(1)>.app-commandBar-item";
        var classElemCancel = bladeId + " .app-commandBar-itemList>.app-br-default:nth-of-type(2)>.app-commandBar-item";

        $(classElemFinish).addClass("app-has-hover");
        $(classElemFinish).attr("style", "");
        $(classElemFinish + " .app-commandBar-item-icon>svg").attr("style", "");
        $(classElemFinish + " .app-commandBar-item-text").attr("style", "");

        $(classElemCancel).addClass("app-has-hover");
        $(classElemCancel).attr("style", "");
        $(classElemCancel + " .app-commandBar-item-icon>svg").attr("style", "");
        $(classElemCancel + " .app-commandBar-item-text").attr("style", "");
    }
    setPropertyForUpdate(data, index) {
        let lastIndex = this.totalWayPoint() - 1;

        let arrivalEnable = true;

        if (index == lastIndex) {
            isEnabled = false;
        }

        this.isRadioStartShipment(this.jobDetail().startJob);
        this.isRadioClosedShipment(this.jobDetail().closeJob);

        if (this.jobDetail().startJob == this.Constant.DEPART &&
            index == 0) {
            isStartArrival = false;
        }

        if (index == 0) {
            arrivalEnable = false;
        }

        if (this.jobDetail().startJob == this.Constant.ARRIVAL &&
            index == 0) {
            isStartArrival = true;
        }

        data.indexLst = ko.observable(index);
        data.imgUrl = this.resolveUrl("~/images/icon_customroute_remove.svg");
        data.stop = ko.observable(data.planStopTime);
        data.travel = ko.observable(data.planDriveTime);
        data.distance = ko.observable(data.planDistance);
        data.radiusDisplay = ko.observable(data.radius);
        data.minPark = ko.observable(data.insideWaypointMinTime);
        data.maxPark = ko.observable(data.insideWaypointMaxTime);
        data.waypoint = ko.observable({
            item: {
                label: data.jobWaypointName,
                name: data.jobWaypointName,
                latitude: data.lat,
                longitude: data.lon
            }
        });
        data.requestWaypoint = null;
        data.dType = ko.observable(data.deliveryType);
        data.shipmentToAddressDisplay = ko.observable(data.customerAddress);
        data.waypointVal = ko.observable(data.jobWaypointName);
        data.InfoStatus = Enums.InfoStatus.Update;
        data.arrivalEnable = ko.observable(arrivalEnable);
        data.departEnable = ko.observable(isStartArrival);
        data.arrivalId = ko.observable("arrival" + index);
        data.departId = ko.observable("depart" + index);
        data.arrivalMinDate = this.startDateTime().date;
        data.tempPlanStopTime = data.planStopTime;

        return data;
    }
    addMinutes(currentDate, minutes) {
        var result = new Date(currentDate);
        result.setMinutes(parseInt(result.getMinutes()) + (parseInt(minutes)));
        return result
    }

    //จัดรูปแบบการแสดง Name ของ Driver
    formatNameDriver(response) {
        var data = [];
        for (let i = 0; i < response.length; i++) {
            var dataItem = response[i];
            dataItem.name = dataItem.firstName + " " + dataItem.lastName;
            data.push(dataItem);
        }
        return data;
    }


    setCoDriver() {
        var self = this;
        self.coDriverList = ko.observableArray([]);
        let isValidate = false; //การทำงานของปุ่ม New 
        if (this.mode() == 'update') {
            let driverId = this.valueDriver() == null ? null : this.valueDriver().id;

            this.ddcoDriverList(this.ddDriverlist().filter((res) => {
                return res.id != driverId ? res : null
            }));
            for (let item in this.resultCodriver()) {
                this.resultCodriver()[item].imgUrl = this.customRouteImage;
                let driverfilter = this.ddcoDriverList().filter((res) => {
                    if (res.id == this.resultCodriver()[item].id) {
                        return res;
                    }
                })
                this.resultCodriver()[item].selectedItem = ko.observable(driverfilter[0]);
                this.resultCodriver()[item].selectedItem.subscribe((driver) => {

                    if (driver != undefined) {
                        this.selectedItem(ScreenHelper.findOptionByProperty(this.ddDriverlist, "id", driver.id));
                        this.list([])
                        for (let item in this.coDriverList()) {
                            this.list().push(this.coDriverList()[item].selectedItem());
                        }


                        let isCheckDup = _.uniqBy(this.list(), 'id')
                        if (isCheckDup.length != this.coDriverList().length) {
                            //this.isCodriverDup(false);
                            this.resultCodriver()[item].selectedItem().isDup = false;
                        } else {
                            //this.isCodriverDup(true);
                            this.resultCodriver()[item].selectedItem().isDup = true;

                        }
                    }


                });
                this.resultCodriver()[item].selectedItem.extend({
                    required: true,
                    validation: {
                        validator: ((val) => {
                            return val.isDup

                        }),
                        message: this.i18n('M010')()
                    }
                });
            }
            self.coDriverList(this.resultCodriver())

        }




        self.newRecordCodriver = function () {
            if (this.valueBusinessUnit() && this.valueDriver()) {
                for (let i in self.coDriverList()) {
                    let lst = self.coDriverList()[i]
                    if (lst.selectedItem() == "" ||
                        lst.selectedItem() == null) {
                        isValidate = true
                    } else {
                        isValidate = false;
                    }
                }

                if (!isValidate) {
                    self.coDriverList.push(this.setDatacoDriver());
                }
            }
        }


        self.cancelCodriverChang = function () {

            let coDriver = _.filter(this.coDriverList(), (item)=>{
                return _.size(item.selectedItem());
            });
    
            if(_.size(coDriver)){
                this.showMessageBox(null, this.i18n("M246")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch(button) {
                        case BladeDialog.BUTTON_YES:
                            this.clearDataCoDriver();
                            break;
                    }
                });
            }else{
                this.clearDataCoDriver();
            }

            
        }


        self.removeCodriver = function () {
            self.coDriverList.remove(this);
        }

    }

    clearDataCoDriver(){
        for (let item in this.cancelCodriver()) {
            this.cancelCodriver()[item].imgUrl = this.customRouteImage;

            let driverfilter = this.ddDriverlist().filter((res) => {
                if (res.id == this.cancelCodriver()[item].id) {
                    return res;
                }
            })
            this.cancelCodriver()[item].selectedItem = ko.observable(driverfilter[0]);

            this.cancelCodriver()[item].selectedItem.subscribe((driver) => {

                if (driver != undefined) {
                    this.selectedItem(ScreenHelper.findOptionByProperty(this.ddDriverlist, "id", driver.id));
                    this.list([])
                    for (let item in this.coDriverList()) {
                        this.list().push(this.coDriverList()[item].selectedItem());
                    }


                    let isCheckDup = _.uniqBy(this.list(), 'id')
                    if (isCheckDup.length != this.coDriverList().length) {
                        //this.isCodriverDup(false);
                        this.cancelCodriver()[item].selectedItem().isDup = false;
                    } else {
                        //this.isCodriverDup(true);
                        this.cancelCodriver()[item].selectedItem().isDup = true;

                    }
                }


            });



            this.cancelCodriver()[item].selectedItem.extend({
                required: true,
                validation: {
                    validator: ((val) => {
                        return val.isDup

                    }),
                    message: this.i18n('M010')()
                }
            });

        }
        this.resultCodriver.replaceAll(this.cancelCodriver())
        //this.txtDriver(this.cancelDriver());
        this.coDriverList(this.resultCodriver())
    }
    

    setDatacoDriver() {
        let item = {
            selectedItem: ko.observable(null),
            imgUrl: this.customRouteImage,
            isDup: true
        };

        this.ddcoDriverList(this.ddDriverlist().filter((res) => {
            return res.id != this.valueDriver().id ? res : null
        }));

        item.selectedItem.subscribe((driver) => {
            if (driver != undefined) {
                this.selectedItem(ScreenHelper.findOptionByProperty(this.ddDriverlist, "id", driver.id));
                this.list([])
                for (let item in this.coDriverList()) {
                    this.list().push(this.coDriverList()[item].selectedItem());
                }

                // เช็คข้อมูลว่าซ้ำกันมั้ย
                let isCheckDup = _.uniqBy(this.list(), 'id')
                if (isCheckDup.length != this.coDriverList().length) {
                    item.selectedItem().isDup = false;
                } else {
                    item.selectedItem().isDup = true;

                }
            }
        })


        item.selectedItem.extend({
            required: true,
            validation: {
                validator: ((val) => {
                    return val.isDup
                }),
                message: this.i18n('M010')()
            }
        });

        return item;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        // commands.push(
        //     this.createCommand(
        //         "cmdFilter",
        //         this.i18n("Common_Search")(),
        //         "svg-cmd-search"
        //     )
        // );
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        var isValidate = new Array();
        for (let i = 0; i < this.shipmentJobWaypointInfoLst().length; i++) {
        // var isValidate = false;
       
            let lst = this.shipmentJobWaypointInfoLst()[i];

            let minPark = _.clone(lst.minPark());
            let maxPark = _.clone(lst.maxPark());

            lst.assignOrder = parseInt(i) + 1;
            if (lst.waypointVal() == "" ||
                lst.waypointVal() == null ||
                lst.waypoint() == null) {
                // isValidate = true;
                lst.waypointVal("validate Trigger");
                lst.waypointVal("");
                isValidate.push(true);
            }

            // if (lst.stop() == null) {
            //     // isValidate = true;
            //     isValidate.push(true);
            // }

            // if (lst.travel() == null) {
            //     // isValidate = true;
            //     isValidate.push(true);
            // }

            if (lst.stop() == null || lst.stop() < 0) {
                // isValidate = true;
                isValidate.push(true);
            }

            if (lst.travel() == null || lst.travel() < 0) {
                // isValidate = true;
                isValidate.push(true);
            }

            if (lst.distance() == null) {
                // isValidate = true;
                isValidate.push(true);
            }

            if (lst.radiusDisplay() == null) {
                // isValidate = true;
                isValidate.push(true);
            }

            if (lst.isLatLonDup) {
                // isValidate = true;
                isValidate.push(true);
            }

            //validation min park nad max park
            if(minPark > maxPark){
                lst.minPark(0);
                lst.minPark(minPark);
                // isValidate = true;
                isValidate.push(true);
            }

            if(maxPark < minPark){
                lst.maxPark(0);
                lst.maxPark(maxPark);
                // isValidate = true;
                isValidate.push(true);
            }
        }

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }



            var coDriverTemplateIds = [];
            var model = {
                businessUnitId: this.valueBusinessUnit(),
                driverId: _.isNil(this.valueDriver()) ? null : this.valueDriver().id,
                name: this.valueTemplateName(),
                waypointTemplatesName: this.valueTemplateName(),
                code: this.valueTemplateCode(),
                jobName: this.valueShipmentName(),
                vehicleId: _.isNil(this.valueVehicle()) ? null : this.valueVehicle().id,
                trailId: _.isNil(this.valueTrail()) ? null : this.valueTrail().id,
                deliveryManId: this.valueDelivertMan(),
                description: this.valueDescription(),
                startJob: this.jobDetail().startJob,
                closeJob: this.jobDetail().closeJob,
                isTerminal: this.jobDetail().isTerminal,
                planStartTime: this.valueStartTime(),
                waitingTime: this.valueWaitingTime(),
                jobWaypointTemplatesInfos: this.shipmentJobWaypointInfoLst(),
                jobAutoFinish: this.valueEnumJobAutoFinish() ? this.valueEnumJobAutoFinish().value : null,
                jobAutoFinishDuration: this.isVisibleDuration() ? this.valueDuration() : null,
                lateShipmentRole: this.valueLastShipmentRole() ? this.valueLastShipmentRole().value : null,
                jobRegionId: this.selectedZone() ? this.selectedZone().id : null,
                travelMode: this.selectedTravelMode() ? this.selectedTravelMode().value : null,
                jobCategoryId: this.selectedShipmentCategory() ? this.selectedShipmentCategory().id : null
            }


            for (let i in this.coDriverList()) {
                let lst = this.coDriverList()[i].selectedItem()


                if (lst == undefined) {
                    // isValidate = true;
                    isValidate.push(true);
                } else {
                    coDriverTemplateIds.push(lst.id)
                }

            }
            let item = _.uniqBy(coDriverTemplateIds)

            if (item.length == coDriverTemplateIds.length) {
                model.coDriverTemplateIds = coDriverTemplateIds;
                this.isLatLonDup(false)

            } else {
                // isValidate = true;
                isValidate.push(true);
                this.selectedIndex(1);
                this.isLatLonDup(true)

            }

            let validationModelShipmentGrid = _.filter(isValidate, (val)=>{
                return val;
            });
            
            if (!_.size(validationModelShipmentGrid)) {
                this.isBusy(true);
                if (this.shipmentId != null) { // update
                    model.infoStatus = Enums.InfoStatus.Update;
                    model.id = this.shipmentTemplateData().id;
                    model.jobStatus = this.shipmentTemplateData().jobStatus;
                    this.webRequestShipment.updateShipmentTemplate(model).done((res) => {
                        this.afterCallService();
                    }).fail((e) => {
                        this.handleError(e);
                        this.isBusy(false);
                    });
                } else { // create
                    model.infoStatus = Enums.InfoStatus.Add;
                    this.webRequestShipment.createShipmentTemplate(model).done((res) => {
                        this.afterCallService();
                    }).fail((e) => {
                        this.handleError(e);
                        this.isBusy(false);
                    });
                }
            } else {
                this.isBusy(false);
            }
        }
    }

    afterCallService() {
        this.publishMessage("cw-shipment-template-manage-refresh");
        this.isBusy(false);
        this.close(true);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        // switch (sender.id) {
        //     case "cmdFilter":
        //         this.navigate("cw-shipment-templates-search", {});
        //         break;

        //     default:
        //         break;
        // }
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {

    }
}

export default {
    viewModel: ScreenBase.createFactory(ShipmentTemplateManage),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeMargin from "../../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Constants, Enums } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestShipment from "../../../../../app/frameworks/data/apitrackingcore/webRequestShipment";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicle";
import Utility from "../../../../../app/frameworks/core/utility";
import WebRequestDriver from "../../../../../app/frameworks/data/apitrackingcore/webRequestDriver";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import WebRequestShipmentCategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestShipmentCategory";
import WebRequestShipmentZone from "../../../../../app/frameworks/data/apitrackingcore/webRequestShipmentZone";

class ShipmentTemplateSearch extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Common_Search")());
        this.bladeSize = BladeSize.Small;

        let currentDate = new Date();
        this.businessUnitList = ko.observableArray([]);
        this.shipmentStatusList = ko.observableArray([]);
        this.businessUnit = ko.observable(null);
        this.shipmentStatus = ko.observable(null);
        this.includeSubBU = ko.observable(false);
        this.txtShipmentName = ko.observable();
        this.txtTemplateName = ko.observable();
        this.txtCreateBy = ko.observable();

        this.vehicle = ko.observableArray([]);
        this.driver = ko.observableArray([]);
        this.zone = ko.observableArray([]);
        this.travelMode = ko.observableArray([]);
        this.shipmentCategory = ko.observableArray([]);

        this.selectedVehicle = ko.observable();
        this.selectedDriver = ko.observable();
        this.selectedZone = ko.observable();
        this.selectedTravelMode = ko.observable();
        this.selectedShipmentCategory = ko.observable();

        var addMonth = 3;
        this.shipmentCreatedStartDate = ko.observable(null);
        this.shipmentCreatedEndDate = ko.observable(null);
        this.shipmentCreatedStartTime = ko.observable(null);
        this.shipmentCreatedEndTime = ko.observable(null);  

        this.minCreateDate = ko.observable(Utility.minusMonths(new Date(),addMonth));
        this.maxCreateDate = ko.observable(Utility.addMonths(new Date()));

        this.dateFormat = WebConfig.companySettings.shortDateFormat;
        this.businessUnitDriver = ko.observable();
        this.businessUnitVehicle = ko.observable();

        /* Begin Subscribe */
        this.businessUnit.subscribe((businessUnitIds)=>{
            this.shipmentCategory([]);
            if(_.size(businessUnitIds)){
                let shipmentCateFilter = {
                    businessUnitIds: businessUnitIds
                }

                let d3 = this.webRequestShipmentCategory.listShipmentCategory(shipmentCateFilter);

                this.isBusy(true);
                $.when(d3).done((r3)=>{
                    r3.items.unshift({id: 0, name: this.i18n("Common_CheckAll")(), parent: "#"});

                    _.forEach(r3.items, (item)=>{
                        if(item.id){
                            item.parent = 0;
                        }
                    });

                    this.shipmentCategory(r3.items);
                    this.isBusy(false);
                });
            }

        });

        // Bu Vehicle
        this.businessUnitVehicle.subscribe((businessUnitIds) => {
            this.vehicle([]);

            if(_.size(businessUnitIds)){
                let vehicleFilter = {
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: businessUnitIds,
                    sortingColumns: DefaultSorting.Vehicle
                }
                let d1 = this.webRequestVehicle.listVehicleSummary(vehicleFilter);

                this.isBusy(true);
                $.when(d1).done((r1)=>{
                    r1.items.unshift({id: 0, license: this.i18n("Common_CheckAll")(), parent: "#"});
                    _.forEach(r1.items, (item)=>{
                        if(item.id){
                            item.parent = 0;
                        }
                    });
                    this.vehicle(r1.items);
                    this.isBusy(false);
                });
            }
        });

        // Bu Driver
        this.businessUnitDriver.subscribe((businessUnitIds) => {
            this.driver([]);

            if(_.size(businessUnitIds)){
                let driverFilter = {
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: businessUnitIds,
                    sortingColumns: DefaultSorting.Driver
                }
                let d2 = this.webRequestDriver.listDriverSummary(driverFilter);

                this.isBusy(true);
                $.when(d2).done((r2)=>{
                    r2.items.unshift({id: 0, displayName: this.i18n("Common_CheckAll")(), parent: "#"});
                    _.forEach(r2.items, (item)=>{
                        if(item.id){
                            item.displayName = item.firstName + " " + item.lastName;
                            item.parent = 0;
                        }
                    });
                    this.driver(r2.items);
                    this.isBusy(false);
                });
            }
        });
        /* End Subscribe */

        /*Auto Complete TemplateName*/
        this.onDatasourceRequestTemplateName = (request, response) => {
            var filter = {
                businessUnitIds: this.businessUnit(),
                vehicleIds: this.selectedVehicle(),
                regionIds: this.selectedZone(),
                travelModes: this.selectedTravelMode(),
                jobCategoryIds: this.selectedShipmentCategory(),
                templateName: request.term
            }
            this.webRequestShipment.autocompleteShipmentTemplate(filter).done((data) => {
                response( $.map( data, (item) => {
                    return {
                        label: item,
                        value: item
                    };
                }));
            }).fail(e => this.handleError(e));
        }
        /*End Auto Complete TemplateName*/
    }

    setupExtend() {
        
        this.selectedVehicle.extend({
            required: {
                onlyIf: () => {
                    return _.size(this.businessUnitVehicle())
                }
            }
        });

        this.selectedDriver.extend({
            required: {
                onlyIf: () => {
                    return _.size(this.businessUnitDriver())
                }
            }
        });

        this.shipmentCreatedStartDate.extend({
            required: {
                onlyIf: () => {
                    return !(_.isNil(this.shipmentCreatedStartTime()) 
                        || this.shipmentCreatedStartTime() == "" 
                        || this.shipmentCreatedStartTime() == null
                        || this.shipmentCreatedStartTime() == undefined)
                }
            }
        })

        this.shipmentCreatedEndDate.extend({
            required: {
                onlyIf: () => {
                    return !(_.isNil(this.shipmentCreatedEndTime()) 
                        || this.shipmentCreatedEndTime() == "" 
                        || this.shipmentCreatedEndTime() == null
                        || this.shipmentCreatedEndTime() == undefined)
                }
            }
        })

        let paramsValidation = {
            shipmentCreatedStartDate: this.shipmentCreatedStartDate,
            shipmentCreatedEndDate: this.shipmentCreatedEndDate,
            selectedVehicle: this.selectedVehicle,
            selectedDriver: this.selectedDriver
        }

        this.validationModel = ko.validatedObservable(paramsValidation);
    }
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    get webRequestShipment() {
        return WebRequestShipment.getInstance();
    }

    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    get webRequestVehicle() {
        return WebRequestVehicle.getInstance();
    }

    get webRequestDriver() {
        return WebRequestDriver.getInstance();
    }

    get webRequestShipmentCategory() {
        return WebRequestShipmentCategory.getInstance();
    }

    get webRequestShipmentZone() {
        return WebRequestShipmentZone.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // TODO: Verify access right to specific items
        if (!isFirstLoad) {
            return;
        }

        // CreateDateStart condition
        this.shipmentCreatedStartDate.subscribe((dateTime) => {
            let startDate = new Date(dateTime);
            let endDate = new Date(this.shipmentCreatedEndDate());

            if(dateTime == null) {
                return
            }

            if(startDate > endDate) {
                this.setCreateDateMaxMin(dateTime);
                this.setCreateDateMaxMin();
            }

            if((startDate.getMonth() - endDate.getMonth()) != 0) {
                this.maxCreateDate(Utility.addMonths(dateTime, 1));
                this.setCreateDateMaxMin();
            }
        });

        // CreateDateEnd condition
        this.shipmentCreatedEndDate.subscribe((dateTime) => {
            let startDate = new Date(this.shipmentCreatedStartDate());
            let endDate = new Date(dateTime);

            if(dateTime == null) {
                return
            }

            if(endDate < startDate) {
                this.setCreateDateMaxMin(dateTime);
                this.setCreateDateMaxMin();
            }

            if((startDate.getMonth() - endDate.getMonth()) != 0) {
                this.minCreateDate(Utility.minusMonths(dateTime, 1));
                this.setCreateDateMaxMin();
            }
        });
        // CreateDateStart condition
    
        var dfd = $.Deferred();

        var dfdBusinessUnit = null;
        var dfdVehicle = null;
        var dfdShipmentStatus = null;
       

        /* Process : Business Unit */
        var businessUnitFilter = {
            companyId: WebConfig.userSession.currentCompanyId
        }

        dfdBusinessUnit = this.webRequestBusinessUnit.listBusinessUnitSummary(businessUnitFilter).done((response) => {
            
            this.businessUnitList(response["items"]);
            this.businessUnit(ScreenHelper.findOptionByProperty(this.businessUnitList, "id"));
        });

        var shipmentStatusFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            types: [Enums.ModelData.EnumResourceType.JobStatus]
        }

        dfdShipmentStatus = this.webRequestEnumResource.listEnumResource(shipmentStatusFilter).done((response) => {
            this.shipmentStatusList(response["items"]);
            this.shipmentStatus(ScreenHelper.findOptionByProperty(this.shipmentStatusList, "value"));
        });

        let travelModeFilter = { 
            companyId: WebConfig.userSession.currentCompanyId, 
            types: Enums.ModelData.EnumResourceType.TravelMode,
            sortingColumns: DefaultSorting.EnumResource
        }

        
        let dfdZone = this.webRequestShipmentZone.listShipmentZone({enable: true});
        let dfdTravelMode = this.webRequestEnumResource.listEnumResource(travelModeFilter);

        /* Check State All Process */
        $.when(dfdBusinessUnit, dfdShipmentStatus, dfdZone, dfdTravelMode).done((r1, r2, r3, r4) => {
            this.zone(r3.items);
            this.travelMode(r4.items);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    setCreateDateMaxMin(dateTime) {
        if(dateTime == undefined) {
            this.minCreateDate(Utility.minusMonths(new Date(), 3));
            this.maxCreateDate(Utility.addMonths(new Date()));
        } else {
            this.minCreateDate(new Date(dateTime));
            this.maxCreateDate(new Date(dateTime));
        }
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        
      
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Search")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdClear", this.i18n("Common_Clear")(), "svg-cmd-clear"));
        // commands.push(
        //     this.createCommand(
        //         "cmdFilter",
        //         this.i18n("Common_Search")(),
        //         "svg-cmd-search"
        //     )
        // );
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {
           
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.submitSearchFilter();
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdClear") {
            this.clearSearchFilter();
        }
    }

    submitSearchFilter() {

        var filter = {};

        var tavelModes = new Array();
        _.forEach(this.selectedTravelMode(), (id) =>{
            let obj = _.find(this.travelMode(), (item)=>{
                return item.id == id
            });
            tavelModes.push(obj.value);
        });

        filter.businessUnitIds = this.businessUnit();
        filter.vehicleIds = this.selectedVehicle();
        filter.driverIds = this.selectedDriver();
        filter.regionIds = this.selectedZone();
        filter.travelModes = tavelModes;
        filter.jobCategoryIds = this.selectedShipmentCategory();

        filter.shipmentFromDate = this.setFormatDateTime(this.shipmentCreatedStartDate(), this.shipmentCreatedStartTime(), "00:00:00");
        filter.shipmentToDate = this.setFormatDateTime(this.shipmentCreatedEndDate(), this.shipmentCreatedEndTime(), "23:59:59");
        

        filter.templateName = this.txtTemplateName();
        filter.shipmentName = this.txtShipmentName();
        filter.createBy = this.txtCreateBy();

        this.publishMessage("cw-shipment-template-search", filter);
    }

    getSelectedBU(isIncludeSubBU)
    {
        if (isIncludeSubBU) {
            var businessUnitId = this.businessUnitList().filter((item) => {
                if (item.parentBusinessUnitId == this.businessUnit()) {
                    return item;
                }
            });

            if (businessUnitId.length == 0) {
                businessUnitId = [parseInt(this.businessUnit())];
            } else {
                businessUnitId = businessUnitId.map((item) => {
                    return item.id;
                });
                businessUnitId.push(parseInt(this.businessUnit()));
            }
        }
        else { businessUnitId = this.businessUnit();}

        return businessUnitId;
    }

    setFormatDateTime(dateObj, timeObj, defaultTime) {

        var newDateTime = "";

        if (dateObj && timeObj) {

            if (typeof (dateObj.getMonth) === 'function') {

                let d = new Date();
                let year = d.getFullYear().toString();
                let month = (d.getMonth() + 1).toString();
                let day = d.getDate().toString();

                month = month.length > 1 ? month : "0" + month;
                day = day.length > 1 ? day : "0" + day;
                newDateTime = year + "-" + month + "-" + day + "T" + timeObj;
            } else {
                let newDate = dateObj.split("T")[0] + "T";
                let newTime = dateObj.split("T")[1] = timeObj + ":00";
                newDateTime = newDate + newTime;
            }

        }
        else if (dateObj && defaultTime) {
            if (typeof (dateObj.getMonth) === 'function') {

                let d = new Date();
                let year = d.getFullYear().toString();
                let month = (d.getMonth() + 1).toString();
                let day = d.getDate().toString();

                month = month.length > 1 ? month : "0" + month;
                day = day.length > 1 ? day : "0" + day;
                newDateTime = year + "-" + month + "-" + day + "T" + defaultTime;
            } else {
                let newDate = dateObj.split("T")[0] + "T";
                let newTime = dateObj.split("T")[1] = defaultTime;
                newDateTime = newDate + newTime;
            }
        }
        else
        { }

        return newDateTime;
    }
    clearSearchFilter() {
        let currentDate = new Date();

        this.businessUnit(null);
        this.includeSubBU(false); 
        this.txtTemplateName(null);
        this.txtShipmentName(null);
        this.txtCreateBy(null);
        this.selectedZone(null);
        this.selectedTravelMode(null);
        this.selectedShipmentCategory(null);
        //this.shipmentCreatedStartDate(currentDate);
        //this.shipmentCreatedEndDate(currentDate);
        //this.shipmentCreatedStartTime('00:00');
        //this.shipmentCreatedEndTime('23:59'); 
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {

    }
}

export default {
    viewModel: ScreenBase.createFactory(ShipmentTemplateSearch),
    template: templateMarkup
};
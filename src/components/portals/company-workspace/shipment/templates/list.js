﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeMargin from "../../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Constants, Enums } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestShipment from "../../../../../app/frameworks/data/apitrackingcore/webRequestShipment";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import GenerateDataGridExpand from "../../generateDataGridExpand";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";
import * as EventAggregator from "../../../../../app/frameworks/constant/eventAggregator";


class ShipmentTemplateList extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Transportation_Mngm_Template_Title")());
        this.bladeSize = BladeSize.XLarge_A1;
        this.bladeIsMaximized = true;

        this.filter = ko.observable();
        this.apiDataSource = ko.observableArray([]);


        //this.filter({
        //    shipmentFromDate: this.setFormatDateTime(new Date(), "00:00:00"),
        //    shipmentToDate: this.setFormatDateTime(new Date(), "23:59:59")
        //});
        // this.filter({
        //     "shipmentFromDate": "2018-07-04T00:00:00",
        //     "shipmentToDate": "2018-07-04T23:59:59"
        // });

        this.onDetailClickColumns = ko.observableArray([]);
        // this.onDetailClickColumns(
        //     GenerateDataGridExpand.getOnClickColumns(this.generateColumnDetail())
        // );
        this.txtRefreshTime = ko.observable("");
        this.txtRefresh = ko.pureComputed(() => {
            return (
                this.i18n("Transportation_Mngm_Shipment_AutoRefresh")() +
                " (" +
                this.i18n("Vehicle_MA_LastSync")() +
                " " +
                this.txtRefreshTime() +
                ")"
            );
        });

        this.isPermissionDelete = false;
        this.isPermissionUpdate = false;
        if (WebConfig.userSession.hasPermission(Constants.Permission.ShipmentTemplatesDelete)) {
            this.isPermissionDelete = true;
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.ShipmentTemplatesUpdate)) {
            this.isPermissionUpdate = true;
        }

        this.onUpdateShipmentTemplate = (data) => {
           

            this.navigate("cw-shipment-templates-manage", {
                shipmentTemplateId: data.id
            });

        }
        this.onDeleteShipmentTemplate = (data) => {
            let shipmentTemplateId = data.id
            

            this.showMessageBox(null, this.i18n("M212")(),
                BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            console.log("DeleteJaa")
                            this.isBusy(true);
                            this.webRequestShipment.deleteShipmentTemplate(shipmentTemplateId).done(() => {
                                this.isBusy(false);
                                this.refreshShipmentList();
                            }).fail((e) => {
                                this.handleError(e);
                            }).always(() => {
                                this.isBusy(false);
                            });
                            break;

                        default: break;
                    }
                });
        }

        this.clickViewOnMap = (data) => { 
            console.log(data)
            this.isBusy(true);
            var viewOnMapQuery = this.webRequestShipment.viewOnmap(data.id);
            $.when(viewOnMapQuery).done(res => {
                this.isBusy(false);
                this.prepareViewOnMapData(res);
            });
        };

        //Subscribe
        this.subscribeMessage("cw-shipment-template-search", info => {
            // console.log("infoSubscribe>>",info)
            this.filter(info);
            this.refreshShipmentList();
        });
        this.subscribeMessage("cw-shipment-template-manage-refresh", info => {
            this.refreshShipmentList();
        });

         //Map Subscribe
         this._eventAggregator.subscribe(
            EventAggregator.MAP_COMMAND_COMPLETE,
            info => {
                switch (info.command) {
                    case "open-shipment-legend-template":
                        this.minimizeAll();
                        break;
                    default:
                        break;
                }
            }
          );
    }
    
    onDatasourceRequestRead(gridOption) {
       
        var dfd = $.Deferred();

        var filter = Object.assign({}, this.filter(), gridOption);

        this.isBusy(true);
        this.webRequestShipment
            .listShipmentTemplateSummary(filter)
            .done(response => {
                console.log("myResponse>>",response)
                this.txtRefreshTime(response.formatTimestamp);
                dfd.resolve({
                    items: response["items"],
                    totalRecords: response["totalRecords"],
                    currentPage: response["currentPage"] 
                    // totalRecords: response["totalRecords"],
                    // currentPage: response["currentPage"]

                });
            })
            .fail(e => {
                this.handleError(e);
            })
            .always(() => {
                this.isBusy(false);
                // this.onAutoRefresh(true);
            });

        return dfd;
    }

    onDetailInit(evt) {
        var self = this;
        var templateId = evt.data.id
        // var shipmentID = evt.data.shipmentId;
        this.webRequestShipment
            .getShipmentTemplateDetail(templateId)
            .done(response => {
                self.formatDetailData(response);
                $("<div id='" + self.id + templateId + "'/>")
                    .appendTo(evt.detailCell)
                    .kendoGrid({
                        dataSource: response,
                        noRecords: {
                            template: self.i18n("M111")()
                        },
                        pageable: GenerateDataGridExpand.getPageable(false),
                        // set column of kendo grid
                        columns: GenerateDataGridExpand.getColumns(
                            self.generateColumnDetail()
                        )
                    });
            })
            .fail(e => {
                this.handleError(e);
            })
            .always(() => {
                this.isBusy(false);
            });
    }
    formatDetailData(response) {
        for (var i = 0; i < response.length; i++) {
            response[i].renderShipmentDetailColumn = this.renderShipmentDetailColumn;
        }
    }
    generateColumnDetail() {
        var self = this;
        var columnsDetail = [
            {
                type: "text",
                width: "15px",
                title: this.i18n("Transportation_Mngm_Shipment_Order")(),
                data: "order",
                className: "dg-body-right"
            },
            {
                type: "text",
                width: "100px",
                title: this.i18n("Transportation_Mngm_Shipment_Template_Waypoint")(),
                data: "jobWaypointName",
                className: "dg-body-left"
            },
            {
                type: "text",
                width: "30px",
                title: this.i18n("Transportation_Mngm_Shipment_Template_Stop_Time")(),
                data: "formatPlanStopTime",
                className: "dg-body-right"
            },
            {
                type: "text",
                width: "30px",
                title: this.i18n("Transportation_Mngm_Shipment_Template_Travel_Time")(),
                data: "formatPlanDriveTime",
                className: "dg-body-right"
            },
            {
                type: "text",
                width: "30px",
                title: this.i18n("Transportation_Mngm_Shipment_Plan_Distance")(),
                data: "formatPlanDistance",
                className: "dg-body-right"
            },
            {
                type: "text",
                width: "30px",
                title: this.i18n("Transportation_Mngm_Shipment_Radius")(),
                data: "radius",
                className: "dg-body-right"
            },
            {
                type: "text",
                width: "30px",
                title: this.i18n("Transportation_Mngm_Shipment_Template_Min_Park")(),
                data: "formatInsideWaypointMinTime",
                className: "dg-body-right"
            },
            {
                type: "text",
                width: "30px",
                title: this.i18n("Transportation_Mngm_Shipment_Template_Max_Park")(),
                data: "formatInsideWaypointMaxTime",
                className: "dg-body-right"
            },
            {
                type: "text",
                width: "30px",
                title: this.i18n("Transportation_Mngm_Shipment_Delivery_Type")(),
                data: "deliveryTypeDisplayName",
                className: "dg-body-left"
            }
        ];
        return columnsDetail;
    }

    refreshShipmentList() {
        this.dispatchEvent("dgShipmentLst", "refresh");
    }
    // formatData(response) {
    //     var data = {
    //         items: []
    //     };
    //     console.log("response in formatdata>>>",response)
    //     for (let i = 0; i < response.items.length; i++) {
    //         var dataItem = response.items[i];

    //         dataItem.formatWaypoint = {
    //             waypointName: dataItem.waypointName,
    //             formatETA: dataItem.formatETA,
    //             formatNE: dataItem.formatNE,
    //             iconName: dataItem.icon.name,
    //             etaColor: dataItem.etaColor
    //         };

    //         dataItem.formatDisplayStartDate = {
    //             formatPlanStartDate: dataItem.formatPlanStartDate,
    //             formatActualStartDate: dataItem.formatActualStartDate,
    //             startLateColor: dataItem.startLateColor
    //         };

    //         dataItem.formatDisplayEndDate = {
    //             formatPlanEndDate: dataItem.formatPlanEndDate,
    //             formatActualEndDate: dataItem.formatActualEndDate,
    //             endLateColor: dataItem.endLateColor
    //         };

    //         dataItem.driverNameAndMobile = {
    //             driverName: dataItem.driverName, driverMobile: dataItem.driverMobile
    //         };

    //         data.items.push(dataItem);
    //     }
    //     return data;
    // }

    setupExtend() {

    }

    get webRequestShipment() {
        return WebRequestShipment.getInstance();
    }

    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {

        if (!isFirstLoad)
            return;

        let dfd = $.Deferred();
        dfd.resolve();

        return dfd;
    }
    /**
       * Clear reference from DOM for garbage collection.
       * @lifecycle Called when View is unloaded.
       */
      onUnload() {
        MapManager.getInstance().closeViewOnMap();
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {

    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.ShipmentTemplatesCreate)) {
            commands.push(
                this.createCommand(
                    "cmdCreate",
                    this.i18n("Common_Create")(),
                    "svg-cmd-add"
                )
            );
        }
        commands.push(
            this.createCommand(
                "cmdFilter",
                this.i18n("Common_Search")(),
                "svg-cmd-search"
            )
        );
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdFilter":
                this.navigate("cw-shipment-templates-search", {});
                break;
            case "cmdCreate":
                this.navigate("cw-shipment-templates-manage", {});
                break;

            default:
                break;
        }
    }
    setFormatDateTime(dateObj, time) {
        var newDateTime = "";

        if (dateObj) {
            if (typeof dateObj.getMonth === "function") {
                let d = new Date();
                let year = d.getFullYear().toString();
                let month = (d.getMonth() + 1).toString();
                let day = d.getDate().toString();

                month = month.length > 1 ? month : "0" + month;
                day = day.length > 1 ? day : "0" + day;
                newDateTime = year + "-" + month + "-" + day + "T" + time;
            } else {
                let newDate = dateObj.split("T")[0] + "T";
                let newTime = (dateObj.split("T")[1] = time);
                newDateTime = newDate + newTime;
            }
        }
        return newDateTime;
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {

    }

    prepareViewOnMapData(result) {

        result.driverName = result.driverName.trim();

        var viewonmapData = {
            panel: {
                jobName: _.isNil(result.jobName) ? "-" : result.jobName,
                driverName: _.isNil(result.driverName) ? "-" : result.driverName,
                jobCode: _.isNil(result.jobCode) ? "-" : result.jobCode,
                jobStatusDisplayName: _.isNil(result.jobStatusDisplayName) ? "-" : result.jobStatusDisplayName,
                vehicleLicense: _.isNil(result.vehicleLicense) ? "-" : result.vehicleLicense
            },
            actual: {
                pin: [],
                route: []
            },
            plan: {
                pin: [],
                route: []
            },
            vehicle: []
        };

        _.forEach(result.shipmentPlanInfos, (planInfo) => {
            var pinItem = {
                longitude: planInfo.longitude,
                latitude: planInfo.latitude,
                attributes: {
                    jobWaypointStatus: planInfo.jobWaypointStatus,
                    jobWaypointName: planInfo.jobWaypointName,
                    info: [
                        {
                            "title": this.i18n("waypoint-info-status")(),
                            "key": "jobWaypointStatusDisplayName",
                            "text": planInfo.jobWaypointStatusDisplayName || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-planIncomingDate")(),
                            "key": "formatPlanIncomingDate",
                            "text": planInfo.formatPlanIncomingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-actualIncomingDate")(),
                            "key": "formatActualIncomingDate",
                            "text": planInfo.formatActualIncomingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-actualOutgoingDate")(),
                            "key": "formatActualOutgoingDate",
                            "text": planInfo.formatActualOutgoingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-remark")(),
                            "key": "jobDescription",
                            "text": planInfo.jobDescription || '-'
                        }
                    ]
                }
            };
            viewonmapData.plan.pin.push(pinItem);
            viewonmapData.plan.route = viewonmapData.plan.route.concat(planInfo.shipmentLocations);
        });

        // _.forEach(result.shipmentActualInfos, (actualInfo) => {
        //     var pinItem = {
        //         longitude: actualInfo.longitude,
        //         latitude: actualInfo.latitude,
        //         attributes: {
        //             jobWaypointStatus: actualInfo.jobWaypointStatus,
        //             jobWaypointName: actualInfo.jobWaypointName,
        //             info: [
        //                 {
        //                     "title": this.i18n("waypoint-info-status")(),
        //                     "key": "jobWaypointStatusDisplayName",
        //                     "text": actualInfo.jobWaypointStatusDisplayName || '-'
        //                 },
        //                 {
        //                     "title": this.i18n("waypoint-info-planIncomingDate")(),
        //                     "key": "formatPlanIncomingDate",
        //                     "text": actualInfo.formatPlanIncomingDate || '-'
        //                 },
        //                 {
        //                     "title": this.i18n("waypoint-info-actualIncomingDate")(),
        //                     "key": "formatActualIncomingDate",
        //                     "text": actualInfo.formatActualIncomingDate || '-'
        //                 },
        //                 {
        //                     "title": this.i18n("waypoint-info-actualOutgoingDate")(),
        //                     "key": "formatActualOutgoingDate",
        //                     "text": actualInfo.formatActualOutgoingDate || '-'
        //                 },
        //                 {
        //                     "title": this.i18n("waypoint-info-remark")(),
        //                     "key": "jobDescription",
        //                     "text": actualInfo.jobDescription || '-'
        //                 }
        //             ]
        //         }
        //     };
        //     viewonmapData.actual.pin.push(pinItem);
        //     viewonmapData.actual.route = viewonmapData.actual.route.concat(actualInfo.shipmentLocations);
        // });


        // var directionConst = ["N", "NE", "E", "SE", "S", "SW", "W", "NW"];
        // _.forEach(result.locationInfo, (locationInfo) => {


        //     var locationInfoStatus = AssetInfo.getLocationInfoStatus(locationInfo);
        //     var shipmentInfo = AssetInfo.getShipmentInfo(locationInfo, locationInfoStatus);

        //     locationInfo.vehicleLicense = _.isNil(locationInfo.vehicleLicense) ? "-" : locationInfo.vehicleLicense;
            
        //     var vehicleItem = {
        //         longitude: locationInfo.longitude,
        //         latitude: locationInfo.latitude,
        //         attributes: {
        //             "TITLE": locationInfo.vehicleLicense,
        //             "SHIPMENTINFO": shipmentInfo
        //         }
        //     };

        //     var vehicleIcon = result.vehicleIconPath["1"];
        //     var movementImageUrl = "";
        //     _.forEach(result.vehicleIconPath["1"], (vehicleIcon) => {
        //         if (vehicleIcon.movementType == locationInfo.movement) {
        //             movementImageUrl = vehicleIcon.imageUrl + "/" + directionConst[locationInfo.directionType - 1] + ".png"
        //         }
        //     });
        //     vehicleItem.movementImageUrl = Utility.resolveUrl(movementImageUrl);
        //     viewonmapData.vehicle.push(vehicleItem);
        // });
        MapManager.getInstance().clear();
        MapManager.getInstance().openShipmentLegendTemplate({ data: viewonmapData });
    }
}

export default {
    viewModel: ScreenBase.createFactory(ShipmentTemplateList),
    template: templateMarkup
};
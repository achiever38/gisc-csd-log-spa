﻿import ko from "knockout";
import templateMarkup from "text!./result.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as Screen from "../../../../app/frameworks/constant/screen";
import * as EventAggregatorConstant from "../../../../app/frameworks/constant/eventAggregator";
import {Constants, Enums} from "../../../../app/frameworks/constant/apiConstant";
import ContextMenuManager from "../../../controls/gisc-chrome/contextmenu/contextmenuManager";
import WebRequestGlobalSearch from "../../../../app/frameworks/data/apitrackingcore/webRequestGlobalSearch";
import WebRequestVehicle from "../../../../app/frameworks/data/apitrackingcore/webRequestVehicle";
import WebRequestUser from "../../../../app/frameworks/data/apicore/webRequestUser";
import WebRequestFleetMonitoring from "../../../../app/frameworks/data/apitrackingcore/webRequestFleetMonitoring";
import WebRequestCustomPOI from "../../../../app/frameworks/data/apitrackingcore/webRequestCustomPOI";
import WebRequestCustomArea from "../../../../app/frameworks/data/apitrackingcore/webRequestCustomArea";
import WebRequestCustomRoute from "../../../../app/frameworks/data/apitrackingcore/webRequestCustomRoute";
import WebRequestMap from "../../../../app/frameworks/data/apitrackingcore/webRequestMap";
import MapManager from "../../../controls/gisc-chrome/map/mapManager";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import FindCategory from "./findCategory";

// Logistics context menu.
const CONTEXT_BU_DETAIL = "ctx110";
const CONTEXT_VEHICLE_DETAIL = "ctx120";
const CONTEXT_VEHICLE_VIEW_MAP = "ctx121";
const CONTEXT_VEHICLE_VIEW_PLAYBACK = "ctx122";
const CONTEXT_POI_DETAIL = "ctx130";
const CONTEXT_POI_VIEW_MAP = "ctx131";
const CONTEXT_POI_FIND_ASSET = "ctx132";
const CONTEXT_AREA_DETAIL = "ctx140";
const CONTEXT_AREA_VIEW_MAP = "ctx141";
const CONTEXT_ROUTE_DETAIL = "ctx150";
const CONTEXT_ROUTE_VIEW_MAP = "ctx151";
const CONTEXT_DRIVER_DETAIL = "ctx160";
const CONTEXT_USER_DETAIL = "ctx170";
const CONTEXT_USER_VIEW_MAP = "ctx171";
const CONTEXT_USER_VIEW_PLAYBACK = "ctx172";

// Map context menu.
const CONTEXT_MAP_VIEWONMAP = "ctx210";
const CONTEXT_MAP_VIEWSTREET_ONMAP = "ctx211";
// Admin Poly context menu.
const CONTEXT_ADMINPOLY_VIEWONMAP = "ctx310";

/**
 * Global search result screen.
 */
class GlobalSearchResultScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("GlobalSearch_SearchResults")());
        this.bladeSize = BladeSize.Medium;
        this.bladeMargin = BladeMargin.Narrow;

        // Page properties.
        this.contextMenu = ContextMenuManager.getInstance();
        this.displayStart = 0;
        this.skipServerLoadMore = false;
        this.pageSize = 20; // Call extra items for determine next page available.
        this.visibleViewport = ko.observable(20);
        this.searchType = this.ensureNonObservable(params.searchType, -1);
        this.keyword = this.ensureNonObservable(params.keyword, "");
        this.category = this.ensureNonObservable(params.category, "");
        this.latitude = this.ensureNonObservable(params.latitude, "");
        this.longitude = this.ensureNonObservable(params.longitude, "");
        this.items = ko.observableArray();
        this.canViewLoadMore = ko.pureComputed(() => {
            return this.visibleViewport() < this.items().length;
        });

        // Service properties.
        this.webRequestFleetMonitoring = WebRequestFleetMonitoring.getInstance();
        this.webRequestGlobalSearch = WebRequestGlobalSearch.getInstance();
        this.webRequestVehicle = WebRequestVehicle.getInstance();
        this.webRequestUser = WebRequestUser.getInstance();
        this.webRequestCustomPOI = WebRequestCustomPOI.getInstance();
        this.webRequestCustomArea = WebRequestCustomArea.getInstance();
        this.webRequestCustomRoute = WebRequestCustomRoute.getInstance();
        this.webRequestMap = WebRequestMap.getInstance();

        // Register event subscribe.
        this._eventAggregator.subscribe(EventAggregatorConstant.MAP_COMMAND_COMPLETE, this.onMapComplete.bind(this));
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) {
            return;
        }

        // Do not search when user refresh browser manually.
        if(this.searchType === -1){
            return;
        }
        else {
            return this.performSearch(isFirstLoad);
        }
    }

    /**
     * Perform search to server with user keyword and category.
     * @param {boolean} isFirstLoad
     * @return {jQuery Deferred}
     */
    performSearch(isFirstLoad) {
        var dfdPerformSearch = $.Deferred();
        var filter = {
            companyId: WebConfig.userSession.currentCompanyId,
            type: this.searchType,
            catagories: [this.category],
            keyword: this.keyword,
            latitude: this.latitude,
            longitude: this.longitude
        };

        // Do not send page size when search admin poly.
         switch(this.searchType) {
            case Enums.ModelData.GlobalSearchType.Logistics:
            case Enums.ModelData.GlobalSearchType.Map:
                var effectiveDisplayLength = this.pageSize + 1;

                // Search start from 0 but increase each time.
                if(!isFirstLoad) {
                    // We need to increase display start fromm page size manually.
                    this.displayStart += (effectiveDisplayLength);
                }

                filter.displayStart = this.displayStart;
                filter.displayLength = effectiveDisplayLength;
                break;
            case Enums.ModelData.GlobalSearchType.AdminPoly:
            case Enums.ModelData.GlobalSearchType.Street:
                // Search for all items so no need to send paginage params.
                break;
        }

        // Calling service for search result.
        if(!isFirstLoad) {
            this.isBusy(true);
        }
        this.webRequestGlobalSearch.list(filter)
        .done((result) => {
            if(result.items.length) {
                // Populate extra icon for each item.
                result.items.forEach((item) => {
                    // Attach icon property.
                    item.icon = FindCategory.getIconName(this.searchType, item.category);
                    // Attach selected property.
                    item.selected = ko.observable(false);
                    // Append to items datasource.
                    this.items.push(item);
                });
            }
            if(result.items.length <= this.pageSize) {
                // Skip server next call.
                this.skipServerLoadMore = true;
            }

            dfdPerformSearch.resolve();
        })
        .fail((e) => {
            dfdPerformSearch.reject(e);
        })
        .always(() => {
            if(!isFirstLoad) {
                this.isBusy(false);
            }
        });

        return dfdPerformSearch;
    }

    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {}

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * Update selected state of search result.
     * 
     * @param {any} newItem 
     */
    updateSelectedItem(newItem) {
        // Unselect old item if exists.
        if(this.selectedItem) {
            this.selectedItem.selected(false);
        }

        // Update with new one.
        if(newItem) {
            newItem.selected(true);
        }

        this.selectedItem = newItem;
    }

    /**
     * Append new trackable item.
     * 
     * @param {any} vehicleId 
     * @param {any} deliveryManId
     */
    updateTrackLoc(vehicleId, deliveryManId) {
        var newVehicles = WebConfig.fleetMonitoring.trackLocationViewVehicleIds().slice();
        var newDeliveryMen = WebConfig.fleetMonitoring.trackLocationViewDeliveryManIds().slice();

        if(vehicleId) {
            newVehicles = _.union(newVehicles, [vehicleId]);
        }

        if(deliveryManId) {
            newDeliveryMen = _.union(newDeliveryMen, [deliveryManId]);
        }

        MapManager.getInstance().setFollowTracking(true);
        this.isBusy(true);
        WebConfig.fleetMonitoring.updateTrackLocationAssetIds(
            this.webRequestFleetMonitoring,
            WebConfig.userSession,
            newVehicles,
            newDeliveryMen,
            WebConfig.fleetMonitoring.trackLocationFollowVehicleIds().slice(),
            WebConfig.fleetMonitoring.trackLocationFollowDeliveryManIds().slice()
        )
        .done(() => {
            // Possible values are undefined, object with empty results.
            // For all case we need to find from track location items.
            var trackLoc = null;
            WebConfig.fleetMonitoring.trackLocationItems().forEach((t) => {
                if(vehicleId) {
                    if(t.vehicleId === vehicleId) {
                        trackLoc = t;
                    }
                }
                else if(deliveryManId) {
                    if(t.deliveryManId === deliveryManId) {
                        trackLoc = t;
                    }
                }
            });

             // Call map for pan zoom if trackloc available.
             if(trackLoc) {
                MapManager.getInstance().zoomPoint(
                    this.id,
                    trackLoc.latitude,
                    trackLoc.longitude,
                    WebConfig.mapSettings.defaultZoom
                );
             }
        })
        .fail((e) => {
            this.handleError(e);
        })
        .always(() => {
            this.isBusy(false);
        });
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}

    /**
     * Open blade for search result screen.
     * @param {any} menuItem 
     * @param {any} screenName 
     * @param {any} optionObj
     */
    navigateToScreen(menuItem, screenName, optionObj = {}) {
        // Append object with readonly mode.
        optionObj.mode = Screen.SCREEN_MODE_READONLY;

        // Calling navigate with deferred done to hilight active row.
        this.navigate(screenName, optionObj).done(() => {
            this.updateSelectedItem(menuItem.item);
        });
    }

    /**
     * Handle when user clicking on context menu.
     * @param {any} menuItem
     */
    onContextMenuExecute(menuItem) {
        var defaultPOIUrl = this.resolveUrl("~/images/pin_destination.png");
        var defaultPOISymbol = {
            url: defaultPOIUrl,
            width: WebConfig.mapSettings.symbolWidth,
            height: WebConfig.mapSettings.symbolHeight,
            offset: {
                x: WebConfig.mapSettings.symbolOffsetX,
                y: WebConfig.mapSettings.symbolOffsetY
            },
            rotation: WebConfig.mapSettings.symbolRotation
        };

        var hex = (x) => {
            let hexDigits = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f");
            return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
        };

        var rgbTohex = (rgb) => {
            let objRgb = rgb.split(",");
            return "#" + hex(objRgb[0]) + hex(objRgb[1]) + hex(objRgb[2]);
        };

        switch(menuItem.id) {
            case CONTEXT_BU_DETAIL:
                this.navigateToScreen(menuItem, "ca-business-unit-view", {id: menuItem.item.entityId});
                break;
            case CONTEXT_VEHICLE_DETAIL:
                this.navigateToScreen(menuItem, "ca-asset-vehicle-view", {id: menuItem.item.entityId});
                break;
            case CONTEXT_VEHICLE_VIEW_MAP:
                this.updateTrackLoc(menuItem.item.entityId, null);
                break;
            case CONTEXT_VEHICLE_VIEW_PLAYBACK:
                this.navigationService.navigate("cw-fleet-monitoring-playback", { id: "V-" + menuItem.item.entityId });
                break;
            case CONTEXT_DRIVER_DETAIL:
                this.navigateToScreen(menuItem, "ca-asset-driver-view", {driverId: menuItem.item.entityId});
                break;
            case CONTEXT_USER_DETAIL:
                this.navigateToScreen(menuItem, "ca-user-view", {userId: menuItem.item.entityId});
                break;
            case CONTEXT_USER_VIEW_MAP:
                this.updateTrackLoc(null, menuItem.item.entityId);
                break;
            case CONTEXT_USER_VIEW_PLAYBACK:
                this.navigationService.navigate("cw-fleet-monitoring-playback", { id: "D-" + menuItem.item.entityId });
                break;
            case CONTEXT_AREA_DETAIL:
                this.navigateToScreen(menuItem, "cw-geo-fencing-custom-area-view", {id: menuItem.item.entityId});
                break;
            case CONTEXT_AREA_VIEW_MAP:
                this.isBusy(true);
                this.webRequestCustomArea.getCustomArea(menuItem.item.entityId)
                .done((result) => {
                    var effectiveFillColor = WebConfig.mapSettings.fillColor;
                    var effectiveBorderColor = WebConfig.mapSettings.borderColor;
                    if(result.type) {
                        effectiveFillColor = rgbTohex(result.type.symbolFillColor);
                        effectiveBorderColor = rgbTohex(result.type.symbolBorderColor);
                    }

                    var points = JSON.parse(result.points);

                    MapManager.getInstance().drawCustomArea(
                        this.id,
                        points,
                        effectiveFillColor,
                        effectiveBorderColor,
                        WebConfig.mapSettings.fillOpacity,
                        WebConfig.mapSettings.borderOpacity
                    );
                })
                .always(() => {
                    this.isBusy(false);
                });
                break;
            case CONTEXT_POI_DETAIL:
                this.navigateToScreen(menuItem, "cw-geo-fencing-custom-poi-view", {customPoiId: menuItem.item.entityId});
                break;
            case CONTEXT_POI_VIEW_MAP:
                this.isBusy(true);
                this.webRequestCustomPOI.getCustomPOI(menuItem.item.entityId)
                .done((result) => {
                    // Try to populate the poi icon from current POI setting.
                    // If not available then we use default poi.
                    var effectiveSymbol = defaultPOISymbol;

                    if(result.icon && result.icon.image.fileUrl !== null) {
                        // Populate new symbol from system.
                        effectiveSymbol = {
                            url: this.resolveUrl("~/images/block_hilight.png"),
                            width: 32,
                            height: 32,
                            offset: {
                                x: 0,
                                y: 0
                            },
                            rotation: WebConfig.mapSettings.symbolRotation
                        };
                    }
                    MapManager.getInstance().openCustomPOILayer();
                    MapManager.getInstance().drawOnePointZoom(
                        this.id, 
                        {lat: result.latitude, lon: result.longitude},
                        effectiveSymbol,
                        WebConfig.mapSettings.defaultZoom,
                        null
                    );
                })
                .always(() => {
                    this.isBusy(false);
                });
                break;
            case CONTEXT_POI_FIND_ASSET:
                this.isBusy(true);
                this.webRequestCustomPOI.getCustomPOI(menuItem.item.entityId)
                .done((result) => {
                    this.navigationService.navigate("cw-map-functions-find-nearest-asset", { 
                        lat: result.latitude,
                        lon: result.longitude
                    });
                })
                .always(() => {
                    this.isBusy(false);
                });
                break;
            case CONTEXT_ROUTE_DETAIL:
                this.navigateToScreen(menuItem, "cw-geo-fencing-custom-route-view", {customRouteId: menuItem.item.entityId});
                break;
            case CONTEXT_ROUTE_VIEW_MAP:
                this.isBusy(true);
                this.webRequestCustomRoute.getCustomRoute(menuItem.item.entityId)
                .done((result) => {
                    var effectiveRouteColor = result.color;
                    var pictures = [];
                    var path = [];

                    // Prepare solve route parameter.
                    var solveRouteOption = {
                        routeMode: result.routeOption.routeMode,
                        routeOption: result.routeOption.routeOption,
                        lang: WebConfig.userSession.currentUserLanguage == "en-US" ? "E" : "L",
                        stops: []
                    };

                    // Execute solve route service if viapoints start from 2 points.
                    if (result.viaPoints.length >= 2) {
                        ko.utils.arrayForEach(result.viaPoints, function(p) {
                            solveRouteOption.stops.push({
                                name: p.name,
                                lat: p.latitude,
                                lon: p.longitude
                            });
                        });

                        // Calling route service.
                        this.webRequestMap.solveRoute(solveRouteOption).done((solveRouteRes) => {
                            var solveRouteResult = JSON.parse(solveRouteRes);
                            
                            if(solveRouteResult && solveRouteResult.results) {
                                ko.utils.arrayForEach(result.viaPoints, (item, index) => {
                                    var pointOrder = index + 1;
                                    // Display last point with destionation image.
                                    if( pointOrder === result.viaPoints.length){
                                        pictures.push(this.resolveUrl("~/images/pin_destination.png"));
                                    }
                                    else
                                    {
                                        // Display start point from number.
                                        pictures.push(this.resolveUrl(`~/images/pin_poi_${pointOrder}.png`));
                                    }
                                });

                                // Render route to map.
                                MapManager.getInstance().drawOneRoute(
                                    this.id,
                                    solveRouteOption.stops,
                                    pictures,
                                    solveRouteResult.results.route,
                                    effectiveRouteColor,
                                    WebConfig.mapSettings.lineWidth,
                                    WebConfig.mapSettings.borderOpacity,
                                    result
                                );
                            }
                        });
                    }
                })
                .always(() => {
                    this.isBusy(false);
                });
                break;
            case CONTEXT_MAP_VIEWONMAP:
                MapManager.getInstance().drawOnePointZoom(
                    this.id, 
                    {lat: menuItem.item.geometry.latitude, lon: menuItem.item.geometry.longitude},
                    defaultPOISymbol,
                    WebConfig.mapSettings.defaultZoom,
                    null
                );
                break;
            case CONTEXT_MAP_VIEWSTREET_ONMAP:
                MapManager.getInstance().drawMultipleLineZoom(
                    this.id,
                    menuItem.item.geometry.route,
                    WebConfig.mapSettings.borderColor,
                    WebConfig.mapSettings.lineStyle,
                    WebConfig.mapSettings.lineWidth
                );
                break;
            case CONTEXT_ADMINPOLY_VIEWONMAP:
                MapManager.getInstance().drawCustomArea(
                    this.id,
                    menuItem.item.geometry.area,
                    WebConfig.mapSettings.fillColor,
                    WebConfig.mapSettings.borderColor,
                    WebConfig.mapSettings.fillOpacity,
                    WebConfig.mapSettings.borderOpacity
                );
                break;
        }
    }

    /**
     * handle when user click context menu.
     * @param {any} item 
     * @param {any} element 
     */
    onContextMenuClick(item, element) {
        // Finding offset of clicked element.
        var contextOffset = $(element).offset();
        var menuItems = [];
        var dfdMenuBuildComplete = null;

        // Checking permission when display view playback or find nearest assets.
        var canViewPlayback = WebConfig.userSession.hasPermission(Constants.Permission.ViewPlayback);
        var canFindNearestAssets = WebConfig.userSession.hasPermission(Constants.Permission.MapFunctions);
        var canTrack = WebConfig.userSession.hasPermission(Constants.Permission.TrackMonitoring);

        switch(this.searchType) {
            case Enums.ModelData.GlobalSearchType.Logistics:
                switch(parseInt(item.category)) {
                    case Enums.ModelData.GlobalSearchLogisticsCategory.BusinessUnit:
                        // view detail menu.
                        menuItems.push({id: CONTEXT_BU_DETAIL, text: this.i18n("GlobalSearch_ViewDetail")(), item: item});
                        break;
                    case Enums.ModelData.GlobalSearchLogisticsCategory.Vehicle:
                        // view detail menu.
                        menuItems.push({id: CONTEXT_VEHICLE_DETAIL, text: this.i18n("GlobalSearch_ViewDetail")(), item: item});
                        // checking for trackable vehicle if true then add extra menu.
                        dfdMenuBuildComplete = this.webRequestVehicle.checkFleetServiceAssociation(item.entityId)
                        .done((hasAssociated) => {
                            if(hasAssociated){
                                if(canTrack) {
                                    menuItems.push({id: CONTEXT_VEHICLE_VIEW_MAP, text: this.i18n("Common_ViewOnMap")(), item: item});
                                }
                                if(canViewPlayback) {
                                    menuItems.push({id: CONTEXT_VEHICLE_VIEW_PLAYBACK, text: this.i18n("Common_ViewPlayback")(), item: item});
                                }
                            }
                        });
                        break;
                    case Enums.ModelData.GlobalSearchLogisticsCategory.Driver:
                        // view detail menu.
                        menuItems.push({id: CONTEXT_DRIVER_DETAIL, text: this.i18n("GlobalSearch_ViewDetail")(), item: item});
                        break;
                    case Enums.ModelData.GlobalSearchLogisticsCategory.User:
                        // view detail menu.
                        menuItems.push({id: CONTEXT_USER_DETAIL, text: this.i18n("GlobalSearch_ViewDetail")(), item: item});
                        // checking for trackable user if true then add extra menu.
                        dfdMenuBuildComplete = this.webRequestUser.checkFleetServiceAssociation(item.entityId)
                        .done((hasAssociated) => {
                            if(hasAssociated){
                                if(canTrack) {
                                    menuItems.push({id: CONTEXT_USER_VIEW_MAP, text: this.i18n("Common_ViewOnMap")(), item: item});
                                }
                                if(canViewPlayback) {
                                    menuItems.push({id: CONTEXT_USER_VIEW_PLAYBACK, text: this.i18n("Common_ViewPlayback")(), item: item});
                                }
                            }
                        });
                        break;
                    case Enums.ModelData.GlobalSearchLogisticsCategory.CustomPoi:
                        menuItems.push({id: CONTEXT_POI_DETAIL, text: this.i18n("GlobalSearch_ViewDetail")(), item: item});
                        menuItems.push({id: CONTEXT_POI_VIEW_MAP, text: this.i18n("Common_ViewOnMap")(), item: item});
                        if(canFindNearestAssets) {
                            menuItems.push({id: CONTEXT_POI_FIND_ASSET, text: this.i18n("Map_FindNearestAsset")(), item: item});
                        }
                        break;
                    case Enums.ModelData.GlobalSearchLogisticsCategory.CustomArea:
                        menuItems.push({id: CONTEXT_AREA_DETAIL, text: this.i18n("GlobalSearch_ViewDetail")(), item: item});
                        menuItems.push({id: CONTEXT_AREA_VIEW_MAP, text: this.i18n("Common_ViewOnMap")(), item: item});
                        break;
                    case Enums.ModelData.GlobalSearchLogisticsCategory.CustomRoute:
                        menuItems.push({id: CONTEXT_ROUTE_DETAIL, text: this.i18n("GlobalSearch_ViewDetail")(), item: item});
                        menuItems.push({id: CONTEXT_ROUTE_VIEW_MAP, text: this.i18n("Common_ViewOnMap")(), item: item});
                        break;
                }
                break;
            case Enums.ModelData.GlobalSearchType.Map:
                menuItems.push({id: CONTEXT_MAP_VIEWONMAP, text: this.i18n("Common_ViewOnMap")(), item: item});
                break;
            case Enums.ModelData.GlobalSearchType.AdminPoly:
                menuItems.push({id: CONTEXT_ADMINPOLY_VIEWONMAP, text: this.i18n("Common_ViewOnMap")(), item: item});
                break;
            case Enums.ModelData.GlobalSearchType.Street:
                menuItems.push({id: CONTEXT_MAP_VIEWSTREET_ONMAP, text: this.i18n("Common_ViewOnMap")(), item: item});
                break;
        }

        // Access context menu from singleton.
        $.when(dfdMenuBuildComplete).done(()=> {
            if(menuItems.length) {
                this.contextMenu.show(
                    menuItems, 
                    contextOffset.left - 220,
                    contextOffset.top - 20
                ).done((item) => {
                    this.onContextMenuExecute(item);
                });
            }
        });
    }

    /**
     * Handle when map command executed succesfully.
     * 
     * @param {any} data
     */
    onMapComplete(data) {
        switch(data.command) {
            case "draw-custom-area":
            case "draw-one-point-zoom":
            case "draw-one-route":
            case "draw-multiple-line-zoom":
            case "zoom-point":
                if(this.isActive()) {
                    this.minimizeAll();
                }
                break;
        }
    }

    /**
     * Handle when user clicks load more.
     */
    onLoadMoreClick() {
        // Do not send page size when search admin poly.
         switch(this.searchType) {
            case Enums.ModelData.GlobalSearchType.Logistics:
            case Enums.ModelData.GlobalSearchType.Map:
                // Calling service for get more datasource then refresh list.
                if(!this.skipServerLoadMore) {
                    this.performSearch(false)
                    .always(() => {
                        this.visibleViewport(this.visibleViewport() + this.pageSize);
                    });
                }
                else {
                    // If returned result has zero then toggle internal search to skip server call.
                    this.visibleViewport(this.visibleViewport() + this.pageSize);
                }

                break;
            case Enums.ModelData.GlobalSearchType.AdminPoly:
            case Enums.ModelData.GlobalSearchType.Street:
                // Search for all items so no need to send paginage params.
                this.visibleViewport(this.visibleViewport() + this.pageSize);
                break;
        }
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.updateSelectedItem(null);
    }

    onDefaultClick(item){
        var defaultItem = new Object();
        var dfdDefaultItemBuildComplete = null;

        // Checking permission when display view playback or find nearest assets.
        var canTrack = WebConfig.userSession.hasPermission(Constants.Permission.TrackMonitoring);

        switch(this.searchType) {
            case Enums.ModelData.GlobalSearchType.Logistics:
                switch(parseInt(item.category)) {
                    case Enums.ModelData.GlobalSearchLogisticsCategory.BusinessUnit:
                        // view detail.
                        defaultItem = {id: CONTEXT_BU_DETAIL, text: this.i18n("GlobalSearch_ViewDetail")(), item: item};
                        break;
                    case Enums.ModelData.GlobalSearchLogisticsCategory.Vehicle:
                        // checking for trackable vehicle if true then add extra menu.
                        dfdDefaultItemBuildComplete = this.webRequestVehicle.checkFleetServiceAssociation(item.entityId)
                        .done((hasAssociated) => {
                            if(hasAssociated){
                                if(canTrack) {
                                    defaultItem = {id: CONTEXT_VEHICLE_VIEW_MAP, text: this.i18n("Common_ViewOnMap")(), item: item};
                                }else{
                                    defaultItem = {id: CONTEXT_VEHICLE_DETAIL, text: this.i18n("GlobalSearch_ViewDetail")(), item: item};
                                }
                            }else{
                                defaultItem = {id: CONTEXT_VEHICLE_DETAIL, text: this.i18n("GlobalSearch_ViewDetail")(), item: item};
                            }
                        });
                        break;
                    case Enums.ModelData.GlobalSearchLogisticsCategory.Driver:
                        // view detail.
                        defaultItem = {id: CONTEXT_DRIVER_DETAIL, text: this.i18n("GlobalSearch_ViewDetail")(), item: item};
                        break;
                    case Enums.ModelData.GlobalSearchLogisticsCategory.User:
                        // checking for trackable user if true then add extra menu.
                        dfdDefaultItemBuildComplete = this.webRequestUser.checkFleetServiceAssociation(item.entityId)
                        .done((hasAssociated) => {
                            if(hasAssociated){
                                if(canTrack) {
                                    defaultItem = {id: CONTEXT_USER_VIEW_MAP, text: this.i18n("Common_ViewOnMap")(), item: item};
                                }else{
                                    defaultItem = {id: CONTEXT_USER_DETAIL, text: this.i18n("GlobalSearch_ViewDetail")(), item: item};
                                }
                            }else{
                                defaultItem = {id: CONTEXT_USER_DETAIL, text: this.i18n("GlobalSearch_ViewDetail")(), item: item};
                            }
                        });
                        break;
                    case Enums.ModelData.GlobalSearchLogisticsCategory.CustomPoi:
                        defaultItem = {id: CONTEXT_POI_VIEW_MAP, text: this.i18n("Common_ViewOnMap")(), item: item};
                        break;
                    case Enums.ModelData.GlobalSearchLogisticsCategory.CustomArea:
                        defaultItem = {id: CONTEXT_AREA_VIEW_MAP, text: this.i18n("Common_ViewOnMap")(), item: item};
                        break;
                    case Enums.ModelData.GlobalSearchLogisticsCategory.CustomRoute:
                        defaultItem = {id: CONTEXT_ROUTE_VIEW_MAP, text: this.i18n("Common_ViewOnMap")(), item: item};
                        break;
                }
                break;
            case Enums.ModelData.GlobalSearchType.Map:
                defaultItem = {id: CONTEXT_MAP_VIEWONMAP, text: this.i18n("Common_ViewOnMap")(), item: item};
                break;
            case Enums.ModelData.GlobalSearchType.AdminPoly:
                defaultItem = {id: CONTEXT_ADMINPOLY_VIEWONMAP, text: this.i18n("Common_ViewOnMap")(), item: item};
                break;
            case Enums.ModelData.GlobalSearchType.Street:
                defaultItem = {id: CONTEXT_MAP_VIEWSTREET_ONMAP, text: this.i18n("Common_ViewOnMap")(), item: item};
                break;
        }
        
        // Access context menu from singleton.
        $.when(dfdDefaultItemBuildComplete).done(()=> {
            this.onContextMenuExecute(defaultItem);
        });
       
        
    }
}

export default {
    viewModel: ScreenBase.createFactory(GlobalSearchResultScreen),
    template: templateMarkup
};
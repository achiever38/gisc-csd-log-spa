import ko from "knockout";
import {Enums} from "../../../../app/frameworks/constant/apiConstant";

/**
 * Default class for display category icon.
 * 
 * @export
 * @class FindCategory
 */
export default class FindCategory {
    constructor(id, text, icon)
    {
        this.id = id;
        this.text = text;
        this.icon = icon;
        this.selected = ko.observable(false);
        this.cssClass = ko.pureComputed(() => {
            return this.selected() ? "selected" : "";
        });
    }

    /**
     * Map category icon.
     * 
     * @param {any} searchType 
     * @param {any} searchCategory
     * @return {string}
     */
    static getIconName(searchType, searchCategory) {
        var iconName = "svg-cat-etc";

        switch(searchType) {
            case Enums.ModelData.GlobalSearchType.Logistics:
                switch(searchCategory) {
                    case "1":
                        iconName = "svg-cat-logis-bussinessunit";
                        break;
                    case "2":
                        iconName = "svg-cat-logis-vehicle";
                        break;
                    case "3":
                        iconName = "svg-cat-logis-custompoi";
                        break;
                    case "4":
                        iconName = "svg-cat-logis-customarea";
                        break;
                    case "5":
                        iconName = "svg-cat-logis-route";
                        break;
                    case "6":
                        iconName = "svg-cat-logis-driver";
                        break;
                    case "7":
                        iconName = "svg-cat-logis-user";
                        break;
                }
                break;
            case Enums.ModelData.GlobalSearchType.Map:
                switch(searchCategory) {
                    case "ACCOMMODATION":
                        iconName = "svg-cat-map-accommodation";
                        break;
                    case "ASSOCIATION":
                        iconName = "svg-cat-map-association";
                        break;
                    case "ATM":
                        iconName = "svg-cat-map-atm";
                        break;
                    case "ATTRACTION":
                        iconName = "svg-cat-map-attraction";
                        break;
                    case "AUTOMO":
                        iconName = "svg-cat-map-autoservice";
                        break;
                    case "BANK":
                        iconName = "svg-cat-map-bank";
                        break;
                    case "BUSINESS":
                        iconName = "svg-cat-map-bussiness";
                        break;
                    case "CARCARE":
                        iconName = "svg-cat-map-carcare";
                        break;
                    case "CARPARK":
                        iconName = "svg-cat-map-carpark";
                        break;
                    case "CINEMA":
                        iconName = "svg-cat-map-movie";
                        break;
                    case "EDUCATION":
                        iconName = "svg-cat-map-education";
                        break;
                    case "FOOD-BAKECOF":
                        iconName = "svg-cat-map-bakerycoffee";
                        break;
                    case "FOOD-FASTFOOD":
                        iconName = "svg-cat-map-fastfood";
                        break;
                    case "FOOD-FOODSHOP":
                        iconName = "svg-cat-map-foodshop";
                        break;
                    case "FOOD-PUB":
                        iconName = "svg-cat-map-publounge";
                        break;
                    case "FOOD-RESRAURANT":
                        iconName = "svg-cat-map-restaurant";
                        break;
                    case "FUEL-EV":
                        iconName = "svg-cat-map-vehiclecharging";
                        break;
                    case "FUEL-LPG":
                        iconName = "svg-cat-map-lpg";
                        break;
                    case "FUEL-NGV":
                        iconName = "svg-cat-map-ngv";
                        break;
                    case "FUEL-OIL":
                        iconName = "svg-cat-map-oilstation";
                        break;
                    case "GOVERNMENT":
                        iconName = "svg-cat-map-goverment";
                        break;
                    case "HEALTHCARE":
                        iconName = "svg-cat-map-health";
                        break;
                    case "HOSPITAL":
                        iconName = "svg-cat-map-hospital";
                        break;
                    case "HOTEL":
                        iconName = "svg-cat-map-hostel";
                        break;
                    case "MASSTRANSIT":
                        iconName = "svg-cat-map-masstransit";
                        break;
                    case "OTHER":
                        iconName = "svg-cat-map-other";
                        break;
                    case "PHARMACY":
                        iconName = "svg-cat-map-pharmacy";
                        break;
                    case "POLICE":
                        iconName = "svg-cat-map-police";
                        break;
                    case "POSTOFFICE":
                        iconName = "svg-cat-map-postoffice";
                        break;
                    case "RECREATION":
                        iconName = "svg-cat-map-recreations";
                        break;
                    case "RELIGION":
                        iconName = "svg-cat-map-religion";
                        break;
                    case "SHOPPING-CONVSTORE":
                        iconName = "svg-cat-map-conveniencestore";
                        break;
                    case "SHOPPING-DEPTSTORE":
                        iconName = "svg-cat-map-departmentstore";
                        break;
                    case "SHOPPING-MARKET":
                        iconName = "svg-cat-map-market";
                        break;
                    case "SHOPPING-STORE":
                        iconName = "svg-cat-map-stores";
                        break;
                    case "TRANSPORTATION":
                        iconName = "svg-cat-map-transport";
                        break;
                }
                break;
            case Enums.ModelData.GlobalSearchType.AdminPoly:
                iconName = "svg-cat-map-adminpoly";
                break;
            case Enums.ModelData.GlobalSearchType.Street:
                iconName = "svg-cat-map-road";
                break;
        }

        return iconName;
    }
}
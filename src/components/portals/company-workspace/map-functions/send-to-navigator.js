import ko from "knockout";
import templateMarkup from "text!./send-to-navigator.html";
import ScreenBase from "../../screenbase";
import ScreenHelper from "../../screenhelper";
import Utility from "../../../../app/frameworks/core/utility";
import WebRequestBusinessUnit from "../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestVehicle from "../../../../app/frameworks/data/apitrackingcore/webRequestVehicle";
import WebRequestFleetMonitoring from "../../../../app/frameworks/data/apitrackingcore/webRequestFleetMonitoring";
import { BusinessUnitFilter } from "../businessUnitFilter";
import AssetInfo from "../asset-info";

/**
 * Handle navigation messaging. 
 * @class MapFunctionsSendToNavigatorScreen
 * @extends {ScreenBase}
 */
class MapFunctionsSendToNavigatorScreen extends ScreenBase {
    /**
     * Creates an instance of MapFunctionsSendToNavigatorScreen.
     * 
     * @param {any} params
     * 
     * @memberOf MapFunctionsSendToNavigatorScreen
     */
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Map_SendToNaviagator")());

        this.message = ko.observable();
        this.latitude = this.ensureObservable(Utility.trimDecimal(params.lat, 6));
        this.longitude = this.ensureObservable(Utility.trimDecimal(params.lon, 6));
        this.vehicle = ko.observable();
        this.vehicleOptions = ko.observableArray();
        this.businessUnit = ko.observable(null);
        this.businessUnitOptions = ko.observableArray([]);
        this.noneAccessibleBU = ko.observableArray([]);
        this.businessUnitFilter = new BusinessUnitFilter();
        this.includeSubBusinessUnitEnable = ko.pureComputed(() => {return !_.isEmpty(this.businessUnit());});
        this.includeSubBusinessUnit = ko.observable(false);
        this.isClearOldCommand = ko.observable(false);

        // Subscribtion.
        this._changeBusinessUnitSubscribe = this.businessUnit.subscribe((newValue) => { this.updateVehicles(newValue, this.includeSubBusinessUnit()); });
        this._changeIncludeSubBusinessUnit = this.includeSubBusinessUnit.subscribe((newValue) => { this.updateVehicles(this.businessUnit(), newValue); });
    }

    /**
     * Setup detect change and validators.
     */
    setupExtend() {
        //Track Change
        this.message.extend({
            trackChange: true
        });

        this.businessUnit.extend({
            trackChange: true
        });

        this.vehicle.extend({
            trackChange: true
        });

         //Validation
        this.message.extend({
            required: true,
            serverValidate: {
                message: this.i18n("M001")()
            }
        });

        this.businessUnit.extend({
            required: true,
            serverValidate: {
                message: this.i18n("M001")()
            }
        });

        this.vehicle.extend({
            required: true,
            serverValidate: {
                message: this.i18n("M001")()
            }
        });

        this.validationModel = ko.validatedObservable({
            messages: this.message,
            businessUnit: this.businessUnit,
            vehicle: this.vehicle
        });
    }

    /**
     * WebRequest Business Unit instance.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
     * WebRequest Vehicle instance. 
     * @readonly
     */
    get webRequestVehicle() {
        return WebRequestVehicle.getInstance();
    }

    /**
     * FleetMonitoring instance.
     * @readonly
     */
    get sendMessage() {
        return WebRequestFleetMonitoring.getInstance();
    }

    /**
     * Prepare model before submiting.
     * @returns
     */
    generateModel() {
        var model = {
            message: this.message(),
            latitude: this.latitude(),
            longitude: this.longitude(),
            vehicleId: this.vehicle().id,
            businessUnitId: this.vehicle().businessUnitId,
            clearCommand: this.isClearOldCommand()
        };

        return model;
    }

    /**
     * Handle when screen is loading.
     * @param {any} isFirstLoad
     * @returns
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();
        var d0 = this.webRequestBusinessUnit.listBusinessUnitSummary(this.businessUnitFilter);

        $.when(d0).done((r0) => {
            var bu = r0["items"];
            this.businessUnitOptions(bu);
            this.noneAccessibleBU(AssetInfo.getNoneAccessibleBU(bu));
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Handle when user has changed business unit.
     */
    updateVehicles (businessUnitId, includeSubBusinessUnit) {
        // Default selection is single.
        var selectedBusinessUnitIds = [];
        var selectedBusinessUnitId = parseInt(businessUnitId);
        if(!isNaN(selectedBusinessUnitId)){
            // If user included sub children then return all business unit ids.
            if(includeSubBusinessUnit){
                selectedBusinessUnitIds = ScreenHelper.findBusinessUnitsById(this.businessUnitOptions(), selectedBusinessUnitId);
            }
            else {
                // Single selection for business unit.
                selectedBusinessUnitIds = [selectedBusinessUnitId];
            }
        }

        if (selectedBusinessUnitIds.length) {
            this.isBusy(true);
            var vehicleFilter = {
                companyId: this.businessUnitFilter.companyId,
                businessUnitIds: selectedBusinessUnitIds,
                enable:true,
                IsIncludeGarminFmi: true,
                IsAssociatedWithFleetService: true,
                IsActiveFleetService: true
            }
            this.webRequestVehicle.listVehicleSummary(vehicleFilter)
            .done((r) => {
                var vehicle = this.getVehicleDatasource(r["items"]);
                this.vehicleOptions.replaceAll(vehicle);
            })
            .fail((e) => {
                this.handleError(e);
            })
            .always(() => {
                this.isBusy(false);
            });
        }
        else {
            this.vehicleOptions.replaceAll([]);
        }
    }

    /**
     * Build blade action bar. 
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSend", this.i18n("Common_Send")()));
        actions.push(this.createActionCancel());
    }

     /**
      * Handle when blade action click.
      * @param {any} sender
      * @returns
      */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSend") {

            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);
            var model = this.generateModel();

            this.sendMessage.sendMessageToNavigator(model, true).done((response) => {
                this.isBusy(false);
                this.close(true);
            }).fail((e) => {
                this.isBusy(false);

                this.handleError(e);
            });
        }
    }

    /**
     * Format vehicle string.
     * @param {any} items
     * @returns
     */
    getVehicleDatasource(items) {
        Utility.applyFormattedPropertyToCollection(items, "vehicleModelDisplayText", "{0} - {1} ( {2} )", "brand", "model","license");
        return items;
    }

    /**
     * Handle when screen is unload.
     */
    onUnload() {}
}

export default {
    viewModel: ScreenBase.createFactory(MapFunctionsSendToNavigatorScreen),
    template: templateMarkup
};
﻿import ko from "knockout";
import ScreenBaseWidget from "../../../screenbasewidget";
import templateMarkup from "text!./dialog-sentmail.html";
import WebRequestReportExports from "../../../../../app/frameworks/data/apitrackingreport/webRequestReportExports";

class SentMailDialogWidget extends ScreenBaseWidget {
    constructor(params) {
        super(params);

        this.params = this.ensureNonObservable(params);
        this.mailTo = this.params ? this.params.mailTo : "";

        
    }


    get webRequestReportExports() {
        return WebRequestReportExports.getInstance();
    }

    /**
    * @lifecycle Called when View is loaded.
    * @param {boolean} isFirstLoad true if ViewModel is first load
    */
    onLoad(isFirstLoad) {
        
        if (!isFirstLoad) {
            return;
        }
        
    }

    onClickOK() {

        if (this.params) {
            this.params.entityType = this.params.EntityTypeID;
            this.params.SelectedEntity = this.params.SelectedEntityID;
            this.params.FromDate = this.params.SDate;
            this.params.ToDate = this.params.EDate;
            this.params.Email = this.params.mailTo;
            this.params.IsGroup = this.params.IsGroup ? true : false;

            this.webRequestReportExports.sendEmailTraclocReport(this.params).done(() => {
                $('#cw-report-general-sentmail-dialog').data("kendoWindow").close();
                this.publishMessage("cw-report-general-sentmail-dialog-success");

            }).fail((e) => {
                alert(e.error().responseJSON.message);
                this.handleError(e);
            });
        }
    }

    /**
     * Close modal dialog
     */
    onClickCancel() {
        var confirmDialog = $('#cw-report-general-sentmail-dialog').data("kendoWindow");
        confirmDialog.close();
    }
}

export default {
    viewModel: ScreenBaseWidget.createFactory(SentMailDialogWidget),
    template: templateMarkup
};
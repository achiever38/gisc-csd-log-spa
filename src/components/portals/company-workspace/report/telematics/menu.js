﻿import ko from "knockout";
import templateMarkup from "text!./menu.html";
import ScreenBase from "../../../screenbase";
import * as BladeMargin from "../../../../../app/frameworks/constant/bladeMargin";

/**
 * Display available subscription menu based on user permission.
 * @class MenuScreen
 * @extends {ScreenBase}
 */
class MenuScreen extends ScreenBase {
    constructor(params){
        super(params);
        
        this.bladeTitle(this.i18n("Report_Telematics")());
        this.bladeCanMaximize = false;
        this.bladeMargin = BladeMargin.Narrow;
        this.selectedIndex = ko.observable();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedIndex(null);
    }
    /**
     * Navigate to inner screens.
     * @param {any} extras
     */
    goto (extras){
        // Passing report names to next page.
        //  this.navigate("bo-report-and-group-ovserspeed", {reportSource: extras});
        this.navigate(extras.id,extras.name);
    }
}

export default {
    viewModel: ScreenBase.createFactory(MenuScreen),
    template: templateMarkup
};
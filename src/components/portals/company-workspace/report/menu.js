﻿import ko from "knockout";
import templateMarkup from "text!./menu.html";
import ScreenBase from "../../screenbase";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import { Enums, Constants } from "../../../../app/frameworks/constant/apiConstant";

/**
 * Display available subscription menu based on user permission.
 * @class MenuScreen
 * @extends {ScreenBase}
 */
class MenuScreen extends ScreenBase {
    constructor(params){
        super(params);
        
        this.bladeTitle(this.i18n("Menu_Reports")());
        this.bladeCanMaximize = false;
        this.bladeMargin = BladeMargin.Narrow;
        this.selectedIndex = ko.observable();
        
        this._selectingRowHandler = (item) => {
            if (item) {
                return this.navigate(item.id);
            }
            return false;
        };

        this.items = ko.observableArray([]);

    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        
        if(WebConfig.userSession.hasPermission(Constants.Permission.GeneralReport)){
            this.items.push({
                text: this.i18n("Report_General")(),
                id: 'cw-report-general'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.ComparisonReport)){
            this.items.push({
                text: this.i18n("Report_Comparison")(),
                id: 'cw-report-comparison'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport)){
            this.items.push({
                text: this.i18n("Report_Summary")(),
                id: 'cw-report-summary'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.DelinquentReport)){
            this.items.push({
                text: this.i18n("Report_Delinquent")(),
                id: 'cw-report-delinquent'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.AdvanceReport)){
            this.items.push({
                text: this.i18n("Report_AdvanceReport")(),
                id: 'cw-report-advance'
            });
        }
        if(WebConfig.userSession.hasPermission(Constants.Permission.FleetTypeLogReport)){
            this.items.push({
                text: this.i18n("Report_FleetTypeLogReport")(),
                id: 'cw-report-fleettype'
            });
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.KPIReport)) {
            this.items.push({
                text: this.i18n("Report_KpiReport")(),
                id: 'cw-report-kpi-search'
            });
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.AdvanceDriverPerformanceReport)) {
            this.items.push({
                text: this.i18n("Report_AdvanceDriverPerformanceReport")(),
                id: 'cw-report-advance-driverperformance'
            });
        }
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedIndex(null);
    }
    /**
     * Navigate to inner screens.
     * @param {any} extras
     */
    goto (extras){
        // Passing report names to next page.
        //  this.navigate("bo-report-and-group-ovserspeed", {reportSource: extras});
        this.navigate(extras.id,extras.name);
    }
}

export default {
    viewModel: ScreenBase.createFactory(MenuScreen),
    template: templateMarkup
};

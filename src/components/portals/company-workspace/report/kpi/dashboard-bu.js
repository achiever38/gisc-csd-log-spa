﻿import ko from "knockout";
import templateMarkup from "text!./dashboard-bu.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Constants, Enums } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestTruckDriverUtilization from "../../../../../app/frameworks/data/apitrackingcore/webRequestTruckDriverUtilization";

/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class KpiDashboardBu extends ScreenBase {
    /**
     * Creates an instance of DashboardOverall.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        // console.log("MyParamsIndashboardBu>>",params)
        //params = {"businessUnits":["0","91","1"]}
        this.height = $('.app-portal-content')["0"].clientHeight - 150;
        $('.dashboard-bu').css("height", this.height + "px");

        this.bladeTitle(this.i18n("Report_KpiReport")());
        this.bladeSize = BladeSize.XLarge;
        this.bladeIsMaximized = true;

        this.horizontalChartData = ko.observableArray([]);
        // this.nameChart = ko.observable(this.i18n("")());

        this.companyName = ko.observable(params.companyName);
        this.dateFromForDisplay = ko.observable(params.formatStartDateForDisplay);
        this.dateToForDisplay = ko.observable(params.formatEndDateForDisplay);

        this.dateFrom = ko.observable(params.filter.dateFrom);
        this.dateTo = ko.observable(params.filter.dateTo);

        // this.fleetType = ko.observable('DFM');
        // this.kpiType = ko.observable('Over Speed');

        this.kpiName = ko.observable('');
        this.fleetTypeName = ko.observable('');

        this.filter = ko.observable({
            "businessUnitIds": params.filter.businessUnitIds,
            "dateFrom": params.filter.dateFrom,
            "dateTo": params.filter.dateTo,
            "graphId": params.graphId 
        });

        this.maximumDisplayHorizontalChart = ko.observable(14);
        this.chartLegend = ko.observableArray([{
            "title": ">= 80%",
            "color": "#46bc78"
        }, {
            "title": ">= 70%",
            "color": "#f48e06"
        }, {
            "title": "< 70%",
            "color": "#ee4036"
        }])
        this.isShowChartScrollbar = ko.computed(() => {
            return this.horizontalChartData().length > this.maximumDisplayHorizontalChart();   //return true or false
        });
        this.fixedColumnWidth = ko.computed(() => {
            return this.horizontalChartData().length > 5 ? null : 60;
        });
    }

    get webRequestTruckDriverUtilization() {
        return WebRequestTruckDriverUtilization.getInstance();
    }


    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // TODO: Verify access right to specific items
        if (!isFirstLoad) {
            return;
        }
        this.refreshData();
    }
    
    refreshData() {
       
        let dfd = $.Deferred();
        let dfdGraph = this.webRequestTruckDriverUtilization.kpiReportBarchartSummaryList(this.filter());
        $.when(dfdGraph).done((response) => {
            


            _.forEach(response.items[0].barChartInfos, (items)=>{
                items.score = items.value;
            }); //add feild score for show in balloon text

            this.kpiName(response.items[0].kpiReportType)
            this.fleetTypeName(response.items[0].fleetTypeName);


            // console.log("response.items[0].barChartInfos>>",response.items[0].barChartInfos)
            response.items[0].barChartInfos.sort((param1, param2) => parseInt(param2.percent) - parseInt(param1.percent)); //sort data
            this.horizontalChartData.replaceAll(response.items[0].barChartInfos);

            dfd.resolve();

        }).fail(e => {
            this.handleError(e);
        }).always(() => {
                this.isBusy(false);
            });

        return dfd;



    }

    horizontalChartCallback(params) {
        let graphAllIds = params.item.dataContext.id
        let graphAllIdsSplit = graphAllIds.split("_");

        let graphId = graphAllIds;
        let buId = graphAllIdsSplit[0];
        let fleetTypeId = graphAllIdsSplit[1];
        let kpiTypeId = graphAllIdsSplit[2];
        let dateFrom = this.dateFrom();
        let dateTo = this.dateTo();
        let reportTitle = this.kpiName();

        let reportName;
        if(kpiTypeId == "A"  )
        {
            if(fleetTypeId == 4)
            {
                reportName = "Gisc.Csd.Service.Report.Library.KPI.ActivatingKPIReport,Gisc.Csd.Service.Report.Library";
            }else{
                reportName = "Gisc.Csd.Service.Report.Library.KPI.ActivatingKPIReportNonDFM,Gisc.Csd.Service.Report.Library";
            }
        }else{
            reportName = "Gisc.Csd.Service.Report.Library.KPI.DelinquentKPIReport,Gisc.Csd.Service.Report.Library";
        }
        
        this.reports = {
            report: reportName,
            parameters: {
               CompanyID:WebConfig.userSession.currentCompanyId,
                UserID: WebConfig.userSession.id,
                Language: WebConfig.userSession.currentUserLanguage,
                PrintBy:WebConfig.userSession.fullname,
                GraphId: graphId,
                FleetType:fleetTypeId,
                BuIds: buId,
                DateFrom: dateFrom,
                DateTo: dateTo
            }
        };
        // console.log("myReport>>>",this.reports)

        // this.navigate("cw-report-reportviewer", {});
        this.navigate("cw-report-reportviewer", {reportSource: this.reports, reportName:this.i18n(reportTitle)()});

    }

    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdRefresh", this.i18n("Common_Refresh")(), "svg-cmd-refresh"));
    }

    onCommandClick(sender) {

        switch (sender.id) {
            case "cmdRefresh":
                this.refreshData();
                break;
        }
    }



    setFormatNumber(num) {
        //return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }

    
}


export default {
    viewModel: ScreenBase.createFactory(KpiDashboardBu),
    template: templateMarkup
};


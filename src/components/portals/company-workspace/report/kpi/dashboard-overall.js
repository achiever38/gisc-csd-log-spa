﻿import ko from "knockout";
import templateMarkup from "text!./dashboard-overall.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Constants, Enums } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestReports from "../../../../../app/frameworks/data/apitrackingreport/WebRequestReports";
import WebRequestTruckDriverUtilization from "../../../../../app/frameworks/data/apitrackingcore/webRequestTruckDriverUtilization";
/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class KpiDashboardOverall extends ScreenBase {
    /**
     * Creates an instance of DashboardOverall.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.height = $('.app-portal-content')["0"].clientHeight - 145;
        $('.dashboard-overall').css("height", this.height + "px");

        //params = {"businessUnits":["0","91","1"]}
        // console.log("height>>",this.height)
        this.bladeTitle(this.i18n("Report_KpiReport")());
        this.bladeSize = BladeSize.XLarge;
        this.bladeIsMaximized = true;
        this.activatingDrivingParentWord = ko.observable(this.i18n("Report_KpiReport_Hr")())
        // this.activatingDrivingParentWord = ko.observable(`(${this.HrWord})`)

        this.companyName = ko.observable(params.companyName);
        this.dateFrom = ko.observable(params.formatStartDateForDisplay);
        this.dateTo = ko.observable(params.formatEndDateForDisplay);

        // this.fleetType = ko.observable('test');
        // this.kpiType = ko.observable('test');
        this.donutChartList = ko.observableArray([]);
        this.formatBalloonText = ko.observable('[[name]]: [[percents]]% ([[value]])');
        this.animation = ko.observable();
        this.dataNotFoundTxt = ko.observable('');
        this.dataNotFoundMarginTop = ko.observable('');
        this.isHideLegend = ko.observable('');


        this.filter = ko.observable({
            "businessUnitIds": params.businessUnitIds,
            "dateFrom": params.startDate,
            "dateTo": params.endDate,
            "fleetType": params.shippingTypeId
        });

        console.log(this.filter())
        console.log("myParams>>", params);

    }


    onClickPieChart(params) {
        console.log("myParams>>", params);

        let filter = {
            graphId: params.dataItem.dataContext.id,
            filter: this.filter(),
            formatStartDateForDisplay: this.dateFrom(),
            formatEndDateForDisplay: this.dateTo(),
            companyName: this.companyName()
        }

        this.navigate("cw-report-kpi-dashboard-bu", filter);
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // TODO: Verify access right to specific items
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();
        var self = this;
        this.donutChartList.replaceAll([])

        let dfdGraph = this.webRequestTruckDriverUtilization.kpiReportSummaryList(this.filter());
        $.when(dfdGraph).done((response) => {
            console.log("response>>>", response)

            let property = [{
                // labelsEnabled: false,
                autoMargins: false,
                marginTop: 10,
                marginBottom: 0,
                marginLeft: 0,
                marginRight: 10,
                pullOutRadius: 0
            }]

            if (response.items.length != 0) {
                this.dataNotFoundMarginTop('0px');
                this.isHideLegend('visible');
                response.items.forEach((item) => {
                    console.log("items.fleetType>>", item)
                    let parentWord = "";
                    let formatParentWord;
                    if (item.fleetTypeId == 4) {
                        parentWord = this.i18n("Report_KpiReport_Drivers")();
                    } else if (item.fleetTypeId == 5) {
                        parentWord = this.i18n("Report_KpiReport_Shipments")();
                    }

                    // formatParentWord = "(" + parentWord + ")";
                    // formatParentWord = `(${parentWord})`;
                    // console.log("formatParentWord>>", formatParentWord)
                    this.donutChartList.push({
                        overspeed: item.overSpeed.graphInfos,
                        hahb: item.hahb.graphInfos,
                        swerve: item.swerve.graphInfos,
                        over4hr: item.overtime4Hr.graphInfos,
                        over10hr: item.overtime10Hr.graphInfos,
                        activatingdrive: item.activatingDriving.graphInfos,
                        rest10hr: [], //mock เป็น Array ว่าง
                        // rest10hr: item.rest10hr.graphInfos, //ไม่รู้ชื่อฟิลของ rest10hr
                        

                        overSpeedPercentage: `${item.overSpeed.percentage}%`,
                        hahbPercentage: `${item.hahb.percentage}%`,
                        swervePercentage: `${item.swerve.percentage}%`,
                        overtime4HrPercentage: `${item.overtime4Hr.percentage}%`,
                        overtime10HrPercentage: `${item.overtime10Hr.percentage}%`,
                        activatingDrivingPercentage: `${item.activatingDriving.percentage}%`,
                        rest10hrPercentage: undefined,
                        // rest10hrPercentage: `${item.rest10hr.percentage}%`, //ไม่รู้ชื่อฟิลของ rest10hr
                       

                        // kpiname: item.overspeed.kpiname,
                        parentWord: parentWord,
                        title: item.fleetType,
                        formatBalloonText: this.formatBalloonText(),
                        animation: this.animation(false),
                        propertyGraph: property
                    });
                });
            } else {
                console.log("in ELSE")
                this.dataNotFoundMarginTop('30px');
                this.isHideLegend('hidden');
                this.dataNotFoundTxt(this.i18n('Common_DataNotFound')())
                // $("#" + self.id).html(this.i18n('Common_DataNotFound')())
            }

            dfd.resolve();

        }).fail(e => {
            this.handleError(e);
        }).always(() => {
            this.isBusy(false);
        });

        return dfd;

        // this.refreshData();
    }

    buildCommandBar(commands) {
        // commands.push(this.createCommand("cmdRefresh", this.i18n("Common_Refresh")(), "svg-cmd-refresh"));
    }

    onCommandClick(sender) {
        // if (sender.id == "cmdRefresh") {
        //     this.navigate("cw-report-kpi-dashboard-bu", {});
        // }
    }

    refreshData() {

        // var dfd = $.Deferred();
        // let jsontodsob = [{
        //     name: "DFM",
        //     totleover4hrdfm: 90,
        //     totleover4hrnondfm: 90,
        //     totleover10hrdfm: 90,
        //     totleover10hrnondfm: 90,
        //     totleaotivatingdfm: 90,
        //     totleaotivatingnondfm: 90,
        //     totlerest10hrdfm: 90,
        //     totlerest10hrnondfm: 90,

        //     overspeed: {
        //         kpiname: "Over Speed(Times)",
        //         info: [{
        //             color: "#59D559",
        //             kpiName: "Over Speed(Times)",
        //             fleetType: "DFM",
        //             // statusId: 2,
        //             value: "5",
        //             text: "pass"
        //         }, {
        //             color: "#FF0000",
        //             kpiName: "Over Speed(Times)",
        //             fleetType: "DFM",
        //             // statusId: 2,
        //             value: "14",
        //             text: "fail"
        //         }]
        //     },
        //     hahb: [{
        //         color: "#59D559",
        //         // statusId: 2,
        //         value: "21",
        //         text: "pass"
        //     }, {
        //         color: "#FF0000",
        //         // statusId: 2,
        //         value: "10",
        //         text: "fail"
        //     }],
        //     swerve: [{
        //         color: "#59D559",
        //         // statusId: 2,
        //         value: "1",
        //         text: "pass"
        //     }, {
        //         color: "#FF0000",
        //         // statusId: 2,
        //         value: "1",
        //         text: "fail"
        //     }],
        //     over4hr: [{
        //         color: "#59D559",
        //         // statusId: 2,
        //         value: "4",
        //         text: "pass"
        //     }, {
        //         color: "#FF0000",
        //         // statusId: 2,
        //         value: "1",
        //         text: "fail"
        //     }],
        //     over10hr: [{
        //         color: "#59D559",
        //         // statusId: 2,
        //         value: "9",
        //         text: "pass"
        //     }, {
        //         color: "#FF0000",
        //         // statusId: 2,
        //         value: "11",
        //         text: "fail"
        //     }],
        //     activatingdrive: [{
        //         color: "#59D559",
        //         // statusId: 2,
        //         value: "2",
        //         text: "pass"
        //     }, {
        //         color: "#FF0000",
        //         // statusId: 2,
        //         value: "6",
        //         text: "fail"
        //     }],
        //     rest10hr: [{
        //         color: "#59D559",
        //         // statusId: 2,
        //         value: "1",
        //         text: "pass"
        //     }, {
        //         color: "#FF0000",
        //         // statusId: 2,
        //         value: "1",
        //         text: "fail"
        //     }]
        // }, {
        //     name: "Non DFM",
        //     totleover4hrdfm: 90,
        //     totleover4hrnondfm: 90,
        //     totleover10hrdfm: 90,
        //     totleover10hrnondfm: 90,
        //     totleaotivatingdfm: 90,
        //     totleaotivatingnondfm: 90,
        //     totlerest10hrdfm: 90,
        //     totlerest10hrnondfm: 90,
        //     overspeed: {
        //         kpiname: "Over Speed(Times)",
        //         info: [{
        //             color: "#59D559",
        //             fleetType: "Non DFM",
        //             kpiName: "Over Speed(Times)",
        //             // statusId: 2,
        //             value: "5",
        //             text: "pass"
        //         }, {
        //             color: "#FF0000",
        //             fleetType: "Non DFM",
        //             kpiName: "Over Speed(Times)",
        //             // statusId: 2,
        //             value: "1",
        //             text: "fail"
        //         }]
        //     },
        //     hahb: [{
        //         color: "#59D559",
        //         // statusId: 2,
        //         value: "12",
        //         text: "pass"
        //     }, {
        //         color: "#FF0000",
        //         // statusId: 2,
        //         value: "13",
        //         text: "fail"
        //     }],
        //     swerve: [{
        //         color: "#59D559",
        //         // statusId: 2,
        //         value: "16",
        //         text: "pass"
        //     }, {
        //         color: "#FF0000",
        //         // statusId: 2,
        //         value: "11",
        //         text: "fail"
        //     }],
        //     over4hr: [{
        //         color: "#59D559",
        //         // statusId: 2,
        //         value: "11",
        //         text: "pass"
        //     }, {
        //         color: "#FF0000",
        //         // statusId: 2,
        //         value: "19",
        //         text: "fail"
        //     }],
        //     over10hr: [{
        //         color: "#59D559",
        //         // statusId: 2,
        //         value: "10",
        //         text: "pass"
        //     }, {
        //         color: "#FF0000",
        //         // statusId: 2,
        //         value: "1",
        //         text: "fail"
        //     }],
        //     activatingdrive: [{
        //         color: "#59D559",
        //         // statusId: 2,
        //         value: "17",
        //         text: "pass"
        //     }, {
        //         color: "#FF0000",
        //         // statusId: 2,
        //         value: "1",
        //         text: "fail"
        //     }],
        //     rest10hr: [{
        //         color: "#59D559",
        //         // statusId: 2,
        //         value: "14",
        //         text: "pass"
        //     }, {
        //         color: "#FF0000",
        //         // statusId: 2,
        //         value: "1",
        //         text: "fail"
        //     }],
        // }]
        // // property margin
        // let property = [{
        //     // labelsEnabled: false,
        //     autoMargins: false,
        //     marginTop: 10,
        //     marginBottom: 0,
        //     marginLeft: 0,
        //     marginRight: 10,
        //     pullOutRadius: 0
        // }]

        // jsontodsob.forEach((item) => {
        //     this.donutChartList.push({
        //         overspeed: item.overspeed.info,
        //         hahb: item.hahb,
        //         swerve: item.swerve,
        //         over4hr: item.over4hr,
        //         over10hr: item.over10hr,
        //         activatingdrive: item.activatingdrive,
        //         rest10hr: item.rest10hr,
        //         // rest10hrnondfm: item.rest10hrnondfm,

        //         overSpeedPercentage: `${item.totleover4hrdfm}%`,
        //         hahbPercentage: `${item.totleover4hrdfm}%`,
        //         swervePercentage: `${item.totleover4hrdfm}%`,
        //         overtime4HrPercentage: `${item.totleover4hrdfm}%`,
        //         overtime10HrPercentage: `${item.totleover4hrdfm}%`,
        //         activatingDrivingPercentage: `${item.totleover4hrdfm}%`,

        //         // totlerest10hrnondfm: `${item.totlerest10hrnondfm}%`,

        //         kpiname: item.overspeed.kpiname,
        //         parentWord: "(test)",
        //         title: item.name,
        //         formatBalloonText: this.formatBalloonText(),
        //         animation: this.animation(false),
        //         propertyGraph: property
        //     });
        // });

        // console.log("this.donutchartlist>>", this.donutChartList());

        // dfd.resolve();
        // // })

        // return dfd;
    }

    setFormatNumber(num) {
        //return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }


    get webRequestTruckDriverUtilization() {
        return WebRequestTruckDriverUtilization.getInstance();
    }

    // get webRequestReports() {
    //     return WebRequestReports.getInstance();
    // }

}


export default {
    viewModel: ScreenBase.createFactory(KpiDashboardOverall),
    template: templateMarkup
};



//--------------------- ห้ามลบนะครับ!!!!!!!! ------------------------------------------------------

//  ################ 7 Graph(HTML) ##########################
//  ################################################### 

//  <div style="width: 100%; height: 100%; float: left; margin-top: -30px;">
//         <!-- ko foreach: donutChartList -->
//             <div style="height: 30%; width: 100%; float: left; margin-top: 50px ">

//                 <div style="width: 100%">
//                     <b>
//                         <span data-bind="text:$parent.i18n('Report_ShippingType')"></span>
//                         <span>:</span>
//                     </b>
//                     <span style="font-size:25px; " data-bind="text:title"></span>
//                 </div>


//                 <div style="width: 14%; height: 100%; float: inherit;">
//                     <div style="width: 100%; text-align: center;">
//                         <span>Over Speed(Times)</span>
//                     </div>
//                     <gisc-ui-donutchart params="data: overspeed,
//                         innerRadius: 40,
//                         formatBalloonText: formatBalloonText,
//                         centerLabel: totleover4hrdfm, 
//                         paddingbottom:50,
//                         formatLabelText: '',
//                         onClickPieChart:$parent.onClickPieChart.bind($parent),
//                         propertyGraph:propertyGraph
//                         " />
//                 </div>
//                 <div style="width: 14%; height: 100%; float: inherit;">
//                     <div style="width: 100%; text-align: center;">
//                         <span>HAHB(Times)</span>
//                     </div>
//                     <gisc-ui-donutchart params="data: overspeed,
//                             innerRadius: 40,
//                             formatBalloonText: formatBalloonText,
//                             centerLabel: totleover4hrdfm, 
//                             paddingbottom:50,
//                             formatLabelText: '',
//                             propertyGraph:propertyGraph
//                             " />
//                 </div>
//                 <div style="width: 14%; height: 100%; float: inherit;">
//                     <div style="width: 100%; text-align: center;">
//                         <span>Swerve(Times)</span>
//                     </div>
//                     <gisc-ui-donutchart params="data: overspeed,
//                                 innerRadius: 40,
//                                 formatBalloonText: formatBalloonText,
//                                 centerLabel: totleover4hrdfm, 
//                                 paddingbottom:50,
//                                 formatLabelText: '',
//                                 propertyGraph:propertyGraph
//                                 " />
//                 </div>
//                 <div style="width: 14%; height: 100%; float: inherit;">
//                     <div style="width: 100%; text-align: center;">
//                         <span>Overtime 4 hr(Times)</span>
//                     </div>
//                     <gisc-ui-donutchart params="data: overspeed,
//                                     innerRadius: 40,
//                                     formatBalloonText: formatBalloonText,
//                                     centerLabel: totleover4hrdfm, 
//                                     paddingbottom:50,
//                                     formatLabelText: '',
//                                     propertyGraph:propertyGraph
//                                     " />
//                 </div>
//                 <div style="width: 14%; height: 100%; float: inherit;">
//                     <div style="width: 100%; text-align: center;">
//                         <span>Overtime 10 hr(Times)</span>
//                     </div>
//                     <gisc-ui-donutchart params="data: overspeed,
//                                         innerRadius: 40,
//                                         formatBalloonText: formatBalloonText,
//                                         centerLabel: totleover4hrdfm, 
//                                         paddingbottom:50,
//                                         formatLabelText: '',
//                                         propertyGraph:propertyGraph
//                                         " />
//                 </div>
//                 <div style="width: 14%; height: 100%; float: inherit;">
//                     <div style="width: 100%; text-align: center;">
//                         <span>Activating Driving(Times)</span>
//                     </div>
//                     <gisc-ui-donutchart params="data: overspeed,
//                                             innerRadius: 40,
//                                             formatBalloonText: formatBalloonText,
//                                             centerLabel: totleover4hrdfm, 
//                                             paddingbottom:50,
//                                             formatLabelText: '',
//                                             propertyGraph:propertyGraph
//                                             " />
//                 </div>
//                 <div style="width: 14%; height: 100%; float: inherit;">
//                     <div style="width: 100%; text-align: center;">
//                         <span>Rest 10 hr(Times)</span>
//                     </div>
//                     <gisc-ui-donutchart params="data: overspeed,
//                                                 innerRadius: 40,
//                                                 formatBalloonText: formatBalloonText,
//                                                 centerLabel: totleover4hrdfm, 
//                                                 paddingbottom:50,
//                                                 formatLabelText: '',
//                                                 propertyGraph:propertyGraph
//                                                 " />
//                 </div>
//             </div>

//         <!-- /ko -->
//     </div> 
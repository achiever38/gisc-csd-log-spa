import ko from "knockout";
import templateMarkup from "text!./form.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import "jquery-ui";
import "jqueryui-timepicker-addon";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestCategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestCategory";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import WebRequestDriver from "../../../../../app/frameworks/data/apitrackingcore/webRequestDriver";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import { Enums, Constants } from "../../../../../app/frameworks/constant/apiConstant";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebRequestReportExports from "../../../../../app/frameworks/data/apitrackingreport/webRequestReportExports";
/**
 * Demo for the following features:
 * 1. Validation on ViewModel
 * - Required Field
 * - Numeric Field
 * - Email
 * - Custom (RegEx)
 * - Remote Validation
 * 2. Track change on ViewModel
 * 
 * @class FormValidationDemo
 * @extends {ScreenBase}
 */
class FormOverspeed extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeSize = BladeSize.Small;
        this.bladeTitle(this.i18n('Report_Filter')());

        this.selectedBusinessUnit = ko.observable(null);
        this.isIncludeSubBU = ko.observable(false);
        this.selectedEntityType = ko.observable();
        this.categoryVehicle = ko.observable();
        this.selectedEntity = ko.observable();
        this.shippingType = ko.observable();

        this.selectedReportType = ko.observable();
        this.selectedDuration = ko.observable();

        this.businessUnits = ko.observableArray([]);
        this.entityTypes = ko.observableArray([]);
        this.selectEntity = ko.observableArray([]);
        this.categoryVehicleOptions = ko.observableArray([]);
        this.shippingTypeOptions = ko.observableArray([]);

        this.isIncludeFormerDriver = ko.observable(false);
        this.isIncludeResignedDriver = ko.observable(false);

        this.reportTypes = ko.observableArray([]);
        this.durations = ko.observableArray([]);

        //for keep data this.entityTypes onload
        this.tempEntityTypes = ko.observableArray([]);


        this.valueRadio = ko.observable('Today');
        this.isDateTimeEnabled = ko.observable(false);


        this.durationVisible = ko.pureComputed(() => {
            return this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.DelinquentReportType.DelinquentDriveOverTime;
        });

        this.sectionVisible = ko.pureComputed(() => {
            var isSectionVisible =
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.DelinquentReportType.DelinquentDriveOverTime)||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.DelinquentReportType.DelinquentAnonymousDriver)||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.DelinquentReportType.DelinquentRestBelow10Hours);
            return isSectionVisible;
        });

        this.entityTypeVisible = ko.pureComputed(() => {
            var isEntityTypeVisible =
                ((this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.DelinquentReportType.DelinquentDriveOverTime) ||
                    (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.DelinquentReportType.DelinquentAnonymousDriver));
            return isEntityTypeVisible;
        });

        this.driverVisible = ko.pureComputed(() => {
            return this.selectedEntityType() && this.selectedEntityType().value === Enums.ModelData.SearchEntityType.Driver;
        });

        this.categoryVehicleVisible = ko.pureComputed(() => {
            var isCategoryVisible = ((Object.keys(this.categoryVehicleOptions()).length > 1) ? true : false) &&
                ((this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.DelinquentReportType.DelinquentDriveOverTime) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.DelinquentReportType.DelinquentAnonymousDriver));
            return isCategoryVisible;
        });
        

        this.entityVisible = ko.pureComputed(() => {
            var isEntityVisible =
                ((this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.DelinquentReportType.DelinquentDriveOverTime) ||
                    (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.DelinquentReportType.DelinquentAnonymousDriver)||
                    (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.DelinquentReportType.DelinquentRestBelow10Hours));
            return isEntityVisible;
        });
        
        this.shippingTypeVisible = ko.pureComputed(() => {
            var isShippingTypeVisible = ((Object.keys(this.shippingTypeOptions()).length > 1) ? true : false) &&
                ((this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.DelinquentReportType.DelinquentDriveOverTime) ||
                 (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.DelinquentReportType.DelinquentAnonymousDriver));
            return false;//isShippingTypeVisible;
        });

        this.visibleRadio = ko.pureComputed(() => { // เช็คส่า ถ้าเป็น DelinquentRestBelow10Hours ไม่ต้อง แสดง Radio Today
            return !(this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.DelinquentReportType.DelinquentRestBelow10Hours);
        });

        //this.selectedEntityType().value === Enums.ModelData.SearchEntityType.Vehicle && businessUnitIds


        this.selectedEntityBinding = ko.pureComputed(() => {
            var businessUnitIds = this.selectedBusinessUnit();
            var result = this.i18n('Common_PleaseSelect')();
            var countItem = 0;
            this.selectEntity([]);
            //if check include Sub BU
            if (this.isIncludeSubBU() && businessUnitIds != null) {
                businessUnitIds = Utility.includeSubBU(this.businessUnits(), businessUnitIds);
            }

            let tmpItem = this.tempEntityTypes();
            if(this.selectedReportType().value == Enums.ModelData.DelinquentReportType.DelinquentRestBelow10Hours){
                tmpItem  = ko.utils.arrayFilter(this.tempEntityTypes(),(item) => {
                    return item.value === Enums.ModelData.SearchEntityType.Driver;
                });
                //this.tempEntityTypes(tmpItem);
                //this.selectedEntityType(tmpItem);
                //console.log(tmpItem);
            }
            else if(this.selectedReportType().value == Enums.ModelData.DelinquentReportType.DelinquentAnonymousDriver){
                tmpItem  = ko.utils.arrayFilter(this.tempEntityTypes(),(item) => {
                    return item.value === Enums.ModelData.SearchEntityType.Vehicle;
                });
            };
            this.entityTypes(tmpItem);
            if (this.selectedEntityType().value === Enums.ModelData.SearchEntityType.Vehicle && businessUnitIds != null) {
                this.webRequestVehicle.listVehicleSummary({
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: businessUnitIds,
                    sortingColumns: DefaultSorting.Vehicle,
                    vehicleCategoryId: this.categoryVehicle().id
                }).done((response) => {
                    //set displayName
                    response.items.forEach(function (arr) {
                        arr.displayName = arr.license;
                    });
                    let listSelectEntity = response.items;
                    //add displayName "Report_All" and id 0
                    if (Object.keys(listSelectEntity).length > 0 && listSelectEntity[0].id !== 0) {
                        listSelectEntity.unshift({
                            displayName: this.i18n('Report_All')(),
                            id: 0
                        });
                    }
                    this.selectEntity(listSelectEntity);
                    var defaultSelectEntity = ScreenHelper.findOptionByProperty(this.selectEntity, "displayName", this.i18n('Report_All')());
                    this.selectedEntity(defaultSelectEntity);
                });
            } else if (this.selectedEntityType().value === Enums.ModelData.SearchEntityType.Driver && businessUnitIds != null) {
                let includeFormerDriver = this.isIncludeFormerDriver() ? true : null;
                let includeResignedDriver = this.isIncludeResignedDriver() ? null : true;

                this.webRequestDriver.listDriverSummary({
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: businessUnitIds,
                    sortingColumns: DefaultSorting.Driver,
                    IncludeFormerInBusinessUnit : this.isIncludeFormerDriver(),
                    isEnable: this.isIncludeResignedDriver() ? null : true ,

                }).done((response) => {
                    //set displayName
                    response.items.forEach(function (arr) {
                        arr.displayName = arr.firstName + " " + arr.lastName;
                    });
                    let listSelectEntity = response.items;
                    //add displayName "Report_All" and id 0
                    if (Object.keys(listSelectEntity).length > 0 && listSelectEntity[0].id !== 0) {
                        listSelectEntity.unshift({
                            displayName: this.i18n('Report_All')(),
                            id: 0
                        });
                        countItem = 1;
                    }
                    this.selectEntity(listSelectEntity);
                    var defaultSelectEntity = ScreenHelper.findOptionByProperty(this.selectEntity, "displayName", this.i18n('Report_All')());
                    this.selectedEntity(defaultSelectEntity);
                });
            } else {
                this.selectEntity([]);
            }

            return result;
        });



        //datetime
        this.formatSet = "dd/MM/yyyy";
        var addDay = 30;
        var addMonth = 3;
        var setTimeNow = ("0" + new Date().getHours()).slice(-2)+":"+("0" + new Date().getMinutes()).slice(-2);
        this.start = ko.observable(new Date());
        this.end = ko.observable(new Date());
        this.timeStart = ko.observable("00:00");
        this.timeEnd = ko.observable("23:59");
        this.maxDateStart = ko.observable(new Date());
        this.minDateEnd = ko.observable(new Date());

        this.minDateShipment = ko.observable(Utility.minusMonths(new Date(),addMonth));
        this.maxDateShipment = ko.observable(Utility.addMonths(new Date(),addMonth));

        //for disable column
        this.renderItemColumn = (data, type, row) => {
            if(data) {
                return 'Yes';
            }
            return 'No';
        };

        this.radioCriteria = ko.computed(()=>{
            if(this.selectedReportType() && (this.selectedReportType().value === Enums.ModelData.DelinquentReportType.DelinquentRestBelow10Hours )){

                this.start(new Date());
                this.end(new Date());
                this.timeStart("00:00");
                this.timeEnd("23:59");
                this.valueRadio('Yesterday');
            }
           
            return true;

        })

        this.periodDay = ko.pureComputed((e)=>{
            // can't select date more than current date
            var isDate = Utility.addDays(this.start(),addDay);
            let currentDate = parseInt(new Date() / (1000 * 60 * 60 * 24));
            let endDate = parseInt(isDate / (1000 * 60 * 60 * 24)) + 1;
            var calDay = endDate - currentDate;
            if(calDay > 0){
                isDate = new Date();
            }
     
            // set date when clear start date and end date
            if(this.start() === null){
                // set end date when clear start date
                if(this.end() === null){
                    
                    isDate = new Date();
                   
                }else{
                 
                    isDate = this.end();
              
                }
            
                this.maxDateStart(isDate);
                this.minDateEnd(isDate);
          
            }else{
                
                this.minDateEnd(this.start())
         
            }
            let setDate = this.visibleRadio() ? 0 : 1;  //Set Date for RestLess10HrReport
            isDate.setDate(isDate.getDate() - setDate);  //Set Date for RestLess10HrReport
            this.maxDateStart(Utility.minusDays(new Date(),setDate));  //Set Date for RestLess10HrReport
            return isDate;
        });

        // Date Time Criteria
        this.valueRadio.subscribe((res) => {
            let currentDate = new Date();
            var startDate = new Date();
            var endDate = new Date();
            let setDate = this.visibleRadio() ? 0 : 1;  //Set Date for RestLess10HrReport
            switch (res) {
                case 'Today':
                    this.isDateTimeEnabled(false);
                    this.start(this.setFormatDateTime({
                        data: startDate,
                        start: true
                    }))
                    this.end(this.setFormatDateTime({
                        data: endDate,
                        end: true
                    }))
                    $("#from").val(this.setFormatDisplayDateTime(startDate));
                    $("#to").val(this.setFormatDisplayDateTime(endDate));
                    this.timeStart("00:00");
                    this.timeEnd("23:59");
                    break;

                case 'Yesterday':
                    
                    this.isDateTimeEnabled(false);
    
                    startDate.setDate(startDate.getDate() - 1)
                    // console.log("startDate.setDate(startDate.getDate() - 1)",startDate)
                    this.start(this.setFormatDateTime({
                        data: startDate,
                        start: true
                    }))
                    this.end(this.setFormatDateTime({
                        data: startDate,
                        end: true
                    }))
                    $("#from").val(this.setFormatDisplayDateTime(startDate));
                    $("#to").val(this.setFormatDisplayDateTime(startDate));

                    break;
            
                case 'ThisWeek':
                    this.isDateTimeEnabled(false);
                    // let startWeek = parseInt(startDate.getDate() - (startDate.getDay() - 1));
                    // let endWeek = parseInt(endDate.getDate() + (7 - endDate.getDay()));

                    let startWeek;
                    // let endWeek = parseInt(endDate.getDate() + (7 - endDate.getDay()));

                    //setStartDate
                    if(currentDate.getDay() == 0){ //ถ้าเป็นวันอาทิตย์ ( 0 คือวันอาทิตย์ )
              
                        startWeek = parseInt((startDate.getDate() - 6));

                    }else if(currentDate.getDay() == 1) { //วันจันทร์
                 
                        startWeek = parseInt((startDate.getDate() ));

                    }
                    else{
                    
                        startWeek = parseInt((startDate.getDate() - (startDate.getDay()-1)));
                    }
                  
                    startDate.setDate(startWeek)

                    //setEndDate
                    if(currentDate.getDay() == 1){ //ถ้าเป็นวันจันทร์ ( 1 คือวันจันทร์ )
                        endDate.setDate(currentDate.getDate());
                        this.maxDateStart(new Date(),0);
                    }
                    else{
                        endDate.setDate(currentDate.getDate() - setDate); //Set Date for RestLess10HrReport
                    }

                    // startDate.setDate(startWeek)
                    // endDate.setDate(endWeek)

                    this.start(this.setFormatDateTime({
                        data: startDate,
                        start: true
                    }))
                    this.end(this.setFormatDateTime({
                        data: endDate,
                        end: true
                    }))

                   

                    $("#from").val(this.setFormatDisplayDateTime(startDate));
                    $("#to").val(this.setFormatDisplayDateTime(endDate));
                    this.timeStart("00:00");
                    this.timeEnd("23:59");

                    break;

                case 'LastWeek':

                    this.isDateTimeEnabled(false);
                    let LastendWeek = parseInt((startDate.getDate() - (startDate.getDay() - 1)));
                    let LaststartWeek = parseInt(LastendWeek - 7);

                    startDate.setDate(LaststartWeek)
                    endDate.setDate(LastendWeek - 1)

                    this.start(this.setFormatDateTime({
                        data: startDate,
                        start: true
                    }))
                    this.end(this.setFormatDateTime({
                        data: endDate,
                        end: true
                    }))

                    $("#from").val(this.setFormatDisplayDateTime(startDate));
                    $("#to").val(this.setFormatDisplayDateTime(endDate));

                    // $("#searchStartDate").val(this.setFormatDisplayDateTime(startDate));
                    // $("#searchEndDate").val(this.setFormatDisplayDateTime(endDate));

                    break;
                
                case 'ThisMonth':
                    this.isDateTimeEnabled(false);
                    let firstDay = new Date(startDate.getFullYear(), startDate.getMonth(), 1);
                    let lastDay = new Date(endDate.getFullYear(), endDate.getMonth() + 1, 0);

                    endDate.setDate(currentDate.getDate() - setDate); //Set Date for RestLess10HrReport
                    this.start(this.setFormatDateTime({
                        data: firstDay,
                        start: true
                    }));
                    this.end(this.setFormatDateTime({
                        data: endDate,
                        end: true
                    }));
                    $("#from").val(this.setFormatDisplayDateTime(firstDay));
                    $("#to").val(this.setFormatDisplayDateTime(endDate));
                    this.timeStart("00:00");
                    this.timeEnd("23:59");
                    // $("#searchStartDate").val(this.setFormatDisplayDateTime(firstDay));
                    // $("#searchEndDate").val(this.setFormatDisplayDateTime(lastDay));
                    break;
                case 'LastMonth':
                    this.isDateTimeEnabled(false);
                    let LastfirstDay = new Date(startDate.getFullYear(), startDate.getMonth() - 1, 1);
                    let LastlastDay = new Date(endDate.getFullYear(), endDate.getMonth(), 0);

                    this.start(this.setFormatDateTime({
                        data: LastfirstDay,
                        start: true
                    }))
                    this.end(this.setFormatDateTime({
                        data: LastlastDay,
                        end: true
                    }))

                    $("#from").val(this.setFormatDisplayDateTime(LastfirstDay));
                    $("#to").val(this.setFormatDisplayDateTime(LastlastDay));

                    break;
                case 'Custom':
                    this.isDateTimeEnabled(true)
                    break;

                case '':
                    this.isRadioSelectedStartDate(false);
                    this.isRadioSelectedEndDate(false);
                    break;
            }
        });
    }

    setupExtend() {

        // Use ko-validation to set up validation rules
        // See url for more information about rules
        // https://github.com/Knockout-Contrib/Knockout-Validation

        this.selectedReportType.extend({ required: true });
        this.selectedBusinessUnit.extend({ required: true });
        //Entity Type
        this.selectedEntityType.extend({ required: true });
        this.selectedEntity.extend({ required: true });



        this.selectedDuration.extend({ required: true });
        this.start.extend({ required: true });
        this.end.extend({ required: true });
        // this.end = this.end.extend({
        //     date: true,
        //     min:{
        //         params:this.start,
        //         message:"Please select date greate than or equal to \"From Date\""
        //     }
        // });
        this.timeStart.extend({ required: true });
        this.timeEnd.extend({ required: true });
        // With custom message
        this.validationModel = ko.validatedObservable({
            selectedReportType: this.selectedReportType,
            end:this.end,
            start:this.start,
            timeStart:this.timeStart,
            timeEnd:this.timeEnd
        });


        this.validationBusinessUnit = ko.validatedObservable({
            selectedBusinessUnit: this.selectedBusinessUnit
        });

        this.validationSection = ko.validatedObservable({
            selectedEntityType: this.selectedEntityType,
            selectedEntity: this.selectedEntity,
        });

        this.validDeqRestLess10hr = ko.validatedObservable({
            selectedEntity: this.selectedEntity
        });

        this.validationDuration = ko.validatedObservable({
            selectedDuration: this.selectedDuration,
        });


    }

    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }

    get webRequestDriver() {
        return WebRequestDriver.getInstance();
    }

    get webRequestCategory() {
        return WebRequestCategory.getInstance();
    }

    get webRequestReportExports() {
        return WebRequestReportExports.getInstance();
    }

    getReportTemplateType() {
        var reportType = new Array();
        if (WebConfig.userSession.hasPermission(Constants.Permission.DelinquentReport_DriverOvertime)) {
            reportType.push(Enums.ModelData.DelinquentReportType.DelinquentDriveOverTime);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.DelinquentReport_AnonymousDriver)) {
            reportType.push(Enums.ModelData.DelinquentReportType.DelinquentAnonymousDriver);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.DelinquentReport_IncorrectDriverLicense)) {
            reportType.push(Enums.ModelData.DelinquentReportType.DelinquentIncorrectDriverLicenseType);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.DelinquentReport_ConnectionLost)) {
            reportType.push(Enums.ModelData.DelinquentReportType.DelinquentConnectionLost);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.DelinquentReport_RestLessThan10Hours)) {
            reportType.push(Enums.ModelData.DelinquentReportType.DelinquentRestBelow10Hours);
        }
        if(reportType.length < 1)
        {
            reportType = [0];
        }
        return reportType;
    }

    setFormatDateTime(dateObj) {

        var newDateTime = "";

        let myDate = new Date(dateObj.data)
        let year = myDate.getFullYear().toString();
        let month = (myDate.getMonth() + 1).toString();
        let day = myDate.getDate().toString();
        let hour = myDate.getHours().toString();
        let minute = myDate.getMinutes().toString();
        let sec = "00";

        if (dateObj.start) {
            hour = "00";
            minute = "00";
        } else if (dateObj.end) {
            hour = "23";
            minute = "59";
            sec = "59";
        } else { }

        month = month.length > 1 ? month : "0" + month;
        day = day.length > 1 ? day : "0" + day;
        hour = hour.length > 1 ? hour : "0" + hour;
        minute = minute.length > 1 ? minute : "0" + minute;
        newDateTime = year + "-" + month + "-" + day + "T" + hour + ":" + minute + ":" + sec;

       

        return newDateTime;
    }

    setFormatDisplayDateTime(objDate) {
        let displayDate = "";
        let d = objDate;
        let year = d.getFullYear().toString();
        let month = (d.getMonth() + 1).toString();
        let day = d.getDate().toString();
        day = day.length > 1 ? day : "0" + day;
        displayDate = day + "/" + month + "/" + year;

        return displayDate
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        
        var dfd = $.Deferred();

        var enumReportTemplateTypeFilter = {
            types: [Enums.ModelData.EnumResourceType.DelinquentReportType],
            values: this.getReportTemplateType(),
            sortingColumns: DefaultSorting.EnumResource
        };

        var enumDurationFilter = {
            types: [Enums.ModelData.EnumResourceType.ReportTemplateType],
            values: [
                        Enums.ModelData.ReportTemplateType.DelinquentDriveOver4Hours, 
                        Enums.ModelData.ReportTemplateType.DelinquentDriveOver8Hours
                    ],
            sortingColumns: DefaultSorting.EnumResource
        };

        var enumEntityTypeFilter = {
            types: [Enums.ModelData.EnumResourceType.SearchEntityType],
            values: [Enums.ModelData.SearchEntityType.Vehicle, Enums.ModelData.SearchEntityType.Driver],
            sortingColumns: DefaultSorting.EnumResource
        };

        var businessUnitFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            sortingColumns: DefaultSorting.BusinessUnit
        };

        var filterShippingType = {
            categoryId: Enums.ModelData.CategoryType.ShippingType,
        };

        var filterCategoryVehicle = {
            categoryId: Enums.ModelData.CategoryType.Vehicle,
        };

        var d1 = this.webRequestEnumResource.listEnumResource(enumReportTemplateTypeFilter);
        var d2 = this.webRequestEnumResource.listEnumResource(enumDurationFilter);
        var d3 = this.webRequestEnumResource.listEnumResource(enumEntityTypeFilter);
        var d4 = this.webRequestBusinessUnit.listBusinessUnitSummary(businessUnitFilter);
        var d5 = this.webRequestCategory.listCategory(filterShippingType);
        var d6 = this.webRequestCategory.listCategory(filterCategoryVehicle);

        //fake 4 and 8 hours
        var durationsHours = new Array();
        durationsHours.push(
            {
                displayName:"4",
                id:4525,
                infoStatus:1,
                name:"DelinquentDriveOver4Hours",
                nameResourceKey:null,
                order:25,
                resourceType:null,
                type:48,
                value:25
            },
            {
                displayName:"8",
                id:4526,
                infoStatus:1,
                name:"DelinquentDriveOver8Hours",
                nameResourceKey:null,
                order:26,
                resourceType:null,
                type:48,
                value:26
            },
            {
                displayName: "10",
                id: 4534,
                infoStatus: 1,
                name: "DelinquentDriveOver10Hours",
                nameResourceKey: null,
                order: 34,
                resourceType: null,
                type: 48,
                value: 34
            }
        );

        $.when(d1,d3,d4,d5,d6).done((r1,r3,r4,r5,r6) => {

            this.reportTypes(r1["items"]);
            this.durations(durationsHours);


            /*this.reportTypes.push({
                displayName:'RestLessThan10Hr',
                value:Enums.ModelData.DelinquentReportType.DelinquentRestBelow10Hours
            });*/ //Mock Up Parameter

            r5.items.unshift({
                name: this.i18n('Report_All')(),
                id: 0
            });

            r6.items.unshift({
                name: this.i18n('Report_All')(),
                id: 0
            });

            this.tempEntityTypes(r3.items);
            this.businessUnits(r4.items);
            this.shippingTypeOptions(r5.items);
            this.categoryVehicleOptions(r6.items);

            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });

        return dfd;

    }

    buildActionBar(actions){
        actions.push(this.createAction("actPreview", this.i18n('Report_Preview')()));
    }

    onActionClick(sender) {
        if(sender.id === "actPreview") {

            //for report namespace
            var reportNameSpace = null;
            //for validate selected
            var durationType = null;


            var BusinessUnitID = null;
            var IncludeSubBU = (this.isIncludeSubBU()) ? 1 : 0;
            var EntityTypeID = null;
            var SelectedEntityID = null;
            var shippingTypeId = (this.shippingType()) ? this.shippingType().id : 0;
            var categoryId = (this.categoryVehicle()) ? this.categoryVehicle().id : 0;


            var isDurationValid = true;
            var isSectionValid = true;
            var isEntityValid = true;

            //validate Duration
            if(this.durationVisible()){
                if(this.validationDuration.isValid()){
                    durationType = this.selectedDuration().value;
                }else{
                    this.validationDuration.errors.showAllMessages();
                    isDurationValid = false;
                }
            }

            if (this.sectionVisible()){
                if (this.validationBusinessUnit.isValid()) {
                    BusinessUnitID = this.selectedBusinessUnit();
                } else {
                    this.validationBusinessUnit.errors.showAllMessages();
                    isSectionValid = false;
                }
            }

            if (this.entityTypeVisible() && this.entityVisible()){
                if (this.validationSection.isValid()) {
                    EntityTypeID = this.selectedEntityType() && this.selectedEntityType().value;
                    SelectedEntityID = this.selectedEntity() && this.selectedEntity().id;
                } else {
                    this.validationSection.errors.showAllMessages();
                    isEntityValid = false;
                }
            }

            if(this.entityVisible()){
                if (this.validDeqRestLess10hr.isValid()) {
                    EntityTypeID = this.selectedEntityType() && this.selectedEntityType().value;
                    SelectedEntityID = this.selectedEntity() && this.selectedEntity().id;
                } else {
                    this.validDeqRestLess10hr.errors.showAllMessages();
                    isEntityValid = false;
                }
            }

            if (!isDurationValid || !isSectionValid || !isEntityValid) {
                return false;
            }


            

            var formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.start())) + " " + this.timeStart() + ":00";
            var formatEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.end())) + " " + this.timeEnd() + ":00";

            //validate Model
            if(this.validationModel.isValid()) {

                var reportTitle = "Report_Delinquent";
                //switch case for selected report type
                var reportExport = new Object();
                switch (this.selectedReportType() && this.selectedReportType().value) {
                    case Enums.ModelData.DelinquentReportType.DelinquentOverSpeed:
                        this.showMessageBox("ขออภัยในความไม่สะดวก", "รายงาน " + this.selectedReportType().name + " ปิดปรับปรุงระบบชั่วคราว", BladeDialog.DIALOG_OK);
                        return false;
                    case Enums.ModelData.DelinquentReportType.DelinquentAnonymousDriver:
                        reportNameSpace = "Gisc.Csd.Service.Report.Library.Delinquent.DelinquentAnonymousDriverReport, Gisc.Csd.Service.Report.Library";
                        //reportNameSpace = "Gisc.Csd.Service.Report.Library.Delinquent.DelinquentReport, Gisc.Csd.Service.Report.Library";
                        reportTitle = "DelinquentReport_DelinquentAnonymousDriver";
                        break;
                    case Enums.ModelData.DelinquentReportType.DelinquentDriveOverTime:
                        reportNameSpace = "Gisc.Csd.Service.Report.Library.Delinquent.DelinquentOverTimeReport, Gisc.Csd.Service.Report.Library";
                        //reportNameSpace = "Gisc.Csd.Service.Report.Library.Delinquent.DelinquentReport, Gisc.Csd.Service.Report.Library";
                        reportTitle = "DelinquentReport_DelinquentDriveOverTime";
                        break;
                    case Enums.ModelData.DelinquentReportType.DelinquentIncorrectDriverLicenseType:
                        reportNameSpace = "Gisc.Csd.Service.Report.Library.Delinquent.DelinquentIncorrectDriverLicenseTypeReport, Gisc.Csd.Service.Report.Library";
                        //reportNameSpace = "Gisc.Csd.Service.Report.Library.Delinquent.DelinquentReport, Gisc.Csd.Service.Report.Library";
                        reportTitle = "DelinquentReport_DelinquentIncorrectDriverLicenseType";
                        break;
                    case Enums.ModelData.DelinquentReportType.DelinquentConnectionLost:
                        reportNameSpace = "Gisc.Csd.Service.Report.Library.Delinquent.DelinquentConnectionLostReport, Gisc.Csd.Service.Report.Library";
                        //reportNameSpace = "Gisc.Csd.Service.Report.Library.Delinquent.DelinquentReport, Gisc.Csd.Service.Report.Library";
                        reportTitle = "DelinquentReport_DelinquentConnectionLost";
                        break;
                    case Enums.ModelData.DelinquentReportType.DelinquentRestBelow10Hours:
                        reportNameSpace = "Gisc.Csd.Service.Report.Library.Delinquent.DelinquentRestLessThan10Hr, Gisc.Csd.Service.Report.Library";
                        //reportNameSpace = "Gisc.Csd.Service.Report.Library.Delinquent.DelinquentReport, Gisc.Csd.Service.Report.Library";
                        reportTitle = "DelinquentReport_DelinquentRestLessThan10Hr";
                        break;
                    default:
                        reportNameSpace = null;
                        break;
                }

               
                //console.log(BusinessUnitID);
                this.reports = {
                                    report:reportNameSpace, 
                                    parameters: {
                                                    UserID:WebConfig.userSession.id,
                                                    CompanyID:WebConfig.userSession.currentCompanyId,
                                                    ReportTemplateType:this.selectedReportType().value,
                                                    StartDate:formatSDate,
                                                    EndDate:formatEDate,
                                                    DurationType:durationType,
                                                    PrintBy:WebConfig.userSession.fullname,
                                                    Language: WebConfig.userSession.currentUserLanguage,
                                                    IncludeFormerInBusinessUnit: this.isIncludeFormerDriver() ? 1 : 0 ,
                                                    Enable: this.isIncludeResignedDriver() ? 1 : 0 ,
                                                    BusinessUnitID: BusinessUnitID,
                                                    IncludeSubBusinessUnit: IncludeSubBU,
                                                    EntityTypeID: EntityTypeID,
                                                    SelectedEntityID: SelectedEntityID,
                                                    ShippingType: shippingTypeId,
                                                    CategoryID: categoryId,
                                                    BuIds: BusinessUnitID != null ? BusinessUnitID.toString() : null 
                                                }
                };

                reportExport.CSV = this.webRequestReportExports.exportCSVDelinquentReport();
                this.navigate("cw-report-reportviewer", {reportSource: this.reports, reportName:this.i18n(reportTitle)(), reportExport:reportExport});
            } else {
                // This is unreachable code, since we disable save button when VM is not valid
                this.validationModel.errors.showAllMessages();
               
            }
        } 
    }
}

export default {
    viewModel: ScreenBase.createFactory(FormOverspeed),
    template: templateMarkup
};
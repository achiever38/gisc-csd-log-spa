﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import { Enums, Constants } from "../../../../app/frameworks/constant/apiConstant";
import AssetUtility from "../asset-utility";
import * as BladeDialog from "../../../../app/frameworks/constant/bladeDialog";

import WebRequestAutoMailReport from "../../../../app/frameworks/data/apitrackingcore/webRequestAutoMailReport"

/**
 * Display available subscription menu based on user permission.
 * @class MenuScreen
 * @extends {ScreenBase}
 */
class AutoMailReportManage extends ScreenBase {
    constructor(params){
        super(params);
        
        this.bladeSize = BladeSize.XLarge;
        //this.bladeIsMaximized = true;
        this.bladeTitle(this.i18n("Menu_Auto_Mail_Report")());
        this.columns = [];
        this.selectedItems = ko.observableArray([]);

        this.filterState = ko.observable({
            CompanyId: WebConfig.userSession.currentCompanyId,
            // Keyword: "",
            // CountryIds: [],
            // ProvinceIds: [],
            // CityIds: [],
            // TownIds: [],
            // PoiIconIds: [],
            // FromCreateDate: null,
            // ToCreateDate: null,
            // CreateBy: "",
            // poiType:null,
            // includeCount:true,
            // PageProperty: 1,
            page: 1,
            pageSize: 20,
            skip: 0,
            take: 20,
        });
        this.onSelectingRow = (autoMailReport, e) => {
            if(e){
                let reportSource = {
                    parameters:autoMailReport
                }
                if (WebConfig.userSession.hasPermission(Constants.Permission.UPDATEAUTOMAILREPORT)) {
                    this.navigate("cw-automail-report-manage-create", {
                        dataToSender:{reportSource:reportSource},
                        mode:'update'
                    });
                }
            }
        }


        this.subscribeMessage("cw-auto-mail-report", (autoMailId) => {
            this.refreshShipmentList()
        })
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        this.refreshShipmentList();

      

    }
    refreshShipmentList() {
        this.dispatchEvent("autoMailReportList", "refresh");
    }
    
    onDatasourceRequestRead (gridOption) {

        var dfd = $.Deferred();
        var self = this
        var filter = Object.assign({}, this.filterState(), gridOption);
        this.isBusy(true);
        this.webRequestAutoMailReport
        .listAutoReport(filter)
          .done(response => {
              
              dfd.resolve({
                  items: response["items"],
                  totalRecords: response["totalRecords"],
                  currentPage : response["currentPage"]
              });
          })
          .fail(e => {
              this.handleError(e);
          })
          .always(() => {
              this.isBusy(false);
            //   this.onAutoRefresh(true);
          });

        return dfd;
    }

    buildCommandBar(commands) {

        if (WebConfig.userSession.hasPermission(Constants.Permission.DELETEAUTOREPORT)) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdDelete":
                if(this.selectedItems().length > 0){
                    this.showMessageBox(null, this.i18n("M257")(), BladeDialog.DIALOG_YESNO).done((button) => {
                       switch (button) {
                           case BladeDialog.BUTTON_YES:
                               let deleteData = []
                               this.selectedItems().forEach(element => {
                                   deleteData.push(element.id)
                               });
                               this.webRequestAutoMailReport
                                .deleteAutoReport(deleteData)
                                .done(response => {
                                    this.publishMessage("cw-auto-mail-report", {});

                                })
                               break;
                       
                           default:
                               break;
                       }
                    });
                }else{
                    this.showMessageBox(null, this.i18n("M256")(), BladeDialog.BUTTON_OK).done((button) => {
                       
                    });
                }
                break;
        
            default:
                break;
        }
    }

    get webRequestAutoMailReport(){
        return WebRequestAutoMailReport.getInstance();
    }
}



export default {
    viewModel: ScreenBase.createFactory(AutoMailReportManage),
    template: templateMarkup
};

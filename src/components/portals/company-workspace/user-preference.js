﻿import ko from "knockout";
import templateMarkup from "text!./user-preference.html";
import ScreenBase from "../screenbase";
import ScreenHelper from "../screenhelper";
import WebConfig from "../../../app/frameworks/configuration/webConfiguration";
import WebRequestLanguage from "../../../app/frameworks/data/apicore/webRequestLanguage";
import WebRequestUser from "../../../app/frameworks/data/apicore/webRequestUser";
import {Enums} from "../../../app/frameworks/constant/apiConstant";

/**
 * Handle update user preferrences require restart when change.
 * 
 * @class UserPreferenceScreen
 * @extends {ScreenBase}
 */
class UserPreferenceScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Blade properties.
        this.bladeTitle(this.i18n("User_UserPreferences")());

        // Services injection.
        this.webRequestUser = WebRequestUser.getInstance();
        this.webRequestLanguage = WebRequestLanguage.getInstance();

        // Screen properties.
        this.enableSoundAlertValue = ko.observable();
        this.enableSoundAlertOptions = ScreenHelper.createYesNoObservableArray();
        this.fleetDelayTime = ko.observable();
        this.selectedLanguage = ko.observable();
        this.enabledLanguages = ko.observableArray();
        this.enableAlertNotificationValue = ko.observable();
        this.enableAlertNotificationOptions = ScreenHelper.createYesNoObservableArray();

        // Validation properties.
        this.validationModel = ko.validatedObservable();

        // Preferrence temporary.
        this.serverStorePreferences = [];
    }
    /**
     * Finding preference by key.
     * 
     * @param {any} Key
     * @returns
     */
    findPreferenceByKey(key){
        var pref = null;

        this.serverStorePreferences.forEach((p)=>{
            if(p.key === key){
                pref = p;
            }
        });

        return pref;
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        var dfd = $.Deferred();

        // Loading enabled language list from server.
        var languageFilter = { enable: true};
        var dfdLanguageRequest = this.webRequestLanguage.listLanguage(languageFilter);

        // Loading preferences from server.
        var preferenceFilter = {keys: [
            Enums.ModelData.UserPreferenceKey.EnableSoundAlert,
            Enums.ModelData.UserPreferenceKey.FleetDelayTime,
            Enums.ModelData.UserPreferenceKey.PreferredLanguage,
            Enums.ModelData.UserPreferenceKey.EnableAlertNotification]
        };
        var dfdPreferenceRequest  = this.webRequestUser.getUserPreferences(preferenceFilter);

        $.when(dfdLanguageRequest, dfdPreferenceRequest)
        .done((languages, preferences) => {
            // Restore languages.
            this.enabledLanguages.merge(languages.items);
            this.serverStorePreferences = preferences;

            var serverEnableSoundAlert = this.findPreferenceByKey(Enums.ModelData.UserPreferenceKey.EnableSoundAlert);
            if(serverEnableSoundAlert) {
                this.enableSoundAlertValue(
                    ScreenHelper.findOptionByProperty(this.enableSoundAlertOptions, "value", (serverEnableSoundAlert.value) === "true"
                ));
            }

            var serverFleetDelayTime = this.findPreferenceByKey(Enums.ModelData.UserPreferenceKey.FleetDelayTime);
            if(serverFleetDelayTime) {
                this.fleetDelayTime(serverFleetDelayTime.value);
            }

            var serverLanguage = this.findPreferenceByKey(Enums.ModelData.UserPreferenceKey.PreferredLanguage);
            if(serverLanguage){
                this.selectedLanguage(
                    ScreenHelper.findOptionByProperty(this.enabledLanguages, "code", serverLanguage.value
                ));
            }

            var serveAlertNotification = this.findPreferenceByKey(Enums.ModelData.UserPreferenceKey.EnableAlertNotification);
            if(serveAlertNotification){
                this.enableAlertNotificationValue(
                    ScreenHelper.findOptionByProperty(this.enableAlertNotificationOptions, "value", (serveAlertNotification.value) === "true")
                )
            }

            // Unlock loading screen.
            dfd.resolve();
        })
        .fail(error => {
            // Unlock with error handling.
            dfd.reject(error);
        });

        return dfd;
    }
    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        // Setup validators.
        this.enableSoundAlertValue.extend({
            required: true
        });
        this.selectedLanguage.extend({
            required: true
        });
        this.enableAlertNotificationValue.extend({
            required: true
        });
        this.validationModel({
            enableSoundAlertValue: this.enableSoundAlertValue,
            selectedLanguage: this.selectedLanguage,
            enableAlertNotificationValue: this.enableAlertNotificationValue
        });

        // Track change for specific properties.
        this.enableSoundAlertValue.extend({
            trackChange: true
        });
        this.fleetDelayTime.extend({
            trackChange: true
        });
        this.selectedLanguage.extend({
            trackChange: true
        });
        this.enableAlertNotificationValue.extend({
            trackChange: true
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }

    /**
     * Generate user model from View Model
     * 
     * @returns user model which is ready for ajax request 
     */
    generateModel() {
        var updatedModel = [];

        var serverEnableSoundAlert = this.findPreferenceByKey(Enums.ModelData.UserPreferenceKey.EnableSoundAlert);
        if(serverEnableSoundAlert) {
            serverEnableSoundAlert.value = this.enableSoundAlertValue().value;
            serverEnableSoundAlert.infoStatus = Enums.InfoStatus.Update;
            updatedModel.push(serverEnableSoundAlert);
        }

        var serverFleetDelayTime = this.findPreferenceByKey(Enums.ModelData.UserPreferenceKey.FleetDelayTime);
        if(serverFleetDelayTime) {
            serverFleetDelayTime.value = this.fleetDelayTime();
            serverFleetDelayTime.infoStatus = Enums.InfoStatus.Update;
            updatedModel.push(serverFleetDelayTime);
        }

        var serverLanguage = this.findPreferenceByKey(Enums.ModelData.UserPreferenceKey.PreferredLanguage);
        if(serverLanguage){
            serverLanguage.value = this.selectedLanguage().code;
            serverLanguage.infoStatus = Enums.InfoStatus.Update;
            updatedModel.push(serverLanguage);
        }

        var serverAlertNotification = this.findPreferenceByKey(Enums.ModelData.UserPreferenceKey.EnableAlertNotification);
        if(serverAlertNotification){
            serverAlertNotification.value = this.enableAlertNotificationValue().value;
            serverAlertNotification.infoStatus = Enums.InfoStatus.Update;
            updatedModel.push(serverAlertNotification);
        }
        return updatedModel;
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        switch(sender.id){
            case "actSave":
                // Check validation is valid.
                if (!this.validationModel.isValid()) {
                    this.validationModel.errors.showAllMessages();
                    return;
                }

                // Turn on is busy indicator.
                this.isBusy(true);

                // Always update preferrence to server.
                var model = this.generateModel();

                // Invoke service for saving data.
                this.webRequestUser.updateUserPreferences(model)
                .done((success)=> {
                    // Apply runtime variable for user settings, enable sound alet.
                    if(this.enableSoundAlertValue.isDirty()) {
                        var enableSoundAlertBoolean = this.enableSoundAlertValue().value;
                        WebConfig.userSession.enableSoundAlert = (enableSoundAlertBoolean);
                    }

                    // Apply runtime variable for user settings fleet delay time.
                    if(this.fleetDelayTime.isDirty()) {
                        WebConfig.userSession.fleetDelayTime = (this.fleetDelayTime());
                    }

                    // IF change call server then restart website.
                    if(this.selectedLanguage.isDirty()) {
                        // Restart portal.
                        window.location.reload();
                    }

                    // Apply runtime variable for user settings, enable sound alet.
                    if(this.enableAlertNotificationValue.isDirty()) {
                        var enableAlertNotificationBoolean = this.enableAlertNotificationValue().value;
                        WebConfig.userSession.enableAlertNotification = (enableAlertNotificationBoolean);
                        window.location.reload();
                    }

                    this.isBusy(false);

                    // Closing this form.
                    this.close(true);
                })
                .fail((error) => {
                    // Turn off loading indicator.
                    this.isBusy(false);

                    this.handleError(e);
                });
                break;
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(UserPreferenceScreen),
    template: templateMarkup
};
import ko from "knockout";
import { Enums } from "../../../app/frameworks/constant/apiConstant";

class GenerateSortingColumns {
    /**
     * GenerateSortingColumns for track monitoring, view Alerts, nearest assets.
     * @static
     * @param {any} indexArray
     * @param {any} columnCollection
     * @returns
     * 
     * @memberOf GenerateSortingColumns
     */
    static getSortingColumn (indexArray, columnCollection) {

        var sortColumn = [];
        if(!_.isNil(indexArray) && !_.isNil(columnCollection)){
            sortColumn = [{},{}];

            sortColumn[0].column = columnCollection[indexArray[0]].enumColumnName;
            sortColumn[0].direction = indexArray[1] === "asc" ? 1 : 2 ;

            sortColumn[1].column = Enums.SortingColumnName.Id;
            sortColumn[1].direction = sortColumn[0].direction;
        }

        return sortColumn;
    }
}

export default GenerateSortingColumns;
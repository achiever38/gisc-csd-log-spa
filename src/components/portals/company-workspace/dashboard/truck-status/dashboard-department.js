﻿import ko from "knockout";
import templateMarkup from "text!./dashboard-department.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Constants, Enums } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestTruckDriverUtilization from "../../../../../app/frameworks/data/apitrackingcore/webRequestTruckDriverUtilization";
/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class TruckStatusDashboardDepartment extends ScreenBase {
    /**
     * Creates an instance of Department.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        //params = {"businessUnitId":1,"businessUnitName":"SCCC"};
        //params = {"businessUnitId":91,"businessUnitName":"Aggregate"};

        this.bladeTitle(this.i18n("Dashboard_Truck_Status_Dashboard_Header")());
        this.bladeSize = BladeSize.XLarge;
        this.bladeIsMaximized = true;
        this.lblLastSync = ko.observable(`${this.i18n("Vehicle_MA_LastSync")()} `)

        /* Graph BU */
        this.filterGraphBu = ko.observable(params)
        this.donutChartGraphBUData = ko.observableArray([]);
        this.centerLabelGraphBU = ko.observable(" ");
        this.titleGraphBU = ko.observable(params.businessUnitName);
        this.formatLabelText = ko.observable('[[percents]]%\n([[value]])');
        this.formatBalloonText = ko.observable('[[title]]: [[percents]]%([[value]])');
        this.graphBULst = ko.observableArray([]);
        this.textontopchart = ko.observable('[[value]]')

        /* Graph Parking & Moving */
        this.filterGraphParkingMoving = ko.observable(params);
        this.graphParkingData = ko.observableArray([]);
        this.graphMovingData = ko.observableArray([]);
        this.centerLabelGraphParking = ko.observable(" ");
        this.centerLabelGraphMoving = ko.observable(" ");
        this.titleGraphParking = ko.observable("No Shipment: Parking");
        this.titleGraphMoving = ko.observable("No Shipment: Moving");
        this.graphParkingLst = ko.observableArray([]);
        this.graphMovingLst = ko.observableArray([]);
        this.showEntries = ko.observable(false);

        // Set Default Filter Graph Parking & Moving
        this.filterGraphParkingMoving().parking = "NP"
        this.filterGraphParkingMoving().moving = "NM"  

        /* Graph Parking & Moving(Detail) */
        this.filterGraphParkingDetail = ko.observable(params);
        this.graphParkingDetailData = ko.observableArray([]);
        this.titleParkingDetail = ko.observable();
        this.graphParkingDetailLst = ko.observableArray([]);

        this.filterGraphMovingDetail = ko.observable(params);        
        this.graphMovingDetailData = ko.observableArray([]);        
        this.titleMovingDetail = ko.observable();
        this.graphMovingDetailLst = ko.observableArray([]);

        this.formatBalloonBarChartText = ko.observable('[[name]]: <b>[[value]]</b>');
        
        // Default Filter Graph Parking & Moving(Detail)
        this.filterGraphParkingDetail(`${this.filterGraphParkingMoving().parking}_I`) // val == NP_I
        this.filterGraphMovingDetail(`${this.filterGraphParkingMoving().moving}_I`) // val == NM_I

        // this.animation = ko.observable(false);

        this.setBladeHeight()        
        this.propertygraph = ko.observable();
        // autoRefresh
        this.timeOutHandler = this.ensureNonObservable(null);
        this.isAutoRefresh = ko.observable(true);

    }

    onClickPieChartBU(params) {

        let filterGraphParkingMoving = this.filterGraphParkingMoving()
        let nameDataGraph = params.dataItem.dataContext.id;
        let namePieChart = params.dataItem.dataContext.name;
        if(params.dataItem.dataContext.name.search('Parking') != -1) { // check ว่าข้อมูลที่คลิกได้มาเป็น Parking หรือ Moving
            filterGraphParkingMoving.parking = nameDataGraph

            this.titleGraphParking(namePieChart);
            this.filterGraphParkingDetail(`${nameDataGraph}_I`)
            this.graphParkingLst()[nameDataGraph].parkingMovingGraphInfos.filter((item)=> {
                if(item.id == this.filterGraphParkingDetail().split("_")[1]) {
                    this.titleParkingDetail(item.name)
                }
            })
            this.createChartParking() // chart ที่ 2
            this.createChartParkingDetail() // chart ที่ 4      
        } else {
            filterGraphParkingMoving.moving = nameDataGraph

            this.titleGraphMoving(namePieChart);
            this.filterGraphMovingDetail(`${nameDataGraph}_I`)
            this.graphMovingLst()[nameDataGraph].parkingMovingGraphInfos.filter((item)=> {
                if(item.id == this.filterGraphMovingDetail().split("_")[1]) {
                    this.titleMovingDetail(item.name)
                }
            })
            this.createChartMoving() // chart ที่ 3  
            this.createChartMovingDetail() // chart ที่ 5  
        }  

        
    }

    onClickPieChartPark(params) {
        this.filterGraphParkingDetail(`${this.filterGraphParkingMoving().parking}_${params.dataItem.dataContext.id}`)
        this.titleParkingDetail(params.dataItem.dataContext.name)
        this.createChartParkingDetail() // chart ที่ 4    
    }

    onClickPieChartMove(params) {
        this.filterGraphMovingDetail(`${this.filterGraphParkingMoving().moving}_${params.dataItem.dataContext.id}`)
        this.titleMovingDetail(params.dataItem.dataContext.name)
        this.createChartMovingDetail() // chart ที่ 5  
    }

    onClickBarChart(type, params) {
        let model = {}
        model.state = type
        model.data = {
            businessUnitId: [this.filterGraphBu().businessUnitId],
            durationType: params.item.dataContext.id,
            jobType: null,
            movementType: null,
            graphType: null
        }
        switch (type) {
            case "parking":
                let filterGraphParkingDetail = this.filterGraphParkingDetail().split("_")
                model.data.jobType = filterGraphParkingDetail[0]
                model.data.graphType = filterGraphParkingDetail[1]
                model.data.movementType = Enums.ModelData.MovementType.Park
                break
            case "moving":
                let filterGraphMovingDetail = this.filterGraphMovingDetail().split("_")
                model.data.jobType = filterGraphMovingDetail[0]
                model.data.graphType = filterGraphMovingDetail[1]
                model.data.movementType = Enums.ModelData.MovementType.Move
                break
            default:
                break
        }
        this.navigate("cw-dashboard-truck-status-details", model)
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // TODO: Verify access right to specific items
        if (!isFirstLoad) {
            return;
        }
        this.getChartData();     
    }

     /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() { 
        this.isAutoRefresh(false);
    }

    getChartData() {
        this.isBusy(true)
        var propertygraph = [{
            // labelsEnabled: false,
            autoMargins: false,
            marginTop: 10,
            marginBottom: 0,
            marginLeft: 0,
            marginRight: 10
        }]
        this.propertygraph(propertygraph)
        let dfdTruckStatusGraphBU = this.webRequestTruckDriverUtilization.truckStatusGraphBU(this.filterGraphBu())

        $.when(dfdTruckStatusGraphBU).done((res) => {
            this.lblLastSync(`${this.i18n("Vehicle_MA_LastSync")()} ${res.formatTimestamp}`)
            /* binding Data */
            this.graphBULst(res.items["0"])
            this.graphParkingLst(res.items["0"].parkingMovingGraphInfos)
            this.graphMovingLst(res.items["0"].parkingMovingGraphInfos)
            this.graphParkingDetailLst(res.items["0"].barGraphInfos)
            this.graphMovingDetailLst(res.items["0"].barGraphInfos)

            /* set ค่า default เมื่อเรียก service เสร็จ */
            res.items["0"].businessUnitGraphInfos.filter((item)=> {
                if(item.id == this.filterGraphParkingMoving().parking) {
                    this.titleGraphParking(item.name)
                }
            })
            res.items["0"].businessUnitGraphInfos.filter((item)=> {
                if(item.id == this.filterGraphParkingMoving().moving) {
                    this.titleGraphMoving(item.name);
                }
            })
            res.items["0"].parkingMovingGraphInfos[this.filterGraphParkingMoving().parking].parkingMovingGraphInfos.filter((item)=> {
                if(item.id == this.filterGraphParkingDetail().split("_")[1]) {
                    this.titleParkingDetail(item.name)
                }
            })
            res.items["0"].parkingMovingGraphInfos[this.filterGraphParkingMoving().moving].parkingMovingGraphInfos.filter((item)=> {
                if(item.id == this.filterGraphMovingDetail().split("_")[1]) {
                    this.titleMovingDetail(item.name)
                }
            })
            /* set ค่า default เมื่อเรียก service เสร็จ */

            this.createGraphBU() // chart ที่ 1
            this.createChartParking() // chart ที่ 2
            this.createChartMoving() // chart ที่ 3
            this.createChartParkingDetail() // chart ที่ 4
            this.createChartMovingDetail() // chart ที่ 5    
            this.onAutoRefresh(true);

        }).fail((e) => {
            this.handleError(e)
        }).always(() => {
            this.isBusy(false)
        })
    }

    createGraphBU() {
        let lst = this.graphBULst()

        this.donutChartGraphBUData(lst.businessUnitGraphInfos)
        //`Total: ${this.setFormatNumber(lst.totalTruck)}` แสดงค่าใน donutChart
        this.centerLabelGraphBU(`${this.setFormatNumber(lst.totalTruck)}`)
        this.titleGraphBU(lst.businessUnitName)
    }

    createChartParking() {    
        let filter = this.filterGraphParkingMoving().parking    
        let lst = this.graphParkingLst()
        let selectedLst = lst[filter].parkingMovingGraphInfos
        let total = lst[filter].totalTruck
        
        
        
        let popperty = [{
            y: "50%",
            align: "center",
            size: 16,
            bold: true,
            text: total,
            color: "#616161"
        },{
            
            align: "center",
            size: 16,
            bold: true,
            text: this.titleGraphParking(),
            color: "#616161"
        }]

        this.centerLabelGraphParking(popperty)
        this.graphParkingData(selectedLst)
    }

    createChartMoving() {
        let filter = this.filterGraphParkingMoving().moving
        let lst = this.graphMovingLst()
        let selectedLst = lst[filter].parkingMovingGraphInfos
        let total = lst[filter].totalTruck
        
        
        let popperty = [{
            y: "50%",
            align: "center",
            size: 16,
            bold: true,
            text: total,
            color: "#616161"
        },{
            
            align: "center",
            size: 16,
            bold: true,
            text: this.titleGraphMoving(),
            color: "#616161"
        }]

        this.centerLabelGraphMoving(popperty)
        this.graphMovingData(selectedLst)
    }

    createChartParkingDetail() {
        let filter = this.filterGraphParkingDetail()        
        let lst = this.graphParkingDetailLst()
        let selectedLst = lst[filter].barGraphinfos

        
        this.graphParkingDetailData(selectedLst)
    }

    createChartMovingDetail() {
        let filter = this.filterGraphMovingDetail()
        let lst = this.graphParkingDetailLst()     
        let selectedLst = lst[filter].barGraphinfos

        this.graphMovingDetailData(selectedLst)
    }

    setFormatNumber(num) {
        return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }

    setBladeHeight() {
        /* เอาความสูงของหน้าจอมากำหนดให้ blade เพื่อทำให้กราฟแสดงเต็มจอ */
        this.bladeId = $(".app-blade-content-container")["0"].parentElement.id
        this.screenHeight = ($("#" + this.bladeId + " .app-blade-content-container")["0"].clientHeight - 20)
        $(".div-dashboard-department").attr("style", `height:${this.screenHeight}px;`)
        /* เอาความสูงของหน้าจอมากำหนดให้ blade เพื่อทำให้กราฟแสดงเต็มจอ */
    }

    onAutoRefresh(reCall) {
        if (reCall) {
            clearTimeout(this.timeOutHandler);
            this.onAutoRefresh();
            return;
        }

        if (this.isAutoRefresh()) {
            this.timeOutHandler = setTimeout(() => {
                this.getChartData();
                this.onAutoRefresh();
            }, WebConfig.appSettings.trackStatusAutoRefreshTime);
        } else {
            clearTimeout(this.timeOutHandler);
        }
    }

    get webRequestTruckDriverUtilization() {
        return WebRequestTruckDriverUtilization.getInstance()
    }

    get DepartmentConstants() {
        return {
            SHIPMENT_ASSIGNED_PARK: 'SP',
            SHIPMENT_ASSIGNED_MOVE: 'SM',
            NO_SHIPMENT_ASSIGNED_PARK: 'NP',
            NO_SHIPMENT_ASSIGNED_MOVE: 'NM'
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(TruckStatusDashboardDepartment),
    template: templateMarkup
};
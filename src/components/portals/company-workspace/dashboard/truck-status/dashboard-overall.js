﻿import ko from "knockout";
import templateMarkup from "text!./dashboard-overall.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import {
    Constants,
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestTruckDriverUtilization from "../../../../../app/frameworks/data/apitrackingcore/webRequestTruckDriverUtilization";
/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class TruckStatusDashboardOverall extends ScreenBase {
    /**
     * Creates an instance of DashboardOverall.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        //params = {"businessUnits":["0","91","1"]}

        this.bladeTitle(this.i18n("Dashboard_Truck_Status_Dashboard_Header")());
        this.bladeSize = BladeSize.XLarge;
        this.bladeIsMaximized = true;
        this.selectedIndex = ko.observable(0);

        this.businessUnitList = ko.observableArray();
        this.businessUnit = params.businessUnits;
        this.donutChartList = ko.observableArray([]);
        this.stackChartList = ko.observableArray([]);
        this.formatLabelText = ko.observable('([[percents]]%)\n[[value]]');
        this.formatBalloonText = ko.observable('([[percents]]%)[[value]]');
        this.lblLastSync = ko.observable(`${this.i18n("Vehicle_MA_LastSync")()} `)
        this.minimizeAll(false)
        this.animation = ko.observable(false);

        this.dataList = ko.observableArray([]);
        this.isVisibleBarChart = ko.observable(true);
        this.textt = ko.observable();

        this.barstackamchartData = ko.observableArray([]);
        // this.nameChart = ko.observable(this.i18n("Dashboard_Driver_Performance_Score_TableColumn_Score")());
        this.nameChart = ko.observable('TEST');
       
       
        /* เอาความสูงของหน้าจอมากำหนดให้ blade */
        this.bladeId = $(".app-blade-content-container")["0"].parentElement.id
        this.screenHeight = ($("#" + this.bladeId + " .app-blade-content-container")["0"].clientHeight - 45) / 2
        $(".div-dashboard-overall-allGraph").attr("style", `height:${this.screenHeight}px;`)



        this.screenHeightForStackChart = ($("#" + this.bladeId + " .app-blade-content-container")["0"].clientHeight - 120)
        $(".div-barstackamchart").attr("style", `height:${this.screenHeightForStackChart}px;`)
        // $(".component-container").attr("style",`padding-bottom:0px;`)

        // this.height = ko.observable('');
        this.maximumDisplayColumnMainChart = ko.observable(15);


        this.isShowChartScrollbar = ko.computed(() => {
            return this.barstackamchartData().length > this.maximumDisplayColumnMainChart(); //return true or false
        });
        this.fixedColumnWidth = ko.computed(() => {
            return this.barstackamchartData().length > this.maximumDisplayColumnMainChart() ? null : 60;
        });

        // autoRefresh
        this.timeOutHandler = this.ensureNonObservable(null);
        this.isAutoRefresh = ko.observable(true);

        // this.selectedIndex.subscribe((index)=> {
        //     console.log("tab screenHeight>>",this.screenHeight)

        //     if(index == 1){
        //         //  this.setHeightDivStackChart(index)
        //          $(".div-barstackamchart").attr("style", `height:${this.screenHeight}px;`)
        //     }

        //     // this.publishMessage("cw-shipment-dashboard-search-swap-content", index);
        // })


    }


    onClickPieChart(params) {
        this.navigate("cw-dashboard-truck-status-dashboard-dept", {
            businessUnitId: params.detailInfo.businessUnitId,
            businessUnitName: params.detailInfo.businessUnitName
        });
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // TODO: Verify access right to specific items
        if (!isFirstLoad) {
            return;
        }
        this.getChartData();
    }
    getChartData() {
        this.isBusy(true)
        var self = this;
        this.donutChartList.replaceAll([]);

        let param = {
            businessUnitId: this.businessUnit //this.businessUnit()
        };
        let dfdGraph = this.webRequestTruckDriverUtilization.truckStatusGraphAll(param);
        
        $.when(dfdGraph).done((response) => {

            let reFormatData = new Array();
            let data = response.items;
            data.forEach(item => {
                let itemData = {
                    "name": item.businessUnitName,
                    "businessUnitId": item.businessUnitId
                };

                item.graphInfos.forEach((itemGrapInfos, index) => {
                    itemData["value" + (index + 1).toString()] = itemGrapInfos.value
                    itemData["color" + (index + 1).toString()] = itemGrapInfos.color
                })
                itemData.totalvalue = itemData.value1 + itemData.value2
                reFormatData.push(itemData);
            });
            this.barstackamchartData.replaceAll(reFormatData);


            this.lblLastSync(`${this.i18n("Vehicle_MA_LastSync")()}  ${response.formatTimestamp}`)

            if (response.items.length != 0) {
                response.items.forEach(item => {
                    let popperty = [{
                        y: "50%",
                        align: "center",
                        size: 16,
                        bold: true,
                        text: `${this.setFormatNumber(item.totalTruck)}`,
                        color: "#616161"
                    }, {
            
                        align: "center",
                        size: 16,
                        bold: true,
                        text: item.businessUnitName,
                        color: "#616161"
                    }]

                    // if (item.totalTruck != 0)
                    this.donutChartList.push({
                        donutChartData: item.graphInfos,
                        formatLabelText: this.formatLabelText(),
                        formatBalloonText: this.formatBalloonText(),
                        animation: this.animation(),
                        allLabelsdata: popperty,
                        detailInfo: item
                    })
                });
            } else {
                $("#" + self.id).html(this.i18n('Common_DataNotFound')())

            }

            this.onAutoRefresh(true);
        }).fail((e) => {
            this.handleError(e)
        }).always(() => {
            this.isBusy(false)
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        this.isAutoRefresh(false);
    }

    buildCommandBar(commands) {
        //commands.push(this.createCommand("cmdRefresh", this.i18n("Common_Refresh")(), "svg-cmd-refresh"));
    }

    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdRefresh":
                break;
        }
    }

    

    setFormatNumber(num) {
        return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }

    stackChartCallback(params) {

        this.navigate("cw-dashboard-truck-status-dashboard-dept", {
            businessUnitId: params.item.dataContext.businessUnitId,
            businessUnitName: params.item.dataContext.name
        });
    }

    onAutoRefresh(reCall) {
        if (reCall) {
            clearTimeout(this.timeOutHandler);
            this.onAutoRefresh();
            return;
        }

        if (this.isAutoRefresh()) {
            this.timeOutHandler = setTimeout(() => {
                this.getChartData();
                this.onAutoRefresh();
            }, WebConfig.appSettings.shipmentAutoRefreshTime);
        } else {
            clearTimeout(this.timeOutHandler);
        }
    }

    get webRequestTruckDriverUtilization() {
        return WebRequestTruckDriverUtilization.getInstance();
    }
}


export default {
    viewModel: ScreenBase.createFactory(TruckStatusDashboardOverall),
    template: templateMarkup
};
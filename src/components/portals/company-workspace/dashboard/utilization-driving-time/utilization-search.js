﻿import ko from "knockout";
import templateMarkup from "text!./utilization-search.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import {
    Constants,
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";


/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class Utilizationsearch extends ScreenBase {

    /**
     * Creates an instance of ShipmentDashboardScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        
        this.bladeTitle(this.i18n("Dashboard_Truck_Status_Header_Search")());
        this.bladeSize = BladeSize.Small;


        this.startDate = this.ensureObservable();
        this.endDate = this.ensureObservable();
        this.businessUnitList = ko.observableArray([]);
        this.businessUnit = ko.observable();
        this.ValueRadio = ko.observable('');

        this.ddEntityType = ko.observableArray([]);
        this.selectedEntityType = ko.observable();

        this.ValueRadio.subscribe((res) => {
            var startDate = new Date();
            var endDate = new Date();

            switch (res) {
                case 'Today':
                    this.startDate(this.setFormatDateTime({
                        data: startDate,
                        start: true
                    }))
                    this.endDate(this.setFormatDateTime({
                        data: endDate,
                        end: true
                    }))
                    $("#searchStartDate").val(this.setFormatDisplayDateTime(startDate));
                    $("#searchEndDate").val(this.setFormatDisplayDateTime(endDate));
                    break;
                case 'Yesterday':
                    startDate.setDate(startDate.getDate() - 1)
                    this.startDate(this.setFormatDateTime({
                        data: startDate,
                        start: true
                    }))
                    this.endDate(this.setFormatDateTime({
                        data: startDate,
                        end: true
                    }))
                    $("#searchStartDate").val(this.setFormatDisplayDateTime(startDate));
                    $("#searchEndDate").val(this.setFormatDisplayDateTime(startDate));

                    break;
                case 'ThisWeek':
                    let startWeek = parseInt(startDate.getDate() - (startDate.getDay() - 1));
                    let endWeek = parseInt(endDate.getDate() + (7 - endDate.getDay()));

                    startDate.setDate(startWeek)
                    endDate.setDate(endWeek)

                    this.startDate(this.setFormatDateTime({
                        data: startDate,
                        start: true
                    }))
                    this.endDate(this.setFormatDateTime({
                        data: endDate,
                        end: true
                    }))
                    $("#searchStartDate").val(this.setFormatDisplayDateTime(startDate));
                    $("#searchEndDate").val(this.setFormatDisplayDateTime(endDate));

                    break;
                case 'LastWeek':
                    let LastendWeek = parseInt((startDate.getDate() - (startDate.getDay() - 1)));
                    let LaststartWeek = parseInt(LastendWeek - 7);

                    startDate.setDate(LaststartWeek)
                    endDate.setDate(LastendWeek - 1)

                    this.startDate(this.setFormatDateTime({
                        data: startDate,
                        start: true
                    }))
                    this.endDate(this.setFormatDateTime({
                        data: endDate,
                        end: true
                    }))
                    $("#searchStartDate").val(this.setFormatDisplayDateTime(startDate));
                    $("#searchEndDate").val(this.setFormatDisplayDateTime(endDate));

                    break;
                case 'ThisMonth':
                    let firstDay = new Date(startDate.getFullYear(), startDate.getMonth(), 1);
                    let lastDay = new Date(endDate.getFullYear(), endDate.getMonth() + 1, 0);
                    this.startDate(this.setFormatDateTime({
                        data: firstDay,
                        start: true
                    }));
                    this.endDate(this.setFormatDateTime({
                        data: lastDay,
                        end: true
                    }));
                    $("#searchStartDate").val(this.setFormatDisplayDateTime(firstDay));
                    $("#searchEndDate").val(this.setFormatDisplayDateTime(lastDay));
                    break;
                case 'LastMonth':
                    let LastfirstDay = new Date(startDate.getFullYear(), startDate.getMonth() - 1, 1);
                    let LastlastDay = new Date(endDate.getFullYear(), endDate.getMonth(), 0);

                    this.startDate(this.setFormatDateTime({
                        data: LastfirstDay,
                        start: true
                    }))
                    this.endDate(this.setFormatDateTime({
                        data: LastlastDay,
                        end: true
                    }))
                    $("#searchStartDate").val(this.setFormatDisplayDateTime(LastfirstDay));
                    $("#searchEndDate").val(this.setFormatDisplayDateTime(LastlastDay));
                    break;
                case '':
                    this.isRadioSelectedStartDate(false);
                    this.isRadioSelectedEndDate(false);
                    break;
            }
        });


        this.isDateTimeEnabled = ko.computed(() => {
            return this.ValueRadio() == 'Custom';
        });


        //this.startDate.subscribe((val) => {
        //    if (this.isRadioSelectedStartDate()) {
        //        this.isRadioSelectedStartDate(false);
        //    }
        //    else {
        //        this.ValueRadio('');
        //    }
        //});

        //this.endDate.subscribe((val) => {
        //    if (this.isRadioSelectedEndDate()) {
        //        this.isRadioSelectedEndDate(false);
        //    }
        //    else {
        //        this.ValueRadio('');
        //    }
        //});
    }




    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // TODO: Verify access right to specific items
        if (!isFirstLoad) {
            return;
        }


        var dfd = $.Deferred();
        var businessUnitFilter = {
            companyId: WebConfig.userSession.currentCompanyId
        }
        var dfdBusinessUnit = this.webRequestBusinessUnit.listBusinessUnitSummary(businessUnitFilter)

        var waypointStatusFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            types: [Enums.ModelData.EnumResourceType.SearchEntityType],
            values: [
                Enums.ModelData.SearchEntityType.Vehicle,
                Enums.ModelData.SearchEntityType.Driver
            ]
        }

        let dfdEnumResource = this.webRequestEnumResource.listEnumResource(waypointStatusFilter);

        $.when(dfdBusinessUnit, dfdEnumResource).done((respondBusinessUnit, respondEnumResource) => {
            this.businessUnitList(respondBusinessUnit.items);
            this.ddEntityType(respondEnumResource.items);

            this.selectedEntityType(ScreenHelper.findOptionByProperty(this.ddEntityType, "value", Enums.ModelData.SearchEntityType.Vehicle));

            dfd.resolve();
        })

        return dfd;

        this.startDate.subscribe((res)=>{
            if(res == null){
                $("#searchStartDate").val("");
            }
        })

        this.endDate.subscribe((res)=>{
            if(res == null){
                $("#searchEndDate").val("");
            }
        })


    }

    setFormatDateTime(dateObj) {

        var newDateTime = "";

        let myDate = new Date(dateObj.data)
        let year = myDate.getFullYear().toString();
        let month = (myDate.getMonth() + 1).toString();
        let day = myDate.getDate().toString();
        let hour = myDate.getHours().toString();
        let minute = myDate.getMinutes().toString();
        let sec = "00";

        if (dateObj.start) {
            hour = "00";
            minute = "00";
        } else if (dateObj.end) {
            hour = "23";
            minute = "59";
            sec = "59";
        } else {}

        month = month.length > 1 ? month : "0" + month;
        day = day.length > 1 ? day : "0" + day;
        hour = hour.length > 1 ? hour : "0" + hour;
        minute = minute.length > 1 ? minute : "0" + minute;
        newDateTime = year + "-" + month + "-" + day + "T" + hour + ":" + minute + ":" + sec;

        return newDateTime;
    }

    setFormatDisplayDateTime(objDate) {
        let displayDate = "";
        let d = objDate;
        let year = d.getFullYear().toString();
        let month = (d.getMonth() + 1).toString();
        let day = d.getDate().toString();

        displayDate = day + "/" + month + "/" + year;

        return displayDate
    }
    setDisplayDate(dategraph) { 
        // dategraph Ex. Format 2018-08-01T00:00:00

        let year = dategraph.split("-")[0]
        let month = dategraph.split("-")[1]
        let day = dategraph.split("-")[2]
        day = day.split("T")[0]
        let date = day + "/" + month + "/" + year

        // Ex. return format 01/08/2018
        return date;
    }

    setupExtend() {
        this.businessUnit.extend({
            required: true
        });
        this.startDate.extend({
            required: true
        });
        this.endDate.extend({
            required: true
        });
        this.selectedEntityType.extend({
            required: true
        });

        var paramsValidation = {
            businessUnit: this.businessUnit,
            startDate: this.startDate,
            txtShipmentCode: this.endDate,
            entityType: this.selectedEntityType
        }
        this.validationModel = ko.validatedObservable(paramsValidation);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actPreview", this.i18n('Dashboard_Utilization_Time_Preview')()));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        switch (sender.id) {
            case "actPreview":

                if (!this.validationModel.isValid()) {
                    this.validationModel.errors.showAllMessages();
                    return;
                }

                let filter = {};
                filter.businessUnitId = this.businessUnit();
                filter.dateFrom = this.startDate();
                filter.dateTo = this.endDate();
                filter.entityType = this.selectedEntityType();
                filter.dataType = "D"
                filter.dateFromFormat = this.setDisplayDate(this.startDate());
                filter.dateToFormat = this.setDisplayDate(this.endDate());
                filter.entityTypeValue = this.selectedEntityType().value;

                this.navigate("cw-dashboard-utilization-driving-time-dashboard", filter);
                
                break;
            default:
                break;
        }
    }


    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

}


export default {
    viewModel: ScreenBase.createFactory(Utilizationsearch),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./details.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import {
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestTruckDriverUtilization from "../../../../../app/frameworks/data/apitrackingcore/webRequestTruckDriverUtilization";
/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class DetailsUtilization extends ScreenBase {

    /**
     * Creates an instance of ShipmentDashboardScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.filter = ko.observable(params);
        this.barChart = ko.observableArray([]);
        this.bladeSize = BladeSize.Medium;
        //this.bladeIsMaximized = true;
        this.dateFrom = ko.observable();
        this.dateTo = ko.observable();
        this.apiDataSource = ko.observableArray([]);
        this.timeOutHandler = this.ensureNonObservable(null);
        this.isAutoRefresh = ko.observable(false);
        this.travelTime = ko.observable(this.i18n("Dashboard_Utilization_Details_Travel_Time")() + '<br>' + "<div style='color:  green;'>" + this.i18n("Dashboard_Utilization_Details_With_Shipment")() + "</div>");
        this.parkingTime = ko.observable(this.i18n("Dashboard_Utilization_Details_Total_Parking_Time")() + '<br>' + "<div style='color:  green;'>" + this.i18n("Dashboard_Utilization_Details_With_Shipment")() + "</div>");
        this.travelOutTime = ko.observable(this.i18n("Dashboard_Utilization_Details_Travel_Time_Without_Shipment")() + '<br>' + "<div style='color:  red;'>" + this.i18n("Dashboard_Utilization_Details_Without_Shipment")() + "</div>");
        this.parkingOutTime = ko.observable(this.i18n("Dashboard_Utilization_Details_Parking_Time_Without_Shipment")() + '<br>' + "<div style='color:  red;'>" + this.i18n("Dashboard_Utilization_Details_Without_Shipment")() + "</div>");

        this.columns = ko.observableArray([{
                type: 'text',
                title: 'i18n:Common_BusinessUnit',
                data: 'businessUnitName'
            },
            {
                type: 'text',
                title: 'i18n:Dashboard_Utilization_Details_Number_of_Shipment',
                data: 'formatNoOfShipment',
                className: 'dg-body-right'
            },
            {
                type: 'text',
                title: 'i18n:Dashboard_Utilization_Details_Total_Travel_Time',
                data: 'totalTravelTime',
                sortable: false,
                searchable: false
            },
            {
                type: 'text',
                title: this.travelTime(),
                data: 'travelTimeShipment',
                sortable: false,
                searchable: false
            },
            {
                type: 'text',
                title: this.parkingTime(),
                data: 'parkingTimeShipment',
                sortable: false,
                searchable: false
            },
            {
                type: 'text',
                title: this.travelOutTime(),
                data: 'travelTimeNoShipment',
                sortable: false,
                searchable: false
            },
            {
                type: 'text',
                title: this.parkingOutTime(),
                data: 'parkingTimeNoshipment',
                sortable: false,
                searchable: false
            }
        ]);

        if (this.filter().entityType == Enums.ModelData.SearchEntityType.Driver) {
            this.bladeTitle(this.i18n("Dashboard_Utilization_Details_Driver")());
            let column = [{
                    type: 'text',
                    title: 'i18n:Common_Driver',
                    data: 'entityName'
                },
                {
                    type: 'text',
                    title: 'i18n:Dashboard_Utilization_Details_Trainer',
                    data: 'trainer'
                }
            ]
            this.columns.splice(1, 0, column[0], column[1])
        } else {
            this.bladeTitle(this.i18n("Dashboard_Utilization_Details_Vehicle")());
            let column = {
                type: 'text',
                title: 'i18n:Common_License',
                data: 'entityName'
            }
            this.columns.splice(1, 0, column)
        }
    }




    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

    }

    onDatasourceRequestRead(gridOption) {
        this.isBusy(true);
        var dfd = $.Deferred();
        var filter = Object.assign({}, this.filter(), gridOption);
        let dfdUtilizationDetails = this.webRequestTruckDriverUtilization.utilizationdetail(filter);


        $.when(dfdUtilizationDetails).done((respontDetails) => {
                dfd.resolve({
                    items: respontDetails["items"],
                    totalRecords: respontDetails["totalRecords"],
                    currentPage: respontDetails["currentPage"]
                });
            }).fail((e) => {
                this.handleError(e);
            })
            .always(() => {
                this.isBusy(false);
                this.onAutoRefresh(true);
            });

        return dfd;
    }

    onAutoRefresh(reCall) {
        if (reCall) {
            clearTimeout(this.timeOutHandler);
            this.onAutoRefresh();
            return;
        }

        if (this.isAutoRefresh()) {
            this.timeOutHandler = setTimeout(() => {
                this.refreshDetails();
                this.onAutoRefresh();
            }, WebConfig.appSettings.shipmentAutoRefreshTime);
        } else {
            clearTimeout(this.timeOutHandler);
        }
    }

    refreshDetails() {
        this.dispatchEvent("dgUtilizationDetail", "refresh");
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        this.isAutoRefresh(false);
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdExport", this.i18n("Common_Export")(), "svg-cmd-export"));
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdFilter":
                break;
            case "cmdRefresh":
                break;
            case "cmdExport":
                var dfdExportDetails = this.webRequestTruckDriverUtilization.utilizationexportdetail(this.filter());
                this.isBusy(true);
                $.when(dfdExportDetails).done(response => {
                    this.isBusy(false);
                    ScreenHelper.downloadFile(response.fileUrl);
                });
                break;
            default:
                break;
        }
    }

    get webRequestTruckDriverUtilization() {
        return WebRequestTruckDriverUtilization.getInstance();
    }

}


export default {
    viewModel: ScreenBase.createFactory(DetailsUtilization),
    template: templateMarkup
};
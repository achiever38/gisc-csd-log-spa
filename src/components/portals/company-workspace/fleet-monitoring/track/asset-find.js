﻿import ko from "knockout";
import templateMarkup from "text!./asset-find.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestCategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestCategory";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import { BusinessUnitFilter } from "../../businessUnitFilter";
import AssetInfo from "../../asset-info";

class TrackFindAssetScreen extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeTitle(this.i18n("Common_FindAssets")());

        this.keyword = ko.observable("");
        this.businessUnitId = ko.observable(null);
        this.entityType = ko.observable();
        this.vehicleType = ko.observable();
        this.vehicleTypeVisible = ko.pureComputed(() => {
            return this.entityType() && this.entityType().value === Enums.ModelData.SearchEntityType.Vehicle;
        });
       
        this.categoryVehicle = ko.observable();
        this.shippingType = ko.observable();

        this.businessUnitOptions = ko.observableArray([]);
        this.noneAccessibleBU = ko.observableArray([]);
        this.defaultFilter = new BusinessUnitFilter();
        // this.includeSubBusinessUnitEnable = ko.pureComputed(() => {return !_.isEmpty(this.businessUnitId());});
        this.includeSubBusinessUnitEnable = ko.observable(true)
        this.includeSubBusinessUnit = ko.observable(false);
        this.entityTypeOptions = ko.observableArray([]);
        this.vehicleTypeOptions = ko.observableArray([]);
        this.categoryVehicleOptions = ko.observableArray([]);
        this.shippingTypeOptions = ko.observableArray([]);

        this.categoryVehicleVisible = ko.pureComputed(() => {
            var isCategoryVehicleVisible = (Object.keys(this.categoryVehicleOptions()).length > 1) ? true : false ;
            return isCategoryVehicleVisible;
        });

        this.shippingTypeVisible = ko.pureComputed(() => {
            var isShippingTypeVisible = (Object.keys(this.shippingTypeOptions()).length > 1) ? true : false ;
            return isShippingTypeVisible;
        });

        this.refIncludeSubBusinessUnit = ko.pureComputed(() => {
            var result = {};
            result.title = (this.includeSubBusinessUnit()) ? "Yes" : "No";
            result.value = (this.includeSubBusinessUnit()) ? this.includeSubBusinessUnit() : false;
            return result;
        });

        //after find asset result then close blade
        this.subscribeMessage("wp-tracking-asset-result", (data) => { 
            this.close(true);
        });

        this.optionsCaption = ko.observable(this.i18n('Common_All')());

    }

    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    get webRequestCategory() {
        return WebRequestCategory.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();
       
        var d1 = this.webRequestBusinessUnit.listBusinessUnitSummary(this.defaultFilter).done((response) => {
                var bu = response["items"];
                this.noneAccessibleBU(AssetInfo.getNoneAccessibleBU(bu));
                this.businessUnitOptions(bu);
        });

        var d2 = this.webRequestEnumResource.listEnumResource({ 
            companyId: WebConfig.userSession.currentCompanyId, 
            types: [ Enums.ModelData.EnumResourceType.SearchEntityType, Enums.ModelData.EnumResourceType.VehicleType ], 
            sortingColumns: DefaultSorting.EnumResource })
            .done((response) => {
                _.forEach(response["items"], (item) => {
                    switch (item.type) {
                        case Enums.ModelData.EnumResourceType.SearchEntityType:
                            // display only Vehicle and Delivery Man
                            if (item.value === Enums.ModelData.SearchEntityType.Vehicle || item.value === Enums.ModelData.SearchEntityType.DeliveryMan) {
                                this.entityTypeOptions.push(item);
                            }
                            break;
                        case Enums.ModelData.EnumResourceType.VehicleType:
                            this.vehicleTypeOptions.push(item);
                            break;
                    }
                });
                this.vehicleTypeOptions.unshift({
                    displayName: this.i18n('Common_All')(),
                    id: 0
                });
                //set dufault selected vehicle
                var defaultEntityType = ScreenHelper.findOptionByProperty(this.entityTypeOptions, "value", Enums.ModelData.SearchEntityType.Vehicle);
                var defaultVehicleType = ScreenHelper.findOptionByProperty(this.vehicleTypeOptions, "id", 0);
                this.entityType(defaultEntityType);
                this.vehicleType(defaultVehicleType);
        });

        var filterCategoryVehicle = {
            categoryId:Enums.ModelData.CategoryType.Vehicle,
        };

        var filterShippingType = {
            categoryId:Enums.ModelData.CategoryType.ShippingType,
        };

        var d3 = this.webRequestCategory.listCategory(filterCategoryVehicle);
        var d4 = this.webRequestCategory.listCategory(filterShippingType);

        $.when(d1, d2, d3, d4).done((r1, r2, r3, r4) => {
            this._changeEntityTypeSubscribe = this.entityType.subscribe((entityType) => {
                this.vehicleType(null);
            });

           
            r3.items.unshift({
                name: this.i18n('Common_All')(),
                value: 0
            });

            r4.items.unshift({
                name: this.i18n('Common_All')(),
                value: 0
            });

            this.categoryVehicleOptions(r3.items);
            this.shippingTypeOptions(r4.items);


            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        

        return dfd;
    }

    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        this.entityType.extend({ required: true });

        this.validationModel = ko.validatedObservable({
            entityType: this.entityType
        });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actFind", this.i18n("Common_Find")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actFind") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            var filter = {
                keyword: this.keyword(),
                businessUnitIds: this.businessUnitId(),
                entityType: this.entityType().value,
                vehicleType: this.vehicleType() ? this.vehicleType().value : null,
                vehicleCategoryId: this.categoryVehicle().id,
                shippingTypeId: this.shippingType().id
            }

            // Default selection is single.
            // if(this.includeSubBusinessUnitEnable()){
            //     var selectedBusinessUnitId = parseInt(this.businessUnitId());

            //     // If user included sub children then return all business unit ids.
            //     if(this.includeSubBusinessUnit()){
            //         filter.businessUnitIds = ScreenHelper.findBusinessUnitsById(this.businessUnitOptions(), selectedBusinessUnitId);
            //     }
            //     else {
            //         // Single selection for business unit.
            //         filter.businessUnitIds = [selectedBusinessUnitId];
            //     }
            // }

            this.navigate("cw-fleet-monitoring-track-asset-find-result", { filter: filter });
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(TrackFindAssetScreen),
    template: templateMarkup
};
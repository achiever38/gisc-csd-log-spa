﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Enums, EntityAssociation, Constants } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestReportTemplate from "../../../../../app/frameworks/data/apitrackingcore/webrequestReportTemplate";
import WebRequestFleetMonitoring from "../../../../../app/frameworks/data/apitrackingcore/webRequestFleetMonitoring";
import Utility from "../../../../../app/frameworks/core/utility";
import * as EventAggregatorConstant from "../../../../../app/frameworks/constant/eventAggregator";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";
import Logger from "../../../../../app/frameworks/core/logger";
import IconFollowTemplateMarkup from "text!./../../../../svgs/icon-follow.html";
import AssetUtility from "../../asset-utility";
import WebRequestGlobalSearch from "../../../../../app/frameworks/data/apitrackingcore/webrequestGlobalSearch";
import QuickSearchScreenBase from "../../../quick-search-screenbase";

class TrackListScreen extends QuickSearchScreenBase {
    constructor(params) {
        super(params);

        var self = this;

        this.bladeTitle(this.i18n("FleetMonitoring_TrackMonitoring")());
        this.bladeSize = BladeSize.XLarge;
        this.bladeEnableQuickSearch = true;

        this.lastSync = ko.pureComputed(() => {
            return WebConfig.fleetMonitoring.trackLocationLastSync();
        });

        // Turn on or of automatic update to datatable when SignalR changed.
        this.automaticUpdate = true;
        this.columns = [];
        this.sortingColumns = [{},{}];

        this.keyword = ko.observable("");

        this.onRemoveTrackLocationClick = (data, e) => {
            Logger.info("onRemoveTrackLocationClick");
            var viewVehicleIds = WebConfig.fleetMonitoring.trackLocationViewVehicleIds().slice();
            var viewDeliveryManIds = WebConfig.fleetMonitoring.trackLocationViewDeliveryManIds().slice();
            var followVehicleIds = WebConfig.fleetMonitoring.trackLocationFollowVehicleIds().slice();
            var followDeliveryManIds = WebConfig.fleetMonitoring.trackLocationFollowDeliveryManIds().slice();
            var datasource = WebConfig.fleetMonitoring.trackLocationItems().slice();
            var list = datasource;
   
            if (data.vehicleId) {
                let vId = _.find(viewVehicleIds, function(o) { return o == data.vehicleId; });
                _.remove(viewVehicleIds, (o) => {
                    return o == data.vehicleId;
                });
                _.remove(followVehicleIds, (o) => {
                    return o == data.vehicleId;
                });
                //set new datasource
                if(!vId){
                    _.remove(datasource, function(o) {
                        return o.vehicleId == data.vehicleId;
                    });
                    list = datasource;
                }
            }
            else {
                let dId = _.find(viewDeliveryManIds, function(o) { return o == data.deliveryManId; });
                _.remove(viewDeliveryManIds, (o) => {
                    return o == data.deliveryManId;
                });
                _.remove(followDeliveryManIds, (o) => {
                    return o == data.deliveryManId;
                });
                //set new datasource
                if(!dId){
                    _.remove(datasource, function(o) {
                        return o.deliveryManId == data.deliveryManId;
                    });
                    list = datasource;
                }
            }

            WebConfig.fleetMonitoring.trackLocationItems(list);

            this._updateTrackLocationAssetIds(
                viewVehicleIds,
                viewDeliveryManIds,
                followVehicleIds,
                followDeliveryManIds
            );

        };
        this.onRemoveAllTrackLocationClick = (items, e) => {
            Logger.info("onRemoveAllTrackLocationClick");
            if(!_.isEmpty(items) && _.isArray(items)){
                var viewVehicleIds = WebConfig.fleetMonitoring.trackLocationViewVehicleIds().slice();
                var viewDeliveryManIds = WebConfig.fleetMonitoring.trackLocationViewDeliveryManIds().slice();
                var followVehicleIds = WebConfig.fleetMonitoring.trackLocationFollowVehicleIds().slice();
                var followDeliveryManIds = WebConfig.fleetMonitoring.trackLocationFollowDeliveryManIds().slice();
                var datasource = WebConfig.fleetMonitoring.trackLocationItems().slice();
                var list = datasource;
                 _.forEach(items, (item) => {
                   var target = _.find(datasource, function(o) { return o.id == item.id; });
                   if(target){
                       if(target.vehicleId){
                            let vId = _.find(viewVehicleIds, function(o) { return o == target.vehicleId; });
                            _.remove(viewVehicleIds, (o) => {
                                return o === target.vehicleId;
                            });
                            _.remove(followVehicleIds, (o) => {
                                return o == target.deliveryManId;
                            });
                            //set new datasource
                            if(!vId){
                                _.remove(datasource, function(o) {
                                    return o.vehicleId == target.vehicleId;
                                });
                                list = datasource;
                            }
                       }
                       else {
                            let dId = _.find(viewDeliveryManIds, function(o) { return o == target.deliveryManId; });
                             _.remove(viewDeliveryManIds, (o) => {
                                return o === target.deliveryManId;
                            });
                            _.remove(followDeliveryManIds, (o) => {
                                return o == target.deliveryManId;
                            });
                            //set new datasource
                            if(!dId){
                                _.remove(datasource, function(o) {
                                    return o.deliveryManId == target.deliveryManId;
                                });
                                list = datasource;
                            }
                        }
                   }
                });

                WebConfig.fleetMonitoring.trackLocationItems(list);

                 this._updateTrackLocationAssetIds(
                    viewVehicleIds,
                    viewDeliveryManIds,
                    followVehicleIds,
                    followDeliveryManIds
                );
            }
        };
        this.onFollowCheckboxDataChanged = (items, e) => {
            Logger.info("onFollowCheckboxDataChanged");
            if(!_.isEmpty(items) && _.isArray(items)){
                var followVehicleIds = WebConfig.fleetMonitoring.trackLocationFollowVehicleIds().slice();
                var followDeliveryManIds = WebConfig.fleetMonitoring.trackLocationFollowDeliveryManIds().slice();
                var datasource = WebConfig.fleetMonitoring.trackLocationItems().slice();

                _.forEach(items, (item) => {
                   var target = _.find(datasource, function(o) { return o.id == item.id; });
                   if(target){
                       if(target.vehicleId){
                           if (item.value) {
                                followVehicleIds.push(target.vehicleId);
                            }
                            else {
                                _.remove(followVehicleIds, (o) => {
                                    return o === target.vehicleId;
                                });
                            }
                       }
                       else {
                            if (item.value) {
                                followDeliveryManIds.push(target.deliveryManId);
                            }
                            else {
                                _.remove(followDeliveryManIds, (o) => {
                                    return o === target.deliveryManId;
                                });
                            }
                        }
                   }
                });

                this._updateTrackLocationAssetIds(
                    WebConfig.fleetMonitoring.trackLocationViewVehicleIds().slice(),
                    WebConfig.fleetMonitoring.trackLocationViewDeliveryManIds().slice(),
                    followVehicleIds.slice(),
                    followDeliveryManIds.slice()
                );
            }
        };
        this.onSelectingRow = (trackLocation, e) => {
            Logger.info("onSelectingRow");
            if(trackLocation) {
                // เปิด Widget สำหรับ tracking.
                // Open widget with target for unique open once.
                self.widget("cw-fleet-monitoring-widget", null, {
                    id: 'trackingWidgetMonitoring',
                    title: this.i18n('FleetMonitoring_TrackMonitoring')(),
                    modal: false,
                    resizable: true,
                    target: "cw-fleet-monitoring-widget",
                    customActions: ["all", "delete"],
                    width: "95%"
                });

                //แผนที่จะ pan ไปยังจุดของยานพาหนะ หรือ คนส่งของ
                self.mapManager.zoomPoint(self.id, trackLocation.latitude, trackLocation.longitude, WebConfig.mapSettings.defaultZoom);
                self.minimizeAll();
            }
        };

        // Subscribe when track location items changed
        this._trackLocationItemsRef = WebConfig.fleetMonitoring.trackLocationItems.ignorePokeSubscribe(() => {
            if(this.automaticUpdate && this.isActive()) {
                Logger.log("Update track location by signalR");
                this._syncTrackLocation();
            }
        });

        // Map
        this._eventAggregator.subscribe(EventAggregatorConstant.MAP_COMMAND_COMPLETE, this.onMapComplete.bind(this));
    
        //after find asset result then minimizeAll blade
        this.subscribeMessage("wp-tracking-asset-result", (data) => { 
            this.minimizeAll();
        });
    
    }
    
    /**
     * 
     * Get MapManager
     * @readonly
     */
    get mapManager() {
        return MapManager.getInstance();
    }

    /**
     * Get WebRequest specific for ReportTemplate module in Web API access.
     * @readonly
     */
    get webRequestReportTemplate() {
        return WebRequestReportTemplate.getInstance();
    }

    /**
     * Get WebRequest specific for FleetMonitoring module in Web API access.
     * @readonly
     */
    get webRequestFleetMonitoring() {
        return WebRequestFleetMonitoring.getInstance();
    }

    /**
     * Get WebRequest specific for GlobalSearch module in Web API access.
     * @readonly
     */
    get webRequestGlobalSearch() {
        return WebRequestGlobalSearch.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // Always enable track for both restore, new blade instance.
        MapManager.getInstance().setFollowTracking(true);

        if (!isFirstLoad) {
            return;
        }

        this.minimizeAll(false);

        var dfd = $.Deferred();
        var items = WebConfig.fleetMonitoring.trackLocationItems().slice();

        var dfdReportTemplate = this.getReportTemplate();
        var dfdTrackLocation = (!_.size(items)) ? this.liatTracklocation() : null;

        $.when(dfdReportTemplate, dfdTrackLocation).done(() => {
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdFindAssets", this.i18n("Common_FindAssets")(), "svg-cmd-search"));
        commands.push(this.createCommand("cmdExport", this.i18n("Common_Export")(), "svg-cmd-export"));
        commands.push(this.createCommand("cmdRefresh", this.i18n("Common_Refresh")(), "svg-cmd-refresh"));
        commands.push(this.createCommand("cmdViewAll", this.i18n("FleetMonitoring_ViewAll")(), "svg-cmd-view-all"));
        commands.push(this.createCommand("cmdClearAll", this.i18n("Common_ClearAll")(), "svg-cmd-clear-all"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdFindAssets":
                this.quickSearchKeyword("");
                this.navigate("cw-fleet-monitoring-track-asset-find");
                break;
            case "cmdExport":
                this.showMessageBox(null, this.i18n("M128")(), BladeDialog.DIALOG_YESNO).done((button)=> {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);

                            //console.log("cmdExport", this.sortingColumns);
                            var filter = {
                                companyId: WebConfig.userSession.currentCompanyId, 
                                vehicleIds: WebConfig.fleetMonitoring.trackLocationViewVehicleIds(), 
                                deliveryManIds: WebConfig.fleetMonitoring.trackLocationViewDeliveryManIds(),
                                templateFileType: Utility.isiOSDevice() ? Enums.TemplateFileType.Xls : Enums.TemplateFileType.Xlsx,
                                sortingColumns: this.sortingColumns
                            };

                            this.webRequestFleetMonitoring.exportTrackLocation(filter).done((response) => {
                                this.isBusy(false);
                                ScreenHelper.downloadFile(response.fileUrl);
                            }).fail((e) => {
                                this.isBusy(false);
                                this.handleError(e);
                            });
                            break;
                    }
                });
                break;
            case "cmdRefresh":
                this.isBusy(true);
                this.quickSearchKeyword(this.keyword());
                this.refreshTrackLocation().done(() => {
                    this.isBusy(false);
                }).fail((e) => {
                    this.isBusy(false);
                    this.handleError(e);
                });
                break;
            case "cmdViewAll":
                this.isBusy(true);
                this.viewAllTrackLocation().done(() => {
                    this.isBusy(false);
                }).fail((e) => {
                    this.isBusy(false);
                    this.handleError(e);
                });
                break;
            case "cmdClearAll":
                this.isBusy(true);
                this.removeAllTrackLocation().done(() => {
                    this.isBusy(false);
                }).fail((e) => {
                    this.isBusy(false);
                    this.handleError(e);
                });
                break;
        }
    }

    /**
     * @lifecycle web request global search autocomplete.
     * @param {any} data
     * @param {any} event
     */
    onQuickSearchClick(data, event) {
        let keyword = this.quickSearchKeyword();
        if( 
            (event.keyCode === 13 || event.type === 'click') && 
            (_.size(keyword) >= data.minLength || _.size(keyword) === 0) 
        ){
            this.keyword(keyword);
            this.isBusy(true);
            this.searchTrackLocation().done(() => {
                this.isBusy(false);
            }).fail((e) => {
                this.isBusy(false);
                this.handleError(e);
            });
        }
        return true;
    }

    /**
     * @lifecycle web request global search autocomplete.
     * @param {any} request
     * @param {promise} response
     */
    onDataSourceQuickSearch(request, response) {
        var filter = {
            companyId: WebConfig.userSession.currentCompanyId,
            keyword: request.term,
        }
        this.webRequestGlobalSearch.autocompleteDriverVehicle(filter).done((data) => {
            response( $.map( data, (item) => {
                return {
                    label: item,
                    value: '"' + item + '"'
                };
            }));
        });      
    }
    
    /**
     * On Datasource Request Read
     * 
     * @param {any} res
     * @returns
     * 
     * @memberOf TrackWidget
     */
    onDatasourceRequestRead (gridOption) {
        var dfd = $.Deferred();
        var items = WebConfig.fleetMonitoring.trackLocationItems().slice();

        //console.log("onDatasourceRequestRead: ", items);
        var totalItems = items.length;

        if(_.isEmpty(gridOption.sort)){
            gridOption.sort = [{field: "dateTime", dir: "desc" }];
        }
        
        this.sortingColumns = [{},{}];this.sortingColumns = [{},{}];
        this.sortingColumns[0].column = Enums.SortingColumnName.DateTime;//Default
        this.sortingColumns[0].direction = gridOption.sort[0].dir === "desc" ? Enums.SortingDirection.Descending : Enums.SortingDirection.Ascending
        this.sortingColumns[1].column = Enums.SortingColumnName.Id;
        this.sortingColumns[1].direction = this.sortingColumns[0].direction;
        var columnDefinition = _.find(this.columns, {data: gridOption.sort[0].field});
        // Required map back from grid to web service with enum id.
        if(columnDefinition){  
            this.sortingColumns[0].column = columnDefinition.enumColumnName;
            gridOption.sort[0].field = columnDefinition.sortData;
        } 

        //console.log("gridOption.sort: ", gridOption.sort);
        var data = AssetUtility.query(items, gridOption.take, gridOption.skip, gridOption.sort);

        let currPage = AssetUtility.currentPage(items, gridOption.page, gridOption.pageSize);

        dfd.resolve({items: data, totalRecords: totalItems, currentPage: currPage});

        return dfd;
    }

    /**
     * Hook on map complete
     * @param {any} data
     */
    onMapComplete(data) {
        //
    }

    /**
     * Refresh track location manually
     */
    searchTrackLocation() {
        let filter = {
            keyword: this.keyword(),
            displayLength: 10000
        }

        return WebConfig.fleetMonitoring.syncTrackLocation(this.webRequestFleetMonitoring, WebConfig.userSession, filter).done(() => {
            if(!this.automaticUpdate) {
                Logger.log("Update track location by refresh manually.");
                this._syncTrackLocation();
            }
        });
    }

    /**
     * Refresh track location manually
     */
    refreshTrackLocation() {
        let filter = {
            keyword: this.keyword()
        }
        return WebConfig.fleetMonitoring.syncTrackLocation(this.webRequestFleetMonitoring, WebConfig.userSession, filter).done(() => {
            if(!this.automaticUpdate) {
                Logger.log("Update track location by refresh manually.");
                this._syncTrackLocation();
            }
        });
    }

    /**
     * Get report template fields
     */
    getReportTemplate() {
        return this.webRequestReportTemplate.listReportTemplate({ 
            reportTemplateType: Enums.ModelData.ReportTemplateType.TrackMonitoring, 
            includeAssociationNames: [ EntityAssociation.ReportTemplate.ReportTemplateFields, EntityAssociation.ReportTemplateField.Feature ] 
        }).done((response) => {

            var reportTemplateFields = response.items["0"].reportTemplateFields;
            var widthIcon = '45px';
            var columns = AssetUtility.getColumnDefinitions(reportTemplateFields, false, 2, Enums.SortingColumnName.VehicleLicense);
            var headerTemplateIcon = AssetUtility.headerTemplateIcon();

            //find column location to set custom render method
            var locationIndex = _.findIndex(columns, function(o) { return o.data === 'location'; });
            columns.unshift({
                type: 'checkbox',
                title: '',
                headerTemplate: headerTemplateIcon({ 'iconCssClass': 'icon-follow', 'title': this.i18n("FleetMonitoring_Follow")(), 'icon': null }),
                data: 'isFollow',
                width: widthIcon,
                columnMenu: false,
                columnMenuItem: false,
                sortable: false,
                resizable: false,
                enableSelectAll: true,
                onDataChanged: this.onFollowCheckboxDataChanged
            });

            columns.unshift({
                type: 'object',
                title: '',
                data: 'id',
                headerTemplate: AssetUtility.renderRemoveColumn,
                onHeaderClick: this.onRemoveAllTrackLocationClick,
                template: AssetUtility.renderRemoveColumn,
                onClick: this.onRemoveTrackLocationClick,
                width: widthIcon,
                columnMenu: false,
                columnMenuItem: false,
                sortable: false,
                resizable: false
            });

            this.columns = columns;
        });
    }

    /**
     * View all track location
     */
    viewAllTrackLocation(){
        var dfd = $.Deferred();
        // Calling service for get all Vehicles
        this.webRequestFleetMonitoring.listAsset({
            companyId: WebConfig.userSession.currentCompanyId,
            businessUnitIds: [],
            searchEntityType: Enums.ModelData.SearchEntityType.Vehicle
        }).done((response) => {
            
            var vehicleViewIdsAdded = [];
            _.forEach(response, (item) => {
                _.forEach(item.assets, (asset) => {
                    vehicleViewIdsAdded.push(asset.vehicleId);
                });
            });

            var currentVehicleViewIds = WebConfig.fleetMonitoring.trackLocationViewVehicleIds().slice();
            var currentVehicleFollowIds = WebConfig.fleetMonitoring.trackLocationFollowVehicleIds().slice();
            var currentDeliveryManViewIds = WebConfig.fleetMonitoring.trackLocationViewDeliveryManIds().slice();
            var currentDeliveryManFollowIds = WebConfig.fleetMonitoring.trackLocationFollowDeliveryManIds().slice();

            var effectiveVehicleViewIds = [];

            effectiveVehicleViewIds = _.union(currentVehicleViewIds, vehicleViewIdsAdded);

            // Update fleet monitoring global items.
            WebConfig.fleetMonitoring.updateTrackLocationAssetIds(
                this.webRequestFleetMonitoring,
                WebConfig.userSession,
                effectiveVehicleViewIds,
                currentDeliveryManViewIds,
                currentVehicleFollowIds,
                currentDeliveryManFollowIds
            )
            .done(() => {
                dfd.resolve();
            })
            .fail((e) => {
                dfd.reject(e);
            });

        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Remove all track location
     */
    removeAllTrackLocation(){
        var dfd = $.Deferred();
        this.isBusy(true);
        var viewVehicleIds = WebConfig.fleetMonitoring.trackLocationViewVehicleIds().slice();
        var viewDeliveryManIds = WebConfig.fleetMonitoring.trackLocationViewDeliveryManIds().slice();
        var followVehicleIds = WebConfig.fleetMonitoring.trackLocationFollowVehicleIds().slice();
        var followDeliveryManIds = WebConfig.fleetMonitoring.trackLocationFollowDeliveryManIds().slice();
        // Calling service for get all Vehicles
        this.webRequestFleetMonitoring.listTrackLocation({
            companyId: WebConfig.userSession.currentCompanyId,
            vehicleIds: viewVehicleIds
        }).done((response) => {
            if(_.size(response.items)){
                _.forEach(response.items, (item) => {
                    if(_.size(item)){
                        if(item.vehicleId){
                            _.pull(viewVehicleIds, item.vehicleId);
                            _.pull(followVehicleIds, item.vehicleId);
                        }
                        else 
                        {
                            _.pull(viewDeliveryManIds, item.deliveryManId);
                            _.pull(followDeliveryManIds, item.deliveryManId);
                        
                        }
                    }
                });

                this._updateTrackLocationAssetIds(
                    viewVehicleIds,
                    viewDeliveryManIds,
                    followVehicleIds,
                    followDeliveryManIds
                );
                
            }
            this.isBusy(false);
            dfd.resolve();
        }).done(() => {
            dfd.resolve();
        })
        .fail((e) => {
            dfd.reject(e);
        })
        .always(()=> {
            this.isBusy(false);
        });
        return dfd;
    };

    /**
     * Sync track location
     */
    _syncTrackLocation() {
        this.dispatchEvent("trackMonitoringList", "refresh");
    }

    /**
     * Update Track Location Asset Ids
     */
    _updateTrackLocationAssetIds(viewVehicleIds, viewDeliveryManIds, followVehicleIds, followDeliveryManIds) {
        return WebConfig.fleetMonitoring.updateTrackLocationAssetIds(
            this.webRequestFleetMonitoring,
            WebConfig.userSession,
            viewVehicleIds,
            viewDeliveryManIds,
            followVehicleIds,
            followDeliveryManIds
        ).fail((e) => {
            this.handleError(e);
        }).always(() => {
            this.isBusy(false);
        });
    }

    liatTracklocation(){
        var dfd = $.Deferred();
        WebConfig.fleetMonitoring.companyId = WebConfig.userSession.currentCompanyId;
        if (WebConfig.userSession.hasPermission(Constants.Permission.TrackMonitoring)) {
            // Load from server for first 20 records follow requirement.
            var filter = {
                    displayStart: 0,
                    displayLength: 20
            };

            var usingRestoredState = (WebConfig.userSession.assetIdsForFleetMonitoring !== null);
            if(usingRestoredState){
                filter = {
                    vehicleIds: WebConfig.userSession.assetIdsForFleetMonitoring.viewVehicleIds,
                    deliveryManIds: WebConfig.userSession.assetIdsForFleetMonitoring.viewDeliveryManIds
                };

                // Calculate display length from total asset ids (cannot be zero).
                filter.displayLength = filter.vehicleIds.length + filter.deliveryManIds.length;
            }

            // Call web servics for track locations.
            WebConfig.fleetMonitoring.syncTrackLocation(
                WebRequestFleetMonitoring.getInstance(),
                WebConfig.userSession,
                filter)
            .done((result) => {
                dfd.resolve();
            }).fail((error) => {
                dfd.reject();
            });
        }else{
            dfd.resolve();
        }
        return dfd;
    }

}

export default {
    viewModel: ScreenBase.createFactory(TrackListScreen),
    template: templateMarkup
};
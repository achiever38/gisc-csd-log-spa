﻿import ko from "knockout";
import templateMarkup from "text!./asset-result.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestFleetMonitoring from "../../../../../app/frameworks/data/apitrackingcore/webRequestFleetMonitoring";
import Utility from "../../../../../app/frameworks/core/utility";
import {
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";

class TrackFindAssetResultScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("FleetMonitoring_FindAssetResult")());
        this.bladeSize = BladeSize.Large;

        this.filter = this.ensureNonObservable(params.filter);

        this.results = ko.observableArray([]);
        this.dtIds = [];
        this.enableApplyAction = ko.observable(true);

        this.entityIsVehicle = this.filter.entityType === Enums.ModelData.SearchEntityType.Vehicle;
        this.entityIsDeliveryMan = this.filter.entityType === Enums.ModelData.SearchEntityType.DeliveryMan;

        this.vehicleViewIds = ko.observableArray([]);
        this.vehicleFollowIds = ko.observableArray([]);
        this.deliveryManViewIds = ko.observableArray([]);
        this.deliveryManFollowIds = ko.observableArray([]);

        // This block will used to keep loaded item checked status.
        this.vehicleViewIdsOriginal = [];
        this.vehicleFollowIdsOriginal = [];
        this.deliveryManViewIdsOriginal = [];
        this.deliveryManFollowIdsOriginal = [];

        this.followCheckboxData = ko.observableArray([]);
        this.viewCheckboxData = ko.observableArray([]);

        // this.assetsChanged = (newValue, targetField, extra = null) => { 
        //     var refreshColumnIndex = [];

        //     if (targetField === "isFollow") {
        //         refreshColumnIndex.push(0);
        //     }
        //     if (targetField === "isView") {
        //         refreshColumnIndex.push(1);
        //     }

        //     if (extra) {
        //         _.forEach(extra.datasource, (item) => {
        //             // if Follow is checked, View need to be checked.
        //             if (targetField === "isFollow" && extra.isMasterChecked) {
        //                 item.isView = true;
        //             }
        //             // if View is unchecked, Follow need to be unchecked.
        //             if (targetField === "isView" && !extra.isMasterChecked) {
        //                 item.isFollow = false;
        //             }
        //         });  
        //     }
        //     else
        //     {
        //         // if Follow is checked, View need to be checked.
        //         if (targetField === "isFollow" && newValue.isFollow) {
        //             newValue.isView = true;
        //         }
        //         // if View is unchecked, Follow need to be unchecked.
        //         if (targetField === "isView" && !newValue.isView) {
        //             newValue.isFollow = false;
        //         }
        //     }

        //     var dfd = $.Deferred();
        //     dfd.resolve({ refreshColumnIndex: refreshColumnIndex });
        //     return dfd;
        // };

        this._resultsSubscribe = this.results.subscribe(() => {
            this.enableApplyAction(this.results() && this.results().length > 0);
        });
    }

    /**
     * Get WebRequest specific for FleetMonitoring module in Web API access.
     * @readonly
     */
    get webRequestFleetMonitoring() {
        return WebRequestFleetMonitoring.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();
        var vehicleViewIds = WebConfig.fleetMonitoring.trackLocationViewVehicleIds().slice();
        var vehicleFollowIds = WebConfig.fleetMonitoring.trackLocationFollowVehicleIds().slice();
        var deliveryManViewIds = WebConfig.fleetMonitoring.trackLocationViewDeliveryManIds().slice();
        var deliveryManFollowIds = WebConfig.fleetMonitoring.trackLocationFollowDeliveryManIds().slice();

        // Calling service for search asset by criteria.
        this.webRequestFleetMonitoring.listAsset({
            companyId: WebConfig.userSession.currentCompanyId,
            keyword: this.filter.keyword,
            businessUnitIds: this.filter.businessUnitIds ? this.filter.businessUnitIds : [],
            searchEntityType: this.filter.entityType,
            vehicleType: this.filter.vehicleType,
            vendorId: this.filter.vendorId,
            vehicleCategoryId: this.filter.vehicleCategoryId,
            shippingTypeId: this.filter.shippingTypeId

        }).done((response) => {
            // set isView, isFollow (from trackLocationItems) to asset result
            _.forEach(response, (item) => {
                _.forEach(item.assets, (asset) => {
                    if (this.entityIsVehicle) {
                        // Determine checked state from global tracking session.
                        asset.isView = _.indexOf(vehicleViewIds, asset.vehicleId) > -1;
                        asset.isFollow = _.indexOf(vehicleFollowIds, asset.vehicleId) > -1;
                    }
                    if (this.entityIsDeliveryMan) {
                        // Determine checked state from global tracking session.
                        asset.isView = _.indexOf(deliveryManViewIds, asset.deliveryManId) > -1;
                        asset.isFollow = _.indexOf(deliveryManFollowIds, asset.deliveryManId) > -1;
                    }
                });
            });

            var titleHeader = {
                titleFleetMonitoringView: this.i18n("FleetMonitoring_View")(),
                titleFleetMonitoringFollow: this.i18n("FleetMonitoring_Follow")()
            }

            // Map original response to UI render object, group by business UNIT.
            var results = _.map(response, (item, index) => {
                var result = new FindAssetsResultItem(item, this.filter.entityType, index, titleHeader);
                this.dtIds.push(result.dtId);
                _.forEach(item.assets, (obj) => {
                    if (this.entityIsVehicle) {
                        if (obj.isFollow) {
                            this.followCheckboxData.push(obj.vehicleId);
                        }
                        if (obj.isView) {
                            this.viewCheckboxData.push(obj.vehicleId);
                        }
                    } else if (this.entityIsDeliveryMan) {

                        if (obj.isFollow) {
                            this.followCheckboxData.push(obj.deliveryManId);
                        }
                        if (obj.isView) {
                            this.viewCheckboxData.push(obj.deliveryManId);
                        }
                    }

                });

                return result;
            });

            // Update search result.
            this.results(results);

            _.forEach(this.results(), (data) => {
                _.forEach(data.customColumns(), (item) => {
                    if (item.data == "isView") {
                        item.onDataChanged = this.onViewCheckboxDataChanged.bind(this);
                    }
                    if (item.data == "isFollow") {
                        item.onDataChanged = this.onFollowCheckboxDataChanged.bind(this);
                    }
                });
            });

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Setup trackChange/validation
     */
    setupExtend() {}

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actApply", this.i18n("Common_Apply")(), "default", true, this.enableApplyAction));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdViewAll", this.i18n("FleetMonitoring_ViewAll")(), "svg-cmd-view-all"));
        commands.push(this.createCommand("cmdClearAll", this.i18n("Common_ClearAll")(), "svg-cmd-clear-all"));
        commands.push(this.createCommand("cmdFollowAl", this.i18n("FleetMonitoring_FollowAll")(), "svg-cmd-follow-all"));
        commands.push(this.createCommand("cmdClearFollow", this.i18n("FleetMonitoring_ClearFollow")(), "svg-cmd-clear-follow"));
    }

    /**
     * Calculate tracking state of current list screen.
     * We will use them for compare change.
     * 
     * @memberof TrackFindAssetResultScreen
     */
    calculateTrackingOriginalState() {
        var currentVehicleViewIds = WebConfig.fleetMonitoring.trackLocationViewVehicleIds();
        var currentVehicleFollowIds = WebConfig.fleetMonitoring.trackLocationFollowVehicleIds();
        var currentDeliveryManViewIds = WebConfig.fleetMonitoring.trackLocationViewDeliveryManIds();
        var currentDeliveryManFollowIds = WebConfig.fleetMonitoring.trackLocationFollowDeliveryManIds();

        // Reset all existing state.
        this.vehicleViewIdsOriginal = [];
        this.vehicleFollowIdsOriginal = [];
        this.deliveryManViewIdsOriginal = [];
        this.deliveryManFollowIdsOriginal = [];

        _.forEach(this.results(), (itemGroup) => {
            _.forEach(itemGroup.assets(), (asset) => {
                if (this.entityIsVehicle) {
                    // Determine checked state from global tracking session.
                    let vehicleId = asset.vehicleId;
                    let isView = _.indexOf(currentVehicleViewIds, vehicleId) > -1;
                    let isFollow = _.indexOf(currentVehicleFollowIds, vehicleId) > -1;

                    // Store original state of vehicles.
                    if (isView) {
                        this.vehicleViewIdsOriginal.push(vehicleId);
                    }
                    if (isFollow) {
                        this.vehicleFollowIdsOriginal.push(vehicleId);
                    }
                }
                if (this.entityIsDeliveryMan) {
                    // Determine checked state from global tracking session.
                    let deliveryManId = asset.deliveryManId;
                    let isView = _.indexOf(currentDeliveryManViewIds, deliveryManId) > -1;
                    let isFollow = _.indexOf(currentDeliveryManFollowIds, deliveryManId) > -1;
                    if (isView) {
                        this.deliveryManViewIdsOriginal.push(deliveryManId);
                    }
                    if (isFollow) {
                        this.deliveryManFollowIdsOriginal.push(deliveryManId);
                    }
                }
            });
        });
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actApply") {

            this.isBusy(true);

            // Calculate current tracking items from global variable.
            // We need to calculate original every click apply button because.
            // User may remove track item from list screen.
            this.calculateTrackingOriginalState();

            // Initialize changes of current tracking screen.
            var vehicleViewIds = [];
            var vehicleFollowIds = [];
            var deliveryManViewIds = [];
            var deliveryManFollowIds = [];

            //set data for logic below
            _.map(this.results(), (data) => {
                _.forEach(data.assets(), (item) => {
                    var fId = null;
                    var vId = null;
                    if (this.entityIsVehicle) {
                        fId = _.filter(this.followCheckboxData(), (id) => {
                            return id == item.vehicleId;
                        });
                        vId = _.filter(this.viewCheckboxData(), (id) => {
                            return id == item.vehicleId;
                        });
                    } else if (this.entityIsDeliveryMan) {
                        fId = _.filter(this.followCheckboxData(), (id) => {
                            return id == item.deliveryManId;
                        });
                        vId = _.filter(this.viewCheckboxData(), (id) => {
                            return id == item.deliveryManId;
                        });
                    }
                    let fChecked = false;
                    let vChecked = false;
                    if ((_.size(fId))) {
                        fChecked = true;
                    }
                    if ((_.size(vId))) {
                        vChecked = true;
                    }
                    item.isFollow = fChecked;
                    item.isView = vChecked;
                });
                return data;
            });

            _.forEach(this.results(), (item) => {
                //find view and follow for each BU
                var _vehicleViewIds = [];
                var _vehicleFollowIds = [];
                var _deliveryManViewIds = [];
                var _deliveryManFollowIds = [];

                if (this.entityIsVehicle) {

                    _vehicleViewIds = _.map(_.filter(item.assets(), function (o) {
                        return o.isView === true;
                    }), "vehicleId")

                    _vehicleFollowIds = _.map(_.filter(item.assets(), function (o) {
                        return o.isFollow === true;
                    }), "vehicleId");
                } else if (this.entityIsDeliveryMan) {
                    _deliveryManViewIds = _.map(_.filter(item.assets(), function (o) {
                        return o.isView === true;
                    }), "deliveryManId");

                    _deliveryManFollowIds = _.map(_.filter(item.assets(), function (o) {
                        return o.isFollow === true;
                    }), "deliveryManId");
                }

                //merge selected of current BU with other BU
                vehicleViewIds = vehicleViewIds.concat(_vehicleViewIds);
                vehicleFollowIds = vehicleFollowIds.concat(_vehicleFollowIds);
                deliveryManViewIds = deliveryManViewIds.concat(_deliveryManViewIds);
                deliveryManFollowIds = deliveryManFollowIds.concat(_deliveryManFollowIds);
            });

            // Merging Step 1: Compare changes for added, removed items.
            // We create shared function for extract two arrays.
            // The source array will be filtered by second array.
            var extractArray = (sourceArray = [], filterArray = []) => {
                if (!sourceArray.length || !filterArray.length) {
                    // Source array will be empty or original value.
                    return sourceArray;
                }

                // append soruce array for first parameter.
                var withoutParameters = _.concat([sourceArray], filterArray);

                // Return new array which filtered.
                var result = _.without.apply(_, withoutParameters);

                return result;
            };

            // Step 2: Merge changes from user to fleet session items.
            var effectiveVehicleViewIds = [];
            var effectiveVehicleFollowIds = [];
            var effectiveDeliveryManViewIds = [];
            var effectiveDeliveryManFollowIds = [];
            var currentVehicleViewIds = WebConfig.fleetMonitoring.trackLocationViewVehicleIds();
            var currentVehicleFollowIds = WebConfig.fleetMonitoring.trackLocationFollowVehicleIds();
            var currentDeliveryManViewIds = WebConfig.fleetMonitoring.trackLocationViewDeliveryManIds();
            var currentDeliveryManFollowIds = WebConfig.fleetMonitoring.trackLocationFollowDeliveryManIds();
            var isSelectionChanged = false;

            if (this.entityIsVehicle) {
                // Detect vehicle changes.
                var vehicleViewIdsAdded = extractArray(vehicleViewIds, this.vehicleViewIdsOriginal);
                var vehicleViewIdsDeleted = extractArray(this.vehicleViewIdsOriginal, vehicleViewIds);
                var vehicleFollowIdsAdded = extractArray(vehicleFollowIds, this.vehicleFollowIdsOriginal);
                var vehicleFollowIdsDeleted = extractArray(this.vehicleFollowIdsOriginal, vehicleFollowIds);
                isSelectionChanged = (vehicleViewIdsAdded.length || vehicleViewIdsDeleted.length || vehicleFollowIdsAdded.length || vehicleFollowIdsDeleted.length);

                // Apply user changes to current session.
                effectiveVehicleViewIds = _.union(extractArray(currentVehicleViewIds, vehicleViewIdsDeleted), vehicleViewIdsAdded);
                effectiveVehicleFollowIds = _.union(extractArray(currentVehicleFollowIds, vehicleFollowIdsDeleted), vehicleFollowIdsAdded);

                // if entity is vehicle, get deliverymanIds from trackloc to prevent remove all.
                effectiveDeliveryManViewIds = currentDeliveryManViewIds;
                effectiveDeliveryManFollowIds = currentDeliveryManFollowIds;
            } else if (this.entityIsDeliveryMan) {
                // Detect deliveryman changes.
                var deliveryManViewIdsAdded = extractArray(deliveryManViewIds, this.deliveryManViewIdsOriginal);
                var deliveryManViewIdsDeleted = extractArray(this.deliveryManViewIdsOriginal, deliveryManViewIds);
                var deliveryManFollowIdsAdded = extractArray(deliveryManFollowIds, this.deliveryManFollowIdsOriginal);
                var deliveryManFollowIdsDeleted = extractArray(this.deliveryManFollowIdsOriginal, deliveryManFollowIds);
                isSelectionChanged = (deliveryManViewIdsAdded.length || deliveryManViewIdsDeleted.length || deliveryManFollowIdsAdded.length || deliveryManFollowIdsDeleted.length);

                // Apply user changes to current session.
                effectiveDeliveryManViewIds = _.union(extractArray(currentDeliveryManViewIds, deliveryManViewIdsDeleted), deliveryManViewIdsAdded);
                effectiveDeliveryManFollowIds = _.union(extractArray(currentDeliveryManFollowIds, deliveryManFollowIdsDeleted), deliveryManFollowIdsAdded);

                // if entity is deliveryman, get vehicleIds from trackloc to prevent remove all.
                effectiveVehicleViewIds = currentVehicleViewIds;
                effectiveVehicleFollowIds = currentVehicleFollowIds;
            }

            // Step 3: Update fleet monitoring page.
            if (!isSelectionChanged) {
                // If nochange just collapse the blade.
                this.isBusy(false);
                // this.minimizeAll();
                this.close(true);
            } else {
                // If user change selection then update fleet monitoring global items.
                WebConfig.fleetMonitoring.updateTrackLocationAssetIds(
                        this.webRequestFleetMonitoring,
                        WebConfig.userSession,
                        effectiveVehicleViewIds,
                        effectiveDeliveryManViewIds,
                        effectiveVehicleFollowIds,
                        effectiveDeliveryManFollowIds
                    )
                    .done(() => {
                        // this.minimizeAll();
                        this.close(true);
                    })
                    .fail((e) => {
                        this.handleError(e);
                    })
                    .always(() => {
                        this.isBusy(false);
                    });
            }

            this.publishMessage("wp-tracking-asset-result", null);

        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (_.isEmpty(this.dtIds) == false) {
            switch (sender.id) {
                case "cmdViewAll":
                    _.forEach(this.dtIds, (dtId) => {
                        this.dispatchEvent(dtId, "updateCheckboxSelectAll", {
                            columnIndex: 0,
                            checked: false
                        });
                    });
                    break;
                case "cmdClearAll":
                    _.forEach(this.dtIds, (dtId) => {
                        this.dispatchEvent(dtId, "updateCheckboxSelectAll", {
                            columnIndex: 0,
                            checked: true
                        });
                    });
                    break;
                case "cmdFollowAl":
                    _.forEach(this.dtIds, (dtId) => {
                        this.dispatchEvent(dtId, "updateCheckboxSelectAll", {
                            columnIndex: 1,
                            checked: false
                        });
                    });
                    break;
                case "cmdClearFollow":
                    _.forEach(this.dtIds, (dtId) => {
                        this.dispatchEvent(dtId, "updateCheckboxSelectAll", {
                            columnIndex: 1,
                            checked: true
                        });
                    });
                    break;
            }
        }
    }

    onFollowCheckboxDataChanged(data, e) {
        
        _.forEach(data, (item) => {
            if (item.value) {
                let fId = _.filter(this.followCheckboxData(), function (id) {
                    return id == item.id;
                });
                if (!_.size(fId)) {
                    this.followCheckboxData.push(item.id);
                    this.viewCheckboxData.push(item.id);  // เมื่อมีการเลือก follow จะ add id ของ vehicle เข้าไป
                }
                //this.followCheckbox(item);// เมื่อมีการเลือก follow จะเลือก view ด้วย
            } else {
                _.pull(this.followCheckboxData(), item.id);
            }

            this.followCheckbox(item,false,true);// เมื่อไม่ follow จะยังคงเลือก view อยู่
        });
    }

    onViewCheckboxDataChanged(data, e) {

        _.forEach(data, (item) => {
            if (item.value) {
                let fId = _.filter(this.viewCheckboxData(), function (id) {
                    return id == item.id;
                });
                if (!_.size(fId)) {
                    this.viewCheckboxData.push(item.id);
                }
                this.followCheckbox(item);// refresh Items ทุกครั้ง เมื่อมีการ check view   
            } else {
                _.pull(this.viewCheckboxData(), item.id);
                _.pull(this.followCheckboxData(), item.id);
                this.followCheckbox(item,true);// เมื่อเลือก view และ follow อยู่ แล้วไม่เลือก view จะ remove follow ด้วย 
            }
        });

    }

    followCheckbox(item,fromView = false,fromFollow = false) {
        _.forEach(this.results(), (dtItem) => {
            let isFollow = false;
            let vehicleItem = _.map(dtItem.assets(), (assetItem) => {

                if (assetItem.vehicleId == item.id) {
                    isFollow = true;

                    if(fromView){ // มาจาก checkbox view ก็ต่อเมื่อ non-selected view 
                        assetItem.isView = false;
                        assetItem.isFollow = false;
                    }
                    else if(fromFollow){
                        assetItem.isFollow = item.value;

                        if(item.value){ //ถ้า เลือก follow ให้เลือก view ด้วย
                            assetItem.isView = true;
                        }
                        else if(assetItem.isView){//ถ้า เคยเลือก view อยู่แล้ว แล้วเอา follow ออก จะไม่เอา view ออก
                            assetItem.isView = true;
                        }
                        else{
                            assetItem.isView = false;
                        }
                    }
                    else{
                        assetItem.isView = true;
                    }
                    return assetItem;
                }  
            });
            if(isFollow){
                this.dispatchEvent(dtItem.dtId, "refresh");
            }
        });
    }
}

class FindAssetsResultItem {

    // info = FindAssetsResultInfo, entityType = Enums.ModelData.SearchEntityType
    constructor(info, entityType, index, title) {
        this.businessUnitId = info.businessUnitId;
        this.businessUnitPath = info.businessUnitPath;

        var customColumns = null;
        var order = null;
        var assets = info.assets;
        var titleFleetMonitoringView = title.titleFleetMonitoringView;
        var titleFleetMonitoringFollow = title.titleFleetMonitoringFollow;

        this.businessUnitPath += " (" + assets.length + ")";

        this.headerTemplateIcon = _.template('<div class="<%= iconCssClass %>" title="<%= title %>"><%= title %></div>');
        var rowId = null;



        switch (entityType) {
            case Enums.ModelData.SearchEntityType.Vehicle:
                //format vehicle brand - model
                Utility.applyFormattedPropertyToCollection(assets, "vehicleBrandModel", "{0} - {1}", "vehicleBrand", "vehicleModel");
                //set tempId for dataTable
                Utility.applyFormattedPropertyToCollection(assets, "tempId", "{0}", "vehicleId");

                customColumns = [{
                        type: 'checkbox',
                        headerTemplate: this.headerTemplateIcon({
                            'iconCssClass': 'icon-follow',
                            'title': titleFleetMonitoringView
                        }),
                        title: titleFleetMonitoringView,
                        data: 'isView',
                        columnMenu: false,
                        columnMenuItem: false,
                        sortable: false,
                        resizable: false,
                        enableSelectAll: true,
                        onDataChanged: null
                    },
                    {
                        type: 'checkbox',
                        headerTemplate: this.headerTemplateIcon({
                            'iconCssClass': 'icon-follow',
                            'title': titleFleetMonitoringFollow
                        }),
                        title: titleFleetMonitoringFollow,
                        data: 'isFollow',
                        columnMenu: false,
                        columnMenuItem: false,
                        sortable: false,
                        resizable: false,
                        enableSelectAll: true,
                        onDataChanged: null
                    },
                    {
                        type: 'label',
                        title: 'i18n:Common_Vehicle',
                        data: 'vehicleBrandModel',
                        width: '210px',
                        sortable: false
                    },
                    {
                        type: 'label',
                        title: 'i18n:Common_License',
                        data: 'vehicleLicense',
                        width: '250px',
                        sortable: false
                    },
                    {
                        type: 'label',
                        title: 'i18n:Common_Driver',
                        data: 'driverName',
                        width: '210px',
                        sortable: false
                    }
                ];

                order = [
                    [3, "asc"]
                ];
                rowId = "vehicleId";
                break;
            case Enums.ModelData.SearchEntityType.DeliveryMan:
                //set tempId for dataTable
                Utility.applyFormattedPropertyToCollection(assets, "tempId", "{0}", "deliveryManId");

                customColumns = [{
                        type: 'checkbox',
                        headerTemplate: this.headerTemplateIcon({
                            'iconCssClass': 'icon-follow',
                            'title': titleFleetMonitoringView
                        }),
                        title: titleFleetMonitoringView,
                        data: 'isView',
                        columnMenu: false,
                        columnMenuItem: false,
                        sortable: false,
                        resizable: false,
                        enableSelectAll: true,
                        onDataChanged: null
                    },
                    {
                        type: 'checkbox',
                        headerTemplate: this.headerTemplateIcon({
                            'iconCssClass': 'icon-follow',
                            'title': titleFleetMonitoringFollow
                        }),
                        title: titleFleetMonitoringFollow,
                        data: 'isFollow',
                        columnMenu: false,
                        columnMenuItem: false,
                        sortable: false,
                        resizable: false,
                        enableSelectAll: true,
                        onDataChanged: null
                    },
                    {
                        type: 'label',
                        title: 'i18n:Common_Username',
                        data: 'username',
                        sortable: false
                    },
                    {
                        type: 'label',
                        title: 'i18n:Common_Email',
                        data: 'email',
                        sortable: false
                    }
                ];

                order = [
                    [2, "asc"]
                ];
                rowId = "deliveryManId";
                break;
        }

        this.dtId = 'dtTrackAssetList' + index;
        this.assets = ko.observableArray(assets);
        this.customColumns = ko.observable(customColumns);
        this.order = ko.observable(order);
        this.rowId = ko.observable(rowId);
    }

    /**
     * On Datasource Request Read
     * 
     * @param {any} res
     * @returns
     * 
     * @memberOf TrackWidget
     */
    onDatasourceRequestRead(gridOption) {
        var dfd = $.Deferred();
        var items = this.assets();
        var totalItems = items.length;
        dfd.resolve({
            items: items,
            totalRecords: totalItems,
            currentPage: 1
        });
        return dfd;
    }

}

export default {
    viewModel: ScreenBase.createFactory(TrackFindAssetResultScreen),
    template: templateMarkup
};
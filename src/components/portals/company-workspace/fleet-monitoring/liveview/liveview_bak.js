﻿import ko from "knockout";
import templateMarkup from "text!./liveview.html";
import ScreenBase from "../../../screenbase";
import * as BladeMargin from "../../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
// import "hik-webSession";
import { WebControl } from "hikvision";
import "ztree";


class MainServiceTesting extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle("Camera");
        
        this.bladeMargin = BladeMargin.Narrow;
        this.bladeSize = BladeSize.Medium;
        this.bladeIsMaximized = true;
        // or playback 17.19-21
        //for login
        this.oLoginWebControl = null;
        this.userName="admin";
        this.pwd="DGFP@$$w0rddgf";
        this.uri="http://49.231.64.169";
        this.domainName="CDG";
        this.address="192.168." + Math.floor(Math.random() * 254) + "." + Math.floor(Math.random() * 254)  ;
        this.codeUuid = new Date().getTime()+Math.random();
        this.urlRequest= this.uri + "/webSDK/login";

        //for init
        this.oWebControl = null; // 插件对象
        this.oTree = null; // 树对象
        this.bDrag = false; // 拖拽标记
        // this.websession = new webSession();
        // this.uri = "http://"+this.websession.getItem("url");
        this.isPriview = true;//默认开启预览
        // this.sessionId = this.websession.getItem("sessionId");
        this.sessionId = ko.observable();
        this.oPermission = {         // 权限
                    "liveView": 1,
                    "playback": 1,
                    "videoSearch": 1,
                    "manualRecording": 1,
                    "twoWayAudio": 1,
                    "ptzControl": 1,
                    "capturePrint": 1,
                    "volume": 1
                };
        this.timer = 0;
        this.nodes={};
        

    }

    

    onDomReady() {
        var self = this;
        

        // 开始预览
        $("#realPlay").click(
            function() {
                self.oWebControl.JS_Play(
                        "[sdk:preview]10.18.147.136/8000/1/0/0/1401/254/0/0/0",
                        "admin", "Abc12345", "0", "10.17.137.240", "Camera 01",
                        self.oPermission, -1);
            });

        // 显示报警信息
        $("#alarmInfo").click(
                function() {
                    self.oWebControl.JS_ShowAlarmInfoInFullScreen("Alarm Title",
                            "Alarm Message", "Alarm Id");
                });

        // Top遮盖
        $("#topCover").click(function() {
            self.oWebControl.JS_SetWndCover("top", 25);
        });

        // Left遮盖
        $("#leftCover").click(function() {
            self.oWebControl.JS_SetWndCover("left", 50);
        });

        // Right遮盖
        $("#rightCover").click(function() {
            self.oWebControl.JS_SetWndCover("right", 75);
        });

        // Bottom遮盖
        $("#bottomCover").click(function() {
            self.oWebControl.JS_SetWndCover("bottom", 100);
        });

        // 遮盖还原
        $("#coverRestore").click(function() {
            self.oWebControl.JS_SetWndCover("top", 0);
            self.oWebControl.JS_SetWndCover("left", 0);
            self.oWebControl.JS_SetWndCover("right", 0);
            self.oWebControl.JS_SetWndCover("bottom", 0);
        });

        //切换到预览窗口
	$("#switchRealPlay").click(function() {
		$(".setUpParams").css("display", "none");
        $("#switchRealPlay").addClass('active');
        $("#switchPlayback").removeClass('active');
        $("#stopAll").removeClass('active');
        $("#exit").removeClass('active');
        self.isPriview = true;
        self.oWebControl.JS_ChangePlayMode(0);
	});

	//切换到回放窗口
	$("#switchPlayback").click(function() {
		$(".setUpParams").css("display", "block");
        $("#switchPlayback").addClass('active');
        $("#switchRealPlay").removeClass('active');
        $("#stopAll").removeClass('active');
        $("#exit").removeClass('active');
        self.isPriview = false;
		$("#startTime").css("border","");
		$("#endTime").css("border","");
        //set default time
        var date = new Date();
         date = self.dateFormat(date,"yyyy-MM-dd");
        var start = date+"T"+"08:00:00";
        var end =  date+"T"+"08:10:00";
        $("#startTime").val(start);
        $("#endTime").val(end);
        self.oWebControl.JS_ChangePlayMode(1);
		
	});
    //stop窗口
    $("#stopAll").click(function() {
        $(".setUpParams").css("display", "none");
        $("#stopAll").addClass('active');
        $("#switchPlayback").removeClass('active');
        $("#switchRealPlay").removeClass('active');
        $("#exit").removeClass('active');
        self.oWebControl.JS_StopTotal();
    });
    //exit窗口
    $("#exit").click(function() {
        $(".setUpParams").css("display", "none");
        $("#exit").addClass('active');
        $("#switchPlayback").removeClass('active');
        $("#switchRealPlay").removeClass('active');
        $("#stopAll").removeClass('active');
        window.clearInterval(timer);
        //登出
        $.ajax({
            url:this.uri+"/webSDK/logout",
            data:{
                sessionID:self.sessionId()
            },
            type:'post',
            success:function(data){
                var result = JSON.parse(data);
                if(result.errorCode == 0){
                    // window.location.href="./login.html";
                }
                else{
                    alert(result.errorCode);
                }
            }
        });

    });
    //
    $("#alarm").click(function() {
        // window.location.href="./alarm.html";
        self.isPriview = true;
        self.oWebControl.JS_ChangePlayMode(0);
    });

    $("#playb").click(function() {
        self.startPlayback(self.nodes[1], 1);
    });

    

    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        $.support.cors = true;
        var self = this;
        var dfd = $.Deferred();
        var dfd2 = $.Deferred();
        var dfd3 = $.Deferred();
        var dfd4 = $.Deferred();
        var dfd5 = $.Deferred();

        //定时调用心跳接口
        this.timer =  window.setInterval(this.keepHeartBeat,1000*60*60);

        
        $.when(self.initLoginWebControl()).done((res)=>{
            console.log(1);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        
        
        
        $.when(dfd).done((res)=>{
            console.log(2);
            self.userLogin();
            dfd2.resolve();
        }).fail((e) => {
            dfd2.reject(e);
        });
        
        setTimeout(function(){
            $.when(dfd2).done((res)=>{
                console.log(3);
                //self.initTreeData();
                self.GetCameraInfomatinon();
                dfd3.resolve();
            }).fail((e) => {
                dfd3.reject(e);
            });
    
            
            $.when(dfd3).done((res)=>{
                console.log(4);
                self.initWebControl();
                dfd4.resolve();
            }).fail((e) => {
                dfd4.reject(e);
            });
    
            
            $.when(dfd4).done((res)=>{
                console.log(5);
                self.initEvent();
                dfd5.resolve();
            }).fail((e) => {
                dfd5.reject(e);
            });
            
        }, 5000);

        
        
        return dfd5;
    }


    initLoginWebControl() {
        var dfd = $.Deferred();
        var self = this;
        // 初始化插件
        self.oLoginWebControl = new WebControl({
            szPluginContainer : "playWnd",
            cbConnectSuccess : function() {
            console.log("控件已加载");
            dfd.resolve();
            },
            cbConnectError : function() {
                console.log("cbConnectError:");
                self.oLoginWebControl = null;
                $("#playWnd").html("确认本地进程是否已安装并开启成功！");
                WebControl.JS_WakeUp("PlatformSDKWebControlPlugin://");
                location.reload();
                dfd.reject();
            },
            cbConnectClose : function() {
                console.log("cbConnectClose");
                self.oLoginWebControl = null;
            }
        });

        return dfd;
    }

    userLogin(){
        var dfd = $.Deferred();
        var self = this;
        // this.userName=$("#loginName").val();
        // this.pwd=$("#password").val();
        // this.uri=$("#ServerIP").val()+":"+$("#ServerPort").val();
        // var domainName=$("#ServerIP").val();
        // var address=$("#ServerIP").val();
        this.oLoginWebControl.JS_LoginEncryption(this.userName, this.pwd).then(function (data) {
            self.pwd = data.encryptInfo;
            self.doLogin();
            
        });
    
        
        
        return dfd;
    }

    doLogin(){
        var dfd = $.Deferred();
        var self = this;
        console.log(self);
        $.ajax({
            url:this.urlRequest,
            data:{
                'name':this.userName,
                'password':this.pwd,
                'port':'8950',
                'url':this.uri,
                'domainName':this.domainName,
                'address':this.address,
                'loginMode':3,
                'codeUuid':this.codeUuid
            },type:"post",
            success: function(data) {
                var result = JSON.parse(data);
                console.log(result);
                if(result.errorCode==0){
                    // this.websession.setItem("url",self.uriLogin);
                    // this.websession.setItem("sessionId",result.data.sessionID);
                    // window.location.href="./demo_window.html?backurl="+window.location.href;
                    self.sessionId(result.data.sessionID);
                    dfd.resolve();
                }else{
                    alert("ErrorCode："+result.errorCode);
                    dfd.reject();
                }
            }
        });
        return dfd;
    }


    keepHeartBeat(){
        console.log("keepheartbeat run time"+new Date());
        $.ajax({
            url:this.uri+"/webSDK/onlineHeartBeat",
            data:{
                'sessionID':this.sessionId()
            },
            type:"post"
        });
    }

    GetCameraInfomatinon()
    {
        
        var self = this;
        var dfd = $.Deferred();
        var url = this.uri+"/webSDK/getAreasByPage";
        console.log("sessionId " + self.sessionId());
        $.ajax({
            url:url,
            data:{
                "page":1,
                "pageSize":200,
                "sessionID":self.sessionId()
            },
            type:"post",
            success:function(data){
                console.log("Get areas Resource success");
                var result = JSON.parse(data);
                console.log(result);
                if(result.errorCode == 0 && result.data.length>0){
                   var areaId = result.data[0].id;
                    var node = {
                        id:-1,
                        pid:0,
                        nodeName:result.data[0].name
                    };
                    var nodes = new Array();
                    nodes[0] = node;
                    $.ajax({
                        url:self.uri+"/webSDK/getCamerasByPage",
                        data:{
                            'page':1,
                            'pageSize':200,
                            'searchType':2,
                            'idArrayString':areaId,
                            'sessionID':self.sessionId()
                        },
                        type:"post",
                        success:function(data){
                            console.log("get camera success");
                            var result = JSON.parse(data);
                            if(result.errorCode == 0){
                                var cameras = result.data;
                                cameras.forEach(function(value,i){
                                    var node = {};
                                    node.id = value.id;
                                    node.pid = -1;
                                    node.nodeName = value.name;
                                    node.indexCode = value.indexCode;
                                    node.channelNo = value.channelNo;
                                    if(value.streamType =="SubStream"){
                                        node.streamType = 1;
                                    }
                                   else{
                                        node.streamType = 0;
                                    }
                                    node.deviceId = value.deviceId;
                                    nodes[i+1] = node;
                                });
                            }
                            console.log(nodes);
                            self.nodes = nodes;
                            dfd.resolve();
                        }
                    });
                }
            },
            error: function (jqXHR, exception) {
                console.log(jqXHR);
                console.log(exception);
            }
        });
        return dfd;
    }

    initTreeData(){
        var self = this;
        var dfd = $.Deferred();
        var url = this.uri+"/webSDK/getAreasByPage";
        console.log("initTreeData");
        console.log("initTreeData url " + url);
        console.log("initTreeData sessionId " + this.sessionId());
        $.ajax({
            url:url,
            data:{
                "page":1,
                "pageSize":200,
                "sessionID":self.sessionId()
            },
            type:"post",
            success:function(data){
                console.log("initTreeData success");
                var result = JSON.parse(data);
                console.log(result);
                if(result.errorCode == 0 && result.data.length>0){
                   var areaId = result.data[0].id;
                    var node = {
                        id:-1,
                        pid:0,
                        nodeName:result.data[0].name
                    };
                    var nodes = new Array();
                    nodes[0] = node;
                    $.ajax({
                        url:self.uri+"/webSDK/getCamerasByPage",
                        data:{
                            'page':1,
                            'pageSize':200,
                            'searchType':2,
                            'idArrayString':areaId,
                            'sessionID':self.sessionId()
                        },
                        type:"post",
                        success:function(data){
                            console.log("initTreeData success success");
                            var result = JSON.parse(data);
                            if(result.errorCode == 0){
                                var cameras = result.data;
                                cameras.forEach(function(value,i){
                                    var node = {};
                                    node.id = value.id;
                                    node.pid = -1;
                                    node.nodeName = value.name;
                                    node.indexCode = value.indexCode;
                                    node.channelNo = value.channelNo;
                                    if(value.streamType =="SubStream"){
                                        node.streamType = 1;
                                    }
                                   else{
                                        node.streamType = 0;
                                    }
                                    node.deviceId = value.deviceId;
                                    nodes[i+1] = node;
                                });
                            }
                            console.log(nodes);
                            self.initTree(nodes);
                            dfd.resolve();
                        }
                    });
                }
            },
            error: function (jqXHR, exception) {
                console.log(jqXHR);
                console.log(exception);
            }
        });

        return dfd;
    }

    initWebControl() {
        var dfd = $.Deferred();
        var self = this;
		// 初始化插件
		this.oWebControl = new WebControl({
			szPluginContainer : "playWnd",
			cbConnectSuccess : function() {
                self.setCallbacks();
				self.oWebControl.JS_CreateWnd("playWnd", 600, 400).then(function() {
                    console.log("JS_CreateWnd success");
                    var divObject = $("#playWnd");
                    console.log(divObject.width(), divObject.height());
                    self.oWebControl.JS_Resize( divObject.width(), divObject.height());
                    self.setWndCover();
                    self.oWebControl.JS_SwitchToSimple(1);
                    console.log(self.nodes);
                    setTimeout(function(){
                        self.startPreview(self.nodes[5]);
                    }, 3000);
                    
                    // self.startPreview2(self.nodes[1]);
                    // self.startPreview3(self.nodes[2]);
                    // self.startPreview4(self.nodes[3]);
                });
                dfd.resolve();
			},
			cbConnectError : function() {
				console.log("cbConnectError:");
				self.oWebControl = null;
                $("#playWnd").html("确认本地进程是否已安装并开启成功！");
                dfd.reject();
			},
			cbConnectClose : function() {
				console.log("cbConnectClose");
				self.oWebControl = null;
                //$("#playWnd").children().eq(0).remove();
                dfd.reject();
			}
        });
        
        return dfd;
    }

    
    // 设置窗口控制回调
	setCallbacks() {
        var self = this;
		self.oWebControl.JS_SetWindowControlCallback({
			cbSelectWnd : self.onSelectWnd.bind(self),
			cbUpdateCameraIcon : self.onUpdateCameraIcon.bind(self),
			cbStopPlayAll : self.onStopPlayAll.bind(self)
		});
    }
    
    // 初始化树
	initTree(nodes) {
        console.log(nodes);
        var self = this;
		// 树配置
		var oSetting = {

			edit : {
				enable : true,
				showRemoveBtn : false,
				showRenameBtn : false,
				drag : {
					next : false,
					inner : false,
					prev : false
				}
			},
			view : {
				showLine : false,
				dblClickExpand : true,
				selectedMulti : false
			},
			data : {
				key : {
					name : 'nodeName',
					title : 'nodeName',
					checked : 'checked'
				},
				simpleData : {
					enable : true,
					idKey : 'id',
					pIdKey : 'pid'
				}
			},
            callback : {
                onDblClick : function(event, treeId, treeNode) {
                    if ((!!treeNode)) {
                        if (!treeNode.isParent) { // 节点是监控点
                            if(self.isPriview){

                                self.startPreview(treeNode);
                            }
                           else{
                                //切换回放
                                self.startPlayback(treeNode);
                            }
                        }
                    }
                },
                beforeDrag : function() { //,treeId, treeNodes
                    self.bDrag = true;
                    self.oWebControl.JS_SetDragMode(0);
                },
                onDragMove : function(event) { //,treeId, treeNodes
                },
                onDrop : function(event, treeId, treeNodes) { //,targetNode, moveType
                    // 触发拖拽功能
                    var mouseX = event.pageX;
                    var mouseY = event.pageY;
                    var left = $('#playWnd').offset().left;
                    var top = $('#playWnd').offset().top;
                    var right = left + $('#playWnd').outerWidth();
                    var bottom = top + $('#playWnd').outerHeight() - 40;// 40：播放控件的工具条高度
                    if ((mouseX > left) && (mouseY > top) && (mouseX < right)
                            && (mouseY < bottom)) {
                        if ((!!treeNodes[0])) {
                            if (!treeNodes[0].isParent) { // 节点是监控点
                                if(self.isPriview){
                                    self.startPreview(treeNodes[0]);
                            }
                           else{
                                //切换回放
                                self.startPlayback(treeNodes[0]);
                            }
                            }
                        }
                    } else {
                        self.oWebControl.JS_SetDragMode(1);
                        self.bDrag = false;
                    }
                }
            }
        };
        
		self.oTree = $.fn.zTree.init($("#cameraTree"), oSetting, nodes);
    }
    
    // 初始化事件
	initEvent() {

        var self = this;
		// 标签关闭/刷新
		$(window).unload(function() {
			var bIE = (!!window.ActiveXObject || 'ActiveXObject' in window);// 是否为IE浏览器
			if (bIE) {
				if (self.oWebControl != null) {
					console.log("JS_Disconnect");
					self.oWebControl.JS_Disconnect();
				}
			} else {
				if (self.oWebControl != null) {
					console.log("JS_DestroyWnd");
					self.oWebControl.JS_DestroyWnd().then(function() {
						console.log("JS_Disconnect");
						self.oWebControl.JS_Disconnect();
					});
				}
            }
		});

		// 窗口resize
		$(window).resize(function() {
			if (self.oWebControl != null) {
                // var iHeight = $('.app-blade-header').width()*0.4;
                // self.oWebControl.JS_Resize(iHeight, iHeight*3/4);
                var divObject = $("#playWnd");
                self.oWebControl.JS_Resize( divObject.width(), divObject.height());
				self.setWndCover();
            }
		});
		/*$(window).resize($.debounce(100, function () {
		    oWebControl.JS_Resize(600, 400);
		}));*/

		// 滚动条scroll
		$('.app-blade-content').scroll(function() {

			if (self.oWebControl != null) {
                var divObject = $("#playWnd");
                self.oWebControl.JS_Resize( divObject.width(), divObject.height());
				self.setWndCover();
            }
        });
    }   
    
    // 设置窗口遮挡
	setWndCover() {
		// var iWidth = $('.app-blade-content').width();
		// var iHeight = $(window).height();
		// var oDivRect = $("#playWnd").get(0).getBoundingClientRect();

		// var iCoverLeft = (oDivRect.left < 0) ? Math.abs(oDivRect.left) : 0;
		// var iCoverTop = (oDivRect.top < 0) ? Math.abs(oDivRect.top) : 0;
		// var iCoverRight = (oDivRect.right - iWidth > 0) ? Math
		// 		.round(oDivRect.right - iWidth) : 0;
		// var iCoverBottom = (oDivRect.bottom - iHeight > 0) ? Math
		// 		.round(oDivRect.bottom - iHeight) : 0;

        // var divObject = $("#playWnd");
        
        // iCoverLeft = (iCoverLeft > divObject.width()) ? divObject.width() : iCoverLeft;
        // iCoverTop = (iCoverTop > divObject.height()) ? divObject.height() : iCoverTop;
        
        // console.log("iCoverTop",iCoverTop);
		// iCoverRight = (iCoverRight > divObject.width()) ? divObject.width() : iCoverRight;
		// iCoverBottom = (iCoverBottom > divObject.height()) ? divObject.height() : iCoverBottom;

        // console.log(iCoverLeft,iCoverTop,iCoverRight,iCoverBottom);
		// this.oWebControl.JS_SetWndCover("left", iCoverLeft);
		// this.oWebControl.JS_SetWndCover("top", iCoverTop);
		// this.oWebControl.JS_SetWndCover("right", iCoverRight);
        // this.oWebControl.JS_SetWndCover("bottom", iCoverBottom);
        
  var oDivparentRect = $(".app-blade-content-container").get(0).getBoundingClientRect();
  var divObject = $('#playWnd');
  var iWidth = oDivparentRect.width;
  var iHeight = oDivparentRect.height;
  var oDivRect = divObject.get(0).getBoundingClientRect();
  
  var iCoverLeft = (oDivRect.left < oDivparentRect.left) ? Math.abs(oDivRect.left) : 0;
  var iCoverTop = (oDivRect.top < oDivparentRect.top) ? Math.abs(oDivRect.top - oDivparentRect.top) : 0;
  var iCoverRight = (oDivRect.right - oDivparentRect.right > 0) ? Math
    .round(Math.abs(oDivRect.right - oDivparentRect.right)) : 0;
  var iCoverBottom = (oDivRect.bottom - oDivparentRect.bottom > 0) ? Math
    .round(Math.abs(oDivRect.bottom - oDivparentRect.bottom)) : 0;

  iCoverLeft = (iCoverLeft > divObject.width()) ? divObject.width() : iCoverLeft;
  iCoverTop = (iCoverTop > divObject.height()) ? divObject.height() : iCoverTop;
  iCoverRight = (iCoverRight > divObject.width()) ? divObject.width() : iCoverRight;
  iCoverBottom = (iCoverBottom > divObject.height()) ? divObject.height() : iCoverBottom;
  
  if(this.oWebControl){
    this.oWebControl.JS_SetWndCover("left", iCoverLeft);
    this.oWebControl.JS_SetWndCover("top", iCoverTop);
    this.oWebControl.JS_SetWndCover("right", 0);
    this.oWebControl.JS_SetWndCover("bottom", iCoverBottom);
  }
    }
    
    // 开启预览
	startPreview(oNode) {
        var self = this;
        var szUrl,userName,userPwd,siteId ;
        $.ajax({
            url:this.uri+"/webSDK/getRealStreamUrl",
            type:"post",
            data:{
                'cameraIndexcode':oNode.indexCode,
                'streamType':oNode.streamType,
                'sessionID':self.sessionId()
            },
            success:function(data){
                var result = JSON.parse(data);
                if(result.errorCode == 0){
                    szUrl = result.data;
                }
                $.ajax({
                    url:self.uri+"/webSDK/getEncodeDevicesByPage",
                    data:{
                        'page':1,
                        'pageSize':200,
                        'searchType':3,
                        'idArrayString':oNode.deviceId,
                        'sessionID':self.sessionId()
                    },
                    type:'post',
                    success:function(data){
                        var result = JSON.parse(data);
                        if(result.errorCode ==0){
                            userName = result.data[0].userName;
                            userPwd = result.data[0].userPwd;
                            siteId = result.data[0].siteId;
                            var szXml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><Params><PlatformType>3</PlatformType></Params>";
                            // 开启预览
                            setTimeout(function() {
                                self.oWebControl.JS_Play(szUrl, userName, userPwd, siteId,
                                oNode.nodeName, oNode.nodeName, self.oPermission, 0,szXml).then(function() {
                                    if (self.bDrag) {
                                        self.oWebControl.JS_SetDragMode(1);
                                        self.bDrag = false;
                                    }
                                });
                            }, 100);// 延时100ms，保证窗口已经切换成功
                        }

                    }
                });
            }
        });

    }
    
    // 开启预览
	startPreview2(oNode) {
        var self = this;
        var szUrl,userName,userPwd,siteId ;
        $.ajax({
            url:this.uri+"/webSDK/getRealStreamUrl",
            type:"post",
            data:{
                'cameraIndexcode':oNode.indexCode,
                'streamType':oNode.streamType,
                'sessionID':self.sessionId()
            },
            success:function(data){
                var result = JSON.parse(data);
                if(result.errorCode == 0){
                    szUrl = result.data;
                }
                $.ajax({
                    url:self.uri+"/webSDK/getEncodeDevicesByPage",
                    data:{
                        'page':1,
                        'pageSize':200,
                        'searchType':3,
                        'idArrayString':oNode.deviceId,
                        'sessionID':self.sessionId()
                    },
                    type:'post',
                    success:function(data){
                        var result = JSON.parse(data);
                        if(result.errorCode ==0){
                            userName = result.data[0].userName;
                            userPwd = result.data[0].userPwd;
                            siteId = result.data[0].siteId;
                            var szXml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><Params><PlatformType>3</PlatformType></Params>";
                            // 开启预览
                            setTimeout(function() {
                                self.oWebControl.JS_Play(szUrl, userName, userPwd, siteId,
                                oNode.nodeName, oNode.nodeName, self.oPermission, 1,szXml).then(function() {
                                    if (self.bDrag) {
                                        self.oWebControl.JS_SetDragMode(1);
                                        self.bDrag = false;
                                    }
                                });
                            }, 100);// 延时100ms，保证窗口已经切换成功
                        }

                    }
                });
            }
        });

    }
    
    // 开启预览
	startPreview3(oNode) {
        var self = this;
        var szUrl,userName,userPwd,siteId ;
        $.ajax({
            url:this.uri+"/webSDK/getRealStreamUrl",
            type:"post",
            data:{
                'cameraIndexcode':oNode.indexCode,
                'streamType':oNode.streamType,
                'sessionID':self.sessionId()
            },
            success:function(data){
                var result = JSON.parse(data);
                if(result.errorCode == 0){
                    szUrl = result.data;
                }
                $.ajax({
                    url:self.uri+"/webSDK/getEncodeDevicesByPage",
                    data:{
                        'page':1,
                        'pageSize':200,
                        'searchType':3,
                        'idArrayString':oNode.deviceId,
                        'sessionID':self.sessionId()
                    },
                    type:'post',
                    success:function(data){
                        var result = JSON.parse(data);
                        if(result.errorCode ==0){
                            userName = result.data[0].userName;
                            userPwd = result.data[0].userPwd;
                            siteId = result.data[0].siteId;
                            var szXml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><Params><PlatformType>3</PlatformType></Params>";
                            // 开启预览
                            setTimeout(function() {
                                self.oWebControl.JS_Play(szUrl, userName, userPwd, siteId,
                                oNode.nodeName, oNode.nodeName, self.oPermission, 2,szXml).then(function() {
                                    if (self.bDrag) {
                                        self.oWebControl.JS_SetDragMode(1);
                                        self.bDrag = false;
                                    }
                                });
                            }, 100);// 延时100ms，保证窗口已经切换成功
                        }

                    }
                });
            }
        });

    }
    
    // 开启预览
	startPreview4(oNode) {
        var self = this;
        var szUrl,userName,userPwd,siteId ;
        $.ajax({
            url:this.uri+"/webSDK/getRealStreamUrl",
            type:"post",
            data:{
                'cameraIndexcode':oNode.indexCode,
                'streamType':oNode.streamType,
                'sessionID':self.sessionId()
            },
            success:function(data){
                var result = JSON.parse(data);
                if(result.errorCode == 0){
                    szUrl = result.data;
                }
                $.ajax({
                    url:self.uri+"/webSDK/getEncodeDevicesByPage",
                    data:{
                        'page':1,
                        'pageSize':200,
                        'searchType':3,
                        'idArrayString':oNode.deviceId,
                        'sessionID':self.sessionId()
                    },
                    type:'post',
                    success:function(data){
                        var result = JSON.parse(data);
                        if(result.errorCode ==0){
                            userName = result.data[0].userName;
                            userPwd = result.data[0].userPwd;
                            siteId = result.data[0].siteId;
                            var szXml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><Params><PlatformType>3</PlatformType></Params>";
                            // 开启预览
                            setTimeout(function() {
                                self.oWebControl.JS_Play(szUrl, userName, userPwd, siteId,
                                oNode.nodeName, oNode.nodeName, self.oPermission, 3,szXml).then(function() {
                                    if (self.bDrag) {
                                        self.oWebControl.JS_SetDragMode(1);
                                        self.bDrag = false;
                                    }
                                });
                            }, 100);// 延时100ms，保证窗口已经切换成功
                        }

                    }
                });
            }
        });

	}

    // 开启回放
    startPlayback(oNode) {
        this.playOrdownload(oNode,1);

    }
    downloadRecord(){
        var treeObj=$.fn.zTree.getZTreeObj("cameraTree");
        var oNode = treeObj.getSelectedNodes();
        if(oNode.length == 0||oNode.isParent){
            alert("please select the camera");
            return;
        }
        this.playOrdownload(oNode[0],2);
    }


     /**
     *
     * @param oNode
     * @param type 类型 1回放 2下载
     */
    playOrdownload(oNode,type){
        var self = this;
        var szUrl,userName,userPwd,siteId ;
        var startTime = $("#startTime").val();
        var endTime = $("#endTime").val();
        if(startTime==""||endTime==""){
            alert("The start time and the end time cannot be empty");
            if(startTime==""){$("#startTime").css("border","1px solid #e54042");}
            else {$("#startTime").css("border","")};
            if(endTime=="")$("#endTime").css("border","1px solid #e54042");
            else {$("#endTime").css("border","")};
            return;
        }
        var recordType = $("#recordType").val();
        var szXml;
        $.ajax({
            url:self.uri+"/webSDK/getRecordStreamUrl",
            type:"post",
            data:{
                'cameraIndexcode':oNode.indexCode,
                'sessionID':self.sessionId(),
                'startTime':startTime,
                'endTime':endTime,
                'recordMediaType':recordType,
                'recordTypeString':"1,2,3,4,5,8,20,21"
            },
            success:function(data){
                var result = JSON.parse(data);
                if(result.errorCode == 0){
                    szUrl = result.data;
                }
                $.ajax({
                    url:self.uri+"/webSDK/getEncodeDevicesByPage",
                    data:{
                        'page':1,
                        'pageSize':200,
                        'searchType':3,
                        'idArrayString':oNode.deviceId,
                        'sessionID':self.sessionId()
                    },
                    type:'post',
                    success:function(data){
                        var result = JSON.parse(data);
                        if(result.errorCode ==0){
                            userName = result.data[0].userName;
                            userPwd = result.data[0].userPwd;
                            siteId = result.data[0].siteId;

                            if(type == 1){
                                // 开启playback
                                if(recordType==1){
                                    szXml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><Params><PlatformType>3</PlatformType><StorageMediaType>1</StorageMediaType><SearchTime><BeginTime>"
                                    +startTime+"</BeginTime><EndTime>"+endTime+"</EndTime></SearchTime></Params>";
                                }
                                if(recordType==3){
                                    szXml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><Params><PlatformType>3</PlatformType><StorageMediaType>3</StorageMediaType><SearchTime><BeginTime>"
                                    +startTime+"</BeginTime><EndTime>"+endTime+"</EndTime></SearchTime></Params>";
                                }
                                setTimeout(function() {
                                    self.oWebControl.JS_Play(szUrl, userName, userPwd, siteId,
                                            oNode.nodeName, oNode.nodeName, self.oPermission, -1,szXml).then(function() {
                                                if (self.bDrag) {
                                                    self.oWebControl.JS_SetDragMode(1);
                                                    self.bDrag = false;
                                                }
                                            });
                                }, 100);// 延时100ms，保证窗口已经切换成功
                            }
                          if(type==2){
                              //增加保存路径
                              var loadpath = $("#loadpath").val();
                              if(recordType==1){
                                  szXml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><Params><PlatformType>3</PlatformType><StorageMediaType>1</StorageMediaType><SearchTime><BeginTime>"
                                  +startTime+"</BeginTime><EndTime>"+endTime+"</EndTime></SearchTime><DownloadFolder>"+loadpath+"</DownloadFolder></Params>";
                              }
                              if(recordType==3){
                                  szXml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><Params><PlatformType>3</PlatformType><StorageMediaType>3</StorageMediaType><SearchTime><BeginTime>"
                                  +startTime+"</BeginTime><EndTime>"+endTime+"</EndTime></SearchTime><DownloadFolder>"+loadpath+"</DownloadFolder></Params>";
                              }
                              self.oWebControl.JS_RecordDownload(szUrl, userName, userPwd, siteId, oNode.nodeName, oNode.nodeName, self.oPermission, -1, szXml);
                            }
                        }

                    }
                });
            }
        });
    }
	// 窗口选中回调
	onSelectWnd(wndIndex, cameraID, siteID) {
		this.showCBInfo("onSelectWnd: wndIndex=" + wndIndex + ", cameraID="
				+ cameraID + ", siteID=" + siteID);
		console.log("onSelectWnd: wndIndex=" + wndIndex + ", cameraID="
				+ cameraID + ", siteID=" + siteID);
	}

	// 更新监控点图标
	onUpdateCameraIcon(cameraID, playing, siteID) {
		this.showCBInfo("onUpdateCameraIcon: cameraID=" + cameraID + ", playing="
				+ playing + ", siteID=" + siteID);
	}

	// 关闭所有视频
	onStopPlayAll() {
		this.showCBInfo("onStopPlayAll");
	}
	// 显示回调信息
	showCBInfo(szInfo) {
		szInfo = "<div>" + this.dateFormat(new Date(), "yyyy-MM-dd hh:mm:ss") + " "
				+ szInfo + "</div>";
		$("#cbInfo").html(szInfo + $("#cbInfo").html());
	}

	// 格式化时间
	dateFormat(oDate, fmt) {
		var o = {
			"M+" : oDate.getMonth() + 1, //月份
			"d+" : oDate.getDate(), //日
			"h+" : oDate.getHours(), //小时
			"m+" : oDate.getMinutes(), //分
			"s+" : oDate.getSeconds(), //秒
			"q+" : Math.floor((oDate.getMonth() + 3) / 3), //季度
			"S" : oDate.getMilliseconds()
		//毫秒
		};
		if (/(y+)/.test(fmt)) {
			fmt = fmt.replace(RegExp.$1, (oDate.getFullYear() + "")
					.substr(4 - RegExp.$1.length));
		}
		for ( var k in o) {
			if (new RegExp("(" + k + ")").test(fmt)) {
				fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
						: (("00" + o[k]).substr(("" + o[k]).length)));
			}
		}
		return fmt;
    }




    
    

    



    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        this.oWebControl.JS_Disconnect();
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {   
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedIndex(null);
    }
}

export default {
    viewModel: ScreenBase.createFactory(MainServiceTesting),
    template: templateMarkup
};
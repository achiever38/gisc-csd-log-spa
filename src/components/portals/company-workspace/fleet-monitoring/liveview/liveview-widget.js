﻿import ko from "knockout";
import templateMarkup from "text!./liveview-widget.html";
import ScreenBaseWidget from "../../../screenbasewidget";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import { VideoIcons } from "../../../../../app/frameworks/constant/svg";
import 'iqtech-swf';
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";
import {Enums} from "../../../../../app/frameworks/constant/apiConstant";

class LiveViewWidget extends ScreenBaseWidget {
    constructor(params) {
        super(params);
        
        // params = JSON.parse('[{"id":21989,"deviceId":"PRV153BP_60-1906","deviceCode":"PRV153BP_60-1906","vehicleModel":"--TRUCK_18W_32T_ST_FLATBED","license":"ปข70-3778","driverName":"นาย สนธยา อารักษ์","brand":3,"mdvrServerKey":"FZbungABQeIH3qV3aOs0vNfD3t/f1zXfimW6/qJGVaO7cxDD3qxwGjKNtlsDVd8RG5C/fqVz+v+2xuKWqIAd5w==","cameraNo":4,"vehicleModelDisplayText":"ปข70-3778 (นาย สนธยา อารักษ์)","devices":[{"vid":"PRV153BP_60-1906","type":1,"did":"prv153bp"}],"sessionId":"7da7078f-4e5a-44a5-9849-4a6b6f7ecd1a","cameraConfig":"prv|000000|https://iqtech-mdvr.com"},{"id":22030,"deviceId":"PRV157_79-0537","deviceCode":"PRV157_79-0537","vehicleModel":"--TRUCK_18W_32T_ST_FLATBED","license":"ปข70-3798","driverName":"นาย ปิติพล วรบุตร","brand":3,"mdvrServerKey":"FZbungABQeIH3qV3aOs0vNfD3t/f1zXfimW6/qJGVaO7cxDD3qxwGjKNtlsDVd8RG5C/fqVz+v+2xuKWqIAd5w==","cameraNo":5,"vehicleModelDisplayText":"ปข70-3798 (นาย ปิติพล วรบุตร)","devices":[{"vid":"PRV157_79-0537","type":1,"did":"prv157"}],"sessionId":"f667e4d6-a7a5-4441-ae26-676d6b26924b","cameraConfig":"prv|000000|https://iqtech-mdvr.com"},{"id":22020,"deviceId":"PRV154B_60-1908","deviceCode":"PRV154B_60-1908","vehicleModel":"--TRUCK_18W_32T_ST_FLATBED","license":"ปข70-3836","driverName":"-","brand":3,"mdvrServerKey":"FZbungABQeIH3qV3aOs0vNfD3t/f1zXfimW6/qJGVaO7cxDD3qxwGjKNtlsDVd8RG5C/fqVz+v+2xuKWqIAd5w==","cameraNo":5,"vehicleModelDisplayText":"ปข70-3836 (-)","devices":[{"vid":"PRV154B_60-1908","type":1,"did":"prv154bp"}],"sessionId":"0d7b0c27-69e0-4b84-844b-9d09744986c5","cameraConfig":"prv|000000|https://iqtech-mdvr.com"}]');
        this.listLiveView = ko.observableArray(params);
        this.listItems = ko.observable();
        this.gridItems = ko.observableArray([]);
        this.bgColor = ko.observable();
        this.isCollapse = ko.observable(true);
        this.viewMode = ko.observable("list");
        this.listMode = ko.observable("collapse");
        this.iconPlus = ko.observable("fa fa-plus-circle");
        this.iconMinus = ko.observable("fa fa-minus-circle active");
        this.iconList = ko.observable("fa fa-list active");
        this.iconAllCamera = ko.observable("fa fa-television");
        this.iconGrid = ko.observable("fa fa-th");
        this.iconListVideo = ko.observable("live-view-icon-list-video");
        this.width = "250px";
        this.height = "200px";
        let isFullMenu = false;
        let configLiveView = WebConfig.appSettings.liveViewFullMenu;
        if(configLiveView === true){
            isFullMenu = true;
        }else if(configLiveView && configLiveView.toLowerCase() == 'true'){
            isFullMenu = true;
        }else{
            isFullMenu = false;
        }

        this.AllCamera_width = "610px";
        this.AllCamera_height = "450px";
        this.isShowMenu = ko.observable(isFullMenu);
        
        WebConfig.fleetMonitoring.trackLocationItems.subscribe((data)=>{
            let listCollapse = new Array();
            let listExpand = new Array();
            _.forEach(this.listItems().collapse, (item, index)=>{
                let fData = _.find(data, (obj)=>{
                    return obj.vehicleId == item.id;
                });
                if(_.size(fData)){
                    item.location(fData.location);
                    item.latitude(fData.latitude);
                    item.longitude(fData.longitude);
                }
                listCollapse.push(item);
            });
            _.forEach(this.listItems().expand, (item, index)=>{
                let fData = _.find(data, (obj)=>{
                    return obj.vehicleId == item.id;
                });
                if(_.size(fData)){
                    item.location(fData.location);
                    item.latitude(fData.latitude);
                    item.longitude(fData.longitude);
                }
                listExpand.push(item);
            });
            this.listItems({
                collapse: listCollapse,
                expand: listExpand
            });
        });
    }

    getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
          color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    onClickViewMode(state) {

        if(state == "list"){
            this.iconList("fa fa-list active");
            this.iconAllCamera("fa fa-television");
            this.iconGrid("fa fa-th");
            this.iconListVideo("live-view-icon-list-video");
            if(this.viewMode() != state){
                setTimeout(()=>{
                    this.renderListCollapseLiveView();
                }, 1000);
            }
        }else if(state == "allCamera"){
            this.iconList("fa fa-list");
            this.iconAllCamera("fa fa-television active");
            this.iconGrid("fa fa-th");
            this.iconListVideo("live-view-icon-list-video");
            if(this.viewMode() != state){
                setTimeout(()=>{
                    this.renderListAllCamera();
                }, 1000);
            }
        }else if(state == "grid"){
            this.iconList("fa fa-list");
            this.iconAllCamera("fa fa-television");
            this.iconGrid("fa fa-th active");
            this.iconListVideo("live-view-icon-list-video");
            //reset state to list view
            this.isCollapse(true);
            if(this.viewMode() != state){
                setTimeout(()=>{
                    this.renderGridLiveView();
                }, 1000);
            }
            
        }else if(state == "list-video"){
            this.iconListVideo("live-view-icon-list-video active");
            this.iconAllCamera("fa fa-television");
            this.iconList("fa fa-list");
            this.iconGrid("fa fa-th");
            if(this.viewMode() != state){
                setTimeout(()=>{
                    this.renderListExpandLiveView();
                }, 1000);
            }
        }
        this.viewMode(state);
    }

    onClickViewOnMap(data, obj, event) {
        
        let latitude = ( data.latitude() ) ? data.latitude()  : '13.717567';
        let longitude = ( data.longitude() ) ? data.longitude() : '100.5653778';
        let LvZoom = 19
        let location;
        let symbol = {
            url: this.resolveUrl("~/images/block_hilight.png"),
            width: '45',
            height: '45',
            offset: {
                x: 0,
                y: 0
            },
            rotation: 0
        }

        location = {lat:latitude,lon:longitude}
       
        MapManager.getInstance().panOnePointZoom(null , location,symbol,LvZoom,null);
        this.onActiveViewOnMap(event);
    }

    onActiveViewOnMap(event){
        $("#cw-fleet-monitoring-liveview-widget-footer-button>.live-view-view-on-map").each(function(){
            $(this).removeClass("active");
        });
        $(event.currentTarget).addClass("active");
    }

    renderListCollapseLiveView(){
        
        _.forEach(this.listItems().collapse, (obj, index)=>{
            swfobject.removeSWF(obj.liveViewId);
            if( (obj.brand === 3) || (obj.brand === 4)){
                var dfdInitPlugin  = this._initPlayer(obj.liveViewId, obj.cameraConfig, obj.allChannel);
                $.when(dfdInitPlugin).done((res)=>{
                    this._playAllCamera(obj.liveViewId,
                    obj.devices[0],
                    obj.sessionId,
                    obj.cameraNo)
                })
            }
        });
        setTimeout(()=>{
            $(".live-view-video").css({position: ""});
            $(".live-view-detail").css({display: ""});
        }, 1000);
    }

    renderListAllCamera(){
        let camera_id = "liveViewAllCamera";
        let camera_num = 0;
        let indexNumber = 0;
        let cameraConfig;

        _.forEach(this.listItems().collapse, (obj, index)=>{

                if( (obj.brand === 3) || (obj.brand === 4)){
                    camera_num += +obj.cameraNo;
                    cameraConfig = obj.cameraConfig;
                    
                }
                
    
            });

        
        swfobject.removeSWF(camera_id);

        var dfdInitPlugin = this._initPlayer(camera_id, cameraConfig , camera_num ,610,480);
        $.when(dfdInitPlugin).done((res)=>{
            _.forEach(this.listItems().collapse, (obj, index)=>{
                for (let i = 0; i < obj.cameraNo; i++) {
                    //swfobject.getObjectById(camera_id).startVideo(indexNumber, obj.sessionId, obj.devices[0].did, i, 1, true);
                    

                    this._playCamera_By_Channel(camera_id,
                        indexNumber,
                        obj.devices[0],
                        obj.sessionId,
                        i)

                    indexNumber++;
                }
            });
        });

    }

    renderListExpandLiveView(){
        _.forEach(this.listItems().expand, (obj, index)=>{
            _.forEach(obj.items, (item, index)=>{
                swfobject.removeSWF(item.liveViewId);
                if( (obj.brand === 3) || (obj.brand === 4)){
                    var dfdInitPlugin  = this._initPlayer(item.liveViewId, item.cameraConfig, item.allChannel);
                    $.when(dfdInitPlugin).done((res)=>{
                        this._playCamera(item.liveViewId,
                            item.devices[0],
                            item.sessionId,
                            item.channelNo)
                    })
                }
            });
        });
        setTimeout(()=>{
            $(".live-view-video").css({position: ""});
            $(".live-view-detail").css({display: ""});
            // _.forEach(this.listItems().expand, (obj, index)=>{
            //     setTimeout(()=>{
            //         _.forEach(obj.items, (item, index)=>{
            //             this._playCamera(item.liveViewId,
            //                 item.devices[0],
            //                 item.sessionId,
            //                 item.channelNo);
            //         });
            //     }, (index*10) * 1000);
            // });
        }, 1000);
    }

    renderGridLiveView(){
        _.forEach(this.gridItems(), (obj, index)=>{
            swfobject.removeSWF(obj.liveViewId);
            if( (obj.brand === 3) || (obj.brand === 4)){
 
                var dfdInitPlugin  = this._initPlayer(obj.liveViewId, obj.cameraConfig, obj.allChannel);
                $.when(dfdInitPlugin).done((res)=>{
                    this._playCamera(obj.liveViewId,
                    obj.devices[0],
                    obj.sessionId,
                    obj.channelNo)
                })
            }
        });
        //when not set flatplayer not init plugin
        setTimeout(()=>{
            $(".live-view-video").css({position: ""});
            $(".live-view-detail").css({display: ""});
        }, 1000);
    }

    _playCamera(windowId, dataCamera, sessionId, channel) {

        swfobject.getObjectById(windowId).setVideoInfo(0,"");
        swfobject.getObjectById(windowId).startVideo(0, sessionId, dataCamera.did, channel-1, 1, true);
        
    }
    
    _playCamera_By_Channel(windowId , index , dataCamera, sessionId, channel) {

        if (typeof dataCamera !== 'undefined'){
            swfobject.getObjectById(windowId).startVideo(index, sessionId , dataCamera.did ,channel, 1, true);
    
        }
    }

    _playAllCamera(windowId, dataCamera, sessionId, allChannel) {

        
        swfobject.getObjectById(windowId).stopVideo(0);
       
        if (typeof dataCamera !== 'undefined'){
            for (let n = 0; n < allChannel; n++) {
                //Play Video
                swfobject.getObjectById(windowId).setVideoInfo(n,"");
                swfobject.getObjectById(windowId).startVideo(n, sessionId, dataCamera.did, n, 1, true);
            }
        }
    
    }

    _initPlayer(windowId, config, channels , width = null , height = null){
        var dfdinitplayer = $.Deferred();
        var cameraConfig = config.split("|")
        var widthPlugin = (width) ? width : this.width;
        var heightPlugin = (height) ? height : this.height;
        var playerPath = (WebConfig.userSession.userType == Enums.UserType.BackOffice) ? "../vendors/iqtech/player.swf" : "vendors/iqtech/player.swf";
        var languagePath = (WebConfig.userSession.userType == Enums.UserType.BackOffice) ? "../vendors/iqtech/en.xml" : "vendors/iqtech/en.xml";

        // try {
            //Video plug-in init param
            var params = {
                allowFullscreen: "true",
                allowScriptAccess: "always",
                bgcolor: "#FFFFFF",
                wmode: "transparent"
            };
            
            //Init flash
            swfobject.embedSWF(playerPath, windowId, widthPlugin, heightPlugin, "9.0.0", null, null, params, null);

            
            switch ( channels ) {
                case 1:
                    var ObjChannel = 1; break;
                case 2:
                case 3:
                case 4:
                    var ObjChannel = 4; break;
                case 5:
                case 6:
                    var ObjChannel = 6; break;
                case 7:
                case 8:
                    var ObjChannel = 8; break;
                default:
                    var ObjChannel = 9; break;
            }

            setTimeout(() =>{
                //Setting the language video widget
                swfobject.getObjectById(windowId).setLanguage(languagePath);
                //First of all windows created
                swfobject.getObjectById(windowId).setWindowNum(36);
                //Re-configure the current number of windows
                 swfobject.getObjectById(windowId).setWindowNum(ObjChannel);
                //Set the video plug-in server
                let arrConfig = cameraConfig[2].split('//');
                swfobject.getObjectById(windowId).setServerInfo(arrConfig[1], "6605");
                dfdinitplayer.resolve()
            }, 1000)
            
        // } catch (ex) {
        //     console.log(ex);
        // }
        return dfdinitplayer;
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        let listCollapse = new Array();
        let listExpand = new Array();
        _.forEach(this.listLiveView(), (item)=>{
            item.width = this.width;
            item.height = this.height;
            item.liveViewId = "live-view-" + item.id
            item.chInfo = "CH " + item.cameraNo;
            item.allChannel = item.cameraNo;
            //set item to list collapse
            listCollapse.push(_.clone(item));

            //set item to list expand
            let liveViews = new Array();
            for (let index = 1; index <= item.cameraNo; index++) {
                let data = _.clone(item);
                data.liveViewId = "live-view-" + item.id + "-" + index;
                data.chInfo = "CH " + index;
                data.channelNo = index;
                data.allChannel = 1;
                liveViews.push(data);
                //set item to grid
                this.gridItems.push(data);
            }
            item.items = liveViews;
            listExpand.push(item);
        });

        this.listItems({
            collapse: listCollapse,
            expand: listExpand
        });
        
        setTimeout(()=>{
            this.renderListCollapseLiveView();
        }, 1000)
        

    }

    onUnload(){
        
    }

    onDomReady(){
        // var self = this;
        // var toolbarId = "#cw-fleet-monitoring-liveview-widget-toolbar :not(#btn-collapse)";
        // var toolbar = $(toolbarId + " i");

        // toolbar.click(function(){
        //     if(!$(this).hasClass("active")){
        //         toolbar.each(function(index, value){
        //             $(value).removeClass("active");
        //         });
        //         $(this).addClass("active");
        //     }
        // });
        
        // footerBtn.click(function(){
        //     if(!$(this).hasClass("active")){
        //         // let rowId = $(this).closest("[data-row-id]").data("row-id");
        //         // let state = $(this).attr("id");
        //         // //set collapse or expand
        //         // _.forEach(self.items(), function(obj){
        //         //     if(state == "liveview-collapse" && obj.id == rowId){
        //         //         obj.collapse(true);
        //         //     }else if(state == "liveview-expand" && obj.id == rowId){
        //         //         obj.collapse(false);
        //         //     }
        //         // });
                


        //         //set class active
        //         // let parentFooterBtn = $(this).closest(footerBtnId);
        //         // $(parentFooterBtn).find("i").each(function(index, value){
        //         //     $(value).removeClass("active");
        //         // });
        //         // $(this).addClass("active");
        //     }
        // });
    }

}

export default {
    viewModel: ScreenBaseWidget.createFactory(LiveViewWidget),
    template: templateMarkup
};
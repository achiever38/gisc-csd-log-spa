﻿import "jquery";
import ko from "knockout";
import ScreenBase from "../../../screenBase";
import templateMarkup from "text!./report.html";
import * as BladeSize from "../../../../../app/frameworks/constant/BladeSize";
import * as BladeType from "../../../../../app/frameworks/constant/BladeType";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";
import WebRequestFleetMonitoring from "../../../../../app/frameworks/data/apitrackingcore/webRequestFleetMonitoring";
import Utility from "../../../../../app/frameworks/core/utility";
import AssetInfo from "../../asset-info";
import {
    Constants,
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";

class TripAnalysisReportListScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(params.reportName);
        this.bladeSize = BladeSize.XLarge_Report;
        this.bladeType = BladeType.Compact;
        this.minimizeAll(false);
        this.bladeCanMaximize = false;
        // Prepare report configuration.
        this.reportUrl = this.resolveUrl(WebConfig.reportSession.reportUrl); 
        this.reportSource = params.reportSource;
        this.reportExport = params.reportExport;

    }


    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {

    }


    get webRequestFleetMonitoring() {
        return WebRequestFleetMonitoring.getInstance();
    }

    onDomReady () {

        var self = this;

        window.PlaybackDisplay = function(buId,vehicleId,strartDate,endDate,license){

            let filter = {
                companyId: WebConfig.userSession.currentCompanyId,
                businessUnitIds: [buId],
                vehicleIds: [vehicleId],
                deliveryManIds: [],
                startDate: strartDate,
                endDate: endDate,
                license: license
            };

            var dfd = $.Deferred();
            self.webRequestFleetMonitoring.findPlaybackTrackLocations(filter).done((result) => {

                let item = result["items"];
                var alertConfig = result["alertIcon"];
                var vehicleIcons = _.isEmpty(result["vehicleIcons"]) ? null : result["vehicleIcons"];

                let mapObj = {
                     mode: "ALL",
                     playback: [],
                     alert: []
                };

                var sortItems = _.sortBy(item, ['dateTime', 'id']);


                _.forEach(sortItems, (data) => {

                    var itemsID = data.vehicleId ? "V-" + data.vehicleId + "-PB" : "D-" + data.deliveryManId;
                    var obj = {
                        id: itemsID,
                        lat: data.latitude,
                        lon: data.longitude,
                        rotation: data.direction,
                        attributes: self.generateAttributes(data),
                    };
                    if (data.vehicleIconId) {
                        obj.pic = self.generateVehicleIconImageUrl(vehicleIcons[data.vehicleIconId], data.movement);
                    } else {
                        obj.pic = self.generateDeliveryIconImageUrl(data.movement);
                    }
                    if (data.movement) {
                        obj.movement = self.generateMovement(data.movement);
                    }

                    mapObj.playback.push(obj);

                    if (data.alertConfigurationId != null) {
                        mapObj.alert.push(self.generateAlertItem(alertConfig, data, obj.attributes.SHIPMENTINFO, obj.attributes.TITLE));
                    }

                });


                _.forEach(item, (data, index) => {
                    if (data.alertConfigurationId != null) {
                        item[index].alertURLInfo = alertConfig[data.alertConfigurationId]
                    }
                });

                self.sendPlaybackData(mapObj, item, filter);

                dfd.resolve();
            }).fail((e) => {
                dfd.reject();
            });

            self.minimizeAll();

        }
    }

    generateAttributes(data) {
        var isVehicle = data.vehicleId ? true : false;
        var datID = isVehicle ? "V-" + data.vehicleId + "-PB" : "D-" + data.deliveryManId + "-PB";
        var title = isVehicle ? data.vehicleLicense : data.username;
        var businessUnitName = "";

        // Detect business unit name based on vehicle or delivery men.
        if (isVehicle) {
            businessUnitName = _.map(_.filter(this.selectedVehicles, {
                id: data.vehicleId
            }), "businessUnitName");
        } else {
            businessUnitName = _.map(_.filter(this.selectedDeliveryMen, {
                id: data.deliveryManId
            }), "businessUnitName");
        }

        // Compose map attribute object.
        var attr = {
            ID: datID,
            TITLE: title,
            BOX_ID: data.vehicleId ? data.boxSerialNo : data.email,
            DRIVER_NAME: data.driverName,
            DEPT: businessUnitName,
            DATE: data.formatDateTime,
            TIME: Utility.getOnlyTime(data.dateTime, "HH:mm"),
            PARK_TIME: data.parkDuration,
            PARK_IDLE_TIME: data.idleTime,
            LOCATION: data.location,
            RAW: data,
            STATUS: AssetInfo.getStatuses(data, WebConfig.userSession),
            IS_DELAY: data.isDelay
        };

        attr.SHIPMENTINFO = AssetInfo.getShipmentInfo(data, attr.STATUS);

        return attr;
    }

    generateMovement(val) {

        switch (val) {
            case Enums.ModelData.MovementType.ParkEngineOn:
                val = "PARKENGINEON";
                break;
            case Enums.ModelData.MovementType.Move:
                val = "MOVE";
                break;
            case Enums.ModelData.MovementType.Park:
                val = "PARK";
                break;
            default:
                val = "STOP";
                break;
        }
        return val;
    }

    generateAlertItem(alertConfig, playbackItem, info, title) {
        var alertItem = {};

        alertItem.latitude = playbackItem.latitude;
        alertItem.longitude = playbackItem.longitude;


        if (alertConfig[playbackItem.alertConfigurationId] != undefined) {
            alertItem.attributes = _.clone(alertConfig[playbackItem.alertConfigurationId]);
        } else {
            alertItem.attributes = {
                imageUrl: "",
                description: ""
            }
        }
        alertItem.attributes.imageUrl = this.resolveUrl(alertItem.attributes.imageUrl);
        alertItem.attributes.TITLE = title;


        alertItem.attributes.SHIPMENTINFO = [{
            key: "alertdesc",
            title: this.i18n('wg-shipmentlegend-alertdesc')(),
            text: alertItem.attributes.description
        }].concat(_.clone(info));

        return alertItem;
    }

    sendPlaybackData(data, dataWidget, filter) {
        MapManager.getInstance().playbackFully(data);
        //switch (this.displayType().value) {
        //    case Enums.ModelData.PlaybackDisplayType.Moving:
        //        MapManager.getInstance().playbackAnimate(data);
        //        break;

        //    case Enums.ModelData.PlaybackDisplayType.AllTrackingTrails:
        //        MapManager.getInstance().playbackFootprint(data);
        //        break;
        //    case Enums.ModelData.PlaybackDisplayType.MovingAndTrails:
        //        MapManager.getInstance().playbackFully(data);
        //        break;
        //}
        //let vehicleLicense = this.txtvehicleLicense()

        let vehicleIds = filter.vehicleIds;
        let license = filter.license;

        this.widget('cw-fleet-monotoring-playback-info-widget', {
            dataWidget,
            vehicleIds,
            license
        }, {
                title: this.i18n("FleetMonitoring_Playback_Timeline_Widget")(),
                modal: false,
                resizable: false,
                target: "cw-fleet-monotoring-playback-info-widget",
                width: "450px",
                height: "85%",
                id: 'cw-fleet-monotoring-playback-info-widget'
            }
        );

        MapManager.getInstance().hideAllTrackVehicle();

    }

    generateVehicleIconImageUrl(data, compareValue) {
        var result = null;

        _.forEach(data, (d) => {
            if (d.movementType === compareValue) {
                result = {
                    N: d.imageUrl + "/n.png",
                    E: d.imageUrl + "/e.png",
                    W: d.imageUrl + "/w.png",
                    S: d.imageUrl + "/s.png",
                    NE: d.imageUrl + "/ne.png",
                    NW: d.imageUrl + "/nw.png",
                    SE: d.imageUrl + "/se.png",
                    SW: d.imageUrl + "/sw.png"
                }
            }
        });

        return result;
    }

    generateDeliveryIconImageUrl(movementType) {
        var movement = "";
        switch (movementType) {
            case Enums.ModelData.MovementType.Move:
                movement = "move";
                break;
            case Enums.ModelData.MovementType.Stop:
                movement = "stop";
                break;
            case Enums.ModelData.MovementType.Park:
                movement = "park";
                break;
        }

        var imageUrl = Utility.resolveUrl("/", "~/images/icon-walk-" + movement + "-direction-");

        return {
            N: imageUrl + "n.png",
            E: imageUrl + "e.png",
            W: imageUrl + "w.png",
            S: imageUrl + "s.png",
            NE: imageUrl + "ne.png",
            NW: imageUrl + "nw.png",
            SE: imageUrl + "se.png",
            SW: imageUrl + "sw.png"
        }
    }

    onLoad(isFirstLoad){

        if (!isFirstLoad) {
            return;
        }

    }

    onUnload () {
        window.PlaybackDisplay = null;
    }


}

export default {
    viewModel: ScreenBase.createFactory(TripAnalysisReportListScreen),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./playback.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import Utility from "../../../../../app/frameworks/core/utility";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicle";
import WebRequestUser from "../../../../../app/frameworks/data/apicore/webRequestUser";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import {Enums} from "../../../../../app/frameworks/constant/apiConstant";
import { BusinessUnitFilter } from "../../businessUnitFilter";
import AssetInfo from "../../asset-info";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";
import WebRequestDriveRule from "../../../../../app/frameworks/data/apitrackingcore/webRequestDriveRule";

class PlaybackScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("FleetMonitoring_Playback")());
        this.bladeSize = BladeSize.Medium;

        this.id = this.ensureNonObservable(params.id);
        this.keyword = ko.observable();
        this.startDate = ko.observable();
        this.endDate = ko.observable();
        this.txtStartTime = ko.observable("00:00");
        this.txtEndTime = ko.observable("23:59");
        this.today = ko.observable();
        this.maxEndDate = ko.pureComputed(() => {
            var maxEndDate = this.today();

            if (!_.isNil(this.startDate())) {
                var exceedDate = Utility.addDays(this.startDate(), 6);
                //if today > exceedDate (startdate + 6), then maxEndDate = exceedDate
                if (Utility.compareDateAsDays(this.today(), exceedDate) > 0) {
                    maxEndDate = exceedDate;
                }
            }

            return maxEndDate;
        });

        this.displayTypeOption = ko.observableArray();
        this.displayType = ko.observable();
        this.typeOption = ko.observableArray();
        this.type = ko.observable();

        //DT Vehicle
        this.vehicles = ko.observableArray([]);
        this.selectedVehicles = ko.observableArray([]);
        this.orderVehicle = ko.observable([[ 1, "asc" ]]);

        //DT Delivery Man
        this.deliveries  = ko.observableArray([]);
        this.selectedDeliveries = ko.observableArray([]);
        this.orderDelivery = ko.observable([[ 1, "asc" ]]);

        //BU
        this.businessUnit = ko.observable(null);
        this.businessUnitOptions = ko.observableArray([]);
        this.noneAccessibleBU = ko.observableArray([]);
        this.includeSubBusinessUnitEnable = ko.pureComputed(() => {return !_.isEmpty(this.businessUnit());});
        this.includeSubBusinessUnit = ko.observable(false);
        this.businessUnitFilter = new BusinessUnitFilter();

        this.driverPerformances = ko.observableArray([]);
         this.selectedDriverPerformance = ko.observable();

        this.findEnable = ko.pureComputed(() => {
            return this.businessUnit.isValid();
        });

        this.selectedAssets = ko.pureComputed(()=> {
            var merge = _.concat(this.selectedVehicles(), this.selectedDeliveries());
            return merge;
        });

        this.showType = ko.pureComputed(()=> {
            return this.displayType().value === Enums.ModelData.PlaybackDisplayType.AllTrackingTrails ? true : false; 
        });

        this.isVehicle = false;
        this.isDeliveryMan = false;
        this.selectedFromMap = ko.observable();

        this.ddrListHour = ko.observableArray();
        this.isRadioDisplayMode = ko.observable('full');
    }

    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    get webRequestVehicle() {
        return WebRequestVehicle.getInstance();
    }

    get webRequestUser() {
        return WebRequestUser.getInstance();
    }

    get webRequestDriveRule() {
        return WebRequestDriveRule.getInstance();
    }
    /**
     * Register observable property for change detection, validatiors.
     */
    setupExtend(){
        //trackChange
        //this.keyword.extend({ trackChange: true });
        //this.businessUnit.extend({ trackChange: true });

        //this.vehicles.extend({ trackArrayChange: true });
        //this.deliveries.extend({ trackArrayChange: true });

        //this.startDate.extend({ trackChange: true });
        //this.endDate.extend({ trackChange: true });
        //this.displayType.extend({ trackChange: true });
        //this.type.extend({ trackChange: true });

        //validation
        this.businessUnit.extend({ required: true });

        this.startDate.extend({ required: true });
        this.endDate.extend({ required: true });
        this.displayType.extend({ required: true });
        this.type.extend({ required: true });

        this.selectedAssets.extend({
            maxArrayLength: 3,
            arrayRequired: {
                params: true,
                message: this.i18n("M001")()
            }
        });

        this.validationModel = ko.validatedObservable({
            businessUnit: this.businessUnit,
            startDate: this.startDate,
            endDate: this.endDate,
            displayType: this.displayType,
            type: this.type,
            selectedAssets: this.selectedAssets
        });
    }

    /**
     * Find all selected business unit ids include child.
     * @returns 
     */
    findBusinessUnitIDs() {
        var result = [];

        // Default selection is single.
        if(this.includeSubBusinessUnitEnable()){
            var selectedBusinessUnitId = parseInt(this.businessUnit());

            // If user included sub children then return all business unit ids.
            if(this.includeSubBusinessUnit()){
                result = ScreenHelper.findBusinessUnitsById(this.businessUnitOptions(), selectedBusinessUnitId);
            }
            else {
                // Single selection for business unit.
                result = [selectedBusinessUnitId];
            }
        }

        return result;
    }

    /**
     * Find drivers and vehicles list.
     * 
     * @returns
     * 
     * @memberOf PlaybackScreen
     */
    findResult(){

        this.isBusy(true);
        this.selectedDeliveries.removeAll();
        this.selectedVehicles.removeAll();
        var dfd = $.Deferred();

        var vehicleFilter = {
            companyId: this.businessUnitFilter.companyId,
            businessUnitIds: this.findBusinessUnitIDs(),
            enable:true,
            isAssociatedWithFleetService: true,
            keyword: this.keyword()
        };

        var userFilter = {
            companyId: this.businessUnitFilter.companyId,
            businessUnitIds: this.findBusinessUnitIDs(),
            enable:true,
            userType: Enums.UserType.Company,
            isAssociatedWithMobileService: true,
            keyword: this.keyword()
        };
        var driverRuleFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            enable: true
        };
        var d1 = this.webRequestVehicle.listVehicleSummary(vehicleFilter);
        var d2 = this.webRequestUser.listUserSummary(userFilter);
        $.when(d1,d2).done((r1,r2) => {
            this.isBusy(false);
            var vehicles = this.getVehicleDatasource(r1["items"]);
            var deliveryMen = r2["items"];
        
            var id = this.selectedFromMap();

            if(this.isVehicle){
                _.each( vehicles, (v)=> {
                    if(v.id === id){
                        this.selectedVehicles.push(v);
                    }
                });

            }
            else if(this.isDeliveryMan){

                _.each( deliveryMen, (d)=> {
                    if(d.id === id){
                        this.selectedDeliveries.push(d);
                    }
                });
            }

            this.vehicles.replaceAll(vehicles);
            this.deliveries.replaceAll(deliveryMen);

            this.isDeliveryMan = false;
            this.isVehicle = false;

            dfd.resolve();
        }).fail((e)=> {
            this.isBusy(false);
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Prepare model for passing search timeline criteria. 
     * @returns
     */
    generateModel() {
        
        var model = {
            startDate: $.datepicker.formatDate("yy-mm-dd", new Date(this.startDate())) + " " + this.txtStartTime() + ":00",
            endDate: $.datepicker.formatDate("yy-mm-dd", new Date(this.endDate())) + " " + this.txtEndTime() + ":00",
            displayType: this.displayType(),
            type: this.showType() ? this.type() : null,
            selectedVehicles: [],
            selectedDeliveryMen: [],
            drivingRuleId: this.selectedDriverPerformance() ? this.selectedDriverPerformance().id : null,
        };

        switch (this.isRadioDisplayMode()) {
            case "full":
                    model.displayMode = Enums.ModelData.DisplayMode.Full;
                break;
            case "overview":
                    model.displayMode = Enums.ModelData.DisplayMode.Overview;
                break;
            case "overview-with-alert":
                    model.displayMode = Enums.ModelData.DisplayMode.OverviewWithAlert;
                break;
            default:
                    model.displayMode = Enums.ModelData.DisplayMode.Full;
                break;
        }

        // Find vehicle ids.
        _.forEach(this.selectedVehicles(), (v)=> {
            model.selectedVehicles.push(v);
        });

        // Find delivery man ids.
        _.forEach(this.selectedDeliveries(), (d)=> {
            model.selectedDeliveryMen.push(d);
        });

        return model;
    }

    setFormatDateTme(objDate, objTime) {
        var displayDateTime = "";
        if(objTime.value == undefined 
            || objTime.value == null
            || objTime.value == "") {
            displayDateTime = objDate;
        } else {
            let newDate = objDate.split("T")[0] + "T";
            let newTime = objDate.split("T")[1] = objTime.value + ":00:00";
            displayDateTime = newDate + newTime;
        }

        return displayDateTime
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        this.minimizeAll(false);

        var dfd = $.Deferred();

        var enumResourceFilter = {
            companyId: this.businessUnitFilter.companyId,
            types: [
                Enums.ModelData.EnumResourceType.PlaybackDisplayType,
                Enums.ModelData.EnumResourceType.PlaybackTrackingType
            ]
        };
        var driverRuleFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            enable: true
        }
        // Loop สร้างชั่วโมง Ddr 
        let formatTime = "";
        let lengthTime = 0;
        for(let hour=0;hour<24;hour++) {
            if(hour < 10) { 
                hour = "0" +  hour.toString();
            }
            formatTime = (hour.toString()) + ":00";
            this.ddrListHour.push({
                text : formatTime,
                value : hour
            });
        }
        lengthTime = this.ddrListHour().length;
        //this.txtEndTime(this.ddrListHour()[lengthTime - 1]); // set txtEndTime 23:00
        //End Loop สร้างชั่วโมง Ddr 

        var d0 ;//= null;

        if(this.id){
            var str = this.id;
            var prefix =  str.charAt(0);
            this.id = str.replace(prefix+'-','');

            switch (prefix) {
                case "V":
                    d0 = this.webRequestVehicle.getVehicle(this.id);
                    this.isVehicle = true;
                    break;
                case "D":
                    d0 = this.webRequestUser.getUser(this.id);
                    this.isDeliveryMan = true;
                    break;
            }

        }
        else{
            d0 = null;
        }

        var d1 = this.webRequestBusinessUnit.listBusinessUnitSummary(this.businessUnitFilter);
        var d2 = this.webRequestEnumResource.listEnumResource(enumResourceFilter);
        var d3 = this.webRequestDriveRule.listDriveRuleSummary(driverRuleFilter);

        $.when(d0, d1,d2,d3).done((r0, r1,r2,r3) => {
            
            var dfdFindResult = null;

            var bu = r1["items"];
            this.noneAccessibleBU(AssetInfo.getNoneAccessibleBU(bu));
            this.businessUnitOptions(bu);
            this.driverPerformances(r3.items);

            if(r0){
                this.businessUnit(r0.businessUnitId.toString());

                this.selectedFromMap(r0.id);
                dfdFindResult = this.findResult();
            }

            var resetDate = Utility.resetTime(r1["timestamp"]);

            this.startDate(resetDate);
            this.endDate(resetDate);
            this.today(resetDate);

            _.forEach(r2["items"], (result)=> {
                switch (result.type) {
                    case Enums.ModelData.EnumResourceType.PlaybackDisplayType:
                        this.displayTypeOption.push(result);
                        break;

                    case Enums.ModelData.EnumResourceType.PlaybackTrackingType:
                        this.typeOption.push(result);
                        break;
                }
            });

            let selected = this.displayTypeOption().filter((item)=> {
                if(item.value == Enums.ModelData.PlaybackDisplayType.AllTrackingTrails) {
                    return item
                }
            });
            this.displayType(selected[0]);

            var initialType = ScreenHelper.findOptionByProperty(this.typeOption, "value", Enums.ModelData.PlaybackTrackingType.AllTracking);
            this.type(initialType);

            $.when(dfdFindResult).done(()=>{
                dfd.resolve();
            });

           
        }).fail((e)=> {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        MapManager.getInstance().showAllTrackVehicle();
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actTimeline", this.i18n("Common_Timeline")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actTimeline") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            var model = this.generateModel();
            this.navigate("cw-fleet-monitoring-playback-timeline", model);
        }
    }

   
    /**
     * Format displayable vehicle.
     * @param {any} items
     * @returns
     */
    getVehicleDatasource(items) {
        Utility.applyFormattedPropertyToCollection(items, "vehicleModelDisplayText", "{0} - {1}", "brand", "model");
        return items;
    }
}

export default {
viewModel: ScreenBase.createFactory(PlaybackScreen),
    template: templateMarkup
};
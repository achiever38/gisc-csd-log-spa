﻿import ko from "knockout";
import templateMarkup from "text!./playback-info-widget.html";
import ScreenBaseWidget from "../../../screenbasewidget";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";

import Utility from "../../../../../app/frameworks/core/utility";


class PlaybackInfoWidget extends ScreenBaseWidget {
    constructor(params) {
        super(params);
        // Sample properties.
        this.dataWidget = params.dataWidget;
        this.dataVehicleIds = this.ensureObservable(params.vehicleIds);
        this.dataVehicleName = params.vehicleLicense;
        this.selectedIndex = ko.observable(0);
        this.arrayObj = ko.observableArray([]);

        this.selectTab1 = ko.observable(null);
        this.selectTab2 = ko.observable(null);
        this.selectTab3 = ko.observable(null);

        this.filterText1 = ko.observable();
        this.filterText2 = ko.observable();
        this.filterText3 = ko.observable();


        this.recentChangedRowIds1 = ko.observableArray([]);
        this.recentChangedRowIds2 = ko.observableArray([]);
        this.recentChangedRowIds3 = ko.observableArray([]);

        this.tab1 = ko.observable(true);
        this.tab2 = ko.observable(true);
        this.tab3 = ko.observable(true);
        this.tabName1 = ko.observable();
        this.tabName2 = ko.observable();
        this.tabName3 = ko.observable();
        this.dataSourcetab1 = ko.observableArray([]);
        this.dataSourcetab2 = ko.observableArray([]);
        this.dataSourcetab3 = ko.observableArray([]);

        this.order = ko.observable([]);

        this.goTotop = (data)=>{
            $('#'+data).animate({ scrollTop:0 }, 200);
        }
        


        // Sample2 properties.
        this.tab2Visible = ko.observable(true);

        this.onContainerScroll = (data) => {
            var scrollPos = document.getElementById("scrollbarid"+data).scrollTop;
            if (scrollPos >= 50){
                $("#btmTap"+data).show();
            }else{
                $("#btmTap"+data).hide();
            }
        }

    }
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;




       
        var obj = [];
        var key = {};
        this.dataVehicleIds().map((item, index) => {
            key[item] = index;
            obj[key[item]] = {
                id: item,
                vehicleLicense: null,
                data: []
            }
            
        });

        this.dataWidget.map((res, index) => {
            obj[key[res.vehicleId]].vehicleLicense = res.vehicleLicense;
            obj[key[res.vehicleId]].data.push(res)
        })
            //EX.Data for this.dataWidget
            // [
            //     {id:222,vehicleLicense:null,data:[]},
            //     {id:223,vehicleLicense:"xx-1234",data:[]}
            // ]
            // console.log(obj.filter((res) => {
            //     return res.vehicleLicense != null ? res:null
            // }));
        
        this.arrayObj.replaceAll(obj);

        if (this.arrayObj().length >= 1) {
            this.tab1(true)
        } else {
            this.tab1(false)
        }

        if (this.arrayObj().length >= 2) {
            this.tab2(true)
        } else {
            this.tab2(false)
        }

        if (this.arrayObj().length >= 3) {
            this.tab3(true)
        } else {
            this.tab3(false)
        }

        this.arrayObj().map((item, index) => {
            if(item.vehicleLicense){
                this['dataSourcetab' + parseInt(index + 1)](this.mapImg(item.data));
                this['tabName' + parseInt(index + 1)](item.vehicleLicense)
            }else{
                let txtVehicleLicese = this.dataVehicleName.filter((res) => {
                    return res.id == item.id ? res.license : null
                })
                this['tabName' + parseInt(index + 1)](txtVehicleLicese[0].license)

                
                    
            }
            
        });

        

        

    }
    mapImg(data) {
        data.map((item, index) => {
            if (item.alertConfigurationId != null) {
                item.alertURLInfo = item.alertURLInfo != undefined?this.tagImg(this.resolveUrl(item.alertURLInfo.imageUrl),item.alertURLInfo.descriptionText):null
            } else {
                item.alertURLInfo = null
            }
        });
        return data;

    }
    tagImg(data,altText, type, row) {
        let node = ""
        if (data != null) {
            node = '<img src="' +
                data + '" style="width: 25px; height: 25px;" alt="'+ altText +'" title="'+ altText +'">';
        }else{
            node = null
        }

        return node;
    }
    zoomAlert(data) {
        let latitude = data.latitude
        let longitude = data.longitude
        let LvZoom = 19
        let symbol = {
            url: this.resolveUrl("~/images/block_hilight.png"),
            width: '45',
            height: '45',
            offset: {
                x: 0,
                y: 0
            },
            rotation: 0
        }

        let location = {lat:data.latitude,lon:data.longitude}
   

        MapManager.getInstance().panOnePointZoom(null , location,symbol,LvZoom,null);
        //MapManager.getInstance().zoomPoint(null, latitude, longitude, LvZoom);
    }
    onClick() {
        console.log('tab demo click');
    }
    onToggleTab() {
        this.tab2Visible(!this.tab2Visible());
    }

    onEventSelected(event) {
        if(event !== null) {
            
            this.zoomAlert(event)
            return Utility.emptyDeferred();
        }
        return false;
    }
}

export default {
    viewModel: ScreenBaseWidget.createFactory(PlaybackInfoWidget),
    template: templateMarkup
};
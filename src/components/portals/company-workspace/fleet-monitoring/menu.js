﻿import ko from "knockout";
import templateMarkup from "text!./menu.html";
import ScreenBase from "../../screenbase";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import {Constants} from "../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";

/**
 * Display configuration menu based on user permission.
 * @class FleetMonitoringMenuScreen
 * @extends {ScreenBase}
 */
class FleetMonitoringMenuScreen extends ScreenBase {

    /**
     * Creates an instance of FleetMonitoringMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Menu_FleetMonitoring")());
        this.bladeMargin = BladeMargin.Narrow;
        this.bladeSize = BladeSize.XSmall;

        this.items = ko.observableArray([]);
        this.selectedIndex = ko.observable();

        this._selectingRowHandler = (item) => {
            if(item.page == "cw-fleet-monitoring-liveview-widget"){
                var liveWidget = this.widget(item.page, null, {
                    title: this.i18n('Live View Widget')(),
                    modal: false,
                    resizable: true,
                    target: item.page,
                    width:"30%",
                    height:"300"
                });
                return liveWidget;
            }else if (item) {
                return this.navigate(item.page);
            }
            return false;
        };
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.TrackMonitoring)) {
            this.items.push({
                text: this.i18n('FleetMonitoring_Track')(),
                page: 'cw-fleet-monitoring-track'
            });
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.ViewAlert)) {
            this.items.push({
                text: this.i18n('FleetMonitoring_Alert')(),
                page: 'cw-fleet-monitoring-alert'
            });
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.ViewPlayback)) {
            this.items.push({
                text: this.i18n('FleetMonitoring_Playback')(),
                page: 'cw-fleet-monitoring-playback'
            });
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.PlaybackAnalysisTool)) {
            this.items.push({
                text: this.i18n('FleetMonitoring_PlaybackAnalysisTools')(),
                page: 'cw-fleet-monitoring-playback-analysis-tool-search'
            });
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.LiveVideo)) {
            this.items.push({
                text: this.i18n('FleetMonitoring_Live_Video')(),
                page: 'cw-fleet-monitoring-liveview'
            });
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.EventVideo)) {
            this.items.push({
                text: this.i18n('FleetMonitoring_Event_Video')(),
                page: 'cw-fleet-monitoring-eventview'
            });
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.Debrief)) {
            this.items.push({
                text: this.i18n('FleetMonitoring_Debrief')(),
                page: "cw-fleet-monitoring-debrief"
            });
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.TripAnalysis)) {
            this.items.push({
                text: this.i18n('FleetMonitoring_TripAnalysis')(),
                page: "cw-fleet-monitoring-tripanalysis"
            });
        }

        // this.items.push({
        //     text: this.i18n('FleetMonitoring_Debrief')(),
        //     page: "cw-fleet-monitoring-debrief"
        // });

        if (WebConfig.appSettings.isDebug) {
           

            

            // this.items.push({
            //     text: "Live View Widget",
            //     page: 'cw-fleet-monitoring-liveview-widget'
            // });
        }
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedIndex(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}
}

export default {
    viewModel: ScreenBase.createFactory(FleetMonitoringMenuScreen),
    template: templateMarkup
};
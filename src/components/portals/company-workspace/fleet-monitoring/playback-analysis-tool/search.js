﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import { BusinessUnitFilter } from "../../businessUnitFilter";
import AssetInfo from "../../asset-info";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicle";
import Utility from "../../../../../app/frameworks/core/utility";
import moment from "moment";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";

class PlaybackAnalysisScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("FleetMonitoring_PlaybackAnalysisTools")());
        //BU
        this.businessUnit = ko.observable(null);
        this.businessUnitOptions = ko.observableArray([]);
        this.noneAccessibleBU = ko.observableArray([]);
        this.includeSubBusinessUnitEnable = ko.pureComputed(() => {return !_.isEmpty(this.businessUnit());});
        this.includeSubBusinessUnit = ko.observable(false);
        this.businessUnitFilter = new BusinessUnitFilter();
        
        //Vehicle
        this.vehicleInfos = ko.observableArray([]);
        this.selectedEntity = ko.observable();
        this.selectedVehicles = ko.observableArray([]);

        this.captionVehicle = ko.observable(this.i18n('Common_PleaseSelect')());

        //Sensore 
        this.sensorInfos = ko.observableArray([]);
        this.selectedSensors = ko.observableArray([]);

        this.order = ko.observable([]);

        //datetime
        this.formatSet = "dd/MM/yyyy";//WebConfig.companySettings.shortDateFormat;
        var addDay = 30;
        var addMonth = 3;
        var setTimeNow = ("0" + new Date().getHours()).slice(-2)+":"+("0" + new Date().getMinutes()).slice(-2);
        this.start = ko.observable(new Date());
        this.end = ko.observable(new Date());
        this.timeStart = ko.observable("00:00");
        this.timeEnd = ko.observable("23:59");
        this.maxDateStart = ko.observable(new Date());
        this.minDateEnd = ko.observable(new Date());
        this.minDateShipment = ko.observable(Utility.minusMonths(new Date(),addMonth));
        this.maxDateShipment = ko.observable(Utility.addMonths(new Date(),addMonth));

        this.findEnable = ko.pureComputed(() => {
            return this.businessUnit.isValid();
        });

        this.sensorInfos.subscribe(() => {
            this.initOnClickCheckbox();
        });

        this.showSensors = ko.pureComputed(() => {
            return _.size(this.sensorInfos()) > 0 ? true : false;
        });

        this.periodDay = ko.pureComputed((e) => {
            // can't select date more than current date
            //addDay = 30;
            var isDate = Utility.addDays(this.start(),1);
            let currentDate = parseInt(new Date() / (1000 * 60 * 60 * 24));
            let endDate = parseInt(isDate / (1000 * 60 * 60 * 24)) + 1;
            var calDay = endDate - currentDate;
            if(calDay > 0){
                isDate = new Date();
            }
     
            // set date when clear start date and end date
            if(this.start() === null){
                // set end date when clear start date
                if(this.end() === null){
                    
                    isDate = new Date();
                   
                }else{
                    isDate = this.end();
                }
            
                this.maxDateStart(isDate);
                this.minDateEnd(isDate);
          
            }else{
                //this.timeEnd("00:00");
                this.end(this.start());
                this.minDateEnd(this.start())
         
            }
            return isDate;
        });

        this.businessUnit.subscribe((val) => {
            this.vehicleInfos([]);
            this.sensorInfos([]);
            this.selectedSensors([]);
            this.includeSubBusinessUnit(false);
        });

        this.selectedEntity.subscribe((val) => {
            this.selectedSensors([]);
            if (val) {
                this.webRequestVehicle.getVehicleFeatures(val.id).done((res) => {
                    let data = res;
                    this.sensorInfos(data);
                }).fail((ex) => {
                    this.handleError(ex);
                });
            }else{
                this.sensorInfos([]);
            }
        });

        this.selectedEntityBinding = ko.pureComputed(() => {

            this.isBusy(true);
            var result = this.i18n('Common_PleaseSelect')();
            this.vehicleInfos([]);
            let buIds = this.findBusinessUnitIDs();
            if (_.size(buIds)) {

                this.webRequestVehicle.listVehicleSummary({
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: buIds,
                    enable: true,
                    isAssociatedWithFleetService: true,
                }).done((response) => {
                    //set displayName
                    response.items.forEach(function (arr) {
                        arr.displayName = arr.license;
                    });
                    let listSelectEntity = response.items;

                    this.vehicleInfos(listSelectEntity);
                    this.isBusy(false);
                });
            }  else {
                this.vehicleInfos([]);
                this.sensorInfos([]);
                this.isBusy(false);
            }

            return result;
        });
    }
    
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    get webRequestVehicle() {
        return WebRequestVehicle.getInstance();
    }

    setupExtend() {
        var self = this;

        this.businessUnit.extend({ trackChange: true });
        this.start.extend({ trackChange: true });
        this.end.extend({ trackChange: true });
        this.selectedEntity.extend({ trackChange: true });
        this.timeStart.extend({ trackChange: true });
        this.timeEnd.extend({ trackChange: true });
        this.selectedSensors.extend({ trackChange: true });


        //validation
        this.businessUnit.extend({ required: true });
        this.start.extend({ required: true });
        this.end.extend({ required: true });
        this.selectedEntity.extend({ required: true });
        this.timeStart.extend({ required: true });
        this.selectedSensors.extend({
            maxArrayPlaybackAnalysisLength: 10, //M252
            arrayRequired: {
                params: true,
                message: this.i18n("M001")()
            }
        });
        this.timeEnd.extend({
            required: true,
            validation: {
                validator: function (val) {
                    let invalidDiffDate = true;
                    if (self.start() != null) {
                        let strEndDateTime = moment(new Date(self.end())).format("YYYY-MM-DD") + " " + val;
                        let strStartDateTime = moment(new Date(self.start())).format("YYYY-MM-DD") + " " + self.timeStart();

                        let dateEnd = moment(strEndDateTime, "YYYY/MM/DD HH:mm");
                        let dateStart = moment(strStartDateTime, "YYYY/MM/DD HH:mm");

                        //if(dateEnd )
                        let diffHours = moment.duration(dateEnd.diff(dateStart)).asHours();
                        invalidDiffDate = diffHours > 24 ? false : true;
                    }
                    return invalidDiffDate;
                },
                message: this.i18n("M065")()
            }
        });

        this.validationModel = ko.validatedObservable({
            timeStart: this.timeStart,
            timeEnd: this.timeEnd,
            businessUnit: this.businessUnit,
            selectedEntity: this.selectedEntity,
            selectedSensors: this.selectedSensors
        });
       
    }

    /**
     * When onDomReady not found table, init onclick again
     */
    initOnClickCheckbox() {
        setTimeout(() => {
            $("#" + this.id + " tr.tbbody .checkbox")
                .off('click', this.onCheckboxClick.bind(this)) // remove handler
                .on('click', this.onCheckboxClick.bind(this)); // add handler

            this.initChecked();

        }, 500);
    }

    initChecked() {
        $("#" + this.id + " .checkbox input[type='checkbox']").each(function (index) {
            if (index < 10) {
                $(this).trigger("click");
            }
        });
    }


    onDomReady() {
        $("#" + this.id + " tr.tbheader .checkboxAll")
            .off('click', this.onCheckboxAllClick.bind(this)) // remove handler
            .on('click', this.onCheckboxAllClick.bind(this)); // add handler
    }

    onCheckboxAllClick(event) {
        if (event.target.type == 'checkbox') {
            // console.log("onCheckboxClick");

            let tbId = $(event.target).closest("table").attr("id");
            let checked = event.target.checked;

            $("#" + tbId + " .checkbox input[type='checkbox']").each(function () {
                // $(this).prop("checked", stateCheckBox);
                let stateCheckBox = $(this).is(':checked');
                if (!stateCheckBox && checked) {
                    $(this).trigger("click");
                } else if (stateCheckBox && !checked) {
                    $(this).trigger("click");
                }
            });
           
        }
    }
    
    /**
     * Function onclick checkbox of table
     */
    onCheckboxClick(event) {
        if (event.target.type == 'checkbox') {
            // console.log("onCheckboxClick");

            let tbId = $(event.target).closest("table").attr("id");
            let cbId = $(event.target).attr("id");
            let checked = event.target.checked;
            let selected = _.clone(this.selectedSensors());
            // if check is all then input checkBoxAll is equal check else input checkBoxAll is equal uncheck
            let isChecked = $("#" + tbId + " .checkbox").find("input[type='checkbox']:checked").length;
            let isCheckBox = $("#" + tbId + " .checkbox").find("input[type='checkbox']").length;

            if (isChecked === isCheckBox) {
                $("#" + tbId + " .checkboxAll").find("input[type='checkbox']").prop("checked", true);
            } else {
                $("#" + tbId + " .checkboxAll").find("input[type='checkbox']").prop("checked", false);
            }

            let fData = _.find(this.sensorInfos(), (item) => {
                return item.id == cbId;
            });

            if (checked) {
                selected.push(fData);
                $(event.target).closest("tr").addClass("selected");
            } else {
                selected = _.pullAll(selected, [fData]);
                $(event.target).closest("tr").removeClass("selected");
            }

            this.selectedSensors(selected);
        }
    }


    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;
        
        var d1 = this.webRequestBusinessUnit.listBusinessUnitSummary(this.businessUnitFilter);

        $.when(d1).done((r1) => {
            
            var bu = r1["items"];

            this.noneAccessibleBU(AssetInfo.getNoneAccessibleBU(bu));
            this.businessUnitOptions(bu);
        });
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actTimeline", this.i18n("Common_Timeline")()));
        actions.push(this.createActionCancel());
    }


    generateModel() {
        let strEndDateTime = moment(new Date(this.end())).format("YYYY-MM-DD") + " " + this.timeEnd()+":00";
        let strStartDateTime = moment(new Date(this.start())).format("YYYY-MM-DD") + " " + this.timeStart()+":00"; 

        return {
            businessUnitIds: [this.selectedEntity().businessUnitId],
            vehicleIds: [this.selectedEntity().id],
            startDate: strStartDateTime,
            endDate: strEndDateTime,
            featureIds: _.map(this.selectedSensors(), 'id')
        };


    }

    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actTimeline") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            let filter = this.generateModel();
            //console.log(filter);

            this.navigate("cw-fleet-monitoring-playback-analysis-tool", filter);
        }
    }


    findBusinessUnitIDs() {
        var result = [];

        // Default selection is single.
        if (this.includeSubBusinessUnitEnable()) {
            var selectedBusinessUnitId = parseInt(this.businessUnit());

            if(!isNaN(selectedBusinessUnitId)){
            // If user included sub children then return all business unit ids.
                if (this.includeSubBusinessUnit() && !isNaN(selectedBusinessUnitId)) {
                    result = ScreenHelper.findBusinessUnitsById(this.businessUnitOptions(), selectedBusinessUnitId);
                }
                else {
                    // Single selection for business unit.
                    result = [selectedBusinessUnitId];
                }
            }
        }

        return result;
    }

    //findResult() {

    //    this.isBusy(true);
    //    var dfd = $.Deferred();

    //    var vehicleFilter = {
    //        companyId: this.businessUnitFilter.companyId,
    //        businessUnitIds: this.findBusinessUnitIDs(),
    //        enable: true,
    //        isAssociatedWithFleetService: true,
    //    };


    //    var d1 = this.webRequestVehicle.listVehicleSummary(vehicleFilter);

    //    $.when(d1).done((r1) => {
    //        this.isBusy(false);
    //        var vehicles = r1["items"];

    //        this.vehicleInfos.replaceAll(vehicles);

    //        dfd.resolve();
    //    }).fail((e) => {
    //        this.isBusy(false);
    //        dfd.reject(e);
    //    });

    //    return dfd;
    //}
}


export default {
    viewModel: ScreenBase.createFactory(PlaybackAnalysisScreen),
    template: templateMarkup
};
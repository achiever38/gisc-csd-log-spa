﻿import ko from "knockout";
import templateMarkup from "text!./playback-analysis-tool.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeType from "../../../../../app/frameworks/constant/BladeType";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import {
    Constants,
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestFleetMonitoring from "../../../../../app/frameworks/data/apitrackingcore/webRequestFleetMonitoring";
import BinarySearchIndex from "./playback-analysis-tool-binary-search";
import AssetInfo from "../../asset-info";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";

class PlaybackAnalysisToolScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // params = '{"businessUnitIds":[2703],"vehicleIds":[14114],"startDate":"2019-09-09 00:00:00","endDate":"2019-09-09 23:59:00","featureIds":[18,110,113,111,114,115,109,108,112,117]}';
        // params = '{"businessUnitIds":[2703],"vehicleIds":[14114],"startDate":"2019-09-12 00:00:00","endDate":"2019-09-12 23:59:00","featureIds":[18,110,113,111,114,115,109,108,112,117]}';
        // params = '{"businessUnitIds":[2108],"vehicleIds":[10199],"startDate":"2019-09-12 00:00:00","endDate":"2019-09-12 23:59:00","featureIds":[18,1,7]}';
        // params = JSON.parse(params);
        // console.log(JSON.stringify(params));
        this.bladeTitle(this.i18n("Common_Timeline")());
        this.bladeSize = BladeSize.XLarge;
        this.bladeIsMaximized = true;
        this.bladeType = BladeType.Compact;
        this.params = this.ensureNonObservable(params, {});

        //auto height
        this.lineChartHeight = ko.observable();
        this.xyChartHeight = ko.observable();

        this.masterDataSource = ko.observable();
        this.dataSource = ko.observable();
        this.description = ko.observableArray([]);
        this.displayTitle = ko.observable();
        this.vehicleLicense = ko.observable();
        this.displayVehicleBrand = ko.observable();

        this.listenerEvent = ko.observable();
        this.listFeature = new Array();
        // this.enableChart = ko.observable("gantt");

    }

    get webRequestFleetMonitoring() {
        return WebRequestFleetMonitoring.getInstance();
    }

    createFormatDisplayDescription(result) {
        var model = [];
        let formatDescription = "";
        _.forEach(result, (item)=>{
            formatDescription = item.description + " " + item.formatTimes + " " + this.i18n("FleetMonitoring_Playback_Times")();
            model.push({
                displayDescription: formatDescription,
                text: item.text,
                style: {
                    backgroundColor: item.backgroundColor,
                    fontColor: item.fontColor
                }
            });
        });
        return model;
        //return formatDescription;
    }


    onTimelineZoom(data){
        let dataSource = _.clone(this.masterDataSource());
        let startDate = data.dateTime && data.dateTime.startDate;
        let endDate = data.dateTime && data.dateTime.endDate;
        if(this.isValidDate(startDate) && this.isValidDate(endDate)){
            var bs = new BinarySearchIndex(dataSource.features);
            let newData = bs.between(Date.parse(startDate), Date.parse(endDate));

            if(_.size(newData) > 2000){
                data.enableChart = "gantt";
            }else{
                data.enableChart = "xy";
                let options = {
                    dataSource: newData,
                    startDate: startDate,
                    endDate: endDate
                } 
                dataSource.features = this.makeFullSensors(options);
                this.dataSource(dataSource);
            }
        }else if(data.mode = "zoomOut"){
            data.enableChart = "gantt";
        }
        this.listenerEvent(data);
        
    }

    makeFullSensors(options){
        let ds = options.dataSource;
        if(_.size(ds)){
            let first = ds[0];
            let bs = new BinarySearchIndex(ds);
            let newData = bs.between(first.dateTimeInt, first.dateTimeInt);
            
            _.forEach(this.listFeature, (data)=>{
                
                let fData = _.filter(newData, function(o) { return o.featureId == data.featureId; });
                if(!_.size(fData)){ 
                    let item = new Object();
                    item.businessUnitName = null;
                    item.color = "rgba(255, 0, 0, 0)";
                    item.dateTime = new Date(first.dateTimeInt);
                    item.dateTimeInt = first.dateTimeInt;
                    item.formatDateTime = null;
                    item.driverName = null;
                    item.employeeId = null;
                    item.engine = null;
                    item.featureId = data.featureId;
                    item.featureName = data.featureName;
                    item.gpsStatus = null;
                    item.latitude = null;
                    item.longitude = null;
                    item.movement = null;
                    item.relatesFeature = [];
                    item.speed = 0;
                    ds.push(item);
                }
            });
        }else{

            _.forEach(this.listFeature, (data)=>{
                // for fake data array to chart
                // first item or start date
                let fItem = new Object();
                fItem.businessUnitName = null;
                fItem.color = "rgba(255, 0, 0, 0)";
                fItem.dateTime = new Date(options.startDate);
                fItem.dateTimeInt = Date.parse(new Date(options.startDate));
                fItem.formatDateTime = null;
                fItem.driverName = null;
                fItem.employeeId = null;
                fItem.engine = null;
                fItem.featureId = data.featureId;
                fItem.featureName = data.featureName;
                fItem.gpsStatus = null;
                fItem.latitude = null;
                fItem.longitude = null;
                fItem.movement = null;
                fItem.relatesFeature = [];
                fItem.speed = 0;
                ds.push(fItem);

                //last item or end date
                let lItem = new Object();
                lItem.businessUnitName = null;
                lItem.color = "rgba(255, 0, 0, 0)";
                lItem.dateTime = new Date(options.endDate);
                lItem.dateTimeInt = Date.parse(new Date(options.endDate));
                lItem.formatDateTime = null;
                lItem.driverName = null;
                lItem.employeeId = null;
                lItem.engine = null;
                lItem.featureId = data.featureId;
                lItem.featureName = data.featureName;
                lItem.gpsStatus = null;
                lItem.latitude = null;
                lItem.longitude = null;
                lItem.movement = null;
                lItem.relatesFeature = [];
                lItem.speed = 0;
                ds.push(lItem);
            });
            
        }

        let fData = ds[0];
        let lData = ds[_.size(ds - 1)];
        let fDateTime = Date.parse(new Date(options.startDate));
        let lDateTime = Date.parse(new Date(options.endDate));

        if(fData.dateTimeInt != fDateTime){
            let fItem = new Object();
            fItem.businessUnitName = null;
            fItem.color = "rgba(255, 0, 0, 0)";
            fItem.dateTime = new Date(options.startDate);
            fItem.dateTimeInt = fDateTime;
            fItem.formatDateTime = null;
            fItem.driverName = null;
            fItem.employeeId = null;
            fItem.engine = null;
            fItem.featureId = fData.featureId;
            fItem.featureName = fData.featureName;
            fItem.gpsStatus = null;
            fItem.latitude = null;
            fItem.longitude = null;
            fItem.movement = null;
            fItem.relatesFeature = [];
            fItem.speed = 0;
            ds.push(fItem);
        }

        if(lData.dateTimeInt != lDateTime){
            let fItem = new Object();
            fItem.businessUnitName = null;
            fItem.color = "rgba(255, 0, 0, 0)";
            fItem.dateTime = new Date(options.endDate);
            fItem.dateTimeInt = lDateTime;
            fItem.formatDateTime = null;
            fItem.driverName = null;
            fItem.employeeId = null;
            fItem.engine = null;
            fItem.featureId = lData.featureId;
            fItem.featureName = lData.featureName;
            fItem.gpsStatus = null;
            fItem.latitude = null;
            fItem.longitude = null;
            fItem.movement = null;
            fItem.relatesFeature = [];
            fItem.speed = 0;
            ds.push(fItem);
        }

        let sData = _.sortBy(ds, ['featureId']);
        
        return sData;
    }


    isValidDate(dateTime) {
        var d = new Date(dateTime);
        return d.getFullYear() && d.getMonth() && d.getDate();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        var dfd = $.Deferred();
        
        let filter = {
            companyId: WebConfig.userSession.currentCompanyId,
            businessUnitIds: this.params.businessUnitIds,
            vehicleIds: this.params.vehicleIds,
            startDate: this.params.startDate,
            endDate: this.params.endDate,
            featureIds: this.params.featureIds
        }
        this.webRequestFleetMonitoring.listPlaybackAnalysistool(filter).done((res)=>{
            if(_.size(res.items)){
                let item = res.items[0];
                //check os device
                var myRegex = new RegExp("iPhone|iPad", "i");
                var isdevice = myRegex.test(navigator.userAgent.toLowerCase());
                item.isIos = isdevice;
                this.masterDataSource(this.transformData(res));
                this.dataSource(_.clone(this.masterDataSource()));
                this.description(this.createFormatDisplayDescription(item.eventTimes));
                this.displayTitle(item.vehicleLicense + " (" + item.vehicleBrand + " - " + item.vehicleModel + ")");
                this.vehicleLicense(item.vehicleLicense);
                this.displayVehicleBrand(" (" + item.vehicleBrand + " - " + item.vehicleModel + ")");
            }
            dfd.resolve();
        });
        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        
    }

    transformData(res){
        let arrFeatureDetail = new Array();
        let arrFeaturePeriods = new Array();
        let items = new Array();
        
        if(_.size(res.items)){
            items = res.items[0];
            _.forEach(items.features, (obj, index)=>{
                this.listFeature.push({
                    featureId: obj.featureId, 
                    featureName: obj.featureName
                });

                _.forEach(obj.featureDetail, (item)=>{
                    item.dateTime = new Date(item.dateTime);
                    item.dateTimeInt = Date.parse(new Date(item.dateTime));
                    // item.formatDateTime = item.formatDateTime
                    item.color = (index % 2 == 0) ? "#19a47e" : "#4473c5";
                    item.featureId = obj.featureId;
                    item.featureName = obj.featureName;
                    arrFeatureDetail.push(item);
                });

                if(_.size(obj.featureDetail) === 0){
                    // for fake data array to chart
                    // first item or start date
                    let fItem = new Object();
                    fItem.businessUnitName = null;
                    fItem.color = "rgba(255, 0, 0, 0)";
                    fItem.dateTime = new Date(this.params.startDate);
                    fItem.dateTimeInt = Date.parse(new Date(this.params.startDate));
                    fItem.formatDateTime = null;
                    fItem.driverName = null;
                    fItem.employeeId = null;
                    fItem.engine = null;
                    fItem.featureId = obj.featureId;
                    fItem.featureName = obj.featureName;
                    fItem.gpsStatus = null;
                    fItem.latitude = null;
                    fItem.longitude = null;
                    fItem.movement = null;
                    fItem.relatesFeature = [];
                    fItem.speed = 0;
                    arrFeatureDetail.push(fItem);

                    //last item or end date
                    let lItem = new Object();
                    lItem.businessUnitName = null;
                    lItem.color = "rgba(255, 0, 0, 0)";
                    lItem.dateTime = new Date(this.params.endDate);
                    lItem.dateTimeInt = Date.parse(new Date(this.params.endDate));
                    lItem.formatDateTime = null;
                    lItem.driverName = null;
                    lItem.employeeId = null;
                    lItem.engine = null;
                    lItem.featureId = obj.featureId;
                    lItem.featureName = obj.featureName;
                    lItem.gpsStatus = null;
                    lItem.latitude = null;
                    lItem.longitude = null;
                    lItem.movement = null;
                    lItem.relatesFeature = [];
                    lItem.speed = 0;
                    arrFeatureDetail.push(lItem);
                }
            });

            _.forEach(items.featurePeriods, (obj, index)=>{
                _.forEach(obj.timePeriod, (item)=>{
                    item.id = obj.featureId;
                    item.startDateTime = new Date(item.startDateTime);
                    item.endDateTime = new Date(item.endDateTime);
                    // item.formatStartDateTime = item.formatStartDateTime;
                    // item.formatEndDateTime = item.formatEndDateTime;
                    item.name = obj.featureName;
                    item.color = (index % 2 == 0) ? "#19a47e" : "#4473c5";
                    arrFeaturePeriods.push(item);
                });

                if(_.size(obj.timePeriod) === 0){

                    let fItem = new Object();
                    fItem.id = obj.featureId;
                    fItem.startDateTime = new Date(this.params.startDate);
                    fItem.endDateTime = new Date(this.params.startDate).setSeconds(new Date(this.params.startDate).getSeconds() + 1);
                    fItem.formatStartDateTime = null;
                    fItem.formatEndDateTime = null;
                    fItem.name = obj.featureName;
                    fItem.color = "rgba(255, 0, 0, 0)";
                    arrFeaturePeriods.push(fItem);

                    let lItem = new Object();
                    let startDateTimeInt = new Date(this.params.endDate).setSeconds(new Date(this.params.endDate).getSeconds() - 1);
                    lItem.id = obj.featureId;
                    lItem.startDateTime = new Date(startDateTimeInt);
                    lItem.endDateTime = new Date(this.params.endDate);
                    lItem.formatStartDateTime = null;
                    lItem.formatEndDateTime = null;
                    lItem.name = obj.featureName;
                    lItem.color = "rgba(255, 0, 0, 0)";
                    arrFeaturePeriods.push(lItem);

                }

            });
        }
        // let dataFeatureDetail = Array.prototype.concat.apply([], arrFeatureDetail);
        // dataFeatureDetail = _.map(dataFeatureDetail, (item)=>{
        //     item.dateTime = new Date(item.dateTime);
        //     item.dateTimeInt = Date.parse(new Date(item.dateTime));
        //     return item;
        // });
        // data = Array.prototype.concat.apply([], [data, data, data, data]);
        
        arrFeatureDetail = _.sortBy(arrFeatureDetail, ['dateTimeInt']);
        

        //set end time
        // if(_.size(arrFeatureDetail)){
        //     let lastItem = arrFeatureDetail[_.size(arrFeatureDetail) - 1];
        //     let intEndDateTime = Date.parse(new Date(this.params.endDate));
        //     if(lastItem.dateTimeInt != intEndDateTime){
        //         let fItem = new Object();
        //         fItem.businessUnitName = null;
        //         fItem.color = "rgba(255, 0, 0, 0)";
        //         fItem.dateTime = new Date(this.params.endDate);
        //         fItem.dateTimeInt = intEndDateTime;
        //         fItem.formatDateTime = null;
        //         fItem.driverName = null;
        //         fItem.employeeId = null;
        //         fItem.engine = null;
        //         fItem.featureId = lastItem.featureId;
        //         fItem.featureName = lastItem.featureName;
        //         fItem.gpsStatus = null;
        //         fItem.latitude = null;
        //         fItem.longitude = null;
        //         fItem.movement = null;
        //         fItem.relatesFeature = [];
        //         fItem.speed = 0;
        //         arrFeatureDetail.push(fItem);
        //     }
        // }
        
        let lData = arrFeaturePeriods[_.size(arrFeaturePeriods - 1)];
        let eDateTime = Date.parse(new Date(this.params.endDate));

        if(Date.parse(lData.endDateTime) != eDateTime){
            let lItem = new Object();
            lItem.id = lData.featureId;
            lItem.startDateTime = new Date(this.params.endDate).setSeconds(new Date(this.params.endDate).getSeconds() - 1);
            lItem.endDateTime = new Date(this.params.endDate);
            lItem.formatStartDateTime = null;
            lItem.formatEndDateTime = null;
            lItem.name = lData.featureName;
            lItem.color = "rgba(255, 0, 0, 0)";
            arrFeaturePeriods.push(lItem);
        }

        items.features = arrFeatureDetail;
        items.featurePeriods = arrFeaturePeriods;
        return items;
    }

    onTimelineClickEvent(data) {
        var locationInfoStatus = AssetInfo.getLocationInfoStatus(data.locationInfo);
        var shipmentInfo = AssetInfo.getShipmentInfo(data.locationInfo, locationInfoStatus);
        var alertParam = {
            imageUrl: this.resolveUrl(data.imagePath),
            latitude: data.locationInfo.latitude,
            longitude: data.locationInfo.longitude,
            attributes: {
                TITLE: data.locationInfo.vehicleLicense,
                SHIPMENTINFO: [{
                    key: "alertdesc",
                    title: this.i18n('wg-shipmentlegend-alertdesc')(),
                    text: _.isNil(data.descriptionText) ? "-" : data.descriptionText
                }].concat(shipmentInfo)
            }
        };

        MapManager.getInstance().playbackDrawAlertChart(alertParam);
        this.minimizeAll();
    }

    onPlayBackAnalysisClick(data){
        let clickLocation = {
            lat: data.latitude,
            lon: data.longitude
        },
        symbol = {
            url: this.resolveUrl("~/images/pin_destination.png"),
            width: 35,
            height: 50,
            offset: {
                x: 0,
                y: 0
            },
            rotation: 0
        },
        zoom = 17,
        attributes = null

        MapManager.getInstance().drawOnePointZoom(this.id, clickLocation, symbol, zoom, attributes);
        this.minimizeAll();
    }

    setFormatDateTime(dt) {

        var newDateTime = "";

        let myDate = new Date(dt)
        let year = myDate.getFullYear().toString();
        let month = (myDate.getMonth() + 1).toString();
        let day = myDate.getDate().toString();
        let hour = myDate.getHours().toString();
        let minute = myDate.getMinutes().toString();
        let sec = myDate.getSeconds().toString();

        month = month.length > 1 ? month : "0" + month;
        day = day.length > 1 ? day : "0" + day;
        hour = hour.length > 1 ? hour : "0" + hour;
        minute = minute.length > 1 ? minute : "0" + minute;
        sec = sec.length > 1 ? sec : "0" + sec;
        newDateTime = day + "/" + month + "/" + year + " " + hour + ":" + minute + ":" + sec;

        return newDateTime;
    }

    /**
     * Set auto height of chart
     */
    chartAutoHeight(){
        let bladeContent = $(".app-blade-content-container").height();
        let paddingTop = 10;
        let title = 40;
        let paddingBottom = 10;
        let container = bladeContent - paddingTop - title - paddingBottom;
        let lineChartHeight = (container * 50) / 100;
        let xyChartHeight = (container * 50) / 100;
        lineChartHeight = (lineChartHeight > 405) ? lineChartHeight : 405;
        xyChartHeight = (xyChartHeight > 405) ? xyChartHeight : 405;
        this.lineChartHeight(lineChartHeight);
        this.xyChartHeight(xyChartHeight);
    }

    /**
     * When document ready
     */
    onDomReady(){
        this.chartAutoHeight();
    }

}

export default {
    viewModel: ScreenBase.createFactory(PlaybackAnalysisToolScreen),
    template: templateMarkup
};
import ko from "knockout";
import ScreenBase from "../screenbase";
import Utility from "../../../app/frameworks/core/utility";

class GenerateDataGridExpand {

    
    //data soruce
    static getDataSource(webreqest, pageSize = 5) 
    {
        var dataSource = {
                transport: {
                    read: webreqest,
                    // conver params to JSON.stringify and pass to service
                    parameterMap: function(options, operation) {
                        return JSON.stringify(options);
                    }
                },
                // server side
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                // default page size onload
                pageSize: pageSize,
                schema: {
                    model: {
                        id: "id"
                    },
                    // set list data
                    data: function(r) {
                        return r.items;
                    },
                    // set total reccord
                    total: function(r) { 
                        return r.totalRecords;
                    }
                }
            }
        return dataSource;
    }
    
    // pagination
    static getPageable (pageSizes = false)
    {   
        var pageable = false;
        if(pageSizes){
            pageable = {
                pageSizes: pageSizes, //[5, 10, 20]
                refresh: false,
                buttonCount: 5
            }
        }
        return pageable;
    }

    static getColumns (obj = [])
    {
        var self = this;
        var columns = [];
        if(obj.length > 0){
            _.forEach(obj, function(val) {
                //create column
                
                var column = {
                    field: Utility.extractParam(val.data),
                    headerTemplate: Utility.extractParam(val.headerTemplate, Utility.extractPossibleI18NParam(val.title, "")),
                    title: Utility.extractPossibleI18NParam(val.title, ""),
                    width: Utility.extractParam(val.width),
                    format: Utility.extractParam(val.format),
                    filterable: false,
                    sortable: false,
                    hidden: Utility.extractParam(val.hidden, false),
                    template: val.template
                };

                //set thbody column attributes onclick
                switch (typeof val.onClick) {
                    case "function" : {

                        if(typeof val.className == "string"){

                            column.attributes = {
                                "data-grid": "table-column-cell-onclick-expand",
                                class: val.className + " k-link"
                            }
                            // self.registerColumnEvents(val.data, "click", val.onClick);

                        }else{
                            //set thbody column
                            column.attributes = {
                                "data-grid": "table-column-cell-onclick-expand",
                                class: " k-link"
                            }
                            // self.registerColumnEvents(val.data, "click", val.onClick);
                        }

                        break;
                    }
                }
                
                //set column attributes class
                switch (typeof val.className) {
                    case "string": {

                        //set thbody column attributes onclick
                        if (typeof val.onClick == "function") {

                            column.attributes = {
                                "data-grid": "table-column-cell-onclick-expand",
                                class: val.className + " k-link"
                            }
                            // self.registerColumnEvents(val.data, "click", val.onClick);

                        }else{

                            //set thbody column
                            column.attributes = {
                                class: val.className
                            }

                        }
                        
                        break;
                    }
                }

                if(val.columns){
                    // for generate column merge
                    column.columns = GenerateDataGridExpand.getColumns(val.columns);
                }

                columns.push(column);
            });
        }
        return columns;
    }


    static getOnClickColumns(obj = []){
        var columns = [];
        if(obj.length > 0){
            _.forEach(obj, function(val) {
                //header merge1
                if (typeof val.onClick == "function") {
                    columns.push({
                        data:val.data,
                        title:val.title,
                        onClick:val.onClick
                    });
                }

                //header merge2
                if(val.columns){
                    _.forEach(val.columns, function(val) {
                        if (typeof val.onClick == "function") {
                            columns.push({
                                data:val.data,
                                title:val.title,
                                onClick:val.onClick
                            });
                        }
                        //header merge3
                        if(val.columns){
                            _.forEach(val.columns, function(val) {
                                if (typeof val.onClick == "function") {
                                    columns.push({
                                        data:val.data,
                                        title:val.title,
                                        onClick:val.onClick
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
        return columns;
    }
    
}

export default GenerateDataGridExpand;



﻿import "jquery";
import ko from "knockout";
import ScreenBase from "../../../screenBase";
import templateMarkup from "text!./view.html";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebRequestAreaCategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomAreaCategory";
import {
    Constants,
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";

class ViewAreaCategoriesScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("ViewAreaCategories_Title")());

        this.areaId = this.ensureNonObservable(params.id, -1);
        this.name = ko.observable();
        this.description = ko.observable();
        this.code = ko.observable();
        this.areaColor = ko.observable();
        this.type = ko.observable();

        this.subscribeMessage("cw-geo-fencing-custom-area-category-changed", (id) => {
            this.webRequestAreaCategory.getCustomAreaCategory(id).done((response) => {
                var name = response["name"];
                var description = response["description"];
                var code = response["code"];
                var areaColor = response["color"];
                var type = response["areaCategoryTypeDisplayName"] ? response["areaCategoryTypeDisplayName"] : "-";
                this.name(name);
                this.description(description);
                this.code(code);
                this.areaColor(areaColor);
                this.type(type);
            })
        });
    }

    /**
     * Get WebRequest specific for Subscription module in Web API access.
     * @readonly
     */
    get webRequestAreaCategory() {
            return WebRequestAreaCategory.getInstance();
        }
    /**
      * @lifecycle Called when View is loaded.
      * @param {boolean} isFirstLoad true if ViewModel is first load
      */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        var dfd = $.Deferred();
        this.webRequestAreaCategory.getCustomAreaCategory(this.areaId).done((response) => {
            var name = response["name"];
            var description = response["description"];
            var code = response["code"];
            var areaColor = response["color"];
            var type = response["areaCategoryTypeDisplayName"] ? response["areaCategoryTypeDisplayName"] : "-";
            this.name(name);
            this.description(description);
            this.code(code);
            this.areaColor(areaColor);
            this.type(type);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        //  this.selectedSubscription(null);
    }

    /**
     * Life cycle: create command bar
     * @public
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.UpdateCustomAreaCategory)){
            commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.DeleteCustomAreaCategory)){
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }

    }
    /**
      * @lifecycle Handle when button on ActionBar is clicked.
      * @param {any} commands
      */
    onActionClick(sender) {
            super.onActionClick(sender);
        }
    /**
      * @lifecycle Hangle when command on CommandBar is clicked.
      * @param {any} sender
      */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdUpdate":
                this.navigate("cw-geo-fencing-custom-area-category-manage", {
                    mode: "update",
                    id: this.areaId
                });
                break;
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M053")(),
                    BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.webRequestAreaCategory.deleteCustomAreaCategory(this.areaId).done(() => {
                                this.publishMessage("cw-geo-fencing-custom-area-category-delete");
                                this.close(true);
                            }).fail((e) => {
                                this.handleError(e);
                            });
                            break;
                    }
                });
                break;
        }
    }

}

export default {
    viewModel: ScreenBase.createFactory(ViewAreaCategoriesScreen),
    template: templateMarkup
};
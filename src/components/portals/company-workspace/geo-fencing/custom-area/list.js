﻿import "jquery";
import ko from "knockout";
import ScreenBase from "../../../screenBase";
import templateMarkup from "text!./list.html";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestCustomArea from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomArea";
import {
    Constants,
    Enums,
    EntityAssociation
} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import * as EventAggregatorConstant from "../../../../../app/frameworks/constant/eventAggregator";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import Utility from "../../../../../app/frameworks/core/utility";
import ScreenHelper from "../../../screenhelper";

class CustomAreaScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("CustomArea_CustomArea")());
        this.bladeSize = BladeSize.XLarge;
        this.customArea = ko.observableArray();
        this.order = ko.observable([
            [1, "asc"]
        ]);
        this.filterText = ko.observable();
        this.selectedCustomArea = ko.observable();
        this.order = ko.observable([
            [1, "asc"]
        ]);
        this.areaUnit = ko.observable();
        this.areaUnitType = ko.observable(1);
        this.recentChangedRowIds = ko.observableArray();
        // Determine if selecting action is legal, return false to cancel selecting event.
        this._selectingRowHandler = (customArea) => {
            if (customArea) {
                return this.navigate("cw-geo-fencing-custom-area-view", {
                    id: customArea.id
                });
            }
            return false;
        };

        this.filterState = ko.observable({
            CompanyId: WebConfig.userSession.currentCompanyId,
            CustomAreaTypeIds: [],
            Keyword: null,
            FromCreateDate: null,
            ToCreateDate: null,
            CreateBy: ""
        });

        // Observe change about filter
        this.subscribeMessage("cw-geo-fencing-custom-area-filter-changed", (newFilter) => {
            this.filterState(newFilter);

            this.webRequestCustomArea.listCustomAreaSummary(this.filterState()).done((data) => {

                var items = data["items"];
                var viewonMap = {
                    viewonMap: this.resolveUrl("~/images/ic-search.svg")
                };
                if (items.length > 0) {
                    var areaUnitType = this.areaUnitType();
                    ko.utils.arrayForEach(items, function(value, index) {
                        value = $.extend(value, viewonMap);
                        value.size = Utility.convertAreaUnit(value.size, areaUnitType);
                        //value= Object.assign(value, viewonMap);
                    });
                }
                this.customArea.replaceAll(items);

            });
        });

        this.subscribeMessage("cw-geo-fencing-custom-area-changed", (id) => {
            var params = {
                CompanyId: WebConfig.userSession.currentCompanyId,
                Keyword: null,
                CustomAreaTypeIds: [],
                FromCreateDate: null,
                ToCreateDate: null,
                CreateBy: null
            }
            this.webRequestCustomArea.listCustomAreaSummary(params).done((response) => {
                var items = response["items"];
                var viewonMap = {
                    viewonMap: this.resolveUrl("~/images/ic-search.svg")
                };
                if (items.length > 0) {
                    var areaUnitType = this.areaUnitType();
                    ko.utils.arrayForEach(items, function (value, index) {
                        value = $.extend(value, viewonMap);
                        value.size = Utility.convertAreaUnit(value.size, areaUnitType);
                    });
                }

                this.customArea.replaceAll(items);

                this.recentChangedRowIds.replaceAll([id]);
            });
        });

        this.subscribeMessage("cw-geo-fencing-custom-area-delete", (id) => {
            var params = {
                CompanyId: WebConfig.userSession.currentCompanyId,
                Keyword: null,
                CustomAreaTypeIds: [],
                FromCreateDate: null,
                ToCreateDate: null,
                CreateBy: null
            }
            this.webRequestCustomArea.listCustomAreaSummary(params).done((response) => {
                var items = response["items"];
                var viewonMap = {
                    viewonMap: this.resolveUrl("~/images/ic-search.svg")
                };
                if (items.length > 0) {
                    var areaUnitType = this.areaUnitType();
                    ko.utils.arrayForEach(items, function (value, index) {
                        value = $.extend(value, viewonMap);
                        value.size = Utility.convertAreaUnit(value.size, areaUnitType);
                    });
                }

                this.customArea.replaceAll(items);

                this.recentChangedRowIds.replaceAll([id]);
            });
        });

        this.subscribeMessage("cw-geo-fencing-custom-area-delete", (id) => {
            var params = {
                CompanyId: WebConfig.userSession.currentCompanyId,
                Keyword: null,
                CustomAreaTypeIds: [],
                FromCreateDate: null,
                ToCreateDate: null,
                CreateBy: null
            }
            this.webRequestCustomArea.listCustomAreaSummary(params).done((response) => {
                var items = response["items"];
                var viewonMap = {
                    viewonMap: this.resolveUrl("~/images/ic-search.svg")
                };
                if (items.length > 0) {
                    var areaUnitType = this.areaUnitType();
                    ko.utils.arrayForEach(items, function(value, index) {
                        value = $.extend(value, viewonMap);
                        value.size = Utility.convertAreaUnit(value.size, areaUnitType);
                    });
                }

                this.customArea.replaceAll(items);

                this.recentChangedRowIds.replaceAll([id]);
            });
        });



        // Set custom column title with DataTable
        this.onCustomAreaListTableReady = () => {

            if (!_.isEmpty(this.areaUnit())) {
                this.dispatchEvent("dtCustomAreaList", "setColumnTitle", [{
                    columnIndex: 5,
                    columnTitle: this.i18n("CustomArea_Size_2", [this.areaUnit()])
                }]);
            }
        };

        this.viewMapClick = (data) => {

            let obj = JSON.parse(data.points);
            let fillColor = this.rgbTohex(data.symbolFillColor);
            let borderColor = this.rgbTohex(data.symbolBorderColor);
            MapManager.getInstance().drawOnePolygonZoom(this.id, obj, fillColor, borderColor, 0.5, 1, data);
            this.minimizeAll();
        };
        MapManager.getInstance().setFollowTracking(false);
    }

    /**
     * Get WebRequest specific for Company module in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }


    /**
     * Get WebRequest specific for Subscription module in Web API access.
     * @readonly
     */
    get webRequestCustomArea() {
        return WebRequestCustomArea.getInstance();
    }

    renderViewnOnMapColumn(data, type, row) {
            let node = '<img src=' +
                data + ' style="cursor:pointer">';
            return node;
        }
    /**
    convert hex format to a rgb color
    */
    rgbTohex(rgb) {

        let objRgb = rgb.split(",");
        return "#" + this.hex(objRgb[0]) + this.hex(objRgb[1]) + this.hex(objRgb[2]);
    }

    hex(x) {
        let hexDigits = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f");
        return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        var dfd = $.Deferred();
        var params = {
            CompanyId: WebConfig.userSession.currentCompanyId,
            Keyword: null,
            CustomAreaTypeIds: [],
            FromCreateDate: null,
            ToCreateDate: null,
            CreateBy: null
        }

        var dfdCompanySettings = this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId);
        var dfdCustomAreaList = this.webRequestCustomArea.listCustomAreaSummary(params);

        $.when(dfdCompanySettings, dfdCustomAreaList).done((r1,r2) => {
            this.areaUnit(r1.areaUnitSymbol);
            this.areaUnitType(r1.areaUnit);

            var items = r2["items"];
            var viewonMap = {
                viewonMap: this.resolveUrl("~/images/ic-search.svg")
            };
            if (items.length > 0) {

                var areaUnitType = this.areaUnitType();
                ko.utils.arrayForEach(items, function (value, index) {

                    //console.log("value",value);

                    value = $.extend(value, viewonMap);
                    value.size = Utility.convertAreaUnit(value.size, areaUnitType);
                });
            }

            this.customArea(items);

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });


        return dfd;
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        //  this.selectedSubscription(null);
    }

    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.CreateCustomArea)){
            commands.push(this.createCommand("cmdCreate", this.i18n("CustomArea_Create")(), "svg-cmd-add"));
        }

        commands.push(this.createCommand("cmdFilter", this.i18n("CustomArea_Filters")(), "svg-cmd-search"));
        commands.push(this.createCommand("cmdExport", this.i18n("CustomArea_Export")(), "svg-cmd-export"));
    }

    onCommandClick(sender) {

        switch (sender.id) {

            case "cmdCreate":
                this.navigate("cw-geo-fencing-custom-area-manage", {
                    mode: "create"
                });
                break;
            case "cmdFilter":
                this.navigate("cw-geo-fencing-custom-area-search", {
                    filterState: this.filterState()
                });
                break;
            case "cmdExport":
                this.showMessageBox(null, this.i18n("M128")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.filterState().templateFileType = Utility.isiOSDevice() ? Enums.TemplateFileType.Xls : Enums.TemplateFileType.Xlsx;
                            this.webRequestCustomArea.exportCustomArea(this.filterState()).done((response) => {
                                this.isBusy(false);
                                ScreenHelper.downloadFile(response.fileUrl);
                            }).fail((e) => {
                                this.isBusy(false);
                                this.handleError(e);
                            });
                            break;
                    }
                });
                break;
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(CustomAreaScreen),
    template: templateMarkup
};
﻿import "jquery";
import ko from "knockout";
import ScreenBase from "../../../screenBase";
import ScreenHelper from "../../../screenhelper";
import templateMarkup from "text!./manage.html";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebRequestPOICategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomPOICategory";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import {
    Constants,
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";


class ManagePOICategoriesScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.mode = this.ensureNonObservable(params.mode);
        if (this.mode === "create") {
            this.bladeTitle(this.i18n("CreatePOICategories_Title")());
        } else if (this.mode === "update") {
            this.bladeTitle(this.i18n("UpdatePOICategories_Title")());
        }
        this.poiId = this.ensureNonObservable(params.id, -1);
        this.name = ko.observable();
        this.code = ko.observable();
        this.description = ko.observable();
        this.color = ko.observable();

        this.typeOptions = ko.observableArray();
        this.type = ko.observable();
        // Set datasource to DataTable control

        // To preserve state when journey is inactive.
    }

    /**
     * Get WebRequest specific for Subscription module in Web API access.
     * @readonly
     */
    get webRequestPOICategory() {
            return WebRequestPOICategory.getInstance();
    }


    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }


    /**
      * @lifecycle Called when View is loaded.
      * @param {boolean} isFirstLoad true if ViewModel is first load
      */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        var dfd = $.Deferred();


        this.isBusy(true);
        var dfdPOIType = this.webRequestEnumResource.listEnumResource({
            companyId: WebConfig.userSession.currentCompanyId,
            types: [Enums.ModelData.EnumResourceType.PoiCategoryType]
        });
        var dfdGetCustomPOICat = null;
        
        if (this.mode === "update") {
            dfdGetCustomPOICat = this.webRequestPOICategory.getCustomPOICategory(this.poiId);
        }

        $.when(dfdPOIType, dfdGetCustomPOICat).done((resPOICatType, resDataPOICat) => {
            if (resPOICatType) {
                this.typeOptions(resPOICatType.items);
            }
            if (resDataPOICat){
                var name = resDataPOICat["name"];
                var description = resDataPOICat["description"];
                var code = resDataPOICat["code"];
                var color = resDataPOICat["color"];

                this.name(name);
                this.description(description);
                this.code(code);
                this.color(color);
                
                var type = !_.isNil(resDataPOICat.poiCategoryType) ? ScreenHelper.findOptionByProperty(this.typeOptions, "value", resDataPOICat.poiCategoryType) : null;
                this.type(type);
            }
            dfd.resolve();
        }).fail((e) => {
            this.handleError(e);
            this.isBusy(false);
            dfd.reject(e);
        }).always(() => {
            this.isBusy(false);
        });


        return dfd;
    }
    onUnload() {}

    setupExtend() {
        this.name.extend({
            trackChange: true
        });
        this.color.extend({
            trackChange: true
        });
        this.code.extend({
            trackChange: true
        });

        this.name.extend({
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });

        this.code.extend({
            serverValidate: {
                params: "Code",
                message: this.i18n("M010")()
            }
        });
        this.color.extend({
            required: true
        });

        this.type.extend({
            required: true
        });

        this.validationModel = ko.validatedObservable({
            name: this.name,
            color: this.color,
            type: this.type
        });
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        //  this.selectedSubscription(null);
    }

    /**
     * Life cycle: create command bar
     * @public
     * @param {any} commands
     */
    buildCommandBar(commands) {

    }

    generateModel() {
            if (this.mode === "create") {
                var model = {
                    companyId: WebConfig.userSession.currentCompanyId,
                    name: this.name() ? this.name() : null,
                    description: this.description() ? this.description() : null,
                    code: this.code() ? this.code() : null,
                    color: this.color() ? this.color() : null,
                    poiCategoryType: this.type() ? this.type().value  : null
                };
            } else if (this.mode === "update") {
                var model = {
                    id: this.poiId,
                    companyId: WebConfig.userSession.currentCompanyId,
                    name: this.name() ? this.name() : null,
                    description: this.description() ? this.description() : null,
                    code: this.code() ? this.code() : null,
                    color: this.color() ? this.color() : null,
                    poiCategoryType: this.type() ? this.type().value : null
                };
            }
            return model;
        }
        /**
         * Life cycle: handle command click
         * @public
         * @param {any} sender
         */
    onCommandClick(sender) {

    }

    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }

    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
            var model = this.generateModel();
            switch (this.mode) {
                case "create":
                    this.isBusy(true);
                    this.webRequestPOICategory.createCustomPOICategory(model).done((response) => {
                        this.publishMessage("cw-geo-fencing-custom-poi-category-changed");
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });
                    break;
                case "update":
                    this.isBusy(true);
                    this.webRequestPOICategory.updateCustomPOICategory(model).done((response) => {
                        this.publishMessage("cw-geo-fencing-custom-poi-category-changed", this.poiId);
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });
                    break;
            }
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(ManagePOICategoriesScreen),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import ScreenBase from "../../../screenBase";
import ScreenHelper from "../../../screenhelper";
import WebRequestCountry from "../../../../../app/frameworks/data/apicore/webrequestCountry";
import WebRequestProvince from "../../../../../app/frameworks/data/apicore/webrequestProvince";
import WebRequestCity from "../../../../../app/frameworks/data/apicore/webrequestCity";
import WebRequestTown from "../../../../../app/frameworks/data/apicore/webrequestTown";
import WebRequestPoiIcon from "../../../../../app/frameworks/data/apitrackingcore/webRequestPoiIcon";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import Utility from "../../../../../app/frameworks/core/utility";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import {
    Constants,
    Enums,
    EntityAssociation
} from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestCustomAreaType from "../../../../../app/frameworks/data/apitrackingcore/webrequestCustomAreaType";

class CustomPOIFilterScreen extends ScreenBase {

    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("CustomRoutes_Filters_Title")());

        //this.bladeSize = BladeSize.Medium;

        this.iconOptions = ko.observableArray([]);
        this.countryOptions = ko.observableArray();
        this.provinceOptions = ko.observableArray();
        this.cityOptions = ko.observableArray();
        this.townOptions = ko.observableArray();
        this.dateFormat = ko.observable("yyyy-MM-dd");
        this.createDateFrom = ko.observable();
        this.createDateTo = ko.observable();
        this.createBy = ko.observable();

        this.name = ko.observable();
        this.code = ko.observable();
        this.subPoiName = ko.observable();
        this.subPoiCode = ko.observable();

        this.subPoiTypeList = ko.observableArray();
        this.subPoiType = ko.observable();
        this.isSubArea = ko.observable(false);
        this.isDisable = ko.observable(false);

        this.country = ko.observable();
        this.province = ko.observable();
        this.city = ko.observable();
        this.town = ko.observable();
        this.iconSelectedItem = ko.observable();


        this.filterState = this.ensureNonObservable(params.filterState);

        this.poiTypeOptions = ko.observableArray();
        this.poiType = ko.observable();
        this.isPermissionTypePublic = WebConfig.userSession.permissionType == Enums.ModelData.PermissionType.Public;
    }

    get webRequestCountry() {
        return WebRequestCountry.getInstance();
    }

    get webRequestProvince() {
        return WebRequestProvince.getInstance();
    }

    get webRequestCity() {
        return WebRequestCity.getInstance();
    }

    get webRequestTown() {
        return WebRequestTown.getInstance();
    }


    get webRequestPoiIcon() {
        return WebRequestPoiIcon.getInstance();
    }

    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    get webRequestCustomAreaType(){
        return WebRequestCustomAreaType.getInstance();
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        var dfd = $.Deferred();
        var d0 = $.Deferred();

        var filterPoiIcon = {
            includeAssociationNames: [EntityAssociation.PoiIcon.Image]
        };
        var t1 = this.webRequestPoiIcon.listPoiIconSummary(filterPoiIcon);
        var t2 = this.webRequestCountry.listCountry({ sortingColumns: DefaultSorting.Country });
        var t3 = this.webRequestEnumResource.listEnumResource({
            types: [Enums.ModelData.EnumResourceType.PoiType]
        });
        var t6 = this.webRequestCustomAreaType.listCustomSubAreaType();

        $.when(t1, t2, t3,t6).done((responsePoiIcon, responseCountry, responsePoiType,responseSubAreaType) => {
            
            if(responseSubAreaType){
                this.subPoiTypeList(responseSubAreaType["items"])
            }
            
            if (responsePoiIcon) {
                var poiIcon = responsePoiIcon["items"];

                var cboDataPoi = [];
                if (poiIcon.length > 0) {
                    ko.utils.arrayForEach(poiIcon, function (value, index) {
                        let obj = {
                            image: value.image.fileUrl,
                            value: value.id,
                            text: value.name
                        };
                        cboDataPoi.push(obj);
                    });
                }
                this.iconOptions(cboDataPoi);
            }

            let tmpPoiType = _.filter(responsePoiType["items"], (x) => {
                if (WebConfig.userSession.userType != Enums.UserType.BackOffice) {
                    return this.isPermissionTypePublic ? x.value != Enums.ModelData.PoiType.Private : x.value != Enums.ModelData.PoiType.Public;
                }
                return x;
                //else {
                //    if (!WebConfig.userSession.hasPermission(Constants.Permission.SharedCustomPOI)) {
                //        return x.value != Enums.ModelData.PoiType.Shared;
                //    }
                //    return x;
                //}

            });
            this.poiTypeOptions(tmpPoiType);

            var countries = responseCountry["items"];
            this.countryOptions(countries);
            var defaultCountry = ScreenHelper.findOptionByProperty(this.countryOptions, "isDefault", true);

            this.webRequestProvince.listProvince({
                countryIds: [defaultCountry.id],
                sortingColumns: DefaultSorting.Province
            }).done((response) => {
                var provinces = response["items"];
                this.provinceOptions(provinces);
                d0.resolve();
            }).fail((e) => {
                d0.reject(e);
                this.handleError(e);
            }).always(() => {

            });

            this._changeCountrySubscribe = this.country.subscribe((country) => {
                if (country) {
                    this.webRequestProvince.listProvince({
                        countryIds: [country.id],
                        sortingColumns: DefaultSorting.Province
                    }).done((response) => {
                        var provinces = response["items"];
                        this.provinceOptions.replaceAll(provinces);
                        //this.isBusy(false);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {

                    });
                } else {
                    this.provinceOptions.replaceAll([]);
                }

                // publish message to regional screen

                this.publishMessage("cw-geo-fencing-custom-poi-manage-country-changed", country);
            });

            this._changeProvinceSubscribe = this.province.subscribe((province) => {
                if (province) {
                    //this.isBusy(true);
                    this.city(null);
                    this.webRequestCity.listCity({
                        provinceIds: [province.id],
                        sortingColumns: DefaultSorting.City
                    }).done((response) => {
                        var cities = response["items"];
                        this.cityOptions.replaceAll(cities);
                        //this.isBusy(false);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {

                    });
                } else {
                    this.cityOptions.replaceAll([]);
                }
            });

            this._changeCitySubscribe = this.city.subscribe((city) => {
                if (city) {
                    //this.isBusy(true);
                    this.town(null);
                    this.webRequestTown.listTown({
                        cityIds: [city.id],
                        sortingColumns: DefaultSorting.Town
                    }).done((response) => {
                        var towns = response["items"];
                        this.townOptions.replaceAll(towns);
                        //this.isBusy(false);
                    }).fail((e) => {
                        //dfd.reject(e);
                        this.handleError(e);
                    }).always(() => {

                    });
                } else {
                    this.townOptions.replaceAll([]);
                }
            });

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
            this.handleError(e);
        }).always(() => {

        });
        this.isSubArea.subscribe((checked) => {
            this.isDisable(checked)

        })

        return dfd;
    }

    onUnload() { }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Search")()));
        actions.push(this.createActionCancel());
    }
    /**
      * @lifecycle Build available commands collection.
      * @param {any} commands
      */

    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdClear", this.i18n("Common_Clear")(), "svg-cmd-clear"));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            this.filterState.FromCreateDate = this.createDateFrom() ? this.createDateFrom() : null;
            this.filterState.ToCreateDate = this.createDateTo() ? this.createDateTo() : null;
            this.filterState.CreateBy = this.createBy() ? this.createBy() : "";
            this.filterState.PoiIconIds = this.iconSelectedItem() ? this.iconSelectedItem().value == "" ? [] : [this.iconSelectedItem().value] : [];
            this.filterState.TownIds = this.town() ? this.town().id : null;
            this.filterState.CityIds = this.city() ? this.city().id : null;
            this.filterState.ProvinceIds = this.province() ? this.province().id : null;
            this.filterState.CountryIds = this.country() ? this.country().id : null;
            this.filterState.poiType = this.poiType() ? this.poiType().value : null;

            this.filterState.name = this.name()?this.name():null;
            this.filterState.code = this.code()?this.code():null;
            this.filterState.hasSubPoiArea = this.isSubArea();
            this.filterState.subAreaName = this.subPoiName()?this.subPoiName():null;
            this.filterState.subAreaCode = this.subPoiCode()?this.subPoiCode():null;
            this.filterState.subAreaType = this.subPoiType()?this.subPoiType().id:null;
            

            this.publishMessage("cw-geo-fencing-custom-poi-filter-changed", this.filterState);

            //this.close(true);
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdClear") {
            this.dateFormat(null);
            this.createDateFrom(null);
            this.createDateTo(null);
            this.createBy(null);

            this.country(null);
            this.province(null);
            this.city(null);
            this.town(null);
            this.poiType(null);
            this.iconSelectedItem({
                value: "",
                text: ""
            });
        }
    }


}

export default {
    viewModel: ScreenBase.createFactory(CustomPOIFilterScreen),
    template: templateMarkup
};
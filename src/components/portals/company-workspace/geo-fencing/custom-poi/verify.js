﻿import ko from "knockout";
import templateMarkup from "text!./verify.html";
import ScreenBase from "../../../screenbase";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";
import * as EventAggregatorConstant from "../../../../../app/frameworks/constant/eventAggregator";
import WebRequestCustomPOI from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomPOI";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import {
    EntityAssociation,
    Constants,
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestMap from "../../../../../app/frameworks/data/apitrackingcore/webRequestMap";
/**
 * 
 * 
 * @class CustomPOIDetailScreen
 * @extends {ScreenBase}
 */
class CustomPOIViewVerifyScreen extends ScreenBase {
    /**
     * Creates an instance of CustomPOIDetailScreen.
     * 
     * @param {any} params
     * 
     * @memberOf CustomPOIDetailScreen
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("CustomPOI_Verify")());
        this.customPoiId = this.ensureNonObservable(params.customPoiId);

        this.name = ko.observable();
        this.code = ko.observable();
        this.description = ko.observable();
        this.category = ko.observable();
        this.icon = ko.observable();
        this.latitude = ko.observable();
        this.longitude = ko.observable();
        this.radius = ko.observable();

        this.accessibleBusinessUnits = ko.observableArray();

        this.currentValue = ko.observable();
        this.detectedValue = ko.observableArray([{
            id: 1,
            lat: "8.414953",
            lon: "99.973082"
        }, {
            id: 2,
            lat: "13.1235",
            lon: "100.0002"
        }, {
            id: 3,
            lat: "10.163223",
            lon: "98.903176"
        }]);

        this.selectedItems = ko.observableArray();

        this.selectedItems.subscribe(function(value) {

        }.bind(this));


    }
    get webRequestCustomPOI() {
        return WebRequestCustomPOI.getInstance();
    }

    get webRequestMap() {
            return WebRequestMap.getInstance();
    }
    /**
      * @lifecycle Called when View is loaded.
      * @param {boolean} isFirstLoad true if ViewModel is first load
      */
    onLoad(isFirstLoad) {



            if (!isFirstLoad) return;

            var dfd = $.Deferred();
            this.webRequestCustomPOI.getCustomPOI(this.customPoiId).done((response) => {

                this.latitude(response.latitude);
                this.longitude(response.longitude);
                this.currentValue(this.latitude() + " ," + this.longitude());
                this.name(response.name);
                this.code(response.code);
                this.description(response.description);
                this.category(response.categoryId);
                this.icon(response.iconId);
                this.radius(response.radius);
                this.accessibleBusinessUnits(response.accessibleBusinessUnits);
                dfd.resolve();
            }).fail((e) => {
                dfd.reject(e);
                this.handleError(e);
            }).always(() => {

            });
            return dfd;
        }
   /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}
    /**
      * @lifecycle Build available action button collection.
      * @param {any} actions
      */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", "Save"));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {

        super.onActionClick(sender);
        var self = this;
        if (sender.id === "actSave") {
            if (this.selectedItems()[0].lat && this.selectedItems()[0].lon) {
                var param = {
                    lat: this.selectedItems()[0].lat,
                    lon: this.selectedItems()[0].lon,
                    extent: ''
                };

                this.webRequestMap.identify(param).done((responseIden) => {
                    if (responseIden != null) {

                        let model = {
                            CompanyId: WebConfig.userSession.currentCompanyId,
                            Name: self.name(),
                            Code: self.code(),
                            Description: self.description(),
                            Latitude: param.lat,
                            Longitude: param.lon,
                            Radius: self.radius(),
                            ProvinceCode: responseIden.adminLevel1Code,
                            CityCode: responseIden.adminLevel2Code,
                            TownCode: responseIden.adminLevel3Code,
                            CountryCode: responseIden.countryCode,
                            CategoryId: self.category() ? self.category() : null,
                            IconId: self.icon() ? self.icon() : null,
                            Id: self.customPoiId
                        };

                        model.accessibleBusinessUnits = self.accessibleBusinessUnits();
                        this.webRequestCustomPOI.updateCustomPOI(model).done(() => {
                            self.publishMessage("cw-geo-fencing-custom-poi-changed", this.customPoiId);
                            self.close(true);
                        }).fail((e) => {
                            self.handleError(e);
                        }).always(() => {

                        });
                    }

                }).fail((e) => {
                    self.handleError(e);
                }).always(() => {

                });

            }
            this.close(true);

        }
    }

}

export default {
    viewModel: ScreenBase.createFactory(CustomPOIViewVerifyScreen),
    template: templateMarkup
};
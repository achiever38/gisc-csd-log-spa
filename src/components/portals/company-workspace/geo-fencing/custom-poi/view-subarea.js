import ko from "knockout";
import templateMarkup from "text!./view-subarea.html";
import ScreenBase from "../../../screenbase";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestCustomArea from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomArea";
import * as EventAggregatorConstant from "../../../../../app/frameworks/constant/eventAggregator";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Constants, Enums } from "../../../../../app/frameworks/constant/apiConstant";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";

class CustomSubAreaViewScreen extends ScreenBase {

    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("CustomArea_View")());
        this.bladeSize = BladeSize.Medium;
        this.currentData = null;
console.log("params",params);
        this.mainArea = ko.observable(params.parentPoiArea)
        this.subAreaSelect = ko.observable(params.selectedSubArea)
        this.subAreaList = ko.observableArray(params.subareaList)
        this.areaId = this.ensureNonObservable(params.id);
        this.name = ko.observable();
        this.description = ko.observable();
        this.category = ko.observable();
        this.createdDate = ko.observable();
        this.createdBy = ko.observable();
        this.lastUpdatedDate = ko.observable();
        this.lastUpdatedBy = ko.observable();
        this.size = ko.observable();
        this.numberofareapoint = ko.observable();
        this.moveoutarea = ko.observable();
        this.moveinarea = ko.observable();
        this.code = ko.observable();
        this.type = ko.observable();
        this.obj = ko.observable();
        this.areaPoint = ko.observableArray();
        this.fillColor = ko.observable();
        this.borderColor = ko.observable();
        this.accessibleBusinessUnits = ko.observableArray([]);
        this.areaUnit = ko.observable('');
        this.areaUnitType = ko.observable(1);
        this.orderAccessibleBusinessUnits = ko.observable([
            [1, "asc"]
        ]);

        this.poiType = ko.observable();
        this.poiUserPermissionType = ko.observable();
        this.poiTypeEnum = ko.observable();

        this.subscribeMessage("cw-geo-fencing-custom-area-changed", (id) => {
            this.webRequestCustomArea.getCustomArea(id).done((response) => {
                this.currentData = response;
                let category = response.category != null ? response.category.name : response.category;
                this.name(response.name);
                this.description(response.description);
                this.category(category);
                this.createdDate(response.formatCreateDate);
                this.createdBy(response.createBy);
                this.lastUpdatedDate(response.formatUpdateDate);
                this.lastUpdatedBy(response.updateBy);
                // this.size(Utility.convertAreaUnit(response.size, this.areaUnitType()));
                this.size(response.size);
                this.numberofareapoint(response.totalPoints);
                this.moveoutarea(response.outsideActionDisplayName);
                this.moveinarea(response.insideActionDisplayName);
                this.code(response.code);
                this.type(response.type.displayName);
                this.fillColor(this.rgbTohex(response.type.symbolFillColor));
                this.borderColor(this.rgbTohex(response.type.symbolBorderColor));
                this.obj = JSON.parse(response.points);
                this.poiType(response.poiTypeDisplayName);
                this.poiUserPermissionType(response.poiUserPermissionType);
                this.poiTypeEnum(response.poiType);
                var point = [];
                ko.utils.arrayForEach(this.obj[0], function(items, index) {
                    point.push({
                        id: (index + 1),
                        point: items[0].toFixed(5) + "," + items[1].toFixed(5)
                    })
                });
                point.splice(point.length - 1);
                this.areaPoint.replaceAll(point);

                let selectedBU = response.accessibleBusinessUnits || [];
                var mappedSelectedBUs = selectedBU.map((bu) => {
                    let mbu = bu;
                    mbu.businessUnitId = bu.businessUnitId;
                    mbu.businessUnitName = bu.businessUnitName;
                    return mbu;
                });
                this.accessibleBusinessUnits(mappedSelectedBUs);
            })
        });

    }


    /**
     * Get WebRequest specific for Company module in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    get webRequestCustomArea() {
        return WebRequestCustomArea.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
            if (!isFirstLoad) return;
            var dfd = $.Deferred();


            var dfdCompanySettings = this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId);
            // var dfdCustomAreaList = this.webRequestCustomArea.getCustomArea(this.areaId);

            $.when(dfdCompanySettings).done((r1) => { //, dfdCustomAreaList
                this.areaUnit(this.i18n("CustomArea_Size_2", [r1.areaUnitSymbol]));
                this.areaUnitType(r1.areaUnit);
                
                
                this.name(this.subAreaSelect().name);
                this.code(this.subAreaSelect().code);
                this.type(this.subAreaSelect().typeName);
                this.poiType(this.subAreaSelect().poiTypeDisplayName);
                this.description(this.subAreaSelect().description);
                // let category = r2.category != null ? r2.category.name : r2.category;
                // this.category(this.subAreaSelect().categoryName);
                // this.moveoutarea(this.subAreaSelect().outsideActionDisplayName);
                // this.moveinarea(this.subAreaSelect().insideActionDisplayName);
                // let selectedBU = r2.accessibleBusinessUnits || [];
                // var mappedSelectedBUs = selectedBU.map((bu) => {
                    //     let mbu = bu;
                    //     mbu.businessUnitId = bu.businessUnitId;
                    //     mbu.businessUnitName = bu.businessUnitName;
                    //     return mbu;
                    // });
                    // this.accessibleBusinessUnits(mappedSelectedBUs);
                this.obj = JSON.parse(this.subAreaSelect().points);

                if(this.obj && this.obj.length > 0){
                    var point = [];
                    ko.utils.arrayForEach(this.obj[0], function (items, index) {
                        point.push({
                            id: (index + 1),
                            point: items[0].toFixed(5) + "," + items[1].toFixed(5)
                        })
                    });
                    point.splice(point.length - 1);
                    this.areaPoint.replaceAll(point);

                }
                //this.size(Utility.convertAreaUnit(this.subAreaSelect().size, this.areaUnitType()));
                this.size(this.subAreaSelect().size);                
                this.numberofareapoint(this.subAreaSelect().totalPoints);
                this.createdDate(this.subAreaSelect().formatCreateDate);
                this.createdBy(this.subAreaSelect().createBy);
                this.lastUpdatedDate(this.subAreaSelect().formatUpdateDate);
                this.lastUpdatedBy(this.subAreaSelect().updateBy);
                    
                    // this.mainArea
                    // this.subAreaSelect 
                    // this.subAreaList
                    
                    //     this.currentData = r2;
            //     this.fillColor(this.rgbTohex(r2.type.symbolFillColor));
            //     this.borderColor(this.rgbTohex(r2.type.symbolBorderColor));
            //     this.obj = JSON.parse(r2.points);
            //     this.poiUserPermissionType(r2.poiUserPermissionType);
            //     this.poiTypeEnum(r2.poiType);


                dfd.resolve();
            }).fail((e) => {
                dfd.reject(e);
            });


            return dfd;
        }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
            MapManager.getInstance().clear(this.id);
            MapManager.getInstance().cancelDrawingPolygon(this.id);
        }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        // if (WebConfig.userSession.hasPermission(Constants.Permission.UpdateCustomArea) && 
        //     (WebConfig.userSession.userType == Enums.UserType.BackOffice || WebConfig.userSession.permissionType == this.poiUserPermissionType())
        // )
        // {
        //     if (this.poiTypeEnum() == Enums.ModelData.PoiType.Shared) { // check ว่าเป็น Shared ไหม
        //         if (WebConfig.userSession.hasPermission(Constants.Permission.SharedCustomArea)) { // เช็คว่า มี Permission Shared ไหม
        //             commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));
        //         }
        //     }
        //     else {
        //         commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));

        //     }
        // }

        // commands.push(this.createCommand("cmdViewOnMap", this.i18n("Common_ViewOnMap")(), "svg-cmd-search"));

        // if (WebConfig.userSession.hasPermission(Constants.Permission.DeleteCustomArea) &&
        //     (WebConfig.userSession.userType == Enums.UserType.BackOffice || WebConfig.userSession.permissionType == this.poiUserPermissionType())
        // )
        // {
        //     if(this.poiTypeEnum() == Enums.ModelData.PoiType.Shared) {
        //         if (WebConfig.userSession.hasPermission(Constants.Permission.SharedCustomArea)) {
        //             commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        //         }
        //     }
        //     else {
        //         commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));

        //     }
        // }

    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
            super.onActionClick(sender);
        }
    /**
      * @lifecycle Hangle when command on CommandBar is clicked.
      * @param {any} sender
      */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdUpdate":
                //     MapManager.getInstance().clearGraphicsId("draw-polygon-sub-area");
                //     MapManager.getInstance().clearGraphicsId("draw-polygon-main-area");
                // let mainArea = JSON.parse(this.mainArea().points)
                // MapManager.getInstance().drawOnePolygonZoomArea(this.id, mainArea, '#80ff00', '#ff9b00', 0.5, 1, null,"draw-polygon-main-area");


                // this.navigate("cw-geo-fencing-custom-poi-manage-sub-poi-area", {
                //     mode: "update",
                //     selectedSubArea: _.cloneDeep(this.subAreaSelect())
                // });
                break;
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M051")(),
                    BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            // this.webRequestCustomArea.deleteCustomArea(this.areaId).done(() => {
                            //     this.publishMessage("cw-geo-fencing-custom-area-delete");
                            //     this.close(true);
                            // }).fail((e) => {
                            //     this.handleError(e);
                            // });
                            break;
                    }
                });
                break;
            case "cmdViewOnMap":
                // MapManager.getInstance().drawOnePolygonZoom(this.id, this.obj, this.fillColor(), this.borderColor(), 0.5, 1, this.currentData);
                // this.minimizeAll();
                break;
        }
    }
    printMapAndDirection() {}
    /**
      convert hex format to a rgb color
      */
    rgbTohex(rgb) {

        let objRgb = rgb.split(",");
        return "#" + this.hex(objRgb[0]) + this.hex(objRgb[1]) + this.hex(objRgb[2]);
    }

    hex(x) {
        let hexDigits = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f");
        return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
    }
}

export default {
    viewModel: ScreenBase.createFactory(CustomSubAreaViewScreen),
    template: templateMarkup
};
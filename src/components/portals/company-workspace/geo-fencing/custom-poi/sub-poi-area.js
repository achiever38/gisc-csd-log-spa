import "jquery";
import ko from "knockout";
import _ from "lodash";
import ScreenBase from "../../../screenBase";
import ScreenHelper from "../../../screenhelper";
import templateMarkup from "text!./sub-poi-area.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import Utility from "../../../../../app/frameworks/core/utility";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import {
    Constants,
    Enums,
    EntityAssociation
} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";
import * as EventAggregatorConstant from "../../../../../app/frameworks/constant/eventAggregator";
// import WebRequestCustomPOI from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomPOI";
// import WebRequestCountry from "../../../../../app/frameworks/data/apicore/webrequestCountry";
// import WebRequestProvince from "../../../../../app/frameworks/data/apicore/webrequestProvince";
// import WebRequestCity from "../../../../../app/frameworks/data/apicore/webrequestCity";
// import WebRequestTown from "../../../../../app/frameworks/data/apicore/webrequestTown";
// import WebRequestPOICategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomPOICategory";
// import WebRequestPOIIcon from "../../../../../app/frameworks/data/apitrackingcore/webRequestPoiIcon";
// import WebRequestMap from "../../../../../app/frameworks/data/apitrackingcore/webRequestMap";
// import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
// import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestCustomAreaType from "../../../../../app/frameworks/data/apitrackingcore/webrequestCustomAreaType";

class CustomPOIMangeScreen extends ScreenBase {

    constructor(params) {
        super(params);
        // This mode is parse from params, it can comes from both URL or NavigationService
        this.mode = this.ensureNonObservable(params.mode);
        this.markpoi = this.ensureNonObservable(params.markpoi);
        this.customPoiId = this.ensureNonObservable(params.customPoiId);
        this.EditData = this.ensureNonObservable(params.selectedSubArea)

        if (this.mode === Screen.SCREEN_MODE_CREATE) {
            this.bladeTitle(this.i18n("CustomPOI_Sub_Area_Create")());
        } else if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            this.bladeTitle(this.i18n("CustomPOI_Sub_Area_Update")());
        }
        this.accessibleBusinessUnits = ko.observableArray([]);
        this.orderAccessibleBusinessUnits = ko.observable([
            [1, "asc"]
        ]);
        this.iconOptions = ko.observableArray();
        this.iconSelectedItem = ko.observable();
        this.countryOptions = ko.observableArray();
        this.provinceOptions = ko.observableArray();
        this.cityOptions = ko.observableArray();
        this.townOptions = ko.observableArray();

        this.name = ko.observable();
        this.code = ko.observable();
        this.description = ko.observable();
        this.type = ko.observable();
        this.point = ko.observable();

        this.latitude = ko.observable();
        this.longitude = ko.observable();
        this.radius = ko.observable(100);
        this.category = ko.observable();
        this.country = ko.observable();
        this.province = ko.observable();
        this.city = ko.observable();
        this.town = ko.observable();
        this.displaySize = ko.observable();
        this.numberofareapoint = ko.observable();
        this.areaUnit = ko.observable();
        this.areaUnitType = ko.observable();
        this.areaUnitText = ko.observable();

        this._responseIden = ko.observableArray();
        this._flagIden = false;
        this.isDropdownEnable = ko.observable(false);
        this.enablePoiType = false;

        //Poi Type
        this.poiTypeOptions = ko.observableArray();
        this.poiType = ko.observable();


        this.typeAreaOptions = ko.observableArray([]);
        this.typeArea = ko.observable();

        this.areaPoint = ko.observableArray();
        this.areaPOIList = ko.observableArray();
        this.areaId = this.ensureNonObservable(params.id);

        this.tmpAreaMap = ko.observable();
        this.selectSubAreaId = ko.observable();

        
        if (this.mode === Screen.SCREEN_MODE_CREATE) {
            this.enablePoiType = WebConfig.userSession.hasPermission(Constants.Permission.SharedCustomPOI) || WebConfig.userSession.userType == Enums.UserType.BackOffice ? true : false;
        }
        else {
            if(params.selectedSubArea){
                this.selectSubAreaId(params.selectedSubArea.id)
            }
            this.enablePoiType = WebConfig.userSession.hasPermission(Constants.Permission.SharedCustomPOI) ? true : false; 
        }
        
        this.isPermissionTypePublic = WebConfig.userSession.permissionType == Enums.ModelData.PermissionType.Public;
        this.subscribeMessage("cw-geo-fencing-custom-main-area-close-sub-area", () => {
            this.close(true)
        })


        // Observe change about Accessible Business Units
        this.subscribeMessage("cw-geo-fencing-custom-poi-manage-accessible-business-unit-selected", (selectedBusinessUnits) => {
            var mappedSelectedBUs = selectedBusinessUnits.map((bu) => {
                let mbu = bu;
                mbu.businessUnitId = bu.id;
                mbu.businessUnitName = bu.name;
                mbu.businessUnitPath = bu.businessUnitPath;
                mbu.canManage = true;
                return mbu;
            });
            this.accessibleBusinessUnits.replaceAll(mappedSelectedBUs);
        });

        this.extent = ko.observable();

        this.isSettingUpdate = false;

        this.areaPoint.subscribe((area)=>{
            if (!this.isSettingUpdate){
                this.drawPolygon()
            }
        })

        this.typeArea.subscribe((ee)=>{
        })

        //Subscribe Latitude
        this.latitude.subscribe((lat) => {
            if (!this.isSettingUpdate){
                this.idenPoi(this.latitude(), this.longitude(), this.extent());
            }
        });

        //Subscribe Longitude
        this.longitude.subscribe((lon) => {
            if (!this.isSettingUpdate) {
                this.idenPoi(this.latitude(), this.longitude(), this.extent());
            }
        });

        this._eventAggregator.subscribe(EventAggregatorConstant.MAP_COMMAND_COMPLETE, this.onMapComplete.bind(this));
        MapManager.getInstance().setFollowTracking(false);
    }

    goto(extras) {
        // TODO: Uncomment this block for allow real navigation in next phase.
        this.navigate(extras.id);
    }
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    // get webRequestCustomPOI() {
    //     return WebRequestCustomPOI.getInstance();
    // }

    // get webRequestCountry() {
    //     return WebRequestCountry.getInstance();
    // }

    // get webRequestProvince() {
    //     return WebRequestProvince.getInstance();
    // }

    // get webRequestCity() {
    //     return WebRequestCity.getInstance();
    // }

    // get webRequestTown() {
    //     return WebRequestTown.getInstance();
    // }

    // get webRequestPOICategory() {
    //     return WebRequestPOICategory.getInstance();
    // }

    get webRequestCustomAreaType(){
        return WebRequestCustomAreaType.getInstance();
    }

    // get webRequestPoiIcon() {
    //     return WebRequestPOIIcon.getInstance();
    // }

    // get webRequestMap() {
    //     return WebRequestMap.getInstance();
    // }

    // get webRequestBusinessUnit() {
    //     return WebRequestBusinessUnit.getInstance();
    // }


    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    // get webRequestEnumResource() {
    //     return WebRequestEnumResource.getInstance();
    // }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        MapManager.getInstance().clearGraphicsId("enable-draw-polygon-area");

        var dfd = $.Deferred();
        var d0 = $.Deferred();
        var d1 = $.Deferred();

        var filter = {
            companyId: WebConfig.userSession.currentCompanyId,
            includeAssociationNames: [
                EntityAssociation.BusinessUnit.ChildBusinessUnits
            ]
        };
        
        var filterPoiIcon = {
            includeAssociationNames: [EntityAssociation.PoiIcon.Image]
        };
        // var t1 = this.webRequestPOICategory.listCustomPOICategorySummary({
        //     "companyId": WebConfig.userSession.currentCompanyId
        // });
        // var t2 = this.webRequestPoiIcon.listPoiIconSummary(filterPoiIcon);
        // var t3 = this.webRequestBusinessUnit.listBusinessUnitSummary(filter);
        // var t4 = this.webRequestEnumResource.listEnumResource({
        //     types: [Enums.ModelData.EnumResourceType.PoiType]
        // });
        var t5 = this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId);
        var t6 = this.webRequestCustomAreaType.listCustomSubAreaType();
        $.when(t5,t6).done(
        (companySettings,responseSubAreaType) => {

            if(responseSubAreaType){
                this.typeAreaOptions(responseSubAreaType["items"])
            }

            if (companySettings) {
                this.areaUnitText(companySettings.areaUnitSymbol);
                this.areaUnit(this.i18n("CustomArea_Size_2", [this.areaUnitText()]));
                this.areaUnitType(companySettings.areaUnit);
            }

            d1.resolve();
        }).fail((e) => {
            d1.reject(e);
            this.handleError(e);
        }).always(() => {

        });
        
        switch (this.mode) {
            case "create":
                // this.poiType(ScreenHelper.findOptionByProperty(this.poiTypeOptions, "value", WebConfig.userSession.permissionType));

                // this._originalAccessibleBusinessUnits = [];
                // this.webRequestCountry.listCountry({
                //     sortingColumns: DefaultSorting.Country
                // }).done((response) => {
                //     var countries = response["items"];
                //     this.countryOptions(countries);
                //     var defaultCountry = ScreenHelper.findOptionByProperty(this.countryOptions, "isDefault", true);
                //     //this.country(defaultCountry);

                //     this.webRequestProvince.listProvince({
                //         countryIds: [defaultCountry.id]
                //     }).done((response) => {
                //         var provinces = response["items"];
                //         this.provinceOptions(provinces);
                        d0.resolve();
                //     }).fail((e) => {
                //         d0.reject(e);
                //         this.handleError(e);
                //     }).always(() => {

                //     });
                // }).fail((e) => {
                //     d0.reject(e);
                //     this.handleError(e);
                // }).always(() => {

                // });
                // if (this.markpoi != null) {
                //     this.extent(this.markpoi.extent?this.markpoi.extent:'')
                //     this.latitude(this.markpoi.lat);
                //     this.longitude(this.markpoi.lon);
                    
                // }
                break;
            case "update":
                // let resultArea = JSON.parse(this.EditData.Points)
                // console.log("update",resultArea);

                // // this.isSettingUpdate = true;
                // // this.webRequestCustomPOI.getCustomPOI(this.customPoiId).done((response) => {
                //     //     if (response) {
                //     let point = [];
                //     ko.utils.arrayForEach(resultArea[0], function(items, index) {
                //         point.push({
                //             id: (index + 1),
                //             point: items[0].toFixed(5) + "," + items[1].toFixed(5)
                //         })
                //     });
                //     point.splice(point.length - 1);


                //         this.name(this.EditData.Name);
                //         this.code(this.EditData.Code)
                //         this.description(this.EditData.Description)
                //         this.typeArea(this.EditData.Type)
                //         // this.typeArea(ScreenHelper.findOptionByProperty(this.typeAreaOptions, "name", this.EditData.Type.name))
                //         this.areaPOIList.replaceAll([{
                //             rings: JSON.parse(this.EditData.Points),
                //             spatialReference: {wkid: 4326}
                //         }]);
                //         this.areaPoint.replaceAll(point)

                //         console.log(JSON.parse(this.EditData.Points));
                        //         this.latitude(response.latitude);
                //         this.longitude(response.longitude);
                //         this.drawPoint(this.latitude(), this.longitude());
                //         this.code(response.code);
                //         this.description(response.description);
                //         this.radius(response.radius);
                //         this.accessibleBusinessUnits(response.accessibleBusinessUnits);
                //         this._originalAccessibleBusinessUnits = _.cloneDeep(response.accessibleBusinessUnits);

                //         if (WebConfig.userSession.userType == Enums.UserType.BackOffice) {
                //             let filterPoiType = [];
                //             switch (response.poiType) {
                //                 case Enums.ModelData.PoiType.Public:
                //                     filterPoiType = this.filterPoiType(this.poiTypeOptions(), 1);
                //                     break;

                //                 case Enums.ModelData.PoiType.Private:
                //                     filterPoiType = this.filterPoiType(this.poiTypeOptions(), 2);
                //                     break;

                //                 case Enums.ModelData.PoiType.Shared:
                //                     if (response.poiUserPermissionType == 1)  // 1 is Public of PoiUserPermission , 2 is Private of PoiUserPermission
                //                     {
                //                         filterPoiType = this.filterPoiType(this.poiTypeOptions(), 1);
                //                     }
                //                     else {
                //                         filterPoiType = this.filterPoiType(this.poiTypeOptions(), 2);
                //                     }
                //                     break;
                //             }
                //             this.poiTypeOptions(filterPoiType);
                //         }
                        
                //         this.poiType(ScreenHelper.findOptionByProperty(this.poiTypeOptions, "value", response.poiType));


                //         var d1 = this.webRequestCountry.listCountry({});
                //         var d2 = response.countryId ? this.webRequestProvince.listProvince({
                //             countryIds: [response.countryId]
                //         }) : null;
                //         var d3 = response.provinceId ? this.webRequestCity.listCity({
                //             provinceIds: [response.provinceId]
                //         }) : null;
                //         var d4 = response.cityId ? this.webRequestTown.listTown({
                //             cityIds: [response.cityId]
                //         }) : null;

                //         $.when(d1, d2, d3, d4).done((r1, r2, r3, r4) => {

                //             // var countries = r1["items"];
                //             // this.countryOptions(countries);

                //             if (r1) {

                //                 var countries = r1["items"];
                //                 this.countryOptions(countries);
                //                 if (response.countryCode) {
                //                     this.country(ScreenHelper.findOptionByProperty(this.countryOptions, "code", response.countryCode));
                //                 } else {
                //                     this.country(ScreenHelper.findOptionByProperty(this.countryOptions, "isDefault", true));

                //                 }
                //             }

                //             if (r2) {
                //                 var provinces = r2["items"];
                //                 this.provinceOptions(provinces);
                //                 if (response.provinceCode) {
                //                     this.province(ScreenHelper.findOptionByProperty(this.provinceOptions, "code", response.provinceCode));
                //                 }else{
                //                     this.province(ScreenHelper.findOptionByProperty(this.provinceOptions, "id", response.provinceId));

                //                 }
                //             }

                //             if (r3) {
                //                 var cities = r3["items"];
                //                 this.cityOptions(cities);

                //                 if (response.cityCode) {
                //                     this.city(ScreenHelper.findOptionByProperty(this.cityOptions, "code", response.cityCode));
                //                 }else{
                //                     this.city(ScreenHelper.findOptionByProperty(this.cityOptions, "id", response.cityId));

                //                 }
                //             }

                //             if (r4) {
                //                 var towns = r4["items"];
                //                 this.townOptions(towns);

                //                 if (response.townCode) {
                //                     this.town(ScreenHelper.findOptionByProperty(this.townOptions, "", response.townCode));
                //                 }else{
                //                     this.town(ScreenHelper.findOptionByProperty(this.townOptions, "id", response.townId));

                //                 }
                //             }
                //             this.isSettingUpdate = false;
                            d0.resolve();
                //         }).fail((e) => {
                //             d0.reject(e);
                //             this.isSettingUpdate = false;
                //             this.handleError(e);
                //         }).always(() => {

                //         });

                //     }
                // });
                break;
        }


        $.when(d0, d1).done(() => {

            // this._changeCountrySubscribe = this.country.subscribe((country) => {

            //     if (country) {
            //         this.webRequestProvince.listProvince({
            //             countryIds: [country.id]
            //         }).done((response) => {
            //             var provinces = response["items"];
            //             this.provinceOptions.replaceAll(provinces);

            //             setTimeout(() => {
            //                 if (this._responseIden) {
            //                     this.province(ScreenHelper.findOptionByProperty(this.provinceOptions, "code", this._responseIden.adminLevel1Code));
            //                 }
            //             }, 500);
            //         }).fail((e) => {
            //             this.handleError(e);
            //         }).always(() => {

            //         });
            //     } else {
            //         this.provinceOptions.replaceAll([]);
            //     }

            //     this.publishMessage("cw-geo-fencing-custom-poi-manage-country-changed", country);
            // });

            // this._changeProvinceSubscribe = this.province.subscribe((province) => {
            //     if (province) {
            //         this.city(null);
            //         this.webRequestCity.listCity({
            //             provinceIds: [province.id]
            //         }).done((response) => {
            //             var cities = response["items"];
            //             this.cityOptions.replaceAll(cities);
            //             setTimeout(() => {
            //                 if (this._responseIden) {
                                
            //                     this.city(ScreenHelper.findOptionByProperty(this.cityOptions, "code", this.generateModel().CityCode));

            //                 }
            //             }, 500);

            //         }).fail((e) => {
            //             this.handleError(e);
            //         }).always(() => {

            //         });
            //     } else {
            //         this.cityOptions.replaceAll([]);
            //     }
            // });

            // this._changeCitySubscribe = this.city.subscribe((city) => {

            //     if (city) {
            //         this.town(null);
            //         this.isBusy(true);
            //         this.webRequestTown.listTown({
            //             cityIds: [city.id]
            //         }).done((response) => {
            //             var towns = response["items"];
            //             this.townOptions.replaceAll(towns);

            //             setTimeout(() => {

            //                 if (this._responseIden) {

            //                     this.town(ScreenHelper.findOptionByProperty(this.townOptions, "code", this.generateModel().TownCode));

            //                 }

            //             }, 500);


            //         }).fail((e) => {
            //             this.handleError(e);
            //         }).always(() => {
            //             this.isBusy(false);
            //         });
            //     } else {
            //         this.townOptions.replaceAll([]);
            //     }
            // });
            switch (this.mode) {
                case "update":
                    if(this.EditData){

                let points = this.EditData.Points?this.EditData.Points:this.EditData.points
            let resultArea = JSON.parse(points)

                // this.isSettingUpdate = true;
                // this.webRequestCustomPOI.getCustomPOI(this.customPoiId).done((response) => {
                    //     if (response) {
                    let point = [];
                    ko.utils.arrayForEach(resultArea[0], function(items, index) {
                        point.push({
                            id: (index + 1),
                            point: items[0].toFixed(5) + "," + items[1].toFixed(5)
                        })
                    });
                    point.splice(point.length - 1);

                    let isName = this.EditData.Name?this.EditData.Name:this.EditData.name
                        this.name(isName);
                    let isCode = this.EditData.Code?this.EditData.Code:this.EditData.code                    
                        this.code(isCode)
                    let isDes = this.EditData.Description?this.EditData.Description:this.EditData.description                    
                        this.description(isDes)
                        // this.typeArea(this.EditData.Type)
                    let isTypeId = this.EditData.Type?this.EditData.Type.id:this.EditData.type.id                    
                   
                    this.typeArea(ScreenHelper.findOptionByProperty(this.typeAreaOptions, "id", isTypeId))
                    let isPoints = this.EditData.Points?this.EditData.Points:this.EditData.points                    
                        this.areaPOIList.replaceAll([{
                            rings: JSON.parse(isPoints),
                            spatialReference: {wkid: 4326}
                        }]);
                        this.areaPoint(point)
                        this.point(JSON.parse(isPoints))

                        
                        if(this.EditData.tmpData){
                            this.displaySize(Utility.convertAreaUnit(this.EditData.tmpData.area, this.areaUnitType()));
                            this.numberofareapoint(this.EditData.tmpData.count);
                        }else{
                            this.displaySize(this.EditData.size);
                            this.numberofareapoint(this.EditData.totalPoints);
                        }

                    }
                        
                        break;
            
                        default:
                        this.typeArea(ScreenHelper.findOptionByProperty(this.typeAreaOptions, "id", 1105))

                            break;
                    }
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
            this.handleError(e);
        }).always(() => {

        });


        return dfd;

    }
    drawPolygon(){
        MapManager.getInstance().drawOnePolygonZoomArea(this.id, this.areaPOIList()[0].rings, `rgba(${this.typeArea().symbolFillColor})`, `rgba(${this.typeArea().symbolBorderColor})`, 0.5, 1, {name:this.name()},"enable-draw-polygon-area");
    }
    onUnload() {
        MapManager.getInstance().clearGraphicsId("enable-draw-polygon-area");
        this.publishMessage("cw-geo-fencing-custom-sub-poi-close",_.cloneDeep(this.EditData));
        // MapManager.getInstance().clear(this.id);
        MapManager.getInstance().cancelDrawingPolygon(this.id);
    }

    enableClickMap() {

        if (!this.validationModel.isValid()) {
                    this.validationModel.errors.showAllMessages();
                    return;
            }

        this.minimizeAll(true);

        var areaUnit = {
            type: this.areaUnitType(),
            text: this.areaUnit(),
            keyMessage:"area",
            layerSubArea:"layerSubArea",
            typeArea:this.typeArea(),
            name:this.name()
        };

        var prePoint = this.point() && this.point().length > 0? this.point():null
        
        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            // MapManager.getInstance().clear(this.id);
            MapManager.getInstance().clearGraphicsId("enable-draw-polygon-area");
            MapManager.getInstance().enableDrawingPolygonArea(
                this.id, 
                prePoint, 
                areaUnit,
                this.name(),
                this.typeArea());
        } else {
            MapManager.getInstance().clearGraphicsId("enable-draw-polygon-area");
            MapManager.getInstance().enableDrawingPolygonArea(
                this.id, 
                prePoint, 
                areaUnit,
                this.name(),
                this.typeArea());
            // MapManager.getInstance().enableDrawingPolygon(this.id, null, areaUnit);
        }
    }
    drawPOIArea(){
        // Call clear map command.
        MapManager.getInstance().clear(this.id);

        // Call click command.
        MapManager.getInstance().drawPOIArea(this.customPoiId);
        this.minimizeAll();
    }

    onMapComplete(response) {
        if (response.command == "enable-click-map") {
            this.drawPoint(response.result.location.lat, response.result.location.lon);
            this.extent(response.result.location.extent);
            this.latitude(response.result.location.lat.toFixed(8));
            this.longitude(response.result.location.lon.toFixed(8));

            //this.idenPoi();

        }

        if (response.command == "enable-draw-polygon-area") {

            var point = [];
            ko.utils.arrayForEach(response.result.geometry.rings[0], function(items, index) {
                point.push({
                    id: (index + 1),
                    point: items[0].toFixed(5) + "," + items[1].toFixed(5)
                })
            });
            point.splice(point.length - 1);

            this.areaPOIList.replaceAll([response.result.geometry]);
            this.tmpAreaMap(response.result)
            if (this.mode === Screen.SCREEN_MODE_UPDATE) {
                this.areaPoint(_.cloneDeep(point));
                this.displaySize(Utility.convertAreaUnit(response.result.area, this.areaUnitType()));
                this.numberofareapoint(response.result.count);
                this.point(response.result.geometry.rings);
            }else{
                this.areaPoint(_.cloneDeep(point));
                this.displaySize(Utility.convertAreaUnit(response.result.area, this.areaUnitType()));
                this.numberofareapoint(response.result.count);
                this.point(response.result.geometry.rings);

                // if(!this.latitude() && !this.longitude()){
                //     this.latitude(response.result.centralLatitude);
                //     this.longitude(response.result.centralLongitude);
                // }
            }
        }else if(response.command == "cancel-drawing-polygon"){
            if(this.point()){
                this.drawPolygon()
            }
        }
    }

    drawPoint(locationLat, locationLon) {
        let clickLocation = {
                lat: locationLat,
                lon: locationLon,
            },

            symbol = {
                url: this.resolveUrl("~/images/pin_destination.png"),
                width: '35',
                height: '50',
                offset: {
                    x: 0,
                    y: 23.5
                },
                rotation: 0
            },
            zoom = 17,
            attributes = null

        //Call clear command.
        MapManager.getInstance().drawOnePointZoom(this.customPoiId, clickLocation, symbol, zoom, attributes);
    }

    idenPoi(lat, lon,extent) {

        if (lat && lon && !_.endsWith(lat, '.') && !_.endsWith(lon, '.')) {
            this._flagIden = true;
            var param = {
                lat: lat,
                lon: lon,
                extent: extent
            };
            this.drawPoint(lat, lon)
            this.isBusy(true);
            this.webRequestMap.identify(param).done((response) => {
                this._responseIden = response;

                this._responseIden.countryCode != null && this._responseIden.countryCode != "" ? this.isDropdownEnable(false) : this.isDropdownEnable(true)
                var countryCode = this._responseIden.countryCode != null && this._responseIden.countryCode != "" ? this._responseIden.countryCode : '';
                if(countryCode != '')
                {
                    this.country(ScreenHelper.findOptionByProperty(this.countryOptions, "code", countryCode));
                    this.province(ScreenHelper.findOptionByProperty(this.provinceOptions, "code", response.adminLevel1Code));
                }
            }).fail((e) => {
                this.handleError(e);
            }).always(() => {
                this.isBusy(false);
            });

        }
    }

    findCountryCode(code) {
        var countryCode;
        switch (code) {

            case "TH":
                countryCode = Constants.CountryCode.TH;
                break;
            case "SG":
                countryCode = Constants.CountryCode.SG;
                break;
            case "MY":
                countryCode = Constants.CountryCode.MY;
                break;
            case "BN":
                countryCode = Constants.CountryCode.BN;
                break;
            case "VN":
                countryCode = Constants.CountryCode.VN;
                break;
            case "PH":
                countryCode = Constants.CountryCode.PH;
                break;
            case "ID":
                countryCode = Constants.CountryCode.ID;
                break;
            case "MM":
                countryCode = Constants.CountryCode.MM;
                break;
            case "LA":
                countryCode = Constants.CountryCode.LA;
                break;
            case "KH":
                countryCode = Constants.CountryCode.KH;
                break;
        }

        return countryCode;
    }
    setupExtend() {

        this.name.extend({
            trackChange: true
        });

        this.code.extend({
            trackChange: true
        });

        this.typeArea.extend({
            trackChange:true
        })


        this.name.extend({
            required: true,
            serverValidate: {
                params: "name",
                message: this.i18n("M010")()
            }
        });
        this.code.extend({
            required: true,
            serverValidate: {
                params: "code",
                message: this.i18n("M010")()
            }
        });
        this.typeArea.extend({
            required: true,
        });
        

        this.validationModel = ko.validatedObservable({
            name: this.name,
            code: this.code,
            typeArea:this.typeArea
            // lat: this.latitude,
            // lon: this.longitude,
            // accessibleBusinessUnits: this.accessibleBusinessUnits,
            // country: this.country,
            // province: this.province,
            // city: this.city,
            // radius: this.radius
        });
    }
    /**
     * TODO:Delete selected item
     * Life cycle: handle command click
     * @public
     * @param {any} sender
     */
    onCommandClick(sender) {

    }


    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        // actions.push(this.createActionCancel());
        actions.push(this.createAction("actCancel", this.i18n("Common_Cancel")()));
    }

    generateModel() {
        
        // console.log("this.tmpAreaMap()",this.tmpAreaMap());
        let model = {
            companyId: WebConfig.userSession.currentCompanyId,
            name: this.name()? this.name() : null,
            code: this.code()? this.code() : null,
            description: this.description()? this.description() : null,
            // Color:
            size: this.displaySize()? this.displaySize():null,
            points: JSON.stringify(this.point()),
            totalPoints: this.numberofareapoint()?this.numberofareapoint():null,//this.tmpAreaMap() ? this.tmpAreaMap().count : 0.0,
            centralLatitude: this.tmpAreaMap() ? this.tmpAreaMap().centralLatitude : 0.0,
            centralLongitude: this.tmpAreaMap() ? this.tmpAreaMap().centralLongitude : 0.0,
            radius: 0,
            // InsideAction:
            // OutsideAction:
            // CategoryId:
            // Category:
            // AccessibleBusinessUnits:
            // CustomPoi:
            // CustomPoiId:
            // ReferenceCustomPoiId:
            // CategoryName:
            typeId:this.typeArea().id,
            typeName:this.typeArea().name,
            type: this.typeArea(),
            mode:this.mode,
            maxLatitude: this.tmpAreaMap() ? this.tmpAreaMap().maxLatitude : 0.0,
            minLatitude: this.tmpAreaMap() ? this.tmpAreaMap().minLatitude : 0.0,
            maxLongitude:this.tmpAreaMap() ? this.tmpAreaMap().maxLongitude : 0.0,
            minLongitude:this.tmpAreaMap() ? this.tmpAreaMap().minLongitude : 0.0,
            
            // InsideAction: this.moveIn() ? this.moveIn().value : null,
            // OutsideAction: this.moveOut() ? this.moveOut().value : null,
            // CategoryId: this.categoryId() ? this.categoryId().id : null,
            // TypeId: this.type().id ? this.type().id : null,
            
            // PoiType: this.poiType().value
            id:this.selectSubAreaId() ? this.selectSubAreaId() : 0,
            tmpData:this.tmpAreaMap() ? this.tmpAreaMap():null
        };

        return model;
    }
    getCode(result) {
        let code;
        if (result) {
            code = result.code
        } else {
            code = null
        }
        return code;
    }
    /**
     * 
     * @public
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {
            var dfdDelay = $.Deferred();
            setTimeout(function () {
                dfdDelay.resolve();
            }, 500);

            $.when(dfdDelay).done(() => {
                if (!this.validationModel.isValid()) {
                    this.validationModel.errors.showAllMessages();
                    return;
                }

                var model = this.generateModel();
                
                if(model.points){

                switch (this.mode) {
                    case Screen.SCREEN_MODE_CREATE:
                        this.isBusy(true);
                            this.publishMessage("cw-geo-fencing-custom-sub-poi-area-changed",_.cloneDeep(model));
                            this.close(true);
                        break;
                    case Screen.SCREEN_MODE_UPDATE:
                        this.isBusy(true);
                        this.publishMessage("cw-geo-fencing-custom-sub-poi-area-changed",_.cloneDeep(model));
                        this.close(true);
                        break;
                }

            }

               
            }); //

        } else if (sender.id == "actCancel") {
            let model = this.generateModel();
                model.mode = "actCancel"
            this.publishMessage("cw-geo-fencing-custom-sub-poi-area-changed",_.cloneDeep(model));
            this.close(true);
            // Call clear map command.
          //  MapManager.getInstance().clear(this.id);

        }
    }

    filterPoiType(data, filterType) {

        var filter = [];

        switch (filterType) {
            case 1:
                filter = _.filter(data, (x) => {
                    return x.value != Enums.ModelData.PoiType.Private;
                });
                break;
            case 2:
                filter = _.filter(data, (x) => {
                    return x.value != Enums.ModelData.PoiType.Public;
                });
                break;
            default:
                break;

        }
        return filter;
    }

    /**
     * Choose Accessible Business Unit
     */
    addAccessibleBusinessUnit() {
        var selectedBusinessUnitIds = this.accessibleBusinessUnits().map((obj) => {
            return obj.businessUnitId;
        });
        this.navigate("cw-geo-fencing-custom-poi-manage-accessible-business-unit-select", {
            mode: this.mode,
            selectedBusinessUnitIds: selectedBusinessUnitIds
        });
    }
    addSubPOIArea(){

        this.navigate("cw-geo-fencing-custom-poi-manage-sub-poi-area", {
            mode: this.mode,
            // selectedBusinessUnitIds: selectedBusinessUnitIds
        });
    }
}

export default {
    viewModel: ScreenBase.createFactory(CustomPOIMangeScreen),
    template: templateMarkup
};
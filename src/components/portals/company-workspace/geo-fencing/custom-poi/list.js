﻿import "jquery";
import ko from "knockout";
import ScreenBase from "../../../screenBase";
import templateMarkup from "text!./list.html";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebRequestCustomPOI from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomPOI";
import WebReqestCustomArea from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomArea"
import {
    Constants,
    Enums,
    EntityAssociation
} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";
import * as EventAggregatorConstant from "../../../../../app/frameworks/constant/eventAggregator";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import Utility from "../../../../../app/frameworks/core/utility";
import ScreenHelper from "../../../screenhelper";
import GenerateDataGridExpand from "../../generateDataGridExpand";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";

class CustomPOIListScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("CustomPOI_CustomPOI")());
        // this.bladeSize = BladeSize.XLarge_A1;
        this.bladeSize = BladeSize.Medium;
        this.customPoi = ko.observableArray();
        this.recentChangedRowIds = ko.observableArray();
        this.filterText = ko.observable();
        this.selectedCustomPOI = ko.observable();
        this.order = ko.observable([
            [2, "asc"] //

            //[2, "desc"]
            //[4, "asc"],[5,"desc"]
            //[9,"asc"],[8,"asc"],[7,"asc"]
        ]);
        this.apiDataSource = ko.observableArray([]);
        this.selectedItems = ko.observableArray([]);
        this.areaUnit = ko.observable();
        this.areaUnitType = ko.observable();
        this.areaUnitText = ko.observable();
        // Determine if selecting action is legal, return false to cancel selecting event.
        this._selectingRowHandler = (customPoi,col) => {
            //เพิ่มเงื่อนไขการคลิกที่ Row
            if(col && col !== 'icon'){
                if (customPoi && customPoi.detectionType) {
                    MapManager.getInstance().clear(this.id);
                    return this.navigate("cw-geo-fencing-custom-poi-view", {
                        customPoiId: customPoi.id
                    });
                }
                return false;
            }
        };

        this.filterState = ko.observable({
            CompanyId: WebConfig.userSession.currentCompanyId,
            Keyword: "",
            CountryIds: [],
            ProvinceIds: [],
            CityIds: [],
            TownIds: [],
            PoiIconIds: [],
            // FromCreateDate: null,
            // ToCreateDate: null,
            CreateBy: "",
            poiType:null,
            includeCount:true,
            PageProperty: 1,
            page: 1,
            pageSize: 20,
            skip: 0,
            take: 20,
        });

        // Subscribe Message when custom poi created/updated/delete
        this.subscribeMessage("cw-geo-fencing-custom-poi-changed", (customPoiId) => {
            var self = this;
        this.refreshShipmentList();

            //this.filterState(customPoiId);
            // Refresh entire datasource
            // this.webRequestCustomPOI.listCustomPOISummary(this.filterState()).done((response) => {
            //     var customPoi = response["items"];
            //     var viewonMap = {
            //         viewonMap: this.resolveUrl("~/images/ic-search.svg")
            //     };
            //     if (customPoi.length > 0) {
            //         ko.utils.arrayForEach(customPoi, function(value, index) {
            //             // items = $.extend(items, {
            //             //     viewonMap: self.resolveUrl("~/images/ic-search.svg"),
            //             //     detectionType:{
            //             //         detectionType:detectionType,
            //             //         url: self.resolveUrl("~/images/ic-search.svg")}
            //             // });
            //             if(value.detectionType === "R"){
            //                 viewonMap["detectionType"] = {
            //                     detectionType:value.detectionType,
            //                     url:self.resolveUrl("~/images/ic_detectradius.svg")
            //                 }
            //                 value = $.extend(value, viewonMap);
            //             }else{
            //                 viewonMap["detectionType"] = {
            //                     detectionType:value.detectionType,
            //                     url:self.resolveUrl("~/images/ic_detectarea.svg")
            //                 }
            //                 value = $.extend(value, viewonMap);
            //             }
            //         });

            //     }
            //     this.customPoi.replaceAll(customPoi);
            //     this.recentChangedRowIds.replaceAll([customPoiId]);
            // });
        });


        //Subscribe Message when custom poi filter
        this.subscribeMessage("cw-geo-fencing-custom-poi-filter-changed", (newFilter) => {
            this.filterState(newFilter);
            this.getListCustomPoiSummary();
        });

        this.subscribeMessage("cw-geo-fencing-custom-poi-delete", (newFilter) => {
            this.filterState();
            this.getListCustomPoiSummary();
        });


        this.viewMapClick = (data) => {

            let clickLocation = {
                    lat: data.latitude,
                    lon: data.longitude
                },
                symbol = {
                    url: this.resolveUrl("~/images/block_hilight.png"),
                    width: 32,
                    height: 32,
                    offset: {
                        x: 0,
                        y: 0
                    },
                    rotation: 0
                },
                zoom = 17,
                attributes = null

            //openCustomPOILayer
            MapManager.getInstance().openCustomPOILayer();

            //Call clear command.
            // MapManager.getInstance().drawOnePointZoom(this.id, clickLocation, symbol, zoom, attributes);
            this._eventAggregator.subscribe(EventAggregatorConstant.MAP_COMMAND_COMPLETE, this.onMapComplete.bind(this));
            
            this.webRequestCustomPOI.getCustomPOI(data.id).done((response) => {
                MapManager.getInstance().drawPointPoiArea(this.id, clickLocation, symbol, zoom, attributes);
                
                MapManager.getInstance().clearGraphicsId("draw-polygon-sub-area");
                MapManager.getInstance().clearGraphicsId("draw-polygon-main-area");
                if(response.parentPoiArea.name){
                    
                    let mainArea = JSON.parse(response.parentPoiArea.points) //'#80ff00', '#ff9b00'this.mainPoint()
                    MapManager.getInstance().drawOnePolygonZoomArea(this.id,mainArea , `rgba(${response.parentPoiArea.type.symbolFillColor})`, `rgba(${response.parentPoiArea.type.symbolFillColor})`, 0.5, 1, null,"draw-polygon-main-area");
                    response.subPoiArea.map((itm,index)=>{
                        let result = JSON.parse(itm.points)
                        let attributes = {
                            name:itm.name,
                            index:index
                        }
                        MapManager.getInstance().drawOnePolygonZoomArea(this.id, result, `rgba(${itm.type.symbolFillColor})`, `rgba(${itm.type.symbolBorderColor})`, 0.5, 1, attributes,"draw-polygon-sub-area");
                    })
                }
            })
            this.minimizeAll();
        };
        MapManager.getInstance().setFollowTracking(false);

        this.onDetailClickColumns = ko.observableArray([]);
        this.onDetailClickColumns(
            GenerateDataGridExpand.getOnClickColumns(this.generateColumnDetail())
        );
        var self = this;
        this.renderPOIIcon = (data) => {
            var node = null;
            if (!_.isNil(data)) {
                if(data.detectionType === "R"){//`<img src="${self.resolveUrl("~/images/ic_detectradius.svg")}" />`
                    node = `<img style="width:32px;" src="${self.resolveUrl("~/images/ic_detectradius.svg")}" />`
                }else{
                    node = `<img style="width:32px;" src="${self.resolveUrl("~/images/ic_detectarea.svg")}" />`
                }

            }
            return node
        }
        this.renderViewnOnMapColumn = (data, type, row)=> {
            let node = ""
            if (data.icon != null) {
                node = '<img src="' +
                    data.icon.image.fileUrl + '" style="cursor:pointer">';
            }
    
            return node;
        }
    }

    getListCustomPoiSummary() {
        this.refreshShipmentList();

        // this.webRequestCustomPOI.listCustomPOISummary(this.filterState()).done((responseFilter) => {
        //     var customPoi = responseFilter["items"];
        //     var viewonMap = {
        //         viewonMap: this.resolveUrl("~/images/ic-search.svg")
        //     };
        //     var self = this
        //     if (customPoi.length > 0) {
        //         ko.utils.arrayForEach(customPoi, function(value, index) {
        //             // items = $.extend(items, {viewonMap,
        //             //     detectionType:{
        //             //         detectionType:detectionType,
        //             //         url: self.resolveUrl("~/images/ic-search.svg")}
        //             //     });

        //             if(value.detectionType === "R"){
        //                 viewonMap["detectionType"] = {
        //                     detectionType:value.detectionType,
        //                     url:self.resolveUrl("~/images/ic_detectradius.svg")
        //                 }
        //                 value = $.extend(value, viewonMap);
        //             }else{
        //                 viewonMap["detectionType"] = {
        //                     detectionType:value.detectionType,
        //                     url:self.resolveUrl("~/images/ic_detectarea.svg")
        //                 }
        //                 value = $.extend(value, viewonMap);
        //             }

        //         });

        //     }
        //     // NOTE: this block have low performance when large number of items.
        //     //this.customPoi.replaceAll(customPoi);

        //     // We Preferred using dispatch event.
        //     this.dispatchEvent("dtCustomPOI", "reset", customPoi);

        // }).fail((e) => {
        //     d1.reject(e);
        //     this.handleError(e);
        // }).always(() => {

        // });
    }

    /**
     * Get WebRequest specific for Subscription module in Web API access.
     * @readonly
     */
    get webRequestCustomPOI() {
        return WebRequestCustomPOI.getInstance();
    }
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    get webReqestCustomArea(){
        return WebReqestCustomArea.getInstance();
    }
    setFormatDateTime(dateObj, time) {
        var newDateTime = "";

        if (dateObj) {
            if (typeof dateObj.getMonth === "function") {
                let d = new Date();
                let year = d.getFullYear().toString();
                let month = (d.getMonth() + 1).toString();
                let day = d.getDate().toString();

                month = month.length > 1 ? month : "0" + month;
                day = day.length > 1 ? day : "0" + day;
                newDateTime = year + "-" + month + "-" + day + "T" + time;
            } else {
                let newDate = dateObj.split("T")[0] + "T";
                let newTime = (dateObj.split("T")[1] = time);
                newDateTime = newDate + newTime;
            }
        }
        return newDateTime;
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {

        if (!isFirstLoad) return;
        var filter = Object.assign({}, this.filterState(), {
            // fromCreateDate: null,
            // toCreateDate: null,
            // fromCreateDate: this.setFormatDateTime(new Date(), "00:00:00"),
            // toCreateDate: this.setFormatDateTime(new Date(), "23:59:59"),
        });
        this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId).done((response) => {
            if (response) {
                this.areaUnitText(response.areaUnitSymbol);
                this.areaUnit(this.i18n("CustomArea_Size_2", [this.areaUnitText()]));
                this.areaUnitType(response.areaUnit);
            }
        })
        this.refreshShipmentList();
        // var dfd = $.Deferred();
        // this.apiDataSource({
        //     read: this.webRequestCustomPOI.listCustomPOISummaryNew(filter),
        //     // update: this.webRequestAssetMonitoring.listAssetMonitoringSummaryNew(filter)
        // });
        // this.webRequestCustomPOI.listCustomPOISummary(this.filterState()).done((response) => {
        //     var items = response["items"];
        //     var viewonMap = {
        //         viewonMap: this.resolveUrl("~/images/ic-search.svg")
        //     };
        //     var self = this
        //     if (items.length > 0) {
        //         ko.utils.arrayForEach(items, function(value, index) {
        //             if(value.detectionType === "R"){
        //                 viewonMap["detectionType"] = {
        //                     detectionType:value.detectionType,
        //                     url:self.resolveUrl("~/images/ic_detectradius.svg")
        //                 }
        //                 value = $.extend(value, viewonMap);
        //             }else{
        //                 viewonMap["detectionType"] = {
        //                     detectionType:value.detectionType,
        //                     url:self.resolveUrl("~/images/ic_detectarea.svg")
        //                 }
        //                 value = $.extend(value, viewonMap);
        //             }
        //         });
        //     }
        //     this.customPoi(items);
        //     dfd.resolve();

        // }).fail((e) => {
        //     dfd.reject(e);
        //     this.handleError(e);
        // }).always(() => {

        // });
        // return dfd;
    }

    onUnload() {
        MapManager.getInstance().clearGraphicsId("draw-polygon-sub-area");
        MapManager.getInstance().clearGraphicsId("draw-polygon-main-area");
        MapManager.getInstance().clear(this.id);
    }

    refreshShipmentList() {
        this.dispatchEvent("dtCustomPOI", "refresh");
    }

    // renderViewnOnMapColumn(data, type, row) {
    //     let node = ""
    //     if (data != null) {
    //         node = '<img src="' +
    //             data + '" style="cursor:pointer">';
    //     }

    //     return node;
    // }
    renderTypePOIColumn (data, type, row) {
       let node = ""
       if (data != null) {
        node = '<img src="' +
            data.url + '" >';
        }
        return node;
    }
    onDatasourceRequestRead(gridOption) {
        var dfd = $.Deferred();
        var self = this
        var filter = Object.assign({}, this.filterState(), gridOption);
        this.isBusy(true);
        this.webRequestCustomPOI
        .listCustomPOISummary(filter)
          .done(response => {
        
            //   var formatData = this.formatData(response);
            //   this.txtRefreshTime(response.formatTimestamp);

            //   for(let index in response["items"]) {
            //     let item = response["items"][index];
            //     if(item.detectionType === "R"){//`<img src="${self.resolveUrl("~/images/ic_detectradius.svg")}" />`
            //         response["items"][index].isExpand = true
            //     }else{
            //         response["items"][index].isExpand = false
            //     }
            //   }
              
              dfd.resolve({
                  items: response["items"],
                  totalRecords: response["totalRecords"],
                  currentPage : response["currentPage"]
              });
          })
          .fail(e => {
              this.handleError(e);
          })
          .always(() => {
              this.isBusy(false);
            //   this.onAutoRefresh(true);
          });

        return dfd;
    }

    onMapComplete(response) {
        //console.log("view on map : ", response);
        // Call cancel command.
        //MapManager.getInstance().cancelClickMap(this.id);

    }

    buildCommandBar(commands) {

        if (WebConfig.userSession.hasPermission(Constants.Permission.CreateCustomPOI)) {
            commands.push(this.createCommand("cmdCreate", this.i18n("CustomPOI_Create")(), "svg-cmd-add"));
        }

        commands.push(this.createCommand("cmdFilter", this.i18n("CustomPOI_Filters")(), "svg-cmd-search"));
        if(WebConfig.userSession.hasPermission(Constants.Permission.CreateCustomPOI) && WebConfig.userSession.hasPermission(Constants.Permission.UpdateCustomPOI)){
            commands.push(this.createCommand("cmdImport", this.i18n("Common_Import")(), "svg-cmd-import"));
        }
        commands.push(this.createCommand("cmdExport", this.i18n("CustomPOI_Export")(), "svg-cmd-export"));

    }

    onDetailInit(evt) {
        // var self = this;
        // $("<div/>")
        //         .appendTo(e.detailCell)
        //         .kendoGrid({
        //             dataSource: {},
        //             noRecords: {
        //                 template: self.i18n("M111")()
        //             },
        //             pageable: GenerateDataGridExpand.getPageable(false),
        //             // set column of kendo grid
        //             columns: GenerateDataGridExpand.getColumns(
        //                 self.generateColumnDetail()
        //             )
        //         });
        var self = this;
        var poiId = evt.data.id;

        // var shipmentID = evt.data.shipmentId;
        this.webReqestCustomArea
          .getSubArea(poiId).done(response => {
        //       self.formatDetailData(response);
              $("<div id='" + self.id + poiId + "'/>")
                .appendTo(evt.detailCell)
                .kendoGrid({
                    dataSource: response,
                    noRecords: {
                        template: self.i18n("M111")()
                    },
                    pageable: GenerateDataGridExpand.getPageable(false),
                    // set column of kendo grid
                    columns: GenerateDataGridExpand.getColumns(
                        self.generateColumnDetail()
                    )
                });
          })
          .fail(e => {
              this.handleError(e);
          })
          .always(() => {
              this.isBusy(false);
          });
    }

    generateColumnDetail() {
        var columnsDetail = [
            { type: "text", title: this.i18n("CustomArea_Name")(), data: 'name', },
            { type: "text", title: this.i18n("CustomArea_Code")(), data: 'code',  },
            { type: "text", title: this.i18n("CustomArea_PoiType")(), data: 'poiTypeDisplayName'},
            { type: "text", title: this.i18n("CustomArea_Type")(), data: 'typeName', },
            { type: "text", title: this.areaUnit(), data: 'size', },
            { type: "text", title: this.i18n("CustomArea_NumberAreaPoint")(), data: 'totalPoints', },
            { type: "text", title: this.i18n("CustomArea_CreateDate")(), data: 'formatCreateDate', },
            { type: "text", title: this.i18n("CustomArea_CreateBy")(), data: 'createBy', },
        ];
        return columnsDetail;
    }
    /**
     * Life cycle: handle command click
     * @public
     * @param {any} sender
     */
    onCommandClick(sender) {

        switch (sender.id) {

            case "cmdCreate":
                MapManager.getInstance().clearGraphicsId("draw-polygon-sub-area");
                MapManager.getInstance().clearGraphicsId("draw-polygon-main-area");
                MapManager.getInstance().clear(this.id);                
                this.navigate("cw-geo-fencing-custom-poi-manage", {
                    mode: "create"
                });
                break;
            case "cmdFilter":
                this.navigate("cw-geo-fencing-custom-poi-search", {
                    filterState: this.filterState()
                });
                break;
            case "cmdImport":
                this.navigate("cw-geo-fencing-custom-poi-import");
                break;
            case "cmdExport":
                this.showMessageBox(null, this.i18n("M128")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.filterState().templateFileType = Utility.isiOSDevice() ? Enums.TemplateFileType.Xls : Enums.TemplateFileType.Xlsx;
                            this.webRequestCustomPOI.exportCustomPOI(this.filterState()).done((response) => {
                                this.isBusy(false);
                                ScreenHelper.downloadFile(response.fileUrl);
                            }).fail((e) => {
                                this.isBusy(false);
                                this.handleError(e);
                            });
                            break;
                    }
                });
                break;
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(CustomPOIListScreen),
    template: templateMarkup
};
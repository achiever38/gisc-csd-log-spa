﻿import "jquery";
import ko from "knockout";
import ScreenBase from "../../../screenBase";
import templateMarkup from "text!./custom-poi-select.html";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebRequestCustomPOI from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomPOI";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";

class CustomPOISelectScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("CustomRoutes_SelectCustomPOI")());
        this.bladeSize = BladeSize.Large;

        this.mode = this.ensureNonObservable(params.mode);

        this.customPOI = ko.observableArray();
        this.selectedItems = ko.observableArray();
        this.filterText = ko.observable();
        this.order = ko.observable([
            [3, "asc"]
        ]);
        //// onClick of columnId: 'viewOnMapColumn'
        //this.viewMapClick = (data) => {
        //    if (data) {
        //        return this.navigate("cw-geo-fencing-custom-route-manage-custom-poi-detail", {
        //            poiId: data.id
        //        });
        //    }
        //    return false;
        //};

        this._selectingRowHandler = (customPoi) => {
            if (customPoi) {
                return this.navigate("cw-geo-fencing-custom-route-manage-custom-poi-detail", {
                    poiId: customPoi.id
                });
            }
            return false;
        };
    }

    renderIconColumn(data) {
        let node = ""
            if (data != null) {
                node = '<img src="' +
                    data + '" style="cursor:pointer">';
            }

        return node;
        }
        /**
         * Get WebRequest specific for custompoi module in Web API access.
         * @readonly
         */
    get webRequestCustomPOI() {
            return WebRequestCustomPOI.getInstance();
        }
        /**
         * @lifecycle Called when View is loaded.
         * @param {boolean} isFirstLoad true if ViewModel is first load
         */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var self=this;
        var dfd = $.Deferred();
        var params = {
            "CompanyId": WebConfig.userSession.currentCompanyId,
            "Keyword": "",
            "CountryIds": [],
            "ProvinceIds": [],
            "CityIds": [],
            "TownIds": [],
            "PoiIconIds": [],
            "FromCreateDate": null,
            "ToCreateDate": null,
            "CreateBy": ""
        }

        this.webRequestCustomPOI.listCustomPOISummary(params).done((response) => {
            var customPOI = response["items"];
            if (customPOI.length > 0) {

                //ko.utils.arrayForEach(customPOI, function(items, index) {
                //    items = $.extend(items, {
                //        viewonMap: self.resolveUrl("~/images/ic-search.svg")
                //    });
                //});

            }
            this.customPOI(customPOI);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        //  this.selectedSubscription(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actChoose", this.i18n("Common_OK")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actChoose") {
            this.publishMessage("cw-geo-fencing-custom-route-manage-custom-poi-select-selected", this.selectedItems());
            this.close(true);
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}
}

export default {
    viewModel: ScreenBase.createFactory(CustomPOISelectScreen),
    template: templateMarkup
};
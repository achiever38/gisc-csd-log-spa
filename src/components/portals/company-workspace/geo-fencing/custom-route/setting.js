﻿import "jquery";
import ko from "knockout";
import ScreenBase from "../../../screenBase";
import templateMarkup from "text!./setting.html";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import {
    Constants,
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import ScreenHelper from "../../../screenhelper";
import WebRequestCustomRoute from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomRoute";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
class CustomRouteListScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("CustomRoutes_Settings")());
        this.bladeSize = BladeSize.Small;

        // This mode is parse from params, it can comes from both URL or NavigationService
        this.mode = this.ensureNonObservable(params.mode);
        this.routeOptions = ko.observableArray();
        this.routeMode = ko.observableArray();

        this.settings = this.ensureNonObservable($.extend(true, {}, params.settings));
        this.settingOption = ko.observable();
        this.settingMode = ko.observable();

    }

    get webRequestEnumResource() {
            return WebRequestEnumResource.getInstance();
    }
    /**
     * Get WebRequest specific for customroute module in Web API access.
     * @readonly
     */
    get webRequestRoute() {
            return WebRequestCustomRoute.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        var dfd = $.Deferred();
        var filter = {
            CompanyId: WebConfig.userSession.currentCompanyId,
            types: [Enums.ModelData.EnumResourceType.RouteMode, Enums.ModelData.EnumResourceType.RouteOption]
        };
        this.webRequestEnumResource.listEnumResource(filter).done((response) => {
            // console.log("response",response);
            var routeOption = response["items"].filter((options) => {
                if (options.type === Enums.ModelData.EnumResourceType.RouteOption) {
                    return options;
                }
            });
            var routeMode = response["items"].filter((mode) => {
                if (mode.type === Enums.ModelData.EnumResourceType.RouteMode) {
                    return mode;
                }
            });
            this.routeOptions(routeOption);
            this.routeMode(routeMode);

            var routeOptionsValue = !_.isNil(this.settings.settingOption) ? ScreenHelper.findOptionByProperty(this.routeOptions, "value", this.settings.settingOption) : ScreenHelper.findOptionByProperty(this.routeOptions, "isDefault", true);
            var routeModeValue = !_.isNil(this.settings.settingMode) ? ScreenHelper.findOptionByProperty(this.routeMode, "value", this.settings.settingMode) : ScreenHelper.findOptionByProperty(this.routeMode, "isDefault", true);

            // set value
            this.settingOption(routeOptionsValue);
            this.settingMode(routeModeValue);

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {

    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actChoose", this.i18n("Common_OK")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actChoose") {

            this.settings.settingOption = this.settingOption() ? this.settingOption().value : null;
            this.settings.settingMode = this.settingMode() ? this.settingMode().value : null;
            var routeOptionFilter = {
                RouteMode: this.settings.settingMode,
                RouteOption: this.settings.settingOption
            };
            this.webRequestRoute.listCustomRouteOption(routeOptionFilter).done((dataRouteOptions) => {
                this.settings.settingId = dataRouteOptions["items"][0].id;
                this.publishMessage("cw-geo-fencing-custom-route-manage-setting-selected", this.settings);
                this.close(true);
            });


        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}
}

export default {
    viewModel: ScreenBase.createFactory(CustomRouteListScreen),
    template: templateMarkup
};
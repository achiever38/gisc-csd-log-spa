﻿import ko from "knockout";
import templateMarkup from "text!./custom-poi-detail.html";
import ScreenBase from "../../../screenbase";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebRequestCustomPOI from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomPOI";
/**
 * 
 * 
 * @class CustomPOIDetailScreen
 * @extends {ScreenBase}
 */
class CustomPOIDetailScreen extends ScreenBase {
    /**
     * Creates an instance of CustomPOIDetailScreen.
     * 
     * @param {any} params
     * 
     * @memberOf CustomPOIDetailScreen
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("CustomRoutes_POIDetail")());

        this.poiId = this.ensureNonObservable(params.poiId);
        this.name = ko.observable();
        this.description = ko.observable();
        this.category = ko.observable();
        this.icon = ko.observable();
        this.accessibleBU = ko.observableArray();
        this.createdDate = ko.observable();
        this.createdBy = ko.observable();
        this.lastUpdatedDate = ko.observable();
        this.lastUpdatedBy = ko.observable();
        this.latitude = ko.observable();
        this.longitude = ko.observable();
        this.radius = ko.observable();
        this.subdistrict = ko.observable();
        this.district = ko.observable();
        this.province = ko.observable();
        this.country = ko.observable();

    }

    /**
     * Get WebRequest specific for Subscription module in Web API access.
     * @readonly
     */
    get webRequestCustomPOI() {
        return WebRequestCustomPOI.getInstance();
    }
    /**
      * @lifecycle Called when View is loaded.
      * @param {boolean} isFirstLoad true if ViewModel is first load
      */
    onLoad(isFirstLoad) {

        if (!isFirstLoad) return;

        var dfd = $.Deferred();
        this.webRequestCustomPOI.getCustomPOI(this.poiId).done((response) => {

            let category = response.category != null ? response.category.name : response.category;
            this.name(response.name);
            this.description(response.description);
            this.category(category);
            if(response.icon != null) 
            {
                this.icon(response.icon.image.fileUrl);
            }
            this.createdDate(response.formatCreateDate);
            this.createdBy(response.createBy);
            this.lastUpdatedDate(response.formatUpdateDate);
            this.lastUpdatedBy(response.updateBy);
            this.latitude(response.latitude);
            this.longitude(response.longitude);
            this.radius(response.formatRadius);
            this.subdistrict(response.townName);
            this.district(response.cityName);
            this.province(response.provinceName);
            this.country(response.countryName);
            this.accessibleBU.replaceAll(response.accessibleBusinessUnits);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;

    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}
    /**
    * @lifecycle Build available action button collection.
    * @param {any} actions
    */
    buildActionBar(actions) {}
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {

    }
    /**
      * @lifecycle Handle when button on ActionBar is clicked.
      * @param {any} commands
      */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
      * @lifecycle Hangle when command on CommandBar is clicked.
      * @param {any} sender
      */
    onCommandClick(sender) {

    }
    printMapAndDirection() {}

}

export default {
viewModel: ScreenBase.createFactory(CustomPOIDetailScreen),
    template: templateMarkup
};
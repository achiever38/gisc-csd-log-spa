﻿import "jquery";
import ko from "knockout";
import _ from "lodash";
import ScreenBase from "../../../screenBase";
import ScreenHelper from "../../../screenhelper";
import templateMarkup from "text!./manage.html";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebRequestCustomRouteCategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomRouteCategory";
import WebRequestCustomRoute from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomRoute";
import WebRequestMap from "../../../../../app/frameworks/data/apitrackingcore/webRequestMap";
import * as EventAggregatorConstant from "../../../../../app/frameworks/constant/eventAggregator";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import {
    Constants,
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import Utility from "../../../../../app/frameworks/core/utility";
import WebRequestUnit from "../../../../../app/frameworks/data/apicore/webRequestUnitResource";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";



class CustomRouteManageScreen extends ScreenBase {

    /**
     * Creates an instance of CustomRouteManageScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
       
        // This mode is parse from params, it can comes from both URL or NavigationService
        this.mode = this.ensureNonObservable(params.mode);

        if (this.mode === "create") {
            this.bladeTitle(this.i18n("CustomRoutes_CreateCustomRoutes")());
        } else if (this.mode === "update") {
            this.bladeTitle(this.i18n("CustomRoutes_UpdateCustomRoutes")());
        }

        this.accessibleBusinessUnits = ko.observableArray([]);
        this.orderAccessibleBusinessUnits = ko.observable([
            [1, "asc"]
        ]);
        
        // translate Icon
        this.txtClickOnMap = this.i18n("CustomRoutes_ClickOnMap")();
        this.txtViewOnMap = this.i18n("CustomRoutes_ViewOnMap")();
        this.txtPrintMap = this.i18n("CustomRoutes_PrintMap")();
        this.txtSelectPoi = this.i18n("CustomRoutes_SelectPOI")();
        this.txtCreateCustomRoutes = this.i18n("CustomRoutes_CreateCustomRoutes")();
        this.txtReverse = this.i18n("CustomRoutesReverse")();
        this.txtCreateCustomArea = this.i18n("CustomRoutes_CreateCustomArea")();

        this.customRouteId = this.ensureNonObservable(params.customRouteId);
        this.routeFromHere = this.ensureNonObservable(params.routefromhere);
        this.name = ko.observable();
        this.code = ko.observable();
        this.description = ko.observable();
        this.routeColor = ko.observable();
        this.category = ko.observable();
        this.categoryOptions = ko.observableArray();
        this.distance = ko.observable();
        this.driveTime = ko.observable();
        this.selectedTabIndex = ko.observable(0);
        this.settings = ko.observable({
            settingMode: null,
            settingOption: null,
            settingId: null
        });
        // this.isFirstClickTab = ko.observable(false)
        this.selectedSubMenuItem = ko.observable();
        this.customRouteViaPoint = ko.observableArray();
        this.routeDirection = ko.observableArray();
        this.path = ko.observableArray();
        this.selectedCustomPOI = ko.observableArray();
        this._originalViaPoint = [];
        this.isSolveRoute = ko.observable(false);
        this.isFirstSolve = ko.observable(true);
        this.selectedCustomRouteLenght = ko.observable(); //เก็บ lenght ของ point ปัจจุบัน
        this.hasCreateCustomAreaPermission = ko.observable(WebConfig.userSession.hasPermission(Constants.Permission.CreateCustomArea));

        this.units = ko.observableArray();
        this.displayDistance = ko.observable("-");
        this.displayDuration = ko.observable("-");

        //Poi Type
        this.poiTypeOptions = ko.observableArray();
        this.poiType = ko.observable();
        if (this.mode === Screen.SCREEN_MODE_CREATE) {
            this.enablePoiType = WebConfig.userSession.hasPermission(Constants.Permission.SharedCustomRoute) || WebConfig.userSession.userType == Enums.UserType.BackOffice ? true : false;
        }
        else {
            this.enablePoiType = WebConfig.userSession.hasPermission(Constants.Permission.SharedCustomRoute) ? true : false;
        }
        this.isPermissionTypePublic = WebConfig.userSession.permissionType == Enums.ModelData.PermissionType.Public;

        // Observe change about Accessible Business Units
        this.subscribeMessage("cw-geo-fencing-custom-route-manage-accessible-business-unit-selected", (selectedBusinessUnits) => {
           
            var mappedSelectedBUs = selectedBusinessUnits.map((bu) => {
                let mbu = bu;
                mbu.businessUnitId = bu.id;
                mbu.businessUnitName = bu.name;
                mbu.businessUnitPath = bu.businessUnitPath;
                mbu.canManage = true;
                return mbu;
            });
            this.accessibleBusinessUnits.replaceAll(mappedSelectedBUs);
        });

        // Subscribe Message when CustomRoute settings changed
        this.subscribeMessage("cw-geo-fencing-custom-route-manage-setting-selected", (data) => {
            

            this.settings(data);
        });

        this.subscribeMessage("cw-geo-fencing-custom-area-by-route-changed", (data) => {
         
            this.clearMap();
        });


        //Subscribe customRouteViaPoint
        this.customRouteViaPoint.subscribe((viapoint) => {
        //    if(viapoint.length !== this.selectedCustomRouteLenght() && this.isFirstClickTab()){ //เมื่อมีการเพิ่มหรือลบpointจะให้ทำการclearเส้นทางบนแผนที่และsetให้this.isFirstSolve()เป็นtrueเพื่อที่จังหวะกดtab direction จะได้ไปทำการ solve route ใหม่
        //         console.log("ffffffffffffffffffffffffffffffffffffffffffffffffffff")
        //        this.selectedCustomRouteLenght(viapoint.length)
        //        this.routeDirection([]);
        //        this.clearMap();
        //        this.isFirstSolve(true)
        //    }

        ko.utils.arrayForEach(viapoint, function (items, index) {
            $.extend(items, {
                order:index+1
            });
        });
            if (this.isSolveRoute()) {

                this.routeDirection([]);
                this.clearMap();
                this.isSolveRoute(false);
                
            }
         
            this.drawPoint();
        });

        //Subscribe Message When Select Custom POI changed
        var self = this;
        this.subscribeMessage("cw-geo-fencing-custom-route-manage-custom-poi-select-selected", (dataPOI) => {
          

            if (dataPOI.length > 0) {
                this.selectedCustomPOI = dataPOI;
                ko.utils.arrayForEach(dataPOI, function (items, index) {

                    let objPoint = {
                        name: items.name,
                        id: parseInt(self.customRouteViaPoint().length + 1),
                        latitude: items.latitude,
                        longitude: items.longitude,
                        countryId: items.countryId,
                        provinceId: items.provinceId,
                        cityId: items.cityId,
                        townId: items.townId,
                        customPoiId: items.id
                    }
                    self.customRouteViaPoint.push(objPoint);
                    if (self.isSolveRoute()) {
                        self.routeDirection([]);
                        self.clearMap();
                        self.isSolveRoute(false);
                    }
                    self.drawPoint();
                });
                // self.solveRoute();
            }

        });

        // Register event subscribe in constructor.
        this._eventAggregator.subscribe(EventAggregatorConstant.MAP_COMMAND_COMPLETE, this.onMapComplete.bind(this));

        // Determine if selecting action is legal, return false to cancel selecting event.
        this._selectingRowHandler = (customRoute) => {


            if (customRoute) {
                //  MapManager.getInstance().zoomPoint(this.id, customRoute.latitude,  customRoute.longitude, 15);

               this.zoomPoint(customRoute.latitude, customRoute.longitude);
            }
            return false;
        };

        this._dataSourceChangedHandler = (customViaPoint) => {

        }
        MapManager.getInstance().setFollowTracking(false);
    }
    onMapComplete(response) {
        if (response.result) {
            var self = this;
            if (response.command == "enable-click-map") {

                var param = {
                    lat: response.result.location.lat,
                    lon: response.result.location.lon,
                    extent: response.result.location.extent
                };

                //--click บนแผนที่ Identify เอาค่า AdminLevelCode ต่างๆ /

                this.webRequestMap.identify(param).done((responseIden) => {

                    var cityCode = "",
                        townCode = "";
                    if (responseIden.countryCode == "TH") {
                        // case NOSTRA's data code 2 digit
                        if (responseIden.adminLevel2Code.length == 2) {
                            cityCode = responseIden.adminLevel1Code + responseIden.adminLevel2Code;
                            townCode = responseIden.adminLevel1Code + responseIden.adminLevel2Code + responseIden.adminLevel3Code;
                        } else {
                            cityCode = responseIden.adminLevel2Code;
                            townCode = responseIden.adminLevel3Code;
                        }
                    } else {
                        cityCode = responseIden.adminLevel2Code;
                        townCode = responseIden.adminLevel3Code;
                    }
                    let objPoint = {
                        name: WebConfig.userSession.currentUserLanguage == "en-US" ? responseIden.nameEnglish : responseIden.nameLocal,
                        id: parseInt(self.customRouteViaPoint().length + 1),
                        latitude: response.result.location.lat,
                        longitude: response.result.location.lon,
                        countryCode: responseIden.countryCode,
                        provinceCode: responseIden.adminLevel1Code,
                        cityCode: cityCode,
                        townCode: townCode
                    }
                    self.customRouteViaPoint.push(objPoint);
                    
                    if (self.isSolveRoute()) {
                        
                        self.routeDirection([]);
                        self.clearMap();
                        self.isSolveRoute(false);
                    }
                    // self.drawPoint();
                    //add distance,driveTime
                    var param = {
                        routeMode: this.settings().settingMode,
                        routeOption: this.settings().settingOption,
                        lang: WebConfig.userSession.currentUserLanguage == "en-US" ? "E" : "L",
                        stops: []
                    };
                    
                    if (this.customRouteViaPoint().length >= 2) {
                        ko.utils.arrayForEach(this.customRouteViaPoint(), function (items, index) {
                            param.stops.push({
                                name: items.name,
                                lat: items.latitude,
                                lon: items.longitude
                            });
                        });
                        this.webRequestMap.solveRoute(param).done((responseSolveRouteString) => {
                            var responseSolveRoute = JSON.parse(responseSolveRouteString);
                            // console.log("boy debug :: responseSolveRoute",responseSolveRoute);

                            if (responseSolveRoute != null) {
                                if (responseSolveRoute.results != null) {
                                    self.path(responseSolveRoute.results.route);
                                    ko.utils.arrayForEach(responseSolveRoute.results.directions, function (items, index) {

                                        items = $.extend(items, {
                                            order: (index + 1)
                                        });
                                    });
                                    self.routeDirection.replaceAll(responseSolveRoute.results.directions);
                                    self.distance(Math.round(responseSolveRoute.results.totalLength));
                                    self.driveTime(Math.round(responseSolveRoute.results.totalTime));
                                    self.displayDistance(self.formatDistance(responseSolveRoute.results.totalLength));
                                    self.displayDuration(self.formatTime(responseSolveRoute.results.totalTime));
                                    // Route โดยการกด Route เอง
                                    // var pics = [];
                                    // ko.utils.arrayForEach(self.customRouteViaPoint(), function (items, index) {
                                    //     let urlPic = "~/images/";
                                    //     if (index === (self.customRouteViaPoint().length - 1)) {
                                    //         urlPic += "pin_destination.png";

                                    //     } else {
                                    //         urlPic += "pin_poi_" + (index + 1).toString() + ".png";
                                    //     }
                                    //     pics.push(self.resolveUrl(urlPic));
                                    // });
                                    // self.drawRoute(param.stops, pics, self.path(), self.routeColor(), undefined, undefined, responseSolveRoute.results);
                                    self.isSolveRoute(true);

                                }
                            }
                        }).fail((e) => {
                            var errorObj = e.responseJSON;
                            console.log("errorObj in distance", errorObj);
                        });
                    }

                }).fail((e) => {

                    var errorObj = e.responseJSON;
                    console.log("errorObj", errorObj);
                });
            }
        }
    }
    goto(extras) {

        this.canCelClickMap();
        // TODO: Uncomment this block for allow real navigation in next phase.
        this.navigate(extras.id);
    }
    /**
     * Get WebRequest specific for CustomRouteCategory module in Web API access.
     * @readonly
     */
    get webRequestCustomRouteCategory() {
        return WebRequestCustomRouteCategory.getInstance();
    }
    /**
     * Get WebRequest specific for customroute module in Web API access.
     * @readonly
     */
    get webRequestRoute() {
        return WebRequestCustomRoute.getInstance();
    }
    /**
     * Get WebRequest specific for map module in Web API access.
     * @readonly
     */
    get webRequestMap() {
        return WebRequestMap.getInstance();
    }

    get webRequestUnit() {

        return WebRequestUnit.getInstance();
    }

    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        //

        if (!isFirstLoad) return;

        this._originalAccessibleBusinessUnits = [];

        var self = this;
        var dfd = $.Deferred();
        var d1 = this.webRequestCustomRouteCategory.listCustomRouteCategorySummary({
            companyId: WebConfig.userSession.currentCompanyId
        });
        var d2 = (this.mode === Screen.SCREEN_MODE_UPDATE) ? this.webRequestRoute.getCustomRoute(this.customRouteId) : null;
        var d3 = this.webRequestUnit.listUnitResource({
            companyId: WebConfig.userSession.currentCompanyId
        });
        var d4 = this.webRequestEnumResource.listEnumResource({
            types: [Enums.ModelData.EnumResourceType.PoiType]
        });

        $.when(d1, d2, d3,d4).done((category, customRouteInfo, unitsInfo,poiTypeInfo) => {


            if (unitsInfo) {

                this.units.replaceAll(unitsInfo["items"]);
            }
            if (category) {

                var categorys = category["items"];
                this.categoryOptions.replaceAll(categorys);

            }

            let tmpPoiType = _.filter(poiTypeInfo["items"], (x) => {
                if (WebConfig.userSession.userType != Enums.UserType.BackOffice) {
                    return this.isPermissionTypePublic ? x.value != Enums.ModelData.PoiType.Private : x.value != Enums.ModelData.PoiType.Public;
                }
                else {
                    if (!WebConfig.userSession.hasPermission(Constants.Permission.SharedCustomRoute)) {
                        return x.value != Enums.ModelData.PoiType.Shared;
                    }
                    return x;
                }
            });
            this.poiTypeOptions(tmpPoiType);

            if (customRouteInfo) {

                //update mode set data

                var categoryValue = !_.isNil(customRouteInfo.categoryId) ? ScreenHelper.findOptionByProperty(this.categoryOptions, "id", customRouteInfo.categoryId) : ScreenHelper.findOptionByProperty(this.categoryOptions, "isDefault", true);

                this.accessibleBusinessUnits(customRouteInfo.accessibleBusinessUnits || []);
                this._originalAccessibleBusinessUnits = _.cloneDeep(customRouteInfo.accessibleBusinessUnits || []);

                this.name(customRouteInfo.name);
                this.code(customRouteInfo.code);
                this.description(customRouteInfo.description);
                this.routeColor(customRouteInfo.color);
                this.category(categoryValue);
                this.distance(customRouteInfo.distance);
                this.driveTime(customRouteInfo.driveTime);


                if (WebConfig.userSession.userType == Enums.UserType.BackOffice) {
                    let filterPoiType = [];
                    switch (customRouteInfo.poiType) {
                        case Enums.ModelData.PoiType.Public:
                            filterPoiType = this.filterPoiType(this.poiTypeOptions(), 1);
                            break;

                        case Enums.ModelData.PoiType.Private:
                            filterPoiType = this.filterPoiType(this.poiTypeOptions(), 2);
                            break;

                        case Enums.ModelData.PoiType.Shared:
                            if (customRouteInfo.poiUserPermissionType == 1)  // 1 is Public of PoiUserPermission , 2 is Private of PoiUserPermission
                            {
                                filterPoiType = this.filterPoiType(this.poiTypeOptions(), 1);
                            }
                            else {
                                filterPoiType = this.filterPoiType(this.poiTypeOptions(), 2);
                            }
                            break;
                    }
                    this.poiTypeOptions(filterPoiType);
                }

                this.poiType(ScreenHelper.findOptionByProperty(this.poiTypeOptions, "value", customRouteInfo.poiType));

                this.settings().settingMode = customRouteInfo.routeOption.routeMode;
                this.settings().settingOption = customRouteInfo.routeOption.routeOption;
                this.settings().settingId = customRouteInfo.routeOption.id;
                // 

                var _viaPointsCurrent = [];
                var _viaPointsOriginal = [];
                ko.utils.arrayForEach(customRouteInfo.viaPoints, function (items, index) {
                    var itemsOriginal = $.extend(items, {
                        original: true
                    });
                    _viaPointsCurrent.push(itemsOriginal);
                    _viaPointsOriginal.push(itemsOriginal);
                });
                var sortCurrent = this.sortByKey(_viaPointsCurrent, "order");
                var sortOriginal = this.sortByKey($.extend(true, [], _viaPointsOriginal), "order");
                this.customRouteViaPoint.replaceAll(sortCurrent);
                this._originalViaPoint = sortOriginal;
                // this.selectedCustomRouteLenght(sortCurrent.length)
             

            }
            if (this.mode === Screen.SCREEN_MODE_UPDATE) {
         
                this.solveRoute();
            } else {
                //comment
                if (this.routeFromHere != null) {
                    var param = {
                        lat: this.routeFromHere.lat,
                        lon: this.routeFromHere.lon,
                        extent: ""
                    };
                    this.webRequestMap.identify(param).done((responseIden) => {
                        var cityCode = "",
                            townCode = "";
                        if (responseIden.countryCode == "TH") {
                            // case NOSTRA's data code 2 digit
                            if (responseIden.adminLevel2Code.length == 2) {
                                cityCode = responseIden.adminLevel1Code + responseIden.adminLevel2Code;
                                townCode = responseIden.adminLevel1Code + responseIden.adminLevel2Code + responseIden.adminLevel3Code;
                            } else {
                                cityCode = responseIden.adminLevel2Code;
                                townCode = responseIden.adminLevel3Code;
                            }
                        } else {
                            cityCode = responseIden.adminLevel2Code;
                            townCode = responseIden.adminLevel3Code;
                        }
                        let objPoint = {
                            name: WebConfig.userSession.currentUserLanguage == "en-US" ? responseIden.nameEnglish : responseIden.nameLocal,
                            id: parseInt(self.customRouteViaPoint().length + 1),
                            latitude: param.lat,
                            longitude: param.lon,
                            countryCode: responseIden.countryCode,
                            provinceCode: responseIden.adminLevel1Code,
                            cityCode: cityCode,
                            townCode: townCode
                        }
                        self.customRouteViaPoint.push(objPoint);
                       
                        if (self.isSolveRoute()) {
                            self.routeDirection([]);
                            self.clearMap();
                            self.isSolveRoute(false);
                        }
                        self.drawPoint();

                    }).fail((e) => {

                        var errorObj = e.responseJSON;
                        console.log("errorObj", errorObj);
                    });

                }
            }
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Generate customroute model from View Model
     * 
     * @returns customroute model which is ready for ajax request 
     */
    generateModel() {


        var model = {
            companyId: WebConfig.userSession.currentCompanyId,
            name: this.name() ? this.name() : null,
            code: this.code() ? this.code() : null,
            color: this.routeColor() ? this.routeColor() : null,
            description: this.description() ? this.description() : null,
            distance: this.distance(),
            driveTime: this.driveTime(),
            totalPoints: this.customRouteViaPoint().length,
            categoryId: this.category() ? this.category().id : null,
            routeOptionId: this.settings().settingId,
            infoStatus: (this.mode === Screen.SCREEN_MODE_CREATE) ? Enums.InfoStatus.Add : Enums.InfoStatus.Update,
            id: this.customRouteId,
            viaPoints: [],
            poiType: this.poiType().value,
            UpdateBy: WebConfig.userSession.fullname
        };

        model.accessibleBusinessUnits = Utility.generateArrayWithInfoStatus(
            this._originalAccessibleBusinessUnits,
            this.accessibleBusinessUnits()
        );

        var viapoints = [];
        ko.utils.arrayForEach(this.customRouteViaPoint(), function (items, index) {
            var itemsCurrent = $.extend(items, {
                order: index + 1
            });
            viapoints.push(itemsCurrent);
        });

        if (this.mode === Screen.SCREEN_MODE_CREATE) {
            model.viaPoints = viapoints;
        } else {

            // Generate array with InfoStatus on each item.
            model.viaPoints = this.generateViaPointWithInfoStatus(
                this._originalViaPoint, viapoints
            );

        }
        return model;
    }

    generateViaPointWithInfoStatus(originalArray, currentArray, identityProperty = "id", identityPropertyForUpdate = "order") {

        var originalSet = new Set();
        var addedSet = new Set();
        var deletedSet = new Set();
        var updatedSet = new Set();

        // Find Original, Add,Update

        currentArray.forEach(function (cItem) {
            if (cItem.original) {
                originalArray.forEach(function (oItem) {

                    if (cItem.id === oItem.id) {
                        if (cItem[identityPropertyForUpdate] === oItem[identityPropertyForUpdate]) {

                            Utility.applyInfoStatus(cItem, Enums.InfoStatus.Original);
                            originalSet.add(cItem);

                        } else {
                            Utility.applyInfoStatus(cItem, Enums.InfoStatus.Update);
                            updatedSet.add(cItem);
                        }
                    }

                }, this);
            } else {

                Utility.applyInfoStatus(cItem, Enums.InfoStatus.Add);
                addedSet.add(cItem);
            }
        }, this);


        // Find Delete
        originalArray.forEach(function (oItem) {
            let match = _.find(currentArray, (cItem) => {
                return cItem[identityProperty] === oItem[identityProperty];
            });
            if (match) {
                //Utility.applyInfoStatus(match, Enums.InfoStatus.Original);
                //originalSet.add(match);
            } else {
                Utility.applyInfoStatus(oItem, Enums.InfoStatus.Delete);
                deletedSet.add(oItem);
            }
        }, this);

        var oArray = _.toArray(originalSet); //Array.from(originalSet);
        var aArray = _.toArray(addedSet); //Array.from(addedSet);
        var dArray = _.toArray(deletedSet); //Array.from(deletedSet);
        var uArray = _.toArray(updatedSet); //Array.from(updatedSet);
        //console.log("originalSet",oArray);
        //console.log("addedSet",aArray);
        //console.log("deletedSet",dArray);
        //console.log("updatedSet",uArray);
        return oArray.concat(aArray, dArray, uArray);
    }

    selectedPOI() {
        this.canCelClickMap();
        var page = "cw-geo-fencing-custom-route-manage-custom-poi-select";
        var options = {
            mode: this.mode
        };
        this.navigate(page, options);
    }

    enableClickMap() {
        this.minimizeAll();
        // Call click command.
        MapManager.getInstance().enableClickMap(this.id);
    }

    canCelClickMap() {

        // Call cancel command.
        MapManager.getInstance().cancelClickMap(this.id);
    }

    clearMap() {

        MapManager.getInstance().clear(this.id);
    }

    zoomPoint(lat, lon, zoom = 15) {

        MapManager.getInstance().zoomPoint(this.id, lat, lon, zoom);
    }

    drawPoint() {

        var objPoint = [];
        var self = this;
        ko.utils.arrayForEach(this.customRouteViaPoint(), function (items, index) {
            let urlPic = self.resolveUrl("~/images/pin_poi_" + (index + 1).toString() + ".png");
            let obj = {
                location: {
                    lat: items.latitude,
                    lon: items.longitude
                },
                symbol: {
                    url: urlPic,
                    width: '35',
                    height: '50',
                    offset: {
                        x: 0,
                        y: 23.5
                    }
                },
                attributes: null
            }
            objPoint.push(obj);
        });

        MapManager.getInstance().drawMultiplePoint(this.id, objPoint);
    }
    drawRoute(stops, pics, path, color = "#7000db", width = 5, opacity = 1, attributes = null) {

        MapManager.getInstance().drawOneRoute(this.id, stops, pics, path, color, width, opacity, attributes);
    }
    reverseViaPoint() {
        this.canCelClickMap();

        var customRoutereverse = this.customRouteViaPoint.reverse();
        this.customRouteViaPoint.replaceAll(customRoutereverse());
    }


    onClickTab(param){
        // console.log("this.sedlectedTabindex",this.selectedTabIndex())
        // console.log("param",param)
        if(this.selectedTabIndex() == 0){ //tab 1
            // this.isFirstSolve(true)
        }else if(this.selectedTabIndex() == 1){ //tab 2
            // this.isSolveRoute(true)
            if(this.isFirstSolve()){
                // console.log("llllllllllllllllllllllll")
                // this.isBusy(true);
                this.solveRoute(false)
                this.isFirstSolve(false)
                // this.isFirstClickTab(true)

            }
            if(this.customRouteViaPoint().length<=1){ //กรณี point มีแค่จุดเดียวหรือไม่มีเลยให้clear direction 
                this.routeDirection([])
            }
        } 
        

   
    
    }

    solveRoute(params) {

        var self = this;
        var validationModelSolveRoute = ko.validatedObservable({
            settings: self.settings,
            customRouteViaPoint: self.customRouteViaPoint
        });
        if (!validationModelSolveRoute.isValid()) {
            validationModelSolveRoute.errors.showAllMessages();
            return;
        }

        var param = {
            routeMode: this.settings().settingMode,
            routeOption: this.settings().settingOption,
            lang: WebConfig.userSession.currentUserLanguage == "en-US" ? "E" : "L",
            stops: []
        };

        if (this.customRouteViaPoint().length >= 2) {
            ko.utils.arrayForEach(this.customRouteViaPoint(), function (items, index) {
                param.stops.push({
                    name: items.name,
                    lat: items.latitude,
                    lon: items.longitude
                });
            });


            this.webRequestMap.solveRoute(param).done((responseSolveRouteString) => {
                // console.log("boy debug :: responseSolveRoute",responseSolveRouteString);
                var responseSolveRoute = JSON.parse(responseSolveRouteString);
                if (responseSolveRoute != null) {
                    if (responseSolveRoute.results != null) {
                        self.path(responseSolveRoute.results.route);
                        ko.utils.arrayForEach(responseSolveRoute.results.directions, function (items, index) {

                            items = $.extend(items, {
                                order: (index + 1)
                            });
                        });
                       
                        self.routeDirection.replaceAll(responseSolveRoute.results.directions);
                        self.distance(Math.round(responseSolveRoute.results.totalLength));
                        self.driveTime(Math.round(responseSolveRoute.results.totalTime));
                        self.displayDistance(self.formatDistance(responseSolveRoute.results.totalLength));
                        self.displayDuration(self.formatTime(responseSolveRoute.results.totalTime));
                        var pics = [];
                        ko.utils.arrayForEach(self.customRouteViaPoint(), function (items, index) {
                            let urlPic = "~/images/";
                            if (index === (self.customRouteViaPoint().length - 1)) {
                                urlPic += "pin_destination.png";

                            } else {
                                urlPic += "pin_poi_" + (index + 1).toString() + ".png";
                            }
                            pics.push(self.resolveUrl(urlPic));
                        });
                        self.drawRoute(param.stops, pics, self.path(), self.routeColor(), undefined, undefined, responseSolveRoute.results);
                        self.isSolveRoute(true);
                        
                        if((typeof params == "boolean") && params){
                            self.saveProcess();
                        } 
                    }
                }
            }).fail((e) => {
                this.handleError(e);
            }).always(() => {
                this.isBusy(false);
            });
            
        }

    }
    formatTime(time) {

        let timeHour = time / 60;
        let timeMin = time % 60;
        let textTime = "";
        if (timeHour >= 1) {
            textTime = (Math.floor(timeHour)) + (WebConfig.userSession.currentUserLanguage == "en-US" ? " hr " : " ชม. ");
        }
        textTime += (Math.floor(timeMin)) + (WebConfig.userSession.currentUserLanguage == "en-US" ? " min " : " น. ");

        return textTime;
    }

    formatDistance(distance) {

        let lengthUnit = distance >= 1000 ? WebConfig.userSession.currentUserLanguage == "en-US" ? " km. " : " กม. " : WebConfig.userSession.currentUserLanguage == "en-US" ? " m. " : " ม.";
        let dist = distance > 1000 ? ((distance / 1000).toFixed(2)) : (distance).toFixed(2);

        return dist + lengthUnit;
    }

    createAreaByRoute() {

        var page = "cw-geo-fencing-custom-route-manage-custom-area-create";
        var options = {
            mode: this.mode,
            path: this.path(),
            color: this.routeColor()
        };
        this.navigate(page, options);

    }

    viewOnMap() {
        this.solveRoute();
    }

    printMapAndDirection() {

        MapManager.getInstance().printRouteMap(this.displayDistance(), this.displayDuration(), this.routeDirection());
    }
    getBaseLocation() {

        var url = window.location.href.split("?");
        if (url.length > 1) {
            url.pop();
        }

        url = url[0].split("/");

        url.pop();
        var baseLocation = url.join("/");
        return baseLocation;
    }
    sortByKey(array, key) {
        return array.sort(function (a, b) {
            var x = a[key];
            var y = b[key];
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
    }
    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {

        this.name.extend({
            trackChange: true
        });
        this.description.extend({
            trackChange: true
        });
        this.routeColor.extend({
            trackChange: true
        });
        this.code.extend({
            trackChange: true
        });
        this.customRouteViaPoint.extend({
            trackChange: true
        });
        this.distance.extend({
            trackChange: true
        });
        this.driveTime.extend({
            trackChange: true
        });
        // Validation
        this.name.extend({
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });
        this.code.extend({
            serverValidate: {
                params: "Code",
                message: this.i18n("M010")()
            }
        });
        this.routeColor.extend({
            required: true
        });
        this.distance.extend({
            required: true
        });
        this.driveTime.extend({
            required: true
        });
        this.settings.extend({
            validation: {
                validator: function (settings) {
                    return settings.settingMode &&
                        settings.settingOption && settings.settingId
                },
                message: this.i18n("M001")()
            }
        });

        this.customRouteViaPoint.extend({
            arrayRequired: {
                params: true,
                message: this.i18n("M001")()
            }
        });

        this.accessibleBusinessUnits.extend({
            trackArrayChange: true
        });

        this.accessibleBusinessUnits.extend({
            trackChange: true
        });

        this.accessibleBusinessUnits.extend({
            arrayRequired: true
        });

        this.validationModel = ko.validatedObservable({
            name: this.name,
            routeColor: this.routeColor,
            settings: this.settings,
            customRouteViaPoint: this.customRouteViaPoint,
            distance: this.distance,
            driveTime: this.driveTime,
            accessibleBusinessUnits: this.accessibleBusinessUnits
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        this.clearMap();
        this.customRouteViaPoint([])
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {

    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {

    }

    onSelectSubMenuItem(index) {
        this.selectedSubMenuItem(index);
        var page = "";
        var options = {
            mode: this.mode
        };

        switch (index) {
            case 1:
                page = "cw-geo-fencing-custom-route-manage-setting";
                options.settings = this.settings();
                break;

        }

        this.navigate(page, options);
    }



    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }


    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender)  {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            this.solveRoute(true);

            // if (!this.validationModel.isValid()) {
              
            //     this.validationModel.errors.showAllMessages();
            //     return;
            // }

            // var model = this.generateModel();

            // switch (this.mode) {
            //     case Screen.SCREEN_MODE_CREATE:
            //     console.log("model>>",model)
            //         // this.isBusy(true);
            //         // this.webRequestRoute.createCustomRoute(model).done((response) => {
            //         //     this.publishMessage("cw-geo-fencing-custom-route-changed", response ? response.id : null);
            //         //     this.close(true);
            //         // }).fail((e) => {
            //         //     this.handleError(e);
            //         // }).always(() => {
            //         //     this.isBusy(false);
            //         // });
            //         break;
            //     case Screen.SCREEN_MODE_UPDATE:
            //         this.isBusy(true);
            //         this.webRequestRoute.updateCustomRoute(model).done((response) => {
            //             this.publishMessage("cw-geo-fencing-custom-route-changed", this.customRouteId);
            //             this.close(true);
            //         }).fail((e) => {
            //             this.handleError(e);
            //         }).always(() => {
            //             this.isBusy(false);
            //         });
            //         break;
            // }

        }
    }

    saveProcess() {

        if (!this.validationModel.isValid()) {
            this.validationModel.errors.showAllMessages();
            return;
        }

        var model = this.generateModel();
      
        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
          
                this.isBusy(true);
                this.webRequestRoute.createCustomRoute(model).done((response) => {
                    this.publishMessage("cw-geo-fencing-custom-route-changed", response ? response.id : null);
                    this.close(true);
                }).fail((e) => {
                    this.handleError(e);
                }).always(() => {
                    this.isBusy(false);
                });
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.isBusy(true);
                this.webRequestRoute.updateCustomRoute(model).done((response) => {
                    this.publishMessage("cw-geo-fencing-custom-route-changed", this.customRouteId);
                    this.close(true);
                }).fail((e) => {
                    this.handleError(e);
                }).always(() => {
                    this.isBusy(false);
                });
                break;
        }

    }

    filterPoiType(data, filterType) {

        var filter = [];

        switch (filterType) {
            case 1:
                filter = _.filter(data, (x) => {
                    return x.value != Enums.ModelData.PoiType.Private;
                });
                break;
            case 2:
                filter = _.filter(data, (x) => {
                    return x.value != Enums.ModelData.PoiType.Public;
                });
                break;
            default:
                break;

        }
        return filter;
    }

    /**
     * Choose Accessible Business Unit
     */
    addAccessibleBusinessUnit() {
        var selectedBusinessUnitIds = this.accessibleBusinessUnits().map((obj) => {
            return obj.businessUnitId;
        });
        this.navigate("cw-geo-fencing-custom-route-manage-accessible-business-unit-select", {
            mode: this.mode,
            selectedBusinessUnitIds: selectedBusinessUnitIds
        });
    }
}

export default {
    viewModel: ScreenBase.createFactory(CustomRouteManageScreen),
    template: templateMarkup
};
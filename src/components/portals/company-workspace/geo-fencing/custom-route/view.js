﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import ScreenBase from "../../../screenbase";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebRequestCustomRoute from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomRoute";
import WebRequestMap from "../../../../../app/frameworks/data/apitrackingcore/webRequestMap";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";
import * as EventAggregatorConstant from "../../../../../app/frameworks/constant/eventAggregator";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";

import { Enums,Constants } from "../../../../../app/frameworks/constant/apiConstant";
/**
 * 
 * 
 * @class CustomRouteViewScreen
 * @extends {ScreenBase}
 */
class CustomRouteViewScreen extends ScreenBase {
    /**
     * Creates an instance of CustomRouteViewScreen.
     * 
     * @param {any} params
     * 
     * @memberOf CustomRouteViewScreen
     */
    constructor(params) {
            super(params);

            this.bladeTitle(this.i18n("CustomRoutes_View")());
            this.bladeSize = BladeSize.Medium;

            this.customRouteId = this.ensureNonObservable(params.customRouteId);
            this.name = ko.observable();
            this.code = ko.observable();
            this.description = ko.observable();
            this.routeColor = ko.observable();
            this.category = ko.observable();
            this.settingOption = ko.observable();
            this.settingMode = ko.observable();
            this.createdDate = ko.observable();
            this.createdBy = ko.observable();
            this.lastUpdatedDate = ko.observable();
            this.lastUpdatedBy = ko.observable();
            this.customRouteViaPoint = ko.observableArray();
            this.routeDirection = ko.observableArray([])
            this.isFirstSolve = ko.observable(true);
            
            this.selectedTabIndex = ko.observable(0);
            this.customRouteData = ko.observable();
            this.distance = ko.observable();
            this.driveTime = ko.observable();
            this.responseRoute=ko.observable();
            this.isPrint = ko.observable(false);
            this.accessibleBusinessUnits = ko.observableArray();
            this.orderAccessibleBusinessUnits = ko.observable([
                [1, "asc"]
            ]);

            this.poiType = ko.observable();
            this.poiUserPermissionType = ko.observable();
            this.poiTypeEnum = ko.observable();

            this.subscribeMessage("cw-geo-fencing-custom-route-changed", () => {
                this.isBusy(true);
                this.webRequestRoute.getCustomRoute(this.customRouteId).done((response) => {
                    let category = response.category != null ? response.category.name : response.category;
                    this.customRouteData(response);
                    this.name(response.name);
                    this.code(response.code);
                    this.description(response.description);
                    this.category(category);
                    this.settingOption(response.routeOption.routeOptionDisplayName);
                    this.settingMode(response.routeOption.routeModeDisplayName);
                    this.createdDate(response.formatCreateDate);
                    this.createdBy(response.createBy);
                    this.lastUpdatedDate(response.formatUpdateDate);
                    this.lastUpdatedBy(response.updateBy);
                    this.routeColor(response.color);
                    this.poiType(response.poiTypeDisplayName);
                    this.poiUserPermissionType(response.poiUserPermissionType);
                    this.poiTypeEnum(response.poiType);
                    var sortViaPoints=this.sortByKey(response.viaPoints,"order");
                    this.customRouteViaPoint(sortViaPoints);
                    this.distance(this.formatDistance(response.distance));
                    this.driveTime(this.formatTime(response.driveTime));
                    this.solveRoute(true);
                    this.isBusy(false);
                   

                    var mappedSelectedBUs = response.accessibleBusinessUnits.map((bu) => {
                        let mbu = bu;
                        mbu.businessUnitId = bu.businessUnitId;
                        mbu.businessUnitName = bu.businessUnitName;
                        return mbu;
                    });
                    this.accessibleBusinessUnits(mappedSelectedBUs);
                }).fail((e) => {
                    this.handleError(e);
                    this.isBusy(false);
                });
            });
          // Register event subscribe in constructor.
        this._eventAggregator.subscribe(EventAggregatorConstant.MAP_COMMAND_COMPLETE, this.onMapComplete.bind(this));
    }
    onMapComplete(response) {

            var self = this;
            if (response.command == "draw-one-route") {
                if (this.isPrint() === true) {
                    setTimeout(() => { 
                        MapManager.getInstance().printRouteMap(this.distance(),this.driveTime(),this.routeDirection());
                    }, 500);
                   
                    this.isPrint(false);
                }
            }
        
    }
    /**
     * Get WebRequest specific for customroute module in Web API access.
     * @readonly
     */
    get webRequestRoute() {
            return WebRequestCustomRoute.getInstance();
    }
    /**
     * Get WebRequest specific for map module in Web API access.
     * @readonly
     */
    get webRequestMap() {
            return WebRequestMap.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {

            if (!isFirstLoad) return;
            var dfd = $.Deferred();
            this.webRequestRoute.getCustomRoute(this.customRouteId).done((response) => {
                // let mock = [{length: 0,
                //     maneuverType: "Depart",
                //     order: 1,
                //     point: [[100.52523529935573,13.722063200575578]],
                //     text: "Start at Sam Phum Garden",
                //     time: 0}]
             
                // console.log("response",response)
                // this.routeDirection([{length: 0,
                //         maneuverType: "Depart",
                //         order: 1,
                //         point: [[100.52523529935573,13.722063200575578]],
                //         text: "Start at Sam Phum Garden",
                //         time: 0}])
                let category = response.category != null ? response.category.name : response.category;
                this.accessibleBusinessUnits(response.accessibleBusinessUnits);
                this.customRouteData(response);
                this.name(response.name);
                this.code(response.code);
                this.description(response.description);
                this.category(category);
                this.settingOption(response.routeOption.routeOptionDisplayName);
                this.settingMode(response.routeOption.routeModeDisplayName);
                this.createdDate(response.formatCreateDate);
                this.createdBy(response.createBy);
                this.lastUpdatedDate(response.formatUpdateDate);
                this.lastUpdatedBy(response.updateBy);
                this.routeColor(response.color);
                var sortViaPoints=this.sortByKey(response.viaPoints,"order");
                this.customRouteViaPoint(sortViaPoints);
                this.distance(this.formatDistance(response.distance));
                this.driveTime(this.formatTime(response.driveTime));
                this.poiType(response.poiTypeDisplayName);
                this.poiUserPermissionType(response.poiUserPermissionType);
                this.poiTypeEnum(response.poiType);
                //this.solveRoute(false);
                dfd.resolve();
            }).fail((e) => {
                dfd.reject(e);
            });
            return dfd;
        }
    /**
      Draw Route
    */
    drawRoute(stops, pics, path, color = "#7000db", width = 5, opacity = 1, attributes = null) {

        MapManager.getInstance().drawOneRoute(this.id, stops, pics, path, color, width, opacity, attributes);
    }
    getBaseLocation(){

        var url = window.location.href.split("?");
        if (url.length > 1)
        { url.pop(); }

        url = url[0].split("/");

        url.pop();
        var baseLocation = url.join("/");
        return baseLocation;
    }
    onClickTab(){
        if(this.isFirstSolve()){
            
            this.solveRoute(false)
            this.isFirstSolve(false)
        }
        
    }
    solveRoute(drawRoute = false) {

        var self=this;
        if (this.customRouteData) {
            var param = {
                routeMode: this.customRouteData().routeOption.routeMode,
                routeOption: this.customRouteData().routeOption.routeOption,
                lang: WebConfig.userSession.currentUserLanguage == "en-US" ? "E" : "L",
                stops: []
            };

            if (this.customRouteData().viaPoints.length >= 2) {
                ko.utils.arrayForEach(this.customRouteData().viaPoints, function(items, index) {
                    param.stops.push({
                        name: items.name,
                        lat: items.latitude,
                        lon: items.longitude
                    });
                });

                this.webRequestMap.solveRoute(param).done((responseSolveRouteString) => {
                    //console.log("boy debug :: responseSolveRoute",responseSolveRouteString);
                    var responseSolveRoute = JSON.parse(responseSolveRouteString);
                    if (responseSolveRoute != null) {
                        if (responseSolveRoute.results != null) {
                            this.responseRoute(responseSolveRoute);
                            ko.utils.arrayForEach(responseSolveRoute.results.directions, function(items, index) {
                                items = $.extend(items, {
                                    order: (index + 1)
                                });
                            });

                           

                         
                            // var data = self.array().slice(0);
                            // self.array([]);
                            // self.array(data);

                            // var data = self.routeDirection().slice(0);
                            // self.routeDirection([]);
                            // self.routeDirection(data);
                            this.routeDirection.replaceAll(responseSolveRoute.results.directions);
                            drawRoute = true;

                            var pics = [];
                            ko.utils.arrayForEach(this.customRouteData().viaPoints, function(items, index) {
                                let urlPic = "~/images/";
                                if(index===(self.customRouteData().viaPoints.length-1)){
                                    urlPic+="pin_destination.png";
                                
                                }else{
                                    urlPic+="pin_poi_"+(index+1).toString()+".png";
                                } 
                                pics.push(self.resolveUrl(urlPic));
                            });
                            if (drawRoute) {
                                this.drawRoute(param.stops, pics, responseSolveRoute.results.route, this.customRouteData().color, undefined, undefined, this.customRouteData());
                                this.minimizeAll(false);
                            }

                        }
                    }

                }).fail((e) => {
                    var errorObj = e.responseJSON;
                    console.log("errorObj", errorObj);
                });
            }
        }

    }
    formatTime(time) {

        let timeHour = time / 60;
        let timeMin = time % 60;
        let textTime = "";
        if (timeHour >= 1) {
            textTime = (Math.floor(timeHour)) + (WebConfig.userSession.currentUserLanguage == "en-US" ? " hr " : " ชม. ");
        }
        textTime += (Math.floor(timeMin)) + (WebConfig.userSession.currentUserLanguage == "en-US" ? " min " : " น. ");

        return textTime;
    }

    formatDistance(distance) {

            let lengthUnit = distance >= 1000 ? WebConfig.userSession.currentUserLanguage == "en-US" ? " km. " : " กม. " : WebConfig.userSession.currentUserLanguage == "en-US" ? " m. " : " ม.";
            let dist = distance > 1000 ? ((distance / 1000).toFixed(2)) : (distance).toFixed(2);

            return dist + lengthUnit;
    }
    sortByKey(array, key) {
        return array.sort(function(a, b) {
            var x = a[key]; var y = b[key];
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
            // console.log("test >>", this.routeDirection())
            MapManager.getInstance().clear(this.id);
    }
    
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {

        if (WebConfig.userSession.hasPermission(Constants.Permission.UpdateCustomRoute) &&
            (WebConfig.userSession.userType == Enums.UserType.BackOffice || WebConfig.userSession.permissionType == this.poiUserPermissionType())
        )
        {
            if (this.poiTypeEnum() == Enums.ModelData.PoiType.Shared) { // check ว่าเป็น Shared ไหม
                if (WebConfig.userSession.hasPermission(Constants.Permission.SharedCustomRoute)) { // เช็คว่า มี Permission Shared ไหม
                    commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));
                }
            }
            else {
                commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));

            }
        }

        commands.push(this.createCommand("cmdView", this.i18n("Common_ViewOnMap")(), "svg-cmd-search"));

        if (WebConfig.userSession.hasPermission(Constants.Permission.DeleteCustomRoute) &&
            (WebConfig.userSession.userType == Enums.UserType.BackOffice || WebConfig.userSession.permissionType == this.poiUserPermissionType())
        )
        {
            if (this.poiTypeEnum() == Enums.ModelData.PoiType.Shared) {
                if (WebConfig.userSession.hasPermission(Constants.Permission.SharedCustomRoute)) {
                    commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
                }
            }
            else {
                commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));

            }
        }

    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
            super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdUpdate":
                this.navigate("cw-geo-fencing-custom-route-manage", {
                    mode: "update",
                    customRouteId: this.customRouteId
                });
                break;
            case "cmdDelete":

                //Show dialogBox messages for delete item
                this.showMessageBox(null, this.i18n("M048")(),
                    BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.webRequestRoute.deleteCustomRoute(this.customRouteId).done(() => {
                                this.isBusy(false);
                                this.publishMessage("cw-geo-fencing-custom-route-deleted");
                                this.close(true);
                            }).fail((e) => {
                                this.isBusy(false);
                                this.handleError(e);
                            });
                            break;
                    }
                });

                break;
            case "cmdView":
                this.solveRoute(true);
                break;
        }
    }

    printMapAndDirection() {
        this.isPrint(true);
        var self=this;
        var stops=[];
        ko.utils.arrayForEach(this.customRouteData().viaPoints, function(items, index) {
            stops.push({
                name: items.name,
                lat: items.latitude,
                lon: items.longitude
            });
        });
        var pics = [];
        ko.utils.arrayForEach(this.customRouteData().viaPoints, function(items, index) {
            let urlPic = "~/images/";
            if(index===(self.customRouteData().viaPoints.length-1)){
                urlPic+="pin_destination.png";
                                
            }else{
                urlPic+="pin_poi_"+(index+1).toString()+".png";
            } 
            pics.push(self.resolveUrl(urlPic));
        });
        this.drawRoute(stops, pics, this.responseRoute().results.route, this.customRouteData().color);
        
    
    }

}

export default {
    viewModel: ScreenBase.createFactory(CustomRouteViewScreen),
    template: templateMarkup
};
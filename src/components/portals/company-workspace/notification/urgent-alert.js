﻿import ko from "knockout";
import templateMarkup from "text!./urgent-alert.html";
import ScreenBase from "../../screenbase";
import WebRequestAlert from "../../../../app/frameworks/data/apitrackingcore/webRequestAlert";
import {EntityAssociation} from "../../../../app/frameworks/constant/apiConstant";
import AssetInfo from "../asset-info";
import MapManager from "../../../controls/gisc-chrome/map/mapManager";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";

class UrgentAlertNotificationScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Blade Properties
        this.bladeTitle(this.i18n("Notifications_ViewUrgentAlert")());

        // Properties.
        this.alertId = this.ensureNonObservable(params.id, -1);
        this.alertType = null;
        this.alertDateTime = null;
        this.alertMessage = null;
        this.alertBusinessUnitPath = null;
        this.deliveryMen = null;
        this.driver = null;
        this.location = null;
        this.features = [];
        this.latitude = null;
        this.longitude = null;

        // Verify permission set.
        this.canViewBusinessUnit = WebConfig.userSession.hasPermission(Constants.Permission.ViewBusinessUnit);

        // Service properties.
        this.webRequestAlert = WebRequestAlert.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
if(!isFirstLoad) {
            return;
        }

        var dfdAlertLoader = $.Deferred();
        var associations = [
            EntityAssociation.Alert.BusinessUnit,
            EntityAssociation.Alert.Messages,
            EntityAssociation.Alert.Vehicle,
            EntityAssociation.Alert.DeliveryMan,
            EntityAssociation.Alert.Driver,
            EntityAssociation.Alert.CoDrivers,
            EntityAssociation.Alert.FeatureValues
        ];

        // Calling service to get alert information.
        this.webRequestAlert.getAlert(this.alertId, associations)
        .done((result) => {
            // Pouplate model to the screen.
            if(result) {
                this.alertType = result.alertTypeDisplayName;
                this.alertDateTime = result.formatAlertDateTime;
                this.alertMessage = result.message;
                this.alertBusinessUnitPath = result.businessUnitPath;

                if(result.email) {
                    this.deliveryMen = `${result.username} (${result.email})`;
                }
                else {
                    this.deliveryMen = result.username;
                }

                this.driver = result.driverName;
                this.location = result.location;
                this.features = AssetInfo.getStatuses(result, WebConfig.userSession);
                this.latitude = result.latitude;
                this.longitude = result.longitude;
            }

            dfdAlertLoader.resolve();
        })
        .fail((e) => {
            dfdAlertLoader.reject(e);
        });

        return dfdAlertLoader;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actClose", this.i18n("Common_Close")()));
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdViewOnMap", this.i18n("Common_ViewOnMap")(), "svg-icon-customroute-viewonmap"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        switch (sender.id) {
            case "actClose":
                this.close(true);
                break;
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdViewOnMap":
                let location = {
                    lat: this.latitude,
                    lon: this.longitude
                },
                symbol = {
                    url: this.resolveUrl("~/images/pin_alerts.png"),
                    width: '35',
                    height: '50',
                    offset: {
                        x: 0,
                        y: 23.5
                    },
                    rotation: 0
                };

                MapManager.getInstance().drawOnePointZoom("A" + this.alertId, location, symbol, WebConfig.mapSettings.defaultZoom, null);
                break;
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(UrgentAlertNotificationScreen),
    template: templateMarkup
};
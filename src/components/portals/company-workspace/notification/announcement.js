﻿import ko from "knockout";
import templateMarkup from "text!./announcement.html";
import ScreenBase from "../../screenbase";
import WebRequestAnnouncement from "../../../../app/frameworks/data/apicore/webRequestAnnouncement";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";

class AnnouncementNotificationScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Notifications_ViewAnnouncement")());

        this.announcementId = this.ensureNonObservable(params.id, -1);
        this.announcementStartDate = ko.observable();
        this.announcementMessage = ko.observable();

        this.webRequestAnnouncement = WebRequestAnnouncement.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     * @return {jQuery Deferred}
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) {
            return;
        }

        var dfdLoadAnnouncement = $.Deferred();

        // Loading announcement from server.
        var filter = {
            ids: [this.announcementId],
            companyId: WebConfig.userSession.currentCompanyId
        };
        this.webRequestAnnouncement.listAnnouncementNotification(filter)
        .done( result => {
            if(result.length) {
                var currentItem = result[0];
                this.announcementStartDate(currentItem.formatStartDate);
                this.announcementMessage(currentItem.message);
            }
            dfdLoadAnnouncement.resolve();
        })
        .fail( error => {
            dfdLoadAnnouncement.reject(error);
        });

        return dfdLoadAnnouncement;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actClose", this.i18n("Common_Close")()));
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        switch (sender.id) {
            case "actClose":
                this.close(true);
                break;
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(AnnouncementNotificationScreen),
    template: templateMarkup
};
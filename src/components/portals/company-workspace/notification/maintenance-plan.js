﻿import ko from "knockout";
import templateMarkup from "text!./maintenance-plan.html";
import ScreenBase from "../../screenbase";
import WebRequestVehicleMaintenancePlan from "../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenancePlan";
import WebRequestVehicle from "../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import WebRequestCompany from "../../../../app/frameworks/data/apicore/webrequestCompany";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import { EntityAssociation } from "../../../../app/frameworks/constant/apiConstant";

class MaintenancePlanNotificationScreen extends ScreenBase {
    constructor(params) {
        super(params);

        // Blade property.
        this.bladeTitle(this.i18n("Common_ViewMaintenancePlan")());

        // Maintenace property.
        this.maintenanceId= -1;
        this.vehicleId = -1;
        this.vehicleDisplayName = ko.observable();
        if(params.data) {
            this.maintenanceId= this.ensureNonObservable(params.data.maintenancePlanId, -1);
            this.vehicleId = this.ensureNonObservable(params.data.vehicleId, -1);
        }

        this.title = ko.observable();
        this.description = ko.observable();
        this.formatMaintenanceDate = ko.observable();
        this.dayRemind = ko.observable();
        this.remainingMaintenanceDay = ko.observable();
        this.currentDistance = ko.observable();
        this.maintenanceDistance = ko.observable();
        this.distanceRemind = ko.observable();
        this.remainingDistance = ko.observable();
        this.estimateCost = ko.observable();
        this.formatAlertVia = ko.observable();
        this.maintenanceStatus = ko.observable();
        this.maintenanceStatusDisplayName = ko.observable();
        this.performBy = ko.observable();
        this.formatActualDate = ko.observable();
        this.actualDistance = ko.observable();
        this.actualCost = ko.observable();
        this.distanceUnit = ko.observable();
        this.currency = ko.observable();

        this.maintenanceTypeName = ko.observable();
        this.maintenanceSubTypeName = ko.observable();
        this.displayCurrentDistance = ko.observable();

        // Service property.
        this.webRequestVehicleMaintenancePlan = WebRequestVehicleMaintenancePlan.getInstance();
        this.webRequestVehicle = WebRequestVehicle.getInstance();
        this.webRequestCompany = WebRequestCompany.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) {
            return;
        }

        // Loading maintenance plan information.
        var dfd = $.Deferred();

        // Call service for the data.

        var dfdVehicle = this.webRequestVehicle.getVehicle(
            this.vehicleId,
            [
                EntityAssociation.Vehicle.Model,
                EntityAssociation.Vehicle.License
            ]
        );
        var dfdVehicleMaintenance = this.webRequestVehicleMaintenancePlan.getVehicleMaintenancePlan(
            this.maintenanceId, 
            [
                EntityAssociation.VehicleMaintenancePlan.CurrentDistance
            ]
        );

        var dfdCompanySettings = this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId);

        $.when(dfdCompanySettings,dfdVehicle, dfdVehicleMaintenance).done((compSetting ,vehicle, plan) => {
            this.populateData(compSetting, vehicle, plan);
            dfd.resolve();
        })
        .fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actClose", this.i18n("Common_Close")()));
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        switch (sender.id) {
            case "actClose":
                this.close(true);
                break;
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }

    /**
     * Populate Data from ajax response
     * @param {any} response
     */
    populateData(compSetting, vehicle, plan) {
        
        this.distanceUnit(compSetting.distanceUnitSymbol);
        this.currency(compSetting.currencySymbol);

        // Pouplate vehicle information.
        this.vehicleDisplayName(
            vehicle.brand + " " + vehicle.model + " (" + vehicle.license.license + ")"
        );

        console.log(plan);

        this.maintenanceTypeName(plan.maintenanceTypeName);
        this.maintenanceSubTypeName(plan.maintenanceSubTypeName);
        this.displayCurrentDistance(plan.formatCurrentDistance);

        // Poupulate maintenance plan.
        this.title(plan.title);
        this.description(plan.description);
        this.formatMaintenanceDate(plan.formatMaintenanceDate);
        this.dayRemind(plan.dayRemind);
        this.remainingMaintenanceDay(plan.remainingMaintenanceDay);
        //this.currentDistance(plan.currentDistance);
        this.maintenanceDistance(plan.formatMaintenanceDistance);
        this.distanceRemind(plan.formatDistanceRemind);
        this.remainingDistance(plan.formatRemainingDistance);
        this.estimateCost(plan.formatEstimateCost);
        this.formatAlertVia(plan.formatAlertVia);
        this.maintenanceStatus(plan.maintenanceStatus);
        this.maintenanceStatusDisplayName(plan.maintenanceStatusDisplayName);
        this.performBy(plan.performBy);
        this.formatActualDate(plan.formatActualDate);
        this.actualDistance(plan.formatActualDistance);
        this.actualCost(plan.formatActualCost);
        //this.distanceUnit(plan.distanceUnitSymbol);
        //this.currency(plan.currencySymbol);
    }
}

export default {
    viewModel: ScreenBase.createFactory(MaintenancePlanNotificationScreen),
    template: templateMarkup
};
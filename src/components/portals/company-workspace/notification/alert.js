﻿import ko from "knockout";
import templateMarkup from "text!./alert.html";
import ScreenBase from "../../screenbase";
import WebRequestAlert from "../../../../app/frameworks/data/apitrackingcore/webRequestAlert";
import {EntityAssociation} from "../../../../app/frameworks/constant/apiConstant";
import AssetInfo from "../asset-info";
import MapManager from "../../../controls/gisc-chrome/map/mapManager";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import { Constants } from "../../../../app/frameworks/constant/apiConstant";
import Utility from "../../../../app/frameworks/core/utility";

import { AssetIcons } from "../../../../app/frameworks/constant/svg"

class AlertNotificationScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Blade Properties
        this.bladeTitle(this.i18n("Notifications_ViewAlert")());
        // Properties.
        this.alertId = this.ensureNonObservable(params.id, -1);
        this.alertType = null;
        this.alertDateTime = null;
        this.alertMessage = null;
        this.alertBusinessUnitPath = null;
        this.vehicle = null;
        this.deliveryMen = null;
        this.driver = null;
        this.coDrivers = null;
        this.location = null;
        this.features = [];
        this.latitude = null;
        this.longitude = null;
        this.isDeliveryMenMode = false;
        this.alertDuration = null;

        this.svgs = {
            "movement": AssetIcons.IconMovementTemplateMarkup,
            "speed": AssetIcons.IconSpeedTemplateMarkup,
            "gps": AssetIcons.IconGpsTemplateMarkup,
            "fuel" : AssetIcons.IconFuelTemplateMarkup,
            "engine" : AssetIcons.IconEngineTemplateMarkup
        };

        this.alertData = null;

        // Verify permission set.
        this.canViewBusinessUnit = WebConfig.userSession.hasPermission(Constants.Permission.ViewBusinessUnit);
        this.canViewDriver = WebConfig.userSession.hasPermission(Constants.Permission.ViewDriverName);
        this.canViewCoDriver = WebConfig.userSession.hasPermission(Constants.Permission.ViewCoDriverName);

        // Service properties.
        this.webRequestAlert = WebRequestAlert.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) {
            return;
        }

        var dfdAlertLoader = $.Deferred();
        var associations = [
            EntityAssociation.Alert.BusinessUnit,
            EntityAssociation.Alert.Messages,
            EntityAssociation.Alert.Vehicle,
            EntityAssociation.Alert.DeliveryMan,
            EntityAssociation.Alert.Driver,
            EntityAssociation.Alert.CoDrivers,
            EntityAssociation.Alert.FeatureValues
        ];

        // Calling service to get alert information.
        this.webRequestAlert.getAlert(this.alertId, associations)
        .done((result) => {
            // Pouplate model to the screen.
            if(result) {
                this.alertData = result;

                this.alertType = result.alertTypeDisplayName;
                this.alertDateTime = result.formatAlertDateTime;
                this.alertMessage = result.message;
                this.alertBusinessUnitPath = result.businessUnitPath;
                this.vehicle = `${result.vehicleBrand} ${result.vehicleModel} (${result.vehicleLicense})`;

                if(result.email) {
                    this.deliveryMen = `${result.username} (${result.email})`;
                }
                else {
                    this.deliveryMen = result.username;
                }

                this.driver = Utility.createDriverNameWithTelephoneTag(result.driverName, result.driverMobile);//result.driverName;
                this.coDrivers = result.coDrivers;
                this.location = result.location;
                this.features = AssetInfo.getStatuses(result, WebConfig.userSession);
                this.latitude = result.latitude;
                this.longitude = result.longitude;
                this.alertDuration = result.alertDurationFormat;

                // Detect deliverymen mode.
                if(!_.isEmpty(result.username)) {
                    this.isDeliveryMenMode = true;
                }
            }

            dfdAlertLoader.resolve();
        })
        .fail((e) => {
            dfdAlertLoader.reject(e);
        });

        return dfdAlertLoader;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actClose", this.i18n("Common_Close")()));
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdViewOnMap", this.i18n("Common_ViewOnMap")(), "svg-icon-customroute-viewonmap"));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        switch (sender.id) {
            case "actClose":
                this.close(true);
                break;
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdViewOnMap":
                // let location = {
                //     lat: this.latitude,
                //     lon: this.longitude
                // },
                // symbol = {
                //     url: this.resolveUrl("~/images/pin_alerts.png"),
                //     width: '35',
                //     height: '50',
                //     offset: {
                //         x: 0,
                //         y: 23.5
                //     },
                //     rotation: 0
                // };

                // MapManager.getInstance().drawOnePointZoom("A" + this.alertId, location, symbol, WebConfig.mapSettings.defaultZoom, null);
                // break;

                this.drawAlertOnMap(this.latitude, this.longitude, this.alertData);

                break;
        }
    }

    /**
     * Draw alert on map
     * @param {any} lat
     * @param {any} lon
     */
    drawAlertOnMap(lat, lon, data) {

        data["svgIcon"] = this.svgs;

        MapManager.getInstance().drawAlertPoint("A" + this.alertId, 
        {
            lat: lat,
            lon: lon
        }, 
        {
            url: this.resolveUrl("~/images/pin_alerts.png"),
            width: WebConfig.mapSettings.symbolWidth,
            height: WebConfig.mapSettings.symbolHeight,
            offset: {
                x: WebConfig.mapSettings.symbolOffsetX,
                y: WebConfig.mapSettings.symbolOffsetY
            },
            rotation: WebConfig.mapSettings.symbolRotation
        }, 
        WebConfig.mapSettings.defaultZoom,
        data);
    }
}

export default {
    viewModel: ScreenBase.createFactory(AlertNotificationScreen),
    template: templateMarkup
};
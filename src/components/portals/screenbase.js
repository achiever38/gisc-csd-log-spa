import ko from "knockout";
import ComponentBase from "../componentBase";
import Navigation from "../controls/gisc-chrome/shell/navigation";
import WidgetNavigation from "../controls/gisc-chrome/shell/widgetNavigation";
import Logger from "../../app/frameworks/core/logger";
import * as BladeSize from "../../app/frameworks/constant/bladeSize";
import * as BladeMargin from "../../app/frameworks/constant/bladeMargin";
import * as BladeDialog from "../../app/frameworks/constant/bladeDialog";
import * as Screen from "../../app/frameworks/constant/screen";
import EventAggregator from "../../app/frameworks/core/eventAggregator";
import GC from "../../app/frameworks/core/gc";
import Utility from "../../app/frameworks/core/utility";
import MessageboxManager from "../../components/controls/gisc-chrome/messagebox/messageboxManager";
import {Router} from "../../app/router";


/**
 * Base class for all screen that render under Blade
 * 
 * @class ScreenBase
 * @extends {ComponentBase}
 */
class ScreenBase extends ComponentBase {

    /**
     * Creates an instance of ScreenBase.
     * @public
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.bladeMode = this.ensureNonObservable(params.mode, null);
        this.bladeSize = BladeSize.Small;
        this.bladeCanMaximize = true;
        this.bladeIsMaximized = false;
        this.bladeMargin = BladeMargin.Normal;
        this.bladeTitle = ko.observable("Untitled");
        this.bladeDescription = ko.observable("");
        this.actionButtons = ko.observableArray();
        this.commandButtons = ko.observableArray();
        this.navigationService = Navigation.getInstance();
        this.widgetService = WidgetNavigation.getInstance();

        this.isBusy = ko.observable(false);

        // Internal members.
        this._eventAggregator = new EventAggregator();
        this._blade = null;

        // Subscribe some properties, the subscription will be disposed later
        this._isBusySubscribeRef = this.isBusy.subscribe((isBusy) => {
            if(this._blade) {
                this._blade.isBusy(isBusy);

                _.forEach(this.actionButtons(), (ab) => {
                    ab.enable(!isBusy);
                });
                _.forEach(this.commandButtons(), (cb) => {
                    cb.enable(!isBusy);
                });
            }
        });

        // use for save/load current blade scroll position
        this.bladeScrollPosition = null;
    }

    /**
     * Additional lifecycle for building blade's toolbar and command bar.
     * @private
     */
    buildButtons() {
        if (this.isFirstLoad()) {
            this.buildActionBar(this.actionButtons);
            this.verifyButtons(this.actionButtons);

            // Skip build command bar if current screen is in readonly mode.
            if(Screen.SCREEN_MODE_READONLY !== this.bladeMode) {
                this.buildCommandBar(this.commandButtons);
                this.verifyButtons(this.commandButtons);
            }
        }
    }

    /**
     * Additional lifecycle for buidling extend rules
     * @private
     */
    buildExtendRules() {
        if(this.isFirstLoad()) {
            this.setupExtend();
        }
    }

    /**
     * Verify if buttons observable array contains invalid sequence.
     * @private
     * @param {any} buttons
     */
    verifyButtons(buttons) {
        buttons().forEach((button)=>{
            if(!button) this.throwError(1, "Button is not valid: " + button);
        });
    }

    /**
     * Handle Track user page with analytics.
     */
    trackAnalyticsPageView() {
        // Track google analytics for first load only.
        if(this.isFirstLoad()) {
            var currentRoute = Router.getInstance().getCurrentRoute();

            // If current route availble then track.
            if(currentRoute && currentRoute.url) {
                ga("send", "pageview", currentRoute.url, {
                    title: this.bladeTitle()
                });
            }
        }
    }

    /**
     * Life cycle: trigger when DOM is redering to page.
     * @private
     * @returns
     */
    onViewAttach() {
        var task = null;

        try {
            var self = this;

            // Perform analytics tracking.
            self.trackAnalyticsPageView();

            // Geting deferred from screen loading.
            task = super.onViewAttach();

            if (task && task.state) {
                // Using promise so waiting sucess from view model response.
                task.done((data) => {
                    self.buildExtendRules();
                    self.buildButtons();
                }).fail((data) => {
                    self.throwError(0, "Cannot process view model onLoad method.");
                });
            } else {
                self.buildExtendRules();
                self.buildButtons();
            }
        } catch(e) {
            Logger.error(e);
        }

        return task;
    }

    /**
     * Life cycle: trigger when adjacent child (only one level) is closed
     * @public
     */
    onChildScreenClosed() {}

    /**
     * Life cycle: for child screen to build available command button collection.
     * @public
     * @param {any} allCommands
     */
    buildCommandBar(allCommands) {}

    /**
     * Life cycle: for child screen to build available action button collection.
     * @public
     * @param {any} allButtons
     */
    buildActionBar(allButtons) {}

    /**
     * Life cycle: for child screen to setup any extend, such as validation rules, change tracking.
     * @public
     */
    setupExtend() {}

    /**
     * Navigate to target moduleName on a new Blade cascading from current one asynchronous.
     * @public
     * @param {any} moduleName
     * @param {any} [options=null]
     * @param {boolean} [isNewJourney=false]
     * @returns jQuery Deferred.
     */
    navigate(moduleName, options = null, isNewJourney = false) {
        var shouldContinueDfd = $.Deferred();
        if(isNewJourney) {
            shouldContinueDfd.resolve();
        } else {
            var journey = this.navigationService.activeItem();
            var canNavigate = this.navigationService.canNavigate(moduleName, journey, this._blade);

            if(!canNavigate) {
                // Confirm user with discard change.
                MessageboxManager.getInstance().showMessageBox("", this.i18n("M100")(), BladeDialog.DIALOG_OKCANCEL).done((response)=> {
                    if(BladeDialog.BUTTON_OK === response) {
                        shouldContinueDfd.resolve();
                    }
                    else {
                        shouldContinueDfd.reject();
                    }
                }).fail(()=>{
                    shouldContinueDfd.reject();
                });
            } else {
                shouldContinueDfd.resolve();
            }
        }

        // Execute when user confirmation completed.
        var navigateResultDfd = $.Deferred();
        $.when(shouldContinueDfd).done(()=>{
            this.forceNavigate(moduleName, options, isNewJourney);
            navigateResultDfd.resolve();
        }).fail(()=>{
            navigateResultDfd.reject();
        });

        return navigateResultDfd;
    }

    /**
     * Navigate to target moduleName on a new Window
     * @public
     * @param {any} moduleName
     * @param {any} [options=null]
     */
    widget(componentName, options = null, widgetOptions = null){
        this.widgetService.widget(componentName, options, widgetOptions);
    }

    closeWidget(){
        this.widgetService.closewidget();
    }   
    /**
     * Force navigate to target moduleName on a new Blade cascading from current one.
     * @public
     * @param {any} moduleName
     * @param {any} [options=null]
     * @param {boolean} [isNewJourney=false]
     * 
     * @memberOf ScreenBase
     */
    forceNavigate(moduleName, options = null, isNewJourney = false) {
        var journey = null;

        // Create new journey automatically for new journey.
        if (!isNewJourney) {
            journey = this.navigationService.activeItem();
        }

        // Verify current screen has binding with factory which has blade reference.
        this.navigationService.navigate(moduleName, options, journey, this._blade, true);
    }

    /**
     * Guard method to check if the Screen can be closed.
     * 
     * @public
     * @returns {boolean}
     */
    canClose() {
        return !this.isDirty();
    }

    /**
     * Close the current blade.
     * @public
     */
    close(forceClose = false) {
        // Use Journey.tryDeactivate instead of deactivate, to trigger guard function.
        if (this._blade) {
            var journey = this.navigationService.activeItem();
            if(!forceClose) {
                journey.tryDeactivate(this._blade, true);
            } else {
                journey.deactivate(this._blade, true);
            }
        }
    }
    closeByName(name) {
        // Use Journey.tryDeactivate instead of deactivate, to trigger guard function.
        if (this._blade) {
            var journey = this.navigationService.activeItem();
            journey.items().forEach((b) => {
                if(b.componentName === name){
                    journey.deactivate(b, true);
                }
            })
        }
    }
    closeAll(forceClose = false) {
        // Use Journey.tryDeactivate instead of deactivate, to trigger guard function.
        if (this._blade) {
            var journey = this.navigationService.activeItem();
     
                journey.items().forEach((b) => {
                    if(!forceClose) {
                        b.canClose(true)
                        // journey.tryDeactivate(journey.items()[0], true);
                    } else {
                        b.canClose(true)
                        // journey.deactivate(journey.items()[0], true);
                    }
                });
          
           
        }
    }


    /**
    * Restore the current blade.
    * @public
    */
    restore() {
        if (this._blade) {
            this._blade.restore();
        }
    }

    /**
     * Create action button for blade action bar.
     * @public
     * @param {any} id
     * @param {any} text
     * @param {string} [type="default"]
     * @param {boolean} [visible=true]
     * @param {boolean} [enable=true]
     */
    createAction(id, text, type = "default", visible = true, enable = true) {
        var actionButton = {
            id: id,
            title: text,
            type: type,
            visible: this.ensureObservable(visible, true),
            enable: this.ensureObservable(enable, true)
        };
        return actionButton;
    }

    /**
     * Create cancel action.
     * @public
     */
    createActionCancel() {
        return this.createAction("actCancel", this.i18n("Common_Cancel"), "cancel");
    }

    /**
     * Create command button for blade command bar.
     * @public
     * @param {any} id
     * @param {any} text
     * @param {string} [icon="svg-cmd-add"]
     * @param {boolean} [visible=true]
     * @param {boolean} [enable=true]
     * @param {boolean} [active=false]
     */
    createCommand(id, text, icon = "svg-cmd-add", visible = true, enable = true, active = false) {
        var cmdButton = {
            id: id,
            title: text, // Observable allowed.
            icon: icon,
            visible: this.ensureObservable(visible, true),
            enable: this.ensureObservable(enable, true),
            active: this.ensureObservable(active, false),
        };

        cmdButton.effectiveCss = ko.pureComputed(function() {
            var cssClass = "app-commandBar-item";

            if (this.enable()) {
                cssClass += " app-has-hover";
            } else {
                cssClass += " app-commandBar-itemDisabled";
            }

            if (this.active()) {
                cssClass += " app-commandBar-itemActive";
            } 

            return cssClass;
        }, cmdButton);

        return cmdButton;
    }

    /**
     * Handle on Action click when child screen doesn't implement button click.
     * @public
     * @param {any} action
     */
    onActionClick(action) {
        if (action.id === "actCancel") {
            this.close();
        }
    }

    /**
     * Guard method to prevent disabled commands from clickable.
     * @private
     * @param {any} command
     */
    onCommandExcutingClick(command){
        // Prevent disable
        if(command.enable()) {
            this.onCommandClick(command);
        }
    }

    /**
     * Handle on command click when child screen doesn't implement command click.
     * @public
     * @param {any} command
     */
    onCommandClick(command) {}

    /**
     * Internal event when view model is creating
     * @private
     * @param {any} parentBlade
     */
    _onViewModelCreating(parentBlade) {
        Logger.log("Screen vm creating", this.id);
        this._blade = parentBlade;
        this._blade.isMaximized(this.bladeIsMaximized);
    }

    /**
     * Subscribe to Journal based communication.
     * The callback has value of updated message.
     * @public
     * @param {any} messageId
     * @param {any} callback
     */
    subscribeMessage(messageId, callback) {
        // Communication between blades in the same journey require 
        // a unique topic - this comes from combination of journey's id AND topic
        let activeJourney = this.navigationService.activeItem();
        let journalMessageId = activeJourney.id + "_" + messageId;
        this._eventAggregator.subscribe(journalMessageId, callback);
    }

    /**
     * Publish message to Journal based communication. 
     * @public
     * @param {any} messageId
     * @param {any} messageValue
     */
    publishMessage(messageId, messageValue) {
        let activeJourney = this.navigationService.activeItem();
        let journalMessageId = activeJourney.id + "_" + messageId;
        this._eventAggregator.publish(journalMessageId, messageValue);
    }

    /**
     * Dispatch event to control directly, used this with supported controls.
     * 
     * @param {any} controlId
     * @param {any} eventName
     * @param {any} [message=null]
     */
    dispatchEvent(controlId, eventName, message = null) {
        this._eventAggregator.publish(controlId, { 
            eventName: eventName,
            message: message
        });
    }

    /**
     * Internal event when view model is disposing
     * @private
     */
    _onViewModelDisposing() {
        // Dispose all observing subject in EventAggregator
        Logger.log("Screen disposing", this.id);
        this._eventAggregator.destroy();
        this._blade = null;

        // Using GC to cleanup if exists.
        GC.dispose(this);
    }

    /**
     * Display popup status error
     * @public
     * @param {any} text
     */
    displayError(text) {
        this._blade.statusText(this.ensureNonObservable(text));
        this._blade.statusType("fail");
        this._blade.statusVisible(true);
    }

    /**
     * Display popup status success
     * @public
     * @param {any} text
     */
    displaySuccess(text) {
        this._blade.statusText(this.ensureNonObservable(text));
        this._blade.statusType("success");
        this._blade.statusVisible(true);
    }

    /**
     * Clear status bar 
     * @public
     */
    clearStatusBar() {
        this._blade.statusVisible(false);
    }

    /**
     * 
     * Dialog type definitions
     * @readonly
     * @static
     */
    static get DIALOG_OK() {
        return "ok";
    }
    static get DIALOG_YESNO() {
        return "yn";
    }
    static get DIALOG_OKCANCEL() {
        return "oc";
    }

    /**
     * Show DialogBox.
     * @param {any} title
     * @param {any} text
     * @param {any} type
     */
    displayDialog(title, text, type ,callback) {
        Logger.error("Method displayDialog is no longer supported, please use showMessageBox instead.");
    }
    
    /**
     * Display error message on submit from response error.
     * The expect responseError structure:
     * {
     *      message: "There is an existing item with the same value.",
     *      code: "errorCode",
     *      dataDetails: [
     *          "Name", "Code"
     *      ]
     * }
     * This method will call displayError() internally with appropiate errorCode.
     * 
     * @public
     * @param {any} responseError
     */
    displaySubmitError(responseError) {
        Logger.error("Method displaySubmitError is no longer supported, please use handleError instead.");
    }

    /**
     * Display error message on ajax request from response error.
     * The expect responseError structure:
     * {
     *      message: "There is an existing item with the same value.",
     *      code: "errorCode",
     *      dataDetails: [
     *          "Name", "Code"
     *      ]
     * }
     * This method will call displayError() internally with appropiate errorCode.
     * 
     * @param {any} ajaxError a jQuery error object from Deferred.
     */
    handleError(ajaxError) {
        if(!_.isNil(ajaxError) && !_.isNil(ajaxError.responseJSON)) {
            var responseError = ajaxError.responseJSON;

            // Check if code is specific message then handle specific error, else call displayError with formatted message.
            if(!_.isNil(responseError.code) && _.indexOf(["M010", "M065", "M084"], responseError.code) > -1) {
                if(!_.isNil(responseError.dataDetails) && (responseError.dataDetails.length > 0)) {
                    // M084 format message with 2 params
                    if (responseError.code === "M084") {
                        var formattedMsg = Utility.stringFormat(responseError.message, responseError.dataDetails[0], responseError.dataDetails[1]);
                        this.displayError(formattedMsg);
                    }
                    // M010/M065 display inline error message below the field
                    else {
                        // Loop through all prop in this VM that has "serverField" attached
                        for(let key in this) {
                            if (this.hasOwnProperty(key) && 
                                ko.isObservable(this[key]) &&
                                !_.isNil(this[key].serverField)) {
                                
                                // Found serverField on observable, check with dataDetails
                                var serverField = this[key].serverField;
                                
                                // Make sure dataDetails is array
                                var dataDetails = responseError.dataDetails;
                                if(typeof dataDetails === "string") {
                                    dataDetails = [dataDetails];
                                }

                                var matchObj = _.find(dataDetails, function(target) { 
                                    return target === serverField;
                                });
                                if(!_.isNil(matchObj)) {
                                    // Found match, trigger error inline on target field.
                                    var serverMessage = !_.isNil(this[key].serverMessage) ? this[key].serverMessage : "Server validation fail.";
                                    
                                    this[key].setError(serverMessage);
                                    this[key].isModified(true);
                                }
                            }
                        }
                    }
                }
            }
            else {
                // Other error code, call displayError
                var formattedErrorMsg = Utility.stringFormat(responseError.message, responseError.dataDetails);
                this.displayError(formattedErrorMsg);
            }
        }
    }

    
    /**
     * Manually clear error state for given validatableObservable
     * 
     * @param {any} validatableObservable property which has attached validatable (comes from ko-validation)
     */
    clearError(validatableObservable) {
        if(_.isFunction(validatableObservable.clearError)) {
            validatableObservable.clearError();
        }
    }

    /**
     * Guard method to check if current screen and its successors can be closed.
     * 
     * @returns true if current screen and entire children has no pending change, false otherwise.
     */
    journeyCanClose() {
        return this.navigationService.canClose(this.navigationService.activeItem(), this._blade);
    }

    /**
     * Verify current blade is active in journey.
     * @return {boolean}
     */
    isActive() {
        var isActive = false;

        // Check journey of current blade is equal current active blade.
        var activeJourney = this.navigationService.activeItem();
        if(activeJourney) {
            activeJourney.items().forEach((b) => {
                if(this._blade && this._blade.id === b.id) {
                    isActive = true;
                }
            });
        }

        return isActive;
    }

    /**
     * Minimize all blades in current journey.
     * @param {boolean} minimizeCurrent set if current blade should be collapsed.
     */
    minimizeAll(minimizeCurrent = true) {
        // Sometime last blade still open.
        setTimeout(() => {
            var currentId = this._blade.id;
            this.navigationService.minimizeAll(minimizeCurrent, currentId);
        }, 100);
    }


    /**
     * Save current blade scroll position
     * @memberOf ScreenBase
     */
    saveBladeScrollPosition() {
        var $bladeContent = $("#" + this._blade.id + " .app-blade-content");
        this.bladeScrollPosition = {
            'top': $bladeContent.scrollTop(),
            'left': $bladeContent.scrollLeft()
        }
    }

    /**
     * Load blade scroll position
     * @memberOf ScreenBase
     */
    loadBladeScrollPosition() {
        var $bladeContent = $("#" + this._blade.id + " .app-blade-content");
        $bladeContent.scrollTop(this.bladeScrollPosition.top);
        $bladeContent.scrollLeft(this.bladeScrollPosition.left);
        this.bladeScrollPosition = null;
    }

    /**
     * Factory method to create View Model
     * @static
     * @public
     * @param {Clazz} Class of target View Model, derive from ScreenBase.
     * @returns ViewModel of target Clazz
     */
    static createFactory(viewModelClass) {
        var factoryObj = {
            createViewModel: function(params, componentInfo) {
                // debugger;
                var blade = params.blade;

                if (!blade.componentInstance()) {

                    let vmParameter = params.componentOptions;
                    if(!vmParameter) {
                        vmParameter = {};
                    }

                    let vm = new viewModelClass(vmParameter);
                    blade.componentInstance(vm);
                    vm._onViewModelCreating(blade);
                }

                return blade.componentInstance();
            }
        };
        return factoryObj;
    }

    static formatName(responseitem) {
        var data = [];
        for (let i = 0; i < responseitem.length; i++) {
            var dataItem = responseitem[i];
            dataItem.name = dataItem.firstName == null ? dataItem.userName : dataItem.firstName + " " + dataItem.lastName;
            data.push(dataItem);
        }
        return data;
    }
}

export default ScreenBase;
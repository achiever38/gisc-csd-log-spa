﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import FileUploadModel from "../../../../../components/controls/gisc-ui/fileupload/fileuploadModel";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestVehicleIcon from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleIcon";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import {
    Constants,
    Enums,
    EntityAssociation
} from "../../../../../app/frameworks/constant/apiConstant";


const width = 32;
const height = 32;

class ConfigurationVehicleIconManageScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties

        this.mode = this.ensureNonObservable(params.mode);
        this.bladeSize = BladeSize.Medium;

        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("Configurations_CreateVehicleIcon")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("Configurations_UpdateVehicleIcon")());
                break;
        }



        this.isAllCompanies = ScreenHelper.createAllCustomObservableArray();
        this.visibleAccessibleCompanies = ko.pureComputed(() => {
            var a = this.isAllCompaniesSelected();
            if (!a) {
                return true;
            } else {
                return a.value;
            }
        });

        this.vehicleIconId = this.ensureNonObservable(params.vehicleIconId, -1);
        this.name = ko.observable("");
        this.code = ko.observable("");
        this.imageSets = ko.observableArray();
        this.accessibleCompanies = ko.observableArray();
        this.isAllCompaniesSelected = ko.observable();
        this.order = ko.observable([
            [0, "asc"]
        ]);

        // These properties will be used to calculate delta changed.
        this._originalimageSets = [];

        this.imageSetItems = ko.observableArray();

        //Return selected list from accessibleCompanies
        this.subscribeMessage("bo-accessible-companies-selected", (selectedCompanies) => {
            var mappingAccessibleCompanies = selectedCompanies.map((obj) => {
                let mObj = {
                    companyId: obj.id,
                    companyName: obj.name,
                    vehicleIconId: this.vehicleIconId,
                    canManage: true
                };
                return mObj;
            });

            var manageableCompany = mappingAccessibleCompanies;
            var unmanageableCompany = [];
            this.accessibleCompanies().forEach((c) => {
                if (!c.canManage) {
                    unmanageableCompany.push(c);
                }
            });

            var mergeArray = _.concat(manageableCompany, unmanageableCompany);
            this.accessibleCompanies.replaceAll(mergeArray);
        });


        this.previewImage = ko.observable();

        this.previewImageId = ko.observable();
        this.previewImageName = ko.observable();
        this.previewImageSelectedFile = ko.observable();


        this.previewImageUrl = ko.pureComputed(() => {
            if (this.previewImage() && this.previewImage().infoStatus !== Enums.InfoStatus.Delete) {
                return this.previewImage().fileUrl;
            } else {
                return this.resolveUrl("~/images/upload-img-placeholder-100x100.jpg");
            }
        });

        this.isUpload = ko.pureComputed(() => {
            return this.previewImage() && this.previewImage().infoStatus !== Enums.InfoStatus.Delete;
        });
    }

    setupExtend() {
        // Manual setup track change.

        //trackChange
        this.isAllCompaniesSelected.extend({
            trackChange: true
        });

        this.name.extend({
            trackChange: true
        });

        this.code.extend({
            trackChange: true
        });

        this.previewImage.extend({
            trackChange: true
        });

        this.imageSetItems.extend({
            trackChangeByProperty: {
                propertyName: 'isDirty'
            }
        });

        this.accessibleCompanies.extend({
            trackArrayChange: true
        });


        //Required
        this.previewImageSelectedFile.extend({
            fileRequired: true,
            fileExtension: ['png'],
            fileSize: 4,
            fileImageDimension: [width, height, 'equal']
        });

        this.imageSetItems.extend({
            arrayIsValid: 'isValid'
        });

        this.name.extend({
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });

        this.code.extend({
            serverValidate: {
                params: "Code",
                message: this.i18n("M010")()
            }
        });

        this.isAllCompaniesSelected.extend({
            required: true,
        });

        this.validationModel = ko.validatedObservable({
            name: this.name,
            isAllCompaniesSelected: this.isAllCompaniesSelected,
            previewImageSelectedFile: this.previewImageSelectedFile,
            imageSetItems:this.imageSetItems
        });
    }

    get webRequestVehicleIcon() {
        return WebRequestVehicleIcon.getInstance();
    }

    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * 
     * Hook on fileupload before upload
     * @param {any} e
     */
    onBeforeUpload(isValidToSubmit) {
        if (!isValidToSubmit) {
            this.previewImage(null);
            this.previewImageName(null);
        }
    }

    onRemoveFile() {
        this.previewImage(null);
    }

    /**
     * Do something when fileupload fail.
     * 
     * @param {any} e
     * 
     * @memberOf ConfigurationPoiIconManageScreen
     */
    onUploadFail(e) {
        this.handleError(e);
    }


    /**
     * To handle when upload previewImage success. 
     * 
     * @param {any} result
     * 
     * @memberOf ConfigurationVehicleIconManageScreen
     */
    onUploadPreviewSuccess(data) {
        if (data && data.length > 0) {
            var newPreviewImage = data[0];
            newPreviewImage.infoStatus = this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update;
            newPreviewImage.id = this.previewImageId() || 0;
            this.previewImage(newPreviewImage);
        }
    }


    /**
     * 
     * @public
     * @param {any} isFirstLoad
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var dfd = $.Deferred();

        var enumResourceFilter = {
            types: [Enums.ModelData.EnumResourceType.MovementType]
        };

        var d1 = this.webRequestEnumResource.listEnumResource(enumResourceFilter);

        var d2 = (this.mode === Screen.SCREEN_MODE_UPDATE) ? this.webRequestVehicleIcon.getVehicleIcon(this.vehicleIconId, [EntityAssociation.VehicleIcon.PreviewImage, EntityAssociation.VehicleIcon.Images, EntityAssociation.VehicleIcon.AccessibleCompanies]) : null;

        $.when(d1, d2).done((r1, vehicleIcon) => {
            var enumResource = r1["items"];

            enumResource.forEach((e) => {
                this.imageSetItems.push(new ImageSetItem(-1, e.value, e.displayName, Enums.InfoStatus.Add, this.mode, this.onUploadFail));
            });

            if (vehicleIcon) {
                // debugger
                this.name(vehicleIcon.name);
                this.code(vehicleIcon.code);

                if (vehicleIcon.previewImage) {
                    this.previewImage(vehicleIcon.previewImage);
                    this.previewImageId(vehicleIcon.previewImage.id);
                    this.previewImageName(vehicleIcon.previewImage.fileName || "");
                    var file = new FileUploadModel();
                    file.name = vehicleIcon.previewImage.fileName ? vehicleIcon.previewImage.fileName : "";
                    file.extension = "png";
                    file.imageWidth = vehicleIcon.width;
                    file.imageHeight = vehicleIcon.height;
                    this.previewImageSelectedFile(file);
                }

                this.imageSetItems().forEach((img) => {
                    var vImg = vehicleIcon.images;
                    vImg.forEach((v) => {
                        if (img.movementType === v.movementType) {
                            img.id = v.id;
                            img.fileId = v.images[0].id;
                            img.infoStatus = v.infoStatus;
                            img.file(v.images[0]);
                            img.fileName(v.images[0].fileName);
                            img.fileUrl(v.images[0].fileUrl);
                            var file = new FileUploadModel();
                            file.name = v.images[0].fileName ? v.images[0].fileName : "";
                            file.extension = "zip";
                            file.imageWidth = 0;
                            file.imageHeight = 0;
                            img.selectedFile(file);
                        }
                    })


                });

                var isAllCompanies = ScreenHelper.findOptionByProperty(this.isAllCompanies, "value", vehicleIcon.isAllCompanies);
                this.isAllCompaniesSelected(isAllCompanies);
                
                this.accessibleCompanies(vehicleIcon.accessibleCompanies);

            }

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * 
     * @public
     * @param {any} actions
     */
    buildActionBar(actions) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.VehicleIconsManagement)) {
            actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
            actions.push(this.createActionCancel());
        }
    }

    /**
     * Generate model from View Model
     * 
     * @returns model which is ready for ajax request 
     */
    generateModel() {
        var model = {
            id: this.vehicleIconId,
            name: this.name(),
            code: this.code(),
            previewImage: this.previewImage(),
            width: width,
            height: height,
            isAllCompanies: this.isAllCompaniesSelected().value,
            images: this.imageSets(),
            infoStatus: this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update,
        };


        if (this.isAllCompaniesSelected && this.isAllCompaniesSelected().value) {
            model.accessibleCompanies = null;
        } else {
            var canManageCompany = [];
            this.accessibleCompanies().forEach((company) => {
                if (company.canManage) {
                    canManageCompany.push(company);
                }
            })
            model.accessibleCompanies = canManageCompany;

        }

        return model;
    }


    /**
     * 
     * 
     * @param {any} data
     * @returns
     * 
     * @memberOf ConfigurationVehicleIconManageScreen
     */
    generateImageSetsModel(data) {
        // debugger
        var imgSet = [];
        data.forEach((d) => {
            var file = {
                id: d.id,
                vehicleIconId: this.vehicleIconId,
                zipFile: d.file(),
                movementType: d.movementType,
                movementTypeDisplayName: d.displayName,
                jobStatus: null,
                jobStatusDisplayName: null,
                infoStatus: Enums.InfoStatus.Update ///d.infoStatus
            };

            imgSet.push(file);
        })

        return imgSet;

    }

    /**
     * 
     * @public
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id == "actSave") {
            ko.validation.validateObservable(this.imageSetItems);
            _.each(this.imageSetItems(), (item) => {
                // Problem is vm not notify view in this scenario.
                item.selectedFile.valueHasMutated();
            });
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
            this.isBusy(true);

            var im = this.generateImageSetsModel(this.imageSetItems());
            this.imageSets(im);

            var model = this.generateModel();

            switch (this.mode) {
                case "create":
                    this.webRequestVehicleIcon.createVehicleIcon(model, true).done((response) => {
                        this.isBusy(false);

                        this.publishMessage("bo-configuration-vehicle-iccon-change", response.id);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);

                        this.handleError(e);
                    });

                    break;
                case "update":
                    this.webRequestVehicleIcon.updateVehicleIcon(model, true).done((response) => {
                        this.isBusy(false);

                        this.publishMessage("bo-configuration-vehicle-iccon-change", response.id);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);

                        this.handleError(e);
                    });
                    break;
            }
        }
    }


    /**
     * 
     * 
     * @param {any} commands
     * 
     * @memberOf ConfigurationVehicleIconManageScreen
     */
    buildCommandBar(commands) {
        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            if (WebConfig.userSession.hasPermission(Constants.Permission.VehicleIconsManagement)) {
                commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
            }
        }
    }


    /**
     * 
     * 
     * @param {any} sender
     * 
     * @memberOf ConfigurationVehicleIconManageScreen
     */
    onCommandClick(sender) {
        if (sender.id === "cmdDelete") {
            this.showMessageBox(null, this.i18n("M123")(), BladeDialog.DIALOG_YESNO).done((button) => {
                switch (button) {
                    case BladeDialog.BUTTON_YES:
                        this.isBusy(true);
                        // debugger
                        this.webRequestVehicleIcon.deleteVehicleIcon(this.vehicleIconId).done(() => {
                            this.isBusy(false);
                            this.publishMessage("bo-configuration-vehicle-iccon-change", this.vehicleIconId);
                            this.close(true);
                        }).fail((e) => {
                            this.isBusy(false);
                            this.handleError(e);
                        });
                        break;
                }
            });
        }
    }



    /**
     * Navigate to Add accessible company
     */
    addAccessibleCompany() {
        var targetMode = (this.mode === "create") ? "vehicle-icon-create" : "vehicle-icon-update";
        var selectedCompanyIds = this.accessibleCompanies().map((obj) => {
            return obj.companyId;
        });
        // debugger
        this.navigate("bo-shared-accessible-company-select", {
            mode: targetMode,
            selectedCompanyIds: selectedCompanyIds
        });
    }

    onUnload() {}

}


/**
 * 
 * 
 * @class ImageSetItem
 */
class ImageSetItem {
    constructor(id, movementType, displayName, infoStatus, mode, onUploadFail) {
        // debugger
        this.id = id;
        this.fileId;
        this.movementType = movementType;
        this.displayName = displayName;
        this.infoStatus = infoStatus;
        this.file = ko.observable();
        this.fileName = ko.observable();
        this.fileUrl = ko.observable();
        this.selectedFile = ko.observable().extend({
            fileRequired: true,
            fileExtension: ['zip'],
            fileSize: 4
        });
        this.isDirty = false;//Default value = false mean value has not changed. 
        this.isValid = ko.pureComputed(()=> {
            return this.selectedFile.isValid();
        });

        this.onUploadZipSuccess = (data) => {
            if (data && data.length > 0) {
                this.isDirty = true;
                var newFile = data[0];
                newFile.infoStatus = mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update;
                newFile.id = this.fileId || 0;
                this.file(newFile);
                this.fileUrl(data[0].fileUrl);
            }
        };

        this._onUploadFail = onUploadFail;

        this.onUploadFail = (e) => {
            if(this._onUploadFail){
                this.isDirty = mode === Screen.SCREEN_MODE_CREATE ? false : true; 
                this._onUploadFail(e);
            }
        };

        this.onBeforeUpload = (isValidToSubmit) => {
                this.isDirty = mode === Screen.SCREEN_MODE_CREATE ? false : true;
                this.file(null);
                this.fileName(null);
            // }
        };

        this.onRemoveFile = () => {
            this.isDirty = mode === Screen.SCREEN_MODE_CREATE ? false : true;
            this.file(null);
        };

    }
}

export default {
    viewModel: ScreenBase.createFactory(ConfigurationVehicleIconManageScreen),
    template: templateMarkup
};
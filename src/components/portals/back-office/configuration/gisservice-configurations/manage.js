﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import Utility from "../../../../../app/frameworks/core/utility";
import {
    Constants,
    Enums,
    EntityAssociation
} from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestGISServiceConfigurations from "../../../../../app/frameworks/data/apitrackingcore/webRequestGISServiceConfigurations";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";


const width = 32;
const height = 32;

class ConfigurationGISServiceManage extends ScreenBase {
    constructor(params) {
        super(params);
        
        // console.log("params",params);
        // Properties
        this.mode = this.ensureNonObservable(params.mode);
        this.idService = ko.observable(null);
        this.nameService = ko.observable();
        this.urlService = ko.observable();
        this.tokeService = ko.observable();
        this.gisTypeOptions = ko.observableArray([]);
        this.typeService = ko.observable();
        this.gisClassOptions = ko.observableArray([]);
        this.classService = ko.observable();
        this.llandmarkService = ko.observable();
        this.refererService = ko.observable();
        // this.ladminpolyService = ko.observable();

        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("Configurations_GIS_Service_Create")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("Configurations_GIS_Service_Update")());
                this.idService(params.data.id);
                break;
        }

       
    }
    
    get webRequestGISServiceConfigurations() {
        return WebRequestGISServiceConfigurations.getInstance();
    }
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }
    /**
     * 
     * Hook on fileupload before upload
     * @param {any} e
     */



    /**
     * To remove upload icon.
     * 
     * 
     * @memberOf ConfigurationPoiIconManageScreen
     */
    onRemoveFile() {
        // this.icon(null);
    }

    /**
     * Do something when fileupload fail.
     * 
     * @param {any} e
     * 
     * @memberOf ConfigurationPoiIconManageScreen
     */
    onUploadFail(e) {
        // this.handleError(e);
    }


    /**
     * Do something when fileupload success.
     * 
     * @param {any} data
     * 
     * @memberOf ConfigurationPoiIconManageScreen
     */
    onUploadSuccess(data) {
        // if (data && data.length > 0) {
        //     var newIcon = data[0];

        //     newIcon.infoStatus = this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update;
        //     newIcon.id = this.iconId() || 0;
        //     this.icon(newIcon);
        // }
    }

    /**
     * 
     * @public
     * @param {any} isFirstLoad
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var dfd = $.Deferred();
       
            var dfdEnum =this.webRequestEnumResource.listEnumResource({
                types: [ Enums.ModelData.EnumResourceType.GISServiceType ]
            });
            var dfdClass =this.webRequestEnumResource.listEnumResource({
                types: [ Enums.ModelData.EnumResourceType.GISServiceClassType ]
            });

            // Screen.SCREEN_MODE_CREATE:
            //     this.bladeTitle(this.i18n("Configurations_GIS_Service_Create")());
            //     break;
            // case Screen.SCREEN_MODE_UPDATE:

            if(this.mode == Screen.SCREEN_MODE_UPDATE){
                // console.log('id',this.idService())
                var dfdGis  = this.webRequestGISServiceConfigurations.getGisService(this.idService());

            }else {
                var dfdGis = null;
            }


            $.when(dfdEnum,dfdClass ,dfdGis).done((responseEnum,responseClass,responseGetGIS) => {

                this.gisTypeOptions.replaceAll(responseEnum.items)
                this.gisClassOptions.replaceAll(responseClass.items)

                if(this.mode == Screen.SCREEN_MODE_UPDATE){
                    this.nameService(responseGetGIS.serviceName);
                    this.urlService(responseGetGIS.url);
                    this.tokeService(responseGetGIS.token);
                    this.typeService(ScreenHelper.findOptionByProperty(this.gisTypeOptions, "value", responseGetGIS.type ));
                    this.classService(ScreenHelper.findOptionByProperty(this.gisClassOptions, "value", responseGetGIS.classType ));
                    this.llandmarkService(responseGetGIS.layerIDLandmark);
                    this.refererService(responseGetGIS.referer);
                    // this.ladminpolyService(responseGetGIS.layerIDAddress);
                }
                

                this.isBusy(false);
                dfd.resolve();
            }).fail((e) => {
                this.isBusy(false);
                this.handleError(e);
            });

         return dfd;
    }

    setupExtend() {

        //Required
        this.nameService.extend({
            required: true
        });
        this.urlService.extend({
            required: true
        });
        this.typeService.extend({
            required: true
        });
        this.classService.extend({
            required: true
        });
        this.llandmarkService.extend({
            required: true
        });
        this.refererService.extend({
            required: true
        })
        // this.ladminpolyService.extend({
        //     required: true
        // });

        // this.baseMapItem.extend({
        //     serverValidate: {
        //         params: "BaseMapItem",
        //         message: this.i18n("M010")()
        //     }
        // });

        this.validationModel = ko.validatedObservable({
            nameService: this.nameService,
            urlService: this.urlService,
            typeService: this.typeService,
            classService: this.classService,
            llandmarkService: this.llandmarkService,
            referer: this.refererService
            // ladminpolyService: this.ladminpolyService

        });
    }

    /**
     * 
     * @public
     * @param {any} actions
     */
    buildActionBar(actions) {
        // if (WebConfig.userSession.hasPermission(Constants.Permission.POIIconsManagement_BackOffice)) {
            actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
            actions.push(this.createActionCancel());
        // }
       

    }

    /**
     * Generate user model from View Model
     * 
     * @returns user model which is ready for ajax request 
     */
    generateModel() {

        let filterModel = {
            "url": this.urlService(),
            "type": this.typeService().value,
            "classType": this.classService().value,
            "layerIDLandmark": this.llandmarkService(),
            "referer": this.refererService(),
            // "layerIDAddress": this.ladminpolyService(),
            "serviceName": this.nameService(),
            "token": this.tokeService()
        };
        return filterModel;
    }

    /**
     * 
     * @public
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);


        if (sender.id == "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            // this.isBusy(true);

            let filter = this.generateModel();

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                   
                    this.webRequestGISServiceConfigurations.createGisService(filter).done((response) => {
                        this.isBusy(false);

                        this.publishMessage("bo-configuration-gis-service");
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);

                        this.handleError(e);
                    });

                    break;

                case Screen.SCREEN_MODE_UPDATE:
                    filter.id = this.idService();
                    // console.log("ScreenMode>>", Screen.SCREEN_MODE_UPDATE)

                    // console.log("filter>>", filter)

                    this.webRequestGISServiceConfigurations.updateGisService(filter, true).done((response) => {
                        this.isBusy(false);
                        this.publishMessage("bo-configuration-gis-service");
                        this.close(true);
                    }).fail((e) => {
                        console.log("inFail>>")
                        this.handleError(e);
                        this.isBusy(false);


                    });

                    break;
            }
        }
    }

    buildCommandBar(commands) {
        if (this.mode === "update") {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }

    onCommandClick(sender) {
        if (sender.id === "cmdDelete") {

            this.showMessageBox(null, this.i18n("Mapservice_Delete_Dialog_Msg")(), BladeDialog.DIALOG_YESNO).done((button) => {
                switch (button) {
                    case BladeDialog.BUTTON_YES:
                        this.isBusy(true);
                        this.webRequestGISServiceConfigurations.deleteGisService(this.idService()).done(() => {
                            this.isBusy(false);
                            // this.publishMessage("bo-configuration-poi-iccon-change", this.id);
                            this.publishMessage("bo-configuration-gis-service");
                            this.close(true);
                        }).fail((e) => {
                            this.isBusy(false);
                            this.handleError(e);
                        });
                        break;
                }
            });
        }
    }

    onUnload() { }
}

export default {
    viewModel: ScreenBase.createFactory(ConfigurationGISServiceManage),
    template: templateMarkup
};

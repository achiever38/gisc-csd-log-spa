﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebRequestGISServiceConfigurations from "../../../../../app/frameworks/data/apitrackingcore/webRequestGISServiceConfigurations";

class ConfigurationMapService extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties

        this.bladeTitle(this.i18n("Configurations_GIS_Service_List")());
        this.bladeSize = BladeSize.Medium;


        this.filter = ko.observable({
            "companyId": null
        });
        this.selectedItems = ko.observable('');

        this.subscribeMessage("bo-configuration-gis-service", info => {
            this.getNostraResponseData();
        });

        this.selectRow = (row) => {
            this.navigate("bo-configuration-gis-service-configurations-manage", { data: row , mode: "update"});
        }

        this.gisServiceList = ko.observableArray([]);
        this.selectedRow = ko.observable();

        this.filterText = ko.observable();
        this.recentChangedRowIds = ko.observableArray([]);
        this.order = ko.observable([]);
        this.selectedGISService = ko.observable(null);
    }

    /**
     * Get WebRequest specific for Feature module in Tracking Web API access.
     * @readonly
     */
  
    get webRequestGISServiceConfigurations() {
        return WebRequestGISServiceConfigurations.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        this.getNostraResponseData();
    }
   

    getNostraResponseData(id){
        var dfd = $.Deferred();
        this.webRequestGISServiceConfigurations.gisServiceList().done((r) => {
            this.gisServiceList.replaceAll(r["items"]);
            if(id) {
                this.recentChangedRowIds.replaceAll(id);
            }
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;
    }
    

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() { }

    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
    }

    onCommandClick(sender) {
        if (sender.id == "cmdCreate") {
            this.navigate("bo-configuration-gis-service-configurations-manage", { mode: "create" });
        }
    }
}



export default {
    viewModel: ScreenBase.createFactory(ConfigurationMapService),
    template: templateMarkup
};

// this.webReqesMapService.getMapservice(1).done((response) => {
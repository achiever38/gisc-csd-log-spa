﻿import ko from "knockout";
import templateMarkup from "text!./language.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import {Enums} from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestLanguage from "../../../../../app/frameworks/data/apicore/webRequestLanguage";
import DefaultSoring from "../../../../../app/frameworks/constant/defaultSorting";

class TranslationLanguageScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Configurations_AvailableLanguages")());
        this.bladeSize = BladeSize.Medium;

        this.languages = ko.observableArray([]);
        this.filterText = ko.observable('');

        this._originalLanguages = ko.observableArray([]);
        this.recentChangedRowIds = ko.observableArray([]);
        this.order = ko.observable([[ 2, "asc" ]]);

        this.tempLanguageId = -1;
        this.subscribeMessage("bo-language-added", (language) => {
            if(language){ 
                var targetLanguage = ko.utils.arrayFirst(this.languages(), function (item)
                {
                    return item.name === language.name && item.code === language.code;
                });

                if(!targetLanguage){
                    language.id = this.tempLanguageId;
                    language.orderLanguageCode = 1 + language.code;
                    language.orderLanguageName = 1 + language.name;

                    this.languages.push(language);
                    this.languages.replaceAll(this.languages());

                    this.tempLanguageId--;
                    this.recentChangedRowIds.replaceAll([language.id]);
                }else{
                    this.recentChangedRowIds.replaceAll([targetLanguage.id]);
                }
            }
        });
    }
    /**
     * Get WebRequest specific for Language module in Web API access.
     * @readonly
     */
    get webRequestLanguage() {
        return WebRequestLanguage.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();
        var languageFilter = {
            sortingColumns: DefaultSoring.LanguageByCode
        };
        this.webRequestLanguage.listLanguage(languageFilter).done((r)=> {
            var languages = r["items"];
            if (languages && languages.length > 0) {
                //Set orderLanguage
                ko.utils.arrayForEach(languages, (item) => {
                    if(item.isSystem){
                        item.orderLanguageCode = 0 + item.code;
                        item.orderLanguageName = 0 + item.name;
                    }else{
                        item.orderLanguageCode = 1 + item.code;
                        item.orderLanguageName = 1 + item.name;
                    }
                });
            }
            this.languages(languages);

            this._originalLanguages = $.extend(true, [], languages);

            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }

    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        this.languages.extend({
            trackArrayChange: true
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdCreate", this.i18n("Common_Add")(), "svg-cmd-add"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
       super.onActionClick(sender);

        if (sender.id === "actSave") {
            this.isBusy(true);
            var infors = [];
            this.languages().forEach(function(language) {

                var originalLanguage = ko.utils.arrayFirst(this._originalLanguages, function (item)
                {
                    return item.id === language.id;
                });

                if(originalLanguage) {
                    if(originalLanguage.enable != language.enable){
                        infors.push({
                            id: language.id,
                            name: language.name,
                            code: language.code,
                            enable: language.enable,
                            infoStatus: Enums.InfoStatus.Update
                        });
                    }  
                } else {
                    infors.push({
                        name: language.name,
                        code: language.code,
                        enable: language.enable,
                        infoStatus: Enums.InfoStatus.Add
                    });
                }
            }, this);

            this.webRequestLanguage.enableAvailableLanguages(infors).done(() => {
                this.isBusy(false);

                this.publishMessage("bo-languages-enabled");
                this.close(true);
            }).fail((e)=> {
                this.isBusy(false);

                this.handleError(e);
            });
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if(sender.id == "cmdCreate") {
            this.navigate("bo-configuration-translation-language-add");
        }
        this.recentChangedRowIds.removeAll();
    }
}

export default {
    viewModel: ScreenBase.createFactory(TranslationLanguageScreen),
    template: templateMarkup
};
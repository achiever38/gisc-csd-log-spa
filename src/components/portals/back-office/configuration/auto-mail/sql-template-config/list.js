﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../../screenbase";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import {Constants} from "../../../../../../app/frameworks/constant/apiConstant";
import WebRequestDatabaseConnection from "../../../../../../app/frameworks/data/apitrackingcore/webRequestDatabaseConnection";
import WebRequestSqlTemplate from "../../../../../../app/frameworks/data/apicore/webRequestSqlTemplate";
/**
 * 
 * 
 * @class TrackingDatabasesListScreen
 * @extends {ScreenBase}
 */
class SqlTemplatesListScreen extends ScreenBase {
    /**
     * Creates an instance of TrackingDatabasesListScreen.
     * 
     * @param {any} params
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    constructor(params) {
        super(params);
        // Properties
        
        this.bladeTitle(this.i18n("Configurations_SqlTemplateConfig")());
        this.bladeSize = BladeSize.Medium;

        this.sqlTemplates = ko.observableArray([]);
        this.filterText = ko.observable('');
        this.selectedsqlTemp = ko.observableArray();
        this.selectedSqlTemp = ko.observable(null);
        this.recentChangedRowIds = ko.observableArray([]);
        this.order = ko.observable([
            [0, "asc"]
        ]);
        this.filterText = ko.observable();


        /**
         * 
         * 
         * @param {any} sqlTemp
         * @returns
         */
        this._selectingRowHandler = (sqlTemp) => {
       
                return this.navigate("bo-automail-sql-template-manage", {
                    mode: "update",
                    sqlTempId: sqlTemp.id
                });
            
            return false;
        };

        this.subscribeMessage("bo-automail-sql-template-changed", (sqlTempId) => {
            // Refresh entire datasource
            var filter = {
                templateNames:""
            };
            this.webRequestSqlTemplate.listSqlTemplateSummary(filter).done((response) => {
                var sqlTemplates = response["items"];
                this.sqlTemplates(sqlTemplates);
                this.recentChangedRowIds.replaceAll([sqlTempId]);
            });
        });
    }

    /**
     * 
     * 
     * @readonly
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    get webRequestSqlTemplate() {
        return WebRequestSqlTemplate.getInstance();
    }

    /**
     * 
     * 
     * @param {any} isFirstLoad
     * @returns
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var dfd = $.Deferred();
        var filter = {
            templateNames:""
        };
        this.webRequestSqlTemplate.listSqlTemplateSummary(filter).done((response) => {
            var sqlTemplates = response["items"];
            this.sqlTemplates(sqlTemplates);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;
    }

    /**
     * 
     * 
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    onChildScreenClosed() {
        this.selectedSqlTemp(null);
    }

    /**
     * 
     * 
     * @param {any} commands
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    buildCommandBar(commands) {

            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        
    }

    /**
     * 
     * 
     * @param {any} sender
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    onCommandClick(sender) {
        if (sender.id == "cmdCreate") {
            this.navigate("bo-automail-sql-template-manage", {
                mode: "create"
            });
            this.selectedSqlTemp(null);
        }
    }

    /**
     * 
     * 
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    onUnload() {
    }
}

export default {
viewModel: ScreenBase.createFactory(SqlTemplatesListScreen),
    template: templateMarkup
};
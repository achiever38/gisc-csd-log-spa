﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../../screenbase";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import {Constants} from "../../../../../../app/frameworks/constant/apiConstant";
import WebRequestMailConfiguration from "../../../../../../app/frameworks/data/apicore/WebRequestMailConfiguration";

/**
 * 
 * 
 * @class TrackingDatabasesListScreen
 * @extends {ScreenBase}
 */
class MailConfigListScreen extends ScreenBase {
    /**
     * Creates an instance of TrackingDatabasesListScreen.
     * 
     * @param {any} params
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    constructor(params) {
        super(params);
        // Properties
        
        this.bladeTitle(this.i18n("AutoMail_MailConfiguration")());
        this.bladeSize = BladeSize.XLarge;
        this.bladeIsMaximized = true;
        this.selectedItems = ko.observable();
        this.mailconfig = ko.observableArray([]);
        this.recentChangedRowIds = ko.observableArray([]);
        this.filterText = ko.observable('');
        this.order = ko.observable([
            [0, "asc"]
        ]);
        this.test2 = ko.observableArray([{name:"t"}]);


        this.viewHistory = (data)=>{
            return this.navigate("bo-automail-mail-config-view", {
                mode: "view" , 
                mailconfigId: data.id
            });
            return false;
        };
        //this.selectedTrackDB = ko.observable(null);
        //this.recentChangedRowIds = ko.observableArray([]);
        //this.order = ko.observable([
        //    [0, "asc"]
        //]);
        this.filterText = ko.observable();

        /**
         * 
         * 
         * @param {any} trackingDB
         * @returns
         */
        
        this.selectingRow = (mailconfig) => {
            return this.navigate("bo-automail-mail-config-manage", {
                mode: "update",
                mailconfigId: mailconfig.id,
                companyId:mailconfig.companyId ,
                buId:mailconfig.businessUnitId
            });
          return false;          
        };
       
        this.selectedItems.subscribe(function (newValue) {
        });
        
        this.viewMapClick = (ds) => {
            if (ds.Title === 'Title_Demo') {
                return this.navigate("bo-automail-mail-config-view", { mode: "view" });
            }
            return false;
        };
        this.subscribeMessage("bo-automail-mailconfig-template-changed", (mailconfigId) => {
            // Refresh entire datasource
            this.isBusy(true);
            var filter = {};
            this.WebRequestMailConfiguration.summarylistMailConfig(filter).done((response) => {
                var mailconfig = response["items"];

                mailconfig.forEach(function(v){
                    v.view = "../images/ic-search.svg";
                });

                this.mailconfig.replaceAll(mailconfig);
                this.recentChangedRowIds.replaceAll([mailconfigId]);
            }).fail((e) => {
                this.handleError(e);
            })
            .always(() => {
                this.isBusy(false);
            });
        });
    }

    /**
     * 
     * 
     * @readonly
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    get WebRequestMailConfiguration() {
        return WebRequestMailConfiguration.getInstance();
    }

   
    /**
     * 
     * 
     * @param {any} isFirstLoad
     * @returns
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        var filter = { title: "" };
        var dfd = $.Deferred();

        this.WebRequestMailConfiguration.summarylistMailConfig(filter).done((response) => {
            var mailconfig = response["items"];
            
            mailconfig.forEach(function(v){
                v.view = "../images/ic-search.svg";
                v.businessUnitName = (v.businessUnitName)?v.businessUnitName:"-";
            });
            this.mailconfig(mailconfig);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;
    }

    /**
     * 
     * 
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    onChildScreenClosed() {
        //this.selectedTrackDB(null);
    }

    /**
     * 
     * 
     * @param {any} commands
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.TrackingDatabaseManagement)) {
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
    }

    /**
     * 
     * 
     * @param {any} sender
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    onCommandClick(sender) {
        if (sender.id == "cmdCreate") {
            this.navigate("bo-automail-mail-config-manage", {
                mode: "create"
            });
            //this.selectedTrackDB(null);
        }
    }

    /**
     * 
     * 
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    onUnload() {
    }
}

export default {
viewModel: ScreenBase.createFactory(MailConfigListScreen),
    template: templateMarkup
};
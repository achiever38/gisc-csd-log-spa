﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import ScreenBase from "../../../../screenbase";
import ScreenHelper from "../../../../screenhelper";
import * as Screen from "../../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import { Constants, Enums, EntityAssociation } from "../../../../../../app/frameworks/constant/apiConstant";
import WebRequestMailConfiguration from "../../../../../../app/frameworks/data/apicore/WebRequestMailConfiguration";


/**
 * 
 * 
 * @class TrackingDatabasesManageScreen
 * @extends {ScreenBase}
 */
class ViewVersionHistory extends ScreenBase {
    /**
     * Creates an instance of TrackingDatabasesManageScreen.
     * 
     * @param {any} params
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    constructor(params) {
        super(params);
        // Properties
        this.mode = this.ensureNonObservable(params.mode);
        this.id = this.ensureNonObservable(params.mailconfigId);
        this.bladeTitle(this.i18n("AutoMail_ViewHistory")());
        this.bladeSize = BladeSize.XLarge;
        this.bladeIsMaximized = true;
        this.order = ko.observable([]);

        this.ds = ko.observableArray([]);
 
    }

    /**
     * 
     * 
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    setupExtend() {

    }

    get WebRequestMailConfiguration() {
        return WebRequestMailConfiguration.getInstance();
    }
    /**
     * 
     * 
     * @param {any} isFirstLoad
     * @returns
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var dfd = $.Deferred();
        var filter = {id:this.id};
        this.WebRequestMailConfiguration.getVersionSummary(filter).done((getmailconfigV) => { 
            var getdata = getmailconfigV["items"];
            this.ds(getdata);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * 
     * 
     * @param {any} sender
     * @returns
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    onActionClick(sender) {

    }

    /**
     * 
     * 
     * @param {any} commands
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdView", this.i18n("AutoMail_View")(), "svg-cmd-search"));
    }

    /**
     * 
     * 
     * @param {any} sender
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    onCommandClick(sender) {
        if (sender.id == "cmdView") {
            this.navigate("bo-configuration-sending-log-manage", {
                mode: "export",
                id :this.id
            });
            //this.selectedTrackDB(null);
        }
    }

    /**
     * 
     * 
     * @returns
     * 
     * @memberOf TrackingDatabasesManageScreen
     */


    onUnload() { }
}

export default {
    viewModel: ScreenBase.createFactory(ViewVersionHistory),
    template: templateMarkup
};
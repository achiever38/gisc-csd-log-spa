﻿import ko from "knockout";
import templateMarkup from "text!./menu.html";
import ScreenBase from "../../screenbase";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import { Constants } from "../../../../app/frameworks/constant/apiConstant";


/**
 * Display configuration menu based on user permission.
 * @class ConfigurationMenuScreen
 * @extends {ScreenBase}
 */
class ConfigurationMenuScreen extends ScreenBase {

    /**
     * Creates an instance of ConfigurationMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Configurations_ConfigurationsTitlePage")());
        this.bladeMargin = BladeMargin.Narrow;
        this.bladeSize = BladeSize.XSmall;

        this.selectedIndex = ko.observable();
        this.items = ko.observableArray([]);

        this._selectingRowHandler = (item) => {
            if (item) {
                return this.navigate(item.page);
            }
            return false;
        };
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // TODO: Verify access right to specific items.
        if (!isFirstLoad) {
            return;
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.Translation))
        {
            this.items.push({
                text: this.i18n('Configurations_Translation')(),
                page: 'bo-configuration-translation'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.POIIconsManagement_BackOffice))
        {
            this.items.push({
                text: this.i18n('Configurations_POIIcons')(),
                page: 'bo-configuration-poi-icon'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.VehicleIconsManagement))
        {
            this.items.push({
                text: this.i18n('Configurations_VehicleIcons')(),
                page: 'bo-configuration-vehicle-icon'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.UpdateSettings))
        {
            this.items.push({
                text: this.i18n('Configurations_SystemSettings')(),
                page: 'bo-configuration-system-setting'
            });
        }
        
        if(WebConfig.userSession.hasPermission(Constants.Permission.TrackingDatabaseManagement))
        {
            this.items.push({
                text: this.i18n('Configurations_TrackingDatabases')(),
                page: 'bo-configuration-tracking-databases'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.AutoMail))
        {
            this.items.push({
                text: this.i18n('Configurations_AutoMail')(),
                page: 'bo-configuration-auto-mail'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.MapServiceManagement))
        {
            this.items.push({
                text: this.i18n('Configurations_Mapservice')(),
                page: 'bo-configuration-map-service'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.UpdateNostraConfiguration))
        {
            this.items.push({
                text: this.i18n('Configurations_NostraConfigurations')(),
                page: 'bo-configuration-nostra-configurations'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.GISServiceConfiguration))
        {
            this.items.push({
                text: this.i18n('Configurations_Gis_Service')(),
                page: 'bo-configuration-gis-service-configurations'
            });
        }
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedIndex(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}

    // /**
    //  * Navigate to inner screens.
    //  * @param {any} extras
    //  */
    // goto (extras) {
    //     this.navigate(extras.id);
    // }
}

export default {
    viewModel: ScreenBase.createFactory(ConfigurationMenuScreen),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import {
    Constants,
    Enums,
    EntityAssociation
} from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestNostraConfigurations from "../../../../../app/frameworks/data/apitrackingcore/webRequestNostraConfigurations";


class NostraConfigurationsManage extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Configurations_NostraConfigurations_Update")());
        this.bladeSize = BladeSize.Medium;

        this.nostraId = ko.observable(params.data.id)
        this.nostraName = ko.observable();
        this.nostraValue = ko.observable();
        

        
        
        
    }

    get webRequestNostraConfigurations() {
        return WebRequestNostraConfigurations.getInstance();
    }

    /**
     * 
     * Hook on fileupload before upload
     * @param {any} e
     */
    onBeforeUpload(isValidToSubmit) {
        // if (!isValidToSubmit) {
        //     this.icon(null);
        //     this.iconName(null);
        // }
    }


    /**
     * To remove upload icon.
     * 
     * 
     * @memberOf ConfigurationPoiIconManageScreen
     */
    onRemoveFile() {
        // this.icon(null);
    }

    /**
     * Do something when fileupload fail.
     * 
     * @param {any} e
     * 
     * @memberOf ConfigurationPoiIconManageScreen
     */
    onUploadFail(e) {
        // this.handleError(e);
    }


    /**
     * Do something when fileupload success.
     * 
     * @param {any} data
     * 
     * @memberOf ConfigurationPoiIconManageScreen
     */
    onUploadSuccess(data) {
        // if (data && data.length > 0) {
        //     var newIcon = data[0];

        //     newIcon.infoStatus = this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update;
        //     newIcon.id = this.iconId() || 0;
        //     this.icon(newIcon);
        // }
    }

    /**
     * 
     * @public
     * @param {any} isFirstLoad
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var dfd = $.Deferred();
        
        this.webRequestNostraConfigurations.getNostra(this.nostraId()).done((r) => {
            this.nostraName(r.name)
            this.nostraValue(r.value)
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        
      
    }

    setupExtend() {

      // //Track change
    this.nostraValue.extend({
        trackChange: true
    });

    this.nostraValue.extend({
        required: true
    });
    

    this.validationModel = ko.validatedObservable({
        nostraValue: this.nostraValue
    });
    }

    /**
     * 
     * @public
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }
    /**
     * Generate user model from View Model
     * 
     * @returns user model which is ready for ajax request 
     */
    generateModel() {
        // var model = {
        //     name: this.name(),
        //     image: this.icon(),
        //     offsetX: this.offsetX(),
        //     offsetY: this.offsetY(),
        //     width: width,
        //     height: height,
        //     companyId: this.companyId,
        //     infoStatus: this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update,
        //     id: this.id,
        // };

        // return model;
    }

    /**
     * 
     * @public
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);


        if (sender.id == "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);

            var model ={
                id : this.nostraId(),
                name : this.nostraName(),
                value : this.nostraValue(),
                infoStatus : Enums.InfoStatus.Update
            }
            
                    this.webRequestNostraConfigurations.updateNostra(model,true).done((response) => {
                        this.isBusy(false);

                        this.publishMessage("bo-configuration-nostra-configurations-response", response.id);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);

                        this.handleError(e);
                    });

               
            
        }
    }

    buildCommandBar(commands) {
        // if (this.mode === "update") {
        //     if (WebConfig.userSession.hasPermission(Constants.Permission.POIIconsManagement_BackOffice)) {
        //         commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        //     }
        // }
    }

    onCommandClick(sender) {
        // if (sender.id === "cmdDelete") {
        //     this.showMessageBox(null, this.i18n("M121")(), BladeDialog.DIALOG_YESNO).done((button) => {
        //         switch (button) {
        //             case BladeDialog.BUTTON_YES:
        //                 this.isBusy(true);
        //                 this.webRequestPoiIcon.deletePoiIcon(this.id).done(() => {
        //                     this.isBusy(false);
        //                     this.publishMessage("bo-configuration-poi-iccon-change", this.id);
        //                     this.close(true);
        //                 }).fail((e) => {
        //                     this.isBusy(false);
        //                     var errorObj = e.responseJSON;
        //                     if (errorObj) {
        //                         this.displaySubmitError(errorObj);
        //                     }
        //                 });
        //                 break;
        //         }
        //     });
        // }
    }

    onUnload() { }
}

export default {
    viewModel: ScreenBase.createFactory(NostraConfigurationsManage),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import Utility from "../../../../../app/frameworks/core/utility";
import {
    Constants,
    Enums,
    EntityAssociation
} from "../../../../../app/frameworks/constant/apiConstant";
import FileUploadModel from "../../../../../components/controls/gisc-ui/fileupload/fileuploadModel";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestPoiIcon from "../../../../../app/frameworks/data/apitrackingcore/webRequestPoiIcon";
import WebReqesMapService from "../../../../../app/frameworks/data/apitrackingcore/webRequestMapService";

const width = 32;
const height = 32;

class ConfigurationMapServiceManage extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties
        this.mode = this.ensureNonObservable(params.mode);
        this.checkMapType = ko.observable(true);
        this.companyId = WebConfig.userSession.currentCompanyId;
        let currentDateTime = new Date();
        this.MAPTYPE_BASEMAP = 1;
        this.MAPTYPE_LAYERS = 2;
        this.id = params.id;

        //MapType
        this.mapTypeOptions = ko.observableArray([]);
        this.mapType = ko.observable('');

        this.baseMapItem = ko.observable(null);
        this.layerName = ko.observable(null);
        this.serViceId = ko.observable('');

        this.urlLocal = ko.observable('');
        this.urlEnglish = ko.observable('');

        this.tokenLocal = ko.observable('');
        this.tokenEnglish = ko.observable('');

        this.sortIndex = ko.observable();
        this.mapServicesTypeOptions = ko.observableArray([]);
        this.mapServicesType = ko.observable();

        this.dependMap = ko.observable('');

        //Min-Max Dropdown
        this.minLevelOptions = ko.observableArray([]);
        this.minLevel = ko.observable('');

        this.maxLevelOptions = ko.observableArray([]);
        this.maxLevel = ko.observable('');

        //Min Long-La titude
        this.minLongitude = ko.observable();
        this.minLatitude = ko.observable();

        //Max Long-La titude
        this.maxLongitude = ko.observable();
        this.maxLatitude = ko.observable();

        // Visible
        this.mapServiceVisibleOptions = ko.observableArray([]);
        this.mapServiceVisible = ko.observable('');
        // Use All Component 
        this.useAllComponentOptions = ko.observableArray([]);
        this.useAllComponent = ko.observable('');

        this.createDate = Utility.addTime(currentDateTime);
        this.isMinMaxLevelMapDropdownEnable = ko.observable();
        
        this.isBaseMapItemEnable = ko.computed((res) => {
            return this.mapType().value == this.MAPTYPE_BASEMAP;
        });
        this.isLayerNameEnable = ko.computed(() => {
            return this.mapType().value == this.MAPTYPE_LAYERS;
        });

        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("Configurations_Mapservice_Create")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("Configurations_Mapservice_Update")());
                break;
        }

        this.minLevel.subscribe((res) => {
            if (res == undefined) {

            }
        });
        this.mapServicesType.subscribe((res) => {
            let CheckTrueFalse = res.value == 'Tile' || res.value == 'Dynamic' ? true : false;
            this.isMinMaxLevelMapDropdownEnable(CheckTrueFalse);
            
        });

    }

    get webReqesMapService() {
        return WebReqesMapService.getInstance();
    }

    /**
     * 
     * Hook on fileupload before upload
     * @param {any} e
     */



    /**
     * To remove upload icon.
     * 
     * 
     * @memberOf ConfigurationPoiIconManageScreen
     */
    onRemoveFile() {
        // this.icon(null);
    }

    /**
     * Do something when fileupload fail.
     * 
     * @param {any} e
     * 
     * @memberOf ConfigurationPoiIconManageScreen
     */
    onUploadFail(e) {
        // this.handleError(e);
    }


    /**
     * Do something when fileupload success.
     * 
     * @param {any} data
     * 
     * @memberOf ConfigurationPoiIconManageScreen
     */
    onUploadSuccess(data) {
        // if (data && data.length > 0) {
        //     var newIcon = data[0];

        //     newIcon.infoStatus = this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update;
        //     newIcon.id = this.iconId() || 0;
        //     this.icon(newIcon);
        // }
    }

    /**
     * 
     * @public
     * @param {any} isFirstLoad
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var dfd = $.Deferred();

        //mapTypeOptions
        var mapTypeOptions = [{ name: "Basemap", value: this.MAPTYPE_BASEMAP },
        { name: "Layers", value: this.MAPTYPE_LAYERS }]

        //mapServicesTypeOptions
        var mapServicesTypeOptions = [{ name: "Tile", value: "Tile" },
        { name: "Dynamic", value: "Dynamic" },
        { name: "OSM", value: "OSM" },
        { name: "Google", value: "GOOGLE" },
        { name: "GoogleImg", value: "GOOGLEIMG" },];

        //min-max dropdown option
        var levelNumber = [];
        for (let level = 0; level <= 20; level++) {
            levelNumber[level] = { value: level };
        }

        //VIsible Option
        var jsonTrueFalse = [{ name: "True", value: true }, { name: "False", value: false }];

        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:

                // console.log("mapServicesTypeOptions>>", mapServicesTypeOptions)
                this.mapTypeOptions(mapTypeOptions);
                this.mapServicesTypeOptions(mapServicesTypeOptions);
                this.minLevelOptions(levelNumber);
                this.maxLevelOptions(levelNumber);
                this.mapServiceVisibleOptions(jsonTrueFalse);
                this.useAllComponentOptions(jsonTrueFalse);

                dfd.resolve();
                break;

            case Screen.SCREEN_MODE_UPDATE:
                //Get Service here.
                
                this.mapTypeOptions(mapTypeOptions);
                this.mapServicesTypeOptions(mapServicesTypeOptions);
                this.minLevelOptions(levelNumber);
                this.maxLevelOptions(levelNumber);
                this.mapServiceVisibleOptions(jsonTrueFalse);
                this.useAllComponentOptions(jsonTrueFalse);

                // let jobAutoFinish = !_.isNil(resDefValue.jobAutoFinish) ? ScreenHelper.findOptionByProperty(this.jobAutoFinishOptions, "value", resDefValue.jobAutoFinish) : null;//ScreenHelper.findOptionByProperty(this.jobAutoFinishOptions, "isDefault", true);
                // this.jobAutoFinish(jobAutoFinish);

                this.webReqesMapService.getMapservice(this.id).done((response) => {

                    let maptype = !_.isNil(response.baseMapItem) ? ScreenHelper.findOptionByProperty(this.mapTypeOptions, "value", this.MAPTYPE_BASEMAP) : ScreenHelper.findOptionByProperty(this.mapTypeOptions, "value", this.MAPTYPE_LAYERS);//ScreenHelper.findOptionByProperty(this.jobAutoFinishOptions, "isDefault", true);
                    this.mapType(maptype);

                    if (response.baseMapItem != null) {
                        let baseMapItemNum = response.baseMapItem.split("-"); //ตัดเอาเฉพาะตัวเลขข้างหลัง
                        this.baseMapItem(parseInt(baseMapItemNum[2]));
                    } else {
                        let layerNameValue = response.layerName.split("-"); //ตัดเอาเฉพาะชุดคำข้างหลัง
                        this.layerName(layerNameValue[2]);
                    }
                    // !_.isNil(response.baseMapItem) ? this.baseMapItem(response.baseMapItem) : this.layerName(response.layerName) //เช็คถ้า baseMapItem มีค่า ให้Set ฺbasemapitem ถ้าไม่ให้ Set Layername
                    let maxLevel = !_.isNil(response.maxLevel) ? ScreenHelper.findOptionByProperty(this.maxLevelOptions, "value", response.maxLevel) : null;
                    this.maxLevel(maxLevel)
                    let minLevel = !_.isNil(response.minLevel) ? ScreenHelper.findOptionByProperty(this.minLevelOptions, "value", response.minLevel) : null;
                    this.minLevel(minLevel);
                    let mapServicesType = !_.isNil(response.mapServiceType) ? ScreenHelper.findOptionByProperty(this.mapServicesTypeOptions, "value", response.mapServiceType) : null;
                    this.mapServicesType(mapServicesType);
                    this.serViceId(response.serviceID)
                    this.urlLocal(response.urlLocal)
                    this.urlEnglish(response.urlEnglish)
                    this.tokenLocal(response.tokenLocal)
                    this.tokenEnglish(response.tokenEnglish)
                    this.sortIndex(response.sortIndex)
                    this.dependMap(response.dependMap)
                    this.minLongitude(response.extentMinY)
                    this.minLatitude(response.extentMinX)
                    this.maxLongitude(response.extentMaxY)
                    this.maxLatitude(response.extentMaxX);

                    let isVisible = !_.isNil(response.isVisible) ? ScreenHelper.findOptionByProperty(this.mapServiceVisibleOptions, "value", response.isVisible) : null;
                    this.mapServiceVisible(isVisible);
                    let isAllCompanies = !_.isNil(response.isAllCompanies) ? ScreenHelper.findOptionByProperty(this.useAllComponentOptions, "value", response.isAllCompanies) : null;
                    this.useAllComponent(isAllCompanies);

                   
                    dfd.resolve();

                }).fail((e) => {
                    dfd.reject(e);
                });
                break;
            // dfd.resolve();
            // break;
        }
        return dfd;
    }

    setupExtend() {

        // //trackChange
        // this.serViceId.extend({
        //     trackChange: true
        // });
        // this.urlLocal.extend({
        //     trackChange: true
        // });
        // this.urlEnglish.extend({
        //     trackChange:true
        // });



        //Required
        this.serViceId.extend({
            required: true
        });
        this.urlLocal.extend({
            required: true
        });
        this.urlEnglish.extend({
            required: true
        });
        this.sortIndex.extend({
            required: true
        });


        this.baseMapItem.extend({
            required: {
                onlyIf: () => {
                   
                    return this.mapType().value == this.MAPTYPE_BASEMAP ; //BasemapItem จะ require ก็ต่อเมื่อ
                }
            }
        });
        this.layerName.extend({
            required: {
                onlyIf: () => {
                    
                    return this.mapType().value == this.MAPTYPE_LAYERS; //layerName จะ require ก็ต่อเมื่อ
                }
            }
        });

        //Validation
        this.baseMapItem.extend({
            validation: {
                validator: () => {
                    var isValidate = true;
                    if (!_.isNil(this.baseMapItem()) && this.mapType().value ==  this.MAPTYPE_BASEMAP ) {
                        isValidate = this.baseMapItem() > 3;
                    }
                    return isValidate;
                },
                message: this.i18n("Mapservice_Valiadate_Text_Basemap_Item")() // N must be over 3
            }
        })

        this.minLongitude.extend({
            validation: {
                validator: () => {
                    var isValidate = true;
                    if (!_.isNil(this.minLongitude()) && !_.isNil(this.maxLongitude())) {
                        isValidate = this.minLongitude() < this.maxLongitude();
                    }
                    return isValidate;
                },
                message: this.i18n("Mapservice_Valiadate_Text_Min_Longitude")() //min longitude must be less than max longitude.
            }
        });
        this.minLatitude.extend({
            validation: {
                validator: () => {
                    var isValidate = true;
                    if (!_.isNil(this.minLatitude()) && !_.isNil(this.maxLatitude())) {
                        isValidate = this.minLatitude() < this.maxLatitude();
                    }
                    return isValidate;
                },
                message: this.i18n("Mapservice_Valiadate_Text_Min_Latitude")() //min latitude must be less than max latitude.
            }
        });

        //min-max dropdown valiadate
        this.minLevel.extend({
            validation: {
                validator: () => {
                    var isValidate = true;
                    if (!_.isNil(this.minLevel()) && !_.isNil(this.maxLevel())) {
                        isValidate = this.minLevel().value <= this.maxLevel().value;
                    }
                    return isValidate;
                },
                message: this.i18n("Mapservice_Valiadate_Text_Min_Level_Map")()  //min level must be less than max level.
            }
        });

        
        //Servervaliadate
        this.baseMapItem.extend({
            serverValidate: {
                params: "BaseMapItem",
                message: this.i18n("M010")()
            }
        });
        this.serViceId.extend({
            serverValidate: {
                params: "ServiceID",
                message: this.i18n("M010")()
            }
        });
        this.layerName.extend({
            serverValidate: {
                params: "LayerName",
                message: this.i18n("M010")()
            }
        });

        this.validationModel = ko.validatedObservable({
            baseMapItem: this.baseMapItem,
            layerName: this.layerName,
            serViceId: this.serViceId,
            urlLocal: this.urlLocal,
            urlEnglish: this.urlEnglish,
            sortIndex: this.sortIndex

        });
    }

    /**
     * 
     * @public
     * @param {any} actions
     */
    buildActionBar(actions) {
        // if (WebConfig.userSession.hasPermission(Constants.Permission.POIIconsManagement_BackOffice)) {
        //     actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        //     actions.push(this.createActionCancel());
        // }
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());

    }

    /**
     * Generate user model from View Model
     * 
     * @returns user model which is ready for ajax request 
     */
    generateModel() {

        let baseMapItem;
        let layerName;
        if(this.isBaseMapItemEnable()){
            baseMapItem = `bm-item-${this.baseMapItem()}`;
            layerName = null;
        }else{
            baseMapItem = null;
            layerName = `ly-name-${this.layerName()}`;
        }

        let filterModel = {
            "baseMapItem": baseMapItem,
            "layerName": layerName,
            "serviceID": this.serViceId(),
            "urlLocal": this.urlLocal(),
            "urlEnglish": this.urlEnglish(),
            "tokenLocal": this.tokenLocal(),
            "tokenEnglish": this.tokenEnglish(),
            "sortIndex": this.sortIndex(),
            "mapServiceType": this.mapServicesType().value,

            "dependMap": this.dependMap(),
            "minLevel": !this.isMinMaxLevelMapDropdownEnable() ? 0 : this.minLevel().value,
            "maxLevel": !this.isMinMaxLevelMapDropdownEnable() ? 0 : this.maxLevel().value,
            "extentMinX": this.minLatitude() == undefined ? 0 : this.minLatitude(),
            "extentMinY": this.minLongitude() == undefined ? 0 : this.minLongitude(),
            "extentMaxX": this.maxLatitude() == undefined ? 0 : this.maxLatitude(),
            "extentMaxY": this.maxLongitude() == undefined ? 0 : this.maxLongitude(),
            "isVisible": this.mapServiceVisible().value,
            "isAllCompanies": this.useAllComponent().value,
            "createDate": this.createDate
        };
        return filterModel;
    }

    /**
     * 
     * @public
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);


        if (sender.id == "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            // this.isBusy(true);

            let filter = this.generateModel();

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    // console.log("WebConfig>>", WebConfig.userSession)
                    this.webReqesMapService.mapServiceCreate(filter).done((response) => {
                        this.isBusy(false);

                        this.publishMessage("bo-configuration-map-service");
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);

                        this.handleError(e);
                    });

                    break;

                case Screen.SCREEN_MODE_UPDATE:
                    filter.id = this.id;
                    // console.log("ScreenMode>>", Screen.SCREEN_MODE_UPDATE)
                    
                    // console.log("filter>>", filter)
                    
                    this.webReqesMapService.mapServiceUpdate(filter, true).done((response) => {
                        this.isBusy(false);
                        this.publishMessage("bo-configuration-map-service");
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                        this.isBusy(false);
                    });

                    break;
            }
        }
    }

    buildCommandBar(commands) {
        if (this.mode === "update") {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }

    onCommandClick(sender) {
        if (sender.id === "cmdDelete") {

            this.showMessageBox(null, this.i18n("Mapservice_Delete_Dialog_Msg")(), BladeDialog.DIALOG_YESNO).done((button) => {
                switch (button) {
                    case BladeDialog.BUTTON_YES:
                        this.isBusy(true);
                        this.webReqesMapService.deleteMapservice(this.id).done(() => {
                            this.isBusy(false);
                            // this.publishMessage("bo-configuration-poi-iccon-change", this.id);
                            this.publishMessage("bo-configuration-map-service");
                            this.close(true);
                        }).fail((e) => {
                            console.log("fail Delete>>")
                            this.isBusy(false);
                            this.handleError(e);
                            // var errorObj = e.responseJSON;
                            // if (errorObj) {
                            //     this.displaySubmitError(errorObj);
                            // }
                        });
                        break;
                }
            });
        }
    }

    onUnload() { }
}

export default {
    viewModel: ScreenBase.createFactory(ConfigurationMapServiceManage),
    template: templateMarkup
};

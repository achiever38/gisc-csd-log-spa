﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
// import WebRequestPoiIcon from "../../../../../app/frameworks/data/apitrackingcore/webRequestPoiIcon";
import { Constants, Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import WebReqesMapService from "../../../../../app/frameworks/data/apitrackingcore/webRequestMapService";
class ConfigurationMapService extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties

        this.bladeTitle(this.i18n("Configurations_Mapservice")());
        this.bladeSize = BladeSize.Medium;

        
        this.filter = ko.observable({
          
        });
        this.selectedItems = ko.observable('');
       
        this._selectingRowHandler = (res) =>{
            this.navigate("bo-configuration-map-service-manage", { mode: "update",id: res.id });
        };

        this.subscribeMessage("bo-configuration-map-service", info => {
            this.refreshShipmentList();            
        });

    }

    /**
     * Get WebRequest specific for Feature module in Tracking Web API access.
     * @readonly
     */
    // get webRequestPoiIcon() {
    //     return WebRequestPoiIcon.getInstance();
    // }
    get webReqesMapService() {
        return WebReqesMapService.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
    }
    refreshShipmentList() {
        
        this.dispatchEvent("dgMapServiceList", "refresh");
    }


    onDatasourceRequestRead(gridOption) {
        
        var dfd = $.Deferred();

        // var filter = Object.assign({}, this.filter(), gridOption);
        var filter = Object.assign({} , gridOption);
       
        this.isBusy(true);
        this.webReqesMapService
            .mapServiceSummaryList()
            .done(response => {
                dfd.resolve({
                    items: response.items,
                    totalRecords: 1, //hardcode
                    currentPage: 1//hardcode
                });
            })
            .fail(e => {
                this.handleError(e);
            })
            .always(() => {
                this.isBusy(false);
                // this.onAutoRefresh(true);
            });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() { }

    buildCommandBar(commands) {
        // if (WebConfig.userSession.hasPermission(Constants.Permission.POIIconsManagement_BackOffice)) {
            // commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        // }
        commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
    }

    onCommandClick(sender) {
        if (sender.id == "cmdCreate") {
            // this.navigate("bo-configuration-poi-icon-manage", { mode: "create" });
            // this.selectedIcon(null);
            this.navigate("bo-configuration-map-service-manage", { mode: "create" });
        }
    }
}



export default {
    viewModel: ScreenBase.createFactory(ConfigurationMapService),
    template: templateMarkup
};

// this.webReqesMapService.getMapservice(1).done((response) => {
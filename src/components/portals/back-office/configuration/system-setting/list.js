﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebRequestSystemConfiguration from "../../../../../app/frameworks/data/apicore/webRequestSystemConfiguration";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import {Constants} from "../../../../../app/frameworks/constant/apiConstant";
import DefaultSoring from "../../../../../app/frameworks/constant/defaultSorting";

/**
 * System Setting > List 
 * 
 * @class SystemSettingListScreen
 * @extends {ScreenBase}
 */
class SystemSettingListScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Configurations_SystemSettings")());
        this.bladeSize = BladeSize.Medium;
        
        this.systemSettings = ko.observableArray([]);
        this.filterText = ko.observable('');
        this.selectedSystemSetting = ko.observable(null);
        this.recentChangedRowIds = ko.observableArray([]);
        this.order = ko.observable([[ 0, "asc" ]]);

        this.filter = {
            types: [Constants.SystemConfigurationType.General],
            sortingColumns: DefaultSoring.SystemSetting
        };

        // Determine if selecting action is legal, return false to cancel selecting event.
        this._selectingRowHandler = (systemSetting) => {
            if(systemSetting && WebConfig.userSession.hasPermission(Constants.Permission.UpdateSettings)) {
                return this.navigate("bo-configuration-system-setting-manage", { systemSettingId: systemSetting.id });// mode: "update"
            }
            return false;
        };

         // Observe change about system setting in this Journey
        this.subscribeMessage("bo-configuration-system-setting-changed", (systemSettingId) => {
            this.isBusy(true);
            this.webRequestSystemConfiguration.listSystemConfiguration(this.filter).done((r)=> {
                this.systemSettings.replaceAll(r.items);
                this.recentChangedRowIds.replaceAll([systemSettingId]);
                this.isBusy(false);
            }).fail((e)=> {
                this.handleError(e);
                this.isBusy(false);
            });
        });
    }

    /**
     * Get WebRequest specific for System Configuration module in Web API access.
     * @readonly
     */
    get webRequestSystemConfiguration() {
        return WebRequestSystemConfiguration.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) { 
        if(!isFirstLoad) return;

        var dfd = $.Deferred();
        this.webRequestSystemConfiguration.listSystemConfiguration(this.filter).done((r)=> {
           this.systemSettings(r["items"]);
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedSystemSetting(null);
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(SystemSettingListScreen),
    template: templateMarkup
};
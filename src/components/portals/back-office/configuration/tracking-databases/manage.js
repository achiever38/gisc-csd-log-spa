import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestDatabaseConnection from "../../../../../app/frameworks/data/apitrackingcore/webRequestDatabaseConnection";
import WebRequestDatabaseServer from "../../../../../app/frameworks/data/apitrackingcore/webRequestDatabaseServer";
import {Constants,Enums,EntityAssociation} from "../../../../../app/frameworks/constant/apiConstant";

/**
 * 
 * 
 * @class TrackingDatabasesManageScreen
 * @extends {ScreenBase}
 */
class TrackingDatabasesManageScreen extends ScreenBase {
    /**
     * Creates an instance of TrackingDatabasesManageScreen.
     * 
     * @param {any} params
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    constructor(params) {
        super(params);
        // Properties

        this.mode = this.ensureNonObservable(params.mode);
        this.bladeSize = BladeSize.Medium;

        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("Configurations_AddTrackingDatabaseTitle")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("Configurations_ViewTrackingDatabaseTitle")());
                break;
        }

        this.trackingDBId = this.ensureNonObservable(params.trackingDBId, -1);
        this.trackingDBName = ko.observable("");
        this.archivedDBName = ko.observable("");
        this.serverOptions = ko.observableArray([]);
        this.serverSelected = ko.observable();
        this.companies = ko.observableArray([]);

        this.disabled = this.mode === Screen.SCREEN_MODE_UPDATE ? false : true;
        this.visibleCompanies = this.mode === Screen.SCREEN_MODE_UPDATE ? true : false;

    }

    /**
     * 
     * 
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    setupExtend(){

        //Track Changed
        this.trackingDBName.extend({
            trackChange: true
        });
        
        this.archivedDBName.extend({
            trackChange: true
        });

        this.serverSelected.extend({
            trackChange: true
        });

        //validation
        this.trackingDBName.extend({
            required: true,
            serverValidate: {
                params: "TrackingDatabaseName",
                message: this.i18n("M010")()
            }
        });

        this.archivedDBName.extend({
            required: true,
            serverValidate: {
                params: "ArchivedDatabaseName",
                message: this.i18n("M010")()
            }
        });

        this.serverSelected.extend({
            required: true
        });


        this.validationModel = ko.validatedObservable({
            trackingDBName: this.trackingDBName,
            archivedDBName: this.archivedDBName,
            serverSelected: this.serverSelected,
        });

    }

    /**
     * 
     * Get WebRequest for DatabaseConnection in Web API access.
     * @readonly
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    get webRequestDatabaseConnection() {
        return WebRequestDatabaseConnection.getInstance();
    }

    /**
     * 
     * Get WebRequest for DatabaseServer in Web API access.
     * @readonly
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    get webRequestDatabaseServer() {
        return WebRequestDatabaseServer.getInstance();
    }

    /**
     * 
     * 
     * @param {any} isFirstLoad
     * @returns
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var dfd = $.Deferred();

        var d1 = this.webRequestDatabaseServer.listDatabaseServer();

        var d2 = (this.mode === Screen.SCREEN_MODE_UPDATE) ? this.webRequestDatabaseConnection.getDBConnection(this.trackingDBId, [EntityAssociation.DatabaseConnection.Companies]) : null;
        $.when(d1, d2).done((r1, trackBD) => {
            var servers = r1["items"];
            this.serverOptions(servers);
            if(trackBD){

                this.trackingDBName(trackBD.trackingDatabaseName);
                this.archivedDBName(trackBD.archivedDatabaseName);
                this.companies(trackBD.companies);
                var selectedObj = ScreenHelper.findOptionByProperty(this.serverOptions, "id", trackBD.databaseServerId);
                this.serverSelected(selectedObj);

            }

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * 
     * 
     * @param {any} actions
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    buildActionBar(actions) {
        if (this.mode === Screen.SCREEN_MODE_CREATE) {
            if(WebConfig.userSession.hasPermission(Constants.Permission.TrackingDatabaseManagement)){
                actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
                actions.push(this.createActionCancel());
            }
        }
    }

    /**
     * 
     * 
     * @param {any} sender
     * @returns
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id == "actSave") {
         
            if (!this.validationModel.isValid()) {
                // console.log(this.validationModel.errors.showAllMessages());
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);

            var model = this.generateModel();

            this.webRequestDatabaseConnection.createDBConnection(model, true).done((response) => {
                this.isBusy(false);
                this.publishMessage("bo-configuration-tracking-databases-changed", response.id);
                this.close(true);
            }).fail((e) => {
                this.isBusy(false);
                this.handleError(e);
            });

           
        }
    }


    /**
     * 
     * 
     * @param {any} commands
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    buildCommandBar(commands) {
        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            if (WebConfig.userSession.hasPermission(Constants.Permission.TrackingDatabaseManagement)) {
                commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
            }
        }
    }

    /**
     * 
     * 
     * @param {any} sender
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    onCommandClick(sender) {
        if (sender.id === "cmdDelete") {
            this.showMessageBox(null, this.i18n("M099")(), BladeDialog.DIALOG_YESNO).done((button) => {
                switch (button) {
                    case BladeDialog.BUTTON_YES:
                        this.isBusy(true);
                        this.webRequestDatabaseConnection.deleteDBConnection(this.trackingDBId).done(() => {
                            this.isBusy(false);
                            this.publishMessage("bo-configuration-tracking-databases-changed", this.trackingDBId);
                            this.close(true);
                        }).fail((e) => {
                            this.isBusy(false);
                            this.handleError(e);
                        });
                        break;
                }
            });
        }
    }

    /**
     * 
     * 
     * @returns
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    generateModel() {
        var model = {
            id: this.trackingDBId,
            trackingDatabaseName: this.trackingDBName(),
            archivedDatabaseName: this.archivedDBName(),
            databaseServerId: this.serverSelected().id,
            infoStatus: Enums.InfoStatus.Add,
        };
        return model;
    }

    onUnload() {}
}

export default {
viewModel: ScreenBase.createFactory(TrackingDatabasesManageScreen),
    template: templateMarkup
};
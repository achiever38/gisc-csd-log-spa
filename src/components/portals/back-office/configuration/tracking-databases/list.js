import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import {Constants} from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestDatabaseConnection from "../../../../../app/frameworks/data/apitrackingcore/webRequestDatabaseConnection";

/**
 * 
 * 
 * @class TrackingDatabasesListScreen
 * @extends {ScreenBase}
 */
class TrackingDatabasesListScreen extends ScreenBase {
    /**
     * Creates an instance of TrackingDatabasesListScreen.
     * 
     * @param {any} params
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    constructor(params) {
        super(params);
        // Properties
        
        this.bladeTitle(this.i18n("Configurations_TrackingDatabases")());
        this.bladeSize = BladeSize.Medium;

        this.trackingDBs = ko.observableArray([]);
        this.filterText = ko.observable('');
        this.selectedTrackDB = ko.observable(null);
        this.recentChangedRowIds = ko.observableArray([]);
        this.order = ko.observable([
            [0, "asc"]
        ]);
        this.filterText = ko.observable();


        /**
         * 
         * 
         * @param {any} trackingDB
         * @returns
         */
        this._selectingRowHandler = (trackingDB) => {
            if (trackingDB && WebConfig.userSession.hasPermission(Constants.Permission.TrackingDatabaseManagement)) {
                return this.navigate("bo-configuration-tracking-databases-manage", {
                    mode: "update",
                    trackingDBId: trackingDB.id
                });
            }
            return false;
        };


        this.subscribeMessage("bo-configuration-tracking-databases-changed", (trackingDBId) => {
            this.isBusy(true);
            this.webRequestDatabaseConnection.listDBConnectionSummary().done((r)=> {
                 this.trackingDBs.replaceAll(r.items);
                 this.recentChangedRowIds.replaceAll([trackingDBId]);
                 this.isBusy(false);
            }).fail((e)=> {
                this.handleError(e);
                this.isBusy(false);
            });
        });
    }

    /**
     * 
     * 
     * @readonly
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    get webRequestDatabaseConnection() {
        return WebRequestDatabaseConnection.getInstance();
    }

    /**
     * 
     * 
     * @param {any} isFirstLoad
     * @returns
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var dfd = $.Deferred();
        this.webRequestDatabaseConnection.listDBConnectionSummary().done((r) => {
            this.trackingDBs(r["items"]);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;
    }

    /**
     * 
     * 
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    onChildScreenClosed() {
        this.selectedTrackDB(null);
    }

    /**
     * 
     * 
     * @param {any} commands
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.TrackingDatabaseManagement)) {
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Add")(), "svg-cmd-add"));
        }
    }

    /**
     * 
     * 
     * @param {any} sender
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    onCommandClick(sender) {
        if (sender.id == "cmdCreate") {
            this.navigate("bo-configuration-tracking-databases-manage", {
                mode: "create"
            });
            this.selectedTrackDB(null);
        }
    }

    /**
     * 
     * 
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    onUnload() {
    }
}

export default {
    viewModel: ScreenBase.createFactory(TrackingDatabasesListScreen),
    template: templateMarkup
};
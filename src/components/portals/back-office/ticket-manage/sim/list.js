﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import webRequestTicket from "../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";

class SimManageScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Ticket_SimManagement")());
        this.bladeSize = BladeSize.XLarge;
        this.bladeIsMaximized = true;

        this.selectedItems = ko.observableArray([]);
        this.items = ko.observableArray([]);
        this.apiDataSource = ko.observableArray([]);
        this.filterState = ko.observable();

        this.testClick = (data) => {
            this.navigate("bo-ticket-manage-sim-view",
                {
                    simId: data.id ,
                    actDate : data.activateDate
                });
        };

        // Observe change about filter
        this.subscribeMessage("bo-ticket-sim-management-changed",(data) => { 
           this.apiDataSource({ 
               read: this.webRequestTicket.getTicketSimDataGrid(this.filterState()),
               update: this.webRequestTicket.getTicketSimDataGrid(this.filterState())
           });
       });
      
        // Observe change about filter
        this.subscribeMessage("bo-ticket-sim-search-filter-changed",(data) => {
           this.filterState(data);
           this.apiDataSource({
               read: this.webRequestTicket.getTicketSimDataGrid(data),
               update: this.webRequestTicket.getTicketSimDataGrid(data)
           });
       });
    }

    get webRequestTicket() {
        return webRequestTicket.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var filter = { 
            
        };

        this.apiDataSource({
            read: this.webRequestTicket.getTicketSimDataGrid(filter),
            update: this.webRequestTicket.getTicketSimDataGrid(filter)
        });
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdSearch", this.i18n("Common_Search")(), "svg-cmd-search"));
        commands.push(this.createCommand("cmdImport", this.i18n("Common_Import")(), "svg-cmd-import"));
        commands.push(this.createCommand("cmdExport", this.i18n("Common_Export")(), "svg-cmd-export"));
        commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
       
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdExport":
                this.showMessageBox(null, this.i18n("M128")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.webRequestTicket.exportsimManagement(this.filterState()).done((response) => {
                                this.isBusy(false);
                                ScreenHelper.downloadFile(response.fileUrl);
                            }).fail((e) => {
                                this.isBusy(false);
                                this.handleError(e);
                            });
                            break;
                    }
                });
                break;
            case "cmdCreate":
                this.navigate("bo-ticket-manage-sim-manage", { mode: 'create' });
                //this.recentChangedRowIds.removeAll();
                break;
            case "cmdSearch":
                this.navigate("bo-ticket-manage-sim-search"); 
                break;
            case "cmdImport":
                this.navigate("bo-ticket-manage-sim-import"); 
                break;
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(SimManageScreen),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/WebRequestCompany";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/WebRequestBusinessUnit";
import webRequestTicket from "../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";
import webRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import webRequestOperatorPackage from "../../../../../app/frameworks/data/apitrackingcore/webRequestOperatorPackage";
import UIConstants from "../../../../../app/frameworks/constant/uiConstant";

class SimManageSearchScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Ticket_SimSearch")());
        this.bladeSize = BladeSize.Small;
        this.CompanyList = ko.observableArray([]);
        this.businessUnits = ko.observableArray([]);
        this.OperatorList = ko.observableArray([]);
        this.StatusList = ko.observableArray([]);
        //select Value;
        this.selectedCompany = ko.observable();
        this.selectedBusinessUnit = ko.observable();
        this.selectedOperator = ko.observable(null);
        this.selectedStatus = ko.observable();
        this.isIncludeSubBU = ko.observable(false);
        this.contractNo = ko.observable("");
        this.moblieNo = ko.observable("");

        //datetime
        this.formatSet = "dd/MM/yyyy";
        var addDay = 30;
        this.ContractStart = ko.observable();
        this.DurationStart = ko.observable();
        this.ContractEnd = ko.observable();
        this.DurationEnd = ko.observable();
        this.maxDateStart = ko.observable(new Date());
        this.minDateEnd = ko.observable(new Date());

        this.simType = ko.observableArray([]);
        this.selectedSimType = ko.observable();

        this.exceedDate  = ko.pureComputed(()=>{
            return Utility.addDays(this.ContractStart(),addDay);
        });
        this.exceedDateDuration  = ko.pureComputed(()=>{
            return Utility.addDays(this.DurationStart(),addDay);
        });

        this.selectedCompany.subscribe(val => {
            this.businessUnits([]);
            let companyId = (typeof this.selectedCompany() == "undefined")? null : val.id
            this.WebRequestBusinessUnit.listBusinessUnitSummary({ companyId: companyId }).done((response) => {
                this.businessUnits(response.items);
            });  //Call API BU
        });
     }

    get WebRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    get WebRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    get webRequestTicket() {
        return webRequestTicket.getInstance();
    }
    get webRequestEnumResource() {
        return webRequestEnumResource.getInstance();
    }
    get webRequestOperatorPackage() {
        return webRequestOperatorPackage.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var filter = {id : []};
        var enumSimStatusFilter = {
            types: [Enums.ModelData.EnumResourceType.SimStatus],
            values: []
        };
        this.webRequestEnumResource.listEnumResource(enumSimStatusFilter).done((reponse) => {
            this.StatusList(reponse["items"]);
        });

        //List Company
        this.WebRequestCompany.listCompany(filter).done((response) => {
            this.CompanyList(response.items);
        });

        //List Oparator 
        this.webRequestOperatorPackage.listOperatorPackage().done((response) => {
            //response.items.unshift({
            //    operator: "",
            //    id: 0
            //});
            this.OperatorList(response.items);
        });

        this.simType(UIConstants.SimType);
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSearch", this.i18n("Common_Search")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdClear", this.i18n("Common_Clear")(),"svg-cmd-clear"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        if (sender.id == "actSearch") {
          
            let companyId = (typeof this.selectedCompany() == "undefined")? null : this.selectedCompany().id
            let businessUnitId = (typeof this.selectedBusinessUnit() == "undefined")? null : this.selectedBusinessUnit()
            let operator =(typeof this.selectedOperator() == "undefined")? null : this.selectedOperator().id
            let status = (typeof this.selectedStatus() == "undefined")? null : this.selectedStatus().value
            let moblieNo =(typeof this.moblieNo() == null)? null : this.moblieNo()
            let contractNo =(typeof this.contractNo() == null)? null : this.contractNo()
            var IncludeSubBU = (this.isIncludeSubBU()) ? true : false;
            let simType = (this.selectedSimType()) ? this.selectedSimType().id : null;

            if(this.ContractStart != null){
                var formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.ContractStart()));
                if(formatSDate == "NaN-NaN-NaN" || formatSDate == "1970-01-01")
                {
                   formatSDate = null;
                }
            }
            if(this.ContractEnd != null){
                var formatEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.ContractEnd()));
                if(formatEDate == "NaN-NaN-NaN" || formatEDate == "1970-01-01")
                {
                    formatEDate = null;
                }

            }
            if(this.DurationStart != null){
                var formatSDateDuration = $.datepicker.formatDate("yy-mm-dd", new Date(this.DurationStart()));
                if(formatSDateDuration == "NaN-NaN-NaN" || formatSDateDuration == "1970-01-01")
                {
                    formatSDateDuration = null;
                }
            }
            if(this.DurationEnd != null){
                var formatEDateDuration = $.datepicker.formatDate("yy-mm-dd", new Date(this.DurationEnd()));
                if(formatEDateDuration == "NaN-NaN-NaN" || formatEDateDuration == "1970-01-01")
                {
                   formatEDateDuration = null;
                }
            }
      
            this.filterState = ko.observable({
                companyId: companyId,
                businessUnitId: businessUnitId,
                status: status,
                simType: simType,
                operatorId: operator,
                mobileNo: moblieNo,
                contractNo: contractNo,
                includeSubBusinessUnit: IncludeSubBU,
                contractStartDate : formatSDate,
                contractEndDate : formatEDate,
                endFreeFromDate : formatSDateDuration,
                endFreeToDate : formatEDateDuration,
            });
           
            this.publishMessage("bo-ticket-sim-search-filter-changed", this.filterState());
        }
        else if (sender.id === "actCancel") {
            this.close();
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdClear") {
            this.isIncludeSubBU(false);
            this.selectedBusinessUnit(null);
            this.selectedCompany("");
            this.selectedStatus(null);
            this.moblieNo(null);
            this.contractNo(null);
            this.ContractStart(null);
            this.ContractEnd(null);
            this.DurationStart(null);
            this.DurationEnd(null);
            this.selectedOperator(null);
        }
    }
    
}

export default {
viewModel: ScreenBase.createFactory(SimManageSearchScreen),
    template: templateMarkup
};
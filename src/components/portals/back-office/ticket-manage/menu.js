﻿import ko from "knockout";
import templateMarkup from "text!./menu.html";
import ScreenBase from "../../screenbase";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import { Constants } from "../../../../app/frameworks/constant/apiConstant";

/**
 * Display configuration menu based on user permission.
 * @class ConfigurationMenuScreen
 * @extends {ScreenBase}
 */
class TicketMenuScreen extends ScreenBase {

    /**
     * Creates an instance of ConfigurationMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Ticket_Management")());
        this.bladeMargin = BladeMargin.Narrow;
        this.bladeSize = BladeSize.XSmall;

        this.items = ko.observableArray([]);
        this.selectedIndex = ko.observable();

        this._selectingRowHandler = (item) => {
            if (item) {
                return this.navigate(item.page);
            }
            return false;
        };
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // TODO: Verify access right to specific items.
        if (!isFirstLoad) {
            return;
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.AssetMonitoring))
        {
            this.items.push({
                text: this.i18n('Ticket_AssetMonitoring')(),
                page: 'bo-ticket-manage-asset-find'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.TicketDashboard))
        {
            this.items.push({
                text: this.i18n('Ticket_Dashboard')(),
                page: 'bo-ticket-dashboard'
            });
        }


        this.items.push({
            text: 'Ticket',
            page: 'bo-ticket-search'
        });


        if(WebConfig.userSession.hasPermission(Constants.Permission.Installation))
        {
            this.items.push({
                text: this.i18n('Ticket_Installation')(),
                page: 'bo-ticket-manage-installation'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.Appointments))
        {
            this.items.push({
                text: this.i18n('Ticket_Appointments')(),
                page: 'bo-ticket-manage-appointments'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.MailManagement))
        {
            this.items.push({
                text: this.i18n('Ticket_MailManagement')(),
                page: 'bo-ticket-manage-mail'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.SimManagement))
        {
            this.items.push({
                text: this.i18n('Ticket_SimManagement')(),
                page: 'bo-ticket-manage-sim'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.SimManagement))
        {
            this.items.push({
                text: this.i18n('Sim Stock')(),
                page: 'bo-ticket-manage-sim-stock'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.OperatorPackage))
        {
            this.items.push({
                text: this.i18n('Assets_OperatorPackages')(),
                page: 'bo-ticket-manage-package'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.VendorsManagement))
        {
            this.items.push({
                text: this.i18n('Vendors')(),
                page: 'bo-ticket-manage-vendor'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.TransportersManagement))
        {
            this.items.push({
                text: this.i18n('Transporters')(),
                page: 'bo-ticket-manage-transporter'
            });
        }
        
        

    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedIndex(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}

    // /**
    //  * Navigate to inner screens.
    //  * @param {any} extras
    //  */
    // goto (extras) {
    //     this.navigate(extras.id);
    // }
}

export default {
viewModel: ScreenBase.createFactory(TicketMenuScreen),
    template: templateMarkup
};
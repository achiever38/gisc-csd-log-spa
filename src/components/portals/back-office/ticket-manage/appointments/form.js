﻿import ko from "knockout";
import templateMarkup from "text!./form.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import {Enums} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/WebRequestCompany";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/WebRequestBusinessUnit";
import webRequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicle";
import webRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import webRequestBox from "../../../../../app/frameworks/data/apitrackingcore/webRequestBox";
import webRequestUser from "../../../../../app/frameworks/data/apicore/webRequestUser";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import WebRequestTicket from "../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";


class AppointmentsScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Ticket_SearchAppointment")());
        this.bladeSize = BladeSize.Small;
        this.CompanyList = ko.observableArray([]);
        this.businessUnits = ko.observableArray([]);
        this.LicenseList = ko.observableArray([]);
        this.BoxList = ko.observableArray([]);
        this.StatusList = ko.observableArray([]);
        this.TechnicianList = ko.observableArray([])
        //select Value;
        this.ticketNo = ko.observable();
        this.selectedCompany = ko.observable();
        this.selectedBusinessUnit = ko.observable();
        this.selectedLicense = ko.observable(null);
        this.selectedBoxSerialNo = ko.observable();
        this.selectedStatus = ko.observable();
        this.selectedTechnician = ko.observable();
        this.isIncludeSubBU = ko.observable(false);

        this.serviceTypeOptions = ko.observableArray();
        this.selectedServiceType = ko.observable();

        this.selectedCompany.subscribe(val => {
            this.businessUnits([]);
            let companyId = (typeof this.selectedCompany() == "undefined")? null : val.id
            this.WebRequestBusinessUnit.listBusinessUnitSummary({ companyId: companyId }).done((response) => {
                this.businessUnits(response.items);
            });  //Call API BU
        });
        this.selectedEntityBinding = ko.pureComputed(() => {
            this.LicenseList([]);
            this.selectedLicense([]);
            var businessUnitIds = this.selectedBusinessUnit();
            if(this.isIncludeSubBU() && businessUnitIds != null){
                businessUnitIds = Utility.includeSubBU(this.businessUnits(),businessUnitIds);
            }
            if(this.selectedBusinessUnit() != null && this.selectedCompany() != null) {
                this.webRequestVehicle.listVehicleSummary({ 
                    companyId: this.selectedCompany().id,
                    businessUnitIds: businessUnitIds
                }).done((response) => {
                    response.items.unshift({
                        license: "",
                        id: 0
                    });
                    this.LicenseList(response.items);
                });
                //Call Api Box Serial No 
                this.webRequestBox.listBoxSummary({companyId: this.selectedCompany(),businessUnitIds: businessUnitIds}).done((reponse) => {
                    this.BoxList(reponse.items);
                   
                });
                //Appointment Status
            }
        });

        //datetime
        this.formatSet = "dd/MM/yyyy";
        var addDay = 30;
        var setTimeNow = ("0" + new Date().getHours()).slice(-2)+":"+("0" + new Date().getMinutes()).slice(-2);
        this.start = ko.observable(new Date());
        this.end = ko.observable(Utility.addDays(this.start(),addDay));
        this.timeStart = ko.observable("00:00");
        this.timeEnd = ko.observable(setTimeNow);
        this.maxDateStart = ko.observable(new Date());
        this.minDateEnd = ko.observable(new Date());
        this.exceedDate  = ko.pureComputed(()=>{
            return Utility.addDays(this.start(),addDay);
        });
    }

    setupExtend() {
        this.start.extend({ required: true });
        this.end.extend({ required: true});
        this.timeStart.extend({ required:true});
        this.timeEnd.extend({ required: true});

        this.validationModel = ko.validatedObservable({
            end:this.end,
            start:this.start,
            timeStart:this.timeStart,
            timeEnd:this.timeEnd
        });
    }

    get WebRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    get WebRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    get webRequestVehicle() {
        return webRequestVehicle.getInstance();
    }
    get webRequestBox() {
        return webRequestBox.getInstance();
    }
    get webRequestUser() {
        return webRequestUser.getInstance();
    }
    get webRequestEnumResource() {
        return webRequestEnumResource.getInstance();
    }

    get webRequestTicket() {
        return WebRequestTicket.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var dfd = $.Deferred();

        var filter = {id : []};
        //Ticket Appointment Status 
        var enumTickerStatusFilter = {
            types: [Enums.ModelData.EnumResourceType.TicketAppointmentStatus],
            values: []
        };
        //Technician Team 
        var userfilter = {
            groupIds: 5
        };
        var r1 = this.webRequestEnumResource.listEnumResource(enumTickerStatusFilter);
        var r2 = this.WebRequestCompany.listCompany(filter);
        var r3 = this.webRequestUser.listUserSummary(userfilter);
        var r4 = this.webRequestTicket.listServiceTypeSummary();

        $.when(r1,r2,r3,r4).done((res1,res2,res3,res4) => {
            this.StatusList(res1["items"]);
            this.CompanyList(res2["items"]);
            this.TechnicianList(res3["items"]);
            this.serviceTypeOptions(res4["items"]);

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

      } 
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSearch", this.i18n("Common_Search")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdClear", this.i18n("Common_Clear")(),"svg-cmd-clear"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if(!this.validationModel.isValid()){
            this.validationModel.errors.showAllMessages();
            return ;
        }

        if (sender.id == "actSearch") {
            let companyId = (typeof this.selectedCompany() == "undefined")? null : this.selectedCompany().id
            let businessUnitId = (typeof this.selectedBusinessUnit() == "undefined")? null : this.selectedBusinessUnit()
            let boxId =(typeof this.selectedBoxSerialNo() == "undefined")? null : this.selectedBoxSerialNo().id
            let vehicleId =(typeof this.selectedLicense() == "undefined")? null : this.selectedLicense().id
            let technicianId =(typeof this.selectedTechnician() == "undefined")? null : this.selectedTechnician().id
            let status = (typeof this.selectedStatus() == "undefined")? null : this.selectedStatus().value
            let ticketNo =(typeof this.ticketNo() == "undefined")? null : this.ticketNo()
            var IncludeSubBU = (this.isIncludeSubBU()) ? true : false;
            var timeStart = (this.timeStart()) ? this.timeStart() : "00:00";
            var timeEnd = (this.timeEnd()) ? this.timeEnd() : "00:00";
            var formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.start())) + " " + timeStart + ":00";
            var formatEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.end())) + " " + timeEnd + ":00";
            var serviceTypeId = this.selectedServiceType() ? this.selectedServiceType().id : null;

            if(_.isNil(this.start()) && _.isNil(this.end())){
                formatSDate = null;
                formatEDate = null;
            }

            var filter = {
                boxId: boxId,
                companyId: companyId,
                businessUnitId: businessUnitId,
                status: status,
                vehicleId: vehicleId,
                ticketId: ticketNo,
                technicianId: technicianId,
                includeSubBU: IncludeSubBU,
                appointmentStartDate : formatSDate,
                appointmentEndDate : formatEDate,
                serviceTypeId: serviceTypeId
            };
                this.navigate("bo-ticket-manage-appointments-view", filter);
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {

        switch(sender.id){
        
            case "cmdClear":
                    this.selectedBoxSerialNo(null);
                    this.selectedBusinessUnit(null);
                    this.selectedCompany("");
                    this.selectedLicense(null);
                    this.selectedStatus(null);
                    this.selectedTechnician(null);
                    this.start(new Date());
                    this.end(Utility.addDays(this.start(),30));
                    this.ticketNo(null)
                    this.timeStart("00:00");
                    this.timeEnd("00:00");
                    this.isIncludeSubBU(false);

                    break;
        }
       
        
    }
    
}

export default {
    viewModel: ScreenBase.createFactory(AppointmentsScreen),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import webRequestTicket from "../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";

class AppointmentsView extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Ticket_Appointments")());
        this.bladeSize = BladeSize.XLasrge;
        this.bladeIsMaximized = true;
        
        this.apiDataSource = ko.observableArray([]);

        this.boxId = this.ensureNonObservable(params.boxId);
        this.companyId = this.ensureNonObservable(params.companyId);
        this.businessUnitId = this.ensureNonObservable(params.businessUnitId);
        this.status = this.ensureNonObservable(params.status);
        this.vehicleId = this.ensureNonObservable(params.vehicleId);
        this.technicianId = this.ensureNonObservable(params.technicianId);
        this.includeSubBU = this.ensureNonObservable(params.includeSubBU);
        this.ticketId = this.ensureNonObservable(params.ticketId);
        this.appointmentEndDate = this.ensureNonObservable(params.appointmentEndDate);
        this.appointmentStartDate = this.ensureNonObservable(params.appointmentStartDate);
        this.serviceTypeId = this.ensureNonObservable(params.serviceTypeId);

        this.detailTicket = (data) => {
            this.navigate("bo-ticket-manage-asset-details",{ ticketId : data.ticketId})
        };

        this.subscribeMessage("update-items-appointment",(item) =>{
            //console.log("test");
            var filter = {
                boxId: this.boxId,
                technicianId: this.technicianId,
                appointmentStartDate: this.appointmentStartDate,
                appointmentEndDate: this.appointmentEndDate,
                companyId: this.companyId,
                businessUnitId: this.businessUnitId,
                includeSubBusinessUnit:   this.includeSubBU,
                status: this.status,
                vehicleId: this.vehicleId,
                ticketId:this.ticketId,
                serviceTypeId: this.serviceTypeId
            };
            
            this.apiDataSource({
                read: this.webRequestTicket.listTicketAppointmentDataGrid(filter)
            });
        });
        
    }

    get webRequestTicket() {
        return webRequestTicket.getInstance();
    }


    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var filter = {
            boxId: this.boxId,
            technicianId: this.technicianId,
            appointmentStartDate: this.appointmentStartDate,
            appointmentEndDate: this.appointmentEndDate,
            companyId: this.companyId,
            businessUnitId: this.businessUnitId,
            includeSubBusinessUnit:   this.includeSubBU,
            status: this.status,
            vehicleId: this.vehicleId,
            ticketId:this.ticketId,
            serviceTypeId: this.serviceTypeId
        };

        this.apiDataSource({
            read: this.webRequestTicket.listTicketAppointmentDataGrid(filter)
        });

    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdExport", this.i18n("Common_Export")(), "svg-cmd-export"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
       
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdExport":
                this.showMessageBox(null, this.i18n("M128")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            let filterExport = {
                                boxId: this.boxId,
                                technicianId: this.technicianId,
                                appointmentStartDate: this.appointmentStartDate,
                                appointmentEndDate: this.appointmentEndDate,
                                companyId: this.companyId,
                                businessUnitId: this.businessUnitId,
                                includeSubBusinessUnit:   this.includeSubBU,
                                status: this.status,
                                vehicleId: this.vehicleId,
                                ticketId:this.ticketId,
                                serviceTypeId: this.serviceTypeId
                            };
                            //var filterToExport = this.checkData(this.data);
                            this.webRequestTicket.exportAppointmentTicket(filterExport).done((response)=>{
                                ScreenHelper.downloadFile(response.fileUrl);
                            }).fail((e)=>{
                                this.handleError(e);
                            }).always(()=>{
                                this.isBusy(false);
                            });
                            break;
                    }
                });
                break;
        }
    } 
}

export default {
viewModel: ScreenBase.createFactory(AppointmentsView),
    template: templateMarkup
};
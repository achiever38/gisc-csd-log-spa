import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import {
    Enums,
    EntityAssociation
} from "../../../../../app/frameworks/constant/apiConstant";
import Utility from "../../../../../app/frameworks/core/utility";
import WebRequestAssetMonitoring from "../../../../../app/frameworks/data/apitrackingcore/webRequestAssetMonitoring";
import GenerateDataGridExpand from "../../../company-workspace/generateDataGridExpand";
import {
    AssetIcons
} from "../../../../../app/frameworks/constant/svg";
import WebRequestTicket from "../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";
import DefaultSoring from "../../../../../app/frameworks/constant/defaultSorting";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import moment from "moment";

class TicketSearchListScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.filterState = this.ensureObservable(params, {});
        this.bladeTitle('Ticket');
        // this.bladeSize = BladeSize.XLarge;
        this.bladeIsMaximized = true;
        this.selectedItems = ko.observableArray([]);
        this.selectedRow = ko.observableArray([]);
        this.apiDataSource = ko.observableArray([]);

        this.subscribeMessage("bo-ticket-search-filter-changed",(data) => {
            this.filterState(data);
            this.refreshShipmentList();
       });      

        this.subscribeMessage("bo-ticket-confirm-search-changed", (data) => {
            //set params
            this.data = data;
            this.duration = data.duration;
            this.refreshShipmentList();
        });

        // Observe change about filter
        // this.subscribeMessage("bo-ticket-manage-ticket-search", (data) => {
        // });

        this.subscribeMessage("bo-ticket-manage-update-items-appointment",() =>{
            this.filterState();
            this.refreshShipmentList();
        }); 

        this.templateAppointmentDate = (data) => {
            let dateAppointment = "";
            if(data.ticketAppointment.id > 0){
                dateAppointment = data.ticketAppointment.formatAppointDate;
            }
            return dateAppointment;
        }

        this.templateAppointmentStatus = (data) => {
            let appointmentStatus = "";
            if(data.ticketAppointment.id > 0){
                appointmentStatus = data.ticketAppointment.statusDisplayName;
            }
            return appointmentStatus;
        } 

        this.templateProvinceName = (data) => {
            let appointmentProvinceName = "";
            if(data.ticketAppointment.id > 0){
                let provinceNameVal = data.ticketAppointment.provinceName;
                appointmentProvinceName =  _.isNil(provinceNameVal) ? "" : provinceNameVal;
            }
            return appointmentProvinceName;
        } 

        this.templateTechnicianName = (data) => {
            let appointmentTechnicianName = "";
            if(data.ticketAppointment.id > 0){
                let technicianNameVal = data.ticketAppointment.technicianName;
                appointmentTechnicianName =  _.isNil(technicianNameVal) ? "" : technicianNameVal;
            }
            return appointmentTechnicianName;
        } 

        this.templateTechnicianPhone = (data) => {
            let appointmentTechnicianPhone = "";
            if(data.ticketAppointment.id > 0){
                let technicianPhoneVal = data.ticketAppointment.technicianPhone;
                appointmentTechnicianPhone = _.isNil(technicianPhoneVal) ? "" : technicianPhoneVal;
            }
            return appointmentTechnicianPhone;
        } 

        this.viewDetail = (data) => {
            this.navigate("bo-ticket-manage-asset-details", { ticketId: data.ticketId });
        };

        this.viewAppointment = (data) =>{
            if(data.appointDate){
                data.appointDate = new Date(data.appointDate);
            }
            this.navigate("bo-ticket-manage-edit-appointment",
            {
                items:data,
                companyId : data.companyId
            });
        };

        this.onDetailClickColumns = ko.observableArray([]);
        this.onDetailClickColumns(
            GenerateDataGridExpand.getOnClickColumns(this.generateColumnDetail())
        );
    }

    /**
     * Get WebRequest specific for AssetMonitoring module in Web API access.
     * @readonly
     */
    get webRequestAssetMonitoring() {
        return WebRequestAssetMonitoring.getInstance();
    }
    get webRequestTicket() {
        return WebRequestTicket.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        
        var formatSDate =  moment(Utility.addMonths(new Date(),-1)).format("YYYY-MM-DD")+ " 00:00:00";
        var formatEDate = moment(new Date()).format("YYYY-MM-DD")+ " 23:59:59";
        
        let filter = {
            startCreateDate:formatSDate,
            endCreateDate:formatEDate
        }

        this.filterState(filter);
        
        this.refreshShipmentList();
    }
    
    refreshShipmentList() {
        this.dispatchEvent("TicketSearchList", "refresh");
    }

    onDatasourceRequestRead(gridOption) {
        var dfd = $.Deferred();
        var self = this
        var filter = Object.assign({}, this.filterState(), gridOption);
        this.isBusy(true);

        this.webRequestTicket.listTicketSummary(filter).done(response => {
              dfd.resolve({
                  items: response["items"],
                  totalRecords: response["totalRecords"],
                  currentPage : response["currentPage"]
              });
          })
          .fail(e => {
              this.handleError(e);
          })
          .always(() => {
              this.isBusy(false);
            //   this.onAutoRefresh(true);
          });

        return dfd;
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdSearch", this.i18n("Search")(), "svg-cmd-search"));
        commands.push(this.createCommand("cmdEdit", this.i18n("Ticket_EditMultipleTicket")(), "svg-ticket-editticket"));
        commands.push(this.createCommand("cmdCreate", this.i18n("Ticket_CreateAppointmentMultipleTicket")(), "svg-ticket-create-appointment"));
        commands.push(this.createCommand("cmdSendMail", this.i18n("Ticket_SendMailMultipleTicket")(), "svg-ticket-sentmail"));
        commands.push(this.createCommand("cmdExport", this.i18n("Common_Export")(), "svg-cmd-export"));        
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdEdit":
                if (Object.keys(this.selectedItems()).length < 1) {
                    this.showMessageBox("", this.i18n("M168")(), BladeDialog.DIALOG_OK);
                    return false;
                }
                let infos = {
                    filterState: this.filterState(),
                    items: this.selectedItems()
                }
                this.navigate("bo-ticket-manage-edit", infos);
                break;
            case "cmdCreate":
                if (Object.keys(this.selectedItems()).length < 1) {
                    this.showMessageBox("", this.i18n("M168")(), BladeDialog.DIALOG_OK);
                    return false;
                }
                this.navigate("bo-ticket-manage-create", {
                    items: this.selectedItems(),
                    Ismultiple: true,
                    filterState: this.filterState()
                });
                break;
            case "cmdSendMail":
                if (Object.keys(this.selectedItems()).length < 1) {
                    this.showMessageBox("", this.i18n("M168")(), BladeDialog.DIALOG_OK);
                    return false;
                }
                this.navigate("bo-ticket-manage-sendmail", this.selectedItems());
                break;
            case "cmdExport":
                this.showMessageBox(null, this.i18n("M128")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.webRequestTicket.exportTicket(this.filterState()).done((response) => {
                                ScreenHelper.downloadFile(response.fileUrl);
                            }).fail((e) => {
                                this.handleError(e);
                            }).always(() => {
                                this.isBusy(false);
                            });
                            break;
                    }
                });
                break;
            case "cmdSearch":
                this.navigate("bo-ticket-manage-search");
                break;
        }
    }

    checkData(input) {
        var data = {};
        if (input != null) {
            data = input;
            data.Status = Enums.ModelData.TicketStatus.Confirm;
            data.Duration = this.duration;
        }
        else {
            data = {
                Status: Enums.ModelData.TicketStatus.Confirm,
                Duration: this.duration
            };
        }
        return data;
    }

    generateColumnDetail() {
        var self = this;
        var columnsDetail = [{
            type: "text",
            width: "10px",
            title: this.i18n("Ticket_AppointmentDate")(),
            data: "formatAppointDate",
            className: "dg-body-left",            
            onClick: this.viewAppointment        
        },{
            type: "text",
            width: "60px",
            title: this.i18n("Ticket_AppointmentDescription")(),
            data: "appointmentDescription",
            className: "dg-body-left",
            onClick: this.viewAppointment        
        }, {
            type: "text",
            width: "20px",
            title: this.i18n("Common_Status")(),
            data: "statusDisplayName",
            className: "dg-body-left",
            onClick: this.viewAppointment
        }, {
            type: "text",
            width: "40px",
            title: this.i18n("Ticket_Province")(),
            data: "provinceName",
            className: "dg-body-left",
            onClick: this.viewAppointment
        },{
            type: "text",
            width: "50px",
            title: this.i18n("Ticket_TechnicianTeam")(),
            data: "technicianName",
            className: "dg-body-left",
            onClick: this.viewAppointment
        }, {
            type: "text",
            width: "40px",
            title: this.i18n("Common_Phone")(),
            data: "technicianPhone",
            className: "dg-body-left",
            onClick: this.viewAppointment
        }
    ];
        
        return columnsDetail;
    }

    onDetailInit(evt) {
        var self = this;
        var inactive = evt.data.totalActiveTicket;
        let ticketId = evt.data.id; // ticket test = 203
        
        var filter = {TicketId:ticketId, isClose:true}
       
        var appointmenItems = this.webRequestTicket.listTicketAppointment(filter)


        $.when(appointmenItems).done((responseinactive) => {

                $("<div id='" + self.id + evt.data.businessUnitId + "'/>")
                    .appendTo(evt.detailCell)
                    .kendoGrid({
                        dataSource: responseinactive.items,
                        noRecords: {
                            template: self.i18n("M111")()
                        },
                        pageable: GenerateDataGridExpand.getPageable(false),
                        
                        // set column of kendo grid
                        columns: GenerateDataGridExpand.getColumns(
                            // [{
                            //     type: "text",
                            //     title: this.i18n("Ticket_ActiveTickets")(),
                            //     columns: self.generateColumnDetail(),
                            // }]
                            self.generateColumnDetail()
                        )
                    });

                
            })
            .fail(e => {
                this.handleError(e);
            })
            .always(() => {
                this.isBusy(false);
            });
    }
}

export default {
    viewModel: ScreenBase.createFactory(TicketSearchListScreen),
    template: templateMarkup
};
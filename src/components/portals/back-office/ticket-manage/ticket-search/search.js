import ko from "knockout";
import templateMarkup from "text!./search.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/WebRequestCompany";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/WebRequestBusinessUnit";
import webRequestTicket from "../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";
import webRequestUser from "../../../../../app/frameworks/data/apicore/webRequestUser";
import webRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import webRequestOperatorPackage from "../../../../../app/frameworks/data/apitrackingcore/webRequestOperatorPackage";
import UIConstants from "../../../../../app/frameworks/constant/uiConstant";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import moment from "moment";

class TicketSearchScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle('Search');
        this.bladeSize = BladeSize.Small;
        this.CompanyList = ko.observableArray([]);
        this.businessUnits = ko.observableArray([]);
        this.ticketStatusOptions = ko.observableArray([]);
        this.license = ko.observable();
        this.assetId = ko.observable();
        this.values = ko.observable();
        this.AssignTo = ko.observableArray([]);
        this.vehicleOptions = ko.observableArray();

        this.pendingCause = ko.observableArray([]);
        this.pendingSubCause = ko.observableArray([]);
        this.closeCause = ko.observableArray([]);
        this.closeSubCause = ko.observableArray([]);
        this.serviceId = ko.observable();

        //select Value;
        this.selectedCompany = ko.observable();
        this.selectedBusinessUnit = ko.observable();
        this.selectVehicle = ko.observable();
        this.isIncludeSubBU = ko.observable(false);
        this.ticketNo = ko.observable("");
        this.contractNo = ko.observable("");
        this.mobileNo = ko.observable("");
        this.selectTicketStatus = ko.observable();
        this.selectedAssignTo = ko.observable();

        this.selectedPending = ko.observable();
        this.selectedSubPend = ko.observable();
        this.selectedClose = ko.observable();
        this.selectedSubClose = ko.observable();
        this.ServiceTypeOptions = ko.observableArray([]);

        //datetime
        this.formatSet = "dd/MM/yyyy";
        
        this.TicketStartDate = ko.observable(Utility.addMonths(new Date(),-1));
        this.TicketEndDate = ko.observable(new Date());
        this.maxDateStart = ko.observable(new Date());
        this.minDateEnd = ko.observable(new Date());

        var addDay = 30;
        this.exceedDate  = ko.pureComputed(()=>{
            return Utility.addDays(this.TicketStartDate(),addDay);
        });

        this.enableTicketPending = ko.pureComputed(() => {
            return this.selectTicketStatus() && this.selectTicketStatus().value === Enums.ModelData.TicketStatus.Pending;
        });

        this.enableTicketClose = ko.pureComputed(() => {
            return this.selectTicketStatus() && this.selectTicketStatus().value === Enums.ModelData.TicketStatus.Close;
        });

        this.selectedCompany.subscribe(val => {
            this.businessUnits([]);
            let companyId = (typeof this.selectedCompany() == "undefined")? null : val.id
            this.WebRequestBusinessUnit.listBusinessUnitSummary({ companyId: companyId }).done((response) => {
                this.businessUnits(response.items);
            });  //Call API BU
        });
        
        this.selectedPending.subscribe(val => {
            if(val){
                let pendingId = (typeof this.selectedPending() == "undefined")? null : val.id
                this.webRequestTicket.getPendingSubCause(pendingId).done((reponse) => {
                    this.pendingSubCause(reponse);
                });
            }
            else{
                this.pendingSubCause.replaceAll([]);
            }
        });
        
        this.selectedClose.subscribe(val => {
            if(val){
                let closeId = (typeof val == "undefined")? null : val.id
                this.webRequestTicket.closeSubCause(closeId).done((reponse) => {
                    this.closeSubCause(reponse);
                });
            }
            else{
                this.closeSubCause.replaceAll([]);
            }
        });

        this.periodDay = ko.pureComputed((e) => {
            // can't select date more than current date
            addDay = 30;
            var isDate = Utility.addDays(new Date());
            let currentDate = parseInt(new Date() / (1000 * 60 * 60 * 24));
            let endDate = parseInt(isDate / (1000 * 60 * 60 * 24)) + 1;
            var calDay = endDate - currentDate;
            if(calDay > 0){
                isDate = new Date();
            }
     
            // set date when clear start date and end date
            if(this.TicketStartDate() === null){
                // set end date when clear start date
                if(this.TicketEndDate() === null){
                    
                    isDate = new Date();
                   
                }else{
                 
                    isDate = this.TicketEndDate();
              
                }
            
                this.maxDateStart(isDate);
                this.minDateEnd(isDate);
          
            }else{
                
                this.minDateEnd(this.TicketStartDate())
         
            }

            return isDate;
        });
        
        for(var key in params) {
            var value = params[key];
        }

        this.values(typeof value =='undefined' ? null : value.status);


        this.selectedEntityBinding = ko.pureComputed(() => {
            this.vehicleOptions([]);
            if(this.selectedBusinessUnit() != undefined){
                var businessUnitIds = (this.isIncludeSubBU()) ? Utility.includeSubBU(this.businessUnits(),this.selectedBusinessUnit()) : this.selectedBusinessUnit();
                var currentCompanyId = (typeof this.selectedCompany() == 'undefined') ? null : this.selectedCompany().id;
                //Vehicle
                this.webRequestVehicle.listVehicleSummary({
                    companyId: currentCompanyId,
                    businessUnitIds: businessUnitIds,
                    sortingColumns: DefaultSorting.Vehicle
                }).done((response) => {
                     let listVehicle = response.items;
                    this.vehicleOptions(listVehicle);
                });
            }
            return ' ';
        });
    }

    get WebRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    get WebRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    get webRequestTicket() {
        return webRequestTicket.getInstance();
    }
    get webRequestEnumResource() {
        return webRequestEnumResource.getInstance();
    }
    get webRequestOperatorPackage() {
        return webRequestOperatorPackage.getInstance();
    }
    get webRequestUser() {
        return webRequestUser.getInstance();
    }
    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var filter = {id : []};
        // var enumSimStatusFilter = {
        //     types: [Enums.ModelData.EnumResourceType.SimStatus],
        //     values: []
        // };

        var enumTicketStatus = {
            types:[Enums.ModelData.EnumResourceType.TicketStatus]
        };

        var enumVehicleType = { 
            companyId: WebConfig.userSession.currentCompanyId, 
            types: [ Enums.ModelData.EnumResourceType.VehicleType ], 
            sortingColumns: DefaultSorting.EnumResource 
        };

        var EnumUserGroup = { 
            groupIds:[
                Enums.UserGroup.Administrator,
                Enums.UserGroup.SystemAdmin
            ],
            sortingColumns: DefaultSorting.User,
            enable: true
        };

        //Service Type
        this.webRequestTicket.listServiceTypeSummary({}).done((response) => {
            this.ServiceTypeOptions(response["items"]);
        });

        //List Company
        this.WebRequestCompany.listCompany(filter).done((response) => {
            this.CompanyList(response.items);
        });

        this.webRequestEnumResource.listEnumResource(enumTicketStatus).done((response) => {
            this.ticketStatusOptions(response["items"]);
        });

        this.webRequestTicket.getPendingCause().done((response) => {
            this.pendingCause(response);
            this.closeCause(response);

        });

        this.webRequestUser.listUserSummary(EnumUserGroup).done((response) => {
            response.items.forEach((v)=>{
                v.displayName = (v.fullName == null || v.fullName.trim() == "" ) ? v.username : v.fullName
            });
            this.AssignTo(response.items);
        });
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSearch", this.i18n("Common_Search")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdClear", this.i18n("Common_Clear")(),"svg-cmd-clear"));
    }

    setupExtend() {

        this.TicketStartDate.extend({ required: true });
        this.TicketEndDate.extend({ required: true });

        this.validationModel = ko.validatedObservable({
            TicketStartDate: this.TicketStartDate,
            TicketEndDate: this.TicketEndDate,
        });

    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        if (sender.id == "actSearch") {
          
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
            
            let companyId = (typeof this.selectedCompany() == "undefined")? null : this.selectedCompany().id;
            let businessUnitId = (typeof this.selectedBusinessUnit() == "undefined")? null : this.selectedBusinessUnit();
            let mobileNo =(typeof this.mobileNo() == null)? null : this.mobileNo();
            let ticketNo = (typeof this.ticketNo() == null)? null : this.ticketNo();
            let contractNo = (typeof this.contractNo() == null)? null : this.contractNo();
            var includeSubBU = (this.isIncludeSubBU()) ? true : false;
            let vehicleId = (this.selectVehicle()) ? this.selectVehicle().id : null;
            let license = (typeof this.license() == null)? null : this.license();
            let assetId = (typeof this.assetId() == null)? null : this.assetId();
            let ticketStatusOptions = (this.selectTicketStatus()) ? this.selectTicketStatus().value : null;
            let assignTo = (this.selectedAssignTo()) ? this.selectedAssignTo().id : null;
            var formatSDate =  moment(new Date(this.TicketStartDate())).format("YYYY-MM-DD")+ " 00:00:00";
            var formatEDate = moment(new Date(this.TicketEndDate())).format("YYYY-MM-DD")+ " 23:59:59";
            let serviceType = (typeof this.serviceId() == "undefined")? null : this.serviceId().id
            let pending = (this.selectedPending()) ? this.selectedPending().id : null;
            let subPending = (this.selectedSubPend()) ? this.selectedSubPend().id : null;
            let close = (this.selectedClose()) ? this.selectedClose().id : null;
            let subClose = (this.selectedSubClose()) ? this.selectedSubClose().id : null;

            this.filterState = ko.observable({
                companyId: companyId,
                businessUnitId: businessUnitId,
                includeSubBusinessUnit: includeSubBU,
                vehicleId: vehicleId,
                license: license,
                assetId: assetId,
                ticketId: ticketNo,
                contractNo: contractNo,
                mobileNo: mobileNo,
                status: ticketStatusOptions,
                startCreateDate:formatSDate,
                endCreateDate:formatEDate,
                assignToId: assignTo,
                serviceType: serviceType,
                pendingCauseId: pending,
                pendingSubCauseId: subPending,
                closeCauseId: close,
                closeSubCauseId: subClose
            });
           
            this.publishMessage("bo-ticket-search-filter-changed", this.filterState());
        }
        else if (sender.id === "actCancel") {
            this.close();
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdClear") {
            this.isIncludeSubBU(false);
            this.selectedBusinessUnit('');
            this.selectedCompany("");
            this.selectVehicle('');
            this.selectTicketStatus('');
            this.mobileNo('');
            this.contractNo('');
            this.TicketStartDate(new Date());
            this.TicketEndDate(new Date());
            this.license('');
            this.assetId('');
            this.AssignTo('');
        }
    }
    
}

export default {
viewModel: ScreenBase.createFactory(TicketSearchScreen),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./view-appointment.html";
import ScreenBase from "../../../../screenbase";
import ScreenHelper from "../../../../screenhelper";
import * as Screen from "../../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import { Enums} from "../../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../../app/frameworks/core/utility";
import webRequestEnumResource from "../../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import webRequestCompany from "../../../../../../app/frameworks/data/apicore/webRequestCompany";
import webRequestProvince from "../../../../../../app/frameworks/data/apicore/webRequestProvince";
import webRequestCity from "../../../../../../app/frameworks/data/apicore/webRequestCity";
import webRequestTown from "../../../../../../app/frameworks/data/apicore/webRequestTown";
import webRequestUser from "../../../../../../app/frameworks/data/apicore/webRequestUser";
import webRequestTicket from "../../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";
import "jqueryui-timepicker-addon";

class AppointmentsCreate extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeTitle(this.i18n("Ticket_ViewAppointment")());
        this.bladeSize = BladeSize.Small;
        this.valueParams = (params.items)?params.items:null ; 
        this.id = this.valueParams.id;
        this.currentStatus =  this.valueParams.status ;  
        this.companyId  = this.ensureNonObservable(params.companyId, -1);
        var bb = (Object.keys(params).length > 0)?this.getTimeByParams(this.valueParams):null ; 

        this.status = ko.observableArray([]);
        this.appoiDate = ko.observable();
        this.appoidesc  = ko.observable(this.valueParams.appointmentDescription);
        

        this.isEnable = ko.observable(false);
        this.contract = ko.observableArray([]);
        this.mobile = ko.observable();
        this.email = ko.observable();
        this.formatSet = "dd/mm/yyyy";
        this.time = ko.observable(bb);
        this.minDate = ko.observable(0);
        this.appDatetime = ko.observable(this.valueParams.appointDate);
        this.province = ko.observableArray([]);
        this.city = ko.observableArray([]);
        this.town = ko.observableArray([]);
        this.techteam = ko.observable([]);
        //this.MaintenanceType = ko.observable([]);
        this.selectedContract = ko.observable();
        this.selectedProvince= ko.observable();
        this.selectedCity = ko.observable();
        this.selectedTown = ko.observable();
        //this.selectedMaintenanceType = ko.observable();
        this.selectedTechTeam = ko.observable();
        this.selectedStatus = ko.observable();
        this.isVisible = ko.observable();
        this.pendingCause = ko.observableArray([]);
        this.pendingSubCause = ko.observableArray([]);
        this.selectedPending = ko.observable();
        this.selectedSubPend = ko.observable();
        this.pendingRemark = ko.observable();

        this.selectedContract.subscribe(val =>{
            
            let phone = (typeof val == 'undefined') || (val.phone==null) ? null : val.phone;
            let mail = (typeof val == 'undefined') || (val.email==null) ? null : val.email;
            this.mobile(phone);
            this.email(mail);;
        });

        this.selectedContract.subscribe(val =>{
            let phone = (typeof val == 'undefined') || (val.phone==null) ? null : val.phone;
            let mail = (typeof val == 'undefined') || (val.email==null) ? null : val.email;
            this.mobile(phone);
            this.email(mail);;
        });

        this.isVisible  = ko.pureComputed(()=>{
            return this.selectedStatus() && this.selectedStatus().value == 3;
        });

        if(this.isVisible){
            this.webRequestTicket.getPendingCause().done((pending)=>{
                this.pendingCause(pending);
            });

            this.selectedPending.subscribe(value => {
                if(value){
                    let pendingId = (typeof this.selectedPending() == "undefined")? null : value.id
                    this.webRequestTicket.getPendingSubCause(pendingId).done((reponse) => {
                        this.pendingSubCause(reponse);
                    });
                }
                else{
                    this.pendingSubCause.replaceAll([]);
                } 
            });
        }

    }

    get webRequestEnumResource() {
        return webRequestEnumResource.getInstance();
    }

    get webRequestCompany() {
        return webRequestCompany.getInstance();
    }

    get webRequestProvince() {
        return webRequestProvince.getInstance();
    }

    get webRequestCity() {
        return webRequestCity.getInstance();
    }

    get webRequestTown() {
        return webRequestTown.getInstance();
    }

    get webRequestUser() {
        return webRequestUser.getInstance();
    }

    get webRequestTicket() {
        return webRequestTicket.getInstance();
    }

   

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();
        var d0 = $.Deferred();

        var ContactsCom = [
            "Company.Contacts"
        ];
        var EnumUserGroup = { 
            groupIds:[Enums.UserGroup.Technician]        
        };
        var EnumsStatusAppoi = {
            types:[Enums.ModelData.EnumResourceType.TicketAppointmentStatus]
        };
        var EnumsMaintenanceType = {
        };
       
        var webRequestEnumResource = this.webRequestEnumResource.listEnumResource(EnumsStatusAppoi);
        var webRequestCompany = this.webRequestCompany.getCompany(this.companyId,ContactsCom);
        var w = this.webRequestUser.listUserSummary(EnumUserGroup);
        //var w2 = this.webRequestTicket.listTicketMaintenanceType({});

        $.when(webRequestEnumResource,webRequestCompany,w).done((responseEnum,responseCompany,g) =>{
            g.items.forEach((v)=>{
                v.displayName = (v.fullName == null || v.fullName.trim() == "" ) ? v.username : v.fullName+" ("+v.mobileNo+") "
            });
              
            this.status(responseEnum.items);
            this.techteam(g.items);
            //this.MaintenanceType(tick.items);
            var objContract = [];

            var i = 0 ;
            responseCompany.contacts.forEach((v)=>{
                if(v.name!=null){
                    objContract.push(responseCompany.contacts[i]);
                }
                i++;
            });
    
            this.contract(objContract);
            this.selectedContract(ScreenHelper.findOptionByProperty(this.contract, "id",this.valueParams.companyContactId));
            this.selectedStatus(ScreenHelper.findOptionByProperty(this.status,"value",this.currentStatus));
            this.selectedTechTeam(ScreenHelper.findOptionByProperty(this.techteam,"id",this.valueParams.technicianId));
            //this.selectedMaintenanceType(ScreenHelper.findOptionByProperty(this.MaintenanceType,"id",this.valueParams.maintenanceTypeId));
        });

            var d1 = this.webRequestProvince.listProvince({"countryIds": [1]});
            var d2 = this.valueParams.provinceId ? this.webRequestCity.listCity({
                provinceIds: [this.valueParams.provinceId]
            }) : null;
            var d3 = this.valueParams.cityId ? this.webRequestTown.listTown({
                cityIds: [this.valueParams.cityId]
            }) : null;

            $.when(d1, d2, d3).done((r1, r2, r3) => {

                this.province(r1["items"]);

                if (r1) {
                    var provinceOptions = r1["items"];
                    this.province(provinceOptions);

                    if (this.valueParams.provinceId) {
                        this.selectedProvince(ScreenHelper.findOptionByProperty(this.province,"id",this.valueParams.provinceId));
                    }
                }

                if (r2) {
                    var cities = r2["items"];
                    this.city(cities);
                    if (this.valueParams.cityName) {
                        this.selectedCity(ScreenHelper.findOptionByProperty(this.city,"name",this.valueParams.cityName));
                    }
                }

                if (r3) {
                    var towns = r3["items"];
                    this.town(towns);

                    if (this.valueParams.townId) {
                        this.selectedTown(ScreenHelper.findOptionByProperty(this.town,"id",this.valueParams.townId));
                    }
                }
                d0.resolve();
            }).fail((e) => {
                d0.reject(e);
                this.handleError(e);
            }).always(() => {

            });

            $.when(d0).done(() => {

                this._changeProvinceSubscribe = this.selectedProvince.subscribe((province) => {
                    if (province) {
                        this.webRequestCity.listCity({
                            provinceIds: [province.id]
                        }).done((response) => {
                            var cities = response["items"];
                            this.city.replaceAll(cities);

                        }).fail((e) => {
                            this.handleError(e);
                        }).always(() => {

                        });
                    } else {
                        this.city.replaceAll([]);
                    }
                });

                this._changeCitySubscribe = this.selectedCity.subscribe((city) => {

                    if (city) {
                 
                        this.isBusy(true);
                        this.webRequestTown.listTown({
                            cityIds: [city.id]
                        }).done((response) => {
                            var towns = response["items"];
                            this.town.replaceAll(towns);
                        }).fail((e) => {
                            this.handleError(e);
                        }).always(() => {
                            this.isBusy(false);
                        });
                    } else {
                        this.town.replaceAll([]);
                    }
                });
           
                dfd.resolve();
            }).fail((e) => {
                dfd.reject(e);
                this.handleError(e);
            }).always(() => {

            });
            return dfd;

    }

    setupExtend(){

      
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {

       actions.push(this.createActionCancel());
       
    }

    getTimeByParams(params){
        let a = params.appointDate.getHours()<10?'0'+params.appointDate.getHours():params.appointDate.getHours() ; 
        let b = params.appointDate.getMinutes()<10?'0'+params.appointDate.getMinutes():params.appointDate.getMinutes();
        var aa = [a,b];

        return aa.join(":");
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
       
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        
    }
    
}

export default {
viewModel: ScreenBase.createFactory(AppointmentsCreate),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./create.html";
import { Enums } from "../../../../../../app/frameworks/constant/apiConstant";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import DefaultSorting from "../../../../../../app/frameworks/constant/defaultSorting";
import webRequestEnumResource from "../../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import webRequestProvince from "../../../../../../app/frameworks/data/apicore/webRequestProvince";
import webRequestUser from "../../../../../../app/frameworks/data/apicore/webRequestUser";
import webRequestTicket from "../../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";
import ScreenBase from "../../../../screenbase";

class AppointmentsCreate extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Ticket_CreateAppoiment")());
        this.bladeSize = BladeSize.XMedium;
        this.vehicle = ko.observableArray([]);
        if(params.Ismultiple){
            this.vehicle((Object.keys(params.items).length > 0) ? params.items : []);
            this.ticketId = params.items.map(x=>x.ticketId);
            var companies = params.items.map(x=>x.companyId);
            this.companyId = companies[0];
            // from Ticket-Search
            this.filterState = params.filterState ? params.filterState : {};
        }
        else{
            this.vehicle([]);
            this.ticketId = params.ticketId;
            this.companyId = params.companyId; 
        }

        this.status = ko.observableArray([]);
        this.appoiDate = ko.observable();
        this.appoidesc  = ko.observable();
        this.isVisibleTable = ko.observable(  (params.Ismultiple)?true:false  );
        

        this.isEnable = ko.observable(false);
        this.contract = ko.observableArray([]);
        this.mobile = ko.observable();
        this.email = ko.observable();
        this.formatSet = "dd/MM/yyyy";
        this.time = ko.observable();
        this.minDate = ko.observable(0);
        this.appDatetime = ko.observable();
        this.province = ko.observableArray([]);
        this.techteam = ko.observable([]);
        this.MaintenanceType = ko.observable([]);
        this.selectedProvince= ko.observable();
        this.selectedMaintenanceType = ko.observable();
        this.selectedTechTeam = ko.observable();
        this.selectedStatus = ko.observable();
        this.isSendEmail = ko.observable();
        
        //Send Mail
        this.mailtemplate = ko.observableArray([]);
        this.selectedtemplate = ko.observable();
        this.mailTo = ko.observable();
        this.mailcc = ko.observable("nostralogistics@cdg.co.th");
        this.subject = ko.observable();
        this.textBody = ko.observable();
        this.toolSendMail = ko.observableArray([]);
        //////

        this.selectedtemplate.subscribe(val => {
            if(val){
                let idTickets = this.ticketId ? this.ticketId.hasOwnProperty('length') ? this.ticketId : [this.ticketId]
                                : [];

                let formatAppointmentDate = null;
                if(this.appDatetime()){
                    let timeAppoint = this.time() ? this.time() : '00:00';
                    formatAppointmentDate  = $.datepicker.formatDate("yy-mm-dd", new Date(this.appDatetime())) + " " + timeAppoint + ":00";
                }
                let filterSentMail = {
                    ticketIds:idTickets,
                    id:val.id,
                    appointmentDate: formatAppointmentDate,
                    appointmentDescription: this.appoidesc(),
                    provinceName:this.selectedProvince() ? this.selectedProvince().name : null,
                    technicianTeam:this.selectedTechTeam() ? this.selectedTechTeam().displayName : null,
                    ticketAppointmentStatus: this.selectedStatus() ? this.selectedStatus().value : null
                }

                this.webRequestTicket.listTicketMailSendMail(filterSentMail).done((response)=>{
                    //this.mailTo(response.to);
                    //this.mailcc(response.cc);
                    this.subject(response.subject);
                    this.textBody(response.body);
                });
            }
            else{
                //this.mailTo(null);
                //this.mailcc(null);
                this.subject(null);
                this.textBody(null);
            }
            
        });

        this.selectedTechTeam.subscribe((val) => {
            let mailTo = val ? val.email : null;
            this.mailTo(mailTo);
        })
    }

    get webRequestEnumResource() {
        return webRequestEnumResource.getInstance();
    }

    get webRequestProvince() {
        return webRequestProvince.getInstance();
    }

    get webRequestUser() {
        return webRequestUser.getInstance();
    }

    get webRequestTicket() {
        return webRequestTicket.getInstance();
    }


    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        var EnumUserGroup = { 
            groupIds: [Enums.UserGroup.Technician],
            sortingColumns: DefaultSorting.User,
            enable:true
        };
        var EnumsStatusAppoi = {
            types:[Enums.ModelData.EnumResourceType.TicketAppointmentStatus]
        };

        var webRequestEnumResource = this.webRequestEnumResource.listEnumResource(EnumsStatusAppoi);
        var webRequestProvince = this.webRequestProvince.listProvince({"countryIds": [1]});
        var w = this.webRequestUser.listUserSummary(EnumUserGroup);
        var w2 = this.webRequestTicket.listTicketMaintenanceType({});
        var w3 = this.webRequestTicket.listTicketMailManagement({status: 1});
            
        $.when(webRequestEnumResource,webRequestProvince,w,w2,w3)
        .done((responseEnum,reponseProvince,g,tick,mailTempalte) =>{
            reponseProvince.items.sort(function(a,b){
                if(a.name>b.name) return 1;
                if(a.name<b.name) return -1;
                return 0 ;
            });

            g.items.forEach((v)=>{
                v.displayName = (v.fullName == null || v.fullName.trim() == "" || v.mobileNo == null ) ? v.username : v.fullName+" ("+v.mobileNo+") "
            });

            this.status(responseEnum.items);
            this.province(reponseProvince.items);
            this.techteam(g.items);
            this.MaintenanceType(tick.items);
            this.mailtemplate(mailTempalte.items);
            
            this.selectedStatus(responseEnum.items[1]);

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    setupExtend(){
        this.appDatetime.extend({trackChange:true});
        this.appoidesc.extend({trackChange:true});
        this.selectedProvince.extend({trackChange:true});
        this.selectedTechTeam.extend({trackChange:true});
        this.selectedMaintenanceType.extend({trackChange:true});
        this.time.extend({trackChange:true});
        
       
        this.appDatetime.extend({required: true});
        this.time.extend({required:true});
        this.selectedProvince.extend({required: true});
        this.selectedTechTeam.extend({required: true});

        // Send Mail
        var self = this;
        this.selectedtemplate.extend({
            trackChange: true,
            required : {
                onlyIf : function(){
                    return self.isSendEmail();
                }
            }
        });
        this.mailTo.extend({
            trackChange: true,
            required : {
                onlyIf : function(){
                    return self.isSendEmail();
                }
            },
            validation: {
                validator: function (val, validate) {
                    if (!validate) { return true; }
                    var isValid = true;
                    if (!ko.validation.utils.isEmptyVal(val)) {
                        // use the required: true property if you don't want to accept empty values
                        var values = val.split(',');
                        $(values).each(function (index) {
                            isValid = ko.validation.rules['email'].validator($.trim(this), validate);
                            return isValid; // short circuit each loop if invalid
                        });
                    }
                    return isValid;
                },
                message: this.i18n("I012")()
            }
        });

        this.mailcc.extend({
            trackChange: true,
            validation: {
                validator: function (val, validate) {
                    if (!validate) { return true; }
                    var isValid = true;
                    if (!ko.validation.utils.isEmptyVal(val)) {
                        // use the required: true property if you don't want to accept empty values
                        var values = val.split(',');
                        $(values).each(function (index) {
                            isValid = ko.validation.rules['email'].validator($.trim(this), validate);
                            return isValid; // short circuit each loop if invalid
                        });
                    }
                    return isValid;
                },
                message: this.i18n("I012")()
            }
        });

        this.subject.extend({
            trackChange: true,
            required : {
                onlyIf : function(){
                    return self.isSendEmail();
                }
            }
        });
        this.textBody.extend({
            trackChange: true,
            required : {
                onlyIf : function(){
                    return self.isSendEmail();
                }
            }
        });
        /////
        this.validationModel = ko.validatedObservable({
            Datetime:this.appDatetime,
            Description:this.appoidesc,
            Province:this.selectedProvince,
            TechTeam:this.selectedTechTeam,
            MaintenanceType:this.selectedMaintenanceType,
            time:this.time,
            selectedtemplate: this.selectedtemplate,
            mailTo: this.mailTo,
            subject: this.subject,
            textBody: this.textBody,
            mailcc: this.mailcc
        });

    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {

       actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
       actions.push(this.createActionCancel());
       
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
       
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if(sender.id==="actSave"){

            if(!this.validationModel.isValid()){
                this.validationModel.errors.showAllMessages();
                return ;
            }

            this.isBusy(true);
            var formatDate  = $.datepicker.formatDate("yy-mm-dd", new Date(this.appDatetime())) + " " + this.time() + ":00";
            var filter = {
                status:this.selectedStatus().value,
                appointDate:formatDate,
                appointmentDescription:this.appoidesc(),
                provinceId:this.selectedProvince().id,
                ticketIds:this.ticketId,
                technicianId:this.selectedTechTeam().id,
                maintenanceTypeId :this.selectedMaintenanceType()==null?null:this.selectedMaintenanceType().id
            }

            if(this.isSendEmail()){
                filter.sendMail = true;
                filter.templateName = this.selectedtemplate().name,
                filter.to =  this.mailTo(),
                filter.cc = this.mailcc(),
                filter.subject = this.subject(),
                filter.body = this.textBody()
            }

            this.webRequestTicket.createTicketAppointment(filter).done((response) => {
                this.close(true);
                if(this.isVisibleTable()){
                    this.publishMessage("bo-ticket-edit-multiple-service-changed");
                    this.publishMessage("bo-ticket-manage-ticket-search",this.filterState);
                }
                else{
                    this.publishMessage("update-items-appointment");
                }
            }).fail((e)=>{
                this.handleError(e);
            }).always(() => {
                this.isBusy(false);
            });     
        }
     
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        
    }
    
}

export default {
viewModel: ScreenBase.createFactory(AppointmentsCreate),
    template: templateMarkup
};
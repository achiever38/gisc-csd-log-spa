import ko from "knockout";
import templateMarkup from "text!./export-with-criteria.html";
import { Enums } from "../../../../../../app/frameworks/constant/apiConstant";
import Utility from "../../../../../../app/frameworks/core/utility";
import WebRequestCompany from "../../../../../../app/frameworks/data/apicore/webRequestCompany";
import WebRequestEnumResource from "../../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import ScreenBase from "../../../../screenbase";
import WebRequestTicket from "../../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";
import moment from "moment";
import ScreenHelper from "../../../../screenhelper";

class ExportTicketWithCriteriaScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Export with criteria")());

        this.listCompany = ko.observableArray([]);
        this.selectedCompany = ko.observable();
        this.listTicketStatus = ko.observableArray([]);
        this.selectedTicketStatus = ko.observable();
        this.startDate = ko.observable(new Date());
        this.endDate = ko.observable(new Date());

        //datetime
        this.maxStartDate = new Date();
        this.formatSet = "dd/MM/yyyy";
        this.maxStartDate = ko.observable(new Date());
        this.minStartDate = ko.observable(new Date());


        this.periodDay = ko.pureComputed(() => {
            // can't select date more than current date
            //let addDay = 30;
            var isDate = new Date();//Utility.addDays(this.startDate(), addDay);
            let currentDate = parseInt(new Date() / (1000 * 60 * 60 * 24));
            let endDate = parseInt(isDate / (1000 * 60 * 60 * 24)) + 1;
            var calDay = endDate - currentDate;
            if (calDay > 0) {
                isDate = new Date();
            }

            // set date when clear start date and end date
            if (this.startDate() == null) {
                // set end date when clear start date
                if (this.endDate() == null) {
                    isDate = new Date();
                } else {
                    isDate = this.endDate();
                }
                this.maxStartDate(isDate);
                this.minStartDate(isDate);
            } else {
                this.minStartDate(this.startDate())
            }

            return isDate;
        });
    }

    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    get webRequestTicket() {
        return WebRequestTicket.getInstance();
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();
        var TicketStatus = {
            types: [Enums.ModelData.EnumResourceType.TicketStatus],
            values: []
        };

        var w1 = this.webRequestEnumResource.listEnumResource(TicketStatus);
        var w2 = this.webRequestCompany.listCompanySummary({});

        $.when(w1, w2).done((r1, r2) => {
            this.listTicketStatus(r1["items"]);
            this.listCompany(r2["items"]);

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    setupExtend() {
        this.startDate.extend({ required: true });
        this.endDate.extend({ required: true});

        this.validationModel = ko.validatedObservable({
            startDate: this.startDate,
            endDate: this.endDate
        });

    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdClear", this.i18n("Common_Clear")(), "svg-cmd-clear"));
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actExport", this.i18n("Common_Export")()));
        actions.push(this.createActionCancel());
    }

    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actExport") {

            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);
            
            let filter = {
                status : this.selectedTicketStatus() ? this.selectedTicketStatus().value : null,
                companyId: this.selectedCompany() ? this.selectedCompany().id : null,
                fromDate: moment(this.startDate()).format("YYYY-MM-DD")+" "+"00:00:00",
                toDate:moment(this.endDate()).format("YYYY-MM-DD")+" "+"23:59:59"
            };

            this.webRequestTicket.exportTicket(filter).done((r) => {
                //console.log(r);
                ScreenHelper.downloadFile(r.fileUrl);
            }).fail((e) => {
                this.handleError(e);
            }).always(() => {
                this.isBusy(false);
            });

        }
    }

    onCommandClick(sender) {
        if (sender.id == "cmdClear") {
            this.selectedCompany(null);
            this.selectedTicketStatus(null);
            this.startDate(null);
            this.endDate(null);
        }
    }

}


export default {
    viewModel: ScreenBase.createFactory(ExportTicketWithCriteriaScreen),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./sendmail.html";
import ScreenBase from "../../../../screenbase";
import ScreenHelper from "../../../../screenhelper";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import { Enums, EntityAssociation } from "../../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../../app/frameworks/core/utility";
import webRequestTicket from "../../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";


class Sendmail extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.mode = this.ensureNonObservable(params.mode);
        this.labelMessage = ko.observable();

        this.IsVisible  = ko.observable(true);
        this.IsVisibleMailDetail = ko.observable(false);
        this.bladeSize = BladeSize.Medium;
        this.bladeIsMaximized = true;
        this.mailtemplate = ko.observableArray([]);
        this.selectedtemplate = ko.observable();
        this.mailTo = ko.observable();
        this.mailcc = ko.observable("nostralogistics@cdg.co.th");
        this.subject = ko.observable();
        this.textSendMail = ko.observable();
        this.textBody = ko.observable();
        this.toolSendMail = ko.observableArray([]);

        if(!params.IsTicketDetail){

            let itemTickets = [];
            if(params.IssendOneTicket){
                this.bladeTitle(this.i18n("Send Email")());
                itemTickets = params ? params.items : [];
            }
            else{
                this.bladeTitle(this.i18n("Ticket_SendMailMultipleTicket")());
                itemTickets = (Object.keys(params).length > 0) ? params : [];
            }
            
            this.ticketNo = itemTickets.map(x=>x.ticketId);

            this.selectedtemplate.subscribe(val => {
                if(val){
                    if(Object.keys(this.ticketNo).length > 0){
                        this.webRequestTicket.listTicketMailSendMail({ticketIds:this.ticketNo, id:val.id}).done((response)=>{
                            //this.mailTo(response.to);
                            //this.mailcc(response.cc);
                            this.subject(response.subject);
                            this.textSendMail(response.body);
                        });
                    }
                }
                else{
                    this.subject(null);
                    this.textSendMail(null);
                }
                
            });
        }
        else{
            this.bladeTitle(this.i18n("Email Detail")());
            var items = params["items"]

            this.webRequestTicket.getLogTicketMail(items.id).done((response)=>{
                this.mailTo(response.sentTo);
                this.mailcc(response.cc);
                this.subject(response.subject);
                this.textBody(response.body);

            });
            this.ticketNo = 0;
            this.IsVisible(false);
            this.IsVisibleMailDetail(true);
        }
       

    }



    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        this.webRequestTicket.listTicketMailManagement({status: 1}).done((response)=>{
            this.mailtemplate(response.items);
        });


      }

    get webRequestTicket() {
        return webRequestTicket.getInstance();
    }

    setupExtend(){

        if(!this.IsVisibleMailDetail()){
            this.subject.extend({trackChange:true});
            this.mailTo.extend({
                validation: {
                    validator: function (val, validate) {
                        if (!validate) { return true; }
                        var isValid = true;
                        if (!ko.validation.utils.isEmptyVal(val)) {
                            // use the required: true property if you don't want to accept empty values
                            var values = val.split(',');
                            $(values).each(function (index) {
                                isValid = ko.validation.rules['email'].validator($.trim(this), validate);
                                return isValid; // short circuit each loop if invalid
                            });
                        }
                        return isValid;
                    },
                    message: this.i18n("I012")()
                },
                trackChange:true
            });

            this.mailcc.extend({
                validation: {
                    validator: function (val, validate) {
                        if (!validate) { return true; }
                        var isValid = true;
                        if (!ko.validation.utils.isEmptyVal(val)) {
                            // use the required: true property if you don't want to accept empty values
                            var values = val.split(',');
                            $(values).each(function (index) {
                                isValid = ko.validation.rules['email'].validator($.trim(this), validate);
                                return isValid; // short circuit each loop if invalid
                            });
                        }
                        return isValid;
                    },
                    message: this.i18n("I012")()
                },
                trackChange:true
            });

            this.selectedtemplate.extend({required:true});
            this.subject.extend({required:true});
            this.mailTo.extend({required:true});

            this.validationModel = ko.validatedObservable({
                selectedtemplate: this.selectedtemplate,
                subject: this.subject,
                mailTo: this.mailTo,
            });
        }

    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        if(this.IsVisibleMailDetail()){
            actions.push(this.createActionCancel());
        }
        else{
            var enable = (Object.keys(this.ticketNo).length > 0) ? true : false;
            actions.push(this.createAction("actSendMail", this.i18n("Ticket_Sendmail")(), "default", true, enable));
            actions.push(this.createActionCancel()); 
            
        }
       
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if(sender.id === "actSendMail"){
            if (!this.validationModel.isValid()) {
               this.validationModel.errors.showAllMessages();
               return;
            }
            this.isBusy(true);
            var sendMailFilter = {
                id: this.selectedtemplate().id,
                ticketIds: this.ticketNo,
                to: this.mailTo(),
                cc: this.mailcc(),
                subject: this.subject(),
                body: this.textSendMail()
            }

            this.webRequestTicket.TicketSend(sendMailFilter).done((response)=>{
                this.close(true);
                this.publishMessage("bo-ticket-edit-multiple-service-changed");
            }).fail((e) => {
                this.handleError(e);
            }).always(()=>{
                this.isBusy(false);
            });
            
        }
        


    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {

    }
    
}

export default {
viewModel: ScreenBase.createFactory(Sendmail),
    template: templateMarkup
};
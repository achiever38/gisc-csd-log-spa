﻿import ko from "knockout";
import templateMarkup from "text!./fleet-service.html";
import ScreenBase from "../../../../screenbase";
import ScreenHelper from "../../../../screenhelper";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
import * as Screen from "../../../../../../app/frameworks/constant/screen";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestFleetService from "../../../../../../app/frameworks/data/apitrackingcore/webRequestFleetService";
import WebRequestOperatorPackage from "../../../../../../app/frameworks/data/apitrackingcore/webRequestOperatorPackage";
import WebRequestBusinessUnit from "../../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestBox from "../../../../../../app/frameworks/data/apitrackingcore/webRequestBox";
import WebRequestVehicle from "../../../../../../app/frameworks/data/apitrackingcore/webRequestVehicle";
import webRequestTicket from "../../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";
import WebRequestVehicleModel from "../../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleModel";
import {Constants} from "../../../../../../app/frameworks/constant/apiConstant";
import {BusinessUnitFilter, PromotionFilter, BoxFilter, VehicleFilter} from "./infos";
import Utility from "../../../../../../app/frameworks/core/utility";
import DefaultSorting from "../../../../../../app/frameworks/constant/defaultSorting";

class FleetServiceTicketScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Medium;
        this.mode = (params.fleetId) ? Screen.SCREEN_MODE_UPDATE : Screen.SCREEN_MODE_CREATE;
        this.ticketId = params.ticketId;
        this.companyId = params.companyId;
        this.mobile = params.mobile;
        this._businessUnitId = params.businessUnitId;
        this._boxId = params.boxId;
        this._vehicleId = params.licenseId;
        
        this.newLicenseVisible = ko.observable();

        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.newLicenseVisible(false);
                this.bladeTitle(this.i18n("Assets_CreateFleetService")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.newLicenseVisible(true);
                this.bladeTitle(this.i18n("Assets_UpdateFleetSetvice")());
                break;
        }

        this.fleetServiceId = this.ensureNonObservable(params.fleetId, -1);
        this.enableOptions = ScreenHelper.createYesNoObservableArray();
        this.promotions = ko.observableArray([]);
        this.businessUnits = ko.observableArray([]);
        this.boxes = ko.observableArray([]);
        this.vehicles = ko.observableArray([]);
        this.temp_mobile  = ko.observable();
        this.mobile = ko.observable();
        this.mobileArray = ko.observableArray([]);
        this.selectedPromotion = ko.observable();
        this.selectedBusinessUnit = ko.observable(null);
        this.selectedBox = ko.observable();
        this.selectedVehicle = ko.observable();
        this.selectedMobile = ko.observable();
        this.enableDltInterface = ko.observable(ScreenHelper.findOptionByProperty(this.enableOptions, "value", false)); 
        this.newLicense = ko.observable();

        this.vehicleModelOptions = ko.observableArray([]);
        this.selectedVehicleModel = ko.observable();
        this.chassisNo = ko.observable();
        this.licensePlate = ko.observable();
        this.licenseProvince = ko.observable();
        this.licenseCountry = ko.observable();
        this.isDLT = ko.observable(false);
        this.isNewVehicle = ko.observable(true);
        this.selectedVehicleOld = ko.observable(null);
        this.selectedVehicleId = ko.observable(this._vehicleId);

        this._selectedBusinessUnitRef = this.selectedBusinessUnit.ignorePokeSubscribe((value)=>{
            this.selectedBox(null);
            this.selectedVehicle(null);
            var selectedBusinessUnit = this.selectedBusinessUnit();
            if(selectedBusinessUnit){
                this.isBusy(true);

                var boxFilter = null;
                var vehicleFilter = null;

                switch (this.mode) {
                    case Screen.SCREEN_MODE_CREATE:
                        boxFilter = new BoxFilter(this.companyId, selectedBusinessUnit);
                        vehicleFilter = new VehicleFilter(this.companyId, selectedBusinessUnit);
                        break;
                    case Screen.SCREEN_MODE_UPDATE:
                        if(this._businessUnitId == selectedBusinessUnit){
                            boxFilter = new BoxFilter(this.companyId, selectedBusinessUnit, this._boxId);
                            vehicleFilter = new VehicleFilter(this.companyId, selectedBusinessUnit, this._vehicleId);
                        }else{
                            boxFilter = new BoxFilter(this.companyId, selectedBusinessUnit);
                            vehicleFilter = new VehicleFilter(this.companyId, selectedBusinessUnit);
                        }
                        break;
                }


                var d1 = this.webRequestBox.listBoxSummary(boxFilter);
                var d2 = this.webRequestVehicle.listVehicleSummary(vehicleFilter);
                $.when(d1, d2).done((r1, r2) => {
                    this.boxes(r1["items"]);
                    this.vehicles(r2["items"]);

                    this.isBusy(false);
                }).fail((e) => {
                    this.handleError(e);
                    this.isBusy(false);
                });
            }else{
                this.boxes([]);
                this.vehicles([]);
            }
        });

        //this.selectedPromotion.subscribe((val)=>{
        //    let id = (typeof val == "undefined") ? null : val.id;
        //    this.webRequestTicket.listSimWithFreetId({CompanyId:this.companyId,BoxId:this._boxId, OperatorId:id}).done((response)=>{
        //        this.mobileArray(response.items);
        //        this.selectedMobile(ScreenHelper.findOptionByProperty(this.mobileArray, "mobileNo", this.mobile));
        //    });
        //});
        this.enableDltInterface.subscribe((obj)=>{
            this.isDLT(obj.value);
        });

        this.isVehicle = ko.pureComputed(() => {
            return (this.newLicense() || this.mode == Screen.SCREEN_MODE_CREATE) ? false : true;
        });

        this.isNewVehicle = ko.pureComputed(() => {
            var res = false;
            if(this.mode == Screen.SCREEN_MODE_CREATE){
                res = false;
            }else if(this.selectedVehicleOld() == this.selectedVehicle()){
                res = true
            }else{
                res = false;
            }
            return res;
        });


        this.selectedVehicle.subscribe((val)=> {
            
            if(val){
                var dfd = $.Deferred();
                this.selectedVehicleId(val.id);
                $.when(this.getVehicleDetail()).done((res) => {
                    dfd.resolve();
                }).fail((e) => {
                    dfd.reject(e);
                });
                return dfd;
            }
        });

        

    }
    /**
     * Get WebRequest specific for Fleet Service module in Web API access.
     * @readonly
     */
    get webRequestFleetService() {
        return WebRequestFleetService.getInstance();
    }
    /**
     * Get WebRequest specific for Operator Package module in Web API access.
     * @readonly
     */
    get webRequestOperatorPackage() {
        return WebRequestOperatorPackage.getInstance();
    }
    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    /**
     * Get WebRequest specific for Box module in Web API access.
     * @readonly
     */
    get webRequestBox() {
        return WebRequestBox.getInstance();
    }
    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebRequestVehicle.getInstance();
    }

    get webRequestVehicleModel() {
        return WebRequestVehicleModel.getInstance();
    }

    get webRequestTicket(){
        return webRequestTicket.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */

    getVehicleDetail() {
        var dfd = $.Deferred();
        var vehicleFilter = new VehicleFilter(this.companyId, this._businessUnitId, this.selectedVehicleId());
        var d1 = this.webRequestVehicle.listVehicle(vehicleFilter);
        $.when(d1).done((r1) => {
            //var objVehicles = r1.items;
            var objVehicles = _.filter(r1.items,{'id':this.selectedVehicleId()})[0]; //filter เพื่อเอา value default ของ vehicleId ที่เลือก  ต้อง filter เพราะ listVehicle return มาหลายตัว เลยต้องfilter
            if(objVehicles){
                this.selectedVehicleModel(ScreenHelper.findOptionByProperty(this.vehicleModelOptions, "id",objVehicles.modelId));
                //this.selectedBusinessUnit(ScreenHelper.findOptionByProperty(this.businessUnits, "id", this.selectedVehicleId()));
                //this.selectedVehicle(ScreenHelper.findOptionByProperty(this.vehicles, "id", objVehicles.id));
                this.chassisNo(objVehicles.chassisNo);
                this.licensePlate(objVehicles.license.licensePlate);
                this.licenseProvince(objVehicles.license.licenseProvince);
                this.licenseCountry(objVehicles.license.licenseCountry);
            }
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;
    }

    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;
        
        var dfd = $.Deferred();
        var businessUnitFilter = new BusinessUnitFilter(this.companyId);
        var promotionFilter = new PromotionFilter(this.companyId);
        var vehicleModelFilter = {
            enable: true,
            sortingColumns: DefaultSorting.VehicleModelByBrandModelCode
        }

        var a1 = (this.mode === Screen.SCREEN_MODE_UPDATE) ? this.webRequestFleetService.getFleetService(this.fleetServiceId) : null;
        var a2 = this.webRequestOperatorPackage.listOperatorPackage(promotionFilter);
        var a3 = this.webRequestBusinessUnit.listBusinessUnitSummary(businessUnitFilter);
        var a4 = this.webRequestVehicleModel.listVehicleModelSummary(vehicleModelFilter);
        $.when(a1, a2, a3, a4).done((r1, r2, r3, r4) => {
            Utility.applyFormattedPropertyToCollection(promotions, "displayText", "{0} – {1}", "operator", "promotion")
            Utility.applyFormattedPropertyToCollection(r4["items"], "displayText", "{0} - {1} ({2})", "brand", "model", "code");
            var fleetService = r1;
            var promotions = r2["items"];
            this.promotions(promotions);
            this.businessUnits(r3["items"]);
            this.vehicleModelOptions(r4["items"]);
            
            if (fleetService && this.mode === Screen.SCREEN_MODE_UPDATE) {
                this.temp_mobile(fleetService.mobile);
                this.selectedPromotion(ScreenHelper.findOptionByProperty(this.promotions, "id", fleetService.operatorId));
                this.selectedBusinessUnit.poke(fleetService.businessUnitId.toString());
                this.enableDltInterface(ScreenHelper.findOptionByProperty(this.enableOptions, "value", fleetService.enableDltInterface));
                //this.selectedVehicleModel(ScreenHelper.findOptionByProperty(this.vehicleModelOptions, "id", fleetService.vehicleModel));

                this.chassisNo(fleetService.chassisNo);
                this.licensePlate(fleetService.licensePlate);
                this.licenseProvince(fleetService.licenseProvince);
                this.licenseCountry(fleetService.licenseCountry);

                this.selectedMobile(fleetService.mobile);
                // var businessUnitId = fleetService.businessUnitId.toString();
                // var boxId = fleetService.boxId;
                // var vehicleId = fleetService.vehicleId;

                // this.webRequestTicket.listSimWithFreetId(fleetService.id).done((aaa)=>{
                    
                   
                // }); 

                var boxFilter = new BoxFilter(this.companyId, fleetService.businessUnitId, fleetService.boxId);
                var vehicleFilter = new VehicleFilter(this.companyId, fleetService.businessUnitId, fleetService.vehicleId);
                
                var a4 = this.webRequestBox.listBoxSummary(boxFilter);
                var a5 = this.webRequestVehicle.listVehicleSummary(vehicleFilter);
                var a6 = [];//this.webRequestTicket.listSimWithFreetId({BoxId:this._boxId, OperatorId:fleetService.operatorId})
                $.when(a4, a5,a6).done((r4, r5,r6) => {
                    this.boxes(r4["items"]);
                    this.vehicles(r5["items"]);
                    //this.mobileArray(r6["items"]);

                    this.selectedBox(ScreenHelper.findOptionByProperty(this.boxes, "id", this._boxId));
                    this.selectedVehicle(ScreenHelper.findOptionByProperty(this.vehicles, "id", this._vehicleId));
                    
                    this.selectedVehicleOld(this.selectedVehicle());
                    //this.selectedMobile(ScreenHelper.findOptionByProperty(this.mobileArray, "mobileNo", fleetService.mobile));
                    dfd.resolve();
                }).fail((e) => {
                    dfd.reject(e);
                });

            }else{
                //get default vehicle detail
                $.when(this.getVehicleDetail()).done((res) => {
                    dfd.resolve();
                }).fail((e) => {
                    dfd.reject(e);
                });
            }   
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        var self = this;
        //this.selectedMobile.extend({
        //    trackChange: true
        //});
        //this.selectedPromotion.extend({
        //    trackChange: true
        //});
        this.selectedBusinessUnit.extend({
            trackChange: true
        });
        this.selectedBox.extend({
            trackChange: true
        });
        this.selectedVehicle.extend({
            trackChange: true
        });
        this.enableDltInterface.extend({
            trackChange: true
        });

        this.selectedVehicleModel.extend({
            trackChange: true
        });
        this.chassisNo.extend({
            trackChange: true
        });
        this.licenseProvince.extend({
            trackChange: true
        });
        this.licenseCountry.extend({
            trackChange: true
        });

        // Manual setup validation rules
        //this.selectedPromotion.extend({
        //    required: true
        //});
        //this.selectedMobile.extend({
        //    required: true
        //});

        this.selectedBusinessUnit.extend({
            required: true
        });
        this.selectedBox.extend({
            required: true
        });
        this.selectedVehicle.extend({
            required: true
        });
        this.enableDltInterface.extend({
            required: true
        });

        this.selectedVehicleModel.extend({
            required: {
                onlyIf: function() {
                    return self.isDLT()
                }
            }
        });
        this.chassisNo.extend({
            required: {
                onlyIf: function() {
                    return self.isDLT()
                }
            }
        });
        this.licenseProvince.extend({
            required: {
                onlyIf: function() {
                    return self.isDLT()
                }
            }
        });
        this.licenseCountry.extend({
            required: {
                onlyIf: function() {
                    return self.isDLT()
                }
            }
        });

        this.validationModel = ko.validatedObservable({
            //selectedPromotion:this.selectedPromotion,
            //selectedMobile: this.selectedMobile,
            selectedBusinessUnit: this.selectedBusinessUnit,
            selectedBox: this.selectedBox,
            selectedVehicle: this.selectedVehicle,
            enableDltInterface: this.enableDltInterface,
            selectedVehicleModel: this.selectedVehicleModel,
            chassisNo: this.chassisNo,
            licenseProvince: this.licenseProvince,
            licenseCountry: this.licenseCountry
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        if((this.mode === Screen.SCREEN_MODE_CREATE && WebConfig.userSession.hasPermission(Constants.Permission.CreateFleetService))
        || (this.mode === Screen.SCREEN_MODE_UPDATE && WebConfig.userSession.hasPermission(Constants.Permission.UpdateFleetService))) 
    {
         actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
    }
    actions.push(this.createAction("actCancel", this.i18n("Common_Cancel")(), "cancel"));
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * Generate fleet service model from View Model
     * 
     * @returns fleet service model which is ready for ajax request 
     */
    generateModel() {
        var model = {
            id:this.fleetServiceId,
            companyId: this.companyId,
            mobile:this.selectedMobile(),
            //simId:this.selectedMobile().id,
            promotionId: this.selectedPromotion().id,
            businessUnitId: this.selectedBusinessUnit(),
            boxId: this.selectedBox().id,
            vehicleId: this.selectedVehicle().id,
            isClosed : false, 
            enableDltInterface: this.enableDltInterface().value,
            ticketId: this.ticketId,
            newVehicleLicense: this.newLicense(),
            vehicleModel: this.selectedVehicleModel().id,
            chassisNo: this.chassisNo(),
            licensePlate: this.licensePlate(),
            licenseProvince: this.licenseProvince(),
            licenseCountry: this.licenseCountry()
        };
        return model;
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
            this.isBusy(true);
            var model = this.generateModel();
            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestFleetService.createFleetService(model).done((response) => {
                        this.publishMessage("update-items-appointment", response.id);
                        this.isBusy(false);
                        this.close(true);
                    }).fail((e)=> {
                        this.isBusy(false);
                        this.handleError(e);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    var simChange = (model.mobile != this.temp_mobile()) ? true : false;
                    if(simChange) {
                        this.showMessageBox(null, this.i18n("M176",[this.temp_mobile()]),
                    BladeDialog.DIALOG_YESNO).done((button) => {
                        switch(button){
                            case BladeDialog.BUTTON_YES:
                                model.isClosed = true;
                                this.webRequestFleetService.updateFleetService(model).done((response) => {
                                    this.publishMessage("update-items-appointment");
                                    this.isBusy(false);
                                    this.close(true);
                                }).fail((e)=> {
                                    this.isBusy(false);
                                    this.handleError(e);
                                });
                                break;
                            case BladeDialog.BUTTON_NO:
                                this.webRequestFleetService.updateFleetService(model).done((response) => {
                                    this.publishMessage("update-items-appointment");
                                    this.isBusy(false);
                                    this.close(true);
                                }).fail((e)=> {
                                    this.isBusy(false);
                                    this.handleError(e);
                                });
                                break;
                        }
                    });

                }else{
                    this.webRequestFleetService.updateFleetService(model).done((response) => {
                        this.publishMessage("update-items-appointment");
                        this.isBusy(false);
                        this.close(true);
                    }).fail((e)=> {
                        this.isBusy(false);
                        this.handleError(e);
                    });
                } 
                break;
            }
        return ; 
        }

    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(FleetServiceTicketScreen),
    template: templateMarkup
};
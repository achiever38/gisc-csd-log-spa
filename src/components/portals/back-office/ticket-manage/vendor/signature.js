﻿import ko from "knockout";
import templateMarkup from "text!./signature.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import FileUploadModel from "../../../../../components/controls/gisc-ui/fileupload/fileuploadModel";

class TicketVendorsManageSignatureScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Small;
        this.mode = this.ensureNonObservable(params.mode, Screen.SCREEN_MODE_CREATE);
        this.id = params.mode == Screen.SCREEN_MODE_CREATE ? 1 : 0 ;
        this.dataOnDt = params.dataOnDt ? params.dataOnDt : null ;
        this.idForUpdate = params.mode == Screen.SCREEN_MODE_UPDATE? params.data.id : null ; //id check itself
        this.idCheck = params.data ? params.data.id : null ;  // Check id for itself
        this.data = this.ensureNonObservable(params.data, []);
        this.name = ko.observable();
        this.position = ko.observable();
        this.fileUploadURL = WebConfig.appSettings.uploadUrlTrackingCore;

        this.infoStatus = ko.observable(Enums.InfoStatus.Add);

        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            this.bladeTitle(this.i18n("Update Signature")());
        } else {
            this.bladeTitle(this.i18n("Create Signature")());
        }

        if(this.dataOnDt && this.dataOnDt.length > 0){ //add id for signature select last id  then +1  is present id
            if(this.idForUpdate == this.idCheck){ // if id itself not +1  is id == id itself
                this.id = this.idForUpdate ; 
            }
            else{ // if not id itself is +1 
                var tmp = params.dataOnDt.map(function(o) {
                    return o.id;
                });
           
                var maxValue = Math.max.apply(Math, tmp); // find max value or last id 
                this.id = parseInt(maxValue + 1) ;
            }
           
        }

        // Logo
        this.logo = ko.observable(null);
        this.logoFileName = ko.observable("");
        this.logoSelectedFile = ko.observable(null);
        this.logoMimeType = [
            Utility.getMIMEType("jpg"),
            Utility.getMIMEType("png")
        ].join();
        this.logoSuggestionWidth = "70";
        this.logoSuggestionHeight = "46";
        this.logoUrl = ko.pureComputed(() => {
            if (this.logo() && this.logo().infoStatus !== Enums.InfoStatus.Delete) {
                this.isBusy(false);
                return this.logo().fileUrl;
            }
            else {
                return this.resolveUrl("~/images/upload-img-placeholder-100x100.jpg");
            }
            
        });

        


     
    }
    /**
     * On click remove logo icon
     */
    onRemoveFileLogo() {
        var logo = this.logo();
        if (logo) {
            if(logo.infoStatus === Enums.InfoStatus.Add){
                this.logo(null);
            }
            else {
                logo.infoStatus = Enums.InfoStatus.Delete;
                this.logo(logo);
            }
        }
    }

    /**
     * Hook onbefore fileupload
     */
    onBeforeUploadLogo() {
        this.isBusy(true);
        //remove current file before upload new file
        this.onRemoveFileLogo();
    }

    /**
     * Hook on fileuploadsuccess
     * @param {any} data
     */
    onUploadSuccessLogo(data) {
        if(data && data.length > 0){
            var newLogo = data[0];
            var currentLogo = this.logo();
            if (currentLogo) {
                switch (currentLogo.infoStatus) {
                    case Enums.InfoStatus.Add:
                        newLogo.infoStatus = Enums.InfoStatus.Add;
                        break;
                    case Enums.InfoStatus.Original:
                    case Enums.InfoStatus.Update:
                    case Enums.InfoStatus.Delete:
                        newLogo.infoStatus = Enums.InfoStatus.Update;
                        newLogo.id = currentLogo.id;
                        break;
                }
            }
            else {
                newLogo.infoStatus = Enums.InfoStatus.Add;
            }
            this.infoStatus(newLogo.infoStatus);
            this.logo(newLogo);
        }
    }

    /**
     *
     * Hook on fileuploadfail
     * @param {any} e
     */
    onUploadFailLogo(e) {
        this.handleError(e);
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        switch(this.mode){
            case Screen.SCREEN_MODE_UPDATE :
                this.name(this.data.name);
                this.position(this.data.position);
                this.infoStatus(this.data.infoStatus);
                this.logo(this.data.image)
                this.logoFileName(this.data.image && this.data.image.fileName);
                var file = new FileUploadModel();
                file.name = this.data.image && this.data.image.fileName;
                file.extension = "png";
                file.imageWidth = this.logoSuggestionWidth;
                file.imageHeight = this.logoSuggestionHeight;
                this.logoSelectedFile(file);
            break;
        }
        
    }

   
    setupExtend() {

        this.logo.extend({ trackChange: true });
        this.name.extend({ trackChange: true });
        this.position.extend({ trackChange: true });

        this.logoSelectedFile.extend({
            fileRequired: true,
            fileExtension: ['jpg', 'png']
        });
        this.name.extend({ required: true });
        this.position.extend({ required: true });

        this.validationModel = ko.validatedObservable({
            logoSelectedFile: this.logoSelectedFile,
            name: this.name,
            position: this.position,
        });
    }

    /**
     * Generate user model from View Model
     * 
     * @returns user model which is ready for ajax request 
     */
    generateModel() {

        var model = {
            id:this.id,
            image: this.logo(),
            name: this.name(),
            position: this.position(),
            infoStatus : Enums.InfoStatus.Add,
            mode: this.mode
        };

        return model;
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actOK", this.i18n("Common_OK")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        var self = this;
        if (sender.id === "actOK") {

            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }else if(this.dataOnDt && this.dataOnDt.length > 0){ // if have dataAll is values 
                let match = _.find(this.dataOnDt , (data)=>{
                    return data.name == this.name() ; 
                });

                if(this.idForUpdate == this.idCheck){
                    let matchSignature = _.find(this.dataOnDt , (data)=>{  // find licenseNo with id at Update 
                        return data.id == this.idForUpdate
                    });

                    if( match && (matchSignature.name != match.name)) { // check license itself (if LicenseNo not equal isn't LicenseNo itself )
                        this.showMessageBox(this.i18n("M010")());
                        return ;
                    }

                    //update value
                    var res = _.forEach(this.dataOnDt, function(v) {
                        if(v.id == self.idForUpdate){
                            v.id = self.idForUpdate,
                            v.image = self.logo(),
                            v.name = self.name(),
                            v.infoStatus =  (v.infoStatus == Enums.InfoStatus.Add) ?  Enums.InfoStatus.Add : Enums.InfoStatus.Update 
                            v.position = self.position(),
                            v.mode = self.mode
                        }
                    });
                    this.publishMessage("bo-ticket-vendor-signature", { dataUpdate : res , idUpdate : this.idForUpdate});
                    this.close(true);
                }
                else if(match){
                    this.showMessageBox(this.i18n("M010")());
                    return ;
                }
                else{
                    //create and push value
                    var model = this.generateModel();
                    this.publishMessage("bo-ticket-vendor-signature", model);
                    this.close(true);
                }
  
            }
            else{
                //create first row
                var model = this.generateModel();
                this.publishMessage("bo-ticket-vendor-signature", model);
                this.close(true);
            }
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        
    }
}
export default {
viewModel: ScreenBase.createFactory(TicketVendorsManageSignatureScreen),
template: templateMarkup
};
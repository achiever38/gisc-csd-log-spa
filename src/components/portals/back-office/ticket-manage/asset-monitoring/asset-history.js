﻿import ko from "knockout";
import templateMarkup from "text!./asset-history.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import WebRequestBox from "../../../../../app/frameworks/data/apitrackingcore/webrequestBox";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import WebRequestTicket from "../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import Utility from "../../../../../app/frameworks/core/utility";

class TicketViewHistory extends ScreenBase {
    constructor(params) {
        super(params);
        console.log("params",params);
        this.bladeIsMaximized = true;
        this.companyId = params.companyId;
        this.businessUnitIdbuId = params.businessUnitId;
        this.licenseId = params.licenseId;
        this.IsCloseTicket = params.IsCloseTicket;
        switch(this.IsCloseTicket){
            case true:
                this.bladeTitle(this.i18n("Ticket_InActiveTickets")());
                break;
            case false:
                this.bladeTitle(this.i18n("Ticket_ActiveTickets")());
                break;
        }
        this.dt = ko.observableArray([]);
        this.apiResult = ko.observableArray([]);

        this.viewTicket = (data) => {
            this.navigate("bo-ticket-manage-asset-details", {ticketId : data.ticketId})
        };
    }

    // /**
    //  * Get WebRequest specific for Box module in Web API access.
    //  * @readonly
    //  */
    get webRequestBox() {
        return WebRequestBox.getInstance();
    }
    /**
     * Get WebRequest specific for Company module in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }

    get WebRequestTicket() {
        return WebRequestTicket.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var filter = { 
            companyId:this.companyId , 
            businessUnitId:this.businessUnitIdbuId ,
            vehicleIds : [this.licenseId],
            IsCloseTicket: this.IsCloseTicket
        }
        console.log("filter",filter);

        if(typeof filter.companyId === 'undefined'){
            filter.vehicleIds = [0] ;
            this.apiResult({
                read:this.WebRequestTicket.listTicketSummaryDataGrid(filter),
                // update:this.WebRequestTicket.listTicketSummaryDataGrid(filter),
            });
        }
        else{
            this.apiResult({
                read:this.WebRequestTicket.listTicketSummaryDataGrid(filter),
                // update:this.WebRequestTicket.listTicketSummaryDataGrid(filter),
            });
        }
        console.log(">>>",this.apiResult());

    }

    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        // this.entityType.extend({ required: true });

        this.validationModel = ko.validatedObservable({
            // entityType: this.entityType
        });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(TicketViewHistory),
    template: templateMarkup
};
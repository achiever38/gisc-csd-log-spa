﻿import ko from "knockout";
import templateMarkup from "text!./asset-update.html";
import ScreenBase from "../../../screenbase";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestContract from "../../../../../app/frameworks/data/apitrackingcore/webRequestContract";
import WebRequestTicket from "../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";

class AssetMonitoringUpdateScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Common_Update")());
        this.bladeSize = BladeSize.Medium;

        this.itemTickets = this.ensureObservable(params.items, []);
        this.filter = this.ensureObservable(params.filter, { noSignalDuration: 30 });

        this.contractNo = ko.observableArray([]); 
        this.selectedContractNo = ko.observable();

        this.items = ko.observableArray([]);
        this.order = ko.observable([[ 0, "asc" ]]);

        this.isSameContract = ko.observable(false);
        this.simContract = ko.observableArray([]);
        this.selectedSimContract = ko.observable();

        this.isEnableSim = ko.pureComputed(() => {
            if(this.isSameContract()){
                this.selectedSimContract(null);
            }
            return !this.isSameContract()
        });
    }
    
    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * Get WebRequest specific for Contract module in Web API access.
     * @readonly
     */
    get webRequestContract() {
        return WebRequestContract.getInstance();
    }

    /**
     * create ticket.
     * @readonly
     */
    get webRequestTicket() {
        return WebRequestTicket.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();
        var d1 = this.webRequestContract.listContract({enable:true});
        $.when(d1).done((r1) => {
            let itemTickets = this.itemTickets();
            
            itemTickets = _.filter(itemTickets, (item)=>{
                return item.boxId;
            });

            this.items(itemTickets);
            this.contractNo(r1.items);
            this.simContract(r1.items);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;
    }

    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        
        var self = this;

        this.items.extend({
            arrayRequired: true
        });
        this.selectedContractNo.extend({
            required: {
                onlyIf : () => {
                    return (self.selectedContractNo() == null && self.selectedSimContract() == null) || self.isSameContract();
                }
            }
        });

        this.selectedSimContract.extend({
            required: {
                onlyIf : () => {
                    return self.selectedContractNo() == null && self.selectedSimContract() == null && !self.isSameContract();
                }
            }
        });


        this.validationModel = ko.validatedObservable({
            selectedContractNo: this.selectedContractNo,
            selectedSimContract: this.selectedSimContract,
            items: this.items
        });

    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {

        var enable = (_.size(this.items())) ? true : false;
        actions.push(this.createAction("actSave", this.i18n("Common_Save")(), "default", true, enable));
        actions.push(this.createActionCancel());

    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if(sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            let items = _.clone(this.items());

            let assetIds = _.map(items, (item)=>{
                return item.boxId;
            });
            
            let simIds = _.map(items,(item) => {
                if(item.simId){
                    return item.simId;
                }
            })

            this.isBusy(true);

            let filter = {
                assetIds: assetIds,
                contractId: this.selectedContractNo() ? this.selectedContractNo().contractId : null,
                simContractId: this.selectedSimContract() ? this.selectedSimContract().contractId : null,
                simIds: simIds,
                isSameContract: this.isSameContract()
            }

            this.webRequestContract.updateContract(filter).done(()=>{
                this.isBusy(false);
                this.publishMessage("add-items-asset",this.filter());
                this.close(true);
            }).fail((e)=>{
                this.isBusy(false);
                this.handleError(e);
            });

        }
        
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(AssetMonitoringUpdateScreen),
    template: templateMarkup
};
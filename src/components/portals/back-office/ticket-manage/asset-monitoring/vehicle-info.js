﻿import ko from "knockout";
import templateMarkup from "text!./vehicle-info.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestAssetMonitoring from "../../../../../app/frameworks/data/apitrackingcore/webRequestAssetMonitoring";
import Utility from "../../../../../app/frameworks/core/utility";
import { AssetIcons } from "../../../../../app/frameworks/constant/svg";

class VehicleInfoScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.companyID = this.ensureNonObservable(params.companyId);
        this.vehicleID = this.ensureNonObservable(params.vehicleId);
        this.signal= this.ensureNonObservable(params.noSignalDuration);
        this.VehicleLicense = this.ensureNonObservable(params.vehicleLicense);
        this.SerialNo = this.ensureNonObservable(params.serialNo);
        this.bladeTitle(this.VehicleLicense+"("+this.SerialNo+")");
        this.bladeDescription(this.BuName);
        this.bladeSize = BladeSize.Small;
        this.fdriverName = ko.observable();
        this.flocation = ko.observable();
        this.flocationDate = ko.observable();
        this.fparkTime = ko.observable();
        this.fengineOn = ko.observable();
        this.disabledPark = ko.observable(true);
        this.disabledEngine = ko.observable(true);

        this.pathMovement = ko.observable(AssetIcons.IconMovementTemplateMarkup);
        this.pathSpeed = ko.observable(AssetIcons.IconSpeedTemplateMarkup);
        this.pathDirection = ko.observable(AssetIcons.IconDirectionTemplateMarkup);
        this.pathGps = ko.observable(AssetIcons.IconGpsTemplateMarkup);
        this.pathGsm = ko.observable(AssetIcons.IconGsmTemplateMarkup); 
        this.pathAltitude = ko.observable(AssetIcons.IconAltitudeTemplateMarkup);
        this.pathEngine = ko.observable(AssetIcons.IconEngineTemplateMarkup);
        this.pathDlt = ko.observable(AssetIcons.IconDltStatusTemplateMarkup);
        this.pathFuel = ko.observable(AssetIcons.IconFuelTemplateMarkup);
        this.pathBattery = ko.observable(AssetIcons.IconVehicleBatteryTemplateMarkup);
        this.pathMileage = ko.observable(AssetIcons.IconMileageTemplateMarkup);
        

        this.txtMovement = ko.observable();
        this.txtSpeed = ko.observable();
        this.txtDirection = ko.observable();
        this.txtGps = ko.observable();
        this.txtGsm = ko.observable();
        this.txtAltitude = ko.observable();
        this.txtDlt = ko.observable();
        this.txtEngine = ko.observable();
        this.txtFuel = ko.observable();
        this.txtBattery = ko.observable();
        this.txtMileage = ko.observable();
        this.txtTempareture = ko.observable();
        this.txtGate = ko.observable();

        this.locationColor = ko.observable('#323b3a');
        this.driverNameColor = ko.observable('green');
        this.locationDateColor = ko.observable('green');
        this.parkColor = ko.observable('red');
        this.engineColor = ko.observable('red');

        this.DataTemperature = ko.observableArray([]);
        this.DataGate = ko.observableArray([]);

        
    }


    get webRequestAssetMonitoring() {
        return WebRequestAssetMonitoring.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var filter = {
            companyId: this.companyID,
            vehicleId: this.vehicleID,
            noSignalDuration: this.signal,
            includeFeature: true
        };
        
        this.webRequestAssetMonitoring.listAssetMonitoringSummary(filter).done((response) => {
            if(response){

                var items = response["items"];
                this.bladeDescription(items[0].businessUnitName);
                var feature = items[0]["trackLocationFeatureValues"];
                //Driver Name
                this.fdriverName(items[0].driverName);
                //Location
                this.flocation(items[0].location);
                //Location Date
                this.flocationDate(items[0].formatLastLocationDate);
                if(items[0].formatParkTime == 0){
                    this.disabledPark(false);
                }else{
                    this.disabledPark(true);
                }
                if(items[0].formatEngineOnTime == 0){
                    this.disabledEngine(false);
                }else{
                    this.disabledEngine(true);
                }

                //ParkTime
                this.fparkTime("Park time "+items[0].formatParkTime + " Minute(s)");
                //Engine On
                this.fengineOn("Enginenon time "+items[0].formatEngineOnTime+ " Minute(s)");
                //Icon Movement
                this.txtMovement(items[0].movementDisplayName);
                //Icon Speed
                this.txtSpeed(items[0].formatSpeed);
                //Icon Direction
                this.txtDirection(items[0].directionTypeDisplayName);
                //Icon GPS
                this.txtGps(items[0].gpsStatusDisplayName);
                //Icon GSM
                this.txtGsm(items[0].gsmStatusDisplayName);
                //Icon Altitute
                this.txtAltitude(items[0].altitude);
                //Icon DLT
                this.txtDlt(items[0].dltStatusDisplayName);

                //Feature[1] list Engine
                //Feature[3] list Fuel
                //Feature[5] list Battery
                //Feature[12] list Mileage

                if(items[0].isCustomPOI == true){ this.locationColor('blue'); }
                if(feature[1]){ this.txtEngine(feature[1].formatValue); }
                if(feature[3]){  this.txtFuel(feature[3].formatValue+feature[3].featureUnitSymbol); }
                if(feature[5]){ this.txtBattery(feature[5].formatValue + " " +feature[5].featureUnitSymbol); }
                if(feature[12]){ this.txtMileage(feature[12].formatValue + " "+ feature[12].featureUnitSymbol);}
                
                var objTemp1 = [];
                var objGate1 = [];
                var objTemp2 = [];
                var objGate2 = [];
                var countTemp = 0;
                var countGate = 0;
                for(var key in Object.keys(feature)) {
                    var value = Object.keys(feature)[key];

                    if(feature[value].featureUnit == 2){
                        objTemp1.push({
                            tempvalue : feature[value].formatValue +" "+ feature[value].featureUnitSymbol , 
                            tempName : feature[value].featureName 
                        });
                        countTemp++;
                    }
                    if(feature[value].featureUnit == 4){
                        objGate1.push({ 
                            gatevalue : feature[value].formatValue,
                            gateName : feature[value].featureName
                        });
                        countGate++;
                    }
                }

                var value1;
                var value2;
                var valueName1;
                var valueName2;
                var valueVisible1;
                var valueVisible2;

                if(countTemp%2 == 0){
                    countTemp--; 
                }
                if(countGate%2 == 0){
                    countGate--;
                }

                for(var i=0 ; i<countTemp ; i+=2){

                    if(objTemp1[i]){
                        value1 = objTemp1[i].tempvalue;
                        valueName1 = objTemp1[i].tempName;
                        valueVisible1 = true;
                    }else{
                        value1 = null;
                        valueName1 = null;
                        valueVisible1 = false;
                    }
                    if(objTemp1[i+1]){
                        value2 = objTemp1[i+1].tempvalue;
                        valueName2 = objTemp1[i+1].tempName;
                        valueVisible2 = true;
                    }else{
                        value2 = null;
                        valueName2 = null;
                        valueVisible2 = false;
                    }

                    objTemp2.push({
                        iconTemparature : AssetIcons.IconTemparetureTemplateMarkup,
                        temparature1 : value1,
                        titleTemperature1 : valueName1,
                        tempvisible1 : valueVisible1,
                        temparature2 : value2,
                        titleTemperature2 : valueName2,
                        tempvisible2 : valueVisible2
                    });
                }

                for(var i=0 ; i<countGate ; i+=2){

                    if(objGate1[i]){
                        value1 = objGate1[i].gatevalue;
                        valueName1 = objGate1[i].gateName;
                        valueVisible1 = true;
                    }else{
                        value1 = null;
                        valueName1 = null;
                        valueVisible1 = false;
                    }
                    if(objGate1[i+1]){
                        value2 = objGate1[i+1].gatevalue;
                        valueName2 = objGate1[i+1].gateName;
                        valueVisible2 = true;
                    }else{
                        value2 = null;
                        valueName2 = null;
                        valueVisible2 = false;
                    }

                    objGate2.push({
                        iconGate : AssetIcons.IconGateTemplateMarkup,
                        gate1 : value1,
                        titleGate1 : valueName1,
                        gatevisible1 : valueVisible1,
                        gate2 : value2,
                        titleGate2 : valueName2,
                        gatevisible2 : valueVisible2
                    });
                }

                this.DataTemperature (objTemp2);
                this.DataGate(objGate2)
            }
        });
        
        
       
        //this.movement = "../../../../../../../svgs/icon-movement.html";
        //this.movementTxt = this.MovementDisplayName;
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {

    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
       
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        
    }
    
}

export default {
viewModel: ScreenBase.createFactory(VehicleInfoScreen),
    template: templateMarkup
};
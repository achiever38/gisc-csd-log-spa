﻿import ko from "knockout";
import templateMarkup from "text!./asset-details.html";
import TableAssetScreenBase from "../../table-asset-screenbase";
import ScreenHelper from "../../../screenhelper";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import IconFollowTemplateMarkup from "text!./../../../../svgs/icon-follow.html";
import webRequestTicket from "../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";

class TicketDetails extends ScreenBase {
    constructor(params) {
        super(params);
        this.id = this.ensureNonObservable(params.ticketId);
        this.statusId = this.ensureObservable(params.status); //Check status for hide Create Appointment Button
        this.bladeTitle(this.i18n("Ticket_TicketDetails")());
        this.bladeSize = BladeSize.XLarge;
        this.bladeIsMaximized = true;
        this.tickDate = ko.observable();
        this.companyId = ko.observable();
        this.company = ko.observable();
        this.ackBy = ko.observable();
        this.pendBy = ko.observable();
        this.tickId = ko.observable();
        this.bu = ko.observable();
        this.actDate = ko.observable();
        this.pDate = ko.observable();
        this.contractNo = ko.observable();
        this.vehicleId = ko.observable();
        this.license = ko.observable();
        this.inProBy = ko.observable();
        this.pCause = ko.observable();
        this.cStart = ko.observable();
        this.boxNo = ko.observable();
        this.inProDates = ko.observable();
        this.pendSubC = ko.observable();
        this.cEnd = ko.observable();
        this.mobileNo = ko.observable();
        this.confirmBy = ko.observable();
        this.pendRemark = ko.observable();
        this.status = ko.observable();
        this.boxTemplate = ko.observable();
        this.confirmDate = ko.observable();
        this.closeBy = ko.observable();
        this.serviceType = ko.observable();
        this.statusGps = ko.observable();
        this.followBy = ko.observable();
        this.closeDate = ko.observable();
        this.assignTo = ko.observable();
        this.lastUpdate =ko.observable();
        this.followDate = ko.observable();
        this.targetDate = ko.observable();
        this.lastSendEmail = ko.observable();
        this.owner = ko.observable();
        this.remark = ko.observable();
        this.selectedIndex = ko.observable(0);
        this.dt = ko.observableArray([]);
        this.dtTicketLog = ko.observableArray([]);
        this.dt3 = ko.observableArray([]);
        this.recentChangedRowIds = ko.observableArray([]);
        this.listAppoi  = ko.observableArray([]);
        this.LogTicket = ko.observableArray([]);
        this.apiRead = ko.observableArray([]);
        this.maintenanceType = ko.observable();
        this.IdforGet = (this.id)?this.id:null ;
        this.isStatusClose = this.ensureObservable(params.status);
        this.fleetId = ko.observable();
        this.gpsLastUpdate = ko.observable();
        this.ticketData = ko.observable();
        this.replaceBoxWithOtherModel = ko.observable();
        this.qc = ko.observable();
        this.currItemTicket = ko.observable(null);
        this.closeCause = ko.observable();
        this.closeSubCause = ko.observable();
        this.closeRemark = ko.observable();
        this.IsMoveDefaultDriver = ko.observable();

        this.onClickLicense = (e)=>{
            var params = {
                companyId: this.companyId(),
                vehicleId: this.vehicleId(),
                vehicleLicense: this.license(),
                serialNo: this.boxNo()
            }
            this.navigate("bo-ticket-manage-asset-detail-vehicle-info", params);
        }

        this.onClickBox = (e)=>{
            var params = {
                fleetId: this.fleetId(),
                ticketId: this.tickId(),
                companyId: this.companyId(),
                mobile: this.mobileNo()
            }
            this.navigate("bo-ticket-manage-detail-fleet-service", params);
        }

        this.viewDetail = (data) =>{
            if(data.status == 1)
            {
                return "<u>update</u>";
            }
            else
            {
                return "<u>view</u>";
            }
            
        };

        this.viewBodyDetail = (data) =>{
            return "<u>view</u>";
        };

        this.viewDetailAppoi = (data) =>{
            return "<u>view</u>";
        };
        this.showErrorMsg = (data)=>{
            return (data.errorMessage==null)?"-":data.errorMessage;
        };

        this.clickMaildetail = (data) => {
            this.navigate("bo-ticket-manage-sendmail",{
                items:data,
                IsTicketDetail:true
            });
        };

        this.clickEditAppoiment = (data) =>{
            // if(data.status == 1)
            // {
            //   this.navigate("bo-ticket-manage-edit-appointment",
            //   {
            //       items:data,
            //       companyId : this.companyId()
            //   });
            // }
            // else
            // {
            //     this.navigate("bo-ticket-manage-view-appointment",
            //      {
            //          items:data,
            //          companyId : this.companyId()
            //      });
            // }
            this.navigate("bo-ticket-manage-edit-appointment",
            {
                items:data,
                companyId : this.companyId()
            });

        };
        this.viewError = (data) =>{
            this.navigate("bo-ticket-manage-ticket-view-error",data.id)
        };

        this.subscribeMessage("update-items-appointment",(appointId) =>{
            this.callDataWithApi();
        }); 

        this.subscribeMessage("bo-ticket-edit-multiple-service-changed",(e) =>{
            this.callDataWithApi();
            this.statusId(Enums.ModelData.TicketStatus.Close);
        });

        
 
    }


    get webRequestTicket() {
        return webRequestTicket.getInstance();
    }
   
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {

        if (!isFirstLoad) {
            return;
        }
        if(this.id){

            this.callDataWithApi();
        }

        else{

            this.id = 0 ; 
            this.listAppoi({
                read:this.webRequestTicket.listTicketAppointmentDataGrid({TicketId:this.id, isClose:true}),
                update:this.webRequestTicket.listTicketAppointmentDataGrid({TicketId:this.id})
            }); 

            this.LogTicket({
                read:this.webRequestTicket.getLogTicketDataGrid(this.id),
                update:this.webRequestTicket.getLogTicketDataGrid(this.id)
            });

            this.apiRead({
                read:this.webRequestTicket.getTicketEmailLogDataGrid({TicketId:this.id}),
                update:this.webRequestTicket.getTicketEmailLogDataGrid({TicketId:this.id})
            });

        }

    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(this.statusId() != Enums.ModelData.TicketStatus.Close){
            commands.push(this.createCommand("cmdCreate", this.i18n("Ticket_CreateAppoiment")(), "svg-ticket-create-appointment")); 
            commands.push(this.createCommand("ticketClosed", this.i18n("Ticket_CloseTicket")(), "svg-ticket-editticket")); 
            commands.push(this.createCommand("editTicket", this.i18n("Edit Ticket")(),"svg-ticket-edit-ticket"));     
            commands.push(this.createCommand("cmdSendMail", this.i18n("Send Emails")(), "svg-ticket-sentmail"));    
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdCreate":
                this.navigate("bo-ticket-manage-create",{ticketId : this.tickId() , companyId:this.companyId()});
                break;
            case "ticketClosed":
                this.navigate("bo-ticket-manage-detail-close",{ticketId : this.id , companyId:this.companyId(), statusId:this.statusId()});
                break;
            case "editTicket":
                let arrTicket = this.ticketData() ? [this.ticketData()] : [] ; 
                this.navigate("bo-ticket-manage-edit",arrTicket);
                break;
            case "cmdSendMail":
                this.navigate("bo-ticket-manage-sendmail", {
                    items:[this.currItemTicket()],
                    IssendOneTicket:true
                });
                break;
        }
        
    }


    /*
        function call api this page
    */
    callDataWithApi(){

        this.listAppoi({
            read:this.webRequestTicket.listTicketAppointmentDataGrid({TicketId:this.id, isClose:true}),
            update:this.webRequestTicket.listTicketAppointmentDataGrid({TicketId:this.id})
        }); 

        this.LogTicket({
            read:this.webRequestTicket.getLogTicketDataGrid({id:this.id}),
            update:this.webRequestTicket.getLogTicketDataGrid({id:this.id})
        });

        this.apiRead({
            read:this.webRequestTicket.getTicketEmailLogDataGrid({TicketId:this.id}),
            update:this.webRequestTicket.getTicketEmailLogDataGrid({TicketId:this.id})
        });

        var w1 = this.webRequestTicket.getTicket(this.id);

        $.when(w1).done((response)=>{
            this.currItemTicket(response); // use for sendmail

            this.companyId(response.companyId);
            //Row1
            this.tickDate(response.formatTicketDate);
            this.company(response.companyName);
            this.ackBy(response.acknowledgeBy);
            this.pendBy(response.pendingBy);
            //Row2
            this.tickId(response.ticketId);
            this.bu(response.businessUnitName);
            this.actDate(response.formatAcknowledgeDate);
            this.pDate(response.formatPendingDate);
            //Row3
            this.contractNo(response.contractNo);
            this.license(response.license);
            this.inProBy(response.inprogressBy);
            this.pCause(response.pendingCause);
            this.closeCause(response.closeCause);
            //Row4
            this.cStart(response.formatContractStart);
            this.boxNo(response.serialNo);
            this.inProDates(response.formatInprogressDate);
            this.pendSubC(response.pendingSubCause);
            this.closeSubCause(response.closeSubCause);
            //Row5
            this.cEnd(response.formatContractEnd);
            this.mobileNo(response.mobile);
            this.confirmBy(response.confirmBy);
            this.pendRemark(response.pendingRemark);
            this.closeRemark(response.closeCauseRemark);
            //Row6
            this.status(response.statusDisplayName);
            this.boxTemplate(response.boxTemplate);
            this.confirmDate(response.formatConfirmDate);
            this.closeBy(response.closeBy);
            //Row7
            this.serviceType(response.serviceTypeDisplayName);
            this.statusGps(response.gpsStatus==true?"~../../../gis-js/images/vehicle_status/icon_nearest_assets_gsm_good@3x.png":"~../../../gis-js/images/vehicle_status/icon_nearest_assets_gsm_bad@3x.png");
            this.followBy(response.followBy);
            this.closeDate(response.formatCloseDate);
            //Row8
            this.assignTo(response.assignTo);
            this.gpsLastUpdate(response.formatGpsLastUpdate);
            this.followDate(response.formatFollowDate);
            this.maintenanceType(response.maintenanceTypeName);
            this.IsMoveDefaultDriver(response.isMoveDefaultDriver);
            //Row9
            this.targetDate(response.formatTargetDate);
            this.lastSendEmail(response.formatLastSendMailDate);
            this.lastUpdate(response.formatUpdateDate);
            this.replaceBoxWithOtherModel(response.isReplaceBoxWithOtherModel ? "true" : null)
            //Row10
            this.owner(response.owner);
            this.remark(response.remark);
            this.qc(response.isQC ? "true" : null)
            //other
            this.vehicleId(response.vehicleId);
            this.fleetId(response.fleetId);

            this.ticketData(response);
        }).fail((e)=>{
            this.handleError(e);
        });
    }
}

export default {
viewModel: ScreenBase.createFactory(TicketDetails),
    template: templateMarkup
};
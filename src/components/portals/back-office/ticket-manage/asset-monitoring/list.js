﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import {
    Enums,
    EntityAssociation
} from "../../../../../app/frameworks/constant/apiConstant";
import Utility from "../../../../../app/frameworks/core/utility";
import WebRequestAssetMonitoring from "../../../../../app/frameworks/data/apitrackingcore/webRequestAssetMonitoring";
import GenerateDataGridExpand from "../../../company-workspace/generateDataGridExpand";
import {
    AssetIcons
} from "../../../../../app/frameworks/constant/svg";
import WebRequestTicket from "../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";
import DefaultSoring from "../../../../../app/frameworks/constant/defaultSorting";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";

class AssetMonitoringScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.filterState = this.ensureObservable(params, {});
        this.bladeTitle(this.i18n("Ticket_AssetMonitoring")());
        // this.bladeSize = BladeSize.XLarge;
        this.bladeIsMaximized = true;
        this.selectedItems = ko.observableArray([]);
        this.selectedRow = ko.observableArray([]);
        this.items = ko.observableArray([]);
        this.apiDataSource = ko.observableArray([]);

        // gen header icon
        this.headerTemplateIcon = _.template('<div class="<%= iconCssClass %>" title="<%= title %>"><%= icon %></div>');
        this.headerTemplateEngine = this.headerTemplateIcon({
            'iconCssClass': 'icon-engine',
            'title': this.i18n("Common_Engine")(),
            'icon': AssetIcons.IconEngineTemplateMarkup
        });
        this.headerTemplateMovement = this.headerTemplateIcon({
            'iconCssClass': 'icon-movement',
            'title': this.i18n("Common_Movement")(),
            'icon': AssetIcons.IconMovementTemplateMarkup
        });
        this.headerTemplateSpeed = this.headerTemplateIcon({
            'iconCssClass': 'icon-speed',
            'title': this.i18n("Common_Speed")(),
            'icon': AssetIcons.IconSpeedTemplateMarkup
        });
        this.headerTemplateGSMStatus = this.headerTemplateIcon({
            'iconCssClass': 'icon-gsm',
            'title': this.i18n("Ticket_GSMStatus")(),
            'icon': AssetIcons.IconGsmTemplateMarkup
        });
        this.headerTemplateGPSStatus = this.headerTemplateIcon({
            'iconCssClass': 'icon-gps',
            'title': this.i18n("Ticket_GPSStatus")(),
            'icon': AssetIcons.IconGpsTemplateMarkup
        });


        this.boxSerialNoClick = (data) => {
            data.ticketId = -1;
            this.navigate("bo-ticket-manage-fleet-service", data);
        }

        this.headerIcon = () => {
            var icon = '<div></div><div class="icon-engine"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 22"><ellipse class="cls-1" cx="11.47" cy="12.06" rx="1.63" ry="1.59"></ellipse><path class="cls-1" d="M5.7,7Z"></path><path class="cls-1" d="M20.38,11.14a23.88,23.88,0,0,0-.57-2.88c0-.15-.2-.39-.31-.39-.86,0-1.06,0-2,0V9.43h-.81c0-.29,0-.53,0-.77,0-.67-.15-.81-.83-.81h-1c0-.18,0-.28,0-.39,0-1.25-.34-1.55-1.62-1.55H12V5.09c.63,0,1.22,0,1.81,0,.43,0,.65-.08.65-.58S14.27,4,13.86,4Q10.63,4,7.4,4C7,4,6.74,4,6.74,4.54s.29.56.68.55c.59,0,1.18,0,1.79,0V5.9c-.55,0-1.07,0-1.58,0C6.77,5.84,6.07,6,5.9,7H5.57c-.59,0-.77.19-.78.76s0,1.15,0,1.73,0,.91,0,1.33H3.25v-4a.85.85,0,1,0-1.71,0v4a.86.86,0,0,0,.52.79.5.5,0,0,0,0,.21v3.7a.51.51,0,1,0,1,0v-3.7a.5.5,0,0,0,0-.15H4.8c0,1.06,0,2.06,0,3.06,0,.61.18.79.8.8a4.32,4.32,0,0,1,.71,0,1.36,1.36,0,0,1,.77.31c.66.6,1.26,1.26,1.91,1.87a.93.93,0,0,0,.57.26c2.43,0,3.53,0,6,0a1.1,1.1,0,0,0,1.2-1.17c0-.33,0-.66,0-1h.84v1.63a10.63,10.63,0,0,1,1.46,0c.56.05.74-.2.88-.66A14.51,14.51,0,0,0,20.38,11.14Zm-4.85,1.23c0,.13,0,.17-1.11.6l-.14.33c.5,1,.47,1.08.37,1.18l-.66.64h-.08a5.66,5.66,0,0,1-1.08-.4l-.34.14C12.09,16,12,16,11.91,16h-.85c-.13,0-.17,0-.61-1.08l-.34-.14A3.93,3.93,0,0,1,9,15.15H9l-.06-.05-.6-.59c-.09-.09-.12-.12.35-1.19L8.52,13c-1.11-.4-1.11-.44-1.11-.58v-.84c0-.13,0-.17,1.11-.6l.14-.33c-.5-1.05-.47-1.09-.37-1.18l.65-.64H9a5.7,5.7,0,0,1,1.08.4l.34-.14C10.84,8,10.89,8,11,8h.85C12,8,12,8,12.48,9.08l.34.14a3.88,3.88,0,0,1,1.07-.42H14l.06.05.6.59c.09.09.12.12-.35,1.2l.14.33c1.11.4,1.11.44,1.11.57Z"></path></svg></div>';
            return icon;
        }

        this.onClickActiveTicket = (data) => {
            
            this.navigate("bo-ticket-manage-asset-history", {
                companyId: data.companyId,
                businessUnitId: data.businessUnitId,
                licenseId: data.licenseId,
                IsCloseTicket: false
            });
        };

        this.onClickInActiveTicket = (data) => {
            this.navigate("bo-ticket-manage-asset-history", {
                companyId: data.companyId,
                businessUnitId: data.businessUnitId,
                licenseId: data.licenseId,
                IsCloseTicket: true
            });
        };

        this.viewLicense = (data) => {
            this.navigate("bo-ticket-manage-asset-vehicle-info", {
                companyId: data.companyId,
                vehicleId: data.licenseId,
                vehicleLicense: data.vehicleLicense,
                serialNo: data.serialNo,
            });
        };

        this.subscribeMessage("add-items-asset", (data) => {
            this.selectedItems().length = 0;
            this.apiDataSource({
                read: this.webRequestAssetMonitoring.listAssetMonitoringSummaryNew(data),
            });
        });

        this.subscribeMessage("update-items-appointment", () => {
            this.apiDataSource({
                read: this.webRequestAssetMonitoring.listAssetMonitoringSummaryNew(this.filterState()),
            });
        });
        this.viewTicket=(res)=>{
            //console.log("res",res)
            this.navigate("bo-ticket-manage-asset-details", {ticketId : res.ticketId})
        }
        // Dynamics server side processing Read.
        this.onDatasourceRequestRead = (res) => {

            var filter = Object.assign({}, this.generateModel(), res);

            var dfd = $.Deferred();

            this.isBusy(true);
            this.webRequestAssetMonitoring.listAssetMonitoringSummary(filter).done((response) => {
                // var i = 1;
                // response.items.forEach(function(e){
                //     e.checked = (i%2==0)?true:false;
                //     i++;
                // });
                this.selectedItems([]);
                dfd.resolve(response);
            }).fail((e) => {
                this.handleError(e);
                dfd.fail(e);
            }).always(() => {
                this.isBusy(false);
            });

            return dfd;
        };

        this.active = ko.observable(true);
        this.apiResult = ko.observableArray([]);

        this.onDetailClickColumns = ko.observableArray([]);
        this.onDetailClickColumns(
            GenerateDataGridExpand.getOnClickColumns(this.generateColumnDetail())
        );
    }

    /**
     * Get WebRequest specific for AssetMonitoring module in Web API access.
     * @readonly
     */
    get webRequestAssetMonitoring() {
        return WebRequestAssetMonitoring.getInstance();
    }
    get webRequestTicket() {
        return WebRequestTicket.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {

        if (!isFirstLoad) {
            return;
        }

        this.apiDataSource({
            read: this.webRequestAssetMonitoring.listAssetMonitoringSummaryNew(this.filterState()),
            // update: this.webRequestAssetMonitoring.listAssetMonitoringSummaryNew(filter)
        });

    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdFind", this.i18n("Common_FindAssets")(), "svg-cmd-search"));
        commands.push(this.createCommand("cmdCreate", this.i18n("Ticket_CreateTicket")(), "svg-ticket-create"));
        commands.push(this.createCommand("cmdExport", this.i18n("Common_Export")(), "svg-cmd-export"));
        commands.push(this.createCommand("cmdUpdate", "Update Contract", "svg-icon-update-contract"));
        if(WebConfig.userSession.isSysAdmin){
            commands.push(this.createCommand("cmdBatchAction","Batch Action", "svg-ic-mt-maintenancestypes"));
        }
        
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdFind":
                this.close(true);
            break;
            case "cmdCreate":

                if (Object.keys(this.selectedItems()).length < 1) {
                    this.showMessageBox("", this.i18n("M168")(), BladeDialog.DIALOG_OK);
                    return false;
                }
                this.navigate("bo-ticket-manage-asset-create", {
                    items: this.selectedItems(),
                    filter: this.filterState()
                });
            break;
            case "cmdExport":
                this.showMessageBox(null, this.i18n("M128")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.webRequestAssetMonitoring.exportAssetMonitoringSummary(this.filterState()).done((response) => {
                                ScreenHelper.downloadFile(response.fileUrl);
                            }).fail((e) => {
                                this.handleError(e);
                            }).always(() => {
                                this.isBusy(false);
                            });
                            break;
                    }
                });
            break;
            case "cmdUpdate":
                if (Object.keys(this.selectedItems()).length < 1) {
                    this.showMessageBox("", this.i18n("M168")(), BladeDialog.DIALOG_OK);
                    return false;
                }
                this.navigate("bo-ticket-manage-asset-update", {
                    items: this.selectedItems(),
                    filter: this.filterState()
                });
            break;
            case "cmdBatchAction":
                if (Object.keys(this.selectedItems()).length < 1) {
                    this.showMessageBox("", this.i18n("M168")(), BladeDialog.DIALOG_OK);
                    return false;
                }
                this.navigate("bo-ticket-manage-asset-batch-action", {
                    items: this.selectedItems(),
                    filter: this.filterState()
                });
            break;
        }
    }

    generateModel() {

        var model = {
            assetId: this.assetId(),
            businessUnitId: this.businessUnitId(),
            companyId: this.companyId(),
            contractNo: this.contractNo(),
            includeFeature: this.includeFeature(),
            includeSubBusinessUnit: this.includeSubBusinessUnit(),
            license: this.license(),
            mobileNo: this.mobileNo(),
            noSignalDuration: this.noSignalDuration(),
            noTicketOnly: this.noTicketOnly(),
            serviceType: this.serviceType(),
            ticketClosedEndDate: this.ticketClosedEndDate(),
            ticketClosedStartDate: this.ticketClosedStartDate(),
            ticketNo: this.ticketNo(),
            vehicleId: this.vehicleId(),
        }

        return model;
    }
    
    generateColumnDetail() {
        var self = this;
        var columnsDetail = [{
            type: "text",
            width: "80px",
            title: this.i18n("Common_Status")(),
            data: "statusDisplayName",
            className: "dg-body-left",
            onClick: this.viewTicket
        },{
                type: "text",
                width: "",
                title: this.i18n("Ticket_TicketDate")(),
                data: "formatTicketDate",
                className: "dg-body-left",
                onClick: this.viewTicket
            }, {
                type: "text",
                width: "80px",
                title: this.i18n("Ticket_TicketNo")(),
                data: "ticketId",
                className: "dg-body-left",
                onClick: this.viewTicket
            }, {
                type: "text",
                width: "",
                title: this.i18n("Assets_ContractNo")(),
                data: "contractNo",
                className: "dg-body-left",
                onClick: this.viewTicket
            },
            //{
            //    type: "text",
            //    width: "",
            //    title: this.i18n("Common_Company")(),
            //    data: "companyName",
            //    className: "dg-body-left",
            //    onClick: this.viewTicket
            //},
            //{
            //    type: "text",
            //    width: "",
            //    title: this.i18n("Common_BusinessUnit")(),
            //    data: "businessUnitName",
            //    className: "dg-body-left",
            //    onClick: this.viewTicket
            //},
            {
                type: "text",
                width: "",
                title: this.i18n("Common_License")(),
                data: "license",
                className: "dg-body-left",
                onClick: this.viewTicket
            }, {
                type: "text",
                width: "",
                title: this.i18n("Ticket_AssetId")(),
                data: "serialNo",
                className: "dg-body-left",
                onClick: this.viewTicket
            }, {
                type: "text",
                width: "",
                title: this.i18n("Ticket_MobileNo")(),
                data: "mobile",
                className: "dg-body-left",
                onClick: this.viewTicket
            }, {
                type: "text",
                width: "",
                title: this.i18n("Ticket_ServiceType")(),
                data: "serviceTypeDisplayName",
                className: "dg-body-left",
                onClick: this.viewTicket
            },  {
                type: "text",
                width: "",
                title: this.i18n("Ticket_TargetDate")(),
                data: "formatTargetDate",
                className: "dg-body-left",
                onClick: this.viewTicket
            }, {
                type: "text",
                width: "",
                title: this.i18n("Ticket_Owner")(),
                data: "owner",
                className: "dg-body-left",
                onClick: this.viewTicket
            }, {
                type: "text",
                width: "",
                title: this.i18n("Ticket_AssignTo")(),
                data: "assignTo",
                className: "dg-body-left",
                onClick: this.viewTicket
            }, {
                type: "text",
                width: "",
                title: this.i18n("Ticket_CloseDate")(),
                data: "formatCloseDate",
                className: "dg-body-left",
                onClick: this.viewTicket
            }, {
                type: "text",
                width: "",
                title: this.i18n("Ticket_CloseBy")(),
                data: "closeBy",
                className: "dg-body-left",
                onClick: this.viewTicket
            }, {
                type: "text",
                width: "",
                title: this.i18n("Ticket_Remark")(),
                data: "remark",
                className: "dg-body-left",
                onClick: this.viewTicket
            }
        ];
        
        
        return columnsDetail;
    }

    onDetailInit(evt) {
        var self = this;
        var inactive = evt.data.totalActiveTicket;
        var filter = {
            companyId: evt.data.companyId,
            businessUnitId: evt.data.businessUnitId,
            vehicleIds: typeof evt.data.companyId === 'undefined' ? [0] : [evt.data.licenseId],
            IsCloseTicket: true
        }

        if(inactive != ""){
            filter.IsCloseTicket = false;
        }
       
        var active = this.webRequestTicket.listTicketSummary(filter)


        $.when(active).done((responseinactive) => {

                $("<div id='" + self.id + evt.data.businessUnitId + "'/>")
                    .appendTo(evt.detailCell)
                    .kendoGrid({
                        dataSource: responseinactive.items,
                        noRecords: {
                            template: self.i18n("M111")()
                        },
                        pageable: GenerateDataGridExpand.getPageable(false),
                        
                        // set column of kendo grid
                        columns: GenerateDataGridExpand.getColumns(
                            // [{
                            //     type: "text",
                            //     title: this.i18n("Ticket_ActiveTickets")(),
                            //     columns: self.generateColumnDetail(),
                            // }]
                            self.generateColumnDetail()
                        )
                    });

                
            })
            .fail(e => {
                this.handleError(e);
            })
            .always(() => {
                this.isBusy(false);
            });
    }

    formatDetailData(response) {
        for (var i = 0; i < response.length; i++) {
            response[i].renderShipmentDetailColumn = this.renderShipmentDetailColumn;

        }
    }

    renderTicketDate(data) {
        return data.renderShipmentDetailColumn({
            text: data.formatTicketDate,
            color: "#4c4c4c"
        });
    }

    renderShipmentDetailColumn(data) {
        var renderColumn = "";
        if (data.text != "" && data.text != null) {
            renderColumn =
                "<span style='color:" + data.color + ";'>" + data.text + "</span>";
        }
        return renderColumn;
    }

    /**
     * Get report template fields
     */

}

export default {
    viewModel: ScreenBase.createFactory(AssetMonitoringScreen),
    template: templateMarkup
};
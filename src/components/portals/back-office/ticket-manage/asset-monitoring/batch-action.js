import ko from "knockout";
import templateMarkup from "text!./batch-action.html";
import ScreenBase from "../../../screenbase";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebRequestAssetMonitoring from "../../../../../app/frameworks/data/apitrackingcore/webRequestAssetMonitoring";

class AssetMonitoringBatchActionScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Batch Action")());
        this.bladeSize = BladeSize.Small;

        this.itemTickets = ko.observableArray(params.items ? params.items : []);
        this.filter = this.ensureObservable(params.filter, { noSignalDuration: 30 });

        this.disableVehicle = ko.observable(false); 
        this.prepareToCloseSim = ko.observable(false); 
        this.maintenanceBox = ko.observable(false); 

    }
    
    /**
     * webRequest AssetMonitoring.
     * @readonly
     */
    get webRequestAssetMonitoring() {
        return WebRequestAssetMonitoring.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
    }

    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        this.itemTickets.extend({
            arrayRequired: true
        });

        this.validationModel = ko.validatedObservable({
            itemTickets: this.itemTickets
        });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {

        actions.push(this.createAction("actSave", this.i18n("Common_Save")(), "default"));
        actions.push(this.createActionCancel());

    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if(sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);

            let vehicleIds = [];
            let boxIds = [];
            let simIds = [];

            _.map(this.itemTickets(),(val) => { 
                if(val.id){
                    vehicleIds.push(val.id);
                }
                if(val.boxId){
                    boxIds.push(val.boxId);
                }
                if(val.simId){
                    simIds.push(val.simId);
                }
            });

            let info = {
                disableVehicle: this.disableVehicle(),
                pareToCloseSim: this.prepareToCloseSim(),
                maintenanceBox: this.maintenanceBox(),
                vehicleIds: vehicleIds,
                boxIds: boxIds,
                simIds: simIds
            }
            this.webRequestAssetMonitoring.batchAction(info).done(() =>{
                this.publishMessage("add-items-asset",this.filter());
                this.close(true);
            }).fail((e) =>{
                this.handleError(e);
            }).always(() =>{
                this.isBusy(false);
            })
            
            
        }
        
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {

    }

}

export default {
    viewModel: ScreenBase.createFactory(AssetMonitoringBatchActionScreen),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./asset-find.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import WebRequestBox from "../../../../../app/frameworks/data/apitrackingcore/webrequestBox";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import WebRequestAssetMonitoring from "../../../../../app/frameworks/data/apitrackingcore/webRequestAssetMonitoring";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import Utility from "../../../../../app/frameworks/core/utility";
import WebRequestTicket from "../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";

class TicketFindAssetScreen extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeTitle(this.i18n("Common_FindAssets")());

        this.companyOptions = ko.observableArray([]); 
        this.businessUnitOptions = ko.observableArray([]);
        this.vehicleOptions = ko.observableArray([]);
        this.boxOptions = ko.observableArray([]);

        this.companyId = ko.observable();
        this.businessUnitId = ko.observable(null);
        this.vehicleId = ko.observable();
        this.isIncludeSubBU = ko.observable(false);
        this.boxId = ko.observable();
        this.noSignalDuration = ko.observable();
        this.isHaveTicket = ko.observable(false);
        this.isTicketOnly = ko.observable(false);
        this.assetID = ko.observable(null);
        this.contractNo = ko.observable(null);
        this.mobileNo = ko.observable(null);
        this.ServiceTypeOptions = ko.observableArray([]);
        this.serviceId = ko.observable();
        this.ticketId = ko.observable(null);
        this.disabled = ko.observable();
        this.license = ko.observable();

        this.formatSet = "dd/MM/yyyy";
        var addDay = 30;
        this.startDate = ko.observable(new Date());
        this.endDate = ko.observable(new Date());
        this.maxStartDate = ko.observable(new Date());
        this.minStartDate = ko.observable(new Date());
        this.periodDay = ko.pureComputed(() => {
            // can't select date more than current date
            var isDate = Utility.addDays(this.startDate(), addDay);
            let currentDate = parseInt(new Date() / (1000 * 60 * 60 * 24));
            let endDate = parseInt(isDate / (1000 * 60 * 60 * 24)) + 1;
            var calDay = endDate - currentDate;
            if (calDay > 0) {
                isDate = new Date();
            }

            // set date when clear start date and end date
            if (this.startDate() == null) {
                // set end date when clear start date
                if (this.endDate() == null) {
                    isDate = new Date();
                } else {
                    isDate = this.endDate();
                }
                this.maxStartDate(isDate);
                this.minStartDate(isDate);
            } else {
                this.minStartDate(this.startDate())
            }

            return isDate;
        });
        /*
         //datetime
        this.formatSet = "dd/MM/yyyy";
        var addDay = 30;
        var setTimeNow = ("0" + new Date().getHours()).slice(-2) + ":" + ("0" + new Date().getMinutes()).slice(-2);
        this.start = ko.observable(new Date());
        this.end = ko.observable(new Date());
        this.timeStart = ko.observable("00:00");
        this.timeEnd = ko.observable(setTimeNow);
        this.maxDateStart = ko.observable(new Date());
        this.minDateEnd = ko.observable(new Date());

        this.periodDay = ko.pureComputed(() => {
            // can't select date more than current date
            var isDate = Utility.addDays(this.start(), addDay);
            let currentDate = parseInt(new Date() / (1000 * 60 * 60 * 24));
            let endDate = parseInt(isDate / (1000 * 60 * 60 * 24)) + 1;
            var calDay = endDate - currentDate;
            if (calDay > 0) {
                isDate = new Date();
            }

            // set date when clear start date and end date
            if (this.start() == null) {
                // set end date when clear start date
                if (this.end() == null) {
                    isDate = new Date();
                } else {
                    isDate = this.end();
                }
                this.maxDateStart(isDate);
                this.minDateEnd(isDate);
            } else {
                this.minDateEnd(this.start())
            }

            return isDate;
        });
        */
        this.companyId.subscribe(value => {
            this.businessUnitOptions([]);
            this.vehicleOptions([]);
            var currentCompanyId = (typeof value == 'undefined') ? null : value.id;
            this.webRequestBusinessUnit.listBusinessUnitSummary({
                companyId: currentCompanyId, 
                sortingColumns: DefaultSorting.BusinessUnit
            }).done((response) => {
                this.businessUnitOptions(response.items);
            });
        });

        this.disabled = ko.pureComputed (() => {
            this.result = this.isHaveTicket() === true ;
            if(this.isHaveTicket() === false){
                this.startDate(null);
                this.endDate(null);
            }
            return this.result;
        });

        this.selectedEntityBinding = ko.pureComputed(() => {
            this.vehicleOptions([]);
            var businessUnitIds = (this.isIncludeSubBU()) ? Utility.includeSubBU(this.businessUnitOptions(),this.businessUnitId()) : this.businessUnitId();
            var currentCompanyId = (typeof this.companyId() == 'undefined') ? null : this.companyId().id;
            if(this.businessUnitId() != null){
                //Vehicle
                this.webRequestVehicle.listVehicleSummary({
                    companyId: currentCompanyId,
                    businessUnitIds: businessUnitIds,
                    sortingColumns: DefaultSorting.Vehicle
                }).done((response) => {
                    let listVehicle = response.items;
                    //add displayName "Report_All" and id 0
                    // if(Object.keys(listVehicle).length > 0 && listVehicle[0].id !== 0){
                    //     listVehicle.unshift({
                    //         license: this.i18n('Report_All')(),
                    //         id: 0
                    //     });
                    // }
                    this.vehicleOptions(listVehicle);
                });
                // //Box
                this.webRequestBox.listBoxSummary({ 
                    companyId: currentCompanyId,
                    businessUnitIds: businessUnitIds,
                    sortingColumns: DefaultSorting.BoxBySerialNo
                }).done((response) => {
                    let listBox = response.items;
                    //add displayName "Report_All" and id 0
                    if(Object.keys(listBox).length > 0 && listBox[0].id !== 0){
                        listBox.unshift({
                            serialNo: this.i18n('Report_All')(),
                            id: 0
                        });
                    }
                    this.boxOptions(listBox);
                });
            }
            return ' ';
        });

    }

    // /**
    //  * Get WebRequest specific for Box module in Web API access.
    //  * @readonly
    //  */
    get webRequestBox() {
        return WebRequestBox.getInstance();
    }
    /**
     * Get WebRequest specific for Company module in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    get webRequestTicket() {
        return WebRequestTicket.getInstance();
    }

    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }
    get webRequestAssetMonitoring() {
        return WebRequestAssetMonitoring.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();

        // var EnumServiceType = {
        //     types:[Enums.ModelData.EnumResourceType.ServiceType]
        // };

        var d1 = this.webRequestCompany.listCompanySummary({ sortingColumns: DefaultSorting.CompanyByName });
        var d2 = this.webRequestTicket.listServiceTypeSummary();

        $.when(d1,d2).done((r1,r2) => {
            this.companyOptions(r1["items"]);
            this.ServiceTypeOptions(r2["items"]);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        
    }

    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        // this.entityType.extend({ required: true });

        this.validationModel = ko.validatedObservable({
            // entityType: this.entityType
        });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actFind", this.i18n("Common_Find")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actFind") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
            let serviceType = (typeof this.serviceId() == "undefined")? null : this.serviceId().id
            let company = (typeof this.companyId() == "undefined")? null : this.companyId().id
            let businessUnit = (typeof this.businessUnitId() == "undefined")? null : this.businessUnitId()
            let vehicle = (typeof this.vehicleId() == "undefined")? null : this.vehicleId().id
            let assetId = (typeof this.assetID() == "")? null : this.assetID()
            let ticketId = (typeof this.ticketId() == "")?  null : this.ticketId()
            let contractNo = (typeof this.contractNo() == "")? null : this.contractNo()
            let mobileNo = (typeof this.mobileNo() == "")? null : this.mobileNo()
            let nosignal = (typeof this.noSignalDuration() == 0)? 0 : this.noSignalDuration()

            
            if(this.startDate != null){
                var formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.startDate()));
                if(formatSDate == "1970-01-01")
                {
                    formatSDate = null;
                }
            }
            if(this.endDate != null){
                var formatEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.endDate()));
                if(formatEDate == "1970-01-01")
                {
                    formatEDate = null;
                }

            }


            let filterState = {
                serviceType : serviceType,
                companyId: company,
                businessUnitId: businessUnit,
                vehicleId: vehicle,
                mobileNo: mobileNo,
                contractNo: contractNo,
                includeSubBusinessUnit: this.isIncludeSubBU(),
                assetId : assetId,
                ticketId : ticketId,
                noSignalDuration : nosignal,
                noTicketOnly : this.isTicketOnly(),
                ticketClosedStartDate : formatSDate,
                ticketClosedEndDate :  formatEDate,
                license: this.license(),
                haveTicket : this.isHaveTicket()
            };

            this.navigate("bo-ticket-manage-asset", filterState);
            // this.close(true);

        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(TicketFindAssetScreen),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import webRequestTicket from "../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";

class MailManagementScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Medium;
        
        // Set blade title
        this.mode = this.ensureNonObservable(params.mode);
        this.id = this.ensureNonObservable(params.mailId);
        if (this.mode === Screen.SCREEN_MODE_CREATE) {
            this.bladeTitle(this.i18n("Ticket_CreatedMailTemplate")());
        } else if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            this.bladeTitle(this.i18n("Ticket_UpdateMailTemplate")());
        }
        this.mailId = this.ensureNonObservable(params.mailId,-1);
        this.statusId =  this.ensureNonObservable(params.statusId);
        this.selectedStatus = ko.observable();
        this.status =  ko.observableArray([ 
            { status: 'Enable' },
            { status: 'Disable' }
        ]);
        this.templateName = ko.observable("");
        this.subject = ko.observable("");
        this.body = ko.observable("");
        this.items = ko.observableArray([]);
        this.selectedItems = ko.observableArray([]);
        this.order = ko.observable([]);
        this.itemsChanged = ko.observableArray([]);
        this.checkListItemBeforeUpdate = this.ensureNonObservable([]);
        this.isCheckListInvalid = ko.observable();
        this.isCheckListInvalid(false);
        this.mappings = ko.observableArray([]);
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        var dfd = null
        var dfdCreate =  $.Deferred();
        var arrCheckList = new Array();
        switch (this.mode) {
            case Screen.SCREEN_MODE_UPDATE:
                dfd = this.webRequestTicket.getMailManagement(this.id).done((mail) => {
                    if (mail){
                        this.templateName(mail.templateName);
                        this.subject(mail.subject);
                        this.body(mail.body);
                        let Isstatus = ScreenHelper.findOptionByProperty(this.status,"status",mail.status == true? "Enable":"Disable");
              
                        this.selectedStatus(Isstatus);
                       
                        var mappings = mail["mappings"];
                        for (var i = 0; i < mappings.length; i++) {
                            var checkListItem = {
                                ticketMailConfigurationId: mappings[i].ticketMailConfigurationId,
                                fieldNameId: mappings[i].fieldNameId,
                                fieldName: mappings[i].fieldName,
                                isCheck: mappings[i].isCheck,
                                infoStatus: mappings[i].infoStatus,
                                createDate: mappings[i].createDate,
                                fomatCreateDate: mappings[i].fomatCreateDate,
                                createBy: mappings[i].createBy,
                                order: mappings[i].order,
                            };
                            arrCheckList.push(checkListItem);
                        }
                        this.checkListItemBeforeUpdate = arrCheckList;
                        this.items.replaceAll(arrCheckList);
                    }
                });
                $.when(dfd).done(() => {
                    dfd.resolve();
                }).fail((e) => {
                    dfd.reject(e);
                });
                break;
            case Screen.SCREEN_MODE_CREATE:
                this.webRequestTicket.getMailManagement(0).done((mail) => {
                    if (mail) {
                        this.items(mail["mappings"]);
                    }
                });
                $.when(dfdCreate).done(() => {
                    dfdCreate.resolve();
                }).fail((e) => {
                    dfdCreate.reject(e);
                });                
                break;
        }
        return dfd;
        return dfdCreate;
    }

    generateModel () {
        var filterMail = {
            id:this.id,
            templateName: this.templateName(),
            subject: this.subject(),
            body: this.body(),
            status: this.selectedStatus().status == "Enable"? true:false,
        };

        var mappings = new Array();
        var currentIndex = 0
        this.items().forEach((f) => {
                if (this.mode === Screen.SCREEN_MODE_CREATE && f.isCheck){
                    var checkListItem = {
                        ticketMailConfigurationId: f.ticketMailConfigurationId,
                        fieldNameId: f.fieldNameId,
                        fieldName: f.fieldName,
                        isCheck: f.isCheck,
                        infoStatus: f.infoStatus,
                        createDate: f.createDate,
                        fomatCreateDate: f.fomatCreateDate,
                        createBy: f.createBy,
                        order: f.order,
                    };
                    mappings.push(checkListItem);
                }
                else{
                    if (this.checkListItemBeforeUpdate.length > 0){
                        if (this.checkListItemBeforeUpdate[currentIndex].isCheck || f.isCheck) {

                            var infoStatus = null;
                            if (this.checkListItemBeforeUpdate[currentIndex].isCheck && f.isCheck) {
                                infoStatus = Enums.InfoStatus.Update;
                            }
                            else if (!this.checkListItemBeforeUpdate[currentIndex].isCheck && f.isCheck) {
                                infoStatus = Enums.InfoStatus.Add;
                            }
                            else if (this.checkListItemBeforeUpdate[currentIndex].isCheck && !f.isCheck) {
                                infoStatus = Enums.InfoStatus.Delete;
                            }
                            var checkListItem = {
                                ticketMailConfigurationId: f.ticketMailConfigurationId,
                                fieldNameId: f.fieldNameId,
                                fieldName: f.fieldName,
                                isCheck: f.isCheck,
                                createDate: f.createDate,
                                fomatCreateDate: f.fomatCreateDate,
                                createBy: f.createBy,
                                infoStatus: infoStatus,
                                order: f.order,
                            };
                            mappings.push(checkListItem);
                        }
                    }
                }
                currentIndex++;
        });
        filterMail.mappings = mappings;
        return filterMail;
    }
    /**
     * 
     * 
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    setupExtend(){

        this.templateName.extend({
            trackChange: true
        });
        this.subject.extend({
            trackChange: true
        });
        this.body.extend({
            trackChange: true
        });
        this.selectedItems.extend({
            trackChange: true
        });
        this.selectedStatus.extend({
            trackChange: true
        });
        this.isCheckListInvalid.extend({ trackChange: true });
        this.selectedStatus.extend({ required: true });
        this.templateName.extend({ required: true });
        this.subject.extend({ required: true });
        this.body.extend({ required: true });
       
        this.validationModel = ko.validatedObservable({
            status: this.selectedStatus,
            templateName: this.templateName,
            subject: this.subject,
            body: this.body,
            items: this.items
        });

        this.validationModelUpdate = ko.validatedObservable({
            status: this.selectedStatus,
            templateName: this.templateName,
            subject: this.subject,
            body: this.body,
            items: this.items,
        });

           
    }
    get webRequestTicket() {
        return webRequestTicket.getInstance();
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {        
        if (this.mode === Screen.SCREEN_MODE_CREATE) {
            actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
            actions.push(this.createActionCancel());
        }
        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
            actions.push(this.createActionCancel());
        }
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if(this.mode === Screen.SCREEN_MODE_CREATE){
            if(!this.validationModel.isValid()){
                this.validationModel.errors.showAllMessages();
                return ;
            }
        }
        if(this.mode === Screen.SCREEN_MODE_UPDATE){
            if(!this.validationModelUpdate.isValid()){
                this.validationModelUpdate.errors.showAllMessages();
                return ;
            } 
        }
        if (sender.id === "actSave") {
            
            ko.validation.validateObservable(this.items);
            this.isCheckListInvalid(false);
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            var isCheckListInvalid = false;

            this.items().forEach((f) => {
                if (f.isCheck) {
                    if (f.order == null ||f.order == "") {
                        isCheckListInvalid = true;
                    }
                }
            });

            if (isCheckListInvalid) {
                this.isCheckListInvalid(true);
                return;
            }
            var ModelData = this.generateModel();
            
            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.isBusy(true);
                    this.webRequestTicket.createMailManagement(ModelData,true).done((response) => {
                        this.publishMessage("bo-ticket-mail-management-changed",this.id);
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.isBusy(true);
                    this.webRequestTicket.updateMailManagement(ModelData,true).done((response) => {
                        this.publishMessage("bo-ticket-mail-management-changed",this.id);
                 
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });
                    break;
            }
        }
       
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdDelete") {
            this.showMessageBox(null, this.i18n("M165")(), BladeDialog.DIALOG_YESNO).done((button) => {
                switch (button) {
                    case BladeDialog.BUTTON_YES:
                        this.webRequestTicket.deleteMailManagement(this.id).done(() => {
                            this.publishMessage("bo-ticket-mail-management-changed");
                            this.close(true);
                        }).fail((e)=> {
                            this.handleError(e);
                        }).always(()=>{
                            this.isBusy(false);
                        });
                        break;
                }
            });
        }
    }
}

export default {
viewModel: ScreenBase.createFactory(MailManagementScreen),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./vehicle.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import AssetUtility from "../../../company-workspace/asset-utility";

class TicketTransporterManageVehicleScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Medium;
        this.bladeTitle(this.i18n("Vehicles")());
        this.params = this.ensureNonObservable(params, []);
        // this.mode = this.ensureNonObservable(params.mode, Screen.SCREEN_MODE_CREATE);
        this.placeholderFilterText = ko.observable(this.i18n("Common_Search")());
        this.companyOptions = ko.observableArray();
        this.selectedCompany = ko.observable();

        this.name = ko.observable();
        this.position = ko.observable();
        this.dataSource = ko.observableArray([]);

        // if (this.mode === Screen.SCREEN_MODE_UPDATE) {
        //     this.bladeTitle(this.i18n("Update Vehicles")());
        // } else {
        //     this.bladeTitle(this.i18n("Create Vehicles")());
        // }


        // Subscribe Message when signature create/update 
        this.subscribeMessage("ticket-transporter-find-vehicle", (data) => {
            let newDS = _.clone(data);

            _.forEach(newDS, (item)=>{
                let fData = _.filter(this.dataSource(), (o) => {
                    return o.vehicleId == item.vehicleId;
                });
                if(!_.size(fData)){
                    this.dataSource.push(item);
                }
            })

            this.refreshDataGrid();
        });
     
    }

    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
     * Get WebRequest specific for Company module in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    refreshDataGrid(){
        this.dispatchEvent("dg-transporter-vehicle-selected", "refresh");
    }

     /**
     * On Datasource Request Read
     * 
     * @param {any} res
     * @returns
     * 
     * @memberOf TrackWidget
     */
    onDatasourceRequestRead (gridOption) {
        var dfd = $.Deferred();
        var items = this.dataSource();
        // this._items = _.clone(this.dataSource());
        var totalItems = items.length;
        // if(gridOption.filter){
        //     items = AssetUtility.filter(this._items, gridOption.filter);
        //     totalItems = items.length;
        //     // currPage = AssetUtility.currentPage(items, gridOption.page, gridOption.pageSize);
        // }
        var data = AssetUtility.query(items, gridOption.take, gridOption.skip, gridOption.sort);
        let currPage = AssetUtility.currentPage(items, gridOption.page, gridOption.pageSize);

        dfd.resolve({items: data, totalRecords: totalItems, currentPage: currPage});
        return dfd;
    }

    renderRemoveColumn(){
        return AssetUtility.renderRemoveColumn();
    }

    onRemoveAllTrackLocationClick(items, e){
        this.dataSource([]);
        this.refreshDataGrid();
    }

    onRemoveTrackLocationClick(items, e){
        let ds = _.clone(this.dataSource())
        let newDS = _.filter(ds, (o)=>{
            return o.vehicleId != items.vehicleId;
        });
        this.dataSource(newDS);
        this.refreshDataGrid();
    }
    
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        let ds = (this.params.vehicles) ? this.params.vehicles : [];
        this.dataSource(ds);
        
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actOK", this.i18n("Common_OK")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdAdd", this.i18n("Common_Add")(), "svg-cmd-add"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        var self = this;
        if (sender.id === "actOK") {
            this.publishMessage("ticket-transporter-vehicle", this.dataSource());
            this.close();
        }
    } 
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {

            case "cmdAdd":
                this.navigate("bo-ticket-manage-transporter-manage-find-vehicle");
            break;
        }
    }
}
export default {
viewModel: ScreenBase.createFactory(TicketTransporterManageVehicleScreen),
template: templateMarkup
};
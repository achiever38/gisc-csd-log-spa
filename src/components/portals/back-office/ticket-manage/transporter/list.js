﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import Utility from "../../../../../app/frameworks/core/utility";
import AssetUtility from "../../../company-workspace/asset-utility"
import WebRequestTransporter from "../../../../../app/frameworks/data/apitrackingcore/webRequestTransporter";

class TicketTransporterScreen extends ScreenBase {
    constructor(params) {
        super(params);


        this.bladeTitle(this.i18n("Transporters")());
        this.bladeSize = BladeSize.XMedium;
        this.dataSource = ko.observableArray([]);

        this.selectedRow = (data) => {
            this.navigate("bo-ticket-manage-transporter-manage", { data: data, mode: Screen.SCREEN_MODE_UPDATE });
        }

        // Subscribe Message when signature create/update 
        this.subscribeMessage("ticket-transporter-manage-change", (data) => {
            this.dispatchEvent("dg-ticket-transporter", "refresh");
        });
    

    }

    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    get webRequestTransporter() {
        return WebRequestTransporter.getInstance();
    }

    /**
     * On Datasource Request Read
     * 
     * @param {any} res
     * @returns
     * 
     * @memberOf TrackWidget
     */
    onDatasourceRequestRead (gridOption) {
        // var dfd = $.Deferred();
        // var items = this.dataSource();
        // var totalItems = items.length;
        // if(_.isEmpty(gridOption.sort)){
        //     gridOption.sort = [{field: "id", dir: "asc" }];
        // }

        // var data = AssetUtility.query(items, gridOption.take, gridOption.skip, gridOption.sort);
        // dfd.resolve({items: data, totalRecords: totalItems});

        // return dfd;
        var dfd = $.Deferred();
        this.webRequestTransporter.listTransporter(gridOption).done((response) => {
            var data = response["items"];
            dfd.resolve({items: data, totalRecords: response.totalRecords});
        });
        return dfd;
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        // var dfd = $.Deferred();
        // this.webRequestTransporter.listTransporter({}).done((response) => {
        //     var data = response["items"];
        //     this.dataSource(data);
        //     console.log(data);
        //     dfd.resolve();
        // });
        // return dfd;
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {

    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);


    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdCreate":
                this.navigate("bo-ticket-manage-transporter-manage", { mode: Screen.SCREEN_MODE_CREATE });
            break;
        }
    }

    onUnLoad(){
        
    }
    
}

export default {
    viewModel: ScreenBase.createFactory(TicketTransporterScreen),
    template: templateMarkup
};
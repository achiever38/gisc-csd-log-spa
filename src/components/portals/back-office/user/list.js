﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../screenbase";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import WebRequestUser from "../../../../app/frameworks/data/apicore/webRequestUser";
import {Constants, Enums} from "../../../../app/frameworks/constant/apiConstant";
import DefaultSorting from "../../../../app/frameworks/constant/defaultSorting";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";

/**
 * User Management > List 
 * 
 * @class UserListScreen
 * @extends {ScreenBase}
 */
class UserListScreen extends ScreenBase {

    /**
     * Creates an instance of UserListScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Common_Users")());
        this.bladeSize = BladeSize.Medium;

        this.users = ko.observableArray();
        this.filterText = ko.observable();
        this.selectedUser = ko.observable();
        this.recentChangedRowIds = ko.observableArray();
        this.order = ko.observable([[ 0, "asc" ]]);

        // Determine if selecting action is legal, return false to cancel selecting event.
        this._selectingRowHandler = (user) => {
            if(user && WebConfig.userSession.hasPermission(Constants.Permission.UpdateUser_BackOffice)) {
                return this.navigate("bo-user-manage", { mode: "update", userId: user.id });
            }
            return false;
        };

        // Observe change about user in this Journey
        this.subscribeMessage("bo-user-changed", (userId) => {
            // Refresh entire datasource
            this.isBusy(true);
            
            var userFilter = {
                userType: Enums.UserType.BackOffice,
                sortingColumns: DefaultSorting.User
            };
            this.webRequestUser.listUserSummary(userFilter).done((response)=> {
                // TODO: This dispatchEvent is required when DataTable dom is re-bind from switch journey, see TFS #29852
                // this.dispatchEvent("bo-user-list", "refresh", response["items"]);
                this.users.replaceAll(response.items);

                this.recentChangedRowIds.replaceAll([userId]);
            }).fail((e)=> {
                this.handleError(e);
            }).always(()=>{
                this.isBusy(false);
            });

        });
    }

    /**
     * Get WebRequest specific for User module in Web API access.
     * @readonly
     */
    get webRequestUser() {
        return WebRequestUser.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();
        var userFilter = {
            userType: Enums.UserType.BackOffice,
            sortingColumns: DefaultSorting.User
        };
        this.webRequestUser.listUserSummary(userFilter).done((response)=> {
            var users = response["items"];
            this.users(users);
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedUser(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands){
        if(WebConfig.userSession.hasPermission(Constants.Permission.CreateUser_BackOffice)) {
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Handle when button on CommandBar is clicked
     * @param {any} sender
     */
    onCommandClick(sender) {
        if(sender.id == "cmdCreate") {
            this.navigate("bo-user-manage", { mode: "create" });
            this.selectedUser(null);
            this.recentChangedRowIds.removeAll();
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(UserListScreen),
    template: templateMarkup
};
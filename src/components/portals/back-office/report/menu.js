﻿import ko from "knockout";
import templateMarkup from "text!./menu.html";
import ScreenBase from "../../screenbase";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import { Constants } from "../../../../app/frameworks/constant/apiConstant";

/**
 * Display configuration menu based on user permission.
 * @class ConfigurationMenuScreen
 * @extends {ScreenBase}
 */
class ReportMenuScreen extends ScreenBase {

    /**
     * Creates an instance of ConfigurationMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Report")());
        this.bladeMargin = BladeMargin.Narrow;
        this.bladeSize = BladeSize.XSmall;

        this.selectedIndex = ko.observable();

        this._selectingRowHandler = (item) => {
            if (item) {
                return this.navigate(item.id);
            }
            return false;
        };

        this.items = ko.observableArray([]);
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.EmailLogReport)){
            this.items.push({
                text: this.i18n("Email Log")(),
                id: 'bo-report-email-log'
            });
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.DLTReport)){
            this.items.push({
                text: this.i18n("DLT Report")(),
                id: 'bo-report-dlt-report-search'
            });
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.DLTNewInstallation)){
            this.items.push({
                text: this.i18n("DLT New Installation")(),
                id: 'bo-report-dlt-new-installation-search'
            });
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.DLTNextYear)){
            this.items.push({
                text: this.i18n("DLT Next Year")(),
                id: 'bo-report-dlt-next-year-search'
            });
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.DLTLetter)){
            this.items.push({
                text: this.i18n("Letters")(),
                id: 'bo-report-letter-search'
            });
        }
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedIndex(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}

    // /**
    //  * Navigate to inner screens.
    //  * @param {any} extras
    //  */
    // goto (extras) {
    //     this.navigate(extras.id);
    // }
}

export default {
viewModel: ScreenBase.createFactory(ReportMenuScreen),
    template: templateMarkup
};
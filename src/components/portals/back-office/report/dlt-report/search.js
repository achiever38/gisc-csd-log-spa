﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import ScreenBase from "../../../screenbase";
import Utility from "../../../../../app/frameworks/core/utility";

class DLTReport extends ScreenBase{
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Report Filter")());
        this.license = ko.observable();
        this.chassisNo = ko.observable();
        this.dltId = ko.observable();

        //datetime
        this.formatSet = "dd/MM/yyyy";
        //var addDay = 30;
        //var setTimeNow = ("0" + new Date().getHours()).slice(-2) + ":" + ("0" + new Date().getMinutes()).slice(-2);
        this.start = ko.observable(new Date());
        this.maxDateStart = ko.observable(new Date());
        //this.end = ko.observable(new Date());
        this.timeStart = ko.observable("00:00");
        this.timeEnd = ko.observable("23:59");
        //this.minDateEnd = ko.observable(new Date());
        
      
    }

    setupExtend() {
        let self = this;

        this.start.extend({ required: true });
        this.license.extend({
            required : {
                onlyIf : function(){
                    return self.validField();
                }
            }
        });

        this.chassisNo.extend({
            required : {
                onlyIf : function(){
                    return self.validField();
                }
            }
        });

        this.dltId.extend({
            required : {
                onlyIf : function(){
                    return self.validField();
                }
            }
        });

        this.validationModel = ko.validatedObservable({
            dateStart: this.start,
            license : this.license,
            chassisNo : this.chassisNo,
            dltId : this.dltId
        });

    }

    validField(){
        return !this.license() && !this.chassisNo() && !this.dltId();
    }
    onLoad(isFirstLoad) {
        
    }

    buildActionBar(actions) {
        actions.push(this.createAction("actPreview", this.i18n('Report_Preview')()));
    }

    onActionClick(sender) {
        if (sender.id === "actPreview") {
            var formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.start())) + " " + this.timeStart() + ":00";
            var formatEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.start())) + " " + this.timeEnd() + ":00";

            if(this.validationModel.isValid()){
                var filter = {
                    license : this.license(),
                    chassisNo : this.chassisNo(),
                    dltId : this.dltId(),
                    fromDate : formatSDate,
                    toDate : formatEDate
                };

                this.navigate("bo-report-dlt-report-list",filter);

            }
            else{
                this.validationModel.errors.showAllMessages();
            }
        }

    }

    
    onUnload() {

    }

    buildCommandBar(commands) {
    }

    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(DLTReport),
    template: templateMarkup
};

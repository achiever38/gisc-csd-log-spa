﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import { Enums } from "../../../../../app/frameworks/constant/apiConstant";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import Utility from "../../../../../app/frameworks/core/utility";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestDLTSendEmail from "../../../../../app/frameworks/data/apicore/webRequestDLTSendEmail";
import ScreenBase from "../../../screenbase";
import WebRequestContract from "../../../../../app/frameworks/data/apitrackingcore/webRequestContract";


class DLTNewInstallationScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Search DLT New Installation")());

        this.companyOptions = ko.observableArray([]); 
        this.selectedCompany = ko.observable();

        this.contractNoOptions = ko.observableArray([]);
        this.contractNo = ko.observable();

        this.startDate = ko.observable();
        this.endDate = ko.observable();
        this.formatSet = "dd/MM/yyyy";
        this.maxStartDate = ko.observable(new Date());
        this.minStartDate = ko.observable(new Date());

        this.appStatusOptions = ko.observableArray([]);
        this.selectedApp = ko.observable();

        this.vendorInfos = ko.observableArray([]);
        this.selectedVendor = ko.observable();

        // this.periodDay = ko.pureComputed(() => {
        //     // can't select date more than current date
        //     let addDay = 30;
        //     var isDate = Utility.addDays(this.startDate(), addDay);
        //     let currentDate = parseInt(new Date() / (1000 * 60 * 60 * 24));
        //     let endDate = parseInt(isDate / (1000 * 60 * 60 * 24)) + 1;
        //     var calDay = endDate - currentDate;
        //     if (calDay > 0) {
        //         isDate = new Date();
        //     }

        //     // set date when clear start date and end date
        //     if (this.startDate() == null) {
        //         // set end date when clear start date
        //         if (this.endDate() == null) {
        //             isDate = new Date();
        //         } else {
        //             isDate = this.endDate();
        //         }
        //         this.maxStartDate(isDate);
        //         this.minStartDate(isDate);
        //     } else {
        //         this.minStartDate(this.startDate())
        //     }

        //     return isDate;
        // });

        this.selectedCompany.subscribe((val) => {
            if(val){
                this.webRequestContract.listContract({
                    enable: true,
                    companyId: val.id,
                    hiddenContractExpire: true
                }).done((r) => {
                    this.contractNoOptions(r["items"]);
                });
            }
            else{
                this.contractNoOptions([]);
            }
        });
    }

    /**
     * Get WebRequest specific for Company module in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    /**
     * Get WebRequest specific for EnumResource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource(){
        return WebRequestEnumResource.getInstance();
    }

     /**
     * Get WebRequest specific for DLTSendEmail module in Web API access.
     * @readonly
     */
    get webRequestDLTSendEmail(){
        return WebRequestDLTSendEmail.getInstance();
    }

    /**
     * Get WebRequest specific for Contract module in Web API access.
     * @readonly
     */
    get webRequestContract() {
        return WebRequestContract.getInstance()
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();

        var d1 = this.webRequestCompany.listCompanySummary({ sortingColumns: DefaultSorting.CompanyByName });
        var d2 = this.webRequestEnumResource.listEnumResource({ types:[Enums.ModelData.EnumResourceType.TicketAppointmentStatus]});
        var d3 = this.webRequestDLTSendEmail.listvendorsignature();

        $.when(d1,d2,d3).done((r1,r2,r3) => {
            this.companyOptions(r1["items"]);
            this.appStatusOptions(r2["items"]);

            let arrSignature = [];

            for(let key in r3){
                let objSignature = {
                    id: key,
                    name: r3[key]
                }
                arrSignature.push(objSignature);

            }

            this.vendorInfos(arrSignature);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSearch", this.i18n("Common_Search")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        
        if(sender.id == "actSearch"){
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            let filterList = this.generateModel();
            this.navigate("bo-report-dlt-new-installation-list",{filter: filterList});
        }
    }

    setupExtend() {
        this.selectedCompany.extend({required: true});
        //this.contractNo.extend({required: false});
        //this.startDate.extend({required: false});
        //this.endDate.extend({required: false});
        this.selectedVendor.extend({required: true});
        
        this.validationModel = ko.validatedObservable({
            selectedCompany: this.selectedCompany,
            //startDate: this.startDate,
            //endDate: this.endDate,
            //contractNo: this.contractNo,
            selectedVendor: this.selectedVendor
        });       
    } 

    generateModel(){
        return {
            companyId: this.selectedCompany().id,
            AppointmentStatus: this.selectedApp() ? this.selectedApp().value : null,
            contractNumber: this.contractNo() ? this.contractNo().contractNo : null,
            fromInstallDate: this.startDate(),
            toInstallDate: this.endDate(),
            SignatureId:this.selectedVendor().id,
            typeFilter: Enums.ModelData.DLTReportType.NewInstallation,
            showSignature: true
        };
    }
}


export default {
    viewModel: ScreenBase.createFactory(DLTNewInstallationScreen),
    template: templateMarkup
};
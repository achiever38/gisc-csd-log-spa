﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import WebRequestDLTSendEmail from "../../../../../app/frameworks/data/apicore/webRequestDLTSendEmail";
import ScreenBase from "../../../screenbase";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";

class DLTNewInstallationListScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("DLT New Installation")());
        //this.bladeIsMaximized = true;
        this.bladeSize = BladeSize.XLarge;
        this.filter = this.ensureNonObservable(params.filter, {});

        this.apiDataSource = ko.observableArray([]);
        this.selectedItems = ko.observableArray([]);


    }

    /**
     * Get WebRequest specific for Company module in Web API access.
     * @readonly
     */
    get webRequestDLTSendEmail() {
        return WebRequestDLTSendEmail.getInstance();
    }


    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();

        this.apiDataSource({
            read: this.webRequestDLTSendEmail.listDLTSendEmailSummary(this.filter),
        });

        dfd.resolve();

    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {

    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {

    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdPreview", 'Preview', "svg-cmd-search"));
        commands.push(this.createCommand("cmdSendMail", 'Send Mail', "svg-ticket-sentmail"));
    }

    onCommandClick(sender) {

        if (_.size(this.selectedItems()) <= 0) {
            this.showMessageBox("", "Please select at least one item.", BladeDialog.DIALOG_OK);
            return false;
        }
        
        let strJson = JSON.stringify(this.selectedItems());
        //console.log(strJson);
        switch (sender.id) {
            case "cmdPreview":
                let reportSource = {
                    report: "Gisc.Csd.Log.Service.Core.Report.ReportLibrary.DLTSendEmailReport, Gisc.Csd.Log.Service.Core",
                    parameters: { Json: strJson }
                };
                let reportExport = {PDF: false, Excel:false, Word:false, CSV: false};
                this.navigate("cw-report-reportviewer", { reportSource: reportSource, reportName: "DLT New Installation Report" ,reportExport: reportExport});

                break;
            case "cmdSendMail":

                //var dfd = $.Deferred();
                this.isBusy(true);
                let filter = {
                    StrSentmail : strJson
                }
                this.webRequestDLTSendEmail.sendEmail(filter).done((r) => {
                     // เมื่อส่ง mail เสร็จแล้ว จะทำการ refresh item
                     this.dispatchEvent("dltNewInstallation-list", "refresh");
                }).fail((e) => {
                    //dfd.reject();
                    this.handleError(e);
                }).always(() => {
                    this.isBusy(false);
                });
                
                break;
            default:
                break;
        }
    }

    onDatasourceRequestRead(gridOptions) {
        this.isBusy(true);
        let dfd = $.Deferred();
        let filters =  _.assign({}, gridOptions, this.filter);

        this.webRequestDLTSendEmail.listDLTSendEmailSummaryGrid(filters).done((res) => {
            dfd.resolve({
                items: res.items,
                totalRecords: res.totalRecords,
                currentPage: res.currentPage ?  res.currentPage : 1
            });
        }).fail((e) => {
            this.handleError(e);
        }).always(() => {
            this.isBusy(false);
        });


        return dfd;
    }
}


export default {
    viewModel: ScreenBase.createFactory(DLTNewInstallationListScreen),
    template: templateMarkup
};
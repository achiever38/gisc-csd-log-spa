﻿import ko from "knockout";
import templateMarkup from "text!./email-log.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import webRequestEmail from "../../../../../app/frameworks/data/apicore/webRequestEmail";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import webRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";

class EmailLogs extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeSize = BladeSize.Small;
        this.bladeTitle(this.i18n("Report Filter")());

        // View Model
        this.NotificationTypes = ko.observableArray([]);
        this.selectedNotification = ko.observable();

        this.StatusItems = ko.observableArray([]);
        this.selectedStatus = ko.observable();
        this.companyOptions = ko.observableArray([]); 
        this.businessUnitOptions = ko.observableArray([]);
        this.companyId = ko.observable();
        this.businessUnitId = ko.observable(null);
        this.isIncludeSubBU = ko.observable(false);
        //datetime
        this.formatSet = "dd/MM/yyyy";
        var addDay = 30;
        var setTimeNow = ("0" + new Date().getHours()).slice(-2) + ":" + ("0" + new Date().getMinutes()).slice(-2);
        this.start = ko.observable(new Date());
        this.end = ko.observable(new Date());
        this.timeStart = ko.observable("00:00");
        this.timeEnd = ko.observable(setTimeNow);
        this.maxDateStart = ko.observable(new Date());
        this.minDateEnd = ko.observable(new Date());

        this.periodDay = ko.pureComputed(() => {
            // can't select date more than current date
            var isDate = Utility.addDays(this.start(), addDay);
            let currentDate = parseInt(new Date() / (1000 * 60 * 60 * 24));
            let endDate = parseInt(isDate / (1000 * 60 * 60 * 24)) + 1;
            var calDay = endDate - currentDate;
            if (calDay > 0) {
                isDate = new Date();
            }

            // set date when clear start date and end date
            if (this.start() == null) {
                // set end date when clear start date
                if (this.end() == null) {
                    isDate = new Date();
                } else {
                    isDate = this.end();
                }
                this.maxDateStart(isDate);
                this.minDateEnd(isDate);
            } else {
                this.minDateEnd(this.start())
            }

            return isDate;
        });

        this.companyId.subscribe(value => {
            this.businessUnitOptions([]);
            var currentCompanyId = (typeof value == 'undefined') ? null : value.id;
            this.webRequestBusinessUnit.listBusinessUnitSummary({
                companyId: currentCompanyId, 
                sortingColumns: DefaultSorting.BusinessUnit
            }).done((response) => {
                this.businessUnitOptions(response.items);
            });
        });

    }
    setupExtend() {

        this.selectedNotification.extend({ required: true });

        this.validationModel = ko.validatedObservable({
            //selectedNotification: this.selectedNotification
        });

    }
    get webRequestEmail() {
        return webRequestEmail.getInstance();
    }
    get webRequestEnumResource() {
        return webRequestEnumResource.getInstance();
    }

    /**
    * Get WebRequest specific for Company module in Web API access.
    * @readonly
    */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        var filter = { notificationName: [] };
        var filter2 = { values: [], types: [39] };

        var d1 = this.webRequestCompany.listCompanySummary({ sortingColumns: DefaultSorting.CompanyByName });
        var d2 = this.webRequestEmail.listEmailLog(filter);
        var d3 = this.webRequestEnumResource.listEnumResource(filter2)
       $.when(d1,d2,d3).done((r1,r2,r3) => {
           r2.items.unshift({
                value: "",
                notificationName: "All"
            });
           r3.items.unshift({
                 value: 0,
                 displayName: "All"
           });
            this.companyOptions(r1["items"]);
            this.NotificationTypes(r2.items);
            this.StatusItems(r3.items);
        });

    }
    buildActionBar(actions) {
        actions.push(this.createAction("actPreview", this.i18n('Report_Preview')()));
    }

    onActionClick(sender) {
        if (sender.id === "actPreview") {

            //for validate select type
            var reportTitle = '';
            var reportNamespace = '';
            var reportParam = {};
            var formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.start())) + " " + this.timeStart() + ":00";
            var formatEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.end())) + " " + this.timeEnd() + ":00";

            if(this.validationModel.isValid()){
                reportNamespace = 'Gisc.Csd.Service.Report.Library.BackOffice.EmailLogsReport, Gisc.Csd.Service.Report.Library';
                reportTitle = 'Email Logs Report';
    
    
                this.reports = {
                    report: reportNamespace,
                    parameters: {
                        UserID: WebConfig.userSession.id,
                        NotificationName: this.selectedNotification().notificationName,
                        Status: this.selectedStatus().value,
                        SDate: formatSDate,
                        TDate: formatEDate,
                        PrintBy:WebConfig.userSession.fullname,
                        Language: WebConfig.userSession.currentUserLanguage,
                        companyId : (this.companyId()) ? this.companyId().id : 0 ,
                        businessUnitId : (this.businessUnitId()) ? parseInt(this.businessUnitId()): 0 ,
                        includeSubBusinessUnit : this.isIncludeSubBU()
                    }
                };
                this.navigate("cw-report-reportviewer", { reportSource: this.reports, reportName: this.i18n(reportTitle)() });
            }else{
                this.validationModel.errors.showAllMessages();
            }


            

        }
    }

    onUnload() {

    }

    buildCommandBar(commands) {
    }

    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(EmailLogs),
    template: templateMarkup
};
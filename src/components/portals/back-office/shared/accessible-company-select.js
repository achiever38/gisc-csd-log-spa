﻿import ko from "knockout";
import templateMarkup from "text!./accessible-company-select.html";
import ScreenBase from "../../screenbase";
import WebRequestCompany from "../../../../app/frameworks/data/apicore/webRequestCompany";

/**
 * Select accessible companies
 * 
 * @class AccessibleCompaniesSelectScreen
 * @extends {ScreenBase}
 */
class AccessibleCompaniesSelectScreen extends ScreenBase {

    /**
     * Creates an instance of AccessibleCompaniesSelectScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("User_AccessibleCompanies")());

        this.companies = ko.observableArray();
        this.selectedItems = ko.observableArray();
        this.filterText = ko.observable();
        this.order = ko.observable([[ 1, "asc" ]]);

        // Internal member
        this._selectedCompanyIds = this.ensureNonObservable(params.selectedCompanyIds, []);
        this._selectedItemsSubscribe = this.selectedItems.subscribe((comps) => {
            this._selectedCompanyIds = comps.map((obj) => {
                return obj.id;
            });
        });
    }

    /**
     * Get WebRequest specific for User module in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        // Create Deferred object then resolve when the webRequest done
        var dfd = $.Deferred();
        var filter = { name: "" };
        this.webRequestCompany.listCompanySummary(filter).done((response)=> {
            this.companies(response["items"]);

            // If there is _selectedCompanyIds, set pre-selected value
            this._selectedCompanyIds.forEach(function(element) {
                let company = ko.utils.arrayFirst(this.companies(), function(comp) {
                    return comp.id === element;
                });
                if(company) {
                    this.selectedItems.push(company);
                }
            }, this);

            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }

    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        // Setup track change.
        this.selectedItems.extend({
            trackChange: true
        });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actChoose", this.i18n("Common_OK")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        
        if(sender.id === "actChoose") {
            this.publishMessage("bo-accessible-companies-selected", this.selectedItems());
            this.close(true);
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}
}

export default {
    viewModel: ScreenBase.createFactory(AccessibleCompaniesSelectScreen),
    template: templateMarkup
};
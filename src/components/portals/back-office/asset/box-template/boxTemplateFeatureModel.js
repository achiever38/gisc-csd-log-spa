import ko from "knockout";
import {Enums} from "../../../../../app/frameworks/constant/apiConstant";
import Utility from '../../../../../app/frameworks/core/utility';

export default class BoxTemplateFeatureModel {
    constructor(id, name, features, selectedFeature, note, infoStatus = 1,
                eventModelDataType = null,eventDataTypeDisplay = null) {
        this.id = id;
        this.name = name;
        this.features = features;
        this.selectedFeature = selectedFeature;
        this.note = note;
        this.error = "";
        this.infoStatus = infoStatus;
        this.eventModelDataType = eventModelDataType;
        this.eventDataTypeDisplay = eventDataTypeDisplay;
        // Start tracking change after constructed object.
        this._oldHash = Utility.getHashCode(ko.toJSON(this.selectedFeature + this.note));
    }
    isDirty() {
        var currentHash = Utility.getHashCode(ko.toJSON(this.selectedFeature + this.note));
        return (this._oldHash !== currentHash);
    }

    get name () {
        return this._name;
    }
    set name (value) {
        if(_.isNil(value)){
            value = "";
        }
        this._name = value;
    }
    get isValid() {
        return this.error === "";
    }
    get note() {
        return this._note;
    }
    set note(value) {
        if(_.isEmpty(value)){
            value = null;
        }
        this._note = value;
    }
    get selectedFeature() {
        return this._selectedFeature;
    }
    set selectedFeature(value) {
        this._selectedFeature = parseInt(value);
        if(isNaN(this._selectedFeature)) {
            // use default value as empty.
            this._selectedFeature = null;
        }
    }
}
﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import {Constants} from "../../../../../app/frameworks/constant/apiConstant";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import WebRequestBoxTemplate from "../../../../../app/frameworks/data/apitrackingcore/webrequestBoxTemplate";

/**
 * Box Templates List screen.
 * 
 * @class BoxTemplateListScreen
 * @extends {ScreenBase}
 */
class BoxTemplateListScreen extends ScreenBase {

    /**
     * Creates an instance of BoxTemplateListScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Assets_BoxTemplates")());
        this.bladeSize = BladeSize.Medium;

        this.boxTemplates = ko.observableArray();
        this.selectedBoxTemplate = ko.observable();
        this.filterText = ko.observable();
        this.recentChangedRowIds = ko.observableArray();
        this.order = ko.observable([[ 0, "asc" ]]);

        // Service properties.
        this.webRequestBoxTemplate = WebRequestBoxTemplate.getInstance();
        this.webRequestDefaultFilter = {
            sortingColumns: DefaultSorting.BoxTemplate
        };

        // Subscribe for aggregator when user create/update/delete box model.
        this.subscribeMessage("bo-boxtemplate-changed", (boxTemplateId) => {
            this.isBusy(true);
            this.webRequestBoxTemplate.listBoxTemplateSummary(this.webRequestDefaultFilter)
            .done((response) => {
                // Update box model datasource and refresh recent UI.
                this.boxTemplates.replaceAll(response.items);
                this.recentChangedRowIds.replaceAll([boxTemplateId]);
            })
            .fail((e)=> {
                this.handleError(e);
            })
            .always(()=>{
                this.isBusy(false);
            });
        });
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // Do not load web request when blade restore.
        if (!isFirstLoad) {
            return;
        }

        // Loading availble box model from service and return deferred object.
        var dfd = $.Deferred();
        this.webRequestBoxTemplate.listBoxTemplateSummary(this.webRequestDefaultFilter).done((response) => {
            // Update box model datasource.
            this.boxTemplates(response.items);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedBoxTemplate(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.CreateBoxTemplate)){
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdCreate":
                this.navigate("bo-asset-box-template-manage", { mode: "create" });
                break;
        }

        // Clear selected box model on table.
        this.selectedBoxTemplate(null);

        // Clear recent changed.
        this.recentChangedRowIds.removeAll();
    }

     /**
     * Handle when user click to each box model.
     * 
     * @param {any} boxModel
     */
    onSelectingRow(boxTemplate) {
        if(boxTemplate && WebConfig.userSession.hasPermission(Constants.Permission.UpdateBoxTemplate)) {
            // When user has permission to update then open update screen.
            return this.navigate("bo-asset-box-template-manage", { mode: Screen.SCREEN_MODE_UPDATE, id: boxTemplate.id });
        }
        return false;
    }
}

export default {
    viewModel: ScreenBase.createFactory(BoxTemplateListScreen),
    template: templateMarkup
};
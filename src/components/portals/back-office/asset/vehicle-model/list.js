﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestVehicleModel from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicleModel";
import {Constants} from "../../../../../app/frameworks/constant/apiConstant";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";

class VehicleModelListScreen extends ScreenBase {
    
    /**
     * Creates an instance of VehicleModelListScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        // Blade configuration.
        this.bladeTitle(this.i18n("Assets_VehicleModels")());
        this.bladeSize = BladeSize.Medium;

        // Screen properties.
        this.vehicleModels = ko.observableArray();
        this.selectedVehicleModel = ko.observable();
        this.filterText = ko.observable();
        this.recentChangedRowIds = ko.observableArray();
        this.order = ko.observable([[ 0, "asc" ]]);

        // Service properties.
        this.webRequestVehicleModel = WebRequestVehicleModel.getInstance();
        this.defaultVehicleModelFilter = {
            sortingColumns: DefaultSorting.VehicleModel
        };

        // Subscribe for aggregator when user create/update/delete box model.
        this.subscribeMessage("bo-vehicle-model-changed", (vehicleModelId) => {
            this.isBusy(true);
            this.webRequestVehicleModel.listVehicleModelSummary(this.defaultVehicleModelFilter)
            .done((response) => {
                // Update box model datasource and refresh recent UI.
                this.vehicleModels.replaceAll(response.items);
                this.recentChangedRowIds.replaceAll([vehicleModelId]);
            })
            .fail((e) => {
                this.handleError(e);
            })
            .always(() => {
                this.isBusy(false);
            });
        });
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // Do not load web request when blade restore.
        if (!isFirstLoad) {
            return;
        }

        // Loading availble vehicle model from service and return deferred object.
        var dfd = $.Deferred();
        this.webRequestVehicleModel.listVehicleModelSummary(this.defaultVehicleModelFilter).done((response) => {
            // Update vehicle model datasource.
            this.vehicleModels(response.items);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedVehicleModel(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.CreateVehicleModel)) {
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {}

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdCreate":
                this.navigate("bo-asset-vehicle-model-manage", { mode: "create" });
                break;
        }

        // Clear selected box model on table.
        this.selectedVehicleModel(null);

        // Clear recent changed.
        this.recentChangedRowIds.removeAll();
    }

    /**
     * Handle when user click to each box model.
     * 
     * @param {any} VehicleModel
     */
    onSelectingRow(vehicleModel) {
        if(vehicleModel && WebConfig.userSession.hasPermission(Constants.Permission.UpdateVehicleModel)) {
            // When user has permission to update then open update screen.
            return this.navigate("bo-asset-vehicle-model-manage", { mode: Screen.SCREEN_MODE_UPDATE, id: vehicleModel.id });
        }
        return false;
    }
}

export default {
    viewModel: ScreenBase.createFactory(VehicleModelListScreen),
    template: templateMarkup
};
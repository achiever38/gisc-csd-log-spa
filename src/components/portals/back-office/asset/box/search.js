﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import WebRequestVendor from "../../../../../app/frameworks/data/apitrackingcore/webRequestVendor";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestBoxTemplate from "../../../../../app/frameworks/data/apitrackingcore/webRequestBoxTemplate";
import WebRequestBoxStock from "../../../../../app/frameworks/data/apitrackingcore/webRequestBoxStock";
import { Enums } from "../../../../../app/frameworks/constant/apiConstant";
import { BoxFilter } from "./infos";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";

class BoxSearchScreen extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeTitle(this.i18n("Common_Search")());

        this.searchFilter = this.ensureNonObservable($.extend(true, {}, params.searchFilter));

        this.vendor = ko.observable();
        this.company = ko.observable();
        this.businessUnitId = ko.observable(null);
        this.status = ko.observable();
        this.template = ko.observable();
        this.stock = ko.observable();

        this.vendorOptions = ko.observableArray([]);
        this.companyOptions = ko.observableArray([]);
        this.businessUnitOptions = ko.observableArray([]);
        this.statusOptions = ko.observableArray([]);
        this.templateOptions = ko.observableArray([]);
        this.stockOptions = ko.observableArray([]);
    }
    
    /**
     * Get WebRequest specific for Vendor module in Web API access.
     * @readonly
     */
    get webRequestVendor() {
        return WebRequestVendor.getInstance();
    }
    /**
     * Get WebRequest specific for Company module in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }
    /**
     * Get WebRequest specific for Box Template module in Web API access.
     * @readonly
     */
    get webRequestBoxTemplate() {
        return WebRequestBoxTemplate.getInstance();
    }
    /**
     * Get WebRequest specific for Box Stock module in Web API access.
     * @readonly
     */
    get webRequestBoxStock() {
        return WebRequestBoxStock.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        var d0 = $.Deferred();
        var d1 = this.webRequestVendor.listVendor({ sortingColumns: DefaultSorting.Vendor });
        var d2 = this.webRequestCompany.listCompanySummary({ sortingColumns: DefaultSorting.CompanyByName });
        var d3 = this.searchFilter.companyId ? this.webRequestBusinessUnit.listBusinessUnitSummary({ companyId: this.searchFilter.companyId, sortingColumns: DefaultSorting.BusinessUnit }) : null;
        var d4 = this.webRequestEnumResource.listEnumResource({ types: [ Enums.ModelData.EnumResourceType.BoxStatus ], sortingColumns: DefaultSorting.EnumResource });
        var d5 = this.webRequestBoxTemplate.listBoxTemplateSummary({ sortingColumns: DefaultSorting.BoxTemplate });
        var d6 = this.webRequestBoxStock.listBoxStockSummary({ sortingColumns: DefaultSorting.BoxStock });

        $.when(d1, d2, d3, d4, d5, d6).done((r1, r2, r3, r4, r5, r6) => {
            this.vendorOptions(r1["items"]);
            this.companyOptions(r2["items"]);
            if (r3) {
                this.businessUnitOptions(r3["items"]);
            }
            this.statusOptions(r4["items"]);
            this.templateOptions(r5["items"]);
            this.stockOptions(r6["items"]);

            if (this.searchFilter.vendorId) {
                this.vendor(ScreenHelper.findOptionByProperty(this.vendorOptions, "id", this.searchFilter.vendorId));
            }
            if (this.searchFilter.companyId) {
                this.company(ScreenHelper.findOptionByProperty(this.companyOptions, "id", this.searchFilter.companyId));
            }
            if (this.searchFilter.businessUnitId) {
                this.businessUnitId(this.searchFilter.businessUnitId.toString());
            }
            if (this.searchFilter.boxStatus) {
                this.status(ScreenHelper.findOptionByProperty(this.statusOptions, "value", this.searchFilter.boxStatus));
            }
            if (this.searchFilter.boxTemplateId) {
                this.template(ScreenHelper.findOptionByProperty(this.templateOptions, "id", this.searchFilter.boxTemplateId));
            }
            if (this.searchFilter.boxStockId) {
                this.stock(ScreenHelper.findOptionByProperty(this.stockOptions, "id", this.searchFilter.boxStockId));
            }

            d0.resolve();
        }).fail((e) => {
            d0.reject(e);
        });

        $.when(d0).done(() => {
            
            this._changeCompanySubscribe = this.company.subscribe((company) => {
                if (company) {
                    this.isBusy(true);
                    this.businessUnitId(null);

                    this.webRequestBusinessUnit.listBusinessUnitSummary({ companyId: company.id, sortingColumns: DefaultSorting.BusinessUnit }).done((response) => {
                        this.businessUnitOptions.replaceAll(response["items"]);
                        this.isBusy(false);
                    }).fail((e) => {
                        this.isBusy(false);
                        this.handleError(e);
                    });
                }
                else {
                    this.businessUnitOptions.replaceAll([]);
                }
            });

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        })

        return dfd;
    }
    
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSearch", this.i18n("Common_Search")()));
        actions.push(this.createAction("actCancel", this.i18n("Common_Cancel")(), "cancel"));
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdClear", this.i18n("Common_Clear")(), "svg-cmd-clear"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSearch") {
            this.searchFilter = new BoxFilter(
                this.vendor() ? this.vendor().id : null,
                this.company() ? this.company().id : null,
                this.businessUnitId() ? this.businessUnitId() : null,
                this.status() ? this.status().value : null,
                this.template() ? this.template().id : null,
                this.stock() ? this.stock().id : null
            );

            this.publishMessage("bo-asset-box-search-filter-changed", this.searchFilter);
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdClear") {
            this.vendor(null);
            this.company(null);
            this.businessUnitId(null);
            this.status(null);
            this.template(null);
            this.stock(null);
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(BoxSearchScreen),
    template: templateMarkup
};
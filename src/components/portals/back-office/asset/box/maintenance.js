﻿import ko from "knockout";
import templateMarkup from "text!./maintenance.html";
import ScreenBase from "../../../screenbase";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebRequestBoxMaintenance from "../../../../../app/frameworks/data/apitrackingcore/webRequestBoxMaintenance";

class BoxMaintenanceScreen extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeTitle(this.i18n("Assets_BoxMaintenances")());
        this.bladeSize = BladeSize.Medium;

        this.id = this.ensureNonObservable(params.id, -1);


        this.boxMaintenanceList = ko.observableArray([]);
        this.selectedBoxMaintenances = ko.observable(null);
        this.recentChangedRowIds = ko.observableArray([]);
        this.order = ko.observable([[ 1, "desc" ]]);

        this.boxMaintenanceFilter = {boxIds: this.id? [this.id] :[]};


        this._selectingRowHandler = (boxMaintenance) => {
            if(boxMaintenance) {
                return this.navigate("bo-asset-box-manage-maintenance-manage", { 
                    mode: Screen.SCREEN_MODE_UPDATE, 
                    id: boxMaintenance.id,
                    boxId: this.id
                });
            }
            return false;
        };

        this.subscribeMessage("bo-asset-box-manage-maintenance-manage-changed", (boxId) => {

            this.isBusy(true);
            
            this.webRequestBoxMaintenance.listBoxMaintenance(this.boxMaintenanceFilter).done((response)=> {
                var boxMaintenance =  response["items"];
                this.boxMaintenanceList(boxMaintenance);
                this.recentChangedRowIds.replaceAll([boxId]);
            }).fail((e)=> {
                this.handleError(e);
            }).always(()=>{
                this.isBusy(false);
            });
        });
    }

     /**
     * Get WebRequest specific for Box Maintenance module in Web API access.
     * @readonly
     */
    get webRequestBoxMaintenance() {
        return WebRequestBoxMaintenance.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
         if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        this.webRequestBoxMaintenance.listBoxMaintenance(this.boxMaintenanceFilter).done((response) => {
            var boxMaintenanceList = response["items"];
            this.boxMaintenanceList(boxMaintenanceList);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdAdd", this.i18n("Common_Add")(), "svg-cmd-add"));
    }
    
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if(sender.id == "cmdAdd") {
            this.navigate("bo-asset-box-manage-maintenance-manage", { 
                mode: Screen.SCREEN_MODE_CREATE,
                boxId: this.id
             });
            this.selectedBoxMaintenances(null);
            this.recentChangedRowIds.removeAll();
        }
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }

      /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedBoxMaintenances(null);
    }
   
}

export default {
    viewModel: ScreenBase.createFactory(BoxMaintenanceScreen),
    template: templateMarkup
};
class BoxFilter {
    constructor (vendorId = null, companyId = null, businessUnitId = null, boxStatus = null, boxTemplateId = null, boxStockId = null) {
        this.vendorId = vendorId;
        this.companyId = companyId;
        this.businessUnitId = businessUnitId;
        this.boxStatus = boxStatus;
        this.boxTemplateId = boxTemplateId;
        this.boxStockId = boxStockId;
    }
}

export { BoxFilter };

class BoxFeature {
    constructor (boxFeatureId, infoStatus, eventType, event, isInstall, note, featureId, featureName, order, boxId, uniqueId) {
        this.eventType = eventType;
        this.event = event;
        this.isInstall = isInstall;
        this.note = note;
        this.boxId = boxId;
        this.featureId = featureId;
        this.featureName = featureName;
        this.order = order;
        this.infoStatus = infoStatus;
        this.id = boxFeatureId || 0;
        this.uniqueId = uniqueId; // Use for dataTable. Use boxFeatureId for mode update and use boxTemplateFeatureId for mode create
    }
}

export { BoxFeature };
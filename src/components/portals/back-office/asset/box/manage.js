﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import {
    Constants,
    Enums,
    EntityAssociation
} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestBox from "../../../../../app/frameworks/data/apitrackingcore/webrequestBox";
import WebRequestVendor from "../../../../../app/frameworks/data/apitrackingcore/webRequestVendor";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestBoxTemplate from "../../../../../app/frameworks/data/apitrackingcore/webRequestBoxTemplate";
import WebRequestBoxStock from "../../../../../app/frameworks/data/apitrackingcore/webRequestBoxStock";
import Utility from "../../../../../app/frameworks/core/utility";
import {
    BoxFeature
} from "./infos";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import WebRequestOperatorPackage from "../../../../../app/frameworks/data/apitrackingcore/webRequestOperatorPackage"
import webRequestTicket from "../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";
import WebRequestBoxModel from "../../../../../app/frameworks/data/apitrackingcore/webRequestBoxModel";

class BoxManageScreen extends ScreenBase {

    constructor(params) {
        super(params);

        this.bladeSize = BladeSize.Medium;

        this.mode = this.ensureNonObservable(params.mode);
        this.id = this.ensureNonObservable(params.id, 0);

        if (this.mode === Screen.SCREEN_MODE_CREATE) {
            this.bladeTitle(this.i18n("Assets_CreateBox")());
        } else if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            this.bladeTitle(this.i18n("Assets_UpdateBox")());
        }

        this.imei = ko.observable("");
        this.serialNo = ko.observable("");
        this.vendor = ko.observable();
        this.isForRent = ko.observable();
        this.company = ko.observable();
        this.businessUnit = ko.observable(null);
        this.stock = ko.observable();
        this.status = ko.observable();
        this.installedDateTime = ko.observable();
        this.installedDateTimeVisible = ko.pureComputed(() => {
            var boxStatus = this.status();
            return !_.isNil(boxStatus) && boxStatus.value === Enums.ModelData.BoxStatus.Ready;
        });
        this.dateFormat = WebConfig.companySettings.shortDateFormat;
        this.timeFormat = WebConfig.companySettings.shortTimeFormat;
        this.trackInterval = ko.observable("");
        this.adjustTemperature = ko.observable("");
        this.isCalculateDistanceByMileage = ko.observable();
        this.enableMultipath = ko.observable();
        this.template = ko.observable();
        this.inputEvents = ko.observableArray([]);
        this.outputEvents = ko.observableArray([]);
        this.inputAccessories = ko.observableArray([]);
        this.outputAccessories = ko.observableArray([]);

        this.vendorOptions = ko.observableArray([]);
        this.isForRentOptions = ScreenHelper.createYesNoObservableArrayWithDisplayValue();
        this.companyOptions = ko.observableArray([]);
        this.businessUnitOptions = ko.observableArray([]);
        this.stockOptions = ko.observableArray([]);
        this.statusOptions = ko.observableArray([]);
        this.isCalculateDistanceByMileageOptions = ScreenHelper.createYesNoObservableArrayWithDisplayValue();
        this.enableMultipathOptions = ScreenHelper.createYesNoObservableArrayWithDisplayValue();
        this.templateOptions = ko.observableArray([]);
        this.sim = ko.observable();
        this.boxid = ko.observable();


        this._originalFeatures = [];
        this.isTemplateChanged = false;

        this.promotions = ko.observableArray([]);
        this.selectedPromotion = ko.observable();

        this.temp_mobile = ko.observable();
        this.mobileArray = ko.observableArray([]);
        this.selectedMobile = ko.observable();


    }
    WebRequestOperatorPackage
    get webRequestOperatorPackage() {
        return WebRequestOperatorPackage.getInstance();
    }
    /**
     * Get WebRequest specific for Box module in Web API access.
     * @readonly
     */
    get webRequestBox() {
        return WebRequestBox.getInstance();
    }

    /**
     * Get WebRequest specific for Vendor module in Web API access.
     * @readonly
     */
    get webRequestVendor() {
        return WebRequestVendor.getInstance();
    }

    /**
     * Get WebRequest specific for Company module in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * Get WebRequest specific for Box Template module in Web API access.
     * @readonly
     */
    get webRequestBoxTemplate() {
        return WebRequestBoxTemplate.getInstance();
    }

    /**
     * Get WebRequest specific for Box Stock module in Web API access.
     * @readonly
     */
    get webRequestBoxStock() {
        return WebRequestBoxStock.getInstance();
    }

    /**
     * Get WebRequest specific for Box Model module in Web API access.
     * @readonly
     */
    get webRequestBoxModel() {
        return WebRequestBoxModel.getInstance();
    }

    /**
     * Get WebRequest specific for Ticket module in Web API access.
     * @readonly
     */
    get webRequestTicket() {
        return webRequestTicket.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();
        var d0 = $.Deferred(); // for subscribe

        var d1 = this.mode === Screen.SCREEN_MODE_UPDATE ? this.webRequestBox.getBox(this.id, [EntityAssociation.Box.Features, EntityAssociation.Box.BusinessUnit]) : null;
        var d2 = $.Deferred(); //vendor
        var d3 = $.Deferred(); //company
        var d4 = $.Deferred(); //business unit
        var d5 = $.Deferred(); //stock
        var d6 = $.Deferred(); //status
        var d7 = $.Deferred(); //template
        var d8 = $.Deferred();

        $.when(d1).done((r1) => {
            var companyIncludeIds = [];
            var templateIncludeIds = [];
            var box = null;

            if (r1) {
                box = r1;
                this.temp_mobile(box);
                this.imei(box.imei);
                this.serialNo(box.serialNo);
                this.isForRent(ScreenHelper.findOptionByProperty(this.isForRentOptions, "value", box.isForRent));
                if (box.companyId) {
                    companyIncludeIds.push(box.companyId);
                }
                this.installedDateTime(box.installedDate);
                this.trackInterval(box.trackInterval);
                this.adjustTemperature(box.adjustTemperature);
                this.isCalculateDistanceByMileage(ScreenHelper.findOptionByProperty(this.isCalculateDistanceByMileageOptions, "value", box.isCalculateDistanceByMileage));
                this.enableMultipath(ScreenHelper.findOptionByProperty(this.enableMultipathOptions, "value", box.enableMultipath));
                if (box.templateId) {
                    templateIncludeIds.push(box.templateId);
                }
                if (box.features) {

                    this.webRequestBoxModel.listBoxModel({
                        includeAssociationNames: [EntityAssociation.BoxModel.PackageType],
                        sortingColumns: DefaultSorting.BoxModel,
                        ids: [box.boxModelId]
                    }).done((r) => {

                        if (_.size(r["items"])) {

                            // api จะ return มา 1 item เสมอ
                            var boxModelItems = r["items"][0].packageType.items;

                            ko.utils.arrayForEach(box.features, (feature) => {
                                var boxFeature = new BoxFeature(
                                    feature.id,
                                    Enums.InfoStatus.Update,
                                    feature.eventType,
                                    feature.event,
                                    feature.isInstall,
                                    feature.note || "",
                                    feature.featureId,
                                    feature.featureName,
                                    feature.order,
                                    feature.boxId,
                                    feature.id);

                                boxFeature.analogValue = feature.analogValue;
                                boxFeature.eventDataType = feature.eventDataTypeFeature;
                                boxFeature.isSupportAnalog = feature.isSupportAnalog;

                                if (boxFeature.featureId) {
                                    let eventTypeModel = _.find(boxModelItems, (model) => {
                                        return model.name == boxFeature.event && model.order == boxFeature.order
                                               && model.eventType == boxFeature.eventType;
                                    }, boxFeature)
                                    boxFeature.eventModelDataType = eventTypeModel.eventDataType;
                                    boxFeature.eventDataTypeDisplay = eventTypeModel.eventDataTypeDisplayName;
                                } else {
                                    boxFeature.eventModelDataType = null;
                                    boxFeature.eventDataTypeDisplay = null;
                                }

                                this._originalFeatures.push($.extend(true, {}, boxFeature));

                                switch (feature.eventType) {
                                    case Enums.ModelData.BoxFeatureEventType.InputEvent:
                                        this.inputEvents.push(boxFeature);
                                        break;
                                    case Enums.ModelData.BoxFeatureEventType.OutputEvent:
                                        this.outputEvents.push(boxFeature);
                                        break;
                                    case Enums.ModelData.BoxFeatureEventType.InputAccessory:
                                        this.inputAccessories.push(boxFeature);
                                        break;
                                    case Enums.ModelData.BoxFeatureEventType.OutputAccessory:
                                        this.outputAccessories.push(boxFeature);
                                        break;
                                }
                            });
                        }
                    });

                }
            } else {
                this.isCalculateDistanceByMileage(ScreenHelper.findOptionByProperty(this.isCalculateDistanceByMileageOptions, "value", false));
                this.enableMultipath(ScreenHelper.findOptionByProperty(this.enableMultipathOptions, "value", true));
            }

            this.webRequestVendor.listVendor({
                sortingColumns: DefaultSorting.Vendor
            }).done((r2) => {
                Utility.applyFormattedPropertyToCollection(r2["items"], "displayText", "{0} ({1})", "name", "vendorId");
                this.vendorOptions(r2["items"]);
                if (box && box.vendorId) {
                    this.vendor(ScreenHelper.findOptionByProperty(this.vendorOptions, "id", box.vendorId));
                }
                d2.resolve();
            }).fail((e) => {
                d2.reject(e);
            });

            this.webRequestCompany.listCompanySummary({
                enable: true,
                includeIds: companyIncludeIds,
                sortingColumns: DefaultSorting.CompanyByName
            }).done((r3) => {
                this.companyOptions(this.formatDisplayTextNameCode(r3["items"]));
                if (box && box.companyId) {
                    this.company(ScreenHelper.findOptionByProperty(this.companyOptions, "id", box.companyId));
                }
                d3.resolve();
            }).fail((e) => {
                d3.reject(e);
            });

            if (box && box.companyId) {
                this.webRequestBusinessUnit.listBusinessUnitSummary({
                    companyId: box.companyId,
                    isIncludeFullPath: true,
                    sortingColumns: DefaultSorting.BusinessUnit
                }).done((r4) => {
                    Utility.applyFormattedPropertyToCollection(r4["items"], "displayText", "{0} ({1})", "name", "fullCode");
                    this.businessUnitOptions(r4["items"]);
                    if (box && box.businessUnitId) {
                        this.businessUnit(box.businessUnitId.toString());
                    }
                    d4.resolve();
                }).fail((e) => {
                    d4.reject(e);
                });
            } else {
                d4.resolve();
            }

            this.webRequestBoxStock.listBoxStock({
                sortingColumns: DefaultSorting.BoxStock
            }).done((r5) => {
                this.stockOptions(this.formatDisplayTextNameCode(r5["items"]));
                if (box && box.boxStockId) {
                    this.stock(ScreenHelper.findOptionByProperty(this.stockOptions, "id", box.boxStockId));
                }
                d5.resolve();
            }).fail((e) => {
                d5.reject(e);
            });

            this.webRequestEnumResource.listEnumResource({
                types: [Enums.ModelData.EnumResourceType.BoxStatus],
                sortingColumns: DefaultSorting.EnumResource
            }).done((r6) => {
                Utility.applyFormattedPropertyToCollection(r6["items"], "displayText", "{0} ({1})", "displayName", "value");
                this.statusOptions(r6["items"]);
                if (box && box.boxStatus) {
                    this.status(ScreenHelper.findOptionByProperty(this.statusOptions, "value", box.boxStatus));
                } else {
                    this.status(ScreenHelper.findOptionByProperty(this.statusOptions, "value", Enums.ModelData.BoxStatus.Idle));
                }
                d6.resolve();
            }).fail((e) => {
                d6.reject(e);
            });

            this.webRequestBoxTemplate.listBoxTemplate({
                enable: true,
                includeIds: templateIncludeIds,
                IncludeAssociationNames: [EntityAssociation.BoxTemplate.Features],
                sortingColumns: DefaultSorting.BoxTemplate
            }).done((r7) => {
                this.templateOptions(this.formatDisplayTextNameCode(r7["items"]));
                if (box && box.templateId) {
                    this.template(ScreenHelper.findOptionByProperty(this.templateOptions, "id", box.templateId));
                }
                d7.resolve();
            }).fail((e) => {
                d7.reject(e);
            });
            this.webRequestOperatorPackage.listOperatorPackage({
                companyId: WebConfig.userSession.currentCompanyId,
                sortingColumns: DefaultSorting.OperatorPackageByOperatorPromotion
            }).done((r8) => {
                this.promotions(r8["items"])
                if (box && box.operatorId) {
                    this.selectedPromotion(ScreenHelper.findOptionByProperty(this.promotions, "id", box.operatorId));
                }
                d8.resolve();
            }).fail((e) => {
                d8.reject(e);
            });

            if (box && box.id) {
                this.sim(box.simId);
                this.boxid(box.id);
            }

        }).fail((e) => {
            d2.reject(e);
            d3.reject(e);
            d4.reject(e);
            d5.reject(e);
            d6.reject(e);
            d7.reject(e);
            d8.reject(e);
        });


        $.when(d2, d3, d4, d5, d6, d7, d8).done(() => {
            d0.resolve();
        }).fail((e) => {
            d0.reject(e);
        });

        $.when(d0).done(() => {

            this._changeCompanySubscribe = this.company.subscribe((company) => {
                if (company) {
                    this.isBusy(true);
                    this.stock(null);
                    this.businessUnit(null);

                    this.webRequestBusinessUnit.listBusinessUnitSummary({
                        companyId: company.id,
                        isIncludeFullPath: true,
                        sortingColumns: DefaultSorting.BusinessUnit
                    }).done((response) => {
                        Utility.applyFormattedPropertyToCollection(response["items"], "displayText", "{0} ({1})", "name", "fullCode");
                        this.businessUnitOptions.replaceAll(response["items"]);
                        this.isBusy(false);
                    }).fail((e) => {
                        this.isBusy(false);
                        this.handleError(e);
                    });
                } else {
                    this.businessUnitOptions.replaceAll([]);
                }
            });

            this._changeStockSubscribe = this.stock.subscribe((stock) => {
                if (stock) {
                    this.company(null);
                }
            });

            this._changeStatusSubscribe = this.status.subscribe((status) => {
                if (_.isNil(status) || status.value != Enums.ModelData.BoxStatus.Ready) {
                    this.installedDateTime(null);
                }
            });

            this._changeTemplateSubscribe = this.template.subscribe((template) => {
                this.isTemplateChanged = true; // set this to true to check confirm uninstall feature

                this.inputEvents.removeAll();
                this.outputEvents.removeAll();
                this.inputAccessories.removeAll();
                this.outputAccessories.removeAll();

                var inputEvents = [];
                var outputEvents = [];
                var inputAccessories = [];
                var outputAccessories = [];
                if (template) {

                    this.webRequestBoxModel.listBoxModel({
                        includeAssociationNames: [EntityAssociation.BoxModel.PackageType],
                        sortingColumns: DefaultSorting.BoxModel,
                        ids: [template.modelId]
                    }).done((r) => {

                        if (_.size(r["items"])) {
                            // api จะ return มา 1 item เสมอ
                            var boxModelItems = r["items"][0].packageType.items;

                            ko.utils.arrayForEach(template.features, (templateFeature) => {
                                var boxFeature = new BoxFeature(
                                    0,
                                    Enums.InfoStatus.Add,
                                    templateFeature.eventType,
                                    templateFeature.event,
                                    templateFeature.featureId !== null,
                                    templateFeature.note || "",
                                    templateFeature.featureId,
                                    templateFeature.featureName,
                                    templateFeature.order,
                                    this.id,
                                    templateFeature.id
                                );
                                
                                boxFeature.eventDataType = templateFeature.eventDataType;
                                boxFeature.analogValue = templateFeature.analogValue;
                                boxFeature.isSupportAnalog = templateFeature.isSupportAnalog;

                                if (boxFeature.featureId) {
                                    let eventTypeModel = _.find(boxModelItems, (model) => {
                                        return model.name == boxFeature.event && model.order == boxFeature.order
                                            && model.eventType == boxFeature.eventType;
                                    }, boxFeature);

                                    boxFeature.eventModelDataType = eventTypeModel.eventDataType;
                                    boxFeature.eventDataTypeDisplay = eventTypeModel.eventDataTypeDisplayName;
                                } else {
                                    boxFeature.eventModelDataType = null;
                                    boxFeature.eventDataTypeDisplay = null;
                                }

                                switch (templateFeature.eventType) {
                                    case Enums.ModelData.BoxFeatureEventType.InputEvent:
                                        inputEvents.push(boxFeature);
                                        break;
                                    case Enums.ModelData.BoxFeatureEventType.OutputEvent:
                                        outputEvents.push(boxFeature);
                                        break;
                                    case Enums.ModelData.BoxFeatureEventType.InputAccessory:
                                        inputAccessories.push(boxFeature);
                                        break;
                                    case Enums.ModelData.BoxFeatureEventType.OutputAccessory:
                                        outputAccessories.push(boxFeature);
                                        break;
                                }
                            });
                            this.inputEvents(inputEvents);
                            this.outputEvents(outputEvents);
                            this.inputAccessories(inputAccessories);
                            this.outputAccessories(outputAccessories);
                        }
                    });
                }

                // If subscribe changed from control try to set default value validationModel might not be setup yet.
                if (this.validationModel) {
                    // Manually clear error for serialNo when change template 
                    if (!this.validationModel.isValid()) {
                        this.clearError(this.serialNo);
                    }
                }
            });

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        this.selectedPromotion.subscribe((res) => {
            this.webRequestTicket.listSimWithFreetId({
                operatorId: res.id,
                boxId: this.boxid()
            }).done((result) => {
                this.mobileArray(result["items"])
                if (this.sim()) {
                    this.selectedMobile(ScreenHelper.findOptionByProperty(this.mobileArray, "id", this.sim()))
                }

            });

        })
        return dfd;
    }

    setupExtend() {
        this.imei.extend({
            trackChange: true
        });
        this.serialNo.extend({
            trackChange: true
        });
        this.vendor.extend({
            trackChange: true
        });
        this.isForRent.extend({
            trackChange: true
        });
        this.company.extend({
            trackChange: true
        });
        this.businessUnit.extend({
            trackChange: true
        });
        this.stock.extend({
            trackChange: true
        });
        this.status.extend({
            trackChange: true
        });
        this.installedDateTime.extend({
            trackChange: true
        });
        this.trackInterval.extend({
            trackChange: true
        });
        this.adjustTemperature.extend({
            trackChange: true
        });
        this.isCalculateDistanceByMileage.extend({
            trackChange: true
        });
        this.enableMultipath.extend({
            trackChange: true
        });
        this.template.extend({
            trackChange: true
        });
        this.inputEvents.extend({
            trackArrayChange: true
        });
        this.outputEvents.extend({
            trackArrayChange: true
        });
        this.inputAccessories.extend({
            trackArrayChange: true
        });
        this.outputAccessories.extend({
            trackArrayChange: true
        });

        // Validation
        this.imei.extend({
            required: true,
            serverValidate: {
                params: "Imei",
                message: this.i18n("M010")()
            }
        });
        this.serialNo.extend({
            required: true,
            serverValidate: {
                params: "SerialNo",
                message: this.i18n("M010")()
            }
        });
        this.vendor.extend({
            required: true
        });
        this.isForRent.extend({
            required: true
        });
        this.company.extend({
            required: {
                onlyIf: () => {
                    return _.isNil(this.stock());
                }
            }
        });
        this.businessUnit.extend({
            required: {
                onlyIf: () => {
                    return !_.isNil(this.company());
                }
            }
        });
        this.stock.extend({
            required: {
                onlyIf: () => {
                    return _.isNil(this.company());
                }
            }
        });
        this.status.extend({
            required: true
        });
        this.installedDateTime.extend({
            required: {
                onlyIf: () => {
                    var boxStatus = this.status();
                    return !_.isNil(boxStatus) && boxStatus.value === Enums.ModelData.BoxStatus.Ready;
                }
            }
        });
        this.trackInterval.extend({
            required: true,
            min: 1
        });
        this.adjustTemperature.extend({
            min: 0
        });
        this.isCalculateDistanceByMileage.extend({
            required: true
        });
        this.enableMultipath.extend({
            required: true
        });
        this.template.extend({
            required: true
        });

        this.validationModel = ko.validatedObservable({
            imei: this.imei,
            serialNo: this.serialNo,
            vendor: this.vendor,
            isForRent: this.isForRent,
            company: this.company,
            businessUnit: this.businessUnit,
            stock: this.stock,
            status: this.status,
            installedDateTime: this.installedDateTime,
            trackInterval: this.trackInterval,
            adjustTemperature: this.adjustTemperature,
            isCalculateDistanceByMileage: this.isCalculateDistanceByMileage,
            enableMultipath: this.enableMultipath,
            template: this.template
        });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.UpdateBox)) {
            actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
            actions.push(this.createAction("actCancel", this.i18n("Common_Cancel")(), "cancel"));
        }
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (this.mode === Screen.SCREEN_MODE_UPDATE) {

            if (WebConfig.userSession.hasPermission(Constants.Permission.UpdateBox)) {
                commands.push(this.createCommand("cmdMaintenances", this.i18n("Common_Maintenances")(), "svg-cmd-maintenance"));
            }

            if (WebConfig.userSession.hasPermission(Constants.Permission.DeleteBox)) {
                commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
            }
        }
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {

            // Always trigger change value for field serialNo for case validation does not work when clear error message after change template.
            this.serialNo.valueHasMutated();

            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            let findError = null;
                if(_.size(this.inputEvents())){
                    // ถ้ามี error ของ textbox support analog จะ return
                    findError = this.findErrorSupportAnalog(this.inputEvents());
                    if(findError) return;
                }
                if(_.size(this.inputAccessories())){
                     // ถ้ามี error ของ textbox support analog จะ return
                     findError = this.findErrorSupportAnalog(this.inputAccessories());
                     if(findError) return;
                }

            this.isBusy(true);

            var model = this.generateModel();
            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestBox.createBox(model, true).done((box) => {
                        this.isBusy(false);
                        this.publishMessage("bo-asset-box-changed", box.id);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);
                        this.handleError(e);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:




                    if (this.confirmUninstallFeature()) {
                        this.isBusy(false);
                        this.showMessageBox(null, this.i18n("M015")(), BladeDialog.DIALOG_YESNO).done((button) => {
                            switch (button) {
                                case BladeDialog.BUTTON_YES:
                                    var isChangeMobileNumber = (model.simId != this.temp_mobile().simId) ? true : false;
                                    if (isChangeMobileNumber) {
                                        this.showMessageBox(null, this.i18n("M176", [this.temp_mobile().mobileNo]),
                                            BladeDialog.DIALOG_YESNO).done((button) => {
                                            switch (button) {
                                                case BladeDialog.BUTTON_YES:
                                                    this.isBusy(true);
                                                    model.isClosed = true;
                                                    this.updateBox(model);
                                                    break;
                                                case BladeDialog.BUTTON_NO:
                                                    this.isBusy(true);
                                                    model.isClosed = false;
                                                    this.updateBox(model);
                                                    break;
                                            }
                                        });
                                    } else {
                                        this.updateBox(model);
                                    }
                                    break;
                            }
                        })

                    } else {

                        var isChangeMobileNumber = (model.simId != this.temp_mobile().simId) ? true : false;
                        if (isChangeMobileNumber) {
                            this.showMessageBox(null, this.i18n("M176", [this.temp_mobile().mobileNo]),
                                BladeDialog.DIALOG_YESNO).done((button) => {
                                switch (button) {
                                    case BladeDialog.BUTTON_YES:
                                        this.isBusy(true);
                                        model.isClosed = true;
                                        this.updateBox(model);
                                        break;
                                    case BladeDialog.BUTTON_NO:
                                        this.isBusy(false);
                                        model.isClosed = false;
                                        this.updateBox(model);
                                        break;
                                }
                            });
                        } else {
                            this.updateBox(model);
                        }



                    }
                    break;


            }
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdMaintenances":
                this.navigate("bo-asset-box-manage-maintenance", {
                    id: this.id
                });
                break;
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M035")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.webRequestBox.deleteBox(this.id).done(() => {
                                this.isBusy(false);
                                this.publishMessage("bo-asset-box-changed", this.id);
                                this.close(true);
                            }).fail((e) => {
                                this.isBusy(false);
                                this.handleError(e);
                            });
                            break;
                    }
                });
                break;
        }
    }

    /**
     * Generate model for webRequest
     * @returns
     */
    generateModel() {
        var model = {
            imei: this.imei(),
            serialNo: this.serialNo(),
            vendorId: this.vendor().id,
            isForRent: this.isForRent().value,
            boxStatus: this.status().value,
            trackInterval: this.trackInterval(),
            adjustTemperature: this.adjustTemperature(),
            installedDate: this.installedDateTime(),
            isCalculateDistanceByMileage: this.isCalculateDistanceByMileage().value,
            enableMultipath: this.enableMultipath().value,
            businessUnitId: this.businessUnit() ? this.businessUnit() : null,
            companyId: this.company() ? this.company().id : null,
            boxStockId: this.stock() ? this.stock().id : null,
            templateId: this.template().id,
            features: [],
            infoStatus: this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update,
            id: this.id,
            simId: this.selectedMobile().id,
            operatorId: this.selectedPromotion()
        };

        model.features = $.extend(true, [], [].concat(this.inputEvents(), this.outputEvents(), this.inputAccessories(), this.outputAccessories()));

        return model;
    }

    /**
     * Check to display message M015 to confirm if there is at least 1 feature uninstall
     * @returns
     */
    confirmUninstallFeature() {
        var confirmUninstallFeature = false;

        // find install feature ids from original
        var originalInstalledFeatureIds = [];
        ko.utils.arrayForEach(this._originalFeatures, (originalFeature) => {
            if (originalFeature.isInstall === true) {
                originalInstalledFeatureIds.push(originalFeature.id);
            }
        });
        if (this.isTemplateChanged) {
            if (originalInstalledFeatureIds.length > 0) {
                // if found uninstall feature, confirm M015
                confirmUninstallFeature = true;
            }
        } else {
            // find install feature ids from update
            var updateFeatures = $.extend(true, [], [].concat(this.inputEvents(), this.outputEvents(), this.inputAccessories(), this.outputAccessories()));
            var updateInstalledFeatureIds = [];
            ko.utils.arrayForEach(updateFeatures, (updateFeature) => {
                if (updateFeature.isInstall === true) {
                    updateInstalledFeatureIds.push(updateFeature.id);
                }
            });

            // find difference between original and update
            var differences = ko.utils.compareArrays(originalInstalledFeatureIds.sort(), updateInstalledFeatureIds.sort());

            // select first item which has been uninstall from original
            var uninstallFeature = ko.utils.arrayFirst(differences, (difference) => {
                return difference.status === "deleted";
            });

            // if found uninstall feature, confirm M015
            if (uninstallFeature) {
                confirmUninstallFeature = true;
            }
        }

        return confirmUninstallFeature;
    }

    /**
     * Call webRequest to updateBox
     * @param {any} model
     */
    updateBox(model) {
        this.webRequestBox.updateBox(model).done(() => {
            this.isBusy(false);
            this.publishMessage("bo-asset-box-changed", this.id);
            this.close(true);
        }).fail((e) => {
            this.isBusy(false);
            this.handleError(e);
        });
    }

    /**
     * 
     * Format displayText "name (code)"
     * @param {any} items
     * @returns
     * 
     * @memberOf BoxManageScreen
     */
    formatDisplayTextNameCode(items) {
        ko.utils.arrayForEach(items, (item) => {
            Utility.applyFormattedPropertyPredicate(item, "displayText", (x) => {
                if (!_.isEmpty(x.code)) {
                    return "{0} ({1})"
                }
                return "{0}"
            }, "name", "code");
        });
        return items;
    }

    onInputEventDataSourceChanged(){
        let tmpInputEvents = _.clone(this.inputEvents());
        let tmpInputAccessories = _.clone(this.inputAccessories());
        this.inputEvents([]);
        this.inputAccessories([]);

        _.forEach(tmpInputEvents,(inputItem) => {
            if(inputItem.analogValue != null){
                this.validAnalogVal(inputItem);
            }
        });
        _.forEach(tmpInputAccessories,(inputItem) => {
            if(inputItem.analogValue != null){
                this.validAnalogVal(inputItem);
            }
        });
        this.inputEvents(tmpInputEvents);
        this.inputAccessories(tmpInputAccessories);
    }

    validAnalogVal(val){
            let analogVal = parseFloat(val.analogValue);
            if(_.isNaN(analogVal)){
                val.errorAnalogVal = val.analogValue == "" ? this.i18n("M001")() : this.i18n("M065")(); //invalid
            } else {
                if (analogVal > 99.99) {
                    val.analogValue = null;
                    val.errorAnalogVal = this.i18n("M001")(); //required
                } else {
                    val.errorAnalogVal = null;
                    val.analogValue = _.round(analogVal, 2);
                }
            }
    }
    
    findErrorSupportAnalog(items){
        return _.find(items,(item) => { return item.errorAnalogVal});
    }
}

export default {
    viewModel: ScreenBase.createFactory(BoxManageScreen),
    template: templateMarkup
};
import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../../screenbase";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import {Constants, Enums} from "../../../../../../app/frameworks/constant/apiConstant";
import DefaultSorting from "../../../../../../app/frameworks/constant/defaultSorting";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import WebRequestADASModel from "../../../../../../app/frameworks/data/apitrackingcore/WebRequestADASModel";
import * as Screen from "../../../../../../app/frameworks/constant/screen";

/**
 * User Management > List 
 * 
 * @class UserListScreen
 * @extends {ScreenBase}
 */
class AssetADASModelListScreen extends ScreenBase {

    /**
     * Creates an instance of UserListScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle("ADAS Model List");
        this.bladeSize = BladeSize.Large;

        this.order = ko.observable([[0, "asc"]]);
        this.filterText = ko.observable();
        this.itemModel = ko.observableArray([]);
        this.recentChangedRowIds = ko.observableArray();

        this._selectingRowHandler = (data) => {
            if (WebConfig.userSession.hasPermission(Constants.Permission.ADASModelUpdate)) {
                data.mode = "update";
                return this.navigate("bo-asset-adas-model-manage", data);
            }
        };

        this.subscribeMessage("bo-asset-adas-models-changed", id => {
            var dfd = $.Deferred();
            this.webRequestADASModel.listADASModelSummary().done((response) => {
                this.itemModel.replaceAll(response["items"]);
                this.recentChangedRowIds.replaceAll([id]);
                dfd.resolve();
            }).fail((e) => {
                dfd.reject(e);
            });
            return dfd;
        });


    }


    get webRequestADASModel() {
        return WebRequestADASModel.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;
        var dfd = $.Deferred();

        this.webRequestADASModel.listADASModelSummary().done((r) => {
            this.itemModel(r["items"]);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.ADASModelCreate)) {
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Handle when button on CommandBar is clicked
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id == "cmdCreate") {
            this.navigate("bo-asset-adas-model-manage", { mode : Screen.SCREEN_MODE_CREATE });
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(AssetADASModelListScreen),
    template: templateMarkup
};
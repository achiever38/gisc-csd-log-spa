import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../../screenbase";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import {Constants, Enums} from "../../../../../../app/frameworks/constant/apiConstant";
import DefaultSorting from "../../../../../../app/frameworks/constant/defaultSorting";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import WebRequestADAS from "../../../../../../app/frameworks/data/apitrackingcore/webRequestADAS";

/**
 * User Management > List 
 * 
 * @class UserListScreen
 * @extends {ScreenBase}
 */
class AssetADASListScreen extends ScreenBase {

    /**
     * Creates an instance of UserListScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle("ADAS Device");
        this.bladeSize = BladeSize.Large;

        this.adsSummary = ko.observableArray();
        this.filterText = ko.observable();
        this.selectedADS = ko.observable();
        this.recentChangedRowIds = ko.observableArray();
        this.order = ko.observable([[ 0, "asc" ]]);

        this.subscribeMessage("bo-asset-adas-device-update", id => {
            var dfd = $.Deferred();
            var adsSummaryFilter = {};
            this.webRequestADAS.listADASSummary(adsSummaryFilter).done((response)=> {
                var ads = response["items"];
                this.adsSummary.replaceAll(ads);
                this.recentChangedRowIds.replaceAll([id]);
                dfd.resolve();
            }).fail((e)=> {
                dfd.reject(e);
            });
            return dfd;
        });
        
        this._selectingRowHandler = (data) => {
            if(WebConfig.userSession.hasPermission(Constants.Permission.ADASDeviceUpdate)){
                data.mode = "update";
                return this.navigate("bo-asset-adas-manage", data);
            }
        };
    }

    initTable(){
        var dfd = $.Deferred();
        var adsSummaryFilter = {};
        this.webRequestADAS.listADASSummary(adsSummaryFilter).done((response)=> {
            var ads = response["items"];
            this.adsSummary.replaceAll(ads);
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }

    get webRequestADAS() {
        return WebRequestADAS.getInstance();
    }


    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;
        var dfd = $.Deferred();
        this.initTable().done((response)=> {
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands){
        if(WebConfig.userSession.hasPermission(Constants.Permission.ADASDeviceCreate)){
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
      
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Handle when button on CommandBar is clicked
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdCreate":
                this.navigate("bo-asset-adas-manage", {
                    mode: "create"
                });
                break;
        }
    }
}

export default {
viewModel: ScreenBase.createFactory(AssetADASListScreen),
    template: templateMarkup
};
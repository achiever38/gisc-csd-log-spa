﻿import ko from "knockout";
import ScreenBaseWidget from "../../../../screenbasewidget";
import templateMarkup from "text!./confirm-dialog.html";
import * as Screen from "../../../../../../app/frameworks/constant/screen";
import WebRequestADAS from "../../../../../../app/frameworks/data/apitrackingcore/webRequestADAS";

class ADASConfirmWidget extends ScreenBaseWidget {
    constructor(params) {
        super(params);

        this.params = this.ensureNonObservable(params);

        this.isRadioADAS = ko.observable('new');

    }

    /**
     * Get WebRequest specific for ADAS module in Web API access.
     * @readonly
     */
    get webRequestADAS() {
        return WebRequestADAS.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
 
        if (!isFirstLoad) {
            return;
        }

    }

    actionSave(data){
        this.isBusy(true);
        switch (data.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.webRequestADAS.createADAS(data).done((res) => {
                    this.isBusy(false);
                    this.publishMessage("bo-asset-adas-device-update");
                    this.publishMessage("bo-asset-adas-device-dialog-update");
                }).fail((e) => {
                    this.isBusy(false);
                    // this.handleError(e);
                    this.publishMessage("bo-asset-adas-device-dialog-error",e);
                });

                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.webRequestADAS.updateADAS(data).done((response) => {
                    this.isBusy(false);
                    this.publishMessage("bo-asset-adas-device-update",response.id);
                    this.publishMessage("bo-asset-adas-device-dialog-update");
                }).fail((e) => {
                    this.isBusy(false);
                    // this.handleError(e);
                    this.publishMessage("bo-asset-adas-device-dialog-error",e);
                });
                break;
        }
    }

    /**
     * Save data to web service
     */
    onClickOK(){
        this.params.RemoveCurrentDevice = (this.isRadioADAS() == "remove") ? true : false;
        this.actionSave(this.params);
        var confirmDialog = $('#bo-asset-adas-manage-confirm-dialog').data("kendoWindow");
        confirmDialog.close();
    }

    /**
     * Close modal dialog
     */
    onClickCancel(){
        var confirmDialog = $('#bo-asset-adas-manage-confirm-dialog').data("kendoWindow");
        confirmDialog.close();
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        this.params = null;
        this.src = null;
    }


}

export default {
viewModel: ScreenBaseWidget.createFactory(ADASConfirmWidget),
    template: templateMarkup
};
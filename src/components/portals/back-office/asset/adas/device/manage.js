﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../../screenbase";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import {Constants, Enums} from "../../../../../../app/frameworks/constant/apiConstant";
import DefaultSorting from "../../../../../../app/frameworks/constant/defaultSorting";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import WebRequestADAS from "../../../../../../app/frameworks/data/apitrackingcore/webRequestADAS";
import * as Screen from "../../../../../../app/frameworks/constant/screen";
import WebRequestBusinessUnit from "../../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestContract from "../../../../../../app/frameworks/data/apitrackingcore/webRequestContract";
import WebRequestTicket from "../../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";
import WebRequestCompany from "../../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestVehicle from "../../../../../../app/frameworks/data/apitrackingcore/webRequestVehicle";
import ScreenHelper from "../../../../screenhelper";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
import WebRequestADASModel from "../../../../../../app/frameworks/data/apitrackingcore/WebRequestADASModel";


class AssetADASManageScreen extends ScreenBase {
    
    /**
     * Creates an instance of UserListScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.mode = this.ensureNonObservable(params.mode);
        this.bladeSize = BladeSize.Small;

        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("ADAS Device Create")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("ADAS Device Update")());
                break;
        }
        this.adasId = params.id;

        this.modelOptions = ko.observableArray([]);
        this.selectedModel = ko.observable('');

        this.deviceId = ko.observable('');
        this.serialNo = ko.observable('');

        this.contractNoOptions = ko.observableArray([]);
        this.selectedContractNo = ko.observable('');

        this.simOptions = ko.observableArray([]);
        this.selectedSim = ko.observable('');


        this.companyOptions = ko.observableArray([]);
        this.selectedCompany = ko.observable('');

        this.buOptions = ko.observableArray([]);
        this.selectedBusinessUnit = ko.observable('');

        this.vehicleOptions = ko.observableArray([]);
        this.selectedVehicle = ko.observable('');

        this.enableModel = this.mode == Screen.SCREEN_MODE_CREATE ? true : null;

        //handleError from confirm dialog 
        this.subscribeMessage("bo-asset-adas-device-dialog-error", err => {
            this.handleError(err);
        });
    }

    /**
     * Get WebRequest specific for DMS module in Web API access.
     * @readonly
     */
    get webRequestADAS() {
        return WebRequestADAS.getInstance();
    }

    get webRequestVehicle() {
        return WebRequestVehicle.getInstance()
    }

    /**
     * Get WebRequest specific for Contract module in Web API access.
     * @readonly
     */
    get webRequestContract() {
        return WebRequestContract.getInstance()
    }

    get webRequestTicket() {
        return WebRequestTicket.getInstance()
    }

    get webRequestCompany() {
        return WebRequestCompany.getInstance()
    }

    get webRequestADASModel() {
        return WebRequestADASModel.getInstance()
    }

    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance()
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();
        let contractNoFilter = {
            enable: true
        };
        let simFilter = {
            simType: Enums.ModelData.SimType.ADAS
        };
        let d1 = this.webRequestADASModel.listADASModelSummary({enable : this.enableModel});
        let d2 = this.webRequestContract.listContract(contractNoFilter);
        let d4 = this.webRequestTicket.listSimManagement(simFilter);
        let d5 = this.webRequestCompany.listCompanySummary();

        $.when(d1, d2, d4, d5).done((r1, r2, r4, r5) => {
            this.modelOptions(r1.items);
            this.contractNoOptions(r2.items);
            this.simOptions(r4.items);
            this.companyOptions(r5.items);
            
            //For Update
            if(this.adasId){
                this.webRequestADAS.getADAS(this.adasId).done((res)=>{

                    let listBU = this.webRequestBusinessUnit.listBusinessUnitSummary({companyId: res.companyId});
                    let listVehicle = this.webRequestVehicle.listVehicleSummary({companyId: res.companyId, businessUnitIds: res.businessUnitId});

                    $.when(listBU, listVehicle).done((resBU, resVehicle) => {
                        this.buOptions(resBU.items);
                        this.vehicleOptions(resVehicle.items);
                        this.selectedModel(ScreenHelper.findOptionByProperty(this.modelOptions, "id", res.adasModelId));
                        this.deviceId(res.deviceId);
                        this.serialNo(res.serialNo);
                        this.selectedContractNo(ScreenHelper.findOptionByProperty(this.contractNoOptions, "id", res.contractId));
                        this.selectedSim(ScreenHelper.findOptionByProperty(this.simOptions, "id", res.simId));
                        
                        let company = !_.isNil(res.companyId) ? ScreenHelper.findOptionByProperty(this.companyOptions , "id", res.companyId) : null;
                        this.selectedCompany(company);

                        this.selectedBusinessUnit((res.businessUnitId) ? res.businessUnitId.toString() : null);

                        let vehicleId = !_.isNil(res.vehicleId) ? ScreenHelper.findOptionByProperty(this.vehicleOptions , "id", res.vehicleId) : null;
                        this.selectedVehicle(vehicleId);
                    
                        this.selectedCompany.subscribe((resCompany) => {
                            let companyId;
                            if (resCompany != undefined) {
                                companyId = resCompany.id
                            } else {
                                companyId = null
                            }
                            this.webRequestBusinessUnit.listBusinessUnitSummary({
                                companyId: companyId
                            }).done((res) => {
                                this.buOptions(res.items);
                            });
                        });
                
                        this.selectedBusinessUnit.subscribe((resBu) => {
                            let filter;
                            if (resBu != undefined) {
                                filter = {
                                    companyId: this.selectedCompany().id,
                                    businessUnitIds: resBu
                                }
                            } else {
                                filter = null;
                            }
                            this.webRequestVehicle.listVehicleSummary(filter).done((res) => {
                                this.vehicleOptions(res.items);
                            });
                        });
                        dfd.resolve();
                    });
                }).fail((e) => {
                    this.handleError(e);
                })
            }else{
                this.subscribeCompanyAndBU();
                dfd.resolve();
            }

        });
        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    setupExtend() {
        

        var self = this;
        this.selectedModel.extend({ trackChange: true });
        this.deviceId.extend({ trackChange: true });
        this.serialNo.extend({ trackChange: true });
        this.selectedContractNo.extend({ trackChange: true });
        this.selectedSim.extend({ trackChange: true });
        this.selectedCompany.extend({ trackChange: true });
        this.selectedBusinessUnit.extend({ trackChange: true });
        this.selectedVehicle.extend({ trackChange: true });

        this.deviceId.extend({ 
            required: true,
            serverValidate: {
                params: "DeviceId",
                message: this.i18n("M010")()
            }
        });

        this.serialNo.extend({ 
            required: true,
            serverValidate: {
                params: "SerialNo",
                message: this.i18n("M010")()
            }
        });

        this.selectedContractNo.extend({
            required: true
        });

        this.selectedBusinessUnit.extend({
            required: {
                onlyIf: function () {
                    return !_.isNil(self.selectedCompany())
                }
            }
        });

        this.selectedVehicle.extend({
            required: {
                onlyIf: function () {
                    return !_.isNil(self.selectedCompany()) && _.isNil(self.selectedVehicle())
                }
            }
        });


        this.validationModel = ko.validatedObservable({
            selectedModel: this.selectedModel,
            deviceId: this.deviceId,
            serialNo: this.serialNo,
            selectedContractNo: this.selectedContractNo,
            selectedBusinessUnit: this.selectedBusinessUnit,
            selectedVehicle: this.selectedVehicle
        });
    }

    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n('Common_Save')()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (this.mode === "update" && WebConfig.userSession.hasPermission(Constants.Permission.ADASDeviceDelete)) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
            let filter = this.generateModel();

            let checkExistingDeviceFilter = {
                vehicleId: filter.vehicleId
            }
            let checkMoveDeviceFilter = {
                vehicleId: filter.vehicleId,
                ids: [this.adasId]
            }

            if(this.mode == Screen.SCREEN_MODE_UPDATE){
                checkExistingDeviceFilter.ids = [this.adasId];
            }
            //check DMS
            let d1 = this.webRequestADAS.checkMoveDevice(checkMoveDeviceFilter);
            let d2 = this.webRequestADAS.checkExistingDevice(checkExistingDeviceFilter);
            $.when(d1,d2).done((r1,r2) => {
                var dfdMoveDevice = $.Deferred();
                if(r1){
                    this.showMessageBox(null, this.i18n("M233")(), BladeDialog.DIALOG_YESNO).done((button) => {
                        switch (button) {
                            case BladeDialog.BUTTON_YES:
                                filter.moveDevice = true;
                                dfdMoveDevice.resolve();
                                break;
                            case BladeDialog.BUTTON_NO:
                                dfdMoveDevice.reject();
                                return false;
                                break;
                        }
                    });
                }else{
                    dfdMoveDevice.resolve();
                }
            
                dfdMoveDevice.done(()=>{
                    if(r2){
                        let data = filter;
                        data.id = this.adasId;
                        data.infoStatus = Enums.InfoStatus.Update;
                        data.mode = this.mode;
                        this.widget('bo-asset-adas-manage-confirm-dialog', data, {
                            title: `Please select an option`,
                            modal: true,
                            resizable: false,
                            minimize: false,
                            target: "bo-asset-adas-manage-confirm-dialog",
                            width: "320",
                            height: "120",
                            id: 'bo-asset-adas-manage-confirm-dialog',
                            left: "40%",
                            bottom: "30%"
                        });

                        this.subscribeMessage("bo-asset-adas-device-dialog-update", (data) => {
                            this.close(true);
                        });

                    
                    }else{
                        this.isBusy(true);
                        switch (this.mode) {
                            case Screen.SCREEN_MODE_CREATE:
                                this.webRequestADAS.createADAS(filter).done((res) => {
                                    this.isBusy(false);
                                    this.publishMessage("bo-asset-adas-device-update",res.id);
                                    this.close(true);

                                }).fail((e) => {
                                    this.isBusy(false);
                                    this.handleError(e);
                                });

                                break;
                            case Screen.SCREEN_MODE_UPDATE:
                                filter.id = this.adasId;
                                filter.infoStatus = Enums.InfoStatus.Update;
                                this.webRequestADAS.updateADAS(filter).done((response) => {
                                    this.isBusy(false);
                                    this.publishMessage("bo-asset-adas-device-update", response.id);
                                    this.close(true);
                                }).fail((e) => {
                                    this.isBusy(false);
                                    this.handleError(e);
                        
                                });
                                break;
                        }
                    }
                });

            })
 
        }
    }
    subscribeCompanyAndBU(){
        this.selectedCompany.subscribe((resCompany) => {
            let companyId;
            if (resCompany != undefined) {
                companyId = resCompany.id
            } else {
                companyId = null
            }
            this.webRequestBusinessUnit.listBusinessUnitSummary({
                companyId: companyId
            }).done((res) => {
                this.buOptions(res.items);
            });
        });

        this.selectedBusinessUnit.subscribe((resBu) => {
            let filter;
            if (resBu != undefined) {
                filter = {
                    companyId: this.selectedCompany().id,
                    businessUnitIds: resBu
                }
            } else {
                filter = null;
            }
            this.webRequestVehicle.listVehicleSummary(filter).done((res) => {
                this.vehicleOptions(res.items);
            });
        });
    }


    generateModel() {
        let filterModel = {
            adasModelId: (this.selectedModel()) ? this.selectedModel().id : null,
            deviceId: this.deviceId(),
            serialNo: this.serialNo(),
            contractId: (this.selectedContractNo()) ? this.selectedContractNo().id : null,
            simId: (this.selectedSim()) ? this.selectedSim().id : null,
            vehicleId: (this.selectedVehicle()) ? this.selectedVehicle().id : null,
        };
        return filterModel;
    }

    

    /**
     * @lifecycle Handle when button on CommandBar is clicked
     * @param {any} sender
     */
    onCommandClick(sender) {

        switch (sender.id) {
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M236")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.webRequestADAS.deleteADAS(this.adasId).done((res) => {
                                this.isBusy(false);
                                this.publishMessage("bo-asset-adas-device-update");
                                this.close(true);
                            }).fail((e) => {
                                this.isBusy(false);
                                this.handleError(e);
                            });
                            break;
                    }
                });
                break;

        }

    }
}

export default {
viewModel: ScreenBase.createFactory(AssetADASManageScreen),
    template: templateMarkup
};
﻿import ko from "knockout";
import ScreenBaseWidget from "../../../../screenbasewidget";
import templateMarkup from "text!./confirm-dialog.html";
import * as Screen from "../../../../../../app/frameworks/constant/screen";
import WebRequestDMS from "../../../../../../app/frameworks/data/apitrackingcore/webRequestDMS";

class FleetCommonVideoWidget extends ScreenBaseWidget {
    constructor(params) {
        super(params);

        this.params = this.ensureNonObservable(params);

        this.isRadioDMS = ko.observable('new');

    }

    /**
     * Get WebRequest specific for DMS module in Web API access.
     * @readonly
     */
    get webRequestDMS() {
        return WebRequestDMS.getInstance()
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
 
        if (!isFirstLoad) {
            return;
        }

    }

    actionSave(data){
        this.isBusy(true);
        switch (data.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.webRequestDMS.createDMS(data).done((res) => {
                    this.isBusy(false);
                    this.publishMessage("bo-asset-dms-device-update");
                    this.publishMessage("bo-asset-dms-device-dialog-update");
                    
                }).fail((e) => {
                    this.isBusy(false);
                    // this.handleError(e);
                    this.publishMessage("bo-asset-dms-device-dialog-error",e);
                });

                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.webRequestDMS.updateDMS(data).done((response) => {
                    this.isBusy(false);
                    this.publishMessage("bo-asset-dms-device-update", data.dmsId);
                    this.publishMessage("bo-asset-dms-device-dialog-update");
                }).fail((e) => {
                    this.isBusy(false);
                    // this.handleError(e);
                    this.publishMessage("bo-asset-dms-device-dialog-error",e);
                });
                break;
        }
    }

    /**
     * Save data to web service
     */
    onClickOK(){
        this.params.RemoveCurrentDevice = (this.isRadioDMS() == "remove") ? true : false;
        this.actionSave(this.params);
        var confirmDialog = $('#bo-asset-dms-manage-confirm-dialog').data("kendoWindow");
        confirmDialog.close();
    }

    /**
     * Close modal dialog
     */
    onClickCancel(){
        var confirmDialog = $('#bo-asset-dms-manage-confirm-dialog').data("kendoWindow");
        confirmDialog.close();
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        this.params = null;
        this.src = null;
    }


}

export default {
    viewModel: ScreenBaseWidget.createFactory(FleetCommonVideoWidget),
    template: templateMarkup
};
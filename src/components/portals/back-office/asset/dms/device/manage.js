﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../../screenbase";
import ScreenHelper from "../../../../screenhelper";
import * as Screen from "../../../../../../app/frameworks/constant/screen";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestDMS from "../../../../../../app/frameworks/data/apitrackingcore/webRequestDMS";
import WebRequestCompany from "../../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestBusinessUnit from "../../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestVehicle from "../../../../../../app/frameworks/data/apitrackingcore/webRequestVehicle";
import WebRequestContract from "../../../../../../app/frameworks/data/apitrackingcore/webRequestContract";
import WebRequestTicket from "../../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";
import WebRequestDMSModel from "../../../../../../app/frameworks/data/apitrackingcore/WebRequestDMSModel";
import {Constants, Enums} from "../../../../../../app/frameworks/constant/apiConstant";
import DefaultSorting from "../../../../../../app/frameworks/constant/defaultSorting";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
class AssetDMSManageScreen extends ScreenBase {
    constructor(params) {
        super(params);
        
        
        this.bladeSize = BladeSize.Small;

        this.params = this.ensureNonObservable(params);
        this.dmsId = this.params.id;
        this.mode = this.params.mode;
        this.bladeTitle((this.mode=="create") ? "DMS Device Create" : "DMS Device Update");
        this.dmsModel = ko.observableArray([]);
        this.contractNo = ko.observableArray([]);
        this.dmsFleet = ko.observableArray([]);
        this.sim = ko.observableArray([]);
        this.company = ko.observableArray([]);
        this.businessUnit = ko.observableArray([]);
        this.vehicle = ko.observableArray([]);

        this.selectedDmsModel = ko.observable();
        this.referenceId = ko.observable();
        this.serialNo = ko.observable();
        this.selectedContractNo = ko.observable();
        this.selectedDmsFleet = ko.observable();
        this.selectedSim = ko.observable();
        this.selectedCompany = ko.observable();
        this.selectedBusinessUnit = ko.observable('');
        this.selectedVehicle = ko.observable();

        //handleError from confirm dialog 
        this.subscribeMessage("bo-asset-dms-device-dialog-error", err => {
            this.handleError(err);
        });

    }
    
    /**
     * Get WebRequest specific for DMS module in Web API access.
     * @readonly
     */
    get webRequestDMS() {
        return WebRequestDMS.getInstance()
    }
    /**
    * Get WebRequest specific for DMS module in Web API access.
    * @readonly
    */
    get webRequestDMSModel() {
        return WebRequestDMSModel.getInstance()
    }

    /**
     * Get WebRequest specific for Company module in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance()
    }
    
    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance()
    }

    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebRequestVehicle.getInstance()
    }

    /**
     * Get WebRequest specific for Contract module in Web API access.
     * @readonly
     */
    get webRequestContract() {
        return WebRequestContract.getInstance()
    }

    /**
     * Get WebRequest specific for Ticket module in Web API access.
     * @readonly
     */
    get webRequestTicket() {
        return WebRequestTicket.getInstance()
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();
        let dmsModelFilter = (this.mode == "create") ? { enable: true } : null;
        let contractNoFilter = {
            enable: true
        };
        let simFilter = {
            simType: Enums.ModelData.SimType.DMS
        };
        let d1 = this.webRequestDMSModel.listDMSModelSummary(dmsModelFilter);
        let d2 = this.webRequestContract.listContract(contractNoFilter);
        let d3 = this.webRequestDMS.listDMSFleet();
        let d4 = this.webRequestTicket.listSimManagement(simFilter);
        let d5 = this.webRequestCompany.listCompanySummary();

        $.when(d1, d2, d3, d4, d5).done((r1, r2, r3, r4, r5) => {
            this.dmsModel(r1.items);
            this.contractNo(r2.items);
            this.dmsFleet(r3.items);
            this.sim(r4.items);
            this.company(r5.items);
            
            //For Update
            if(this.dmsId){
                this.webRequestDMS.getDMS(this.dmsId).done((res)=>{
                    let listBU = this.webRequestBusinessUnit.listBusinessUnitSummary({companyId: res.companyId});
                    let listVehicle = this.webRequestVehicle.listVehicleSummary({companyId: res.companyId, businessUnitIds: res.businessUnitId});

                    $.when(listBU, listVehicle).done((resBU, resVehicle) => {
                        this.businessUnit(resBU.items);
                        this.vehicle(resVehicle.items);
                        this.selectedDmsModel(ScreenHelper.findOptionByProperty(this.dmsModel, "id", res.dmsModelId));
                        this.referenceId(res.referenceId);
                        this.serialNo(res.serialNo);
                        this.selectedContractNo(ScreenHelper.findOptionByProperty(this.contractNo, "id", res.contractId));
                        this.selectedDmsFleet(ScreenHelper.findOptionByProperty(this.dmsFleet, "id", res.dmsFleetId));
                        this.selectedSim(ScreenHelper.findOptionByProperty(this.sim, "id", res.simId));
                        
                        let company = !_.isNil(res.companyId) ? ScreenHelper.findOptionByProperty(this.company , "id", res.companyId) : null;
                        this.selectedCompany(company);

                        this.selectedBusinessUnit((res.businessUnitId) ? res.businessUnitId.toString() : null);

                        let vehicleId = !_.isNil(res.vehicleId) ? ScreenHelper.findOptionByProperty(this.vehicle , "id", res.vehicleId) : null;
                        this.selectedVehicle(vehicleId);
                    
                        this.subscribeCompanyAndBU();
                        dfd.resolve();
                    });
                }).fail((e) => {
                    this.handleError(e);
                })
            }else{
                this.subscribeCompanyAndBU();
                dfd.resolve();
            }

        });
        return dfd;
    }

    subscribeCompanyAndBU(){
        this.selectedCompany.subscribe((resCompany) => {
            let companyId;
            if (resCompany != undefined) {
                companyId = resCompany.id
            } else {
                companyId = null
            }
            this.webRequestBusinessUnit.listBusinessUnitSummary({
                companyId: companyId
            }).done((res) => {
                this.businessUnit(res.items);
            });
        });

        this.selectedBusinessUnit.subscribe((resBu) => {
            let filter;
            if (resBu != undefined) {
                filter = {
                    companyId: this.selectedCompany().id,
                    businessUnitIds: resBu
                }
            } else {
                filter = null;
            }
            this.webRequestVehicle.listVehicleSummary(filter).done((res) => {
                this.vehicle(res.items);
            });
        });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }


    /**
     * Register observable property for change detection, validatiors.
     */
    setupExtend() {
         var self = this;
         this.selectedDmsModel.extend({
             trackChange: true
         });

         this.referenceId.extend({
             trackChange: true
         });

         this.serialNo.extend({
             trackChange: true
         });

         this.selectedContractNo.extend({
             trackChange: true
         });

         this.selectedDmsFleet.extend({
             trackChange: true
         });

         this.selectedSim.extend({
             trackChange: true
         });

         this.selectedCompany.extend({
             trackChange: true
         });

         this.selectedBusinessUnit.extend({
             trackChange: true
         });

         this.selectedVehicle.extend({
             trackChange: true
         });

        

        this.selectedDmsModel.extend({
            required: true
        });

        this.referenceId.extend({
            required: true
        });

        this.serialNo.extend({
            required: true
        });

        this.selectedContractNo.extend({
            required: true
        });

        this.selectedDmsFleet.extend({
            required: true
        });

        this.selectedBusinessUnit.extend({
            required: {
                onlyIf: function () {
                    return !_.isNil(self.selectedCompany())
                }
            }
        });

        this.selectedVehicle.extend({
            required: {
                onlyIf: function () {
                    return !_.isNil(self.selectedCompany()) && _.isNil(self.selectedVehicle())
                }
            }
        });

        this.referenceId.extend({
            serverValidate: {
                params: "ReferenceId",
                message: this.i18n("M010")()
            }
        });

        this.serialNo.extend({
            serverValidate: {
                params: "SerialNo",
                message: this.i18n("M010")()
            }
        });


        this.validationModel = ko.validatedObservable({
            selectedDmsModel: this.selectedDmsModel,
            referenceId: this.referenceId,
            serialNo: this.serialNo,
            selectedContractNo: this.selectedContractNo,
            selectedDmsFleet: this.selectedDmsFleet,
            selectedSim: this.selectedSim,
            selectedCompany: this.selectedCompany,
            selectedBusinessUnit: this.selectedBusinessUnit,
            selectedVehicle: this.selectedVehicle
        });
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n('Common_Save')()));
        actions.push(this.createActionCancel());
    }

    buildCommandBar(commands) {
        if (this.mode === "update") {
            if(WebConfig.userSession.hasPermission(Constants.Permission.DMSDeviceDelete)){
                commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
            }
        }
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onCommandClick(sender) {
        
        switch (sender.id) {

            case "cmdDelete":
                
                this.showMessageBox(null, this.i18n("M229")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.webRequestDMS.deleteDMS(this.dmsId).done((res) => {
                                this.isBusy(false);
                                this.publishMessage("bo-asset-dms-device-update");
                                this.close(true);
                            }).fail((e) => {
                                this.isBusy(false);
                                this.handleError(e);
                            });
                            break;
                    }
                });
                break;

        }
    }
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
            
            let filter = this.generateModel();
            let checkExistingDeviceFilter = {
                vehicleId: filter.vehicleId
            }
            let checkMoveDeviceFilter = {
                vehicleId: filter.vehicleId,
                ids: [this.dmsId]
            }
            if(this.mode == Screen.SCREEN_MODE_UPDATE){
                checkExistingDeviceFilter.ids = [this.dmsId];
            }
            //check DMS
            let d1 = this.webRequestDMS.checkMoveDevice(checkMoveDeviceFilter);
            let d2 = this.webRequestDMS.checkExistingDevice(checkExistingDeviceFilter);
            $.when(d1, d2).done((r1, r2)=>{
                var dfdMoveDevice = $.Deferred();
                if(r1){
                    this.showMessageBox(null, this.i18n("M233")(), BladeDialog.DIALOG_YESNO).done((button) => {
                        switch (button) {
                            case BladeDialog.BUTTON_YES:
                                filter.moveDevice = true;
                                dfdMoveDevice.resolve();
                                break;
                            case BladeDialog.BUTTON_NO:
                                dfdMoveDevice.reject();
                                return false;
                                break;
                        }
                    });
                }else{
                    dfdMoveDevice.resolve();
                }

                dfdMoveDevice.done(()=>{
                    if(r2){
                        let data = filter;
                        data.id = this.dmsId;
                        data.infoStatus = Enums.InfoStatus.Update;
                        data.mode = this.mode;
                        this.widget('bo-asset-dms-manage-confirm-dialog', data, {
                            title: `Please select an option`,
                            modal: true,
                            resizable: false,
                            minimize: false,
                            target: "bo-asset-dms-manage-confirm-dialog",
                            width: "320",
                            height: "120",
                            id: 'bo-asset-dms-manage-confirm-dialog',
                            left: "40%",
                            bottom: "30%"
                        });
    
                        this.subscribeMessage("bo-asset-dms-device-dialog-update", (data) => {
                            this.close(true);
                        });
    
                        
                    }else{
                        this.isBusy(true);
                        switch (this.mode) {
                            case Screen.SCREEN_MODE_CREATE:
                                this.webRequestDMS.createDMS(filter).done((res) => {
                                    this.isBusy(false);
                                    this.publishMessage("bo-asset-dms-device-update");
                                    this.close(true);
                                    
                                }).fail((e) => {
                                    this.isBusy(false);
                                    this.handleError(e);
                                });
    
                                break;
                            case Screen.SCREEN_MODE_UPDATE:
                                filter.id = this.dmsId;
                                filter.infoStatus = Enums.InfoStatus.Update;
                                this.webRequestDMS.updateDMS(filter).done((response) => {
                                    this.isBusy(false);
                                    this.publishMessage("bo-asset-dms-device-update", this.dmsId);
                                    this.close(true);
                                }).fail((e) => {
                                    this.isBusy(false);
                                    this.handleError(e);
                                    
                                });
                                break;
                        }
                    }
                });
            });
            
        }
    }

    generateModel() {
        let filterModel = {
            dmsModelId: (this.selectedDmsModel()) ? this.selectedDmsModel().id : null,
            referenceId: this.referenceId(),
            serialNo: this.serialNo(),
            contractId: (this.selectedContractNo()) ? this.selectedContractNo().id : null,
            dmsFleetId: (this.selectedDmsFleet()) ? this.selectedDmsFleet().id : null,
            simId: (this.selectedSim()) ? this.selectedSim().id : null,
            vehicleId: (this.selectedVehicle()) ? this.selectedVehicle().id : null,
            moveDevice: false
        };
        return filterModel;
    }

}

export default {
    viewModel: ScreenBase.createFactory(AssetDMSManageScreen),
    template: templateMarkup
};




import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../../screenbase";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestDMS from "../../../../../../app/frameworks/data/apitrackingcore/webRequestDMS";
import {Constants, Enums} from "../../../../../../app/frameworks/constant/apiConstant";
import DefaultSorting from "../../../../../../app/frameworks/constant/defaultSorting";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";

/**
 * User Management > List 
 * 
 * @class UserListScreen
 * @extends {ScreenBase}
 */
class AssetDMSListScreen extends ScreenBase {

    /**
     * Creates an instance of UserListScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle("DMS Device");
        this.bladeSize = BladeSize.Large;

        this.dmsSummary = ko.observableArray();
        this.filterText = ko.observable();
        this.selectedDMS = ko.observable();
        this.recentChangedRowIds = ko.observableArray();
        this.order = ko.observable([[ 0, "asc" ]]);

        // Determine if selecting action is legal, return false to cancel selecting event.
        this._selectingRowHandler = (data) => {
            if(WebConfig.userSession.hasPermission(Constants.Permission.DMSDeviceUpdate)){
                data.mode = "update";
                return this.navigate("bo-asset-dms-manage", data);
            }
        };

        this.subscribeMessage("bo-asset-dms-device-update", id => {
            this.initTable(id);
        });

    }

    /**
     * Get WebRequest specific for DMS module in Web API access.
     * @readonly
     */
    get webRequestDMS() {
        return WebRequestDMS.getInstance();
    }

    initTable(id){
        var dfd = $.Deferred();
        var dmsSummaryFilter = {};
        this.webRequestDMS.listDMSSummary(dmsSummaryFilter).done((response)=> {
            var dms = response["items"];
            this.dmsSummary.replaceAll(dms);
            if(id){
                this.recentChangedRowIds.replaceAll([id]);
            }
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;
        var dfd = $.Deferred();
        this.initTable().done((response)=> {
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands){
        if(WebConfig.userSession.hasPermission(Constants.Permission.DMSDeviceCreate)){
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Handle when button on CommandBar is clicked
     * @param {any} sender
     */
    onCommandClick(sender) {
        if(sender.id == "cmdCreate") {
            this.navigate("bo-asset-dms-manage", { mode:"create" });
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(AssetDMSListScreen),
    template: templateMarkup
};
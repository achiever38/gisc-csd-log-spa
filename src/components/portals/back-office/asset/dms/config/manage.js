﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import { Enums,Constants } from "../../../../../../app/frameworks/constant/apiConstant";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../../app/frameworks/constant/screen";
import WebRequestDMSModel from "../../../../../../app/frameworks/data/apitrackingcore/WebRequestDMSModel";
import ScreenBase from "../../../../screenbase";
import ScreenHelper from "../../../../screenhelper";
import WebRequestDMSConfiguration from "../../../../../../app/frameworks/data/apitrackingcore/WebRequestDMSConfiguration";
import WebRequestEnumResource from "../../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import DefaultSorting from "../../../../../../app/frameworks/constant/defaultSorting";


class AssetDMSConfigManageScreen extends ScreenBase {

    /**
     * Creates an instance of UserListScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.mode = this.ensureNonObservable(params.mode);
        this.bladeSize = BladeSize.Medium;
        this.configId = this.ensureNonObservable(params.id,null);
        this.validationConfig = ko.validatedObservable({});

        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("DMS Configuration Create")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("DMS Configuration Update")());
                break;
        }

        this.name = ko.observable();
        this.description = ko.observable();

        this.configKeyList = ko.observableArray([]);
        this.order = ko.observable([[1, "asc"]]);
        this.recentChangedRowIds = ko.observableArray();

        this.integrationTypeOptions = ko.observableArray([]);
        this.selectedIntegration = ko.observable();

        this._originalConfigKey = [];

        this._selectingRowHandler = (data) => {
            return this.navigate("bo-asset-dms-config-config-key", { currId: data.id, configItem: this.configKeyList(), infoStatus: data.infoStatus });
        };

        this.subscribeMessage("bo-asset-dms-config-config-key-change", (data) => {
            let item = data.item;
            let id = data.id;

            this.configKeyList.replaceAll(item);
            this.recentChangedRowIds.replaceAll([id]);

        });

    }


    get webRequestDMSConfiguration() {
        return WebRequestDMSConfiguration.getInstance();
    }

    /**
    * Get WebRequest specific for Enum Resource module in Web API access.
    * @readonly
    */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();

        var enumIntegrationType = {
            types: [Enums.ModelData.EnumResourceType.IntegrationType],
            sortingColumns: DefaultSorting.EnumResource
        };

        var dfdIntegrationType = this.webRequestEnumResource.listEnumResource(enumIntegrationType);
        var dfdDMSConfig = null;
        if (this.mode == Screen.SCREEN_MODE_UPDATE && this.configId) {
            dfdDMSConfig = this.webRequestDMSConfiguration.getDMSConfig(this.configId);
        }
        $.when(dfdDMSConfig, dfdIntegrationType).done((r, r2) => {
            this.integrationTypeOptions(r2["items"]);
            if (r) {
                this.name(r.name);
                this.description(r.description);
                this.configKeyList(r.dmsConfigurationKeyInfos);
                this.selectedIntegration(ScreenHelper.findOptionByProperty(this.integrationTypeOptions, "value", r.integrationType));

                this._originalConfigKey = _.clone(r.dmsConfigurationKeyInfos);
            }

            dfd.resolve();
        }).fail((e) => {
            this.handleError(e);
        });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    setupExtend() {
        this.selectedIntegration.extend({ required: true });
        this.name.extend({
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });

        this.name.extend({ trackChange: true });
        this.selectedIntegration.extend({ trackChange: true });
        this.description.extend({ trackChange: true });
        this.configKeyList.extend({ trackArrayChange: true });


        

        this.validationConfig({
            selectedIntegration: this.selectedIntegration,
            name: this.name
        });
    }

    onUnload() { }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n('Common_Save')()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (this.mode === "update" && WebConfig.userSession.hasPermission(Constants.Permission.DMSConfigDelete)) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {
            if (!this.validationConfig.isValid()) {
                this.validationConfig.errors.showAllMessages();
                return;
            }

            this.isBusy(true);

            var info = this.generateModel();

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestDMSConfiguration.createDMSConfig(info, true)
                        .done((result) => {
                            this.publishMessage("bo-asset-dms-config-changed", result.id);
                            this.close(true);
                        })
                        .fail((e) => {
                            this.handleError(e);
                        })
                        .always(() => {
                            this.isBusy(false);
                        });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.webRequestDMSConfiguration.updateDMSConfig(info)
                        .done(() => {
                            this.publishMessage("bo-asset-dms-config-changed", this.configId);
                            this.close(true);
                        })
                        .fail((e) => {
                            this.handleError(e);
                        })
                        .always(() => {
                            this.isBusy(false);
                        });
                    break;
            }
        }
    }


    generateModel() {
        var info = {
            integrationType: (this.selectedIntegration()) ? this.selectedIntegration().value : 0,
            name: this.name(),
            description: this.description(),
            id: this.configId
        };

        let configKeyItem = this.configKeyList();
        this._originalConfigKey.forEach((o) => {
            let findItem = $.grep(this.configKeyList(), (n, m) => {
                return o.id == n.id;
            });
            if (findItem.length == 0) {
                o.infoStatus = Enums.InfoStatus.Delete;
                configKeyItem.push(o);
            }
        });

        info.dmsConfigurationKeyInfos = configKeyItem;

        return info;
    }



    /**
     * @lifecycle Handle when button on CommandBar is clicked
     * @param {any} sender
     */
    onCommandClick(sender) {

        switch (sender.id) {
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M231")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.webRequestDMSConfiguration.deleteDMSConfig(this.configId).done(() => {
                                this.isBusy(false);
                                this.publishMessage("bo-asset-dms-config-changed");
                                this.close(true);
                            }).fail((e) => {
                                this.isBusy(false);
                                this.handleError(e);
                            });
                            break;
                    }
                });
                break;

        }

    }

    addConfig() {
        this.navigate('bo-asset-dms-config-config-key', {
            configItem: this.configKeyList()
        });
    }

    //LoadIntegrationType() {
    //    return ko.observableArray([{ name: 'Test', value: 1 }, { name: 'Test2', value: 2 }])
    //}

}

export default {
    viewModel: ScreenBase.createFactory(AssetDMSConfigManageScreen),
    template: templateMarkup
};
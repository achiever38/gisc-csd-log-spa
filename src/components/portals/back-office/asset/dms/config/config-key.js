﻿import ko from "knockout";
import templateMarkup from "text!./config-key.html";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import { Enums, Constants } from "../../../../../../app/frameworks/constant/apiConstant";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../../screenbase";

/**
 * User Management > List 
 * 
 * @class UserListScreen
 * @extends {ScreenBase}
 */
class AssetDMSConfigKeyListScreen extends ScreenBase {

    /**
     * Creates an instance of UserListScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle("Configuration Key");
        this.bladeSize = BladeSize.Small;

        this.itemConfigKeyList = this.ensureNonObservable(params.configItem, []);
        this.id = this.ensureNonObservable(params.currId, null);
        this.infoStatus = this.ensureNonObservable(params.infoStatus, 0);

        let tmpId = this.id;
        this.currConfig = _.find(this.itemConfigKeyList, function (o) { return o.id == tmpId });

        this.key = ko.observable();
        this.value = ko.observable();

        if (this.currConfig) {
            this.key(this.currConfig["key"]);
            this.value(this.currConfig["value"]);
        }

    }


    get webRequestDMSConfig() {
        return WebRequestDMSConfig.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        

    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */

    setupExtend() {
      
        this.key.extend({
            required: true,
            validation: {
                validator: (val) => {
                    let checkDupicate = _.filter(this.itemConfigKeyList, (item) => {
                        return item.key.toUpperCase() == val.toUpperCase() && item.id != this.id; // check case sensitive
                    });

                    return !_.size(checkDupicate);
                },
                message: this.i18n("M010")()
            }
        });
        this.value.extend({ required: true });

        this.key.extend({ trackChange: true });
        this.value.extend({ trackChange: true });

        this.validationConfig = ko.validatedObservable({
            key: this.key,
            value: this.value
        });

    }

    onUnload() { }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n('Common_Save')()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationConfig.isValid()) {
                this.validationConfig.errors.showAllMessages();
                return;
            }

            if (this.id) {
                this.itemConfigKeyList = _.map(this.itemConfigKeyList, x => {
                    if (x.id == this.id) {
                        x.key = this.key();
                        x.value = this.value();
                        x.infoStatus = this.infoStatus == Enums.InfoStatus.Add ? Enums.InfoStatus.Add : Enums.InfoStatus.Update;
                    }
                    return x;
                }, this);

            }
            else {
                let lastObj = _.maxBy(this.itemConfigKeyList, 'id');
                let newId = lastObj ? lastObj.id + 1 : 1;

                let newConfig = this.generateModel();
                newConfig.id = newId;
                newConfig.infoStatus = Enums.InfoStatus.Add;

                this.id = newId;
                this.itemConfigKeyList.push(newConfig);
            }

            this.publishMessage("bo-asset-dms-config-config-key-change", { item: this.itemConfigKeyList, id: this.id });
            this.close(true);

        }

        
    }

    generateModel() {
        return {
            key: this.key(),
            value: this.value()
        };
    }

    /**
     * @lifecycle Handle when button on CommandBar is clicked
     * @param {any} sender
     */
    onCommandClick(sender) {
        
    }
}

export default {
    viewModel: ScreenBase.createFactory(AssetDMSConfigKeyListScreen),
    template: templateMarkup
};
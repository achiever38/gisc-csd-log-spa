import ko from "knockout";
import templateMarkup from "text!./list.html";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import { Constants } from "../../../../../../app/frameworks/constant/apiConstant";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../../app/frameworks/constant/screen";
import WebRequestDMSConfig from "../../../../../../app/frameworks/data/apitrackingcore/WebRequestDMSConfiguration";
import ScreenBase from "../../../../screenbase";

/**
 * User Management > List 
 * 
 * @class UserListScreen
 * @extends {ScreenBase}
 */
class AssetDMSConfigListScreen extends ScreenBase {

    /**
     * Creates an instance of UserListScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle("DMS Configuration");
        this.bladeSize = BladeSize.Medium;

        this.order = ko.observable([[0, "asc"]]);
        this.filterText = ko.observable();
        this.itemConfig = ko.observableArray([]);
        this.recentChangedRowIds = ko.observableArray();

        this._selectingRowHandler = (data) => {
            if (WebConfig.userSession.hasPermission(Constants.Permission.DMSConfigUpdate)) {
                data.mode = "update";
                return this.navigate("bo-asset-dms-config-manage", data);
            }
        };

        this.subscribeMessage("bo-asset-dms-config-changed", id => {
            var dfd = $.Deferred();
            this.webRequestDMSConfig.listDMSConfigSummary().done((response) => {
                this.itemConfig.replaceAll(response["items"]);
                this.recentChangedRowIds.replaceAll([id]);
                dfd.resolve();
            }).fail((e) => {
                dfd.reject(e);
            });
            return dfd;
        });


    }


    get webRequestDMSConfig() {
        return WebRequestDMSConfig.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;
        var dfd = $.Deferred();

        this.webRequestDMSConfig.listDMSConfigSummary().done((r) => {
            this.itemConfig(r["items"]);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.DMSConfigCreate)) {
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Handle when button on CommandBar is clicked
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id == "cmdCreate") {
            this.navigate("bo-asset-dms-config-manage", { mode : Screen.SCREEN_MODE_CREATE });
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(AssetDMSConfigListScreen),
    template: templateMarkup
};
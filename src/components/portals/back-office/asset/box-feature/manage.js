﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import Utility from "../../../../../app/frameworks/core/utility";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestFeature from "../../../../../app/frameworks/data/apitrackingcore/webRequestFeature";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import {Constants,Enums} from "../../../../../app/frameworks/constant/apiConstant";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";

/**
 * Box Features > Create/Update
 * 
 * @class BoxFeatureManageScreen
 * @extends {ScreenBase}
 */
class BoxFeatureManageScreen extends ScreenBase {

    /**
     * Creates an instance of BoxFeatureManageScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        // Set blade title
        this.mode = this.ensureNonObservable(params.mode);
        if (this.mode === Screen.SCREEN_MODE_CREATE) {
            this.bladeTitle(this.i18n("Assets_CreateBoxFeature")());
        } else if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            this.bladeTitle(this.i18n("Assets_UpdateBoxFeature")());
        }

        this.featureId = this.ensureNonObservable(params.featureId, -1);

        // These properties will be populated in onLoad
        this.name = ko.observable("");
        this.code = ko.observable("");
        this.description = ko.observable("");
        this.selectedUnit = ko.observable();
        this.selectedEventType = ko.observable();
        this.selectedDataType = ko.observable();
        this.isAccessory = ko.observable(false);
        this.isCustomAlert = ko.observable(false);
        this.isSystem = ko.observable(false);
        this.isSupportAnalog = ko.observable(false);
        this.firstValisAnalog = false;

        // Predefined Datasource
        this.eventTypeDS = ScreenHelper.createEventTypeObservableArray();
        this.eventDataTypeDS = ScreenHelper.createEventDataTypeObservableArray();

        // Computation, fine grained for each display field
        this.isUpdateEditable = ko.pureComputed(function () {
            if ((this.mode === Screen.SCREEN_MODE_UPDATE) && this.isSystem()) {
                return false;
            }
            return true;
        }, this);
        this.isNonUpdateEditable = ko.pureComputed(function () {
            if ((this.mode === Screen.SCREEN_MODE_CREATE) && !this.isSystem()) {
                return true;
            }
            return false;
        }, this);

        this.isNonUpdateEditableUnit = ko.pureComputed( ()=> {
            if ((this.mode === Screen.SCREEN_MODE_CREATE) && !this.isSystem() && (this.selectedDataType().value === 1 || this.selectedDataType().value === 2)) {
                return true;
            }
            return false;
        });

        // isSupportAnalog
        this.isEnableSupportAnalog = ko.pureComputed(() => {
            if (this.mode === Screen.SCREEN_MODE_UPDATE){
                //
                return this.firstValisAnalog == true ? false : true;
            }
            else{
                return true;
            }
        });
        this.isShowSupportAnalog = ko.pureComputed(() => {
            return this.selectedDataType().value == Enums.ModelData.EventDataType.Digital;
        });

    }

    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * Get WebRequest specific for Feature module in Tracking Web API access.
     * @readonly
     */
    get webRequestFeature() {
        return WebRequestFeature.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;
        
        var dfd = $.Deferred();

        var enumResourceFilter = {
            types: [Enums.ModelData.EnumResourceType.FeatureUnit]
        };
        var d1 = this.webRequestEnumResource.listEnumResource(enumResourceFilter);
        var d2 = (this.mode === Screen.SCREEN_MODE_UPDATE) ? this.webRequestFeature.getFeature(this.featureId) : null;

        $.when(d1, d2).done((r1, feature) => {
            var featureUnits = r1["items"];
            if (featureUnits) {
                this.unitDS = ScreenHelper.createGenericOptionsObservableArray(featureUnits, "displayName", "value");
            }

            if (feature) {
                // Update
                this.name(feature.name);
                this.code(feature.code);
                this.description(feature.description);
                this.isAccessory(feature.isAccessory);
                this.isCustomAlert(feature.isCustomAlert);
                this.isSystem(feature.isSystem);
                this.isSupportAnalog(feature.isAnalog);
                this.firstValisAnalog = feature.isAnalog;
                // Feature Unit is nullable
                if (!_.isNil(feature.unit)) {
                    this.selectedUnit(ScreenHelper.findOptionByProperty(this.unitDS, "value", feature.unit));
                }

                this.selectedEventType(ScreenHelper.findOptionByProperty(this.eventTypeDS, "value", feature.eventType));
                this.selectedDataType(ScreenHelper.findOptionByProperty(this.eventDataTypeDS, "value", feature.eventDataType));
            } else {
                // Create
                this.selectedEventType(ScreenHelper.findOptionByProperty(this.eventTypeDS, "value", Enums.ModelData.EventType.Input));
                this.selectedDataType(ScreenHelper.findOptionByProperty(this.eventDataTypeDS, "value", Enums.ModelData.EventDataType.Digital));
            }
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        // Track change
        this.name.extend({
            trackChange: true
        });
        this.code.extend({
            trackChange: true
        });
        this.description.extend({
            trackChange: true
        });
        this.isAccessory.extend({
            trackChange: true
        });
        this.isCustomAlert.extend({
            trackChange: true
        });
        this.selectedUnit.extend({
            trackChange: true
        });
        this.selectedEventType.extend({
            trackChange: true
        });
        this.selectedDataType.extend({
            trackChange: true
        });

        // Validation
        this.name.extend({
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });
        this.code.extend({
            required: true,
            serverValidate: {
                params: "Code",
                message: this.i18n("M010")()
            }
        });
        this.description.extend({
            required: true
        });
        this.selectedEventType.extend({
            required: true
        });
        this.selectedDataType.extend({
            required: true
        });

        this.validationModel = ko.validatedObservable({
            name: this.name,
            code: this.code,
            description: this.description,
            selectedEventType: this.selectedEventType,
            selectedDataType: this.selectedDataType
        });
    }

    generateModel() {
        var model = {
            id: this.featureId,
            name: this.name(),
            code: this.code(),
            description: this.description(),
            eventType: this.selectedEventType().value,
            eventDataType: this.selectedDataType().value,
            isAccessory: this.isAccessory(),
            isCustomAlert: this.isCustomAlert(),
            isSystem: this.isSystem(),
            isAnalog: this.selectedDataType().value != Enums.ModelData.EventDataType.Digital ? false : this.isSupportAnalog()
        };

        if (!_.isNil(this.selectedUnit())) {
            model.unit = this.selectedUnit().value;
        }

        return model;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        if (!this.isSystem()) {
            switch(this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    if (WebConfig.userSession.hasPermission(Constants.Permission.CreateBoxFeature)) {
                        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
                    }
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    if (WebConfig.userSession.hasPermission(Constants.Permission.UpdateBoxFeature)) {
                        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
                    }
                    break;
            }
            actions.push(this.createActionCancel());
        }
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            if (WebConfig.userSession.hasPermission(Constants.Permission.DeleteBoxFeature) && !this.isSystem()) {
                commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
            }
        }
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            // Mark start of long operation
            this.isBusy(true);

            var model = this.generateModel();

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestFeature.createFeature(model).done((response) => {
                        this.isBusy(false);

                        this.publishMessage("bo-asset-feature-changed", response.id);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);

                        this.handleError(e);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.webRequestFeature.updateFeature(model).done((response) => {
                        this.isBusy(false);

                        this.publishMessage("bo-asset-feature-changed", response.id);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);

                        this.handleError(e);
                    });
                    break;
            }
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdDelete") {
            this.showMessageBox(null, this.i18n("M108")(), BladeDialog.DIALOG_YESNO).done((button) => {
                switch(button) {
                    case BladeDialog.BUTTON_YES:
                        this.isBusy(true);

                        this.webRequestFeature.deleteFeature(this.featureId).done(() => {
                            this.isBusy(false);

                            this.publishMessage("bo-asset-feature-changed", this.featureId);
                            this.close(true);
                        }).fail((e) => {
                            this.isBusy(false);

                            this.handleError(e);
                        });
                        break;
                }
            });
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(BoxFeatureManageScreen),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./menu.html";
import ScreenBase from "../../screenbase";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import { Constants } from "../../../../app/frameworks/constant/apiConstant";

/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class AssetMenuScreen extends ScreenBase {

    /**
     * Creates an instance of AssetMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Common_Assets")());
        this.bladeMargin = BladeMargin.Narrow;
        this.bladeSize = BladeSize.XSmall;

        this.items = ko.observableArray([]);
        this.selectedIndex = ko.observable();

        this._selectingRowHandler = (item) => {
            if (item) {
                return this.navigate(item.page);
            }
            return false;
        };
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // This page has only one permission for all sub menu.
        // Do not need extra validation permission.
        if (!isFirstLoad) {
            return;
        }

        if( WebConfig.userSession.hasPermission(Constants.Permission.CreateBoxFeature) ||
            WebConfig.userSession.hasPermission(Constants.Permission.UpdateBoxFeature) ||
            WebConfig.userSession.hasPermission(Constants.Permission.DeleteBoxFeature) )
        {
            this.items.push({
                text: this.i18n('Assets_BoxFeatures')(),
                page: 'bo-asset-box-feature'
            });
        }

        if( WebConfig.userSession.hasPermission(Constants.Permission.CreateBoxModel) ||
            WebConfig.userSession.hasPermission(Constants.Permission.UpdateBoxModel) ||
            WebConfig.userSession.hasPermission(Constants.Permission.DeleteBoxModel))
        {
            this.items.push({
                text: this.i18n('Assets_BoxModels')(),
                page: 'bo-asset-box-model'
            });
        }

        if( WebConfig.userSession.hasPermission(Constants.Permission.CreateBoxTemplate) ||
            WebConfig.userSession.hasPermission(Constants.Permission.UpdateBoxTemplate) ||
            WebConfig.userSession.hasPermission(Constants.Permission.DeleteBoxTemplate))
        {
            this.items.push({
                text: this.i18n('Assets_BoxTemplates')(),
                page: 'bo-asset-box-template'
            });
        }

        if( WebConfig.userSession.hasPermission(Constants.Permission.CreateBoxsStock) ||
            WebConfig.userSession.hasPermission(Constants.Permission.UpdateBoxsStock) ||
            WebConfig.userSession.hasPermission(Constants.Permission.DeleteBoxsStock) )
        {
            this.items.push({
                text: this.i18n('Assets_BoxStocks')(),
                page: 'bo-asset-box-stock'
            });
        }

        if( WebConfig.userSession.hasPermission(Constants.Permission.CreateBox) ||
            WebConfig.userSession.hasPermission(Constants.Permission.UpdateBox) ||
            WebConfig.userSession.hasPermission(Constants.Permission.DeleteBox) )
        {
            this.items.push({
                text: this.i18n('Common_Boxes')(),
                page: 'bo-asset-box'
            });
        }

        if( WebConfig.userSession.hasPermission(Constants.Permission.CreateVehicleModel) ||
            WebConfig.userSession.hasPermission(Constants.Permission.UpdateVehicleModel) ||
            WebConfig.userSession.hasPermission(Constants.Permission.DeleteVehicleModel) )
        {
            this.items.push({
                text: this.i18n('Assets_VehicleModels')(),
                page: 'bo-asset-vehicle-model'
            });
        }



        //if( WebConfig.userSession.hasPermission(Constants.Permission.CreateMDVRModel) ||
        //    WebConfig.userSession.hasPermission(Constants.Permission.UpdateMDVRModel) ||
        //    WebConfig.userSession.hasPermission(Constants.Permission.DeleteMDVRModel) )
        //{
        //    this.items.push({
        //        text: this.i18n('Assets_Mdvr_Models')(),
        //        page: 'bo-asset-mdvr-models'
        //    });
        //}

        if( WebConfig.userSession.hasPermission(Constants.Permission.CreateMDVRs) ||
            WebConfig.userSession.hasPermission(Constants.Permission.UpdateMDVRs) ||
            WebConfig.userSession.hasPermission(Constants.Permission.DeleteMDVRs) )
        {
            this.items.push({
                text: this.i18n('MDVR')(),
                page: 'bo-asset-mdvr-menu'
            });
        }



        if(WebConfig.userSession.hasPermission(Constants.Permission.ADAS))
        {
            this.items.push({
                text: this.i18n('ADAS')(),
                page: 'bo-asset-adas'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.DMS))
        {
            this.items.push({
                text: this.i18n('DMS')(),
                page: 'bo-asset-dms'
            });
        }

        
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedIndex(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}
}

export default {
    viewModel: ScreenBase.createFactory(AssetMenuScreen),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import {
    Constants,
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebRequestBoxStock from "../../../../../app/frameworks/data/apitrackingcore/webRequestBoxStock";
import WebRequestTechnicianTeam from "../../../../../app/frameworks/data/apicore/webRequestTechnicianTeam";
import ScreenHelper from "../../../screenhelper";

/**
 * Box Features > Create/Update
 * 
 * @class BoxFeatureManageScreen
 * @extends {ScreenBase}
 */
class BoxStockManageScreen extends ScreenBase {

    /**
     * Creates an instance of BoxStockManageScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        // Set blade title
        this.mode = this.ensureNonObservable(params.mode);
        if (this.mode === Screen.SCREEN_MODE_CREATE) {
            this.bladeTitle(this.i18n("Assets_CreateBoxStock")());
        } else if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            this.bladeTitle(this.i18n("Assets_UpdateBoxStock")());
        }

        this.boxStockId = this.ensureNonObservable(params.id, -1);

        this.name = ko.observable();
        this.code = ko.observable();
        this.boxStockDescription = ko.observable();
        this.ddtechnicianTeam = ko.observableArray([]);
        this.technicianTeam = ko.observable();


    }

    /**
     * Get WebRequest specific for Box-Stock in Tracking Web API access.
     * @readonly
     */
    get webRequestBoxStock() {
        return WebRequestBoxStock.getInstance();
    }
    get webRequestTechnicianTeam() {
        return WebRequestTechnicianTeam.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var dfd = $.Deferred();
        var dfdTechnician = $.Deferred();

        var d1 = (this.mode === Screen.SCREEN_MODE_UPDATE) ? this.webRequestBoxStock.getBoxStock(this.boxStockId) : null;
       
        $.when(d1).done((r1) => {
            var boxStock = r1;

            this.webRequestTechnicianTeam.listTechnicianTeamSummary({
                //"isBoxStock": true, เปลี่ยน filter เป็น stockType
                "stockType": Enums.ModelData.StockType.BoxStock,
                "technicianTeamId": (this.mode === Screen.SCREEN_MODE_UPDATE) ? boxStock.technicianTeamId : null,
            }).done((r2) => {
                this.ddtechnicianTeam(r2["items"])
                if(this.mode === Screen.SCREEN_MODE_UPDATE){
                    this.technicianTeam(ScreenHelper.findOptionByProperty(this.ddtechnicianTeam, "id", boxStock.technicianTeamId))
                }
                

                dfdTechnician.resolve();
            })

            if (boxStock) {
                this.name(boxStock.name);
                this.code(boxStock.code);
                this.boxStockDescription(boxStock.description);
            }



            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        //Track change
        this.name.extend({
            trackChange: true
        });

        this.code.extend({
            trackChange: true
        });

        this.boxStockDescription.extend({
            trackChange: true
        });


        // Validation
        this.name.extend({
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });

        this.code.extend({
            serverValidate: {
                params: "Code",
                message: this.i18n("M010")()
            }
        });

        this.validationModel = ko.validatedObservable({
            name: this.name,
            code: this.code
        });
    }

    generateModel() {
        var model = {
            name: this.name(),
            code: this.code(),
            description: this.boxStockDescription(),
            id: this.boxStockId,
            infoStatus: this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update,
            technicianTeamId: this.technicianTeam()?this.technicianTeam().id:null
        };
        return model;
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {

        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                if (WebConfig.userSession.hasPermission(Constants.Permission.CreateBoxsStock)) {
                    actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
                    actions.push(this.createActionCancel());
                }
                break;
            case Screen.SCREEN_MODE_UPDATE:
                if (WebConfig.userSession.hasPermission(Constants.Permission.UpdateBoxsStock)) {
                    actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
                    actions.push(this.createActionCancel());
                }
                break;
        }
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            // Mark start of long operation
            this.isBusy(true);

            var model = this.generateModel();

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestBoxStock.createBoxStock(model).done((response) => {
                        this.isBusy(false);

                        this.publishMessage("bo-asset-box-stock-changed", response.id);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);
                        this.handleError(e);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.webRequestBoxStock.updateBoxStock(model).done((response) => {
                        this.isBusy(false);

                        this.publishMessage("bo-asset-box-stock-changed", response.id);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);

                        this.handleError(e);
                    });
                    break;
            }
        }
    }


    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            if (WebConfig.userSession.hasPermission(Constants.Permission.DeleteBoxsStock)) {
                commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
            }
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdDelete") {
            this.showMessageBox(null, this.i18n("M033")(), BladeDialog.DIALOG_YESNO).done((button) => {
                switch (button) {
                    case BladeDialog.BUTTON_YES:
                        this.isBusy(true);

                        this.webRequestBoxStock.deleteBoxStock(this.boxStockId).done(() => {
                            this.isBusy(false);

                            this.publishMessage("bo-asset-box-stock-changed", this.boxStockId);
                            this.close(true);
                        }).fail((e) => {
                            this.isBusy(false);

                            this.handleError(e);
                        });
                        break;
                }
            });
        }
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

}

export default {
    viewModel: ScreenBase.createFactory(BoxStockManageScreen),
    template: templateMarkup
};
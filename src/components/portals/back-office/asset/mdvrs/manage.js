﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenHelper from "../../../screenhelper";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestMDVRModel from "../../../../../app/frameworks/data/apitrackingcore/webRequestMDVRModel";
import WebRequestMDVR from "../../../../../app/frameworks/data/apitrackingcore/webRequestMDVR";
import WebRequestTicket from "../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";
import WebRequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicle";
import WebRequestContract from "../../../../../app/frameworks/data/apitrackingcore/webRequestContract";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import {
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import WebRequestOperatorPackage from "../../../../../app/frameworks/data/apitrackingcore/webRequestOperatorPackage"

class MdvrManageScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.id = params.id;
        // console.log("constructor");
        this.isBindingData = this.ensureNonObservable(false);

        this.mode = this.ensureNonObservable(params.mode);
        this.filter = this.ensureNonObservable(params.filter, {});
        this.bladeSize = BladeSize.Medium;
        this.detail = this.ensureNonObservable(params.detail, null);

        this.modelOptions = ko.observableArray([]);
        this.model = ko.observable('');

        this.deviceCode = ko.observable('');
        this.deviceId = ko.observable('');
        this.serialNo = ko.observable('');

        this.contractNoOptions = ko.observableArray([]);
        this.contractNo = ko.observable('');

        this.simOptions = ko.observableArray([]);
        this.sim = ko.observable('');

        this.preRecordOptions = ko.observableArray([]);
        this.preRecord = ko.observable('');

        this.postRecordOptions = ko.observableArray([]);
        this.postRecord = ko.observable('');

        this.channel = ko.observable('');

        this.domainSettingOptions = ko.observableArray([]);
        this.domainSetting = ko.observable('');

        this.companyOptions = ko.observableArray([]);
        this.company = ko.observable('');

        this.buOptions = ko.observableArray([]);
        this.bu = ko.observable('');

        this.vehicleOptions = ko.observableArray([]);
        this.vehicle = ko.observable('');

        this.simFilter = ko.observable({
            "simType": Enums.ModelData.SimType.MDVR
        });
        this.enableModel = this.mode == Screen.SCREEN_MODE_CREATE ? true : null;

        this.promotions = ko.observableArray([]);
        this.selectedPromotion = ko.observable();

        this.temp_mobile = null;

        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("Assets_Mdvrs_Create")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("Assets_Mdvrs_Update")());
                break;
        }

        this.selectedPromotion.ignorePokeSubscribe((value) => {
            this.simOptions([]);
            if (value) {
                let filter = {
                    operatorId: value.id,
                    simType: Enums.ModelData.SimType.MDVR,
                    status: Enums.ModelData.SimStatus.Active,
                    ids: this.mode == Screen.SCREEN_MODE_UPDATE ? [this.detail.simId] : null
                };
                this.loadSim(filter);
            }

        });
    }


    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    // get webRequestEnumResource() {
    //     return WebRequestEnumResource.getInstance();
    // }



    // get webRequestUser() {
    //     return WebRequestUser.getInstance();
    // }

    get webRequestContract() {
        return WebRequestContract.getInstance()
    }

    get webRequestVehicle() {
        return WebRequestVehicle.getInstance();
    }

    get webRequestMDVR() {
        return WebRequestMDVR.getInstance();
    }
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    get webRequestMDVRModel() {
        return WebRequestMDVRModel.getInstance();
    }
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    get webRequestTicket() {
        return WebRequestTicket.getInstance();
    }

    get webRequestOperatorPackage() {
        return WebRequestOperatorPackage.getInstance();
    }
    onLoad(isFirstLoad) {
        // console.log("onLoad")
        if (!isFirstLoad) {
            // console.log("!isFirstLoad")
            return;
        }


        let self = this;
        let preRecordOptions = [{
            value: 5
        }, {
            value: 10
        }, {
            value: 15
        }, {
            value: 20
        }, {
            value: 25
        }, {
            value: 30
        }, {
            value: 60
        }, {
            value: 120
        }, {
            value: 300
        }, {
            value: 600
        }]
        let postRecordOptions = [{
            value: 5
        }, {
            value: 10
        }, {
            value: 30
        }, {
            value: 60
        }, {
            value: 120
        }, {
            value: 300
        }, {
            value: 600
        }]
        this.preRecordOptions(preRecordOptions);
        this.postRecordOptions(postRecordOptions);
        var dfd = $.Deferred();

        var d0 = $.Deferred();
        //var d1 = this.mode === Screen.SCREEN_MODE_UPDATE ? this.webRequestMDVR.getMDVR(this.id) : null;
        //var d2 = $.Deferred(); // company
        //var d3 = $.Deferred(); // Bu
        //var d4 = $.Deferred(); // Vehicle 
        //var d5 = $.Deferred(); // Sim

        //var d6 = $.Deferred(); // DomainSetting
        //var d7 = $.Deferred(); // Model

        //var dfdBU = this.webRequestCompany.listCompanySummary();


        var dfdMVDRSelf = null;
        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            dfdMVDRSelf = this.webRequestMDVR.getMDVR(this.id);
        }

        var dfdContract = this.webRequestContract.listContract({
            "enable": true
        });
        var dfdMDVRModel = this.webRequestMDVRModel.listMDVRModelList({
            enable: this.enableModel
        });
        var dfdDomainSettings = this.webRequestMDVR.listMDVRConfigSummary();
        var dfdCompany = this.webRequestCompany.listCompanySummary();
        var dfdBU = null;
        var dfdVehicles = null;
        var webRequestOperator = this.webRequestOperatorPackage.listOperatorPackage({
            sortingColumns: DefaultSorting.OperatorPackageByOperatorPromotion
        })
        var webRequestSim = this.detail ? this.webRequestTicket.listSimManagement({
            operatorId: this.detail.operatorPackageId,
            simType: Enums.ModelData.SimType.MDVR,
            status: Enums.ModelData.SimStatus.Active,
            ids: this.mode == Screen.SCREEN_MODE_UPDATE ? [this.detail.simId] : null
        }) : null;

        $.when(dfdMVDRSelf, dfdContract).done((resMDVR, resContract) => {
            // console.log("when1",resMDVR)
            this.contractNoOptions(resContract["items"])
            if (resMDVR) {
                if (this.mode === Screen.SCREEN_MODE_UPDATE) {

                    // var companyId = 1; //Mock Data
                    // var businessUnitId = 20; //Mock Data
                    // console.log("resMDVR", resMDVR)
                    // console.log("resMDVR.companyId", resMDVR.companyId)
                    // console.log("resMDVR.businessUnitId", resMDVR.businessUnitId)

                    dfdBU = this.webRequestBusinessUnit.listBusinessUnitSummary({
                        companyId: resMDVR.companyId
                    });
                    dfdVehicles = this.webRequestVehicle.listVehicleSummary({
                        companyId: resMDVR.companyId,
                        businessUnitIds: [resMDVR.businessUnitId]
                    });
                }
            }

            $.when(dfdMDVRModel, dfdCompany, dfdDomainSettings, dfdBU, dfdVehicles, webRequestOperator, webRequestSim)
                .done((resMDVRModel, resCompany, resDomain, resBU, resVehicles, resOperator, resultSim) => {

                    if (resMDVRModel) {
                        this.modelOptions(resMDVRModel.items);
                    }

                    if (resCompany) {
                        this.companyOptions(resCompany.items);
                    }

                    // if (resSim) {
                    //     this.simOptions(resSim.items);
                    // }

                    if (resDomain) {
                        this.domainSettingOptions(resDomain.items);
                    }

                    if (resBU) {
                        this.buOptions(resBU.items);
                    }

                    if (resVehicles) {
                        this.vehicleOptions(resVehicles.items);
                    }

                    if (resOperator) {
                        this.promotions(resOperator["items"]);
                    }

                    if (resMDVR) {
                        this.deviceCode(resMDVR.deviceCode);

                        this.deviceId(resMDVR.deviceId);
                        this.serialNo(resMDVR.serialNo);
                        this.channel(resMDVR.cameraNo);

                        this.model(ScreenHelper.findOptionByProperty(this.modelOptions, "id", resMDVR.mdvrModelId));

                        let contractNo = !_.isNil(resMDVR.contractId) ? ScreenHelper.findOptionByProperty(this.contractNoOptions, "id", resMDVR.contractId) : null;
                        this.contractNo(contractNo)

                        this.sim(ScreenHelper.findOptionByProperty(this.simOptions, "id", resMDVR.simId));

                        this.preRecord(ScreenHelper.findOptionByProperty(this.preRecordOptions, "value", resMDVR.preRecord));
                        this.postRecord(ScreenHelper.findOptionByProperty(this.postRecordOptions, "value", resMDVR.postRecord));

                        //this.domainSetting(ScreenHelper.findOptionByProperty(this.simOptions, "id", resMDVR.simId)); 
                        this.domainSetting(ScreenHelper.findOptionByProperty(this.domainSettingOptions, "id", resMDVR.mdvrServerConfigurationId));


                        let companyId = !_.isNil(resMDVR.companyId) ? ScreenHelper.findOptionByProperty(this.companyOptions, "id", resMDVR.companyId) : null;
                        this.company(companyId);

                        let buId = !_.isNil(resMDVR.businessUnitId) ? ScreenHelper.findOptionByProperty(this.buOptions, "id", resMDVR.businessUnitId) : null;

                        this.bu(buId != null ? buId.id.toString() : null);

                        let vehicleId = !_.isNil(resMDVR.vehicleId) ? ScreenHelper.findOptionByProperty(this.vehicleOptions, "id", resMDVR.vehicleId) : null;
                        this.vehicle(vehicleId);

                        this.selectedPromotion.poke(ScreenHelper.findOptionByProperty(this.promotions, "id", resMDVR.operatorPackageId));

                        if (resMDVR.operatorPackageId) {
                            this.simOptions(resultSim["items"]);
                            let currSim = ScreenHelper.findOptionByProperty(this.simOptions, "id", resMDVR.simId);
                            this.sim(currSim);
                            this.temp_mobile = currSim;
                        }

                    }

                    this.company.subscribe((resCompany) => {

                        let companyId;
                        if (resCompany != undefined) {
                            companyId = resCompany.id
                        } else {
                            companyId = null
                        }
                        this.webRequestBusinessUnit.listBusinessUnitSummary({
                            companyId: companyId
                        }).done((res) => {
                            this.buOptions(res.items);

                        }).fail((e) => {

                        });
                    });

                    this.bu.subscribe((resBu) => {
                        let filter;
                        if (resBu != undefined) {
                            filter = {
                                companyId: this.company().id,
                                businessUnitIds: resBu
                            }
                        } else {
                            filter = null;
                        }

                        this.webRequestVehicle.listVehicleSummary(filter).done((res) => {

                            this.vehicleOptions(res.items);

                        }).fail((e) => {

                        });
                    });


                    dfd.resolve();
                });
        });
        // console.log("dfd");

        return dfd;

    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        // console.log("unload");
    }


    /**
     * Register observable property for change detection, validatiors.
     */
    setupExtend() {

        //trackChange
        this.deviceId.extend({
            trackChange: true
        });

        this.model.extend({
            required: true
        });
        this.deviceCode.extend({
            required: true
        })
        this.deviceId.extend({
            required: true
        });
        this.serialNo.extend({
            required: true
        });
        this.contractNo.extend({
            required: true
        });
        this.preRecord.extend({
            required: true
        });
        this.postRecord.extend({
            required: true
        });
        this.channel.extend({
            required: true
        });
        this.domainSetting.extend({
            required: true
        });
        // this.company.extend({
        //     required: true
        // });
        // this.vehicle.extend({
        //     required: true
        // });

        // Server validate
        this.deviceId.extend({
            serverValidate: {
                params: "DeviceId",
                message: this.i18n("M010")()
            }
        });
        this.serialNo.extend({
            serverValidate: {
                params: "SerialNo",
                message: this.i18n("M010")()
            }
        });
        this.deviceCode.extend({
            serverValidate: {
                params: "DeviceCode",
                message: this.i18n("M010")()
            }
        });
        this.validationModel = ko.validatedObservable({
            model: this.model,
            deviceId: this.deviceId,
            deviceCode: this.deviceCode,
            serialNo: this.serialNo,
            contractNo: this.contractNo,
            preRecord: this.preRecord,
            postRecord: this.postRecord,
            channel: this.channel,
            domainSetting: this.domainSetting
            // company: this.company,
            // vehicle: this.vehicle,
        });
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n('Common_Save')()));
        actions.push(this.createActionCancel());
    }

    buildCommandBar(commands) {
        if (this.mode === "update") {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onCommandClick(sender) {

        switch (sender.id) {

            case "cmdDelete":

                this.showMessageBox(null, "Do you want to delete this “MDVR Device ?", BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            let info = {
                                ids: [this.id]
                            };
                            this.isBusy(true);

                            if (this.temp_mobile) {
                                var dfdDelete = $.Deferred();
                                this.showMessageBox(null, this.i18n("M176", [this.temp_mobile.mobileNo]),
                                    BladeDialog.DIALOG_YESNO).done((button) => {
                                    switch (button) {
                                        case BladeDialog.BUTTON_YES:
                                            info.isCloseSim = true;
                                            dfdDelete.resolve();
                                            break;
                                        case BladeDialog.BUTTON_NO:
                                            info.isCloseSim = false;
                                            dfdDelete.resolve();
                                            break;
                                    }
                                });
                                $.when(dfdDelete).done(() => {
                                    this.deleteMDVR(info);
                                })

                            } else {
                                this.deleteMDVR(info);
                            }
                            break;
                    }
                });
                break;
        }
    }
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            let model = this.generateModel();

            switch (this.mode) {

                case Screen.SCREEN_MODE_CREATE:

                    model.infoStatus = 2;
                    // var dfdCreate = $.Deferred(); // Bu

                    this.webRequestMDVR.createMDVR(model).done((res) => {
                        this.isBusy(false);
                        this.publishMessage("bo-asset-mdvrs", this.model);
                        this.close(true);
                        // dfdCreate.resolve();

                    }).fail((e) => {
                        this.isBusy(false);
                        this.handleError(e);
                        // dfdCreate.reject(e);
                    })

                    break;

                case Screen.SCREEN_MODE_UPDATE:

                    model.id = this.id;
                    model.infoStatus = 3;
                    var isChangeMobileNumber = (this.temp_mobile && (model.simId != this.temp_mobile.id)) ? true : false;

                    if (isChangeMobileNumber) {
                        this.showMessageBox(null, this.i18n("M176", [this.temp_mobile.mobileNo]),
                            BladeDialog.DIALOG_YESNO).done((button) => {
                            switch (button) {
                                case BladeDialog.BUTTON_YES:
                                    this.isBusy(true);
                                    model.isCloseSim = true;
                                    this.updateMDVR(model);
                                    break;
                                case BladeDialog.BUTTON_NO:
                                    this.isBusy(true);
                                    model.isCloseSim = false;
                                    this.updateMDVR(model);
                                    break;
                            }
                        });
                    } else {
                        this.updateMDVR(model);
                    }
                    break;
            }
        }
    }

    generateModel() {

        let filterModel = {


            "mdvrModelId": this.model().id,
            "deviceCode": this.deviceCode(),
            "deviceId": this.deviceId(),
            "serialNo": this.serialNo(),
            // "contractId": 12, //hard code
            "contractId": this.contractNo().id,
            "simId": this.sim() != undefined ? this.sim().id : null,
            "preRecord": this.preRecord().value,
            "postRecord": this.postRecord().value,
            "cameraNo": this.channel(),
            "mdvrServerConfigurationId": this.domainSetting().id,
            "vehicleId": (this.vehicle()) ? this.vehicle().id : null,
            "infoStatus": Screen.SCREEN_MODE_UPDATE ? 3 : 2

        };
        return filterModel;
    }

    loadSim(filter) {
        var dfd = $.Deferred();

        this.webRequestTicket.listSimManagement(filter).done((r) => {
            this.simOptions(r["items"]);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    updateMDVR(info) {
        this.webRequestMDVR.updateMDVR(info).done(() => {
            this.isBusy(false);
            this.publishMessage("bo-asset-mdvrs", this.filter);
            // this.publishMessage("bo-configuration-map-service");
            this.close(true);
        }).fail((e) => {
            this.isBusy(false);
            this.handleError(e);
        });
    }

    deleteMDVR(info) {
        this.webRequestMDVR.deleteMDVR(info).done(() => {
            this.isBusy(false);
            this.publishMessage("bo-asset-mdvrs", this.id);
            // this.publishMessage("bo-configuration-map-service");
            this.close(true);
        }).fail((e) => {
            this.handleError(e);
            this.isBusy(false);

        });
    }

}

export default {
    viewModel: ScreenBase.createFactory(MdvrManageScreen),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./manage2.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenHelper from "../../../screenhelper";
import ScreenBase from "../../../screenbase";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
// import WebRequestMDVR from "../../../../../app/frameworks/data/apitrackingcore/webRequestMDVR";
import WebRequestMDVR from "../../../../../app/frameworks/data/apitrackingcore/webRequestMDVR";
import WebRequestDatabaseConnection from "../../../../../app/frameworks/data/apitrackingcore/webRequestDatabaseConnection";

class MdvrManageScreenTwo extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n('Manager 2')());
        this.bladeSize = BladeSize.Medium;

        this.homeparam = ko.observable("home2")


    }
   
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
     
    }

   
    

    refreshShipmentList() {
        
    }
  

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {

    }

    /**
     * Register observable property for change detection, validatiors.
     */
    setupExtend() {
        //trackChange

    }
    _selectingRowHandler(){

    }
    selectedTrackDB(){

    }



    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        // actions.push(this.createAction("actLiveView", this.i18n('Common_Display')()));
        // actions.push(this.createActionCancel());
    }

    buildCommandBar(commands) {
        //commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        // commands.push(this.createCommand("cmdUpdate", this.i18n("TestUpdate")(), "svg-icon-start-plugin"));
        
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onCommandClick(sender) {
       
    }
    onActionClick(sender) {
        super.onActionClick(sender);
        // if (sender.id === "actLiveView") {

        // }
    }

    onMapComplete(data) {
       
    }
}

export default {
    viewModel: ScreenBase.createFactory(MdvrManageScreenTwo),
    template: templateMarkup
};
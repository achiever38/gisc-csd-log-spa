﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenHelper from "../../../screenhelper";
import ScreenBase from "../../../screenbase";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
// import WebRequestMDVR from "../../../../../app/frameworks/data/apitrackingcore/webRequestMDVR";
import WebRequestMDVR from "../../../../../app/frameworks/data/apitrackingcore/webRequestMDVR";
import WebRequestDatabaseConnection from "../../../../../app/frameworks/data/apitrackingcore/webRequestDatabaseConnection";

class MdvrListScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n('MDVR')());
        this.bladeSize = BladeSize.Medium;
        this.bladeIsMaximized = true;

        this.listMDVRs = ko.observableArray([])


        this.trackingDBs = ko.observableArray([]);
        this.filterText = ko.observable('');
        this.selectedTrackDB = ko.observable();
        this.recentChangedRowIds = ko.observableArray([]);
        //this.gridFilter = ko.observable();
        this.filters = ko.observable({});
        this.order = ko.observable([
            [0, "asc"]
        ]);
        this.apiDataSource = ko.observableArray([]);
        this.selectedItems = ko.observableArray([]);

        this._selectingRowHandler = (res) => {
            return this.navigate("bo-asset-mdvrs-manage", {
                mode: "update",
                id: res.id,
                filter: this.filters(),
                detail: res
            });
        };

        this.subscribeMessage("bo-asset-mdvrs", filter => {
            this.loadGridData(filter);
        });

        this.subscribeMessage("bo-asset-mdvr-search", (filter) => {
            this.loadGridData(filter);
        });
    }
    get webRequestDatabaseConnection() {
        return WebRequestDatabaseConnection.getInstance();
    }
    // get webRequestMDVR() {
    //     return WebRequestMDVR.getInstance();
    // }
    get webRequestMDVR() {
        return WebRequestMDVR.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
    }

    onDatasourceRequestRead(gridOptions) {
        this.isBusy(true);
        let dfd = $.Deferred();
        let filters =  _.assign({}, gridOptions, this.filters());

        this.webRequestMDVR.listMDVRSummary(filters).done((res) => {
            dfd.resolve({
                items: res.items,
                totalRecords: res.totalRecords,
                currentPage: res.currentPage
            });
        }).fail((e) => {
            this.handleError(e);
        }).always(() => {
            this.isBusy(false);
        });


        return dfd;
    }



    refreshShipmentList() {

    }


    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {

    }

    /**
     * Register observable property for change detection, validatiors.
     */
    setupExtend() {
        //trackChange

    }
    _selectingRowHandler() {

    }
    selectedTrackDB() {

    }



    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {

    }

    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        commands.push(this.createCommand("cmdUpdate", this.i18n("Update Multiple")(), "svg-ticket-create-appointment"));
        commands.push(this.createCommand("cmdImport", this.i18n("Common_Import")(), "svg-cmd-import"));
        commands.push(this.createCommand("cmdExport", this.i18n("Export")(), "svg-cmd-export"));
        commands.push(this.createCommand("cmdSearch", this.i18n("Common_Search")(), "svg-cmd-search"));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdCreate":
                this.navigate("bo-asset-mdvrs-manage", {
                    mode: "create",
                    filter: this.filters()
                });
                break;
            case "cmdUpdate":
                if (_.size(this.selectedItems()) <= 0) {
                    this.showMessageBox("", "Please select at least one item.", BladeDialog.DIALOG_OK);
                    return false;
                }

                this.navigate("bo-asset-mdvrs-update-multiple", {
                    items: this.selectedItems(),
                    filter: this.filters()
                });
                break;
            case "cmdExport":
                this.showMessageBox(null, this.i18n("M128")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.webRequestMDVR.ExportMDVR({}).done((response) => {
                                ScreenHelper.downloadFile(response.fileUrl);
                            }).fail((e) => {
                                this.handleError(e);
                            }).always(() => {
                                this.isBusy(false);
                            });
                            break;
                    }
                });
                break;
            case "cmdImport":
                this.navigate("bo-asset-mdvrs-import");
                break;
            case "cmdSearch":
                this.navigate("bo-asset-mdvrs-search");
                break;
        }
    }
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actLiveView") {

        }
    }

    onMapComplete(data) {
        switch (data.command) {
            case "open-liveview":
                // this.minimizeAll();
                break;
        }
    }

    loadGridData(filter) {
        this.filters(filter);
        this.dispatchEvent("dtMDVRsList", "refresh");
    }
}

export default {
    viewModel: ScreenBase.createFactory(MdvrListScreen),
    template: templateMarkup
};
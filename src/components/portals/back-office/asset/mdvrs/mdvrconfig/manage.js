﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import * as Screen from "../../../../../../app/frameworks/constant/screen";
import ScreenHelper from "../../../../screenhelper";
import ScreenBase from "../../../../screenbase";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
import WebRequestMDVR from "../../../../../../app/frameworks/data/apitrackingcore/webRequestMDVR";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import { Constants, Enums } from "../../../../../../app/frameworks/constant/apiConstant";
import EncryptUtility from "../../../../../../app/frameworks/core/encryptUtility"
import DefaultSorting from "../../../../../../app/frameworks/constant/defaultSorting";
import WebRequestCompany from "../../../../../../app/frameworks/data/apicore/webrequestCompany";


class MdvrConfigManageScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.mode = params.mode
        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n('MDVR Configuration Create')());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n('MDVR Configuration Update')());
                this.mdvrModel = params.id;
                break;
        }
        this.mdvrConfigId = this.ensureNonObservable(params.mdvrConfigId,0);
        this.bladeSize = BladeSize.Medium;
        this.validationModel = ko.validatedObservable({});


        this.configName = ko.observable();
        this.configDesc = ko.observable();
        this.configUserName = ko.observable();
        this.configPassWord = ko.observable();
        this.configIP = ko.observable();

        this.companyOptions = ko.observableArray([]); 
        this.companyId = ko.observable();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
       
        var dfd = $.Deferred();
        var dfdMDVRConfig = null;
        var d1 = this.webRequestCompany.listCompanySummary({ sortingColumns: DefaultSorting.CompanyByName });

        if(this.mdvrConfigId){
            dfdMDVRConfig = this.webRequestMDVR.getMDVRConfig(this.mdvrConfigId);
        }

        $.when(dfdMDVRConfig,d1).done((resMDVRConfig,rescompany)=>{
            this.companyOptions(rescompany["items"]);
            if(resMDVRConfig){
                var config = this.encryptUtility.decryption(resMDVRConfig.value)
                var result = config.split("|")
                this.configName(resMDVRConfig.name);
                this.configDesc(resMDVRConfig.description);
                this.configUserName(result[0]);
                this.configPassWord(result[1]);
                this.configIP(result[2])

                this.companyId(ScreenHelper.findOptionByProperty(this.companyOptions,"id",resMDVRConfig.companyId));
            }

            dfd.resolve();
        })
        return dfd;

    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {

    }
    get encryptUtility() {
        return EncryptUtility.getInstance();
    }
    get webRequestMDVR() {
        return WebRequestMDVR.getInstance();
    }
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    /**
     * Register observable property for change detection, validatiors.
     */
    setupExtend() {
        //trackChange
        this.configName.extend({
            trackChange: true
        });
        this.configDesc.extend({
            trackChange: true
        });
        this.configUserName.extend({
            trackChange: true
        });
        this.configPassWord.extend({
            trackChange: true
        });
        this.configIP.extend({
            trackChange: true
        });
        this.companyId.extend({
            trackChange: true,
            required: true
        });

        // Setup validation properties.
        this.configName.extend({
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });
        this.configUserName.extend({
            required: true
        });
        this.configPassWord.extend({
            required: true
        });
        this.configIP.extend({
            required: true
        });
       

        // Compose validation model.
        this.validationModel({
            name: this.configName,
            username: this.configUserName,
            password: this.configPassWord,
            ip:this.configIP,
            companyId: this.companyId
        });
    }



    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n('Common_Save')()));
        actions.push(this.createActionCancel());
    }

    buildCommandBar(commands) {
        switch (this.mode) {
            case Screen.SCREEN_MODE_UPDATE:
                if (WebConfig.userSession.hasPermission(Constants.Permission.DeleteMDVRModel)) {
                    commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
                }
                break;
        }
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdDelete":
                this.showMessageBox(null, "Do you want to delete this “MDVR Configuration”?", BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.webRequestMDVR.deleteMDVRConfig(this.mdvrConfigId)
                                .done(() => {
                                    this.publishMessage("bo-asset-mdvrs-config-changed", this.mdvrConfigId);
                                    this.close(true);
                                })
                                .fail((e) => {
                                    this.handleError(e);
                                })
                                .always(() => {
                                    this.isBusy(false);
                                });
                            break;
                    }
                });
                break;
        }
    }
    onActionClick(sender) {
        super.onActionClick(sender);
        switch (sender.id) {
            case "actSave":
                if (!this.validationModel.isValid()) {
                    this.validationModel.errors.showAllMessages();
                    return;
                }

                this.isBusy(true);

                var key = this.encryptUtility.encryption(this.configUserName(),this.configPassWord(),this.configIP())
                var filter = {
                    name: this.configName(),
                    description: this.configDesc(),
                    value: key,
                    companyId: this.companyId() ? this.companyId().id : null
                }

                switch (this.mode) {
                    case Screen.SCREEN_MODE_CREATE:
                        this.webRequestMDVR.createMDVRConfig(filter, true)
                            .done((vehicleModel) => {
                                this.publishMessage("bo-asset-mdvrs-config-changed", vehicleModel.id);
                                this.close(true);
                            })
                            .fail((e) => {
                                this.handleError(e);
                            })
                            .always(() => {
                                this.isBusy(false);
                            });
                        break;
                    case Screen.SCREEN_MODE_UPDATE:
                        filter.id = this.mdvrConfigId;
                        this.webRequestMDVR.updateMDVRConfig(filter)
                            .done(() => {
                                this.publishMessage("bo-asset-mdvrs-config-changed", this.id);
                                this.close(true);
                            })
                            .fail((e) => {
                                this.handleError(e);
                            })
                            .always(() => {
                                this.isBusy(false);
                            });
                        break;
                }
        }
    }



    onMapComplete(data) {
        switch (data.command) {
            case "open-liveview":
                this.minimizeAll();
                break;
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(MdvrConfigManageScreen),
    template: templateMarkup
};
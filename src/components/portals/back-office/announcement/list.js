﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../screenbase";
import * as Screen from "../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import WebRequestAnnouncement from "../../../../app/frameworks/data/apicore/webRequestAnnouncement";
import { Constants } from "../../../../app/frameworks/constant/apiConstant";
import { SearchFilter } from "./info";

class AnnouncementListScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties
        this.bladeTitle(this.i18n("Announcements_Announcements")());
        this.bladeSize = BladeSize.Medium;

        this.annoucements = ko.observableArray([]);
        
        this.filterText = ko.observable("");
        this.selectedAnnoucement = ko.observable(null);
        this.recentChangedRowIds = ko.observableArray([]);
        this.order = ko.observable([[ 1, "desc" ]]);

        this.pageFiltering = ko.observable(false);
        this.searchFilter = ko.observable(new SearchFilter());
        this.searchFilterDefault = new SearchFilter();


        this._selectingRowHandler = (annoucement) => {
            if(annoucement && WebConfig.userSession.hasPermission(Constants.Permission.UpdateAnnouncement)) {
                return this.navigate("bo-announcement-manage", { mode: "update", id: annoucement.id });
            }
            return false;
        };

        // Subscribe Message when box search filter apply
        this.subscribeMessage("bo-announcements-search-filter-changed", (searchFilter) => {

            this.isBusy(true);
            this.searchFilter(searchFilter);

            this.webRequestAnnouncement.listAnnouncementSummary(searchFilter).done((response) => {

                this.annoucements.replaceAll(response["items"]);
                this.isBusy(false);
            }).fail((e) => {
                this.isBusy(false);
                this.handleError(e);
            });
        });

        this._changeSearchFilterSubscribe = this.searchFilter.subscribe((searchFilter) => {
            // apply active command state if searchFilter does not match default search
            this.pageFiltering(!_.isEqual(this.searchFilter(), this.searchFilterDefault));
        });

        // Subscribe Message when annoucement created/updated
        this.subscribeMessage("bo-announcement-changed", (annoucementId) => {
            this.webRequestAnnouncement.listAnnouncementSummary().done((response) => {
                var annoucements = response["items"];
                this.annoucements.replaceAll(annoucements);
                this.recentChangedRowIds.replaceAll([annoucementId]);
            });
        });
    }

    /**
     * Get WebRequest specific for Announcement in Core Web API access.
     * @readonly
     */
    get webRequestAnnouncement() {
        return WebRequestAnnouncement.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
         if (!isFirstLoad) return;

        var dfd = $.Deferred();

        this.webRequestAnnouncement.listAnnouncementSummary().done((response) => {
            var annoucement = response["items"];
            this.annoucements(annoucement);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }

     /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.CreateAnnouncement)) {
           commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
           commands.push(this.createCommand("cmdSearch", this.i18n("Common_Search")(), "svg-cmd-search", true, true, this.pageFiltering));
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {

        switch (sender.id) {
            case "cmdCreate":
                this.navigate("bo-announcement-manage", { mode: Screen.SCREEN_MODE_CREATE });
                this.selectedAnnoucement(null);
                this.recentChangedRowIds.removeAll();
                break;
        
            case "cmdSearch":
                this.navigate("bo-announcement-search", { searchFilter: this.searchFilter() }); 
                break;
        }
    }

     /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedAnnoucement(null);
    }
}

export default {
    viewModel: ScreenBase.createFactory(AnnouncementListScreen),
    template: templateMarkup
};
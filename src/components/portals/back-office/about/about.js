import ko from "knockout";
import templateMarkup from "text!./about.html";
import ScreenBase from "../../screenbase";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";

/**
 * Handle update user preferrences require restart when change.
 * 
 * @class UserPreferenceScreen
 * @extends {ScreenBase}
 */
class AboutScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Blade properties.
        this.bladeTitle(this.i18n("Common_About")());

        this.productName = this.ensureNonObservable(WebConfig.appSettings.productName, "Nostra Logistics");
        this.versionNumber = this.ensureNonObservable(WebConfig.appSettings.versionNumber, "4.0.0");
        this.buildNumber = this.ensureNonObservable(WebConfig.appSettings.buildNumber, "20190101");

    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
    }
    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(AboutScreen),
    template: templateMarkup
};
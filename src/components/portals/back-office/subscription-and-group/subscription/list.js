﻿import "jquery";
import ko from "knockout";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenBase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import templateMarkup from "text!./list.html";
import WebRequestSubscription from "../../../../../app/frameworks/data/apicore/webRequestSubscription";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import {Constants} from "../../../../../app/frameworks/constant/apiConstant";
import DefaultSoring from "../../../../../app/frameworks/constant/defaultSorting";

class SubscriptionsListScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Common_Subscriptions")());
        this.bladeSize = BladeSize.Medium;

        this.subscriptionList = ko.observableArray([]);
        this.filterText = ko.observable('');
        this.selectedSubscription = ko.observable(null);
        this.recentChangedRowIds = ko.observableArray([]);
        this.order = ko.observable([[ 0, "asc" ]]);

        this.filter = {
            sortingColumns: DefaultSoring.Subscription
        };

        // Determine if selecting action is legal, return false to cancel selecting event.
        this._selectingRowHandler = (subscription) => {
            if(subscription) {
                return this.navigate("bo-subscription-and-group-subscription-manage", {
                    mode: Screen.SCREEN_MODE_UPDATE,
                    subscriptionId: subscription.id
                });
            }
            return false;
        };

        // Subscribe to journal based communication for create/update/delete data
        this.subscribeMessage("bo-subscription-change", (subscriptionId) => {
            this.isBusy(true);
            this.webRequestSubscription.listSubscriptionSummary(this.filter).done((r)=> {
                 this.subscriptionList.replaceAll(r.items);
                 this.recentChangedRowIds.replaceAll([subscriptionId]);
                 this.isBusy(false);
            }).fail((e)=> {
                this.handleError(e);
                this.isBusy(false);
            });
        });
    }
    /**
     * Get WebRequest specific for Subscription module in Web API access.
     * @readonly
     */
    get webRequestSubscription() {
        return WebRequestSubscription.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();
        this.webRequestSubscription.listSubscriptionSummary(this.filter).done((r)=> {
            this.subscriptionList(r["items"]);
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }
    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedSubscription(null);
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.CreateSubscription)) {
           commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdCreate") {
            this.navigate("bo-subscription-and-group-subscription-manage", {
                mode: Screen.SCREEN_MODE_CREATE
            });
        }
        this.selectedSubscription(null);
        this.recentChangedRowIds.removeAll();
    }
}

export default {
    viewModel: ScreenBase.createFactory(SubscriptionsListScreen),
    template: templateMarkup
};
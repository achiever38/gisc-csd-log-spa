﻿import ko from "knockout";
import templateMarkup from "text!./menu.html";
import ScreenBase from "../../screenbase";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";

class MenuScreen extends ScreenBase {

    /**
     * Creates an instance of MenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Subscriptions_SubscriptionManagement")());
        this.bladeMargin = BladeMargin.Narrow;

        this.selectedIndex = ko.observable();

        this._selectingRowHandler = (extras) => {
            if (extras) {
                return this.navigate(extras.id);
            }
            return false;
        };
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {}

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedIndex(null);
    }
}

export default {
    viewModel: ScreenBase.createFactory(MenuScreen),
    template: templateMarkup
};
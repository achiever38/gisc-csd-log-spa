﻿import "jquery";
import ko from "knockout";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenBase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import templateMarkup from "text!./list.html";
import WebRequestSubscription from "../../../../../app/frameworks/data/apicore/webRequestSubscription";
import WebRequestTechnicianTeam from "../../../../../app/frameworks/data/apicore/webRequestTechnicianTeam";

import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import { Constants } from "../../../../../app/frameworks/constant/apiConstant";
import DefaultSoring from "../../../../../app/frameworks/constant/defaultSorting";

class TechnicianTeamListScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Common_Technician_Team")());
        this.bladeSize = BladeSize.Medium;

        this.order = ko.observable([[ 0, "asc" ]]);
        this.technicianTeamList = ko.observableArray([]);
        this.selectedSubscription = ko.observable(null);
        this.filterText = ko.observable('');
        this.recentChangedRowIds = ko.observableArray([]);

        this._selectingRowHandler = (res) => {
            console.log("res",res)
            if(res) {
                return this.navigate("bo-subscription-and-group-technician-team-manage", {
                    mode: Screen.SCREEN_MODE_UPDATE,
                    technicianTeamId: res.id
                });
            }
            return false;
        };

        // Subscribe to journal based communication for create/update/delete data
            this.subscribeMessage("bo-technicianteam-change", (technicianId) => {
                this.isBusy(true);
                this.webRequestTechnicianTeam.listTechnicianTeamSummary(this.filter).done((r)=> {
                    console.log("r",r)
                     this.technicianTeamList.replaceAll(r.items);
                     this.recentChangedRowIds.replaceAll([technicianId]);
                     this.isBusy(false);
                }).fail((e)=> {
                    this.handleError(e);
                    this.isBusy(false);
                });
            });


    }
    /**
     * Get WebRequest specific for Subscription module in Web API access.
     * @readonly
     */
    get webRequestSubscription() {
        return WebRequestSubscription.getInstance();
    }

    get webRequestTechnicianTeam() {
        return WebRequestTechnicianTeam.getInstance();
    }
    
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var dfd = $.Deferred();
        this.webRequestTechnicianTeam.listTechnicianTeamSummary().done((response) => {
           this.technicianTeamList(response.items);
           dfd.resolve();
        }).fail((e) => {
           dfd.reject(e);
        });
        return dfd;
    }
    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        //this.selectedSubscription(null);
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.CreateSubscription)) {
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdCreate") {
            this.navigate("bo-subscription-and-group-technician-team-manage", {
               mode: Screen.SCREEN_MODE_CREATE
            });
        }
        //this.selectedSubscription(null);
        //this.recentChangedRowIds.removeAll();
    }
}

export default {
    viewModel: ScreenBase.createFactory(TechnicianTeamListScreen),
    template: templateMarkup
};
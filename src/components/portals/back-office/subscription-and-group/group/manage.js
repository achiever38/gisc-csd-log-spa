﻿import "jquery";
import ko from "knockout";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenBase";
import templateMarkup from "text!./manage.html";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebRequestPermission from "../../../../../app/frameworks/data/apicore/webRequestPermission";
import WebRequestSubscription from "../../../../../app/frameworks/data/apicore/webRequestSubscription";
import WebRequestGroup from "../../../../../app/frameworks/data/apicore/webRequestGroup";
import {Enums, EntityAssociation, Constants} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import DefaultSoring from "../../../../../app/frameworks/constant/defaultSorting";

class GroupManageScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Large;

        this.mode = this.ensureNonObservable(params.mode);

        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
               this.bladeTitle(this.i18n("Common_CreateUserGroup")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("Common_UpdateUserGroup")());
                break;
        }

        this.groupId = this.ensureNonObservable(params.userGroupId, -1);
        this.name = ko.observable('');
        this.description = ko.observable('');

        this.permissionCategoryVisibleDisplayName = ko.observable('');
        this.permissionCategoryFunctionalityDisplayName = ko.observable('');
        this.permissionCategoryAlertDisplayName = ko.observable('');

        this.permissionCategoryVisible = ko.observableArray([]);
        this.permissionCategoryFunctionality = ko.observableArray([]);
        this.permissionCategoryAlert = ko.observableArray([]);

        this.subscriptionList = ko.observableArray([]);
        this.colspan = ko.observable();

        this._originalPermissions = ko.observableArray([]);
    }
    /**
     * Get WebRequest specific for Pemission module in Web API access.
     * @readonly
     */
    get webRequestPermission() {
        return WebRequestPermission.getInstance();
    }

    /**
     * Get WebRequest specific for Subscription module in Web API access.
     * @readonly
     */
    get webRequestSubscription() {
        return WebRequestSubscription.getInstance();
    }

    /**
     * Get WebRequest specific for Group module in Web API access.
     * @readonly
     */
    get webRequestGroup() {
        return WebRequestGroup.getInstance();
    } 
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();
        var permissionFilter = {
            portal: Enums.ModelData.Portal.Company,
            includeAssociationNames: [EntityAssociation.Permission.ChildPermissions],
            sortingColumns: DefaultSoring.Permission
        };
        var subscriptionFilter = {
            sortingColumns: DefaultSoring.Subscription
        };

        var d1 = this.webRequestPermission.listPermission(permissionFilter);
        var d2 = this.webRequestSubscription.listSubscriptionSummary(subscriptionFilter);
        var d3 = (this.mode === Screen.SCREEN_MODE_UPDATE) ? this.webRequestGroup.getGroup(this.groupId, [EntityAssociation.Group.GroupPermissions]) : null;

        $.when(d1, d2, d3).done((r1, r2, r3) => {
            var permissions = r1["items"];
            var subscriptions = r2["items"];
            var userGroup = r3;

            var permission;
            var permissionChild;
            var permissionChild2;
            var allPermission = [];
            permissions.forEach((permissionItem) => {
                permission = new PermissionConfig(permissionItem.id, permissionItem.name);
                permission.category = permissionItem.category;

                switch (permission.category) {
                    case Enums.ModelData.PermissionCategory.Visibility:
                        this.permissionCategoryVisibleDisplayName(permissionItem.categoryDisplayName);
                        break;
                    case Enums.ModelData.PermissionCategory.Functionality:
                        this.permissionCategoryFunctionalityDisplayName(permissionItem.categoryDisplayName);
                        break;
                    case Enums.ModelData.PermissionCategory.Alert:
                        this.permissionCategoryAlertDisplayName(permissionItem.categoryDisplayName);
                        break;
                }

                subscriptions.forEach((subscriptionItem) => {
                    permission.subscriptions.push(new SubscriptionConfig(subscriptionItem.id, subscriptionItem.name, false, permission));
                });

                permissionItem.childPermissions.forEach((permissionChildItem) => {
                    permissionChild = new PermissionConfig(permissionChildItem.id, permissionChildItem.name, [], permission);
                    subscriptions.forEach((subscriptionItem) => {
                        permissionChild.subscriptions.push(new SubscriptionConfig(subscriptionItem.id, subscriptionItem.name, false, permissionChild));
                    });
                    //add child level 2
                    if(permissionChildItem.childPermissions.length > 0){
                        permissionChildItem.childPermissions.forEach((permissionChildItem2) => {
                            permissionChild2 = new PermissionConfig(permissionChildItem2.id, permissionChildItem2.name, [], permissionChild);
                            subscriptions.forEach((subscriptionItem) => {
                                permissionChild2.subscriptions.push(new SubscriptionConfig(subscriptionItem.id, subscriptionItem.name, false, permissionChild2));
                            });
                            permissionChild.children.push(permissionChild2);
                        });
                    }

                    permission.children.push(permissionChild);
                });
                allPermission.push(permission);
            });
            
            this.subscriptionList(r2["items"]);
            this.colspan(this.subscriptionList().length + 1);

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.permissionCategoryVisible(this.permissionMapItem(allPermission, Enums.ModelData.PermissionCategory.Visibility));
                    this.permissionCategoryFunctionality(this.permissionMapItem(allPermission, Enums.ModelData.PermissionCategory.Functionality));
                    this.permissionCategoryAlert(this.permissionMapItem(allPermission, Enums.ModelData.PermissionCategory.Alert));

                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    if (userGroup) {
                        this.name(userGroup.name);
                        this.description(userGroup.description);

                        var permissions = this.mergeGroupPermissions(userGroup.groupPermissions, allPermission);
                        this.permissionCategoryVisible(this.permissionMapItem(permissions, Enums.ModelData.PermissionCategory.Visibility));
                        this.permissionCategoryFunctionality(this.permissionMapItem(permissions, Enums.ModelData.PermissionCategory.Functionality));
                        this.permissionCategoryAlert(this.permissionMapItem(permissions, Enums.ModelData.PermissionCategory.Alert));

                        this._originalPermissions = $.extend(true, [], userGroup.groupPermissions);
                    } else {
                        this.permissionCategoryVisible(this.permissionMapItem(allPermission, Enums.ModelData.PermissionCategory.Visibility));
                        this.permissionCategoryFunctionality(this.permissionMapItem(allPermission, Enums.ModelData.PermissionCategory.Functionality));
                        this.permissionCategoryAlert(this.permissionMapItem(allPermission, Enums.ModelData.PermissionCategory.Alert));
                    }

                    break;
            }

            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        if((this.mode === Screen.SCREEN_MODE_CREATE && WebConfig.userSession.hasPermission(Constants.Permission.CreateGroup_BackOffice))
            || (this.mode === Screen.SCREEN_MODE_UPDATE && WebConfig.userSession.hasPermission(Constants.Permission.UpdateGroup_BackOffice))) 
        {
            actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        }
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (this.mode === Screen.SCREEN_MODE_UPDATE && WebConfig.userSession.hasPermission(Constants.Permission.DeleteGroup_BackOffice)) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }
    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        this.name.extend({
            trackChange: true
        });
        this.description.extend({
            trackChange: true
        });
        this.permissionCategoryVisible.extend({
           trackArrayChange: {
                stationaryObject: true
            }
        });
        this.permissionCategoryFunctionality.extend({
           trackArrayChange: {
                stationaryObject: true
            }
        });
        this.permissionCategoryAlert.extend({
           trackArrayChange: {
                stationaryObject: true
            }
        });

        // Manual setup validation rules
        this.name.extend({
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });

        this.validationModel = ko.validatedObservable({
            name: this.name,
        });
    }
    /**
     * Generate user model from View Model
     * 
     * @returns user model which is ready for ajax request 
     */
    generateModel() {

        //Merge permission data from different category 
        var mergePermission = this.permissionCategoryVisible().concat(this.permissionCategoryFunctionality(), this.permissionCategoryAlert());
        var groupPermissions = [];
         switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                mergePermission.forEach((groupPermissionsItem) => {
                    groupPermissionsItem.subscriptions.forEach((subscriptionItem) => {
                        if (subscriptionItem.selected() === true) {
                            groupPermissions.push({
                                permissionId: groupPermissionsItem.id,
                                subscriptionId: subscriptionItem.subscriptionId,
                                infoStatus: Enums.InfoStatus.Add
                            });
                        }
                    });
                    groupPermissionsItem.children.forEach((groupPermissionsChildItem) => {
                        groupPermissionsChildItem.subscriptions.forEach((subscriptionChildItem) => {
                            if (subscriptionChildItem.selected() === true) {
                                groupPermissions.push({
                                    permissionId: groupPermissionsChildItem.id,
                                    subscriptionId: subscriptionChildItem.subscriptionId,
                                    infoStatus: Enums.InfoStatus.Add
                                });
                            }
                        });

                        //Children2
                        if(groupPermissionsChildItem.children.length > 0){
                            groupPermissionsChildItem.children.forEach((groupPermissionsChildItem2) => {
                                groupPermissionsChildItem2.subscriptions.forEach((subscriptionChildItem2) => {
                                    if (subscriptionChildItem2.selected() === true) {
                                        groupPermissions.push({
                                            permissionId: groupPermissionsChildItem2.id,
                                            subscriptionId: subscriptionChildItem2.subscriptionId,
                                            infoStatus: Enums.InfoStatus.Add
                                        });
                                    }
                                });
                            });
                        }
                    });
                });
                break;
            case Screen.SCREEN_MODE_UPDATE:
                mergePermission.forEach((groupPermissionsItem) => {
                    //Parent
                    groupPermissionsItem.subscriptions.forEach((subscriptionItem) => {
                        var originaPermission = ko.utils.arrayFirst(this._originalPermissions, function (item)
                        {
                            return item.id === subscriptionItem.id;
                        });

                        if(originaPermission) {
                            if (subscriptionItem.selected() === false) {
                                groupPermissions.push({
                                    id: originaPermission.id,
                                    permissionId: groupPermissionsItem.id,
                                    subscriptionId: subscriptionItem.subscriptionId,
                                    infoStatus: Enums.InfoStatus.Delete
                                });
                            }
                        }
                        else{
                            if (subscriptionItem.selected() === true) {
                                groupPermissions.push({
                                    permissionId: groupPermissionsItem.id,
                                    subscriptionId: subscriptionItem.subscriptionId,
                                    infoStatus: Enums.InfoStatus.Add
                                });
                            }
                        }
                    });
                    //Children
                    groupPermissionsItem.children.forEach((groupPermissionsChildItem) => {
                        groupPermissionsChildItem.subscriptions.forEach((subscriptionChildItem) => {
                            var originaPermission = ko.utils.arrayFirst(this._originalPermissions, function (item)
                            {
                                return item.id === subscriptionChildItem.id;
                            });
                            if(originaPermission) {
                                 if (subscriptionChildItem.selected() === false) {
                                    groupPermissions.push({
                                        id: originaPermission.id,
                                        permissionId: groupPermissionsChildItem.id,
                                        subscriptionId: subscriptionChildItem.subscriptionId,
                                        infoStatus: Enums.InfoStatus.Delete
                                    });
                                }
                            }else{
                                if (subscriptionChildItem.selected() === true) {
                                    groupPermissions.push({
                                        permissionId: groupPermissionsChildItem.id,
                                        subscriptionId: subscriptionChildItem.subscriptionId,
                                        infoStatus: Enums.InfoStatus.Add
                                    });
                                }
                            }
                        });

                        //Children2
                        if(groupPermissionsChildItem.children.length > 0){
                            groupPermissionsChildItem.children.forEach((groupPermissionsChildItem2) => {
                                groupPermissionsChildItem2.subscriptions.forEach((subscriptionChildItem2) => {
                                    var originaPermission = ko.utils.arrayFirst(this._originalPermissions, function (item)
                                    {
                                        return item.id === subscriptionChildItem2.id;
                                    });
                                    if(originaPermission) {
                                        if (subscriptionChildItem2.selected() === false) {
                                            groupPermissions.push({
                                                id: originaPermission.id,
                                                permissionId: groupPermissionsChildItem2.id,
                                                subscriptionId: subscriptionChildItem2.subscriptionId,
                                                infoStatus: Enums.InfoStatus.Delete
                                            });
                                        }
                                    }else{
                                        if (subscriptionChildItem2.selected() === true) {
                                            groupPermissions.push({
                                                permissionId: groupPermissionsChildItem2.id,
                                                subscriptionId: subscriptionChildItem2.subscriptionId,
                                                infoStatus: Enums.InfoStatus.Add
                                            });
                                        }
                                    }
                                });
                            });
                        }
                    });
                });
                break;
        }
        var model = {
            id: this.groupId,
            name: this.name(),
            description: this.description(),
            groupPermissions: groupPermissions
        };

        return model;
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);
            var model = this.generateModel();

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestGroup.createGroup(model).done((response) => {
                        this.isBusy(false);

                        this.publishMessage("bo-userGroup-change", response.id);
                        this.close(true);
                    }).fail((e)=> {
                        this.isBusy(false);

                        this.handleError(e);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.webRequestGroup.updateGroup(model).done((response) => {
                        this.isBusy(false);

                        this.publishMessage("bo-userGroup-change", this.groupId);
                        this.close(true);
                    }).fail((e)=> {
                        this.isBusy(false);

                        this.handleError(e);
                    });
                    break;
            }
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdDelete") {
            this.showMessageBox(null, this.i18n("M101")(),
                BladeDialog.DIALOG_YESNO).done((button) => {
                switch (button) {
                    case BladeDialog.BUTTON_YES:
                         this.isBusy(true);
                            this.webRequestGroup.deleteGroup(this.groupId).done(() => {
                                this.isBusy(false);

                                this.publishMessage("bo-userGroup-change", this.groupId);
                                this.close(true);
                            }).fail((e)=> {
                                this.isBusy(false);

                                this.handleError(e);
                            });
                            break;
                }
            });
        }
    }
    /**
     * @param {any} groupPermissions
     * @param {any} permissions
     * @returns
     */
    mergeGroupPermissions(groupPermissions, permissions) {
        var permissionList = permissions;
        if (groupPermissions) {
            groupPermissions.forEach((g) => {
                var groupPermissionId = g.id;
                var permissionId = g.permissionId;
                var subscriptionId = g.subscriptionId;
                permissionList.forEach((p) => {
    
                    if (p.id === permissionId) {
                        p.subscriptions.forEach((s) => {
                            if (s.subscriptionId === subscriptionId) {
                                s.id = groupPermissionId;
                                s.selected.poke(true);
                            }
                        });
                    } else {
                        p.children.forEach((pchild) => {
                            if (pchild.id === permissionId) {
                                pchild.subscriptions.forEach((sChild) => {
                                    if (sChild.subscriptionId === subscriptionId) {
                                        sChild.id = groupPermissionId;
                                        sChild.selected.poke(true);
                                    }
                                });
                            }
                            if(pchild.children.length > 0){
                                pchild.children.forEach((pchild2) => {
                                    if (pchild2.id === permissionId) {
                                        pchild2.subscriptions.forEach((sChild2) => {
                                            if (sChild2.subscriptionId === subscriptionId) {
                                                sChild2.id = groupPermissionId;
                                                sChild2.selected.poke(true);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            });
        }
        
        return permissionList;
    }
    /**
     * @param {any} list
     * @param {any} category
     * @returns
     *
     */
    permissionMapItem(list, category) {
        var permissionList = list;
        permissionList = _.filter(permissionList, (item) => {
            return item.category === category;
        });
        return permissionList;
    }
}

class PermissionConfig {
    constructor(id, name, subscriptions = [], parent = null) {
        this.id = id;
        this.name = name;
        this.subscriptions = subscriptions;
        this.children = [];
        this.category = null; //This will be injected outside constructor
        
        this.parent = function(){}; // to prevent ko from serialization
        this.parent.instance = parent;
    }
}

class SubscriptionConfig {
    constructor(subscriptionId, subscriptionName, selected, permission) {
        this.subscriptionId = subscriptionId;
        this.subscriptionName = subscriptionName;

        this.permission = function(){}; // to prevent ko from serialization
        this.permission.instance = permission;

        this.selected = ko.observable(selected);
        var self = this;
        this.selected_change = this.selected.ignorePokeSubscribe((newValue) => {
           //If checked
            if (newValue) {
                if(this.permission.instance){
                    //This is a child node
                    if (this.permission.instance.parent && this.permission.instance.parent.instance) {
                        //Auto check a parent node.
                        var parentPermission = this.permission.instance.parent.instance;  
                        
                        parentPermission.subscriptions.forEach((s, i) => {
                            if (s.subscriptionId == this.subscriptionId) {
                                s.selected.poke(true);
                            }
                            //children2
                            if (s.permission.instance.parent && s.permission.instance.parent.instance) {
                                //Auto check a parent node.
                                var parentPermission2 = s.permission.instance.parent.instance;  
                                
                                parentPermission2.subscriptions.forEach((s2, i) => {
                                    if (s2.subscriptionId == this.subscriptionId) {
                                        s2.selected.poke(true);
                                    }
                                });
                            }
                        });

                        //click children lv1 and then checl box all children lv2
                        if(this.permission.instance.children.length > 0){
                            this.permission.instance.children.forEach((childPermission, i) => {
                                childPermission.subscriptions.forEach((s) => {
                                    if (s.subscriptionId == this.subscriptionId) {
                                        s.selected.poke(true);
                                    }
                                });
                            });
                        }
                    }
                    //This is a parent node
                    else if(this.permission.instance.children.length > 0){
                        //Auto check all children node.
                        this.permission.instance.children.forEach((childPermission, i) => {
                            childPermission.subscriptions.forEach((s) => {
                                if (s.subscriptionId == this.subscriptionId) {
                                    s.selected.poke(true);
                                }
                            });

                            if(childPermission.children.length > 0){
                                //Auto check all children2 node.
                                childPermission.children.forEach((childPermission2, i) => {
                                    childPermission2.subscriptions.forEach((s2) => {
                                        if (s2.subscriptionId == this.subscriptionId) {
                                            s2.selected.poke(true);
                                        }
                                    });
                                });
                            }

                        });
                    }
                }
            } 
            //If not checked
            else {
                if(this.permission.instance){
                    //This is a parent node
                    if (this.permission.instance.children.length > 0) {
                        var childPermission = this.permission.instance.children;
                        childPermission.forEach((c) => {
                            c.subscriptions.forEach((s) => {
                                if (s.subscriptionId == this.subscriptionId) {
                                    s.selected.poke(false);
                                }
                            });

                            //children2
                            if(c.children.length > 0){
                                c.children.forEach((c2) => {
                                    c2.subscriptions.forEach((s2) => {
                                        if (s2.subscriptionId == this.subscriptionId) {
                                            s2.selected.poke(false);
                                        }
                                    });
                                });
                            }
                            
                        });
                    }
                }
            }
        });
    }
}

export default {
    viewModel: ScreenBase.createFactory(GroupManageScreen),
    template: templateMarkup
};
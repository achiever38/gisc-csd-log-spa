﻿import ko from "knockout";
import templateMarkup from "text!./setting-manage.html";
import ScreenBase from "../../screenbase";
import ScreenHelper from "../../screenhelper";
import * as Screen from "../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import WebRequestCompany from "../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestDatabaseConnection from "../../../../app/frameworks/data/apitrackingcore/webRequestDatabaseConnection";
import {Constants, Enums} from "../../../../app/frameworks/constant/apiConstant";
import DefaultSorting from "../../../../app/frameworks/constant/defaultSorting";

/**
 * 
 * 
 * @class CompanySettingManageScreen
 * @extends {ScreenBase}
 */
class CompanySettingManageScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Companies_CompanySettings")());
        this.mode = this.ensureNonObservable(params.mode);
        this.bladeSize = BladeSize.Medium;

        var settings = this.ensureNonObservable($.extend(true, {}, params.settings));
        this.settings = {
            id: this.ensureNonObservable(settings.id, 0),
            companyAdminId: this.ensureNonObservable(settings.companyAdminId, 0),
            companyAdminUsername: this.ensureNonObservable(settings.companyAdminUsername),
            companyAdminEmail: this.ensureNonObservable(settings.companyAdminEmail),
            adUrl: this.ensureNonObservable(settings.adUrl),
            adUsername: this.ensureNonObservable(settings.adUsername),
            adPassword: this.ensureNonObservable(settings.adPassword),
            applyPasswordRules: this.ensureNonObservable(settings.applyPasswordRules),
            gatewayServerPort: this.ensureNonObservable(settings.gatewayServerPort),
            databaseConnectionId: this.ensureNonObservable(settings.databaseConnectionId)
        };

        this.id = this.ensureNonObservable(this.settings.id);
        // Company Administrator
        this.companyAdminId = this.ensureNonObservable(this.settings.companyAdminId);
        this.companyAdminUsername = ko.observable(this.ensureNonObservable(this.settings.companyAdminUsername, ""));
        this.companyAdminEmail = ko.observable(this.ensureNonObservable(this.settings.companyAdminEmail, ""));
        // Active Directory Settings
        this.adUrl = ko.observable(this.ensureNonObservable(this.settings.adUrl, ""));
        this.adUsername = ko.observable(this.ensureNonObservable(this.settings.adUsername, ""));
        this.adPassword = ko.observable(this.ensureNonObservable(this.settings.adPassword, ""));
        // Common
        this.applyPasswordRules = ko.observable(this.ensureNonObservable(this.settings.applyPasswordRules, true));
        this.gatewayServerPort = ko.observable(this.ensureNonObservable(this.settings.gatewayServerPort, ""));

        //trackingDB
        this.trackingDB = ko.observableArray([]);
        this.trackingDBSelected = ko.observable();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var dfd = $.Deferred();

        var d1 = this.webRequestDatabaseConnection.listDBConnection({ sortingColumns: DefaultSorting.TrackingDatabase });
    
        $.when(d1).done((r1) => {

            var trackBD = this.modifyTrackingDB(r1["items"]);
            this.trackingDB(trackBD);
            
            //Set selected item for trackingDB
            var selectedObj = ScreenHelper.findOptionByProperty(this.trackingDB, "id", this.settings.databaseConnectionId);
            this.trackingDBSelected(selectedObj); 

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;

    }

    /**
     * Get WebRequest for Company in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    
    /**
     * Get WebRequest for DatabaseConnection in Web API access.
     * @readonly
     * @memberOf CompanySettingManageScreen
     */
    get webRequestDatabaseConnection() {
        return WebRequestDatabaseConnection.getInstance();
    }

    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        this.companyAdminUsername.extend({ trackChange: true });
        this.companyAdminEmail.extend({ trackChange: true });
        this.adUrl.extend({ trackChange: true });
        this.adUsername.extend({ trackChange: true });
        this.adPassword.extend({ trackChange: true });
        this.applyPasswordRules.extend({ trackChange: true });
        this.gatewayServerPort.extend({ trackChange: true });
        this.trackingDBSelected.extend({ trackChange: true });

        // Validation
        this.companyAdminUsername.extend({ 
            required: true,
            serverValidate: {
                params: "Username",
                message: this.i18n("M010")()
            }
        });
        this.companyAdminEmail.extend({ required: true, email: true });
        // required if AdUrl is not null or empty
        this.adUsername.extend({
            required: {
                onlyIf: () => {
                    return !_.isNil(this.adUrl()) && !_.isEmpty(this.adUrl());
                }
            }
        });
        // required if AdUrl is not null or empty
        this.adPassword.extend({
            required: {
                onlyIf: () => {
                    return !_.isNil(this.adUrl()) && !_.isEmpty(this.adUrl());
                }
            }
        });
        this.gatewayServerPort.extend({ 
            required: true,
            serverValidate: {
                params: "GatewayServerPort",
                message: this.i18n("M065")()
            }
        });

        this.validationModel = ko.validatedObservable({
            companyAdminUsername: this.companyAdminUsername,
            companyAdminEmail: this.companyAdminEmail,
            adUsername: this.adUsername,
            adPassword: this.adPassword,
            gatewayServerPort: this.gatewayServerPort
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actOK", this.i18n("Common_OK")()));
        actions.push(this.createAction("actCancel", this.i18n("Common_Cancel")(), "cancel"));
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actOK") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);
            var databaseConnectionId = this.trackingDBSelected() ? this.trackingDBSelected().id : null;
            var companySetting = {
                companyAdminId: this.companyAdminId,
                companyAdminUsername: this.companyAdminUsername(),
                companyAdminEmail: this.companyAdminEmail(),
                adUrl: this.adUrl(),
                adUsername: this.adUsername(),
                adPassword: this.adPassword(),
                applyPasswordRules: this.applyPasswordRules(),
                gatewayServerPort: this.gatewayServerPort(),
                databaseConnectionId: databaseConnectionId,
                infoStatus: this.mode === "company-create" ? Enums.InfoStatus.Add : Enums.InfoStatus.Update,
                id: this.id
            };

            this.webRequestCompany.validateCompanySetting(companySetting).done(() => {
                this.publishMessage("bo-company-setting-changed", companySetting);
                this.isBusy(false);
                this.close(true);
            }).fail((e) => {
                this.isBusy(false);
                this.handleError(e);
            });
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }

    
    /**
     * 
     * @param {any} data
     * @returns
     * 
     * @memberOf CompanySettingManageScreen
     */
    modifyTrackingDB(data){
        var result = [];
        _.each(data, (dt)=> {
            dt.trackingDatabaseName = dt.trackingDatabaseName+" ("+dt.databaseServerName+")";
            result.push(dt);
        });
        return result;
    }
}

export default {
    viewModel: ScreenBase.createFactory(CompanySettingManageScreen),
    template: templateMarkup
};
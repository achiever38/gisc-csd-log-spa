﻿import ko from "knockout";
import ObjectBase from "../../../../app/frameworks/core/objectBase";

class CompanyAddress extends ObjectBase {
    constructor () {
        super();
        this.id = 0;
        this.houseNo = ko.observable("");
        this.street = ko.observable("");
        this.locate = ko.observable("");
        this.country = ko.observable();
        this.province = ko.observable();
        this.city = ko.observable();
        this.town = ko.observable();
        this.postCode = ko.observable("");
        this.latitude = ko.observable("");
        this.longitude = ko.observable("");
    }
}

export {CompanyAddress};

class CompanyRegional extends ObjectBase {
    constructor () {
        super();
        this.id = 0;
        this.languageId = ko.observable();
        this.timezone = ko.observable();
        this.timezoneName = ko.observable("");
        this.currency = ko.observable();
        this.shortDateFormat = ko.observable();
        this.longDateFormat = ko.observable();
        this.shortTimeFormat = ko.observable();
        this.longTimeFormat = ko.observable();
        this.distanceUnit = ko.observable();
        this.areaUnit = ko.observable();
        this.weightUnit = ko.observable();
        this.volumeUnit = ko.observable();
        this.temperatureUnit = ko.observable();
    }
}

export {CompanyRegional};

class CompanySettings extends ObjectBase {
    constructor () {
        super();
        this.id = 0;
        this.companyAdminId = 0;
        this.companyAdminUsername = ko.observable("");
        this.companyAdminEmail = ko.observable("");
        this.adUrl = ko.observable("");
        this.adUsername = ko.observable("");
        this.adPassword = ko.observable("");
        this.applyPasswordRules = ko.observable(true);
        this.gatewayServerPort = ko.observable("");
        this.databaseConnectionId = ko.observable("");
        this.minETADistance = ko.observable("");
        this.themeName = ko.observable("app-theme-gis");
        this.logoSize = ko.observable("logo-layout-s");
        this.keepTicketDuration = ko.observable("");
        this.satelliteBadGps = ko.observable("0");
        this.continueTracking = ko.observable("0");
        this.showPoiCluster = ko.observable(true);
        this.MDVRModel = ko.observable({});
        this.sendGeofence = ko.observable(false);
        this.enableAutomailFTP = ko.observable(false);
        this.automailFTPPath = ko.observable("");
        this.enableOdometerFixing = ko.observable(false);
    }
}

export {CompanySettings};
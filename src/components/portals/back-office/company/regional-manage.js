﻿import ko from "knockout";
import templateMarkup from "text!./regional-manage.html";
import ScreenBase from "../../screenbase";
import ScreenHelper from "../../screenhelper";
import { Constants, Enums } from "../../../../app/frameworks/constant/apiConstant";
import WebRequestLanguage from "../../../../app/frameworks/data/apicore/webrequestLanguage";
import WebRequestTimeZone from "../../../../app/frameworks/data/apicore/webRequestTimeZone";
import WebRequestUnitResource from "../../../../app/frameworks/data/apicore/webRequestUnitResource";
import WebRequestDateTimeFormat from "../../../../app/frameworks/data/apicore/webRequestDateTimeFormat";
import * as Screen from "../../../../app/frameworks/constant/screen";
import WebRequestCompany from "../../../../app/frameworks/data/apicore/webrequestCompany";
import DefaultSorting from "../../../../app/frameworks/constant/defaultSorting";
/**
 * 
 * 
 * @class CompanyRegionalManageScreen
 * @extends {ScreenBase}
 */
class CompanyRegionalManageScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Companies_CompanyRegional")());
        var country = this.ensureNonObservable(params.country);
        var regional = this.ensureNonObservable($.extend(true, {}, params.regional));
        this.regional = {
            id: this.ensureNonObservable(regional.id, 0),
            languageId: this.ensureNonObservable(regional.languageId),
            timezone: this.ensureNonObservable(regional.timezone),
            timezoneName: this.ensureNonObservable(regional.timezoneName),
            currency: this.ensureNonObservable(regional.currency),
            shortDateFormat: this.ensureNonObservable(regional.shortDateFormat),
            longDateFormat: this.ensureNonObservable(regional.longDateFormat),
            shortTimeFormat: this.ensureNonObservable(regional.shortTimeFormat),
            longTimeFormat: this.ensureNonObservable(regional.longTimeFormat),
            distanceUnit: this.ensureNonObservable(regional.distanceUnit),
            areaUnit: this.ensureNonObservable(regional.areaUnit),
            weightUnit: this.ensureNonObservable(regional.weightUnit),
            volumeUnit: this.ensureNonObservable(regional.volumeUnit),
            temperatureUnit: this.ensureNonObservable(regional.temperatureUnit),
        };

        this.id = regional.id;
        this.country = ko.observable(country);

        this.language = ko.observable();
        this.timezone = ko.observable();
        this.currency = ko.observable();
        this.shortDateFormat = ko.observable();
        this.longDateFormat = ko.observable();
        this.shortTimeFormat = ko.observable();
        this.longTimeFormat = ko.observable();
        this.distanceUnit = ko.observable();
        this.areaUnit = ko.observable();
        this.weightUnit = ko.observable();
        this.volumeUnit = ko.observable();
        this.temperatureUnit = ko.observable();

        this.languageOptions = ko.observableArray([]);
        this.timezoneOptions = ko.observableArray([]);
        this.currencyOptions = ko.observableArray([]);
        this.shortDateFormatOptions = ko.observableArray([]);
        this.longDateFormatOptions = ko.observableArray([]);
        this.shortTimeFormatOptions = ko.observableArray([]);
        this.longTimeFormatOptions = ko.observableArray([]);
        this.distanceUnitOptions = ko.observableArray([]);
        this.areaUnitOptions = ko.observableArray([]);
        this.weightUnitOptions = ko.observableArray([]);
        this.volumeUnitOptions = ko.observableArray([]);
        this.temperatureUnitOptions = ko.observableArray([]);

        // Subscribe Message when company settings changed
        this.subscribeMessage("bo-company-manage-country-changed", (country) => {
            this.country(this.ensureNonObservable(country));
        });
    }
    /**
     * 
     * @readonly
     */
    get webRequestLanguage() {
        return WebRequestLanguage.getInstance();
    }
    /**
     * 
     * @readonly
     */
    get webRequestTimeZone() {
        return WebRequestTimeZone.getInstance();
    }
    /**
     * 
     * @readonly
     */
    get webRequestUnitResource() {
        return WebRequestUnitResource.getInstance();
    }
    /**
     * 
     * @readonly
     */
    get webRequestDateTimeFormat() {
        return WebRequestDateTimeFormat.getInstance();
    }

    /**
     * Get WebRequest for Company in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad){
            return;
        }

        var dfd = $.Deferred();

        var d0 = $.Deferred();
        // also include selected languageId for case update to ensure option still remain if subscription is disabled.
        var includeLanguageIds = !_.isNil(this.regional.languageId) ? [this.regional.languageId] : [];
        var d1 = this.webRequestLanguage.listLanguage({ enable: true, includeIds: includeLanguageIds, sortingColumns: DefaultSorting.Language });
        var d2 = this.webRequestTimeZone.getAllTimeZones();
        var d3 = this.webRequestUnitResource.listUnitResource({ types: [
            Enums.ModelData.EnumResourceType.Currency,
            Enums.ModelData.EnumResourceType.DistanceUnit,
            Enums.ModelData.EnumResourceType.AreaUnit,
            Enums.ModelData.EnumResourceType.WeightUnit,
            Enums.ModelData.EnumResourceType.VolumeUnit,
            Enums.ModelData.EnumResourceType.TemperatureUnit
        ], sortingColumns: DefaultSorting.UnitResource });

        $.when(d1, d2, d3).done((r1, r2, r3) => {
            this.languageOptions(r1["items"]);
            this.timezoneOptions(r2);

            ko.utils.arrayForEach(r3["items"], (unitResource) => {
                switch (unitResource.type) {
                    case Enums.ModelData.EnumResourceType.Currency:
                        this.currencyOptions.push(unitResource);
                        break;
                    case Enums.ModelData.EnumResourceType.DistanceUnit:
                        this.distanceUnitOptions.push(unitResource);
                        break;
                    case Enums.ModelData.EnumResourceType.AreaUnit:
                        this.areaUnitOptions.push(unitResource);
                        break;
                    case Enums.ModelData.EnumResourceType.WeightUnit:
                        this.weightUnitOptions.push(unitResource);
                        break;
                    case Enums.ModelData.EnumResourceType.VolumeUnit:
                        this.volumeUnitOptions.push(unitResource);
                        break;
                    case Enums.ModelData.EnumResourceType.TemperatureUnit:
                        this.temperatureUnitOptions.push(unitResource);
                        break;
                }
            });

            //if language has no value and available language does not contains local language for selected country, set default system language
            var languageValue = !_.isNil(this.regional.languageId) ? ScreenHelper.findOptionByProperty(this.languageOptions, "id", this.regional.languageId) : ScreenHelper.findOptionByProperty(this.languageOptions, "code", this.country().localLanguage) || ScreenHelper.findOptionByProperty(this.languageOptions, "isSystem", true);
            var timezoneValue = ScreenHelper.findOptionByProperty(this.timezoneOptions, "id", !_.isNil(this.regional.timezone) ? this.regional.timezone : this.country().localTimeZone);
            var currencyValue = ScreenHelper.findOptionByProperty(this.currencyOptions, "value", !_.isNil(this.regional.currency) ? this.regional.currency : this.country().localCurrency);
            var distanceUnitValue = !_.isNil(this.regional.distanceUnit) ? ScreenHelper.findOptionByProperty(this.distanceUnitOptions, "value", this.regional.distanceUnit) : ScreenHelper.findOptionByProperty(this.distanceUnitOptions, "isDefault", true);
            var areaUnitValue = !_.isNil(this.regional.areaUnit) ? ScreenHelper.findOptionByProperty(this.areaUnitOptions, "value", this.regional.areaUnit) : ScreenHelper.findOptionByProperty(this.areaUnitOptions, "isDefault", true);
            var weightUnitValue = !_.isNil(this.regional.weightUnit) ? ScreenHelper.findOptionByProperty(this.weightUnitOptions, "value", this.regional.weightUnit) : ScreenHelper.findOptionByProperty(this.weightUnitOptions, "isDefault", true);
            var volumeUnitValue = !_.isNil(this.regional.volumeUnit) ? ScreenHelper.findOptionByProperty(this.volumeUnitOptions, "value", this.regional.volumeUnit) : ScreenHelper.findOptionByProperty(this.volumeUnitOptions, "isDefault", true);
            var temperatureUnitValue = !_.isNil(this.regional.temperatureUnit) ? ScreenHelper.findOptionByProperty(this.temperatureUnitOptions, "value", this.regional.temperatureUnit) : ScreenHelper.findOptionByProperty(this.temperatureUnitOptions, "isDefault", true);

            //set value
            this.language(languageValue);
            this.timezone(timezoneValue);
            this.currency(currencyValue);
            this.distanceUnit(distanceUnitValue);
            this.areaUnit(areaUnitValue);
            this.weightUnit(weightUnitValue);
            this.volumeUnit(volumeUnitValue);
            this.temperatureUnit(temperatureUnitValue);

            this.webRequestDateTimeFormat.listDateTimeFormat({ cultureName: this.language().code }).done((response) => {
                var dateTimePatterns = response["dateTimePatterns"];
                this.shortDateFormatOptions(this.formatDateTimeFormatOptionsDisplayName(dateTimePatterns.ShortDate));
                this.longDateFormatOptions(this.formatDateTimeFormatOptionsDisplayName(dateTimePatterns.LongDate));
                this.shortTimeFormatOptions(this.formatDateTimeFormatOptionsDisplayName(dateTimePatterns.ShortTime));
                this.longTimeFormatOptions(this.formatDateTimeFormatOptionsDisplayName(dateTimePatterns.LongTime));

                // if date format has no value, set default to first item
                var shortDateFormatValue = !_.isNil(this.regional.shortDateFormat) ? ScreenHelper.findOptionByProperty(this.shortDateFormatOptions, "value", this.regional.shortDateFormat) : this.shortDateFormatOptions()[0];
                var longDateFormatValue = !_.isNil(this.regional.longDateFormat) ? ScreenHelper.findOptionByProperty(this.longDateFormatOptions, "value", this.regional.longDateFormat) : this.longDateFormatOptions()[0];
                var shortTimeFormatValue = !_.isNil(this.regional.shortTimeFormat) ? ScreenHelper.findOptionByProperty(this.shortTimeFormatOptions, "value", this.regional.shortTimeFormat) : this.shortTimeFormatOptions()[0];
                var longTimeFormatValue = !_.isNil(this.regional.longTimeFormat) ? ScreenHelper.findOptionByProperty(this.longTimeFormatOptions, "value", this.regional.longTimeFormat) : this.longTimeFormatOptions()[0];

                // set value
                this.shortDateFormat(shortDateFormatValue);
                this.longDateFormat(longDateFormatValue);
                this.shortTimeFormat(shortTimeFormatValue);
                this.longTimeFormat(longTimeFormatValue);

                d0.resolve();
            }).fail((e) => {
                d0.reject(e);
            });

        }).fail((e) => {
            d0.reject(e);
        });

        $.when(d0).done(() => {
            this._changeCountrySubscribe = this.country.subscribe((country) => {
                if(country){
                    this.language(ScreenHelper.findOptionByProperty(this.languageOptions, "code", country.localLanguage) || ScreenHelper.findOptionByProperty(this.languageOptions, "isSystem", true));
                    this.timezone(ScreenHelper.findOptionByProperty(this.timezoneOptions, "id", country.localTimeZone));
                    this.currency(ScreenHelper.findOptionByProperty(this.currencyOptions, "value", country.localCurrency));
                }
                else{
                    this.language(null);
                    this.timezone(null);
                    this.currency(null);
                }
            });

            this._changeLanguageSubscribe = this.language.subscribe((language) => {
                if(language){
                    this.isBusy(true);
                    this.webRequestDateTimeFormat.listDateTimeFormat({ cultureName: language.code }).done((response) => {
                        var dateTimePatterns = response["dateTimePatterns"];
                        this.shortDateFormatOptions(this.formatDateTimeFormatOptionsDisplayName(dateTimePatterns.ShortDate));
                        this.longDateFormatOptions(this.formatDateTimeFormatOptionsDisplayName(dateTimePatterns.LongDate));
                        this.shortTimeFormatOptions(this.formatDateTimeFormatOptionsDisplayName(dateTimePatterns.ShortTime));
                        this.longTimeFormatOptions(this.formatDateTimeFormatOptionsDisplayName(dateTimePatterns.LongTime));

                        this.shortDateFormat(this.shortDateFormatOptions()[0]);
                        this.longDateFormat(this.longDateFormatOptions()[0]);
                        this.shortTimeFormat(this.shortTimeFormatOptions()[0]);
                        this.longTimeFormat(this.longTimeFormatOptions()[0]);

                        this.isBusy(false);
                    }).fail((e) => {
                        this.isBusy(false);
                        this.handleError(e);
                    });
                }
                else {
                    this.shortDateFormatOptions([]);
                    this.longDateFormatOptions([]);
                    this.shortTimeFormatOptions([]);
                    this.longTimeFormatOptions([]);
                }
            });          
            dfd.resolve();  
        }).fail((e) => {
            dfd.reject(e);
        })

        return dfd;
    }
    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        this.language.extend({ trackChange: true });
        this.timezone.extend({ trackChange: true });
        this.currency.extend({ trackChange: true });
        this.shortDateFormat.extend({ trackChange: true });
        this.longDateFormat.extend({ trackChange: true });
        this.shortTimeFormat.extend({ trackChange: true });
        this.longTimeFormat.extend({ trackChange: true });
        this.distanceUnit.extend({ trackChange: true });
        this.areaUnit.extend({ trackChange: true });
        this.weightUnit.extend({ trackChange: true });
        this.volumeUnit.extend({ trackChange: true });
        this.temperatureUnit.extend({ trackChange: true });

        // Validation
        this.language.extend({ required: true });
        this.timezone.extend({ required: true });
        this.currency.extend({ required: true });
        this.shortDateFormat.extend({ required: true });
        this.longDateFormat.extend({ required: true });
        this.shortTimeFormat.extend({ required: true });
        this.longTimeFormat.extend({ required: true });
        this.distanceUnit.extend({ required: true });
        this.areaUnit.extend({ required: true });
        this.weightUnit.extend({ required: true });
        this.volumeUnit.extend({ required: true });
        this.temperatureUnit.extend({ required: true });

        this.validationModel = ko.validatedObservable({
            language: this.language,
            timezone: this.timezone,
            currency: this.currency,
            shortDateFormat: this.shortDateFormat,
            longDateFormat: this.longDateFormat,
            shortTimeFormat: this.shortTimeFormat,
            longTimeFormat: this.longTimeFormat,
            distanceUnit: this.distanceUnit,
            areaUnit: this.areaUnit,
            weightUnit: this.weightUnit,
            volumeUnit: this.volumeUnit,
            temperatureUnit: this.temperatureUnit
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actOK", this.i18n("Common_OK")()));
        actions.push(this.createAction("actCancel", this.i18n("Common_Cancel")(), "cancel"));
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actOK") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);

            var companyInfo = {
                address: {
                    countryId: this.country().id
                },
                setting: {
                    languageId: this.language().id,
                    timezone: this.timezone().id,
                    timezoneName: this.timezone().displayName,
                    currency: this.currency().value,
                    shortDateFormat: this.shortDateFormat().value,
                    longDateFormat: this.longDateFormat().value,
                    shortTimeFormat: this.shortTimeFormat().value,
                    longTimeFormat: this.longTimeFormat().value,
                    distanceUnit: this.distanceUnit().value,
                    areaUnit: this.areaUnit().value,
                    weightUnit: this.weightUnit().value,
                    volumeUnit: this.volumeUnit().value,
                    temperatureUnit: this.temperatureUnit().value,
                    infoStatus: this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update,
                    id: this.id
                }
            }

            this.webRequestCompany.validateCompanyRegional(companyInfo).done(() => {
                this.publishMessage("bo-company-regional-changed", companyInfo.setting);
                this.isBusy(false);
                this.close(true);
            }).fail((e) => {
                this.isBusy(false);
                this.handleError(e);
            });
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }

    
    /**
     * Add displayName property to date format dropdownlist
     * @param {any} options
     * @returns
     */
    formatDateTimeFormatOptionsDisplayName (options) {
        ko.utils.arrayForEach(options, (option) => {
            option.displayName = option.value + " (" + option.example + ")"; 
        });

        return options;
    }
}

export default {
    viewModel: ScreenBase.createFactory(CompanyRegionalManageScreen),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Enums, EntityAssociation, Constants } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import webRequestTransporter from "../../../../../app/frameworks/data/apitrackingcore/webRequestTransporter";

class TransporterManagementScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Small;
        this.bladeTitle(this.i18n("Vendor_ViewTransporter")());
        this.transportorId = this.ensureNonObservable(params.transportorId);
    
        this.user =  ko.observable();
        this.code = ko.observable();
        this.name = ko.observable();

        this.subscribeMessage("ca-vendor-transporter-management-changed",(id) => {
            this.webRequestTransporter.getTransporter(this.transportorId).done((response) => {
                this.code(response["code"]);
                this.name(response["name"]);
                this.user(response["username"]);
            }); 
        });
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        this.webRequestTransporter.getTransporter(this.transportorId).done((response) => {
            this.code(response["code"]);
            this.name(response["name"]);
            this.user(response["username"]);
        });
    }

    get webRequestTransporter() {
        return webRequestTransporter.getInstance();
    }

    /**
     * 
     * 
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    setupExtend(){
           
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {        
          actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.UpdateTransporter))
        {
            commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));
            
        }
        if(WebConfig.userSession.hasPermission(Constants.Permission.DeleteTransporter))
        {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
       
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {

        if(WebConfig.userSession.hasPermission(Constants.Permission.UpdateTransporter)) {
            if (sender.id == "cmdUpdate") {
                this.navigate("ca-vendor-management-transporter-manage", {
                    mode: "update",
                    transportorId: this.transportorId,
                }).fail((e)=> {
                    this.handleError(e);
                }).always(()=>{
                    this.isBusy(false);
                });
            }
        }

        if (sender.id == "cmdDelete") {
            if(WebConfig.userSession.hasPermission(Constants.Permission.DeleteTransporter)) {
                this.showMessageBox(null, this.i18n("M185")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.webRequestTransporter.deleteTransporter(this.transportorId).done(() => {
                                this.publishMessage("ca-vendor-transporter-management-changed");
                                this.close(true);
                            }).fail((e)=> {
                                this.handleError(e);
                            }).always(()=>{
                                this.isBusy(false);
                            });
                            break;
                    }
                });
            }
        }
    }
}

export default {
viewModel: ScreenBase.createFactory(TransporterManagementScreen),
    template: templateMarkup
};
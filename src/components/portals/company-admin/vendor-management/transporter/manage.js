﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import webRequestUser from "../../../../../app/frameworks/data/apicore/webRequestUser";
import webRequestTransporter from "../../../../../app/frameworks/data/apitrackingcore/webRequestTransporter";

class TransporterManagementScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Small;
        
        this.mode = this.ensureNonObservable(params.mode);
        this.transportorId = this.ensureNonObservable(params.transportorId);
        if (this.mode === Screen.SCREEN_MODE_CREATE) {
            this.bladeTitle(this.i18n("Vendor_CreateTransporter")());
        } else if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            this.bladeTitle(this.i18n("Vendor_UpdateTransporter")());
        }
        this.selectedUser = ko.observable();
        this.user =  ko.observableArray([]);
        this.code = ko.observable();
        this.name = ko.observable();
        this.inUsed = ko.observable();
        this.checkInUsed = true;

        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            this.checkInUsed = ko.pureComputed(() => {
                if(this.inUsed() == false)
                {
                    return true;
                }
                else
                {
                    return _.isEmpty(params);
                }
            });
        }

    }
    get webRequestUser() {
        return webRequestUser.getInstance();
    }
    get webRequestTransporter() {
        return webRequestTransporter.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        var dfd = $.Deferred();

        var userFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            transporterId: this.transportorId,

        };
        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.webRequestTransporter.userTransporter(userFilter).done((response) => {
                this.user(response.items);
                dfd.resolve();
            });
            break;

            case Screen.SCREEN_MODE_UPDATE:
            var w1 = this.webRequestTransporter.getTransporter(this.transportorId);
            var w2 = this.webRequestTransporter.userTransporter(userFilter);
            $.when(w1,w2).done((r1,r2) => {
                    this.code(r1["code"]);
                    this.name(r1["name"]);
                    this.user(r2["items"]);
                    this.inUsed(r1["inUsed"]);
                    let IsUser = ScreenHelper.findOptionByProperty(this.user,"username",r1.username);
                    this.selectedUser(IsUser);
                    dfd.resolve();
            }).fail((e) => {
                dfd.reject(e);
            });
            break;
        }
        return dfd;
    }

    generateModel() {
        if (this.mode === Screen.SCREEN_MODE_CREATE) {
            var model = {
                name:this.name(),
                code: this.code(),
                userId: this.selectedUser().id,
            };
        } else if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            var model = {
                id: this.transportorId,
                name:this.name(),
                code: this.code(),
                userId: this.selectedUser().id,
            };
        }
        return model;
    }
    /**
     * 
     * 
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    setupExtend(){
        this.name.extend({ trackChange: true });
        this.code.extend({ trackChange: true });
        this.user.extend({ trackChange: true });

        this.name.extend({ required: true});
        this.code.extend({required: true});
        this.selectedUser.extend({required: true});

        this.name.extend({
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });

        this.user.extend({
            required: true,
            serverValidate: {
                params: "Username",
                message: this.i18n("M010")()
            }
        });

        this.code.extend({
            required: true,
            serverValidate: {
                params: "Code",
                message: this.i18n("M010")()
            }
        });

        this.validationModel = ko.validatedObservable({
            selectedUser: this.selectedUser,
            name: this.name,
            code: this.code,
            user: this.user,
        }); 
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {        
        if (this.mode === Screen.SCREEN_MODE_CREATE) {
            actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
            actions.push(this.createActionCancel());
        }
        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
            actions.push(this.createActionCancel());
        }
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
            var model = this.generateModel();
            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.isBusy(true);
                    this.webRequestTransporter.createTransporter(model).done((response) => {
                        this.publishMessage("ca-vendor-transporter-management-changed");
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.isBusy(true);
                    this.webRequestTransporter.updateTransporter(model).done((response) => {
                        this.publishMessage("ca-vendor-transporter-management-changed", this.id);
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });
                    break;
            }
        }
       
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {

    }
}

export default {
viewModel: ScreenBase.createFactory(TransporterManagementScreen),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./menu.html";
import ScreenBase from "../../screenbase";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";

import { Constants, Enums } from "../../../../app/frameworks/constant/apiConstant";

/**
 * Display configuration menu based on user permission.
 * @class ConfigurationMenuScreen
 * @extends {ScreenBase}
 */
class VenderMenuScreen extends ScreenBase {

    /**
     * Creates an instance of ConfigurationMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("VendorManagement_VendorManagementTitle")());
        this.bladeMargin = BladeMargin.Narrow;
        this.bladeSize = BladeSize.Small;

        this.selectedIndex = ko.observable();

        this._selectingRowHandler = (extras) => {
            if (extras) {
                return this.navigate(extras.id);
            }
            return false;
        };
        this.items = ko.observableArray();
       
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {

        if(WebConfig.userSession.hasPermission(Constants.Permission.CreateTransporter) ||
               WebConfig.userSession.hasPermission(Constants.Permission.UpdateTransporter) ||
               WebConfig.userSession.hasPermission(Constants.Permission.DeleteTransporter) )
        {
            this.items.push({
                text : this.i18n('Vendor_TransporterTitle')(),
                id: 'ca-vendor-management-transporter-list'
            });
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.CreateVendor) ||
                WebConfig.userSession.hasPermission(Constants.Permission.UpdateVendor) ||
                WebConfig.userSession.hasPermission(Constants.Permission.DeleteVendor) )
        {
            this.items.push({
                text : this.i18n('Vendor_VendorsTitle')(),
                id: 'ca-vendor-management-vendor-list'
            });
        }

    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedIndex(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}

    // /**
    //  * Navigate to inner screens.
    //  * @param {any} extras
    //  */
    // goto (extras) {
    //     this.navigate(extras.id);
    // }
}

export default {
viewModel: ScreenBase.createFactory(VenderMenuScreen),
    template: templateMarkup
};
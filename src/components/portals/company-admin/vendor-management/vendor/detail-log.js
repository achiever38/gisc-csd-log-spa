﻿import ko from "knockout";
import templateMarkup from "text!./detail-log.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import Utility from "../../../../../app/frameworks/core/utility";
import webRequestVendorTransportConnecter from "../../../../../app/frameworks/data/apitrackingcore/webRequestVendorTransportConnecter";

class DetailLogScreen extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeTitle(this.i18n("Vendor_DetailConnectorLogs")());
        // this.bladeSize = BladeSize.XLarge;
        this.bladeIsMaximized = true;
        this.LogId = this.ensureNonObservable(params.LogId);
        this.apiDataSource = ko.observableArray([]);
        this.messageStatusOption = ko.observableArray([]);
        this.selectedMessageStatus = ko.observable();


        this.selectedMessageStatus.subscribe(function(value) {

            var filter = {
                Id: this.LogId,
                messageStatus: value.id
            };
        
            this.apiDataSource({
                read: this.webRequestVendorTransportConnecter.listVendorDetailDataGrid(filter),
                update: this.webRequestVendorTransportConnecter.listVendorDetailDataGrid(filter)
            });
        }, this);

    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var filter = {
            Id: this.LogId,
            //set default is all
            messageStatus: 0
        };
    
        this.apiDataSource({
            read: this.webRequestVendorTransportConnecter.listVendorDetailDataGrid(filter),
            update: this.webRequestVendorTransportConnecter.listVendorDetailDataGrid(filter)
        });

        let messageStatus = [
            {
                id: 0,
                name: "All",
                displayName: "All"
            },
            {
                id: 1,
                name: "Success",
                displayName: "Success"
            },
            {
                id: 2,
                name: "Error",
                displayName: "Error"
            }
        ]
        this.messageStatusOption(messageStatus);
       
    }
    get webRequestVendorTransportConnecter() {
        return webRequestVendorTransportConnecter.getInstance();
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {

    }

    onUnLoad(){

    }
    
}

export default {
viewModel: ScreenBase.createFactory(DetailLogScreen),
    template: templateMarkup
};
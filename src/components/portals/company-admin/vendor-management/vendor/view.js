﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import WebRequestVendorTransport from "../../../../../app/frameworks/data/apitrackingcore/webRequestVendorTransport";
import WebRequestTransporter from "../../../../../app/frameworks/data/apitrackingcore/webRequestTransporter";
import {Constants} from "../../../../../app/frameworks/constant/apiConstant";


class VendorsListScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties
        this.bladeTitle(this.i18n("Vendor_View")());
        this.id = this.ensureNonObservable(params.id, -1);
        this.bladeSize = BladeSize.Small;
        this.vname = ko.observable();
        this.ip = ko.observable();
        this.accessTran = ko.observableArray([]);
        this.accessBU = ko.observableArray([]);
        this.order = ko.observable([[ 1, "asc" ]]);
        this.itemAccessT = ko.observableArray();
        this.itemWebRequest = ko.observableArray();

    }
    
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    get webRequestVendorTransport() {
        return WebRequestVendorTransport.getInstance();
    }

    get webRequestTransporter(){
        return WebRequestTransporter.getInstance();
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();
        
        this.webRequestVendorTransport.getVendorTransport(this.id).done((response)=>{
            this.itemWebRequest(response); // use bind data to manage page
            this.vname(response.name);
            this.ip(response.ipAddress);

            const addKeyBu = response.businessUnitNames.map(bu => ({bu})); // add key to BusinessName is key = bu
            const addKeyTran = response.transporterNames.map(transport => ({transport})); // add key to BusinessName is key = transport
            this.accessBU(addKeyBu);
            this.accessTran(addKeyTran);

             dfd.resolve(); 
        }).fail((e)=>{
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
   
   
    buildActionBar(actions) {
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.UpdateVendor))
        {
            commands.push(this.createCommand("cmdUpdate", this.i18n("Vendor_Update")(), "svg-cmd-edit"));
            commands.push(this.createCommand("cmdUpdateVehicle", this.i18n("Assets_UpdateVehicle")(), "svg-cmd-edit"));
        }
        
        
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdUpdate") {
            this.navigate("ca-vendor-management-vendor-manage",
                {
                    mode:"update",
                    item: this.itemWebRequest()
                });
        }
    }
    
    /**
     * 
     * Generate BoxFilterInfo for WebRequest
     * @returns
     */
   
}

export default {
viewModel: ScreenBase.createFactory(VendorsListScreen),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebRequestLanguage from "../../../../../app/frameworks/data/apicore/webRequestLanguage";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestStringResource from "../../../../../app/frameworks/data/apicore/webRequestStringResource";
import {Enums, Constants} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import DefaultSoring from "../../../../../app/frameworks/constant/defaultSorting";

class TranslationListScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Configurations_Translation")());
        this.bladeSize = BladeSize.Medium;

        this.selectedLanguage = ko.observable();
        this.selectedCategory = ko.observable();

        this.languages = ko.observableArray([]);
        this.categories = ko.observableArray([]);

        this.filterText = ko.observable('');
        this.showDataTable = ko.observable(false);
        this.stringResources = ko.observableArray([]);
        this.order = ko.observable([[ 0, "asc" ]]);

        this.pageFilter = null;

        this.subscribeMessage("ca-language-translation-imported", (imported) => {
            if(imported && this.pageFilter){
               if(imported.cultureCode === this.pageFilter.cultureCode && imported.resourceType === this.pageFilter.resourceTypes[0]){
                    this.isBusy(true);
                    this.webRequestStringResource.listStringResourceSummary(this.pageFilter).done((r)=> {
                        this.stringResources.replaceAll(r.items);
                        this.isBusy(false);
                    }).fail((e)=> {
                        this.handleError(e);
                        this.isBusy(false);
                    });
               }      
            } 
        });

    }
    /**
     * Get WebRequest specific for Language module in Web API access.
     * @readonly
     */
    get webRequestLanguage() {
        return WebRequestLanguage.getInstance();
    }
    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }
    /**
     * Get WebRequest specific for String Resource module in Web API access.
     * @readonly
     */
    get webRequestStringResource() {
        return WebRequestStringResource.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();
        var languageFilter = {
            enable: true,
            sortingColumns: DefaultSoring.Language
        };
        var enumResourceFilter = {
            types: [Enums.ModelData.EnumResourceType.ResourceType],
            values: [Enums.ModelData.ResourceType.JobStatus, Enums.ModelData.ResourceType.JobWaypointStatus],
            sortingColumns: DefaultSoring.EnumResource
        };

        var d1 = this.webRequestLanguage.listLanguage(languageFilter);
        var d2 = this.webRequestEnumResource.listEnumResource(enumResourceFilter);

        $.when(d1, d2).done((r1, r2) => {
            this.languages(r1["items"]);
            this.categories(r2["items"]);

            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        // Manual setup validation rules
        this.selectedLanguage.extend({
            required: true
        });
        this.selectedCategory.extend({
            required: true
        });
        this.validationModel = ko.validatedObservable({
            selectedLanguage: this.selectedLanguage,
            selectedCategory: this.selectedCategory,
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.TranslationManagement)) {
             commands.push(this.createCommand("cmdImport", this.i18n("Common_Import")(), "svg-cmd-import"));
        }
        commands.push(this.createCommand("cmdExport", this.i18n("Common_Export")(), "svg-cmd-export"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if(sender.id == "cmdImport") {
            this.navigate("ca-configuration-translation-import");
        }
        else if(sender.id == "cmdExport") {
            this.navigate("ca-configuration-translation-export");
        }
    }
    /**
     * FindButtonClick
     * 
     * @memberOf TranslationListScreen
     */
    onFindButtonClick() {
        if (!this.validationModel.isValid()) {
            this.showDataTable(false);
            this.validationModel.errors.showAllMessages();

            this.pageFilter = null;
        }else {
            this.isBusy(true);

            this.pageFilter = {
                companyId: WebConfig.userSession.currentCompanyId,
                cultureCode: this.selectedLanguage().code,
                resourceTypes: [this.selectedCategory().value]
            };
            this.webRequestStringResource.listStringResourceSummary(this.pageFilter).done((r)=> {
                this.order([[ 0, "asc" ]]);
                this.dispatchEvent("dtTranslationList", "reset", r["items"]);
                this.showDataTable(true);
                this.isBusy(false);
            }).fail((e)=> {
                this.isBusy(false);
                this.handleError(e);
            });
        }      
    }
}

export default {
    viewModel: ScreenBase.createFactory(TranslationListScreen),
    template: templateMarkup
};
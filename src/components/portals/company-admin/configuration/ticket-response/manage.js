﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebRequestTickerResponse from "../../../../../app/frameworks/data/apitrackingcore/webRequestTickerResponse";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import {
    Constants
} from "../../../../../app/frameworks/constant/apiConstant";

class ConfigurationTicketResponseManageScreen extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.mode = params.mode;
        this.ticketResponseId = ko.observable(null);
        switch(this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("Configurations_TicketResponse_Create")());
                break
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("Configurations_TicketResponse_Update")());
                this.data = ko.observable(params.data);
                break
            default:
                break
        }

        this.bladeSize = BladeSize.Small;
        this.name = ko.observable(null);
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        if(this.mode === Screen.SCREEN_MODE_UPDATE) {
            this.getTicketResponseData();
        }
    }

    getTicketResponseData() {
        this.isBusy(true);
        this.ticketResponseId(this.data()['id']);        
        this.webRequestTickerResponse.ticketResponseViewDetail(this.ticketResponseId()).done((res)=> {
            this.isBusy(false);
            this.name(res['name']);
        }); 
    }

    buildCommandBar(commands) {}

    onCommandClick(sender) {}

    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }

    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            let model = {
                name: this.name()
            };
    
            this.isBusy(true);

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestTickerResponse.createTicketResponse(model).done((res) => {                        
                        this.publishMessage("ca-configuration-ticket-response-refresh-list", {
                            id: res.id
                        });
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(()=> {
                        this.isBusy(false);
                    });
                    break
                case Screen.SCREEN_MODE_UPDATE:
                    model.id = this.data()['id']
                    this.webRequestTickerResponse.updateTicketResponse(model).done((res) => {                        
                        this.close(true);
                        this.publishMessage("ca-configuration-ticket-response-refresh-view");
                        this.publishMessage("ca-configuration-ticket-response-refresh-list", {
                            id: res.id
                        });
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });
                    break
                default:
                    break
            }
        }
    }

    setupExtend() {
        this.name.extend({ trackChange: true });
        this.name.extend({ required: true });
        this.name.extend({
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });

        this.validationModel = ko.validatedObservable({
            name: this.name
        });
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {

    }

    onUnload() {}

    get webRequestTickerResponse() {
        return WebRequestTickerResponse.getInstance();
    }
}



export default {
    viewModel: ScreenBase.createFactory(ConfigurationTicketResponseManageScreen),
    template: templateMarkup
};
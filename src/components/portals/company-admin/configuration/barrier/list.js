﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import { Constants } from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestBarrier from "../../../../../app/frameworks/data/apitrackingcore/webRequestBarrier";

class ConfigurationBarrierListScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Configurations_Barrier")());
        this.bladeSize = BladeSize.Medium;

        this.barrierLst = ko.observableArray([]);
        this.filterText = ko.observable();
        this.selectedBarrierLst = ko.observable();

        this.selectRow = (row)=> {
            
            this.navigate("ca-configuration-barrier-view", { data: row });
        }

        this.subscribeMessage("ca-configuration-barrier-refresh-list", info => {
            this.getBarrierLst();
        });
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        //this.barrierLst().push({
        //    id: 1,
        //    name: 'name1',
        //    vehicleTypeDisplayName: 'vehicleTypeDisplayName1',
        //    formatTime: 'formatTime1',
        //    formatDayofWeek: 'formatDayofWeek1',
        //    formatEnable: 'formatEnable1'
        //},{
        //    id: 2,
        //    name: 'name2',
        //    vehicleTypeDisplayName: 'vehicleTypeDisplayName2',
        //    formatTime: 'formatTime2',
        //    formatDayofWeek: 'formatDayofWeek2',
        //    formatEnable: 'formatEnable2'
        //});

        this.getBarrierLst();        
    }

    getBarrierLst() {
        this.isBusy(true);
        this.webRequestBarrier.list(WebConfig.userSession.currentCompanyId).done((response) => {
            this.isBusy(false);
            this.barrierLst.replaceAll(response['items'])
        });
    }

    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.Barriers_Create)) {
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
    }

    onCommandClick(sender) {
        if (sender.id == "cmdCreate") {
            this.navigate("ca-configuration-barrier-manage", { mode: Screen.SCREEN_MODE_CREATE });
        }
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {

    }

    onUnload() {}

    get webRequestBarrier() {
        return WebRequestBarrier.getInstance();
    }
}



export default {
viewModel: ScreenBase.createFactory(ConfigurationBarrierListScreen),
    template: templateMarkup
};
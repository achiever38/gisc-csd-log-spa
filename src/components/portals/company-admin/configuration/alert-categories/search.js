﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import {Constants} from "../../../../../app/frameworks/constant/apiConstant";

import UIConstants from "../../../../../app/frameworks/constant/uiConstant";

import WebRequestAlertCategories from "../../../../../app/frameworks/data/apitrackingcore/webRequestAlertCategories";

class ConfigurationAlertCategorySearchScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties

        //this.barrierId = ko.observable(params.data.id);
        this.bladeTitle(this.i18n("Common_Search")());
        this.bladeSize = BladeSize.Small;

        this.searchName = ko.observable('');
        this.alertRank = ko.observableArray([]);
        this.selectedAlertRabk = ko.observable('');

        /*Auto Complete */
        this.RequestAlertName = (request, response) => {
            var filter = {
                name: request.term
            }
            console.log("filter", filter);

            this.webRequestAlertCategories.autocompleteAlertCategory(filter).done((data) => {
                response(data);
            });
        }
        /*End Auto Complete */




    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        this.alertRank(UIConstants.AlertCategoies.Rank);

    }


    buildActionBar(actions) {
        actions.push(this.createAction("actSearch", this.i18n("Common_Search")()));
        actions.push(this.createActionCancel());
    }

    buildCommandBar(commands) {
        // if (WebConfig.userSession.hasPermission(Constants.Permission.Barriers_Update)) {
        commands.push(this.createCommand("cmdClear", this.i18n("Common_Clear")(), "svg-cmd-clear"));
        //}

    }

    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSearch") {
            let searchFilter = {
                "name": this.searchName() ? this.searchName() : null,
                "rank": this.selectedAlertRabk() ? this.selectedAlertRabk().value : null
            }
            console.log("searchFilter", searchFilter);
            this.publishMessage("ca-alert-Category-list-search", searchFilter);
        }
    }

    onCommandClick(sender) {
        if (sender.id === "cmdClear") {
            this.searchName(null);
            this.selectedAlertRabk(null);
        }

    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {

    }

    onUnload() {}

    get webRequestAlertCategories() {
        return WebRequestAlertCategories.getInstance();
    }

}



export default {
    viewModel: ScreenBase.createFactory(ConfigurationAlertCategorySearchScreen),
    template: templateMarkup
};
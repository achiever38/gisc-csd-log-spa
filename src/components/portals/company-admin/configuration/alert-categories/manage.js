﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";

import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import {
    Constants
} from "../../../../../app/frameworks/constant/apiConstant";

import WebRequestAlertCategories from "../../../../../app/frameworks/data/apitrackingcore/webRequestAlertCategories";

import UIConstants from "../../../../../app/frameworks/constant/uiConstant";


class ConfigurationAlertCategoryManageListScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties

        this.mode = this.ensureNonObservable(params.mode);
        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("Configurations_Categories_Alert_Create")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.alertUpdate = ko.observable(params.data);
                this.bladeTitle(this.i18n("Configurations_Categories_Alert_Update")());
                break;
        }

        this.bladeSize = BladeSize.Small;



        this.nameMax50 = ko.observable('');
        this.description = ko.observable('');
        this.selectedRank = ko.observable('');

        this.rank = ko.observableArray([]);




    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        this.rank(UIConstants.AlertCategoies.Rank);



        if (Screen.SCREEN_MODE_UPDATE === this.mode) {
            this.isBusy(true);
            var dfd = $.Deferred();


            var dfdAlertCategory = this.webRequestAlertCategories.getAlertCategory(this.alertUpdate().id);

            $.when(dfdAlertCategory).done((resourceAlertCategory) => {

                this.alertUpdate().rank = this.rank().filter((res) => {
                    return res.value == resourceAlertCategory.rank
                })

                this.nameMax50(resourceAlertCategory.name);
                this.description(resourceAlertCategory.description);
                this.selectedRank(this.alertUpdate().rank[0]);

                dfd.resolve();

            }).fail((e) => {
                dfd.reject(e);
            }).always(() => {
                this.isBusy(false);
            });

            return dfd;
        }

    }


    buildCommandBar(commands) {}

    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }

    onCommandClick(sender) {}

    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();

                return;
            }

            this.isBusy(true);
            let model = {
                name: this.nameMax50(),
                rank: this.selectedRank().value,
                description: this.description(),
            };
            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestAlertCategories.createAlertCategory(model).done((response) => {
                        let responseId = response?response.id:null;
                        this.publishMessage("ca-alert-Category-list", { id: responseId });
                        this.isBusy(false);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);
                        this.handleError(e);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:

                    model.id = this.alertUpdate().id

                    this.webRequestAlertCategories.updateAlertCategory(model).done((response) => {
                        this.publishMessage("ca-alert-Category-view-refresh");
                        this.publishMessage("ca-alert-Category-list", { id: response.id });
                        
                        this.isBusy(false);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);
                        this.handleError(e);
                    });
                    break;
            }
        }
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {

    }

    setupExtend() {
        this.nameMax50.extend({
            trackChange: true
        });
        this.selectedRank.extend({
            trackChange: true
        });

        this.nameMax50.extend({
            required: true
        });
        this.selectedRank.extend({
            required: true
        });


        this.validationModel = ko.validatedObservable({
            nameMax50: this.nameMax50,
            selectedRank: this.selectedRank,
        });

        this.nameMax50.extend({
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });


    }

    onUnload() {}

    get webRequestAlertCategories() {
        return WebRequestAlertCategories.getInstance();
    }

}



export default {
    viewModel: ScreenBase.createFactory(ConfigurationAlertCategoryManageListScreen),
    template: templateMarkup
};
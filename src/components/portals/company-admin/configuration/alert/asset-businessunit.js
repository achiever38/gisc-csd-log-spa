﻿import ko from "knockout";
import templateMarkup from "text!./asset-businessunit.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenbase";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";

class AlertConfigurationAssetBusinessunitScreen extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeTitle(this.i18n("Menu_BusinessUnits")());
        this.businessUnitList = ko.observableArray([]);
        // this.businessUnit = this.ensureObservable(params.businessUnitIds);
        this.businessUnit = ko.observable();
        this.filter = this.ensureNonObservable(params);

        
    }
    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;
        var dfd = $.Deferred();
        let BUFilter = { 
            companyId: WebConfig.userSession.currentCompanyId, 
            includeAssociationNames: [ EntityAssociation.BusinessUnit.ChildBusinessUnits ], 
            sortingColumns: DefaultSorting.BusinessUnit 
        };
        var d1 = this.webRequestBusinessUnit.listBusinessUnitSummary(BUFilter);
        $.when(d1).done((r1) => {
            this.businessUnitList(r1["items"]);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
     /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        this.businessUnit.extend({
            required: true
        });

        this.validationModel = ko.validatedObservable({
            businessUnit: this.businessUnit
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actOK", this.i18n("Common_OK")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if(sender.id === "actOK") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();

                return;
            }
            let buId = _.filter(this.filter.businessUnitIds, (id)=>{
                return id;
            })
            let ids = _.union(buId, this.businessUnit());
            this.publishMessage("ca-alert-asset-businessunit", { businessUnitIds: ids });
            this.close(true);
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }

}

export default {
    viewModel: ScreenBase.createFactory(AlertConfigurationAssetBusinessunitScreen),
    template: templateMarkup
};
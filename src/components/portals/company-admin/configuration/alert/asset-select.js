﻿import ko from "knockout";
import templateMarkup from "text!./asset-select.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenbase";
import WebRequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicle";
import WebRequestUser from "../../../../../app/frameworks/data/apicore/webRequestUser";
import {Enums} from "../../../../../app/frameworks/constant/apiConstant";
import {VehicleFilter, UserFilter} from "./infos";

class AlertConfigurationAssetSelectScreen extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeTitle(this.i18n("Common_Assets")());

        var alertTypeParam = this.ensureNonObservable(params.alertType);
        var featureIdParam = this.ensureNonObservable(params.featureId, null);

        this.alertType = ko.observable(alertTypeParam);
        this.featureId = ko.observable(featureIdParam);

        this.vehicles = ko.observableArray([]);
        this.users = ko.observableArray([]);
        this.orderVehicles = ko.observable([[ 1, "asc" ]]);
        this.orderUsers = ko.observable([[ 1, "asc" ]]);

        this.selectedVehicleIds = params.selectedVehicleIds;
        this.selectedDeliveryMenIds = params.selectedDeliveryMenIds;
        
        this.selectedVehicles = ko.observableArray();
        this.selectedDeliveryMen = ko.observableArray();

        this.visibleVehicle = ko.pureComputed(() => {
           return this.alertType() && _.includes(this.alertType().levels, Enums.ModelData.AlertLevel.Vehicle.toString());
        });
        this.visibleDeliveryMen = ko.pureComputed(() => {
           return this.alertType() && _.includes(this.alertType().levels, Enums.ModelData.AlertLevel.Dispatcher.toString());
        });

        this.forceCloseBlade = ko.pureComputed(() => {
            var result = false;
            var selectedAlertType = this.alertType();
            if(selectedAlertType){
                if(selectedAlertType.value === Enums.ModelData.AlertType.CustomAlert){
                    result = true;
                }else{
                    //Asset (Vehicle), Asset (Delivery Man), Asset (Vehicle, Delivery Man)   
                    if(_.includes(selectedAlertType.levels, Enums.ModelData.AlertLevel.Vehicle.toString()) 
                        || _.includes(selectedAlertType.levels, Enums.ModelData.AlertLevel.Dispatcher.toString())) 
                    {
                        result = false;
                    }else  
                    {
                        result = true;
                    } 
                }
            }

            return result;
        });

        this.subscribeMessage("ca-configuration-manage-alert-changed", (alertTypeValue) => {
           this.selectedVehicles([]);
           this.selectedDeliveryMen([]);

           this.alertType(alertTypeValue);
           if(this.forceCloseBlade()){
                 //force close this blade because alert type dose not valid
                 this.close(true);
            }else if(alertTypeValue != null){
                this.featureId.poke(alertTypeValue.featureId);
                this.updateDatatableDatasource();
            }
        });

        this.subscribeMessage("ca-configuration-manage-custom-alert-featureId-changed", (featureId) => {
            if(featureId == null){
                //force close this blade because alert type dose not valid
                this.close(true);
            }else{
                this.featureId(featureId);
            }
        });

        this._featureIdRef = this.featureId.ignorePokeSubscribe((value)=>{
            this.selectedVehicles([]);
            this.selectedDeliveryMen([]);

            this.updateDatatableDatasource();
        });
    }
    /**
     * Get WebRequest specific for User in Web API access.
     * @readonly
     */
    get webRequestUser() {
        return WebRequestUser.getInstance();
    }
    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebRequestVehicle.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
         if(!isFirstLoad) return;

        var dfd = $.Deferred();
        var a1 = this.visibleVehicle() ? this.webRequestVehicle.listVehicleSummary(new VehicleFilter(this.featureId())) : null;
        var a2 = this.visibleDeliveryMen() ? this.webRequestUser.listUserSummary(new UserFilter()) : null;

        $.when(a1, a2).done((r1, r2) => {
            if(r1){
                this.vehicles(r1.items);
                this.selectedVehicleIds.forEach(function(element) {
                    let vehicle = ko.utils.arrayFirst(this.vehicles(), function(v) {
                        return v.id === element;
                    });
                    if(vehicle) {
                        this.selectedVehicles.push(vehicle);
                    }
                }, this);
            }
            if(r2){
                this.users(r2.items);
                this.selectedDeliveryMenIds.forEach(function(element) {
                    let user = ko.utils.arrayFirst(this.users(), function(u) {
                        return u.id === element;
                    });
                    if(user) {
                        this.selectedDeliveryMen.push(user);
                    }
                }, this);
            }

            dfd.resolve(); 
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
     /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        this.selectedVehicles.extend({
           trackArrayChange: true
        });
        this.selectedDeliveryMen.extend({
           trackArrayChange: true
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actOK", this.i18n("Common_OK")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if(sender.id === "actOK") {
            var selectedVehicleIds = [];
            var selectedDeliveryMenIds = [];

            this.selectedVehicles().forEach(function(element) {
                selectedVehicleIds.push(element.id);
            }, this);
            this.selectedDeliveryMen().forEach(function(element) {
                selectedDeliveryMenIds.push(element.id);
            }, this);

            this.publishMessage("ca-configuration-alert-assets-selected", { selectedVehicleIds: selectedVehicleIds, selectedDeliveryMenIds: selectedDeliveryMenIds });
            this.close(true);
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }

    updateDatatableDatasource(){
        this.isBusy(true);
        var a1 = this.visibleVehicle() ? this.webRequestVehicle.listVehicleSummary(new VehicleFilter(this.featureId())) : null;
        var a2 = this.visibleDeliveryMen() ? this.webRequestUser.listUserSummary(new UserFilter()) : null;
        $.when(a1, a2).done((r1, r2) => {
            if(r1){
                //this.vehicles.replaceAll(r1.items);
                this.dispatchEvent("dtVehiclesAsset", "refresh", r1.items);
            }
            if(r2){
                //this.users.replaceAll(r2.items);
                this.dispatchEvent("dtUsersAsset", "refresh", r2.items);
            }

            this.isBusy(false);
        }).fail((e) => {
             this.handleError(e);
            this.isBusy(false);
        });
    }
}

export default {
    viewModel: ScreenBase.createFactory(AlertConfigurationAssetSelectScreen),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebrequestAlertConfiguration from "../../../../../app/frameworks/data/apitrackingcore/webRequestAlertConfiguration";
import {Constants} from "../../../../../app/frameworks/constant/apiConstant";
import {AlertConfigurationFilter} from "./infos";

class AlertConfigurationListScreen extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeTitle(this.i18n("Configurations_AlertConfigurations")());
        this.bladeSize = BladeSize.XLarge;
        this.pageFiltering = ko.observable(false);

        this.searchFilter = new AlertConfigurationFilter();
        this.searchFilterDefault = new AlertConfigurationFilter(); 

        this.alertConfigurations = ko.observableArray([]);

        this.filterText = ko.observable('');
        this.selectedAlertConfiguration = ko.observable(null);
        this.recentChangedRowIds = ko.observableArray([]);
        this.order = ko.observable([[ 0, "asc" ]]);

        this._selectingRowHandler = (alertConfiguration) => {
            if(alertConfiguration) {
                return this.navigate("ca-configuration-alert-view", {
                    alertConfigurationId: alertConfiguration.id
                });
            }
            return false;
        };

        this.subscribeMessage("ca-configuration-alert-changed", (alertConfigurationId) => {
            this.isBusy(true);
            this.webrequestAlertConfiguration.listAlertConfigurationSummary(this.searchFilter).done((r) => {
                this.alertConfigurations.replaceAll(r.items);
                if(alertConfigurationId){
                    this.recentChangedRowIds.replaceAll([alertConfigurationId]);
                }else{
                    this.recentChangedRowIds.replaceAll([]);
                }
                this.isBusy(false);
            }).fail((e) => {
                this.handleError(e);
                this.isBusy(false);
            });
        });

        this.subscribeMessage("ca-configuration-alert-deleted", () => {
            this.isBusy(true);
            this.webrequestAlertConfiguration.listAlertConfigurationSummary(this.searchFilter).done((r) => {
                this.alertConfigurations.replaceAll(r.items);
                this.recentChangedRowIds.replaceAll([]);
                this.isBusy(false);
            }).fail((e) => {
                this.handleError(e);
                this.isBusy(false);
            });
        });

        this.subscribeMessage("ca-configuration-alert-search-filter-changed", (searchFilter) => {
            this.isBusy(true);
            this.searchFilter = searchFilter;
            
            // apply active command state if searchFilter does not match default search
            this.pageFiltering(!_.isEqual(this.searchFilter, this.searchFilterDefault));
            
            this.webrequestAlertConfiguration.listAlertConfigurationSummary(this.searchFilter).done((r) => {
                this.alertConfigurations.replaceAll(r.items);
                this.recentChangedRowIds.replaceAll([]);
                this.isBusy(false);
            }).fail((e) => {
                this.handleError(e);
                this.isBusy(false);
            });
        });
    }
    /**
     * Get WebRequest specific for Alert Configuration module in Web API access.
     * @readonly
     */
    get webrequestAlertConfiguration() {
        return WebrequestAlertConfiguration.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();
        this.webrequestAlertConfiguration.listAlertConfigurationSummary(this.searchFilter).done((r)=> {
            this.alertConfigurations(r["items"]);
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.CreateAlertManagement)) {
             commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
        commands.push(this.createCommand("cmdFilter", this.i18n("Common_Search")(), "svg-cmd-search", true, true, this.pageFiltering));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
         if(sender.id === "cmdCreate") {
            this.navigate("ca-configuration-alert-manage", { mode: Screen.SCREEN_MODE_CREATE });
        }
        else if(sender.id == "cmdFilter") {
            this.navigate("ca-configuration-alert-search", {searchFilter: this.searchFilter});
        }
        this.selectedAlertConfiguration(null);
        this.recentChangedRowIds.removeAll();
    }
    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedAlertConfiguration(null);
    }
}

export default {
    viewModel: ScreenBase.createFactory(AlertConfigurationListScreen),
    template: templateMarkup
};
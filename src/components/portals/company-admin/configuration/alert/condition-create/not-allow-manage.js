﻿import ko from "knockout";
import templateMarkup from "text!./not-allow-manage.html";
import * as Screen from "../../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../../screenbase";
import ScreenHelper from "../../../../screenhelper";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
// import WebrequestAlertConfiguration from "../../../../../../app/frameworks/data/apitrackingcore/webRequestAlertConfiguration";
// import WebRequestAlertType from "../../../../../../app/frameworks/data/apitrackingcore/webRequestAlertType";
import WebRequestEnumResource from "../../../../../../app/frameworks/data/apicore/webRequestEnumResource";
// import WebRequestCustomAreaType from "../../../../../../app/frameworks/data/apitrackingcore/webrequestCustomAreaType";
// import WebRequestCustomArea from "../../../../../../app/frameworks/data/apitrackingcore/webrequestCustomArea";
import WebRequestCustomAreaCategory from "../../../../../../app/frameworks/data/apitrackingcore/webrequestCustomAreaCategory";
// import WebRequestCustomPOI from "../../../../../../app/frameworks/data/apitrackingcore/webrequestCustomPOI";
import WebRequestCustomPOICategory from "../../../../../../app/frameworks/data/apitrackingcore/webrequestCustomPOICategory";
// import WebRequestAlertCategory from "../../../../../../app/frameworks/data/apitrackingcore/webRequestAlertCategories";
import DefaultSorting from "../../../../../../app/frameworks/constant/defaultSorting";
// import {AlertTypeFilter, EnumResourceFilter, CustomPOIFilter, CustomPOICategoryFilter, CustomAreaFilter, CustomAreaTypeFilter, CustomAreaTypeCategoryFilter, FeatureFilter} from "../infos";
import { EntityAssociation, Enums, Constants } from "../../../../../../app/frameworks/constant/apiConstant";

class NotAllowManageScreen extends ScreenBase {
    constructor(params) {
        super(params);

         ///////////////////Custom Ko Valiadate For//////////////////////////////
        ko.validation.rules['categoryTypeNotSameValueValidate'] = {
            
            validator: function (val, otherField) {

                let boolean = true;
                if (val && otherField != null) {
                    for (let i = 0; i < otherField.length; i++) {
                        if (val.value == otherField[i].categoryTypeValue) {
                            boolean = false
                            break;
                        } else {
                            boolean = true
                        }
                    }
             
                }
          
                return boolean;
        
                // return val != this.getValue(otherField); //ถ้าค่าที่ใส่มาไม่เท่ากับค่าที่มีอยู่แล้ว คืน true
            },
            message: this.i18n("M010")()
        };
        
        
        ko.validation.rules['categoryNameSameValueValidate'] = {
        
            validator: function (val, otherField) {
               
                let boolean = true;
                if (val && otherField != null) {
                    for (let i = 0; i < val.length; i++) {
        
                        for (let j = 0; j < otherField.length; j++) {
        
                            if(val[i] == otherField[j]){
                                boolean = false;
                                i=val.length;
                            }
                        }
                    }
                }
                return boolean;
        
                // return val != this.getValue(otherField); //ถ้าค่าที่ใส่มาไม่เท่ากับค่าที่มีอยู่แล้ว คืน true
            },
            message: this.i18n("M010")()
        };

        ko.validation.registerExtenders();
        ///////////////////////////////////////////////////////////////////////
        this.notAllowLst = params.notAllowList;
        this.isFirst = false;
        this.paramsData = params;
        this.bladeSize = BladeSize.Medium;
        this.mode = this.ensureNonObservable(params.mode);
        this.editTypeMode = this.ensureNonObservable(params.editTypeMode);
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.selectedSubMenuItem = ko.observable(null);
        
        // switch (this.mode) {
        //     case Screen.SCREEN_MODE_CREATE:
        //        this.bladeTitle(this.i18n("Unathorized_Create_Not_Allow")());
        //         break;
        //     case Screen.SCREEN_MODE_UPDATE:
        //         this.bladeTitle(this.i18n("Unathorized_Update_Not_Allow")());
        //         break;
        // }

        if(this.mode == Screen.SCREEN_MODE_CREATE){

            if(this.editTypeMode == "update"){
                this.bladeTitle(this.i18n("Unathorized_Update_Not_Allow")());
            }else{
                this.bladeTitle(this.i18n("Unathorized_Create_Not_Allow")());
            }

        }else if(this.mode == Screen.SCREEN_MODE_UPDATE){

                if(this.editTypeMode == "create"){
                    this.bladeTitle(this.i18n("Unathorized_Create_Not_Allow")());
                }else{
                    this.bladeTitle(this.i18n("Unathorized_Update_Not_Allow")());
                }
        }

        this.enableType = ko.observable(true);
        this.categoryTypeOptions = ko.observableArray();
        this.conditionOptions = ko.observableArray();
        this.categoryNameOptions = ko.observableArray();
        this.speedTypeOptions = ko.observableArray();

        this.selectedCategoryName = ko.observable();
        this.selectedCategoryType = ko.observable();
        this.selectedCondition = ko.observable();
        this.selectedSpeedType = ko.observable();

        this.valueRadio = ko.observable();
        this.parkingNotOvertime = ko.observable();
        this.conditionType = ko.observable();



        //Condition&CategoryType Subscribe//

        this.selectedCondition.subscribe((conditionSelectedObj) => {
           

            var dfd = $.Deferred();
            let enumCategoryTypeFillter = {}
            switch (this.mode) {

                case Screen.SCREEN_MODE_CREATE:
                    if (conditionSelectedObj) {
                        this.conditionType(conditionSelectedObj.value)
                        if (conditionSelectedObj.value == 1) { //Custom Poi
                            enumCategoryTypeFillter = { types: [Enums.ModelData.EnumResourceType.PoiCategoryType] }
        
                        } else if (conditionSelectedObj.value == 2) { //Custom Area
        
                            enumCategoryTypeFillter = { types: [Enums.ModelData.EnumResourceType.AreaCategoryType] };
                        }
                    } else {
                        enumCategoryTypeFillter = { types: [-1] }
                    }
        
                    var d1 = this.webRequestEnumResource.listEnumResource(enumCategoryTypeFillter);
                    $.when(d1).done((r1) => {
        
                        this.categoryTypeOptions(r1.items)
                
        
                        dfd.resolve();
                    }).fail((e) => {
                        dfd.reject(e);
                    });
                break;
    
                case Screen.SCREEN_MODE_UPDATE:

                        if (conditionSelectedObj) {
                            this.conditionType(conditionSelectedObj.value)
                            // this.selectedCategoryType('')
                            if (conditionSelectedObj.value == 1) { //Custom Poi
                                enumCategoryTypeFillter = { types: [Enums.ModelData.EnumResourceType.PoiCategoryType] }
            
                            } else if (conditionSelectedObj.value == 2) { //Custom Area
            
                                enumCategoryTypeFillter = { types: [Enums.ModelData.EnumResourceType.AreaCategoryType] };
                            }
                        } else {
                            enumCategoryTypeFillter = { types: [-1] }
                        }
                      
                    //    let categoryType = this.getEnumCategoryTypeForUpdate();
                        var d2 = this.webRequestEnumResource.listEnumResource(enumCategoryTypeFillter);
                        $.when(d2).done((r2) => {

                            this.categoryTypeOptions(r2.items)
                            
                            this.selectedCategoryType(ScreenHelper.findOptionByProperty(this.categoryTypeOptions, "value", this.paramsData.categoryTypeValue ) )
                            
                            dfd.resolve();
                        }).fail((e) => {
                            dfd.reject(e);
                        });
                break;
            }
            
            return dfd;
        })
        this.selectedCategoryType.subscribe((categoryTypeSelectedObj) => {

            if (this.isFirst) {
                this.selectedCategoryName([]);
                this.categoryNameOptions([]);
            }

                var dfd = $.Deferred();
                var allObj;
                let categoryName;
            
                var d1;
                if(this.conditionType() == 1){
                    d1 = this.webRequestCustomPOICategory.listCustomPOICategorySummary();
                }else if(this.conditionType() == 2){
                    d1 = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary();
                }

                

                switch (this.mode) {

                    case Screen.SCREEN_MODE_CREATE:

                    if (categoryTypeSelectedObj) {

                        $.when(d1).done((r1) => {
                         

                            if(this.conditionType() == 1){
                                categoryName = r1.items.filter((items) => { //filter เอาเฉพาะ categoryname ของ category type ที่ได้เลือก
                                    return items.poiCategoryType == categoryTypeSelectedObj.value
                                });
                            }else if(this.conditionType() == 2){
                                categoryName = r1.items.filter((items) => { //filter เอาเฉพาะ categoryname ของ category type ที่ได้เลือก
                                    return items.areaCategoryType == categoryTypeSelectedObj.value
                                });
                            }
                            /////////////////////////////////////////
                            if(categoryName.length != 0){
                                let allObj = {}
                                allObj = this.addAll()
                                    categoryName.forEach((items)=>{
                                        items.parent = 0
                                })
                                                
                                categoryName.unshift(allObj);
                                this.categoryNameOptions(categoryName)
                            }else{
                                this.categoryNameOptions([])
                            }
                            ///////////////////////////////////////////

                            this.isFirst = true;
                            dfd.resolve();
                        }).fail((e) => {
                            dfd.reject(e);
                        });
        
                        return dfd;
                    } else {
                        // this.categoryNameOptions([])
                    }
    
                    break;

                    case Screen.SCREEN_MODE_UPDATE:
                    if (categoryTypeSelectedObj) {
                        $.when(d1).done((r1) => {
                            
                            if(this.conditionType() == 1){
                                categoryName = r1.items.filter((items) => { //filter เอาเฉพาะ categoryname ของ category type ที่ได้เลือก
                                    return items.poiCategoryType == categoryTypeSelectedObj.value
                                });
                            }else if(this.conditionType() == 2){
                                categoryName = r1.items.filter((items) => { //filter เอาเฉพาะ categoryname ของ category type ที่ได้เลือก
                                    return items.areaCategoryType == categoryTypeSelectedObj.value
                                });
                            }
                            /////////////////////////////////////////////////////
                                if(categoryName.length != 0){
                                    let allObj = {}
                                    allObj = this.addAll()
                                        categoryName.forEach((items)=>{
                                            items.parent = 0
                                    })
                                                    
                                    categoryName.unshift(allObj);
                                    this.categoryNameOptions(categoryName)
                                }else{
                                    this.categoryNameOptions([])
                                }
                            ////////////////////////////////////////////////////
                            this.isFirst = true;
                            dfd.resolve();
                        }).fail((e) => {
                            dfd.reject(e);
                        });
        
                        return dfd;
                    }
                    break;
 
                }

     
            

        })

        this.maxId = this.getMaxId(params.notAllowList);
        this.categoryIdList = this.combineCategoryId(params.notAllowList);
        this.enumCategoryType = this.getEnumCategoryTypeForUpdate();
       
    }
    addAll(){
        let allObj = {}; //add all
                            allObj.id = 0;
                            allObj.name = this.i18n("Common_CheckAll")();
                            allObj.parent = "#";
        
        
        return allObj
    }
    getEnumCategoryTypeForUpdate(){
        let enumCategoryTypeFillter = {}
        if (this.paramsData.conditionValue) {
            if (this.paramsData.conditionValue == 1) { //Custom Poi
                enumCategoryTypeFillter = { types: [Enums.ModelData.EnumResourceType.PoiCategoryType] }

            } else if (this.paramsData.conditionValue == 2) { //Custom Area

                enumCategoryTypeFillter = { types: [Enums.ModelData.EnumResourceType.AreaCategoryType] };
            }
        } else {
            enumCategoryTypeFillter = { types: [-1] }
        }
        
        return enumCategoryTypeFillter;
    }
    getMaxId(notAllowList) {
        let maxId;
        if (notAllowList) {
            if (notAllowList.length != 0) {
                maxId = Math.max.apply(Math, notAllowList.map(function (item) { return item.dataId; })) //หา maxId จาก object array โดย https://stackoverflow.com/questions/4020796/finding-the-max-value-of-an-attribute-in-an-array-of-objects
            } else {
                maxId = 0;
            }
        }
      
        return maxId;
    }
    combineCategoryId(notAllowList){
        
        let categoryIdArr = []
        if (notAllowList) { 
            if(notAllowList.length!=0 && notAllowList!=undefined){ 
             notAllowList.forEach((items,index)=>{
               
                    items.categoryNameId.forEach((id)=>{
                        categoryIdArr.push(id)
                    })
                })
    
            }
         }
        let categoryIdLst = categoryIdArr.filter((items)=>{
            return items != 0;
        })

        return categoryIdLst;
    }
    /**
     * Get WebRequest specific for Alert Configuration module in Web API access.
     * @readonly
     */
    get webrequestAlertConfiguration() {
        return WebrequestAlertConfiguration.getInstance();
    }
    /**
     * Get WebRequest specific for Alert Type module in Web API access.
     * @readonly
     */
    get webRequestAlertType() {
        return WebRequestAlertType.getInstance();
    }
    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }
    /**
     * Get WebRequest specific for Company module in Web API access.
     * @readonly
     */
  
    /**
     * Get WebRequest specific for Custom Area Type module in Web API access.
     * @readonly
     */
    get webRequestCustomAreaType() {
        return WebRequestCustomAreaType.getInstance();
    }
    /**
     * Get WebRequest specific for Feature module in Web API access.
     * @readonly
     */

    /**
     * Get WebRequest specific for Custom Area module in Web API access.
     * @readonly
     */
    get webRequestCustomArea() {
        return WebRequestCustomArea.getInstance();
    }
    /**
     * Get WebRequest specific for Custom Area Category module in Web API access.
     * @readonly
     */
    get webRequestCustomAreaCategory() {
        return WebRequestCustomAreaCategory.getInstance();
    }
    /**
     * Get WebRequest specific for Custom POI module in Web API access.
     * @readonly
     */
    get webRequestCustomPOI() {
        return WebRequestCustomPOI.getInstance();
    }
    /**
     * Get WebRequest specific for Custom POI Category module in Web API access.
     * @readonly
     */
    get webRequestCustomPOICategory() {
        return WebRequestCustomPOICategory.getInstance();
    }
    /**
     * Get WebRequest specific for Custom POI Category module in Web API access.
     * @readonly
     */
    get webRequestAlertCategory() {
        return WebRequestAlertCategory.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        
        if (!isFirstLoad) return;
        var dfd = $.Deferred();

        var enumConditionFilter = {
            types: [Enums.ModelData.EnumResourceType.AlertUnauthorizedCondition],
            // values: this.getReportTemplateType(),
            sortingColumns: DefaultSorting.EnumResource
        };
        var enumSpeedTypeFilter = {
            types: [Enums.ModelData.EnumResourceType.AlertUnauthorizedSpeedType],
            // values: this.getReportTemplateType(),
            sortingColumns: DefaultSorting.EnumResource
        };
        var d1 = this.webRequestEnumResource.listEnumResource(enumConditionFilter);
        var d4 = this.webRequestEnumResource.listEnumResource(enumSpeedTypeFilter);
        switch (this.mode) {

            case Screen.SCREEN_MODE_CREATE:

                    $.when(d1,d4).done((r1,r4) => {

                        
                        this.conditionOptions(r1.items)
                        this.speedTypeOptions(r4.items)

      
                        dfd.resolve();
                    }).fail((e) => {
                        dfd.reject(e);
                    });
            break;

            case Screen.SCREEN_MODE_UPDATE:
                    
                    var d3 ;
                    if(this.paramsData.condition == 1){
                        d3 = this.webRequestCustomPOICategory.listCustomPOICategorySummary();
                    }else if(this.paramsData.condition == 2){
                        d3 = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary();
                    }

                    $.when(d1,d3,d4).done((r1,r3,r4) => {

                        this.conditionOptions(r1.items)
                        this.selectedCondition(ScreenHelper.findOptionByProperty(this.conditionOptions, "value", this.paramsData.conditionValue ) )
                        
                        this.speedTypeOptions(r4.items)
                        this.selectedSpeedType(ScreenHelper.findOptionByProperty(this.speedTypeOptions, "value", this.paramsData.speedTypeValue ) )
                  

                    //////////////////////////////////////////Category Name Dropdown Update//////////////////////////////////////////////////////////////////////
                        let categoryName;
                        if(this.paramsData.condition == 1){
                            categoryName = r3.items.filter((items) => { //filter เอาเฉพาะ categoryname ของ category type ที่ได้เลือก
                                return items.poiCategoryType == this.paramsData.categoryTypeValue
                            });
                        }else if(this.paramsData.condition == 2){
                            categoryName = r3.items.filter((items) => { //filter เอาเฉพาะ categoryname ของ category type ที่ได้เลือก
                                return items.areaCategoryType == this.paramsData.categoryTypeValue
                            });
                        }
                        
                        var allObj = {};
                        allObj.id = 0;
                        allObj.name = this.i18n("Common_CheckAll")();
                        allObj.parent = "#";
            
                        categoryName.forEach((items)=>{
                            items.parent = 0
                        })
                        categoryName.unshift(allObj);
            
                        this.categoryNameOptions(categoryName)//this.categoryNameOptions(categoryName)
                        this.selectedCategoryName(this.paramsData.categoryNameId)

                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                
                        dfd.resolve();
                    }).fail((e) => {
                        dfd.reject(e);
                    });
                break;
        }


        return dfd;
 
    }
    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        var self = this;
        this.selectedCondition.extend({
            required: true
        });
        this.selectedSpeedType.extend({
            required: true
        })

        this.selectedCategoryType.extend({
            required: true,
            categoryTypeNotSameValueValidate: (this.notAllowLst!=undefined && this.notAllowLst.length != 0) ? this.notAllowLst : null
            
        })
        this.selectedCategoryName.extend({
            required: true,
            categoryNameSameValueValidate:(this.categoryIdList != 0) ? this.categoryIdList : null
        })
      


        this.validationModel = ko.validatedObservable({
            selectedCondition: this.selectedCondition,
            selectedCategoryType: this.selectedCategoryType,
            selectedCategoryName: this.selectedCategoryName,
            selectedSpeedType: this.selectedSpeedType
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createAction("actCancel", this.i18n("Common_Cancel")(), "cancel"));
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * Generate Alert Configuration model from View Model
     * 
     * @returns Alert Configuration model which is ready for ajax request 
     */
    generateModel() {
        
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */

    getCategoryNameList(){
        let categoryNameLst = []
        let selectCategoryNameNotAll =  this.selectedCategoryName().filter((items)=>{

            return items != 0;
        })
   
        this.categoryNameOptions().forEach((items)=>{

            selectCategoryNameNotAll.forEach((id)=>{

                if(items.id == id){
                    categoryNameLst.push(items.name)
                }

            })
            
        })
        return categoryNameLst;
    }
    onActionClick(sender) {
        super.onActionClick(sender);
        var isValid = false;

        if (sender.id === "actSave") {

            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            let categoryNameLst = this.getCategoryNameList();
            let param = {

                dataId: (this.mode == Screen.SCREEN_MODE_CREATE) ? this.maxId + 1 : this.paramsData.notAllowId,
                configType:2,
                condition: this.selectedCondition().value,
                conditionForDisplay: this.selectedCondition().displayName,
                conditionValue: this.selectedCondition().value,
                categoryTypeName: this.selectedCategoryType().displayName,
                categoryTypeValue: this.selectedCategoryType().value,
                categoryNameId: this.selectedCategoryName(),
                categoryNameLst: categoryNameLst,
                speedTypeValue:this.selectedSpeedType().value,
                speedTypeForDisplay:this.selectedSpeedType().displayName,
                speedType:this.selectedSpeedType().value,
                customIds: this.selectedCategoryName()
            }
            
            switch (this.mode) {

                case Screen.SCREEN_MODE_CREATE:

                let tableData = this.notAllowLst
                tableData.push(param)

    
                this.publishMessage("not-allow-manage", tableData);
                this.close(true);
            
                break;
    
                case Screen.SCREEN_MODE_UPDATE:
                let allTableList =this.paramsData.allTableList
                let notAllowUpdate = allTableList.map((item)=>{ //วนหาobjก้อนที่ไอดีเหมือนกันเพื่ออัพเดทค่า obj ก้อนนั้น

                            if(item.dataId == param.dataId){
                                item = param
                            }
                            return item
                        
                        })

                        this.publishMessage("not-allow-manage", notAllowUpdate);
                        this.close(true);

                break;
            }

        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedSubMenuItem(null);
    }

 

}

export default {
    viewModel: ScreenBase.createFactory(NotAllowManageScreen),
    template: templateMarkup
};
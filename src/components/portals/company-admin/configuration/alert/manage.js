﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebrequestAlertConfiguration from "../../../../../app/frameworks/data/apitrackingcore/webRequestAlertConfiguration";
import WebRequestAlertType from "../../../../../app/frameworks/data/apitrackingcore/webRequestAlertType";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestCustomAreaType from "../../../../../app/frameworks/data/apitrackingcore/webrequestCustomAreaType";
import WebRequestFeature from "../../../../../app/frameworks/data/apitrackingcore/webRequestFeature";

import WebRequestCustomArea from "../../../../../app/frameworks/data/apitrackingcore/webrequestCustomArea";
import WebRequestCustomAreaCategory from "../../../../../app/frameworks/data/apitrackingcore/webrequestCustomAreaCategory";
import WebRequestCustomPOI from "../../../../../app/frameworks/data/apitrackingcore/webrequestCustomPOI";
import WebRequestCustomPOICategory from "../../../../../app/frameworks/data/apitrackingcore/webrequestCustomPOICategory";
import WebRequestAlertCategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestAlertCategories";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import {AlertTypeFilter, EnumResourceFilter, CustomPOIFilter, CustomPOICategoryFilter, CustomAreaFilter, CustomAreaTypeFilter, CustomAreaTypeCategoryFilter, FeatureFilter} from "./infos";
import {EntityAssociation, Enums} from "../../../../../app/frameworks/constant/apiConstant";

class AlertConfigurationManageScreen extends ScreenBase {
    constructor(params) {
        super(params);
        ko.validation.rules['allowValidate'] = {
        
            validator: function (val, otherField) {
                // val ค่าที่อยู่ในฟิลด์ปัจจุบัน
                // otherField ค่าที่ส่งเข้ามา
        
                let boolean = true;
                if (val && otherField != null) {

                    for(let i = 0;i<otherField.length;i++){

                        for(let j=0;j<val.length;j++){

                            if(otherField[i].conditionValue == val[j].conditionValue && otherField[i].categoryTypeValue == val[j].categoryTypeValue &&(JSON.stringify(otherField[i].categoryNameId) === JSON.stringify(val[j].categoryNameId))){

                                    boolean = false
                                    break;
                            }

                        }

                    }
                    
                }
                
                return boolean;

            },
            message: this.i18n("M010")()
        };

        ko.validation.registerExtenders();

        
        
        this.bladeSize = BladeSize.Medium;
        this.mode = this.ensureNonObservable(params.mode);
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.temperatureUnit = ko.observable('');
        this.selectedSubMenuItem = ko.observable(null);
       
        this.editType;
        if(this.mode == Screen.SCREEN_MODE_CREATE){
            this.editType = "create"

        }else if(this.mode == Screen.SCREEN_MODE_UPDATE){
            this.editType = "update"
        }

        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
               this.bladeTitle(this.i18n("Configurations_CreateAlertConfiguration")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("Configurations_UpdateAlertConfiguration")());
                break;
        }

        this.allowList = ko.observableArray([]);
        this.notAllowList = ko.observableArray([]);
        this.stopOvertimeList = ko.observableArray([]);
   
        this.accessibleUsers3 = ko.observableArray([]);

        this.order = ko.observable();

        this.alertConfigurationId = this.ensureNonObservable(params.alertConfigurationId, -1);
        this.enableOptions = ScreenHelper.createYesNoObservableArray();
        this.alertTypes = ko.observableArray([]);

        this.enableType = ko.observable(true);
        this.mailTo = ko.observable();
        this.selectedAlertType = ko.observable();
        this.priority = ko.observable('1');
        this.selectedEnable = ko.observable();
        this.enableWebsiteAlert = ko.observable(false);
        this.enableEmailAlert = ko.observable(false);
        this.enableSmsAlert = ko.observable(false);
        this.enableInterfaceAlert = ko.observable(false);
        this.enableDeviceAlert = ko.observable(false);
        this.enableCreateTicket = ko.observable(false);
        this.enableMDVR = ko.observable(false);
        this.enableReportAlert = ko.observable(false);
        this.description = ko.observable('');
        
        this.notificationSound = ko.observableArray([]);
        this.selectedNotificationSound = ko.observable();
        this.iconAlertSound = 'svg-icon-notification-sound';
        this.soundUrl = this.resolveUrl("~/media/notification.mp3");
        this.soundUrl2 = this.resolveUrl("~/media/sharp.mp3");
        this.soundId = this.id+'_audio1';
        this.soundId2 = this.id+'_audio2';
        this.visibleNotificationSound = ko.pureComputed(()=>{
            return this.enableWebsiteAlert() == true;
        })

        this.showM132= ko.observable(false);
        this.showM133= ko.observable(false);

        this.tmpBoxTemplateId = ko.observable();
        //Device
        this.selectedBoxTemplate = ko.observable();
        this.selectedCommandTemplate = ko.observable();
        this.selectedOutput = ko.observable();
        this.command = ko.observable();

        this.boxTemplates = ko.observableArray([]);
        this.commandTemplates = ko.observableArray([]);
        this.outputs = ko.observableArray([]);

        //Alert Category
        this.alertCategory = ko.observableArray([]);
        this.selectedAlertCategory = ko.observable();
        

        //Assests
        this.selectedVehicleIds = ko.observableArray([]);
        this.selectedDeliveryMenIds = ko.observableArray([]);
        this._originalSelectedVehicles = [];
        this._originalSelectedDeliveryMen= [];
        this.selectedBusinessUnitIds = ko.observableArray([0]);
        this.visibleAssests = ko.pureComputed(() => {
            var result = false;
            var selectedAlertType = this.selectedAlertType();
            if(selectedAlertType){
                if(selectedAlertType.value === Enums.ModelData.AlertType.CustomAlert){
                    if(this.selectedFeature()){
                        result = true;
                    }
                }else{
                    //Asset (Vehicle), Asset (Delivery Man), Asset (Vehicle, Delivery Man)   
                    if((_.includes(selectedAlertType.levels, Enums.ModelData.AlertLevel.Vehicle.toString()))
                        ||(_.includes(selectedAlertType.levels, Enums.ModelData.AlertLevel.Dispatcher.toString())))
                    {
                        if(this.visibleFeatureUnit()){
                            if(this.selectedFeatureUnit()){
                                result = true; 
                            }
                        }else{
                            result = true;
                        }
                    }
                }
            }
            return result;
        });

        //DateTime
        this.enableOnMonday = ko.observable(false);
        this.enableOnTuesday = ko.observable(false);
        this.enableOnWednesday = ko.observable(false);
        this.enableOnThursday = ko.observable(false);
        this.enableOnFriday = ko.observable(false);
        this.enableOnSaturday = ko.observable(false);
        this.enableOnSunday = ko.observable(false);
        this.startTime = ko.observable();
        this.endTime = ko.observable();

        //Input MaxSpeed MinSpeed MaxRPM MinRPM
        this.inputMinSpeed = ko.observable(null);
        this.inputMaxSpeed = ko.observable(null);
        this.inputMinRPM = ko.observable(null);
        this.inputMaxRPM = ko.observable(null);

        this.visibleMinSpeed = ko.pureComputed(() => {
            var alertType = this.selectedAlertType() && this.selectedAlertType().value;
            return alertType === Enums.ModelData.AlertType.GreenBandDriving || 
                   alertType === Enums.ModelData.AlertType.OverRPM ||
                   alertType === Enums.ModelData.AlertType.FreeWheeling;
        });

        this.visibleMaxSpeed = ko.pureComputed(() => {
            var alertType = this.selectedAlertType() && this.selectedAlertType().value;
            return  alertType === Enums.ModelData.AlertType.GreenBandDriving || 
                    alertType === Enums.ModelData.AlertType.NoSeatBelt ||
                    alertType === Enums.ModelData.AlertType.FreeWheeling;
        });

        this.visibleMinRPM = ko.pureComputed(() => {
            var alertType = this.selectedAlertType() && this.selectedAlertType().value;
            return alertType === Enums.ModelData.AlertType.GreenBandDriving || 
                   alertType === Enums.ModelData.AlertType.FreeWheeling;
        });

        this.visibleMaxRPM = ko.pureComputed(() => {
            var alertType = this.selectedAlertType() && this.selectedAlertType().value;
            return alertType === Enums.ModelData.AlertType.GreenBandDriving ||
                   alertType === Enums.ModelData.AlertType.OverRPM ;
        });


        //Custom Fields
        this.visibleUnauthorizedStop = ko.pureComputed(()=>{
            var alertType = this.selectedAlertType() && this.selectedAlertType().value;
             return (alertType === Enums.ModelData.AlertType.UnauthorizedStop);
        })
        this.visibleAlertCategory = ko.pureComputed(()=>{
            var alertType = this.selectedAlertType() && this.selectedAlertType().value;
            return (alertType !== Enums.ModelData.AlertType.UnauthorizedStop);
        })
        this.visibleCustomAreaCategories = ko.pureComputed(()=>{
            return this.selectedAlertType() && (this.selectedAlertType().value ===  Enums.ModelData.AlertType.POIParking || 
                                                        this.selectedAlertType().value ===  Enums.ModelData.AlertType.POIInOut || 
                                                        this.selectedAlertType().value ===  Enums.ModelData.AlertType.AreaParking ||
                                                        this.selectedAlertType().value ===  Enums.ModelData.AlertType.AreaInOutVehicle);
        })
        this.visibleDecimal = ko.pureComputed(() => {
             var alertType = this.selectedAlertType() && this.selectedAlertType().value;
             return (alertType === Enums.ModelData.AlertType.AccRapid
                    || alertType === Enums.ModelData.AlertType.DecRapid
                    || alertType === Enums.ModelData.AlertType.OverSpeed
                    || alertType === Enums.ModelData.AlertType.Swerve
                );
        });
        this.labelDecimal = ko.observable('');
        this.inputDecimal = ko.observable('');

        this.visibleInteger = ko.pureComputed(() => {
             var alertType = this.selectedAlertType() && this.selectedAlertType().value;
             return (alertType === Enums.ModelData.AlertType.OverTimePark);
        });
        this.labelInteger = ko.observable('');
        this.inputInteger = ko.observable('');

        this.visibleIntegerMax24 = ko.pureComputed(() => {
             var alertType = this.selectedAlertType() && this.selectedAlertType().value;
             return (alertType === Enums.ModelData.AlertType.ExceedLimitDriving
                    || alertType === Enums.ModelData.AlertType.ExceedLimitDrivingIn24Hours
                    ||  alertType === Enums.ModelData.AlertType.ExceedLimitDrivingVehicle
                );
        });
        this.labelIntegerMax24 = ko.observable('');
        this.inputIntegerMax24 = ko.observable('');

        this.visibleDecimalAllowNegative = ko.pureComputed(() => {
            var alertType = this.selectedAlertType() && this.selectedAlertType().value;
            return (alertType === Enums.ModelData.AlertType.OverTemperature
                    || alertType === Enums.ModelData.AlertType.UnderTemperature
                );
        });
        this.labelDecimalAllowNegative= ko.observable('');
        this.inputDecimalAllowNegative = ko.observable('');

        this.visibleDecimalLessThan100 = ko.pureComputed(() => {
             var alertType = this.selectedAlertType() && this.selectedAlertType().value;
             return (alertType === Enums.ModelData.AlertType.VehicleBatteryLow
                    || alertType === Enums.ModelData.AlertType.DeviceBatteryLow
                    || alertType === Enums.ModelData.AlertType.UnsFuelDec
                );
        });
        this.labelDecimalLessThan100 = ko.observable('');
        this.inputDecimalLessThan100 = ko.observable('');

        this.visibleAlertParkingType = ko.pureComputed(() => {
             var alertType = this.selectedAlertType() && this.selectedAlertType().value;
             return (alertType === Enums.ModelData.AlertType.Parking
                    || alertType === Enums.ModelData.AlertType.POIParking
                );
        });
        this.alertParkingTypes = ko.observableArray([]);
        this.selectedAlertParkingType = ko.observable();
        this.visibleOverTimeParkLimit = ko.pureComputed(() => {
             return this.selectedAlertParkingType() && this.selectedAlertParkingType().value ===  Enums.ModelData.AlertParkingType.ParkOverTime;
        });
        this.inputOverTimeParkLimit = ko.observable('');
        this._selectedAlertParkingTypeRef = this.selectedAlertParkingType.ignorePokeSubscribe((value)=>{
           this.inputOverTimeParkLimit('');
        });

        this.visibleAreaParkingType = ko.pureComputed(() => {
             var alertType = this.selectedAlertType() && this.selectedAlertType().value;
             return (alertType === Enums.ModelData.AlertType.AreaParking);
        });
        this.areaParkingTypes = ko.observableArray([]);
        this.selectedAreaParkingType = ko.observable();

        this.visibleDrivingType = ko.pureComputed(() => {
             var alertType = this.selectedAlertType() && this.selectedAlertType().value;
             return (alertType === Enums.ModelData.AlertType.POIInOut || alertType === Enums.ModelData.AlertType.AreaInOutVehicle || alertType === Enums.ModelData.AlertType.AreaInOutJob);
        });
        this.drivingTypes = ko.observableArray([]);
        this.selectedDrivingType = ko.observable();

        this.visibleCustomPoi = ko.pureComputed(() => {
             var alertType = this.selectedAlertType() && this.selectedAlertType().value;
             return (alertType === Enums.ModelData.AlertType.POIParking || alertType === Enums.ModelData.AlertType.POIInOut);
        });
        this.visibleCustomPoiCategory = ko.pureComputed(() => {
            var alertType = this.selectedAlertType() && this.selectedAlertType().value;
            return (alertType === Enums.ModelData.AlertType.POIParking || alertType === Enums.ModelData.AlertType.POIInOut);
        });
        this.customPoiList = ko.observableArray([]);
        this.customPoiCategories = ko.observableArray([]);
        this.selectedCustomPoi = ko.observable();
        this.selectedCustomPoiCategory = ko.observable();
        this._selectedCustomPoiRef = this.selectedCustomPoi.ignorePokeSubscribe((value)=>{
            if(value){
                this.selectedCustomPoiCategory.poke(null);
            }
        });
        this._selectedCustomPoiCategoryRef = this.selectedCustomPoiCategory.ignorePokeSubscribe((value)=>{
            if(value){
                this.selectedCustomPoi.poke(null);
                this.selectedCustomAreaCategory.poke(null);
            }
        });

        this.visibleCustomArea = ko.pureComputed(() => {
             var alertType = this.selectedAlertType() && this.selectedAlertType().value;
             return (alertType === Enums.ModelData.AlertType.AreaParking
                    || alertType === Enums.ModelData.AlertType.AreaInOutVehicle
                    || alertType === Enums.ModelData.AlertType.OverSpeed);
        });
        this.visibleCustomAreaCategory = ko.pureComputed(() => {
            var alertType = this.selectedAlertType() && this.selectedAlertType().value;
            return (alertType === Enums.ModelData.AlertType.AreaParking
                    || alertType === Enums.ModelData.AlertType.AreaInOutVehicle
                    || alertType === Enums.ModelData.AlertType.OverSpeed);
        });
        this.visibleDeviceGroup = ko.pureComputed(() => {
            return this.enableDeviceAlert();
        });
        this.customAreas = ko.observableArray([]);
        this.customAreaTypes = ko.observableArray([]);
        this.customAreaCategories = ko.observableArray([]);
        this.selectedCustomArea = ko.observable();
        this.selectedCustomAreaType = ko.observable();
        this.selectedCustomAreaCategory = ko.observable();
        this._selectedCustomAreaRef = this.selectedCustomArea.ignorePokeSubscribe((value)=>{
            if(value){
                this.selectedCustomAreaType.poke(null);
                this.selectedCustomAreaCategory.poke(null);
            }
        });
        this._selectedCustomAreaTypeRef = this.selectedCustomAreaType.ignorePokeSubscribe((value)=>{
            if(value){
                this.selectedCustomArea.poke(null);
                this.selectedCustomAreaCategory.poke(null);
            }
        });
        this._selectedCustomAreaCategoryRef = this.selectedCustomAreaCategory.ignorePokeSubscribe((value)=>{
            if(value){
                this.selectedCustomArea.poke(null);
                this.selectedCustomAreaType.poke(null);
                this.selectedCustomPoiCategory.poke(null);
            }
        });

        this.visibleTime = ko.pureComputed(() => {
             var alertType = this.selectedAlertType() && this.selectedAlertType().value;
             return (alertType === Enums.ModelData.AlertType.MoveNonWorking || 
                alertType === Enums.ModelData.AlertType.POIInOut || 
                alertType === Enums.ModelData.AlertType.AreaInOutVehicle);
        });
        this.inputFromTime = ko.observable(null);
        this.inputToTime = ko.observable(null);

        this.visibleCustomAlert = ko.pureComputed(() => {
             var alertType = this.selectedAlertType() && this.selectedAlertType().value;
             return (alertType === Enums.ModelData.AlertType.CustomAlert);
        });
        this.customMessage = ko.observable('');
        this.features = ko.observableArray([]);
        this.selectedFeature = ko.observable();
        this.visibleInputDigital = ko.pureComputed(() => {
            return this.visibleCustomAlert() && this.selectedFeature() && this.selectedFeature().eventDataType ===  Enums.ModelData.EventDataType.Digital;
        });
        this.visibleInputAnalog = ko.pureComputed(() => {
            return this.visibleCustomAlert() && this.selectedFeature() && this.selectedFeature().eventDataType ===  Enums.ModelData.EventDataType.Analog;
        });
        this.visibleInputText= ko.pureComputed(() => {
            return this.visibleCustomAlert() && this.selectedFeature() && this.selectedFeature().eventDataType ===  Enums.ModelData.EventDataType.Text;
        });
        this.inputDigital = ko.observable('');
        this.inputMinValue = ko.observable('');
        this.inputMaxValue = ko.observable('');
        this.inputText = ko.observable('');
        this._inputMinValueRef = this.inputMinValue.ignorePokeSubscribe((value)=>{
            var maxValue = this.inputMaxValue();
            if(maxValue && value && value > maxValue){
                this.inputMaxValue.poke(value);
            }
        });
        this._inputMaxValueRef = this.inputMaxValue.ignorePokeSubscribe((value)=>{
            var minValue = this.inputMinValue();
            if(minValue && value && value < minValue){
                this.inputMinValue.poke(value);
            }
        });

        this._selectedFeatureRef = this.selectedFeature.ignorePokeSubscribe((feature)=>{
            if (this.showM133()) {
                this.showMessageBox(null, this.i18n("M133")(), BladeDialog.DIALOG_OK).done((button)=> {
                    this.showM133(false);
                });
            }

            var alertType = this.selectedAlertType() && this.selectedAlertType().value;
            if(alertType == Enums.ModelData.AlertType.CustomAlert){
                var featureId = null;
                if(feature){
                    featureId = feature.id;
                }
                this.publishMessage("ca-configuration-manage-custom-alert-featureId-changed", featureId);
            }
        });

        this.selectedFeatureUnit = ko.observable();
        this.visibleFeatureUnit = ko.pureComputed(() => {
             var alertType = this.selectedAlertType() && this.selectedAlertType().value;
             return (alertType === Enums.ModelData.AlertType.GateClose 
             || alertType === Enums.ModelData.AlertType.GateOpen
             || alertType === Enums.ModelData.AlertType.CloseGateInsPOI
             || alertType === Enums.ModelData.AlertType.CloseGateOtsPOI
             || alertType === Enums.ModelData.AlertType.OpenGateInsPOI
             || alertType === Enums.ModelData.AlertType.OpenGateOtsPOI
             || alertType === Enums.ModelData.AlertType.UnderTemperature
             || alertType === Enums.ModelData.AlertType.OverTemperature);
        });
        this._selectedFeatureUnitRef = this.selectedFeatureUnit.ignorePokeSubscribe((feature)=>{
            if (this.showM133()) {
                this.showMessageBox(null, this.i18n("M133")(), BladeDialog.DIALOG_OK).done((button)=> {
                    this.showM133(false);
                });
            }

            if(this.visibleFeatureUnit()){
                var featureId = null;
                if(feature){
                    featureId = feature.id;
                }
                this.publishMessage("ca-configuration-manage-custom-alert-featureId-changed", featureId);
            }
        });

        this._selectedAlertTypeRef = this.selectedAlertType.ignorePokeSubscribe((value)=>{
            this.selectedVehicleIds([]);
            this.selectedDeliveryMenIds([]);
            this.selectedFeature(null);

            this.publishMessage("ca-configuration-manage-alert-changed", this.selectedAlertType());
            var alertType = this.selectedAlertType() && this.selectedAlertType().value;
            if (this.mode === Screen.SCREEN_MODE_CREATE && this.showM132()) {
                this.showMessageBox(null, this.i18n("M132")(), BladeDialog.DIALOG_OK).done((button)=> {
                    this.showM132(false);
                });
            }

            switch (alertType) {
                case Enums.ModelData.AlertType.GateClose:
                case Enums.ModelData.AlertType.GateOpen:
                case Enums.ModelData.AlertType.CloseGateInsPOI:
                case Enums.ModelData.AlertType.CloseGateOtsPOI:
                case Enums.ModelData.AlertType.OpenGateInsPOI:
                case Enums.ModelData.AlertType.OpenGateOtsPOI:
                    this.isBusy(true);
                    var listCustomAreaCategory = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary(new CustomAreaTypeCategoryFilter());
                    var listFeatures = this.webRequestFeature.listFeatureSummary(new FeatureFilter(Enums.ModelData.FeatureUnit.Gate));
                    $.when(listCustomAreaCategory,listFeatures).done((rlistCustomAreaCategory, rlistFeatures)=> {
                        this.features(rlistFeatures.items);
                        this.customAreaCategories(rlistCustomAreaCategory.items);
                        this.onManageCustomFields(null);
                        this.isBusy(false);
                    }).fail((e) => {
                        this.handleError(e);
                        this.isBusy(false);
                    });
                    break;
                case Enums.ModelData.AlertType.UnderTemperature:
                case Enums.ModelData.AlertType.OverTemperature:
                    this.isBusy(true);
                    var listCustomAreaCategory = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary(new CustomAreaTypeCategoryFilter());
                    var listFeatures = this.webRequestFeature.listFeatureSummary(new FeatureFilter(Enums.ModelData.FeatureUnit.Temperature));
                    $.when(listCustomAreaCategory, listFeatures).done((rlistCustomAreaCategory, rlistFeatures)=> {
                        this.features(rlistFeatures.items);
                        this.customAreaCategories(rlistCustomAreaCategory.items);
                        this.onManageCustomFields(null);
                        this.isBusy(false);
                    }).fail((e) => {
                        this.handleError(e);
                        this.isBusy(false);
                    });
                    break;
                case Enums.ModelData.AlertType.Parking:
                    this.isBusy(true);
                    var listCustomAreaCategory = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary(new CustomAreaTypeCategoryFilter());
                    var listEnumResources = this.webRequestEnumResource.listEnumResource(new EnumResourceFilter(Enums.ModelData.EnumResourceType.AlertParkingType));
                    $.when(listCustomAreaCategory, listEnumResources).done((rlistCustomAreaCategory, rlistEnumResources)=> {
                        this.alertParkingTypes(rlistEnumResources.items);
                        this.customAreaCategories(rlistCustomAreaCategory.items);
                        this.onManageCustomFields(null);
                        this.isBusy(false);
                    }).fail((e) => {
                        this.handleError(e);
                        this.isBusy(false);
                    });
                    break;
                case Enums.ModelData.AlertType.POIParking:
                    this.isBusy(true);
                    var listCustomPOISummary = this.webRequestCustomPOI.listCustomPOISummary(new CustomPOIFilter());
                    var listCustomPOICategorySummary = this.webRequestCustomPOICategory.listCustomPOICategorySummary(new CustomPOICategoryFilter());
                    var listCustomAreaCategory = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary(new CustomAreaTypeCategoryFilter());
                    var listAlertParkingType = this.webRequestEnumResource.listEnumResource(new EnumResourceFilter(Enums.ModelData.EnumResourceType.AlertParkingType))
                    $.when(listCustomPOISummary, listCustomPOICategorySummary, listCustomAreaCategory, listAlertParkingType).done((rListCustomPOISummary, rListCustomPOICategorySummary, rlistCustomAreaCategory, rListAlertParkingType) => {
                        this.customPoiList(rListCustomPOISummary.items);
                        this.customPoiCategories(rListCustomPOICategorySummary.items);
                        this.customAreaCategories(rlistCustomAreaCategory.items);
                        this.alertParkingTypes(rListAlertParkingType.items);
                        this.onManageCustomFields(null);
                        this.isBusy(false);
                    }).fail((e) => {
                        this.handleError(e);
                        this.isBusy(false);
                    });

                    break;
                case Enums.ModelData.AlertType.POIInOut:
                    this.isBusy(true);
                    var listCustomPOISummary = this.webRequestCustomPOI.listCustomPOISummary(new CustomPOIFilter());
                    var listCustomPOICategorySummary = this.webRequestCustomPOICategory.listCustomPOICategorySummary(new CustomPOICategoryFilter());
                    var listCustomAreaCategory = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary(new CustomAreaTypeCategoryFilter());
                    var listDrivingType = this.webRequestEnumResource.listEnumResource(new EnumResourceFilter(Enums.ModelData.EnumResourceType.DrivingType))
                    $.when(listCustomPOISummary, listCustomPOICategorySummary, listCustomAreaCategory, listDrivingType).done((rListCustomPOISummary, rListCustomPOICategorySummary, rlistCustomAreaCategory, rListDrivingType) => {
                        this.customPoiList(rListCustomPOISummary.items);
                        this.customPoiCategories(rListCustomPOICategorySummary.items);
                        this.customAreaCategories(rlistCustomAreaCategory.items);
                        this.drivingTypes(rListDrivingType.items);
                        this.onManageCustomFields(null);
                        this.isBusy(false);
                    }).fail((e) => {
                        this.handleError(e);
                        this.isBusy(false);
                    });

                    break;
                case Enums.ModelData.AlertType.AreaParking:
                    this.isBusy(true);
                    var listCustomAreaSummary = this.webRequestCustomArea.listCustomAreaSummary(new CustomAreaFilter());
                    var listCustomAreaType = this.webRequestCustomAreaType.listCustomAreaType(new CustomAreaTypeFilter());
                    var listCustomAreaCategory = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary(new CustomAreaTypeCategoryFilter());
                    var listAreaParkingType = this.webRequestEnumResource.listEnumResource(new EnumResourceFilter(Enums.ModelData.EnumResourceType.AreaParkingType))

                    $.when(listCustomAreaSummary, listCustomAreaType, listCustomAreaCategory, listAreaParkingType)
                        .done((rListCustomAreaSummary, rListCustomAreaType, rlistCustomAreaCategory, rListAreaParkingType) => {
                        this.customAreas(rListCustomAreaSummary.items);
                        this.customAreaTypes(rListCustomAreaType.items);
                        this.customAreaCategories(rlistCustomAreaCategory.items);
                        this.areaParkingTypes(rListAreaParkingType.items);
                        this.onManageCustomFields(null);
                        this.isBusy(false);
                    }).fail((e) => {
                        this.handleError(e);
                        this.isBusy(false);
                    });
                    break;
                case Enums.ModelData.AlertType.AreaInOutVehicle:
                    this.isBusy(true);
                    var listCustomAreaSummary = this.webRequestCustomArea.listCustomAreaSummary(new CustomAreaFilter());
                    var listCustomAreaType = this.webRequestCustomAreaType.listCustomAreaType(new CustomAreaTypeFilter());
                    var listCustomAreaCategory = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary(new CustomAreaTypeCategoryFilter());
                    var listDrivingType = this.webRequestEnumResource.listEnumResource(new EnumResourceFilter(Enums.ModelData.EnumResourceType.DrivingType))

                    $.when(listCustomAreaSummary, listCustomAreaType, listCustomAreaCategory, listDrivingType)
                        .done((rListCustomAreaSummary, rListCustomAreaType, rlistCustomAreaCategory, rListDrivingType) => {
                        this.customAreas(rListCustomAreaSummary.items);
                        this.customAreaTypes(rListCustomAreaType.items);
                        this.customAreaCategories(rlistCustomAreaCategory.items);
                        this.drivingTypes(rListDrivingType.items);
                        this.onManageCustomFields(null);
                        this.isBusy(false);
                    }).fail((e) => {
                        this.handleError(e);
                        this.isBusy(false);
                    });
                    break;
                case Enums.ModelData.AlertType.AreaInOutJob:
                    this.isBusy(true);
                    var listCustomAreaCategory = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary(new CustomAreaTypeCategoryFilter());
                    var listEnumResources = this.webRequestEnumResource.listEnumResource(new EnumResourceFilter(Enums.ModelData.EnumResourceType.DrivingType));
                    $.when(listEnumResources, listCustomAreaCategory).done((rlistEnumResources, rlistCustomAreaCategory)=> {
                        this.drivingTypes(rlistEnumResources.items);
                        this.customAreaCategories(rlistCustomAreaCategory.items);
                        this.onManageCustomFields(null);
                        this.isBusy(false);
                    }).fail((e) => {
                        this.handleError(e);
                        this.isBusy(false);
                    });
                    break;
                case Enums.ModelData.AlertType.CustomAlert:
                    this.isBusy(true);
                    var listCustomAreaCategory = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary(new CustomAreaTypeCategoryFilter());
                    var listFeatures = this.webRequestFeature.listFeatureSummary(new FeatureFilter());
                    $.when(listFeatures, listCustomAreaCategory).done((rlistFeatures, rlistCustomAreaCategory)=> {
                        this.features(rlistFeatures.items);
                        this.customAreaCategories(rlistCustomAreaCategory.items);
                        this.onManageCustomFields(null);
                        this.isBusy(false);
                    }).fail((e) => {
                        this.handleError(e);
                        this.isBusy(false);
                    });
                    break;
                case Enums.ModelData.AlertType.OverSpeed:
                    this.isBusy(true);
                    var d1 = this.webRequestCustomArea.listCustomAreaSummary(new CustomAreaFilter());
                    var d2 = this.webRequestCustomAreaType.listCustomAreaType(new CustomAreaTypeFilter());
                    var d3 = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary(new CustomAreaTypeCategoryFilter());
                    $.when(d1,d2,d3).done((r1,r2,r3) => {
                        this.customAreas(r1.items);
                        this.customAreaTypes(r2.items);
                        this.customAreaCategories(r3.items);
                        this.onManageCustomFields(null);
                        this.isBusy(false);
                    }).fail((e) => {
                        this.handleError(e);
                        this.isBusy(false);
                    });
                    break;
                default:
                    this.isBusy(true);
                    this.webRequestCustomAreaCategory.listCustomAreaCategorySummary(new CustomAreaTypeCategoryFilter()).done((r) => {
                        this.customAreaCategories(r.items);
                        this.onManageCustomFields(null);
                        this.isBusy(false);
                    }).fail((e) => {
                        this.handleError(e);
                        this.isBusy(false);
                    });
            }
        });

        //Check box Device
        this._enableDeviceAlert = this.enableDeviceAlert.ignorePokeSubscribe((value)=>{
            if(value){
                var d1 = this.webrequestAlertConfiguration.listAlertConfigurationBoxTemplateSummary({ sortingColumns: DefaultSorting.BoxTemplate });
                var d2 = this.webRequestEnumResource.listEnumResource(new EnumResourceFilter(Enums.ModelData.EnumResourceType.EventActionType));
                $.when(d1, d2).done((r1, r2) => {
                    this.boxTemplates(r1.items);          
                    this.commandTemplates(r2.items);
                });
            } else {
                this.selectedBoxTemplate.poke(null);
                this.selectedOutput.poke(null);
                this.selectedCommandTemplate.poke(null);
            }
        });

        //Change boxtemplate and get output boxtemplate
        this.selectedBoxTemplate.ignorePokeSubscribe((value)=>{
            var boxId = (typeof value != 'undefined') ? value.id : null;
            this.outputs([]);
            if(boxId != null){
                var d1 = this.webrequestAlertConfiguration.getAlertConfigurationBoxTemplate(boxId);
                $.when(d1).done((r1) => {
                    this.outputs(r1.features);
                });
            }
            
        });

        //for displayName output boxtemplate
        this.displayName = function(item) {
            return ko.unwrap(item.featureName) + ' (' + ko.unwrap(item.note) + ')';
        };

        //disable commandTemplate
        this.commandTemplateEnable = ko.pureComputed(() => {
            if(this.command()){
                return false;
            }else{
                return true;
            }   
        });
        //disable command
        this.commandEnable = ko.pureComputed(() => {
            if(this.selectedCommandTemplate()){
                return false;
            }else{
                return true;
            }
            
        });

        this.subscribeMessage("ca-configuration-alert-assets-selected", (selectedAssests) => {
            this.selectedBusinessUnitIds(selectedAssests.selectedBusinessUnitIds);
            if(selectedAssests.selectedVehicleIds){
                this.selectedVehicleIds(selectedAssests.selectedVehicleIds);

                if(selectedAssests.selectedVehicleIds.length > 0){
                    this.showM132(true);
                    if(this.visibleCustomAlert() || this.visibleFeatureUnit()){
                        this.showM133(true);
                    }
                }  
            }

            if(selectedAssests.selectedDeliveryMenIds){
                this.selectedDeliveryMenIds(selectedAssests.selectedDeliveryMenIds);

                if(selectedAssests.selectedDeliveryMenIds.length > 0){
                    this.showM132(true);
                    if(this.visibleCustomAlert() || this.visibleFeatureUnit()){
                        this.showM133(true);
                    }
                }
            }

            
        });

        //set value InsideCustomArea 
        this.insideArea = ko.observable(null);
        this.insideAreaType = ko.observable(null);
        this.insideAreaCate = ko.observable(null);
        this.subscribeMessage("ca-configuration-alert-assetsetting-selected", (customArea) => {
            switch(Object.keys(customArea)[0]){
                case "customArea":
                    this.insideArea(customArea.customArea);
                    this.insideAreaType(null);
                    this.insideAreaCate(null);
                    break;
                case "customAreaType":
                    this.insideAreaType(customArea.customAreaType);
                    this.insideArea(null);
                    this.insideAreaCate(null);
                    break;
                case "customAreaCate":
                    this.insideAreaCate(customArea.customAreaCate);
                    this.insideArea(null);
                    this.insideAreaType(null);
                    break;
                default :
                    this.insideAreaCate(null);
                    this.insideArea(null);
                    this.insideAreaType(null);
            }
        })
        
        //set value DateTimeSetting 
        this.enableOnMonday = ko.observable(false);
        this.enableOnTuesday = ko.observable(false);
        this.enableOnWednesday = ko.observable(false);
        this.enableOnThursday = ko.observable(false);
        this.enableOnFriday = ko.observable(false);
        this.enableOnSaturday = ko.observable(false);
        this.enableOnSunday = ko.observable(false);
        this.startTime = ko.observable();
        this.endTime = ko.observable();
        this.subscribeMessage("ca-configuration-alert-datetimesetting-selected", (selectedDateTime) => {
            this.enableOnMonday(selectedDateTime.enableOnMonday);
            this.enableOnTuesday(selectedDateTime.enableOnTuesday);
            this.enableOnWednesday(selectedDateTime.enableOnWednesday);
            this.enableOnThursday(selectedDateTime.enableOnThursday);
            this.enableOnFriday(selectedDateTime.enableOnFriday);
            this.enableOnSaturday(selectedDateTime.enableOnSaturday);
            this.enableOnSunday(selectedDateTime.enableOnSunday);
            this.startTime(selectedDateTime.startTime);
            this.endTime(selectedDateTime.endTime);
        })

        this.mlItem = ko.observableArray();
        this.selectedML = ko.observable();
        this.inputMinimumDuration = ko.observable();
        this.visibleManeuverLevel  = ko.pureComputed(() => {
            let isCallML = this.selectedAlertType() && (this.selectedAlertType().value == Enums.ModelData.AlertType.Acceleration
                           || this.selectedAlertType().value  == Enums.ModelData.AlertType.Breaking
                           || this.selectedAlertType().value  == Enums.ModelData.AlertType.Turn
                           || this.selectedAlertType().value  == Enums.ModelData.AlertType.WideTurn
                           || this.selectedAlertType().value  == Enums.ModelData.AlertType.SwerveWhileAccelerating
                           || this.selectedAlertType().value  == Enums.ModelData.AlertType.WideSwerveWhileAccelerating
                           || this.selectedAlertType().value  == Enums.ModelData.AlertType.SwerveWhileDecelerating
                           || this.selectedAlertType().value  == Enums.ModelData.AlertType.WideSwerveWhileDecelerating
                           || this.selectedAlertType().value  == Enums.ModelData.AlertType.Roundabout
                           || this.selectedAlertType().value  == Enums.ModelData.AlertType.LaneChange
                           || this.selectedAlertType().value  == Enums.ModelData.AlertType.Bypassing
                           || this.selectedAlertType().value  == Enums.ModelData.AlertType.SpeedBump
                           || this.selectedAlertType().value  == Enums.ModelData.AlertType.AccidentSuspicious
                           || this.selectedAlertType().value  == Enums.ModelData.AlertType.RealAccident) ? true : false ;

            if(isCallML && this.mode === Screen.SCREEN_MODE_CREATE ){
                this.webRequestEnumResource.listEnumResource(new EnumResourceFilter(Enums.ModelData.EnumResourceType.ManeuverLevelFilter)).done((r) =>{
                    this.mlItem(r.items);
                })
            }
            return isCallML;
        });

        this.enableRepeatAlert = ko.observable(false);
        this.overspeedDelay = ko.observable();
        this.visibleInfoOverSpeed = ko.pureComputed(() => {
            return (this.selectedAlertType() && this.selectedAlertType().value === Enums.ModelData.AlertType.OverSpeed);
        });

        //Unathorized
        this.recentChangedRowIds = ko.observableArray([]);
        //allow//
        this.subscribeMessage("allow-manage", (allowObj) => {
            
            this.allowList([]);
            this.allowList.replaceAll(allowObj);
           
        });

        this._allowSelectingRowHandler = (res) => {
            // this.navigate("ca-configuration-alert-manage-allow", params);
            // console.log("this.mode",this.mode)
            if(res) {
                
                let allowTableLst = this.allowList().filter((items)=>{ //เอาข้อมูลในตารางเฉพาะที่ไม่ใช่ตัวที่ถูกเลือกส่งไป
                    return items.dataId != res.dataId
                })

               

                let data = {
                    mode: Screen.SCREEN_MODE_UPDATE,
                    editTypeMode: this.editType,
                    allowList: allowTableLst ,
                    allTableList:this.allowList(),
                    allowId: res.dataId,
                    condition: res.condition,
                    conditionValue: res.conditionValue,
                    categoryTypeName: res.categoryTypeName,
                    categoryTypeValue: res.categoryTypeValue,
                    categoryNameId: res.categoryNameId,
                    categoryNameLst: res.categoryNameLst,
                    parkingType: res.parkingType,
                    parkingDuration: res.parkingDuration,
                    parkingDurationForDisplay: res.parkingDurationForDisplay,
                    alertConfigurationId: (res.alertConfigurationId) ? res.alertConfigurationId : null,
                    id: (res.id)?res.id:null,
                    duration: res.duration
                }

                this.navigate("ca-configuration-alert-manage-allow",data);
            }
        
        };

        //not allow//
        this.subscribeMessage("not-allow-manage", (notAllowObj) => {
            
            this.notAllowList.replaceAll(notAllowObj)


        });

        this._notAllowSelectingRowHandler = (res) => {
           
            if(res) {

                let notAllowTableLst = this.notAllowList().filter((items)=>{ //เอาข้อมูลในตารางเฉพาะที่ไม่ใช่ตัวที่ถูกเลือกส่งไป
                    return items.dataId != res.dataId
                })
            

                let data = {
                    mode: Screen.SCREEN_MODE_UPDATE,
                    editTypeMode: this.editType,
                    notAllowList: notAllowTableLst ,
                    allTableList:this.notAllowList(),
                    notAllowId: res.dataId,
                    condition: res.condition,
                    conditionValue: res.conditionValue,
                    categoryTypeName: res.categoryTypeName,
                    categoryTypeValue: res.categoryTypeValue,
                    categoryNameId: res.categoryNameId,
                    categoryNameLst: res.categoryNameLst,
                    speedTypeValue: res.speedTypeValue,
                    speedTypeForDisplay: res.speedTypeForDisplay,
                    alertConfigurationId: (res.alertConfigurationId) ? res.alertConfigurationId : null,
                    id: (res.id)?res.id:null,
                    
                }

                this.navigate("ca-configuration-alert-manage-not-allow",data);
            }
        
        };

        //Stop Over Time//

        
        this.subscribeMessage("stop-over-time-manage",(stopOvertimeObj)=>{
            
            this.stopOvertimeList.replaceAll(stopOvertimeObj)
        })
        this._stopOvertimeSelectingRowHandler=(res)=>{
    
            if(res) {
            

                let stopOvertomeTableLst = this.stopOvertimeList().filter((items)=>{ //เอาข้อมูลในตารางเฉพาะที่ไม่ใช่ตัวที่ถูกเลือกส่งไป
                    return items.dataId != res.dataId
                })
            

                let data = {
                    mode: Screen.SCREEN_MODE_UPDATE,
                    editTypeMode: this.editType,
                    stopOvertimeList: stopOvertomeTableLst ,
                    allTableList:this.stopOvertimeList(),
                    stopOvertimeId: res.dataId,
                    provinceGroupName: res.provinceGroupName,
                    selectedProvinceForDisplay: res.selectedProvinceForDisplay,
                    selectedProvinceId: res.selectedProvinceId,
                    selectedRegion: res.selectedRegion,
                    stopDuration: res.stopDuration,
                    stopDurationForUpdate: res.duration,
                    alertConfigurationId: (res.alertConfigurationId) ? res.alertConfigurationId : null,
                    id: (res.id)?res.id:null,

                }
               

                this.navigate("ca-configuration-alert-manage-stop-overtime-province",data);
            }

        }

        if(this.mode == Screen.SCREEN_MODE_UPDATE){

            this.stopOvertimeList(params.stopProvinceList)
            this.allowList(params.allowList)
            this.notAllowList(params.notAllowList)
        }

        this.moveDurationUnusualFuel = ko.observable();
        this.visibleMoveDurationUnusualFuel = ko.pureComputed(() => {
            return (this.selectedAlertType() && this.selectedAlertType().value === Enums.ModelData.AlertType.UnsFuelDec); 
        });

        this.parkingExceed = ko.observable();
        this.visibleExceedLimitDrivingVehicle = ko.pureComputed(() => {
            return (this.selectedAlertType() && this.selectedAlertType().value === Enums.ModelData.AlertType.ExceedLimitDrivingVehicle);  
        });

        this.turnOptions = ko.observableArray([]);
        this.selectedTurnOption = ko.observable();

        this.isVisibleDirection  = ko.pureComputed(() => {
            let showTurnOption = this.selectedAlertType() && 
                                (this.selectedAlertType().value == Enums.ModelData.AlertType.Turn ||
                                this.selectedAlertType().value == Enums.ModelData.AlertType.WideTurn ||
                                this.selectedAlertType().value == Enums.ModelData.AlertType.SwerveWhileAccelerating ||
                                this.selectedAlertType().value == Enums.ModelData.AlertType.SwerveWhileDecelerating ||
                                this.selectedAlertType().value == Enums.ModelData.AlertType.WideSwerveWhileAccelerating ||
                                this.selectedAlertType().value == Enums.ModelData.AlertType.WideSwerveWhileDecelerating);

            if(showTurnOption && this.mode === Screen.SCREEN_MODE_CREATE ){
                this.webRequestEnumResource.listEnumResource(new EnumResourceFilter(Enums.ModelData.EnumResourceType.TurnOption)).done((r) =>{
                    this.turnOptions(r.items);
                })
            }
            return showTurnOption;
        });

        this.isVisibleMinDuration  = ko.pureComputed(() => {
            let showMinDuration = this.selectedAlertType() && 
                             (this.selectedAlertType().value == Enums.ModelData.AlertType.Acceleration
                           || this.selectedAlertType().value  == Enums.ModelData.AlertType.Breaking
                           || this.selectedAlertType().value  == Enums.ModelData.AlertType.Turn
                           || this.selectedAlertType().value  == Enums.ModelData.AlertType.WideTurn
                           || this.selectedAlertType().value  == Enums.ModelData.AlertType.SwerveWhileAccelerating
                           || this.selectedAlertType().value  == Enums.ModelData.AlertType.WideSwerveWhileAccelerating
                           || this.selectedAlertType().value  == Enums.ModelData.AlertType.SwerveWhileDecelerating
                           || this.selectedAlertType().value  == Enums.ModelData.AlertType.WideSwerveWhileDecelerating
                           || this.selectedAlertType().value  == Enums.ModelData.AlertType.Roundabout
                           || this.selectedAlertType().value  == Enums.ModelData.AlertType.LaneChange
                           || this.selectedAlertType().value  == Enums.ModelData.AlertType.Bypassing
                           || this.selectedAlertType().value  == Enums.ModelData.AlertType.SpeedBump
                           || this.selectedAlertType().value  == Enums.ModelData.AlertType.AccidentSuspicious
                           || this.selectedAlertType().value  == Enums.ModelData.AlertType.RealAccident
                           || this.selectedAlertType().value  == Enums.ModelData.AlertType.FreeWheeling);
            
            if(this.inputMinimumDuration() == null && this.selectedAlertType() && this.selectedAlertType() && this.selectedAlertType().value == Enums.ModelData.AlertType.FreeWheeling){
                return showMinDuration = this.inputMinimumDuration(1);
            }else{
                return showMinDuration;
            }
        });
    }
    
    /**
     * Get WebRequest specific for Alert Configuration module in Web API access.
     * @readonly
     */
    get webrequestAlertConfiguration() {
        return WebrequestAlertConfiguration.getInstance();
    }
    /**
     * Get WebRequest specific for Alert Type module in Web API access.
     * @readonly
     */
    get webRequestAlertType() {
        return WebRequestAlertType.getInstance();
    }
    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }
    /**
     * Get WebRequest specific for Company module in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    /**
     * Get WebRequest specific for Custom Area Type module in Web API access.
     * @readonly
     */
    get webRequestCustomAreaType() {
        return WebRequestCustomAreaType.getInstance();
    }
    /**
     * Get WebRequest specific for Feature module in Web API access.
     * @readonly
     */
    get webRequestFeature() {
        return WebRequestFeature.getInstance();
    }
    /**
     * Get WebRequest specific for Custom Area module in Web API access.
     * @readonly
     */
    get webRequestCustomArea() {
        return WebRequestCustomArea.getInstance();
    }
    /**
     * Get WebRequest specific for Custom Area Category module in Web API access.
     * @readonly
     */
    get webRequestCustomAreaCategory() {
        return WebRequestCustomAreaCategory.getInstance();
    }
    /**
     * Get WebRequest specific for Custom POI module in Web API access.
     * @readonly
     */
    get webRequestCustomPOI() {
        return WebRequestCustomPOI.getInstance();
    }
    /**
     * Get WebRequest specific for Custom POI Category module in Web API access.
     * @readonly
     */
    get webRequestCustomPOICategory() {
        return WebRequestCustomPOICategory.getInstance();
    }
    /**
     * Get WebRequest specific for Custom POI Category module in Web API access.
     * @readonly
     */
    get webRequestAlertCategory() {
        return WebRequestAlertCategory.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();
        var alertTypeFilter = new AlertTypeFilter();
        var a1 = (this.mode === Screen.SCREEN_MODE_UPDATE) ? this.webrequestAlertConfiguration.getAlertConfiguration(this.alertConfigurationId,
                [EntityAssociation.AlertConfiguration.Vehicles, 
                EntityAssociation.AlertConfiguration.DeliveryMen]) : null;
        var a2 = (this.mode === Screen.SCREEN_MODE_CREATE) ? this.webRequestCompany.getCompanySetting(this.companyId) : null;
        var a3 = (this.mode === Screen.SCREEN_MODE_UPDATE) ?  this.webRequestEnumResource.listEnumResource(new EnumResourceFilter(Enums.ModelData.EnumResourceType.ManeuverLevelFilter)) : null ;
        var a4 = this.webRequestAlertCategory.listAlertCategorySummary({});
        var a5 = this.webrequestAlertConfiguration.listAlertConfigurationBoxTemplateSummary({ sortingColumns: DefaultSorting.BoxTemplate });
        // เรียก Web Request เพราะ ว่า มีการ poke enableDeviceAlert ไว้ ไม่ให้ไป SubScribe
        var a6 = this.webRequestEnumResource.listEnumResource(new EnumResourceFilter(Enums.ModelData.EnumResourceType.EventActionType));
        var a7 = (this.mode === Screen.SCREEN_MODE_UPDATE) ?  this.webRequestEnumResource.listEnumResource(new EnumResourceFilter(Enums.ModelData.EnumResourceType.TurnOption)) : null ;        
        var a8 = this.webRequestEnumResource.listEnumResource(new EnumResourceFilter(Enums.ModelData.EnumResourceType.AlertSound));        
        $.when(a1, a2, a3, a4,a5,a6,a7,a8).done((r1, r2, r3, r4,r5,r6,r7,r8) => {
            var alertConfiguration = r1;
            var setting = r2;
            if(r3) this.mlItem(r3.items);
            if(r7) this.turnOptions(r7.items);
            
            this.alertCategory(r4.items);
            this.notificationSound(r8.items);
            
            if(setting){
                 this.temperatureUnit(setting.temperatureUnitSymbol);
            }  
            
            if (alertConfiguration) {
                this.webRequestAlertType.listAlertType(new AlertTypeFilter(alertConfiguration.alertTypeId)).done((r3)=> {
                    this.alertTypes(r3.items);
                    this.boxTemplates(r5.items);
                    this.commandTemplates(r6.items);

                    this.inputMinSpeed(alertConfiguration.minSpeed);
                    this.inputMaxSpeed(alertConfiguration.maxSpeed);
                    this.inputMinRPM(alertConfiguration.minAnalogValue);
                    this.inputMaxRPM(alertConfiguration.maxAnalogValue);

                    this.enableType(false);
                    this.mailTo(alertConfiguration.mailTo);
                    this.temperatureUnit(alertConfiguration.temperatureUnitSymbol);
                    this.selectedAlertType.poke(ScreenHelper.findOptionByProperty(this.alertTypes, "id", alertConfiguration.alertTypeId));
                    this.priority(alertConfiguration.priority);
                    this.selectedEnable(ScreenHelper.findOptionByProperty(this.enableOptions, "value", alertConfiguration.enable));
                    this.enableWebsiteAlert(alertConfiguration.enableWebsiteAlert);
                    this.enableEmailAlert(alertConfiguration.enableEmailAlert);
                    this.enableSmsAlert(alertConfiguration.enableSmsAlert);
                    this.enableCreateTicket(alertConfiguration.enableAutoTicket);
                    this.enableMDVR(alertConfiguration.enableMDVR);
                    this.enableInterfaceAlert(alertConfiguration.enableInterfaceAlert);
                    this.enableDeviceAlert.poke(alertConfiguration.enableDeviceAlert);
                    this.enableReportAlert(alertConfiguration.enableReport);
                    this.description(alertConfiguration.description);
                    this.selectedML(ScreenHelper.findOptionByProperty(this.mlItem, "name", alertConfiguration.maneuverLevelDisplayName));
                    
                    this.inputMinimumDuration(alertConfiguration.minDuration);
                    this.selectedTurnOption(ScreenHelper.findOptionByProperty(this.turnOptions, "value", alertConfiguration.turnOption));
                    //Assets
                    this._originalSelectedVehicles = $.extend(true, [], alertConfiguration.vehicles);
                    this._originalSelectedDeliveryMen = $.extend(true, [], alertConfiguration.deliveryMen);
                    alertConfiguration.vehicles.forEach(function(element) {
                        this.selectedVehicleIds.push(element.vehicleId);
                        let BUId = element.businessUnitId;
                        let checked = _.indexOf(this.selectedBusinessUnitIds(), BUId) > -1;
                        if(!checked){
                            this.selectedBusinessUnitIds.push(BUId);
                        }
                    }, this);
                    alertConfiguration.deliveryMen.forEach(function(element) {
                        this.selectedDeliveryMenIds.push(element.userId);
                    }, this);
                    //DateTime
                    this.enableOnMonday(alertConfiguration.enableOnMonday);
                    this.enableOnTuesday(alertConfiguration.enableOnTuesday);
                    this.enableOnWednesday(alertConfiguration.enableOnWednesday);
                    this.enableOnThursday(alertConfiguration.enableOnThursday);
                    this.enableOnFriday(alertConfiguration.enableOnFriday);
                    this.enableOnSaturday(alertConfiguration.enableOnSaturday);
                    this.enableOnSunday(alertConfiguration.enableOnSunday);
                    this.startTime(alertConfiguration.startTime);
                    this.endTime(alertConfiguration.endTime);

                    this.insideArea(alertConfiguration.inAreaId);
                    this.insideAreaType(alertConfiguration.inAreaTypeId);
                    this.insideAreaCate(alertConfiguration.inAreaCategoryId);

                    //this.selectedBoxTemplate(alertConfiguration.BoxTemplateId);
                    this.selectedOutput(alertConfiguration.BoxTemplateFeatureId);
                    this.selectedCommandTemplate(alertConfiguration.CommandId);
                    this.command(alertConfiguration.CommandText);
                    this.selectedAlertCategory(ScreenHelper.findOptionByProperty(this.alertCategory, "id", alertConfiguration.alertCategoryId));
                    this.selectedNotificationSound(ScreenHelper.findOptionByProperty(this.notificationSound, "value", alertConfiguration.alertSound));
                    this.moveDurationUnusualFuel(alertConfiguration.movementDuration);

                    this.selectedBoxTemplate.poke(ScreenHelper.findOptionByProperty(this.boxTemplates, "id", alertConfiguration.boxTemplateId));
                    
                    switch (this.selectedAlertType().value) {
                        case Enums.ModelData.AlertType.GateClose:
                        case Enums.ModelData.AlertType.GateOpen:
                        case Enums.ModelData.AlertType.CloseGateInsPOI:
                        case Enums.ModelData.AlertType.CloseGateOtsPOI:
                        case Enums.ModelData.AlertType.OpenGateInsPOI:
                        case Enums.ModelData.AlertType.OpenGateOtsPOI:
                            var listCustomAreaCategory = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary(new CustomAreaTypeCategoryFilter());
                            var listFeatures = this.webRequestFeature.listFeatureSummary(new FeatureFilter(Enums.ModelData.FeatureUnit.Gate));
                            $.when(listCustomAreaCategory,listFeatures).done((rlistCustomAreaCategory,rlistFeatures)=> {
                                this.customAreaCategories(rlistCustomAreaCategory.items);
                                this.features(rlistFeatures.items);
                                this.onManageCustomFields(alertConfiguration);
                                dfd.resolve();
                            }).fail((e) => {
                                dfd.reject(e);
                            });
                            break;
                        case Enums.ModelData.AlertType.OverTemperature:
                        case Enums.ModelData.AlertType.UnderTemperature:
                            var listCustomAreaCategory = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary(new CustomAreaTypeCategoryFilter());
                            var listFeatures = this.webRequestFeature.listFeatureSummary(new FeatureFilter(Enums.ModelData.FeatureUnit.Temperature));
                            $.when(listCustomAreaCategory,listFeatures).done((rlistCustomAreaCategory,rlistFeatures)=> {
                                this.customAreaCategories(rlistCustomAreaCategory.items);
                                this.features(rlistFeatures.items);
                                this.onManageCustomFields(alertConfiguration);
                                dfd.resolve();
                            }).fail((e) => {
                                dfd.reject(e);
                            });
                            break;
                        case Enums.ModelData.AlertType.Parking:
                            var listCustomAreaCategory = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary(new CustomAreaTypeCategoryFilter());
                            var listParkingTypes = this.webRequestEnumResource.listEnumResource(new EnumResourceFilter(Enums.ModelData.EnumResourceType.AlertParkingType));
                            $.when(listCustomAreaCategory,listParkingTypes).done((rlistCustomAreaCategory,rlistParkingTypes)=> {
                                this.customAreaCategories(rlistCustomAreaCategory.items);
                                this.alertParkingTypes(rlistParkingTypes.items);
                                this.onManageCustomFields(alertConfiguration);
                                dfd.resolve();
                            }).fail((e) => {
                                dfd.reject(e);
                            });
                            break;
                        case Enums.ModelData.AlertType.POIParking:
                            var listCustomPOISummary = this.webRequestCustomPOI.listCustomPOISummary(new CustomPOIFilter());
                            var listCustomPOICategorySummary = this.webRequestCustomPOICategory.listCustomPOICategorySummary(new CustomPOICategoryFilter());
                            var listCustomAreaCategory = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary(new CustomAreaTypeCategoryFilter());
                            var listAlertParkingType = this.webRequestEnumResource.listEnumResource(new EnumResourceFilter(Enums.ModelData.EnumResourceType.AlertParkingType))
                            $.when(listCustomPOISummary, listCustomPOICategorySummary,listCustomAreaCategory, listAlertParkingType).done((rListCustomPOISummary, rListCustomPOICategorySummary, rlistCustomAreaCategory, rListAlertParkingType) => {
                                this.customPoiList(rListCustomPOISummary.items);
                                this.customPoiCategories(rListCustomPOICategorySummary.items);
                                this.customAreaCategories(rlistCustomAreaCategory.items);
                                this.alertParkingTypes(rListAlertParkingType.items);
                                this.onManageCustomFields(alertConfiguration);
                                dfd.resolve();
                            }).fail((e) => {
                                dfd.reject(e);
                            });
                            break;
                        case Enums.ModelData.AlertType.POIInOut:
                            var listCustomPOISummary = this.webRequestCustomPOI.listCustomPOISummary(new CustomPOIFilter());
                            var listCustomPOICategorySummary = this.webRequestCustomPOICategory.listCustomPOICategorySummary(new CustomPOICategoryFilter());
                            var listDrivingType = this.webRequestEnumResource.listEnumResource(new EnumResourceFilter(Enums.ModelData.EnumResourceType.DrivingType));
                            var listCustomAreaCategory = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary(new CustomAreaTypeCategoryFilter());
                            $.when(listCustomPOISummary, listCustomPOICategorySummary, listDrivingType, listCustomAreaCategory).done((rListCustomPOISummary, rListCustomPOICategorySummary, rListDrivingType, rlistCustomAreaCategory) => {
                                this.customPoiList(rListCustomPOISummary.items);
                                this.customPoiCategories(rListCustomPOICategorySummary.items);
                                this.drivingTypes(rListDrivingType.items);
                                this.customAreaCategories(rlistCustomAreaCategory.items);
                                this.onManageCustomFields(alertConfiguration);
                                dfd.resolve();
                            }).fail((e) => {
                                dfd.reject(e);
                            });
                            break;
                        case Enums.ModelData.AlertType.AreaParking:
                            var listCustomAreaSummary = this.webRequestCustomArea.listCustomAreaSummary(new CustomAreaFilter());
                            var listCustomAreaType = this.webRequestCustomAreaType.listCustomAreaType(new CustomAreaTypeFilter());
                            var listCustomAreaCategory = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary(new CustomAreaTypeCategoryFilter());
                            var listAreaParkingType = this.webRequestEnumResource.listEnumResource(new EnumResourceFilter(Enums.ModelData.EnumResourceType.AreaParkingType))

                            $.when(listCustomAreaSummary, listCustomAreaType, listAreaParkingType, listCustomAreaCategory)
                                .done((rListCustomAreaSummary, rListCustomAreaType, rListAreaParkingType, rlistCustomAreaCategory) => {
                                this.customAreas(rListCustomAreaSummary.items);
                                this.customAreaTypes(rListCustomAreaType.items);
                                this.areaParkingTypes(rListAreaParkingType.items);
                                this.customAreaCategories(rlistCustomAreaCategory.items);
                                this.onManageCustomFields(alertConfiguration);
                                dfd.resolve();
                            }).fail((e) => {
                                dfd.reject(e);
                            });
                            break;
                        case Enums.ModelData.AlertType.AreaInOutVehicle:
                            var listCustomAreaSummary = this.webRequestCustomArea.listCustomAreaSummary(new CustomAreaFilter());
                            var listCustomAreaType = this.webRequestCustomAreaType.listCustomAreaType(new CustomAreaTypeFilter());
                            var listCustomAreaCategory = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary(new CustomAreaTypeCategoryFilter());
                            var listDrivingType = this.webRequestEnumResource.listEnumResource(new EnumResourceFilter(Enums.ModelData.EnumResourceType.DrivingType))

                            $.when(listCustomAreaSummary, listCustomAreaType, listCustomAreaCategory, listDrivingType)
                                .done((rListCustomAreaSummary, rListCustomAreaType, rlistCustomAreaCategory, rListDrivingType) => {
                                this.customAreas(rListCustomAreaSummary.items);
                                this.customAreaTypes(rListCustomAreaType.items);
                                this.customAreaCategories(rlistCustomAreaCategory.items);
                                this.drivingTypes(rListDrivingType.items);
                                this.onManageCustomFields(alertConfiguration);
                                dfd.resolve();
                            }).fail((e) => {
                                dfd.reject(e);
                            });
                            break;
                        case Enums.ModelData.AlertType.AreaInOutJob:
                            var listCustomAreaCategory = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary(new CustomAreaTypeCategoryFilter());
                            var listEnumResource = this.webRequestEnumResource.listEnumResource(new EnumResourceFilter(Enums.ModelData.EnumResourceType.DrivingType));
                            $.when(listCustomAreaCategory, listEnumResource).done((rlistCustomAreaCategory, rlistEnumResource)=> {
                                this.customAreaCategories(rlistCustomAreaCategory.items);
                                this.drivingTypes(rlistEnumResource.items);
                                this.onManageCustomFields(alertConfiguration);
                                dfd.resolve();
                            }).fail((e) => {
                                dfd.reject(e);
                            });
                            break;
                        case Enums.ModelData.AlertType.CustomAlert:
                            var listCustomAreaCategory = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary(new CustomAreaTypeCategoryFilter());
                            var listFratures = this.webRequestFeature.listFeatureSummary(new FeatureFilter());
                            $.when(listCustomAreaCategory,listFratures).done((rlistCustomAreaCategory, rlistFratures)=> {
                                this.customAreaCategories(rlistCustomAreaCategory.items);
                                this.features(rlistFratures.items);
                                this.onManageCustomFields(alertConfiguration);
                            dfd.resolve();
                            }).fail((e) => {
                                dfd.reject(e);
                            });
                            break;
                        case Enums.ModelData.AlertType.OverSpeed:
                            var d1 = this.webRequestCustomArea.listCustomAreaSummary(new CustomAreaFilter());
                            var d2 = this.webRequestCustomAreaType.listCustomAreaType(new CustomAreaTypeFilter());
                            var d3 = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary(new CustomAreaTypeCategoryFilter());
                            $.when(d1,d2,d3).done((r1,r2,r3) => {
                                this.customAreas(r1.items);
                                this.customAreaTypes(r2.items);
                                this.customAreaCategories(r3.items);
                                this.onManageCustomFields(alertConfiguration);
                                dfd.resolve();
                            }).fail((e) => {
                                dfd.reject(e);
                            });
                            break;
                        default:
                            this.webRequestCustomAreaCategory.listCustomAreaCategorySummary(new CustomAreaTypeCategoryFilter()).done((r) => {
                                this.customAreaCategories(r.items);
                                this.onManageCustomFields(alertConfiguration);
                                dfd.resolve();
                            }).fail((e) => {
                                dfd.reject(e);
                            });
                    }
                }).fail((e) => {
                    dfd.reject(e);
                });
            }else{
                this.webRequestAlertType.listAlertType(new AlertTypeFilter()).done((r3)=> {
                    
                    this.alertTypes(r3.items);
                    dfd.resolve();
                }).fail((e) => {
                    dfd.reject(e);
                });
            }   
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        var self = this;
        self.selectedAlertType.extend({
            trackChange: true
        });
        self.inputMinSpeed.extend({
            validation: {
                validator: () => {
                    let result = true;
                    if(this.visibleMinSpeed() && this.visibleMaxSpeed() && this.inputMinSpeed() && this.inputMaxSpeed()){
                        if (this.inputMinSpeed() > this.inputMaxSpeed()){
                            result = false;
                        }
                        return result;
                    }
                    return result;
                },
                message: this.i18n("M260")()
            },
            trackChange: true
        });
        self.inputMaxSpeed.extend({
            trackChange: true
        });
        self.inputMinRPM.extend({
            validation: {
                validator: () => {
                    let result = true;
                    if(this.visibleMinRPM() && this.visibleMaxRPM() && this.inputMinRPM() && this.inputMaxRPM()){
                        if (this.inputMaxRPM() < this.inputMinRPM()){
                            result = false;
                        }
                        return result;
                    }
                    return result;
                },
                message: this.i18n("M261")()
            },
            trackChange: true
        });
        self.inputMaxRPM.extend({
            trackChange: true
        });
        self.priority.extend({
            trackChange: true
        });

        self.selectedEnable.extend({
            trackChange: true
        });
        self.selectedNotificationSound.extend({
            trackChange: true
        });
        self.mailTo.extend({
            validation: {
                validator: function (val, validate) {
                    if (!validate) { return true; }
                    var isValid = true;
                    if (!ko.validation.utils.isEmptyVal(val)) {
                        // use the required: true property if you don't want to accept empty values
                        var values = val.split(',');
                        $(values).each(function (index) {
                            isValid = ko.validation.rules['email'].validator($.trim(this), validate);
                            return isValid; // short circuit each loop if invalid
                        });
                    }
                    return isValid;
                },
                message: this.i18n("M064")()
            },
            trackChange: true
        });
        self.enableWebsiteAlert.extend({
            trackChange: true
        });
        self.enableEmailAlert.extend({
            trackChange: true
        });
        self.enableSmsAlert.extend({
            trackChange: true
        });
        self.enableInterfaceAlert.extend({
            trackChange: true
        });
        self.enableDeviceAlert.extend({
            trackChange: true
        });
        self.description.extend({
            trackChange: true
        });
        self.enableMDVR.extend({
            trackChange: true
        });
        self.enableReportAlert.extend({
            trackChange: true
        });
        //Assets
        self.selectedVehicleIds.extend({
           trackArrayChange: true
        });
        self.selectedDeliveryMenIds.extend({
           trackArrayChange: true
        });
        //Custom Fields
        self.inputDecimal.extend({
            trackChange: true
        });
        self.inputInteger.extend({
            trackChange: true
        });
        self.inputIntegerMax24.extend({
            trackChange: true
        });
        self.inputDecimalAllowNegative.extend({
            trackChange: true
        });
        self.inputDecimalLessThan100.extend({
            trackChange: true
        });
        self.selectedAlertParkingType.extend({
            trackChange: true
        });
        self.inputOverTimeParkLimit.extend({
            trackChange: true
        });
        self.selectedCustomPoi.extend({
            trackChange: true
        });
        self.selectedCustomPoiCategory.extend({
            trackChange: true
        });
        self.selectedCustomArea.extend({
            trackChange: true
        });
        self.selectedCustomAreaType.extend({
            trackChange: true
        });
        self.selectedCustomAreaCategory.extend({
            trackChange: true
        });
        self.selectedAreaParkingType.extend({
            trackChange: true
        });
        self.selectedDrivingType.extend({
            trackChange: true
        });
        self.customMessage.extend({
            trackChange: true
        });
        self.selectedFeature.extend({
            trackChange: true
        });
        self.selectedFeatureUnit.extend({
            trackChange: true
        });
        self.inputDigital.extend({
            trackChange: true
        });
        self.inputText.extend({
            trackChange: true
        });
        self.inputMinValue.extend({
            trackChange: true
        });
        self.inputMaxValue.extend({
            trackChange: true
        });
        self.inputMinimumDuration.extend({
            trackChange: true,
            required: {
                onlyIf: function () { 
                    return self.isVisibleMinDuration(); 
                }
            }
        });
        // Manual setup validation rules
        self.selectedAlertType.extend({
            required: true
        });
        self.inputMinSpeed.extend({
            required: {           
                onlyIf: function () { 
                    return self.visibleMinSpeed(); 
                }
            }
        });
        self.inputMaxSpeed.extend({
            required: {           
                onlyIf: function () { 
                    return self.visibleMaxSpeed(); 
                }
            }
        });
        self.inputMinRPM.extend({
            required: {           
                onlyIf: function () { 
                    return self.visibleMinRPM(); 
                }
            }
        });
        self.inputMaxRPM.extend({
            required: {           
                onlyIf: function () { 
                    return self.visibleMaxRPM(); 
                }
            }
        });
        self.priority.extend({
            required: true
        });
        self.selectedEnable.extend({
            required: true
        });

        self.selectedNotificationSound.extend({
            required: {           
                onlyIf: function () { 
                    return self.enableWebsiteAlert(); 
                }
            }
        });


        self.description.extend({
            required: true
        });

        //Custom Fields
        self.inputDecimal.extend({
            required: {           
                onlyIf: function () { 
                    return self.visibleDecimal(); 
                }
            }
        });
        self.inputInteger.extend({
            required: {           
                onlyIf: function () { 
                    return self.visibleInteger(); 
                }
            }
        });
        self.inputIntegerMax24.extend({
            required: {           
                onlyIf: function () { 
                    return self.visibleIntegerMax24(); 
                }
            }
        });
        self.inputDecimalAllowNegative.extend({
            required: {           
                onlyIf: function () { 
                    return self.visibleDecimalAllowNegative(); 
                }
            }
        });
        self.inputDecimalLessThan100.extend({
            required: {           
                onlyIf: function () { 
                    return self.visibleDecimalLessThan100(); 
                }
            }
        });
        self.selectedAlertParkingType.extend({
            required: {           
                onlyIf: function () { 
                    return self.visibleAlertParkingType(); 
                }
            }
        });
        self.inputOverTimeParkLimit.extend({
            required: {           
                onlyIf: function () { 
                    return self.visibleOverTimeParkLimit(); 
                }
            }
        });
        self.selectedCustomPoi.extend({
            required: {           
                onlyIf: function () { 
                    return self.visibleCustomPoi() && _.isNil(self.selectedCustomPoiCategory());
                }
            }
        });
        self.selectedCustomPoiCategory.extend({
            required: {           
                onlyIf: function () { 
                    return self.visibleCustomPoi() && _.isNil(self.selectedCustomPoi()); 
                }
            }
        });
        /*self.selectedCustomArea.extend({
            required: {           
                onlyIf: function () { 
                    return self.visibleCustomArea() && _.isNil(self.selectedCustomAreaType()) && _.isNil(self.selectedCustomAreaCategory());
                }
            }
        });
        self.selectedCustomAreaType.extend({
            required: {           
                onlyIf: function () { 
                    return self.visibleCustomArea() && _.isNil(self.selectedCustomArea()) && _.isNil(self.selectedCustomAreaCategory()); 
                }
            }
        });
        self.selectedCustomAreaCategory.extend({
            required: {           
                onlyIf: function () { 
                    return self.visibleCustomArea() && _.isNil(self.selectedCustomArea()) && _.isNil(self.selectedCustomAreaType()); 
                }
            }
        });*/
        self.selectedAreaParkingType.extend({
            required: {           
                onlyIf: function () { 
                    return self.visibleAreaParkingType() ; 
                }
            }
        });
        self.selectedDrivingType.extend({
            required: {           
                onlyIf: function () { 
                    return self.visibleDrivingType() ; 
                }
            }
        });
        self.inputFromTime.extend({
            required: {           
                onlyIf: function () { 
                    return self.visibleTime() ; 
                }
            }
        });
        self.inputToTime.extend({
            required: {           
                onlyIf: function () { 
                    return self.visibleTime() ; 
                }
            }
        });
        self.customMessage.extend({
            required: {           
                onlyIf: function () { 
                    return self.visibleCustomAlert(); 
                }
            }
        });
        self.selectedFeature.extend({
            required: {           
                onlyIf: function () { 
                    return self.visibleCustomAlert(); 
                }
            }
        });
        self.selectedFeatureUnit.extend({
            required: {           
                onlyIf: function () { 
                    return self.visibleFeatureUnit(); 
                }
            }
        });
        self.inputMinValue.extend({
            required: {           
                onlyIf: function () { 
                    return self.visibleInputAnalog() && !self.inputMaxValue(); 
                }
            }
        });
        self.inputMaxValue.extend({
            required: {           
                onlyIf: function () { 
                    return self.visibleInputAnalog() && !self.inputMinValue(); 
                }
            }
        });

        self.selectedBoxTemplate.extend({
            required: {           
                onlyIf: function () { 
                    return self.enableDeviceAlert(); 
                }
            }
        });

        self.selectedOutput.extend({
            required: {           
                onlyIf: function () { 
                    return self.enableDeviceAlert(); 
                }
            }
        });

        self.selectedCommandTemplate.extend({
            required: {           
                onlyIf: function () { 
                    return self.enableDeviceAlert() && !self.command(); 
                }
            }
        });

        self.command.extend({
            required: {           
                onlyIf: function () { 
                    return self.enableDeviceAlert() && !self.selectedCommandTemplate(); 
                }
            }
        });

        self.allowList.extend({
           
            allowValidate: this.notAllowList()
        });
        self.notAllowList.extend({
          
            allowValidate: this.allowList()
        });
        this.parkingExceed.extend({
            trackChange: true,
            required: {
                onlyIf: function() {
                    return self.visibleExceedLimitDrivingVehicle()
                }
            }
        });
        this.overspeedDelay.extend({
            trackChange: true,
            required: {
                onlyIf: function() {
                    return self.visibleInfoOverSpeed()
                }
            }
        });

        this.enableRepeatAlert.extend({trackChange: true});
        self.validationModel = ko.validatedObservable({
            allowList: self.allowList,
            notAllowList: self.notAllowList,
            selectedAlertType: self.selectedAlertType,
            inputMinSpeed: self.inputMinSpeed,
            inputMaxSpeed: self.inputMaxSpeed,
            inputMinRPM: self.inputMinRPM,
            inputMaxRPM: self.inputMaxRPM,
            priority: self.priority,
            description : self.description,
            selectedEnable: self.selectedEnable,
            selectedNotificationSound : self.selectedNotificationSound,
            mailTo: self.mailTo,
            inputDecimal: self.inputDecimal,
            inputInteger: self.inputInteger,
            inputIntegerMax24: self.inputIntegerMax24,
            inputDecimalAllowNegative: self.inputDecimalAllowNegative,
            inputDecimalLessThan100: self.inputDecimalLessThan100,
            selectedAlertParkingType: self.selectedAlertParkingType,
            inputOverTimeParkLimit: self.inputOverTimeParkLimit,
            selectedCustomPoi: self.selectedCustomPoi,
            selectedCustomPoiCategory: self.selectedCustomPoiCategory,
            selectedCustomArea: self.selectedCustomArea,
            selectedCustomAreaType: self.selectedCustomAreaType,
            selectedCustomAreaCategory: self.selectedCustomAreaCategory,
            selectedAreaParkingType: self.selectedAreaParkingType,
            selectedDrivingType: self.selectedDrivingType,
            inputFromTime: self.inputFromTime,
            inputToTime: self.inputToTime,
            customMessage: self.customMessage,
            selectedFeature: self.selectedFeature,
            selectedFeatureUnit: self.selectedFeatureUnit,
            inputMinValue: self.inputMinValue,
            inputMaxValue: self.inputMaxValue,
            selectedBoxTemplate:self.selectedBoxTemplate,
            selectedOutput:self.selectedOutput,
            selectedCommandTemplate:self.selectedCommandTemplate,
            command:self.command,
            parkingExceed: self.parkingExceed,
            overspeedDelay: self.overspeedDelay,
            inputMinimumDuration: self.inputMinimumDuration
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createAction("actCancel", this.i18n("Common_Cancel")(), "cancel"));
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }

    getUnauthorized(){
        let unauthorizedArray = this.allowList().concat(this.notAllowList());

        if(this.mode === 'update'){
            
            
            unauthorizedArray.forEach((item)=>{
                item.customIds = item.customIds.filter((items)=>{
                    return items != 0
                })
                delete item.id;
            })

        }else{
       
            unauthorizedArray.forEach((item)=>{
            item.customIds = item.customIds.filter((items)=>{
                return items != 0
            })
        })
        }
         
        return unauthorizedArray
    }
    
    /**
     * Generate Alert Configuration model from View Model
     * 
     * @returns Alert Configuration model which is ready for ajax request 
     */
    generateModel() {

        let unauthorizedObj = this.getUnauthorized()
        let unauthorizedObjFinish = unauthorizedObj.concat(this.stopOvertimeList())
        
        var model = {
            id: this.alertConfigurationId,
            companyId: this.companyId,
            alertTypeId: this.selectedAlertType().id,
            alertCategoryId: (this.selectedAlertCategory()) ? this.selectedAlertCategory().id : null,
            alertSound: (this.selectedNotificationSound()) ? this.selectedNotificationSound().value : null,
            priority: this.priority(),
            enable: this.selectedEnable().value,
            mailTo: this.mailTo(),
            enableWebsiteAlert: this.enableWebsiteAlert(),
            enableEmailAlert: this.enableEmailAlert(),
            enableSmsAlert: this.enableSmsAlert(),
            enableInterfaceAlert: this.enableInterfaceAlert(),
            enableDeviceAlert:this.enableDeviceAlert(),
            description: this.description(),
            featureId: this.selectedAlertType().featureId,
            enableAutoTicket: this.enableCreateTicket(),
            enableMDVR: this.enableMDVR(),
            enableReport : this.enableReportAlert(),
            maneuverLevelFilter : (this.selectedML()) ? this.selectedML().value : null,
            unauthorizedStopInfo : unauthorizedObjFinish,
            movementDuration: this.moveDurationUnusualFuel() ? this.moveDurationUnusualFuel() : null,
            minDuration: this.isVisibleMinDuration() ? this.inputMinimumDuration() : null
        };
        //Assets
        var vehicles = [];
        var deliveryMen = [];
         switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.selectedVehicleIds().forEach((vehicleId) => {
                    vehicles.push({
                        vehicleId: vehicleId,
                        infoStatus: Enums.InfoStatus.Add
                    });
                });
                this.selectedDeliveryMenIds().forEach((userId) => {
                    deliveryMen.push({
                        userId: userId,
                        infoStatus: Enums.InfoStatus.Add
                    });
                });
            break;
            case Screen.SCREEN_MODE_UPDATE:
                this.selectedVehicleIds().forEach((vehicleId) => {
                    var original = ko.utils.arrayFirst(this._originalSelectedVehicles, function (item)
                    {
                        return item.vehicleId === vehicleId;
                    });

                    if(original){
                        vehicles.push({
                            id: original.id,
                            vehicleId: vehicleId,
                            infoStatus: Enums.InfoStatus.Original
                        });
                    }else {
                        vehicles.push({
                            vehicleId: vehicleId,
                            infoStatus: Enums.InfoStatus.Add
                        });
                    }  
                });
                this._originalSelectedVehicles.forEach((vehicle) => {
                    var selectedItem = ko.utils.arrayFirst(this.selectedVehicleIds(), function (vehicleId)
                    {
                        return vehicleId === vehicle.vehicleId;
                    });

                    if(!selectedItem){
                         vehicles.push({
                            id: vehicle.id,
                            vehicleId: vehicle.vehicleId,
                            infoStatus: Enums.InfoStatus.Delete
                        });
                    }
                });

                this.selectedDeliveryMenIds().forEach((userId) => {
                    var original = ko.utils.arrayFirst(this._originalSelectedDeliveryMen, function (item)
                    {
                        return item.userId === userId;
                    });

                    if(original){
                        deliveryMen.push({
                            id: original.id,
                            userId: userId,
                            infoStatus: Enums.InfoStatus.Original
                        });
                    }else {
                        deliveryMen.push({
                            userId: userId,
                            infoStatus: Enums.InfoStatus.Add
                        });
                    }  
                });
                this._originalSelectedDeliveryMen.forEach((user) => {
                    var selectedItem = ko.utils.arrayFirst(this.selectedDeliveryMenIds(), function (userId)
                    {
                        return userId === user.userId;
                    });

                    if(!selectedItem){
                         deliveryMen.push({
                            id: user.id,
                            userId: user.userId,
                            infoStatus: Enums.InfoStatus.Delete
                        });
                    }
                });
            break;
        }

        model.vehicles = vehicles;
        model.deliveryMen = deliveryMen;
        model.customAreaCategoryId = this.selectedCustomAreaCategory() ? this.selectedCustomAreaCategory().id : null;
        //Custom Fields
        switch (this.selectedAlertType().value) {
            case Enums.ModelData.AlertType.GateClose:
            case Enums.ModelData.AlertType.GateOpen:
            case Enums.ModelData.AlertType.CloseGateInsPOI:
            case Enums.ModelData.AlertType.CloseGateOtsPOI:
            case Enums.ModelData.AlertType.OpenGateInsPOI:
            case Enums.ModelData.AlertType.OpenGateOtsPOI:
                model.featureId = this.selectedFeatureUnit().id;
                break;
            case Enums.ModelData.AlertType.AccRapid:
                model.accelerateLimit = this.inputDecimal();
                break;
            case Enums.ModelData.AlertType.VehicleBatteryLow:
                model.vehicleBatteryLowLimit = this.inputDecimalLessThan100();
                break;
            case Enums.ModelData.AlertType.DeviceBatteryLow:
                 model.deviceBatteryLowLimit = this.inputDecimalLessThan100();
                break;
            case Enums.ModelData.AlertType.DecRapid:
                model.decelerateLimit = this.inputDecimal();
                break;
            case Enums.ModelData.AlertType.OverSpeed:
                model.speedLimit = this.inputDecimal();
                model.customAreaId = this.selectedCustomArea() ? this.selectedCustomArea().id : null;
                model.customAreaTypeId = this.selectedCustomAreaType() ? this.selectedCustomAreaType().id : null;
                model.workingPeriodFrom = this.inputFromTime();
                model.workingPeriodTo = this.inputToTime();
                model.overSpeedDelay = this.overspeedDelay();
                model.overSpeedFirstTime = this.enableRepeatAlert();
                break;
            case Enums.ModelData.AlertType.OverTemperature:
                model.temperatureMaxLimit = this.inputDecimalAllowNegative();
                model.featureId = this.selectedFeatureUnit().id;
                break;
            case Enums.ModelData.AlertType.Swerve:
                 model.swerveLimit = this.inputDecimal();
                break;
            case Enums.ModelData.AlertType.UnderTemperature:
                model.temperatureMinLimit = this.inputDecimalAllowNegative();
                model.featureId = this.selectedFeatureUnit().id;
                break;
            case Enums.ModelData.AlertType.UnsFuelDec:
                model.fuelDecreaseLimit = this.inputDecimalLessThan100();
                break;
            case Enums.ModelData.AlertType.OverTimePark:
                model.overTimeParkLimit = this.inputInteger();
                break;
            case Enums.ModelData.AlertType.Parking:
                model.alertParkingType = this.selectedAlertParkingType().value;
                model.overTimeParkLimit = this.inputOverTimeParkLimit();
                break;
            case Enums.ModelData.AlertType.POIParking:
                model.customPoiId = this.selectedCustomPoi() ? this.selectedCustomPoi().id : null;
                model.customPoiCategoryId = this.selectedCustomPoiCategory() ? this.selectedCustomPoiCategory().id : null;
                model.alertParkingType = this.selectedAlertParkingType().value;
                model.overTimeParkLimit = this.inputOverTimeParkLimit();
                break;
            case Enums.ModelData.AlertType.POIInOut:
                model.customPoiId = this.selectedCustomPoi() ? this.selectedCustomPoi().id : null;
                model.customPoiCategoryId = this.selectedCustomPoiCategory() ? this.selectedCustomPoiCategory().id : null;
                model.drivingType = this.selectedDrivingType().value;
                model.workingPeriodFrom = this.inputFromTime();
                model.workingPeriodTo = this.inputToTime();
                break;
            case Enums.ModelData.AlertType.AreaParking:
                model.customAreaId = this.selectedCustomArea() ? this.selectedCustomArea().id : null;
                model.customAreaTypeId = this.selectedCustomAreaType() ? this.selectedCustomAreaType().id : null;
                model.areaParkingType = this.selectedAreaParkingType().value;
                break;
            case Enums.ModelData.AlertType.AreaInOutVehicle:
                model.customAreaId = this.selectedCustomArea() ? this.selectedCustomArea().id : null;
                model.customAreaTypeId = this.selectedCustomAreaType() ? this.selectedCustomAreaType().id : null;
                model.drivingType = this.selectedDrivingType().value;
                model.workingPeriodFrom = this.inputFromTime();
                model.workingPeriodTo = this.inputToTime();
                break;
            case Enums.ModelData.AlertType.AreaInOutJob:
                model.drivingType = this.selectedDrivingType().value;
                break;
            case Enums.ModelData.AlertType.MoveNonWorking:
                model.workingPeriodFrom = this.inputFromTime();
                model.workingPeriodTo = this.inputToTime();
                break;
            case Enums.ModelData.AlertType.ExceedLimitDriving:
                model.drivingTimeLimit = this.inputIntegerMax24();
                break;
            case Enums.ModelData.AlertType.CustomAlert:
                model.customMessage = this.customMessage();
                model.featureId = this.selectedFeature().id;

                switch (this.selectedFeature().eventDataType) {
                    case Enums.ModelData.EventDataType.Digital:
                        model.matchDigitalValue = this.inputDigital();
                        break;
                    case Enums.ModelData.EventDataType.Analog:
                         model.minAnalogValue = this.inputMinValue();
                         model.maxAnalogValue = this.inputMaxValue();
                        break;
                    case Enums.ModelData.EventDataType.Text:
                         model.matchTextValue = this.inputText();
                        break;
                }
                break;
            case Enums.ModelData.AlertType.ExceedLimitDrivingIn24Hours:
                model.drivingTimeLimit = this.inputIntegerMax24();
                break;
            case Enums.ModelData.AlertType.GreenBandDriving:
                model.minSpeed = this.inputMinSpeed();
                model.maxSpeed = this.inputMaxSpeed();
                model.minAnalogValue = this.inputMinRPM();
                model.maxAnalogValue = this.inputMaxRPM();
                break;
            case Enums.ModelData.AlertType.OverRPM:
                model.minSpeed = this.inputMinSpeed();
                model.maxAnalogValue = this.inputMaxRPM();
                break;
            case Enums.ModelData.AlertType.NoSeatBelt:
                model.maxSpeed = this.inputMaxSpeed();
                break;
            case Enums.ModelData.AlertType.ExceedLimitDrivingVehicle:
                model.drivingTimeLimit = this.inputIntegerMax24();
                model.minParkTime = this.parkingExceed();
                break;
            case Enums.ModelData.AlertType.FreeWheeling:
                model.minSpeed = this.inputMinSpeed();
                model.maxSpeed = this.inputMaxSpeed();
                model.minAnalogValue = this.inputMinRPM();
                break;

        }

        if(this.enableDeviceAlert()) {
            model.BoxTemplateId = this.selectedBoxTemplate().id;
            model.BoxTemplateFeatureId = this.selectedOutput().id;
            if(this.selectedCommandTemplate())
                model.CommandId = this.selectedCommandTemplate().id;
            model.CommandText = this.command();
        }
       
        if(this.isVisibleDirection()){
            model.turnOption = this.selectedTurnOption().value;
        }

        model.inAreaId = this.insideArea();
        model.inAreaTypeId = this.insideAreaType();
        model.inAreaCategoryId = this.insideAreaCate();

        model.enableOnMonday = this.enableOnMonday();
        model.enableOnTuesday = this.enableOnTuesday();
        model.enableOnWednesday = this.enableOnWednesday();
        model.enableOnThursday = this.enableOnThursday();
        model.enableOnFriday = this.enableOnFriday();
        model.enableOnSaturday = this.enableOnSaturday();
        model.enableOnSunday = this.enableOnSunday();
        model.startTime = this.startTime();
        model.endTime = this.endTime();
        return model;
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();

                return;
            }
            // var model = this.generateModel();
            // console.log("model",model)    
            this.isBusy(true);
            var model = this.generateModel();

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webrequestAlertConfiguration.createAlertConfiguration(model).done((response) => {
                        this.publishMessage("ca-configuration-alert-changed", response.id);
                        this.isBusy(false);
                        this.close(true);
                    }).fail((e)=> {
                        this.isBusy(false);

                        this.handleError(e);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.webrequestAlertConfiguration.updateAlertConfiguration(model).done((response) => {
                        this.publishMessage("ca-configuration-alert-changed");
                        this.isBusy(false);
                        this.close(true);
                    }).fail((e)=> {
                        this.isBusy(false);

                        this.handleError(e);
                    });
                    break;   
            }
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
    onManageCustomFields(info){
        var alertType = this.selectedAlertType() && this.selectedAlertType().value;
        if(alertType){
            if(info){
                this.selectedCustomAreaCategory.poke(ScreenHelper.findOptionByProperty(this.customAreaCategories, "id", info.customAreaCategoryId));
                if (info.boxTemplateId) {
                    var d1 = this.webrequestAlertConfiguration.getAlertConfigurationBoxTemplate(info.boxTemplateId);
                    $.when(d1).done((r1) => {
                        this.outputs(r1.features);
                        this.selectedOutput.poke(ScreenHelper.findOptionByProperty(this.outputs, "id", info.boxTemplateFeatureId));
                    });
                }
                this.selectedCommandTemplate.poke(ScreenHelper.findOptionByProperty(this.commandTemplates, "id", info.commandId));
                this.command(info.commandText);
            } else {
                this.selectedCustomAreaCategory.poke(null);
                this.selectedBoxTemplate.poke(null);
                this.selectedOutput.poke(null);
                this.selectedCommandTemplate.poke(null);
            }
            switch (alertType) {
                case Enums.ModelData.AlertType.GateClose:
                case Enums.ModelData.AlertType.GateOpen:
                case Enums.ModelData.AlertType.CloseGateInsPOI:
                case Enums.ModelData.AlertType.CloseGateOtsPOI:
                case Enums.ModelData.AlertType.OpenGateInsPOI:
                case Enums.ModelData.AlertType.OpenGateOtsPOI:
                    if(info){
                        this.selectedFeatureUnit(ScreenHelper.findOptionByProperty(this.features, "id", info.featureId)); 
                    }else{
                        this.selectedFeatureUnit(null);
                    }
                    break;
                case Enums.ModelData.AlertType.AccRapid:
                    this.labelDecimal(this.i18n("Configurations_ACCRapid")());
                    if(info){
                        this.inputDecimal(info.accelerateLimit);
                    }else{
                        this.inputDecimal('');
                    }
                    break;
                case Enums.ModelData.AlertType.VehicleBatteryLow:
                    this.labelDecimalLessThan100(this.i18n("Configurations_VehicleBatteryLow")());
                    if(info){
                        this.inputDecimalLessThan100(info.vehicleBatteryLowLimit);
                    }else{
                        this.inputDecimalLessThan100('');
                    }
                    break;
                case Enums.ModelData.AlertType.DeviceBatteryLow:
                    this.labelDecimalLessThan100(this.i18n("Configurations_DeviceBatteryLow")());
                    if(info){
                        this.inputDecimalLessThan100(info.deviceBatteryLowLimit);
                    }else{
                        this.inputDecimalLessThan100('');
                    }
                    break;
                case Enums.ModelData.AlertType.DecRapid:
                    this.labelDecimal(this.i18n("Configurations_DecRapid")());
                    if(info){
                        this.inputDecimal(info.decelerateLimit);
                    }else{
                        this.inputDecimal('');
                    }
                    break;
                case Enums.ModelData.AlertType.OverSpeed:
                    this.labelDecimal(this.i18n("Configurations_OverSpeed")());
                    if(info){
                        this.inputDecimal(info.speedLimit);
                        this.selectedCustomArea.poke(ScreenHelper.findOptionByProperty(this.customAreas, "id", info.customAreaId));
                        this.selectedCustomAreaType.poke(ScreenHelper.findOptionByProperty(this.customAreaTypes, "id", info.customAreaTypeId));
                        this.inputFromTime(info.workingPeriodFrom);
                        this.inputToTime(info.workingPeriodTo);
                        this.insideArea(info.inAreaId);
                        this.insideAreaType(info.inAreaTypeId);
                        this.insideAreaCate(info.inAreaCategoryId);
                        this.overspeedDelay(info.overSpeedDelay);
                        this.enableRepeatAlert(info.overSpeedFirstTime);
                    }else{
                        this.inputDecimal('');
                        this.selectedCustomArea.poke(null);
                        this.selectedCustomAreaType.poke(null);
                    }
                    break;
                case Enums.ModelData.AlertType.OverTemperature:
                    this.labelDecimalAllowNegative(this.i18n("Configurations_OverTemperature", [this.temperatureUnit()]));
                    if(info){
                        this.inputDecimalAllowNegative(info.temperatureMaxLimit);
                        this.selectedFeatureUnit(ScreenHelper.findOptionByProperty(this.features, "id", info.featureId)); 
                    }else{
                        this.inputDecimalAllowNegative('');
                        this.selectedFeatureUnit(null);
                    }
                    break;
                case Enums.ModelData.AlertType.Swerve:
                    this.labelDecimal(this.i18n("Configurations_SwerveLimit")());
                    if(info){
                        this.inputDecimal(info.swerveLimit);
                    }else{
                        this.inputDecimal('');
                    }
                    break;
                case Enums.ModelData.AlertType.UnderTemperature:
                    this.labelDecimalAllowNegative(this.i18n("Configurations_UnderTemperature", [this.temperatureUnit()]));
                    if(info){
                        this.inputDecimalAllowNegative(info.temperatureMinLimit);
                        this.selectedFeatureUnit(ScreenHelper.findOptionByProperty(this.features, "id", info.featureId)); 
                    }else{
                        this.inputDecimalAllowNegative('');
                        this.selectedFeatureUnit(null);
                    }
                    break;
                case Enums.ModelData.AlertType.UnsFuelDec:
                    this.labelDecimalLessThan100(this.i18n("Configurations_UnsFuelDec")());
                    if(info){
                        this.inputDecimalLessThan100(info.fuelDecreaseLimit);
                    }else{
                        this.inputDecimalLessThan100('');
                    }
                    break;
                case Enums.ModelData.AlertType.OverTimePark:
                    this.labelInteger(this.i18n("Configurations_OverTimePark")());
                    if(info){
                        this.inputInteger(info.overTimeParkLimit);
                    }else{
                        this.inputInteger('');
                    }
                    break;
                case Enums.ModelData.AlertType.Parking:
                    if(info){
                        this.selectedAlertParkingType.poke(ScreenHelper.findOptionByProperty(this.alertParkingTypes, "value", info.alertParkingType));
                        this.inputOverTimeParkLimit(info.overTimeParkLimit);
                    }else{
                        this.selectedAlertParkingType.poke(null);
                        this.inputOverTimeParkLimit('');
                    }
                    break;
                case Enums.ModelData.AlertType.POIParking:
                    if(info){
                        this.selectedCustomPoi.poke(ScreenHelper.findOptionByProperty(this.customPoiList, "id", info.customPoiId));
                        this.selectedCustomPoiCategory.poke(ScreenHelper.findOptionByProperty(this.customPoiCategories, "id", info.customPoiCategoryId));
                        this.selectedAlertParkingType.poke(ScreenHelper.findOptionByProperty(this.alertParkingTypes, "value", info.alertParkingType));
                        this.inputOverTimeParkLimit(info.overTimeParkLimit);
                    }else{
                        this.selectedCustomPoi.poke(null);
                        this.selectedCustomPoiCategory.poke(null);
                        this.selectedAlertParkingType.poke(null);
                        this.inputOverTimeParkLimit('');
                    }
                    break;
                case Enums.ModelData.AlertType.POIInOut:
                    if(info){
                        this.selectedCustomPoi.poke(ScreenHelper.findOptionByProperty(this.customPoiList, "id", info.customPoiId));
                        this.selectedCustomPoiCategory.poke(ScreenHelper.findOptionByProperty(this.customPoiCategories, "id", info.customPoiCategoryId));
                        this.selectedDrivingType(ScreenHelper.findOptionByProperty(this.drivingTypes, "value", info.drivingType)); 
                        this.inputFromTime(info.workingPeriodFrom);
                        this.inputToTime(info.workingPeriodTo);
                    }else{
                        this.selectedCustomPoi.poke(null);
                        this.selectedCustomPoiCategory.poke(null);
                        this.selectedDrivingType(null);
                    }
                    break;
                case Enums.ModelData.AlertType.AreaParking:
                    if(info){
                        this.selectedCustomArea.poke(ScreenHelper.findOptionByProperty(this.customAreas, "id", info.customAreaId));
                        this.selectedCustomAreaType.poke(ScreenHelper.findOptionByProperty(this.customAreaTypes, "id", info.customAreaTypeId));
                        this.selectedAreaParkingType(ScreenHelper.findOptionByProperty(this.areaParkingTypes, "value", info.areaParkingType)); 
                    }else{
                        this.selectedCustomArea.poke(null);
                        this.selectedCustomAreaType.poke(null);
                        this.selectedAreaParkingType(null);
                    }
                    break;
                case Enums.ModelData.AlertType.AreaInOutVehicle:
                    if(info){
                        this.selectedCustomArea.poke(ScreenHelper.findOptionByProperty(this.customAreas, "id", info.customAreaId));
                        this.selectedCustomAreaType.poke(ScreenHelper.findOptionByProperty(this.customAreaTypes, "id", info.customAreaTypeId));
                        this.selectedDrivingType(ScreenHelper.findOptionByProperty(this.drivingTypes, "value", info.drivingType)); 
                        this.inputFromTime(info.workingPeriodFrom);
                        this.inputToTime(info.workingPeriodTo);
                    }else{
                        this.selectedCustomArea.poke(null);
                        this.selectedCustomAreaType.poke(null);
                        this.selectedDrivingType(null);
                    }
                    break;
                case Enums.ModelData.AlertType.AreaInOutJob:
                    if(info){
                        this.selectedDrivingType(ScreenHelper.findOptionByProperty(this.drivingTypes, "value", info.drivingType)); 
                    }else{
                        this.selectedDrivingType(null);
                    }
                    break;
                case Enums.ModelData.AlertType.MoveNonWorking:
                    if(info){
                        this.inputFromTime(info.workingPeriodFrom);
                        this.inputToTime(info.workingPeriodTo);
                    }else{
                        this.inputFromTime('');
                        this.inputToTime('');
                    }
                    break;
                case Enums.ModelData.AlertType.ExceedLimitDriving:
                    this.labelIntegerMax24(this.i18n("Configurations_ExceedLimitDriving")());
                    if(info){
                        this.inputIntegerMax24(info.drivingTimeLimit);
                    }else{
                        this.inputIntegerMax24('');
                    }
                    break;
                case Enums.ModelData.AlertType.CustomAlert:
                    if(info){
                        this.customMessage(info.customMessage);
                        this.selectedFeature(ScreenHelper.findOptionByProperty(this.features, "id", info.featureId)); 
                        
                        switch (this.selectedFeature().eventDataType) {
                            case Enums.ModelData.EventDataType.Digital:
                                this.inputDigital(info.matchDigitalValue);
                                break;
                            case Enums.ModelData.EventDataType.Analog:
                                this.inputMinValue(info.minAnalogValue);
                                this.inputMaxValue(info.maxAnalogValue);
                                break;
                            case Enums.ModelData.EventDataType.Text:
                                this.inputText(info.matchTextValue);
                                break;
                        }
                
                    }else{
                        this.customMessage('');
                        this.selectedFeature(null);
                        this.inputDigital('');
                        this.inputMinValue('');
                        this.inputMaxValue('');
                        this.inputText('');
                    }
                    break;
                case Enums.ModelData.AlertType.ExceedLimitDrivingIn24Hours:
                    this.labelIntegerMax24(this.i18n("Configurations_ExceedLimitDrivingIn24Hours")());
                    if(info){
                        this.inputIntegerMax24(info.drivingTimeLimit);
                    }else{
                        this.inputIntegerMax24('');
                    }
                    break;
                case Enums.ModelData.AlertType.ExceedLimitDrivingVehicle:
                    this.labelIntegerMax24(this.i18n("Configurations_ExceedLimitDriving")());
                    let parkTime = info ? info.minParkTime : '';
                    let drivingTimeLimit = info ? info.drivingTimeLimit : '';
                    this.parkingExceed(parkTime);
                    this.inputIntegerMax24(drivingTimeLimit);
                    break;
            }
        }  
    }
    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedSubMenuItem(null);
    }

    onSelectSubMenuItem(index) {
        if (this.journeyCanClose()) {
            this.goToSubPage(index);
        }
        else {
            this.showMessageBox(null, this.i18n("M100")(),
                BladeDialog.DIALOG_OKCANCEL).done((button) => {
                switch (button) {
                    case BladeDialog.BUTTON_OK:
                        this.goToSubPage(index);
                        break;
                }
            });
        }
    }

    goToSubPage (index) {
        var page = "";
        var options = { };
        switch (index) {
            case 1:
                page = "ca-configuration-alert-manage-areasetting-select";
                options.mode = this.mode;
                options.insideArea = this.insideArea();
                options.insideAreaType = this.insideAreaType();
                options.insideAreaCate = this.insideAreaCate();
                break;
            case 2:
                // page = "ca-configuration-alert-manage-asset-select";
                // page = "ca-configuration-alert-manage-find-asset"
                page = "ca-configuration-alert-manage-asset-result";
                // options.mode = this.mode;
                options.alertType = this.selectedAlertType();
                options.featureId = this.selectedAlertType() && this.selectedAlertType().featureId;
                if(this.selectedAlertType() && this.selectedAlertType().value === Enums.ModelData.AlertType.CustomAlert && this.selectedFeature()){
                    options.featureId = this.selectedFeature().id;
                }
                else if(this.visibleFeatureUnit() && this.selectedFeatureUnit())
                {
                    options.featureId = this.selectedFeatureUnit().id;
                }
                options.selectedVehicleIds = this.selectedVehicleIds();
                options.selectedDeliveryMenIds = this.selectedDeliveryMenIds();
                options.businessUnitIds = _.size(this.selectedBusinessUnitIds()) ? this.selectedBusinessUnitIds() : [0];
                break;
            case 3:
                page = "ca-configuration-alert-manage-datetimesetting-select";
                options.mode = this.mode;
                options.enableOnMonday = this.enableOnMonday();
                options.enableOnTuesday = this.enableOnTuesday();
                options.enableOnWednesday = this.enableOnWednesday();
                options.enableOnThursday = this.enableOnThursday();
                options.enableOnFriday = this.enableOnFriday();
                options.enableOnSaturday = this.enableOnSaturday();
                options.enableOnSunday = this.enableOnSunday();
                options.startTime = this.startTime();
                options.endTime = this.endTime();
                break;
        }
        this.forceNavigate(page, options);
        this.selectedSubMenuItem(index);
    }

//Unauthorized Click//fv
    addAllow () {

        let allowParam =    {   mode: Screen.SCREEN_MODE_CREATE,
                                editTypeMode: this.mode,
                                allowList: this.allowList()  
                            }
        this.navigate("ca-configuration-alert-manage-allow", allowParam);


    }
    addNotAllow () {
        this.navigate("ca-configuration-alert-manage-not-allow", {  mode: Screen.SCREEN_MODE_CREATE,
                                                                    editTypeMode: this.mode,
                                                                    notAllowList: this.notAllowList() });
    }
    addStopOvertimeProvince () {
        this.navigate("ca-configuration-alert-manage-stop-overtime-province", { mode: Screen.SCREEN_MODE_CREATE,
                                                                                editTypeMode: this.mode,
                                                                                stopOvertimeList:this.stopOvertimeList() });

    }

    playAlertSound(){
        let alertSound = this.selectedNotificationSound();
        if(alertSound){
            switch (this.selectedNotificationSound().value) {
                case 1:
                    $('#'+this.soundId)[0].play()
                    break;
                case 2:
                    $('#'+this.soundId2)[0].play()
                    break;
            }
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(AlertConfigurationManageScreen),
    template: templateMarkup
};
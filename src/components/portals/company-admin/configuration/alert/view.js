﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenbase";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebrequestAlertConfiguration from "../../../../../app/frameworks/data/apitrackingcore/webRequestAlertConfiguration";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestCustomPOICategory from "../../../../../app/frameworks/data/apitrackingcore/webrequestCustomPOICategory";
import WebRequestCustomAreaCategory from "../../../../../app/frameworks/data/apitrackingcore/webrequestCustomAreaCategory";
import WebRequestProvince from "../../../../../app/frameworks/data/apicore/webRequestProvince";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import { Constants, EntityAssociation, Enums } from "../../../../../app/frameworks/constant/apiConstant";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";

class AlertConfigurationViewScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Configurations_ViewAlertConfiguration")());
        this.bladeSize = BladeSize.Medium;

        this.alertConfigurationId = this.ensureNonObservable(params.alertConfigurationId, -1);
        this.temperatureUnit = ko.observable('');

        this.alertType = ko.observable('');
        this.alertTypeValue = ko.observable('');
        this.alertTypeLevels = ko.observable('');
        this.mailTo = ko.observable('');
        this.ticket = ko.observable('');

        this.alertCategory = ko.observable('');
        this.priority = ko.observable('');
        this.enable = ko.observable('');
        this.enableVia = ko.observable('');
        this.description = ko.observable('');
        this.ticket = ko.observable('');
        this.mdvr = ko.observable('');
        this.maneuverLevel = ko.observable();
        this.insideArea = ko.observable();
        this.insideAreaType = ko.observable();
        this.insideAreaCate = ko.observable();
        this.visibleAreaType = ko.observable(false);
        this.visibleArea = ko.observable(false);
        this.visibleAreaCate = ko.observable(false);
        this.minimumDuration = ko.observable();
        this.typeOption = ko.observable();
        this.notification = ko.observable(null);
        this.visibleNotificationSound  = ko.observable(false);
        this.alertSound = ko.observable(null);

        this.iconAlertSound = 'svg-icon-notification-sound';
        this.soundUrl = this.resolveUrl("~/media/notification.mp3");
        this.soundUrl2 = this.resolveUrl("~/media/sharp.mp3");
        this.soundId = this.id+'_audio1';
        this.soundId2 = this.id+'_audio2';
        
        //Input MaxSpeed MinSpeed MaxRPM MinRPM
        this.inputMinSpeed = ko.observable();
        this.inputMaxSpeed = ko.observable();



        this.inputMinRPM = ko.observable();
        this.inputMaxRPM = ko.observable();

        //unauthorized
        this.allowList = ko.observableArray([]);
        this.notAllowList = ko.observableArray([]);
        this.stopOvertimeList = ko.observableArray([]);

        //Exceed limit driving vehicle
        this.parkingTime = ko.observable();

        //Over Speed
        this.overspeedDelay = ko.observable();
        this.enableRepeatAlert = ko.observable();
        this.visibleUnauthorizedStop = ko.pureComputed(() => {

            var alertType = this.alertTypeValue();

            return (alertType === Enums.ModelData.AlertType.UnauthorizedStop);
        })
        this.visibleMinSpeed = ko.pureComputed(() => {
            var alertType = this.alertTypeValue();
            return alertType === Enums.ModelData.AlertType.GreenBandDriving ||
                   alertType === Enums.ModelData.AlertType.OverRPM ||
                   alertType === Enums.ModelData.AlertType.FreeWheeling;
        });

        this.visibleMaxSpeed = ko.pureComputed(() => {
            var alertType = this.alertTypeValue();
            return  alertType === Enums.ModelData.AlertType.GreenBandDriving ||
                    alertType === Enums.ModelData.AlertType.NoSeatBelt ||
                    alertType === Enums.ModelData.AlertType.FreeWheeling;
        });

        this.visibleMinRPM = ko.pureComputed(() => {
            var alertType = this.alertTypeValue();
            return alertType === Enums.ModelData.AlertType.GreenBandDriving ||
                   alertType === Enums.ModelData.AlertType.FreeWheeling;
        });

        this.visibleMaxRPM = ko.pureComputed(() => {
            var alertType = this.alertTypeValue();
            return alertType === Enums.ModelData.AlertType.GreenBandDriving ||
                   alertType === Enums.ModelData.AlertType.OverRPM;
        });

        //Assets
        this.visibleVehicle = ko.pureComputed(() => {
            return this.alertTypeLevels() && (_.includes(this.alertTypeLevels(), Enums.ModelData.AlertLevel.Vehicle.toString()));
        });
        this.visibleDeliveryMen = ko.pureComputed(() => {
            return this.alertTypeLevels() && (_.includes(this.alertTypeLevels(), Enums.ModelData.AlertLevel.Dispatcher.toString()));
        });
        this.vehicles = ko.observableArray([]);
        this.users = ko.observableArray([]);
        this.orderVehicles = ko.observable([[0, "asc"]]);
        this.orderUsers = ko.observable([[0, "asc"]]);

        //Custom Fields
        this.visibleDecimal = ko.pureComputed(() => {
            var alertType = this.alertTypeValue();
            return (alertType === Enums.ModelData.AlertType.AccRapid
                || alertType === Enums.ModelData.AlertType.DecRapid
                || alertType === Enums.ModelData.AlertType.OverSpeed
                || alertType === Enums.ModelData.AlertType.Swerve
            );
        });
        this.labelDecimal = ko.observable('');
        this.inputDecimal = ko.observable('');

        this.visibleInteger = ko.pureComputed(() => {
            var alertType = this.alertTypeValue();
            return (alertType === Enums.ModelData.AlertType.OverTimePark
                || alertType === Enums.ModelData.AlertType.ExceedLimitDriving
                || alertType === Enums.ModelData.AlertType.ExceedLimitDrivingIn24Hours
                || alertType === Enums.ModelData.AlertType.ExceedLimitDrivingVehicle
            );
        });
        this.labelInteger = ko.observable('');
        this.inputInteger = ko.observable('');

        this.visibleDecimalAllowNegative = ko.pureComputed(() => {
            var alertType = this.alertTypeValue();
            return (alertType === Enums.ModelData.AlertType.OverTemperature
                || alertType === Enums.ModelData.AlertType.UnderTemperature
            );
        });
        this.labelDecimalAllowNegative = ko.observable('');
        this.inputDecimalAllowNegative = ko.observable('');

        this.visibleDecimalLessThan100 = ko.pureComputed(() => {
            var alertType = this.alertTypeValue();
            return (alertType === Enums.ModelData.AlertType.VehicleBatteryLow
                || alertType === Enums.ModelData.AlertType.DeviceBatteryLow
                || alertType === Enums.ModelData.AlertType.UnsFuelDec
            );
        });

        this.labelMoveDurationUnsFuelDec = ko.observable('');
        this.inputMoveDurationUnusualFuel = ko.observable('');

        this.visibleMoveDurationUnusualFuelView = ko.pureComputed(() => {
            var alertType = this.alertTypeValue();
            return (alertType === Enums.ModelData.AlertType.UnsFuelDec);
        });

        this.labelDecimalLessThan100 = ko.observable('');
        this.inputDecimalLessThan100 = ko.observable('');

        this.visibleAlertParkingType = ko.pureComputed(() => {
            var alertType = this.alertTypeValue();
            return (alertType === Enums.ModelData.AlertType.Parking
                || alertType === Enums.ModelData.AlertType.POIParking
            );
        });
        this.alertParkingType = ko.observable('');
        this.alertParkingTypeValue = ko.observable('');
        this.visibleOverTimeParkLimit = ko.pureComputed(() => {
            return this.alertParkingTypeValue() === Enums.ModelData.AlertParkingType.ParkOverTime;
        });
        this.overTimeParkLimit = ko.observable('');

        this.visibleCustomPoi = ko.pureComputed(() => {
            var alertType = this.alertTypeValue();
            return (alertType === Enums.ModelData.AlertType.POIParking || alertType === Enums.ModelData.AlertType.POIInOut);
        });
        this.visibleCustomPoiCategory = ko.pureComputed(() => {
            var alertType = this.alertTypeValue();
            return (alertType === Enums.ModelData.AlertType.POIParking || alertType === Enums.ModelData.AlertType.POIInOut);
        });
        this.customPoi = ko.observable('');
        this.customPoiCategory = ko.observable('');

        this.visibleAreaParkingType = ko.pureComputed(() => {
            var alertType = this.alertTypeValue();
            return (alertType === Enums.ModelData.AlertType.AreaParking);
        });
        this.areaParkingType = ko.observable('');

        this.visibleCustomArea = ko.pureComputed(() => {
            var alertType = this.alertTypeValue();
            return (alertType === Enums.ModelData.AlertType.AreaParking || alertType === Enums.ModelData.AlertType.AreaInOutVehicle);
        });
        this.visibleCustomAreaCategory = ko.pureComputed(() => {
            var alertType = this.alertTypeValue();
            return (alertType === Enums.ModelData.AlertType.AreaParking || alertType === Enums.ModelData.AlertType.AreaInOutVehicle);
        });
        this.customArea = ko.observable('');
        this.customAreaType = ko.observable('');
        this.customAreaCategory = ko.observable('');

        this.visibleDrivingType = ko.pureComputed(() => {
            var alertType = this.alertTypeValue();
            return (alertType === Enums.ModelData.AlertType.POIInOut || alertType === Enums.ModelData.AlertType.AreaInOutVehicle || alertType === Enums.ModelData.AlertType.AreaInOutJob);
        });
        this.drivingType = ko.observable('');

        this.visibleTime = ko.pureComputed(() => {
            var alertType = this.alertTypeValue();
            return (alertType === Enums.ModelData.AlertType.MoveNonWorking ||
                alertType === Enums.ModelData.AlertType.POIInOut ||
                alertType === Enums.ModelData.AlertType.AreaInOutVehicle);
        });

        this.workingPeriodFrom = ko.observable('');
        this.workingPeriodTo = ko.observable('');

        this.visibleCustomAlert = ko.pureComputed(() => {
            var alertType = this.alertTypeValue();
            return (alertType === Enums.ModelData.AlertType.CustomAlert);
        });
        this.customMessage = ko.observable('');
        this.feature = ko.observable('');
        this.featureEventDataType = ko.observable('');
        this.visibleInputDigital = ko.pureComputed(() => {
            return this.featureEventDataType() === Enums.ModelData.EventDataType.Digital;
        });
        this.visibleInputAnalog = ko.pureComputed(() => {
            return this.featureEventDataType() === Enums.ModelData.EventDataType.Analog;
        });
        this.visibleInputText = ko.pureComputed(() => {
            return this.featureEventDataType() === Enums.ModelData.EventDataType.Text;
        });
        this.inputDigital = ko.observable('');
        this.inputMinValue = ko.observable('');
        this.inputMaxValue = ko.observable('');
        this.inputText = ko.observable('');

        this.featureUnit = ko.observable('');;
        this.visibleFeatureUnit = ko.pureComputed(() => {
            var alertType = this.alertTypeValue();
            return (alertType === Enums.ModelData.AlertType.GateClose
                || alertType === Enums.ModelData.AlertType.GateOpen
                || alertType === Enums.ModelData.AlertType.CloseGateInsPOI
                || alertType === Enums.ModelData.AlertType.CloseGateOtsPOI
                || alertType === Enums.ModelData.AlertType.OpenGateInsPOI
                || alertType === Enums.ModelData.AlertType.OpenGateOtsPOI
                || alertType === Enums.ModelData.AlertType.UnderTemperature
                || alertType === Enums.ModelData.AlertType.OverTemperature);
        });

        this.subscribeMessage("ca-configuration-alert-changed", () => {
            this.isBusy(true);
            this.webrequestAlertConfiguration.getAlertConfiguration(this.alertConfigurationId,
                [EntityAssociation.AlertConfiguration.Vehicles,
                EntityAssociation.AlertConfiguration.DeliveryMen,
                EntityAssociation.AlertConfiguration.CustomPOICategory,
                EntityAssociation.AlertConfiguration.CustomPOI,
                EntityAssociation.AlertConfiguration.CustomAreaType,
                EntityAssociation.AlertConfiguration.CustomAreaCategory,
                EntityAssociation.AlertConfiguration.CustomArea,
                EntityAssociation.AlertConfiguration.Feature]
            ).done((alertConfiguration) => {
                if (alertConfiguration.alertTypeValue === Enums.ModelData.AlertType.UnauthorizedStop) {

                    this.setupConditionTable(alertConfiguration);

                }
                this.setupModel(alertConfiguration);
                this.isBusy(false);
            }).fail((e) => {
                this.handleError(e);
                this.isBusy(false);
            });
        });

        this.visibleManeuverLevel = ko.pureComputed(() => {
            return (this.alertTypeValue() == Enums.ModelData.AlertType.Acceleration
                || this.alertTypeValue() == Enums.ModelData.AlertType.Breaking
                || this.alertTypeValue() == Enums.ModelData.AlertType.Turn
                || this.alertTypeValue() == Enums.ModelData.AlertType.WideTurn
                || this.alertTypeValue() == Enums.ModelData.AlertType.SwerveWhileAccelerating
                || this.alertTypeValue() == Enums.ModelData.AlertType.WideSwerveWhileAccelerating
                || this.alertTypeValue() == Enums.ModelData.AlertType.SwerveWhileDecelerating
                || this.alertTypeValue() == Enums.ModelData.AlertType.WideSwerveWhileDecelerating
                || this.alertTypeValue() == Enums.ModelData.AlertType.Roundabout
                || this.alertTypeValue() == Enums.ModelData.AlertType.LaneChange
                || this.alertTypeValue() == Enums.ModelData.AlertType.Bypassing
                || this.alertTypeValue() == Enums.ModelData.AlertType.SpeedBump
                || this.alertTypeValue() == Enums.ModelData.AlertType.AccidentSuspicious
                || this.alertTypeValue() == Enums.ModelData.AlertType.RealAccident);
        });

        this.visibleTurnOption = ko.pureComputed(() => {
            return (this.alertTypeValue() == Enums.ModelData.AlertType.Turn
                || this.alertTypeValue() == Enums.ModelData.AlertType.WideTurn
                || this.alertTypeValue() == Enums.ModelData.AlertType.SwerveWhileAccelerating
                || this.alertTypeValue() == Enums.ModelData.AlertType.SwerveWhileDecelerating
                || this.alertTypeValue() == Enums.ModelData.AlertType.WideSwerveWhileAccelerating
                || this.alertTypeValue() == Enums.ModelData.AlertType.WideSwerveWhileDecelerating);
        });

        this.visibleInfoOverSpeed = ko.pureComputed(() => {
            return this.alertTypeValue() == Enums.ModelData.AlertType.OverSpeed;
        });

        this.visibleParkingTime = ko.pureComputed(() => {
            return this.alertTypeValue() == Enums.ModelData.AlertType.ExceedLimitDrivingVehicle;
        });

        this.isVisibleMinDuration = ko.pureComputed(() => {
            return (this.alertTypeValue() == Enums.ModelData.AlertType.Acceleration
                || this.alertTypeValue() == Enums.ModelData.AlertType.Breaking
                || this.alertTypeValue() == Enums.ModelData.AlertType.Turn
                || this.alertTypeValue() == Enums.ModelData.AlertType.WideTurn
                || this.alertTypeValue() == Enums.ModelData.AlertType.SwerveWhileAccelerating
                || this.alertTypeValue() == Enums.ModelData.AlertType.WideSwerveWhileAccelerating
                || this.alertTypeValue() == Enums.ModelData.AlertType.SwerveWhileDecelerating
                || this.alertTypeValue() == Enums.ModelData.AlertType.WideSwerveWhileDecelerating
                || this.alertTypeValue() == Enums.ModelData.AlertType.Roundabout
                || this.alertTypeValue() == Enums.ModelData.AlertType.LaneChange
                || this.alertTypeValue() == Enums.ModelData.AlertType.Bypassing
                || this.alertTypeValue() == Enums.ModelData.AlertType.SpeedBump
                || this.alertTypeValue() == Enums.ModelData.AlertType.AccidentSuspicious
                || this.alertTypeValue() == Enums.ModelData.AlertType.RealAccident
                || this.alertTypeValue() == Enums.ModelData.AlertType.FreeWheeling);
        });

    }
    /**
     * Get WebRequest specific for Alert Configuration module in Web API access.
     * @readonly
     */
    get webrequestAlertConfiguration() {
        return WebrequestAlertConfiguration.getInstance();
    }
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }
    get webRequestCustomAreaCategory() {
        return WebRequestCustomAreaCategory.getInstance();
    }
    get webRequestCustomPOICategory() {
        return WebRequestCustomPOICategory.getInstance();
    }
    get webRequestProvince() {
        return WebRequestProvince.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var dfd = $.Deferred();

        this.webrequestAlertConfiguration.getAlertConfiguration(this.alertConfigurationId,
            [EntityAssociation.AlertConfiguration.Vehicles,
            EntityAssociation.AlertConfiguration.DeliveryMen,
            EntityAssociation.AlertConfiguration.CustomPOICategory,
            EntityAssociation.AlertConfiguration.CustomPOI,
            EntityAssociation.AlertConfiguration.CustomAreaType,
            EntityAssociation.AlertConfiguration.CustomAreaCategory,
            EntityAssociation.AlertConfiguration.CustomArea,
            EntityAssociation.AlertConfiguration.Feature]
        ).done((alertConfiguration) => {

            if (alertConfiguration.alertTypeValue === Enums.ModelData.AlertType.UnauthorizedStop) {

                this.setupConditionTable(alertConfiguration);

            }



            this.setupModel(alertConfiguration);

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.UpdateAlertManagement)) {
            commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.DeleteAlertManagement)) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdUpdate":
                this.navigate("ca-configuration-alert-manage", {
                    mode: Screen.SCREEN_MODE_UPDATE, alertConfigurationId: this.alertConfigurationId,
                    stopProvinceList: this.stopOvertimeList(), allowList: this.allowList(), notAllowList: this.notAllowList()
                });
                break;
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M055")(),
                    BladeDialog.DIALOG_YESNO).done((button) => {
                        switch (button) {
                            case BladeDialog.BUTTON_YES:
                                this.isBusy(true);

                                this.webrequestAlertConfiguration.deleteAlertConfiguration(this.alertConfigurationId).done(() => {
                                    this.isBusy(false);

                                    this.publishMessage("ca-configuration-alert-deleted");
                                    this.close(true);
                                }).fail((e) => {
                                    this.handleError(e);
                                    this.isBusy(false);
                                });
                                break;
                        }
                    });
                break;
        }
    }
    /**
     * Setup Alert Configuration Model
     *
     */
    setupModel(alertConfiguration) {
        if (alertConfiguration) {
            this.temperatureUnit(alertConfiguration.temperatureUnitSymbol);
            this.alertTypeValue(alertConfiguration.alertTypeValue);
            this.alertType(alertConfiguration.alertTypeDisplayName);
            this.alertTypeLevels(alertConfiguration.alertLevels);
            this.alertCategory(alertConfiguration.alertCategoryDisplayName);
            this.priority(alertConfiguration.formatPriority);
            this.enable(alertConfiguration.formatEnable);
            this.ticket(alertConfiguration.formatEnableAutoTicket);
            this.mdvr(alertConfiguration.formatEnableMDVR);
            this.mailTo(alertConfiguration.mailTo);
            this.maneuverLevel(alertConfiguration.maneuverLevelDisplayName);
            this.minimumDuration(alertConfiguration.minDuration);
            this.typeOption(alertConfiguration.turnOptionDisplayName);

            // Alert Sound
            this.visibleNotificationSound(alertConfiguration.enableWebsiteAlert);
            this.notification(alertConfiguration.alertSoundDisplayName);
            this.alertSound(alertConfiguration.alertSound);

            var enableVia = [];
            // debugger
            if (alertConfiguration.enableWebsiteAlert) {
                enableVia.push(this.i18n("Common_Website")());
            }
            if (alertConfiguration.enableEmailAlert) {
                enableVia.push(this.i18n("Common_Email")());
            }
            if (alertConfiguration.enableSmsAlert) {
                enableVia.push(this.i18n("Common_SMS")());
            }
            if (alertConfiguration.enableInterfaceAlert) {
                enableVia.push(this.i18n("Configurations_Interface")());
            }
            if (alertConfiguration.enableDeviceAlert) {
                enableVia.push(this.i18n("Common_Device")());
            }
            if (alertConfiguration.enableReport) {
                enableVia.push(this.i18n("Configurations_Report")());
            }

            //if (enableVia.length > 0) {
                this.enableVia(enableVia.join('<br />'));
            //}

            this.description(alertConfiguration.description);

            //Assests
            this.vehicles(alertConfiguration.vehicles);
            this.users(alertConfiguration.deliveryMen);

            if (alertConfiguration.customAreaCategory) {
                this.customAreaCategory(alertConfiguration.customAreaCategory.name);
            }
            else {
                this.customAreaCategory(null);
            }

            //Custom Fields
            switch (alertConfiguration.alertTypeValue) {
                case Enums.ModelData.AlertType.GateClose:
                case Enums.ModelData.AlertType.GateOpen:
                case Enums.ModelData.AlertType.CloseGateInsPOI:
                case Enums.ModelData.AlertType.CloseGateOtsPOI:
                case Enums.ModelData.AlertType.OpenGateInsPOI:
                case Enums.ModelData.AlertType.OpenGateOtsPOI:
                    this.featureUnit(alertConfiguration.featureName);
                    break;
                case Enums.ModelData.AlertType.AccRapid:
                    this.labelDecimal(this.i18n("Configurations_ACCRapid")());
                    this.inputDecimal(alertConfiguration.formatAccelerateLimit);
                    break;
                case Enums.ModelData.AlertType.VehicleBatteryLow:
                    this.labelDecimalLessThan100(this.i18n("Configurations_VehicleBatteryLow")());
                    this.inputDecimalLessThan100(alertConfiguration.formatVehicleBatteryLowLimit);
                    break;
                case Enums.ModelData.AlertType.DeviceBatteryLow:
                    this.labelDecimalLessThan100(this.i18n("Configurations_DeviceBatteryLow")());
                    this.inputDecimalLessThan100(alertConfiguration.formatDeviceBatteryLowLimit);
                    break;
                case Enums.ModelData.AlertType.DecRapid:
                    this.labelDecimal(this.i18n("Configurations_DecRapid")());
                    this.inputDecimal(alertConfiguration.formatDecelerateLimit);
                    break;
                case Enums.ModelData.AlertType.OverSpeed:
                    this.labelDecimal(this.i18n("Configurations_OverSpeed")());
                    this.inputDecimal(alertConfiguration.formatSpeedLimit);
                    this.overspeedDelay(alertConfiguration.formatOverSpeedDelay);

                    let checkFirstTime = alertConfiguration.overSpeedFirstTime ? this.i18n("Configurations_OnlyFirstTime")() : null;
                    this.enableRepeatAlert(checkFirstTime);

                    if (alertConfiguration.customPoiCategory) {
                        this.customPoiCategory(alertConfiguration.customPoiCategory.name);
                    }
                    else {
                        this.customPoiCategory(null);
                    }
                    if (alertConfiguration.inAreaCategory) {
                        this.visibleAreaCate(true);
                        this.visibleArea(false);
                        this.visibleAreaType(false);
                        this.insideAreaCate(alertConfiguration.inAreaCategory.name);
                    }
                    else if (alertConfiguration.inAreaId) {
                        this.visibleArea(true);
                        this.visibleAreaCate(false);
                        this.visibleAreaType(false);
                        this.insideArea(alertConfiguration.inArea.name);
                    }
                    else if (alertConfiguration.inAreaTypeId) {
                        this.visibleAreaType(true);
                        this.visibleArea(false);
                        this.visibleAreaCate(false);
                        this.insideAreaType(alertConfiguration.inAreaType.displayName);
                    }
                    this.workingPeriodFrom(alertConfiguration.formatWorkingPeriodFrom);
                    this.workingPeriodTo(alertConfiguration.formatWorkingPeriodTo);
                    break;
                case Enums.ModelData.AlertType.OverTemperature:
                    this.labelDecimalAllowNegative(this.i18n("Configurations_OverTemperature", [this.temperatureUnit()]));
                    this.inputDecimalAllowNegative(alertConfiguration.formatTemperatureMaxLimit);
                    this.featureUnit(alertConfiguration.featureName);
                    break;
                case Enums.ModelData.AlertType.Swerve:
                    this.labelDecimal(this.i18n("Configurations_SwerveLimit")());
                    this.inputDecimal(alertConfiguration.formatSwerveLimit);
                    break;
                case Enums.ModelData.AlertType.UnderTemperature:
                    this.labelDecimalAllowNegative(this.i18n("Configurations_UnderTemperature", [this.temperatureUnit()]));
                    this.inputDecimalAllowNegative(alertConfiguration.formatTemperatureMinLimit);
                    this.featureUnit(alertConfiguration.featureName);
                    break;
                case Enums.ModelData.AlertType.UnsFuelDec:
                    this.labelDecimalLessThan100(this.i18n("Configurations_UnsFuelDec")());
                    this.labelMoveDurationUnsFuelDec(this.i18n("Configurations_MoveDuration")());
                    this.inputDecimalLessThan100(alertConfiguration.formatFuelDecreaseLimit);
                    this.inputMoveDurationUnusualFuel(alertConfiguration.movementDuration);
                    break;
                case Enums.ModelData.AlertType.OverTimePark:
                    this.labelInteger(this.i18n("Configurations_OverTimePark")());
                    this.inputInteger(alertConfiguration.formatOverTimeParkLimit);
                    break;
                case Enums.ModelData.AlertType.Parking:
                    this.alertParkingTypeValue(alertConfiguration.alertParkingType);
                    this.alertParkingType(alertConfiguration.alertParkingTypeDisplayName);
                    this.overTimeParkLimit(alertConfiguration.formatOverTimeParkLimit);
                    break;
                case Enums.ModelData.AlertType.POIParking:
                    if (alertConfiguration.customPoi) {
                        this.customPoi(alertConfiguration.customPoi.name);
                    }
                    else {
                        this.customPoi(null);
                    }
                    if (alertConfiguration.customPoiCategory) {
                        this.customPoiCategory(alertConfiguration.customPoiCategory.name);
                    }
                    else {
                        this.customPoiCategory(null);
                    }
                    this.alertParkingTypeValue(alertConfiguration.alertParkingType);
                    this.alertParkingType(alertConfiguration.alertParkingTypeDisplayName);
                    this.overTimeParkLimit(alertConfiguration.formatOverTimeParkLimit);
                    break;
                case Enums.ModelData.AlertType.POIInOut:
                    this.workingPeriodFrom(alertConfiguration.formatWorkingPeriodFrom);
                    this.workingPeriodTo(alertConfiguration.formatWorkingPeriodTo);

                    if (alertConfiguration.customPoi) {
                        this.customPoi(alertConfiguration.customPoi.name);
                    }
                    else {
                        this.customPoi(null);
                    }
                    if (alertConfiguration.customPoiCategory) {
                        this.customPoiCategory(alertConfiguration.customPoiCategory.name);
                    }
                    else {
                        this.customPoiCategory(null);
                    }
                    this.drivingType(alertConfiguration.drivingTypeDisplayName);
                    break;
                case Enums.ModelData.AlertType.AreaParking:
                    if (alertConfiguration.customArea) {
                        this.customArea(alertConfiguration.customArea.name);
                    }
                    else {
                        this.customArea(null);
                    }
                    if (alertConfiguration.customAreaType) {
                        this.customAreaType(alertConfiguration.customAreaType.displayName);
                    }
                    else {
                        this.customAreaType(null);
                    }
                    this.areaParkingType(alertConfiguration.areaParkingTypeDisplayName);
                    break;
                case Enums.ModelData.AlertType.AreaInOutVehicle:
                    if (alertConfiguration.customArea) {
                        this.customArea(alertConfiguration.customArea.name);
                    }
                    else {
                        this.customArea(null);
                    }
                    if (alertConfiguration.customAreaType) {
                        this.customAreaType(alertConfiguration.customAreaType.displayName);
                    }
                    else {
                        this.customAreaType(null);
                    }
                    this.workingPeriodFrom(alertConfiguration.formatWorkingPeriodFrom);
                    this.workingPeriodTo(alertConfiguration.formatWorkingPeriodTo);
                    this.drivingType(alertConfiguration.drivingTypeDisplayName);
                    break;
                case Enums.ModelData.AlertType.AreaInOutJob:
                    this.drivingType(alertConfiguration.drivingTypeDisplayName);
                    break;
                case Enums.ModelData.AlertType.MoveNonWorking:
                    this.workingPeriodFrom(alertConfiguration.formatWorkingPeriodFrom);
                    this.workingPeriodTo(alertConfiguration.formatWorkingPeriodTo);
                    break;
                case Enums.ModelData.AlertType.ExceedLimitDriving:
                    this.labelInteger(this.i18n("Configurations_ExceedLimitDriving")());
                    this.inputInteger(alertConfiguration.formatDrivingTimeLimit);
                    break;
                case Enums.ModelData.AlertType.CustomAlert:
                    this.customMessage(alertConfiguration.customMessage);
                    this.feature(alertConfiguration.featureName);
                    this.featureEventDataType(alertConfiguration.featureEventDataType);
                    this.inputDigital(alertConfiguration.matchDigitalValue);
                    this.inputMinValue(alertConfiguration.formatMinAnalogValue);
                    this.inputMaxValue(alertConfiguration.formatMaxAnalogValue);
                    this.inputText(alertConfiguration.matchTextValue);
                    break;
                case Enums.ModelData.AlertType.ExceedLimitDrivingIn24Hours:
                    this.labelInteger(this.i18n("Configurations_ExceedLimitDrivingIn24Hours")());
                    this.inputInteger(alertConfiguration.formatDrivingTimeLimit);
                    break;
                case Enums.ModelData.AlertType.GreenBandDriving:
                    this.inputMinSpeed(alertConfiguration.minSpeed);
                    this.inputMaxSpeed(alertConfiguration.maxSpeed);
                    this.inputMinRPM(alertConfiguration.minAnalogValue);
                    this.inputMaxRPM(alertConfiguration.maxAnalogValue);
                    break;
                case Enums.ModelData.AlertType.OverRPM:
                    this.inputMinSpeed(alertConfiguration.minSpeed);
                    this.inputMaxRPM(alertConfiguration.maxAnalogValue);
                    break;
                case Enums.ModelData.AlertType.NoSeatBelt:
                    this.inputMaxSpeed(alertConfiguration.maxSpeed);
                    break;
                case Enums.ModelData.AlertType.ExceedLimitDrivingVehicle:
                    this.labelInteger(this.i18n("Configurations_ExceedLimitDriving")());
                    this.parkingTime(alertConfiguration.formatMinParkTime);
                    this.inputInteger(alertConfiguration.formatDrivingTimeLimit);
                    break;
                case Enums.ModelData.AlertType.FreeWheeling:
                    this.inputMinSpeed(alertConfiguration.minSpeed);
                    this.inputMaxSpeed(alertConfiguration.maxSpeed);
                    this.inputMinRPM(alertConfiguration.minAnalogValue);
                    break;

            }
        }
    }

    getConditionObj(alertConfiguration, configTypeForCheck) {
        // แยกข้อมูล Allow,NotAllow,StopProvince

        let conditionObj = []
        let unauthorizedData = alertConfiguration.unauthorizedStopInfo;

        if (configTypeForCheck == 1) {

            conditionObj = unauthorizedData.filter((items) => {

                return items.configType == 1
            })

        } else if (configTypeForCheck == 2) {

            conditionObj = unauthorizedData.filter((items) => {

                return items.configType == 2
            })

        } else if (configTypeForCheck == 3) {

            conditionObj = unauthorizedData.filter((items) => {

                return items.configType == 3
            })
        }

        return conditionObj;
    }

    setupConditionTable(alertConfiguration) {

        let allowObj = this.getConditionObj(alertConfiguration, 1);
        let notAllowObj = this.getConditionObj(alertConfiguration, 2);
        let stopProvinceObj = this.getConditionObj(alertConfiguration, 3);
        let enumCategoryTypeFillter = {}

        var dfd = $.Deferred();

        var enumConditionFilter = {
            types: [Enums.ModelData.EnumResourceType.AlertUnauthorizedCondition],
            sortingColumns: DefaultSorting.EnumResource
        };
        var enumSpeedTypeFilter = {
            types: [Enums.ModelData.EnumResourceType.AlertUnauthorizedSpeedType],
            // values: this.getReportTemplateType(),
            sortingColumns: DefaultSorting.EnumResource
        };
        var provinceFilter = {
            "countryIds": [1]

        }


        var conditionDfd = this.webRequestEnumResource.listEnumResource(enumConditionFilter);
        var categoryTypeCustomPoiDfd = this.webRequestEnumResource.listEnumResource({ types: [Enums.ModelData.EnumResourceType.PoiCategoryType] });
        var categoryTypeCustomAreaDfd = this.webRequestEnumResource.listEnumResource({ types: [Enums.ModelData.EnumResourceType.AreaCategoryType] });

        var categoryNameDfd = this.webRequestCustomPOICategory.listCustomPOICategorySummary();

        var speedTypeDfd = this.webRequestEnumResource.listEnumResource(enumSpeedTypeFilter);
        var provinceDfd = this.webRequestProvince.listProvince(provinceFilter);

        var customPoiCategoryDfd = this.webRequestCustomPOICategory.listCustomPOICategorySummary();
        var customAreaCategoryDfd = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary();

        $.when(conditionDfd, categoryTypeCustomPoiDfd, categoryTypeCustomAreaDfd, speedTypeDfd, provinceDfd, customPoiCategoryDfd, customAreaCategoryDfd).done((conditionRes, categoryTypeCustomPoiRes, categoryTypeCustomAreaRes, speedTypeRes, provinceRes, customPoiCategoryRes, customAreaCategoryRes) => {


            /////////////// เพิ่ม ConditionForDisplay&conditionValue (Allow&NotAllow) และ dataId ของทั้งสามตาราง /////////
            conditionRes.items.forEach((conditionItems) => {
                allowObj.forEach((allowItems) => {

                    if (allowItems.condition == conditionItems.value) {
                        allowItems.conditionForDisplay = conditionItems.displayName
                        allowItems.dataId = allowItems.id
                    }
                    allowItems.conditionValue = allowItems.condition

                })
            })

            conditionRes.items.forEach((conditionItems) => {
                notAllowObj.forEach((notAllowItems) => {

                    if (notAllowItems.condition == conditionItems.value) {
                        notAllowItems.conditionForDisplay = conditionItems.displayName
                        notAllowItems.dataId = notAllowItems.id
                    }
                    notAllowItems.conditionValue = notAllowItems.condition

                })
            })

            stopProvinceObj.forEach((stopProvinceItems) => { // เพิ่มDataIdให้stopProvince

                stopProvinceItems.dataId = stopProvinceItems.id

            })
            ////////////////////////////////////////////////////////

            /////////////// เพิ่ม Category Type (Allow&NotAllow) ///////////////////// 
            ///////////     Allow     ///////////////////////////
            allowObj.forEach((allowItems, index) => {

                if (allowItems.condition == 1) { //customPOI
                    categoryTypeCustomPoiRes.items.forEach((customPoiItems) => {

                        if (allowItems.poiCategoryType == customPoiItems.value) { //แก้ categoryType
                            allowItems.categoryTypeName = customPoiItems.displayName
                        }


                    })

                    allowItems.categoryTypeName = (allowItems.categoryTypeName) ? allowItems.categoryTypeName : '-'; //Mock
                    // allowItems.categoryTypeValue = index+1; //Mock
                    allowItems.categoryTypeValue = (allowItems.poiCategoryType) ? allowItems.poiCategoryType : null


                } else if (allowItems.condition == 2) { //customArea

                    categoryTypeCustomAreaRes.items.forEach((customAreaItems) => {

                        if (allowItems.areaCategoryType == customAreaItems.value) {
                            allowItems.categoryTypeName = customAreaItems.displayName
                        }

                    })

                    // allowItems.categoryTypeName = '-'; //Mock
                    // allowItems.categoryTypeValue = index+1; //Mock
                    allowItems.categoryTypeValue = (allowItems.areaCategoryType) ? allowItems.areaCategoryType : null

                }

            })
            ////////////////////////////////////////////////////
            ///////////// NOT ALLOW /////////////////////

            notAllowObj.forEach((notAllowItems, index) => {

                if (notAllowItems.condition == 1) { //customPOI
                    categoryTypeCustomPoiRes.items.forEach((customPoiItems) => {

                        if (notAllowItems.poiCategoryType == customPoiItems.value) {
                            notAllowItems.categoryTypeName = customPoiItems.displayName
                        }
                    })

                    notAllowItems.categoryTypeName = (notAllowItems.categoryTypeName) ? notAllowItems.categoryTypeName : '-'; //Mock

                    notAllowItems.categoryTypeValue = (notAllowItems.poiCategoryType) ? notAllowItems.poiCategoryType : null

                } else if (notAllowItems.condition == 2) { //customArea

                    categoryTypeCustomAreaRes.items.forEach((customAreaItems) => {

                        if (notAllowItems.areaCategoryType == customAreaItems.value) { //แก้ categoryType
                            notAllowItems.categoryTypeName = customAreaItems.displayName
                        }
                    })
                    notAllowItems.categoryTypeName = (notAllowItems.categoryTypeName) ? notAllowItems.categoryTypeName : '-'; //Mock

                    notAllowItems.categoryTypeValue = (notAllowItems.areaCategoryType) ? notAllowItems.areaCategoryType : null
                }


            })

            ////////////////////////////////////////////////////



            ///////////////////////////////////////////////////////

            ////////////// เพิ่ม Category Name && CategoryName Id (Allow&NotAllow)  /////////////////////


            //////////////////// Allow ///////////////////////////////////


            let categoryNameAllowLstDfd;

            allowObj.forEach((allowItems) => {
                let allowCategoryNameList = []
                if (allowItems.condition == 1) {
                    categoryNameAllowLstDfd = customPoiCategoryRes.items

                } else if (allowItems.condition == 2) {
                    categoryNameAllowLstDfd = customAreaCategoryRes.items
                }

                allowItems.customIds.forEach((customIdItems) => {
                    categoryNameAllowLstDfd.forEach((categoryNameResItems) => {

                        if (customIdItems == categoryNameResItems.id) {
                            allowCategoryNameList.push(categoryNameResItems.name)
                        }
                    })
                })

                allowItems.categoryNameLst = allowCategoryNameList
                allowItems.categoryNameId = allowItems.customIds
            })


            ////////////////////////////////////////////////

            /////////////// Not Allow ////////////////////
            let categoryNameNotAllowLstDfd

            notAllowObj.forEach((notAllowItems) => {
                let notAllowCategoryNameList = []
                if (notAllowItems.condition == 1) {
                    categoryNameNotAllowLstDfd = customPoiCategoryRes.items
                } else if (notAllowItems.condition == 2) {
                    categoryNameNotAllowLstDfd = customAreaCategoryRes.items
                }

                notAllowItems.customIds.forEach((customIdItems) => {
                    categoryNameNotAllowLstDfd.forEach((categoryNameResItems) => {
                        if (customIdItems == categoryNameResItems.id) {
                            notAllowCategoryNameList.push(categoryNameResItems.name)
                        }
                    })
                })

                notAllowItems.categoryNameLst = notAllowCategoryNameList
                notAllowItems.categoryNameId = notAllowItems.customIds



            })


            /////////////////////////////////////////////

            /////////////////////////////////////////////////////

            ////////////// เพิ่ม Duration (Allow & StopProvince) ///////////////////

            ///////////////// Allow ////////////////////////////////

            allowObj.forEach((allowItems) => {

                if (allowItems.duration == 0) {

                    allowItems.parkingDurationForDisplay = this.i18n("Unauthorized_Stop_Unlimited")()
                    allowItems.parkingType = "Unlimited"
                } else {

                    allowItems.parkingDurationForDisplay = this.i18n("Unauthorized_Stop_Limited")() + " " + allowItems.duration + " " + this.i18n("Unathorized_Stop_Minutes")() //`Limited ${allowItems.duration} minutes`
                    allowItems.parkingType = "Limited"
                }

            })

            ////////////////////////////////////////////////////////
            ///////////////////////////Stop Province///////////////////
            stopProvinceObj.forEach((stopProvinceItems) => {

                stopProvinceItems.stopDuration = stopProvinceItems.duration + " " + this.i18n("Unathorized_Stop_Minutes")()

            })
            ///////////////////////////////////////////////////////////

            //////////////////////////////////////////////////////////////////////

            ///////////// เพิ่ม Speed Type (Not Allow) ////////////////////////////


            speedTypeRes.items.forEach((speedTypeItems) => {
                notAllowObj.forEach((notAllowItems) => {
                    if (notAllowItems.speedType == speedTypeItems.value) {
                        notAllowItems.speedTypeValue = speedTypeItems.value
                        notAllowItems.speedTypeForDisplay = speedTypeItems.displayName
                    }
                })
            })

            /////////////////////////////////////////////////////////////////////

            ////////////// เพิ่ม Province Name (Stop Province) /////////////////////


            stopProvinceObj.forEach((stopProvinceItems) => {
                let provinceName = []
                stopProvinceItems.provinceIds.forEach((provinceIdItems) => {
                    provinceRes.items.forEach((provinceResItems) => {
                        if (provinceIdItems == provinceResItems.id) {
                            provinceName.push(provinceResItems.name)
                        }
                    })
                })

                stopProvinceItems.selectedProvinceForDisplay = provinceName
                stopProvinceItems.selectedProvinceId = stopProvinceItems.provinceIds

            })


            //////////////////////////////////////////////////////////////////////

            /////////// เพิ่ม selected region (Stop Province) //////////////////////////////////
            stopProvinceObj.forEach((stopProvinceItems) => {

                stopProvinceItems.selectedRegion = stopProvinceItems.regionId

            })
            /////////////////////////////////////////////////////////////////

            this.allowList(allowObj)
            this.notAllowList(notAllowObj)
            this.stopOvertimeList(stopProvinceObj)

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });


        return dfd;

    }

    playAlertSound(){
        if(this.alertSound()){
            switch (this.alertSound()) {
                case 1:
                    $('#'+this.soundId)[0].play()
                    break;
                case 2:
                    $('#'+this.soundId2)[0].play()
                    break;
            }
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(AlertConfigurationViewScreen),
    template: templateMarkup
};
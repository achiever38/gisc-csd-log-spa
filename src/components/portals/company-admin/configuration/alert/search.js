﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import WebRequestAlertType from "../../../../../app/frameworks/data/apitrackingcore/webRequestAlertType";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import {EntityAssociation, Enums} from "../../../../../app/frameworks/constant/apiConstant";
import {AlertConfigurationFilter, AlertTypeFilter} from "./infos";

class AlertConfigurationSearchScreen extends ScreenBase {
    constructor(params) {
        super(params);
    
        this.bladeTitle(this.i18n("Common_Search")());

        this.searchFilter = this.ensureNonObservable($.extend(true, {}, params.searchFilter));

        // Alert Types
        this.selectedAlertType = ko.observable();
        this.alertTypes = ko.observableArray([]);
        
        // Enable field.
        this.selectedEnable = ko.observable();
        this.enableOptions = ScreenHelper.createYesNoObservableArray();
    }
    /**
     * Get WebRequest specific for Alert Type module in Web API access.
     * @readonly
     */
    get webRequestAlertType() {
        return WebRequestAlertType.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();
        this.webRequestAlertType.listAlertType(new AlertTypeFilter()).done((r)=> {
            this.alertTypes(r["items"]);
            if(this.searchFilter.alertTypeIds && this.searchFilter.alertTypeIds.length > 0){
                this.selectedAlertType(ScreenHelper.findOptionByProperty(this.alertTypes, "id", this.searchFilter.alertTypeIds[0]));
            }

            if(this.searchFilter.enable){
                this.selectedEnable(ScreenHelper.findOptionByProperty(this.enableOptions, "value", this.searchFilter.enable));
            }
            
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSearch", this.i18n("Common_Search")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdClear", this.i18n("Common_Clear")(), "svg-cmd-clear"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSearch") {
            this.searchFilter = new AlertConfigurationFilter(
                this.selectedAlertType() ? this.selectedAlertType().id : null,
                this.selectedEnable() ? this.selectedEnable().value : null
            );
            this.publishMessage("ca-configuration-alert-search-filter-changed", this.searchFilter);
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdClear") {
            this.selectedAlertType(null);
            this.selectedEnable(null);
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(AlertConfigurationSearchScreen),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import {
    Constants,
    Enums,
    EntityAssociation
} from "../../../../../app/frameworks/constant/apiConstant";
import FileUploadModel from "../../../../../components/controls/gisc-ui/fileupload/fileuploadModel";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestPoiIcon from "../../../../../app/frameworks/data/apitrackingcore/webRequestPoiIcon";

const width = 32;
const height = 32;

class ConfigurationPoiIconManageScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties

        this.mode = this.ensureNonObservable(params.mode);
        this.companyId = WebConfig.userSession.currentCompanyId;

        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("Configurations_CreatePOIIcon")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("Configurations_UpdatePOIIcon")());
                break;
        }

        this.id = this.ensureNonObservable(params.iconId, 0);
        this.name = ko.observable("");
        this.offsetX = ko.observable("");
        this.offsetY = ko.observable("");

        this.icon = ko.observable();
        this.iconId = ko.observable();
        this.iconName = ko.observable();
        this.iconSelectedFile = ko.observable();

        this.iconUrl = ko.pureComputed(() => {
            if (this.icon() && this.icon().infoStatus !== Enums.InfoStatus.Delete) {
                return this.icon().fileUrl;
            } else {
                return this.resolveUrl("~/images/upload-img-placeholder-100x100.jpg");
            }
        });

        this.isUpload = ko.pureComputed(() => {
            return this.icon() && this.icon().infoStatus !== Enums.InfoStatus.Delete;
        });
    }

    get webRequestPoiIcon() {
        return WebRequestPoiIcon.getInstance();
    }

    /**
     * 
     * Hook on fileupload before upload
     * @param {any} e
     */
    onBeforeUpload(isValidToSubmit) {
        if (!isValidToSubmit) {
            this.icon(null);
            this.iconName(null);
        }
    }

     /**
     * To remove upload icon.
     * 
     * 
     * @memberOf ConfigurationPoiIconManageScreen
     */
    onRemoveFile(){
        this.icon(null);
    }

     /**
     * Do something when fileupload fail.
     * 
     * @param {any} e
     * 
     * @memberOf ConfigurationPoiIconManageScreen
     */
    onUploadFail(e) {
        this.handleError(e);
    }
    
    /**
     * 
     * 
     * @param {any} data
     * 
     * @memberOf ConfigurationPoiIconManageScreen
     */
    onUploadSuccess(data) {
        if (data && data.length > 0) {
            var newIcon = data[0];
            newIcon.infoStatus = this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update;
            newIcon.id = this.iconId() || 0;
            this.icon(newIcon);
        }
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var dfd = $.Deferred();

        var d1 = (this.mode === Screen.SCREEN_MODE_UPDATE) ? this.webRequestPoiIcon.getPoiIcon(this.id, [EntityAssociation.PoiIcon.Image]) : null;

        $.when(d1).done((poiIcon) => {
            if (poiIcon) {
                // debugger
                this.name(poiIcon.name);
                this.icon(poiIcon.image);
                this.iconId(poiIcon.image.id);
                this.iconName(poiIcon.image.fileName);
                this.offsetX(poiIcon.offsetX || "");
                this.offsetY(poiIcon.offsetY || "");
                var file = new FileUploadModel();
                file.name = poiIcon.image.fileName;
                file.extension = "png";
                file.imageWidth = poiIcon.width;
                file.imageHeight = poiIcon.height;
                this.iconSelectedFile(file);

            }
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    setupExtend() {

        //trackChange
        this.icon.extend({
            trackChange: true
        });
        this.name.extend({
            trackChange: true
        });
        this.offsetX.extend({
            trackChange: true
        });
        this.offsetY.extend({
            trackChange: true
        });

        //validation
        this.iconSelectedFile.extend({
            fileRequired: false,
            fileExtension: ['png'],
            fileSize: 4,
            fileImageDimension: [width, height, 'equal']
        });

        this.name.extend({
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });

        this.validationModel = ko.validatedObservable({
            iconSelectedFile: this.iconSelectedFile,
            name: this.name
        });
    }



    /**
     * 
     * @public
     * @param {any} actions
     */
    buildActionBar(actions) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.POIIconsManagement_Company)) {
            actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
            actions.push(this.createActionCancel());
        }
    }

    generateModel() {
        var model = {
            name: this.name(),
            image: this.icon(),
            offsetX: this.offsetX(),
            offsetY: this.offsetY(),
            width: width,
            height: height,
            companyId: this.companyId,
            infoStatus: this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update,
            id: this.id,
        };

        return model;
    }

    /**
     * 
     * @public
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id == "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
            
            this.isBusy(true);

            var model = this.generateModel();

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:

                    this.webRequestPoiIcon.createPoiIcon(model, true).done((response) => {
                        this.isBusy(false);

                        this.publishMessage("ca-configuration-poi-icon-change", response.id);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);

                        this.handleError(e);
                    });

                    break;
                case Screen.SCREEN_MODE_UPDATE:

                    this.webRequestPoiIcon.updatePoiIcon(model, true).done((response) => {
                        this.isBusy(false);

                        this.publishMessage("ca-configuration-poi-icon-change", response.id);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);

                        this.handleError(e);
                    });

                    break;
            }
        }
    }

    onUnload() {}

}

export default {
    viewModel: ScreenBase.createFactory(ConfigurationPoiIconManageScreen),
    template: templateMarkup
};
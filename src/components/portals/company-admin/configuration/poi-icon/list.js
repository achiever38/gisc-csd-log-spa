﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import {PoiIconFilter} from "./poiInfos";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestPoiIcon from "../../../../../app/frameworks/data/apitrackingcore/webRequestPoiIcon";
import { Constants, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";

class ConfigurationPoiIconListScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties

        this.bladeTitle(this.i18n("Configurations_POIIcons")());
        this.bladeSize = BladeSize.Medium;

        this.filterText = ko.observable();
        this.selectedIcon = ko.observable();
        this.recentChangedRowIds = ko.observableArray();
        this.order = ko.observable([[1, "asc"]]);

        this.filter = new PoiIconFilter();

        this.poiIcons = ko.observableArray();

        // Determine if selecting action is legal, return false to cancel selecting event.
        this._selectingRowHandler = (poiIcon) => {
            if (poiIcon) {
                return this.navigate("ca-configuration-poi-icon-view", {
                    iconId: poiIcon.id
                });
            }
            return false;
        };

        this.subscribeMessage("ca-configuration-poi-icon-change", (iconId) => {

            this.isBusy(true);
            this.webRequestPoiIcon.listPoiIconSummary(this.filter).done((r)=> {
                 this.poiIcons.replaceAll(r["items"]);
                 this.recentChangedRowIds.replaceAll([iconId]);
                 this.isBusy(false);
            }).fail((e)=> {
                this.isBusy(false);
            });
        });

         this.subscribeMessage("ca-configuration-poi-icon-deleted", (iconId) => {

            this.isBusy(true);
            this.webRequestPoiIcon.listPoiIconSummary(this.filter).done((r)=> {
                 this.poiIcons.replaceAll(r["items"]);
                 this.recentChangedRowIds.replaceAll([iconId]);
                 this.isBusy(false);
            }).fail((e)=> {
                this.isBusy(false);
            });
        });

    }

    get webRequestPoiIcon() {
        return WebRequestPoiIcon.getInstance();
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var dfd = $.Deferred();
        this.webRequestPoiIcon.listPoiIconSummary(this.filter).done((response) => {
            var poiIcons = response["items"];
            this.poiIcons(poiIcons);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;
    }

    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.POIIconsManagement_Company)) {
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }

    }

    onCommandClick(sender) {
        if (sender.id == "cmdCreate") {
            this.navigate("ca-configuration-poi-icon-manage", {
                mode: "create"
            });
            this.selectedIcon(null);
        }
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedIcon(null);
    }

    renderIconColumn(data, type, row) {
        if (data) {
            let node = '<img src="' + data + '"/>';
            return node;
        }
    }

    onUnload() {}
}



export default {
    viewModel: ScreenBase.createFactory(ConfigurationPoiIconListScreen),
    template: templateMarkup
};
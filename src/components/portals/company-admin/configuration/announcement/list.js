﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestAnnouncementBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestAnnouncementBusinessUnit";
import { Constants } from "../../../../../app/frameworks/constant/apiConstant";
import { SearchFilter } from "./info";

class AnnouncementListScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties
        this.bladeTitle(this.i18n("Announcements_Announcements")());
        this.bladeSize = BladeSize.Medium;

        this.annoucements = ko.observableArray([]);
        
        this.filterText = ko.observable("");
        this.selectedAnnoucement = ko.observable(null);
        this.recentChangedRowIds = ko.observableArray([]);
        this.order = ko.observable([[ 1, "desc" ]]);

        this.pageFiltering = ko.observable(false);
        this.searchFilter = ko.observable(new SearchFilter());
        this.searchFilterDefault = new SearchFilter();


        this._selectingRowHandler = (annoucement) => {
            return this.navigate("ca-configuration-announcement-view", { id: annoucement.id });
        };

        // Subscribe Message when box search filter apply
        this.subscribeMessage("ca-announcements-search-filter-changed", (searchFilter) => {

            this.isBusy(true);
            this.searchFilter(searchFilter);

            this.webRequestAnnouncementBusinessUnit.listAnnouncementBusinessUnitSummary(searchFilter).done((response) => {

                this.annoucements.replaceAll(response["items"]);
                this.isBusy(false);
            }).fail((e) => {
                this.isBusy(false);
                this.handleError(e);
            });
        });

        this._changeSearchFilterSubscribe = this.searchFilter.subscribe((searchFilter) => {
            // apply active command state if searchFilter does not match default search
            this.pageFiltering(!_.isEqual(this.searchFilter(), this.searchFilterDefault));
        });

        // Subscribe Message when annoucement created/updated
        this.subscribeMessage("ca-announcement-changed", (annoucementId) => {
            this.webRequestAnnouncementBusinessUnit.listAnnouncementBusinessUnitSummary().done((response) => {
                var annoucements = response["items"];
                this.annoucements.replaceAll(annoucements);
                this.recentChangedRowIds.replaceAll([annoucementId]);
            });
        });
    }

    /**
     * Get WebRequest specific for AnnouncementBusinessUnit in Core Web API access.
     * @readonly
     */
    get webRequestAnnouncementBusinessUnit() {
        return WebRequestAnnouncementBusinessUnit.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
         if (!isFirstLoad) return;

        var dfd = $.Deferred();

        this.webRequestAnnouncementBusinessUnit.listAnnouncementBusinessUnitSummary().done((response) => {
            var annoucement = response["items"];
            this.annoucements(annoucement);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }

     /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.CreateAnnouncementAdmin)) {
           commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }

        commands.push(this.createCommand("cmdSearch", this.i18n("Common_Search")(), "svg-cmd-search", true, true, this.pageFiltering));
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {

        switch (sender.id) {
            case "cmdCreate":
                this.navigate("ca-configuration-announcement-manage", { mode: Screen.SCREEN_MODE_CREATE });
                this.selectedAnnoucement(null);
                this.recentChangedRowIds.removeAll();
                break;
        
            case "cmdSearch":
                this.navigate("ca-configuration-announcement-search", { searchFilter: this.searchFilter() }); 
                break;
        }
    }

     /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedAnnoucement(null);
    }
}

export default {
    viewModel: ScreenBase.createFactory(AnnouncementListScreen),
    template: templateMarkup
};
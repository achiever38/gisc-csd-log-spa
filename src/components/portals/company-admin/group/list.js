﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import WebRequestGroup from "../../../../app/frameworks/data/apicore/webRequestGroup";
import {Constants, Enums} from "../../../../app/frameworks/constant/apiConstant";
import DefaultSorting from "../../../../app/frameworks/constant/defaultSorting";
import * as Screen from "../../../../app/frameworks/constant/screen";

/**
 * Company Admin: List Group
 * 
 * @class GroupListScreen
 * @extends {ScreenBase}
 */
class GroupListScreen extends ScreenBase {

    /**
     * Creates an instance of GroupListScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Common_UserGroups")());
        this.bladeSize = BladeSize.Medium;

        this.userGroups = ko.observableArray();
        this.selectedGroup = ko.observable();
        this.filterText = ko.observable();
        this.recentChangedRowIds = ko.observableArray();
        this.order = ko.observable([[ 0, "asc" ]]);

        // Determine if selecting action is legal, return false to cancel selecting event.
        this._selectingRowHandler = (group) => {
            if(group && WebConfig.userSession.hasPermission(Constants.Permission.GroupManagement)) {
                return this.navigate("ca-group-view", { groupId: group.id });
            }
            return false;
        };

        // Observe change about group/permission
        this.subscribeMessage("ca-group-changed", (message) => {
            // Refresh entire datasource
            this.isBusy(true);

            var groupFilter = {
                companyId: WebConfig.userSession.currentCompanyId,
                isSystem: false,
                isCompanyGroup: true,
                sortingColumns: DefaultSorting.Group
            };
            this.webRequestGroup.listGroupSummary(groupFilter).done((response) => {
                var userGroups = response["items"];
                this.userGroups.replaceAll(userGroups);

                // If it is NOT create mode, then clear recentChangedRowIds
                var recentChangedRowIds = (message.mode === Screen.SCREEN_MODE_CREATE) ? [message.groupId] : [];
                this.recentChangedRowIds.replaceAll(recentChangedRowIds);
            }).fail((e)=> {
                this.handleError(e);
            }).always(()=>{
                this.isBusy(false);
            });
        });
    }

    /**
     * Get WebRequest for Group module in Web API access.
     * @readonly
     */
    get webRequestGroup() {
        return WebRequestGroup.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;
        
        var dfd = $.Deferred();

        var groupFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            isSystem: false,
            isCompanyGroup: true,
            sortingColumns: DefaultSorting.Group
        };
        this.webRequestGroup.listGroupSummary(groupFilter).done((response) => {
            var userGroups = response["items"];
            this.userGroups(userGroups);
            
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedGroup(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.CreateGroup_Company)) {
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdCreate") {
            this.navigate("ca-group-manage", { mode: "create" });
            this.selectedGroup(null);
            this.recentChangedRowIds.removeAll();
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(GroupListScreen),
    template: templateMarkup
};
import ko from "knockout";
import templateMarkup from "text!./form.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import "jquery-ui";
import "jqueryui-timepicker-addon";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestUser from "../../../../../app/frameworks/data/apicore/webRequestUser";
import WebRequestGroup from "../../../../../app/frameworks/data/apicore/webRequestGroup";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import {Enums} from "../../../../../app/frameworks/constant/apiConstant";

/**
 * Demo for the following features:
 * 1. Validation on ViewModel
 * - Required Field
 * - Numeric Field
 * - Email
 * - Custom (RegEx)
 * - Remote Validation
 * 2. Track change on ViewModel
 * 
 * @class FormValidationDemo
 * @extends {ScreenBase}
 */
class FormOverspeed extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeSize = BladeSize.Small;
        this.bladeTitle(this.i18n('Report_Filter')());

        this.selectedBusinessUnit = ko.observable(null);
        this.isIncludeSubBU = ko.observable(false);
        this.selectedGroupByType = ko.observable();
        this.selectedUsers = ko.observable();
        this.selectedUserGroups = ko.observable();
        this.selectedUserEnables = ko.observable();

        this.businessUnits = ko.observableArray([]);
        this.users = ko.observableArray([]);
        this.userGroups = ko.observableArray([]);
        this.userEnables = ScreenHelper.createYesNoObservableArray();
        this.groupByTypes = ko.observableArray([]);

        
        
        //datetime
        this.formatSet = "dd/MM/yyyy";
        var addDay = 30;
        var setTimeNow = ("0" + new Date().getHours()).slice(-2)+":"+("0" + new Date().getMinutes()).slice(-2);
        this.start = ko.observable(new Date());
        this.end = ko.observable(new Date());
        this.startTime = ko.observable("00:00");
        this.endTime = ko.observable(setTimeNow);
        this.maxDateStart = ko.observable(new Date());
        this.minDateEnd = ko.observable(new Date());

        this.periodDay = ko.pureComputed(()=>{
            // can't select date more than current date
            var isDate = Utility.addDays(this.start(),addDay);
            let currentDate = parseInt(new Date() / (1000 * 60 * 60 * 24));
            let endDate = parseInt(isDate / (1000 * 60 * 60 * 24)) + 1;
            var calDay = endDate - currentDate;
            if(calDay > 0){
                isDate = new Date();
            }

            // set date when clear start date and end date
            if(this.start() == null){
                // set end date when clear start date
                if(this.end() == null){
                    isDate = new Date();
                }else{
                    isDate = this.end();
                }
                this.maxDateStart(isDate);
                this.minDateEnd(isDate);
            }else{
                this.minDateEnd(this.start())
            }

            return isDate;
        });


        this.selectedUsersBinding = ko.pureComputed(() => {
            var businessUnitIds = (this.selectedBusinessUnit())?this.selectedBusinessUnit():0;
            this.users([]);
            //if check include Sub BU
            if(this.isIncludeSubBU()){
                businessUnitIds = Utility.includeSubBU(this.businessUnits(),businessUnitIds);
            }

            this.webRequestUser.listUserSummary({
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds :businessUnitIds,
                    userType: Enums.UserType.Company,
                    sortingColumns: DefaultSorting.User
                }).done((response) => {
                    //set displayName
                    let listSelectUser = response.items;
                    listSelectUser.forEach(function(arr) {
                        arr.displayName = (arr.fullName.trim())?arr.fullName:arr.username;
                    });

                    if(Object.keys(listSelectUser).length > 0 && listSelectUser[0].id !== 0){
                        listSelectUser.unshift({
                            displayName: this.i18n('Report_All')(),
                            id: 0
                        });
                    }

                    this.users(listSelectUser);

                });
            
         
            return '';
        });

    }

    setupExtend() {

        // Use ko-validation to set up validation rules
        // See url for more information about rules
        // https://github.com/Knockout-Contrib/Knockout-Validation

        this.selectedBusinessUnit.extend({ required: true });
        this.selectedUsers.extend({ required: true });
        this.selectedUserGroups.extend({ required: true });
        this.selectedUserEnables.extend({ required: true });
        this.start.extend({ required: true });
        this.end.extend({ required: true });

        this.validationModel = ko.validatedObservable({
            selectedBusinessUnit: this.selectedBusinessUnit,
            selectedUsers: this.selectedUsers,
            selectedUserGroups: this.selectedUserGroups,
            selectedUserEnables: this.selectedUserEnables,
            end:this.end,
            start:this.start
        });

    }

    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * Get WebRequest specific for BusinessUnit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
     * Get WebRequest specific for User module in Web API access.
     * @readonly
     */
    get webRequestUser() {
        return WebRequestUser.getInstance();
    }

    /**
     * Get WebRequest for Group module in Web API access.
     * @readonly
     */
    get webRequestGroup() {
        return WebRequestGroup.getInstance();
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        
        var dfd = $.Deferred();


        var businessUnitFilter = {
            companyId: WebConfig.userSession.currentCompanyId, 
            sortingColumns: DefaultSorting.BusinessUnit
        };

        var groupFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            isSystem: false,
            isCompanyGroup: true,
            sortingColumns: DefaultSorting.Group
        };

        var enumGroupByTypeFilter = {
            types: [Enums.ModelData.EnumResourceType.GroupByType],
            values: [
                        Enums.ModelData.GroupByType.DateTime,
                        Enums.ModelData.GroupByType.UserName
                    ],
            //[column : 2] = name, [direction: 2] = Descending
            sortingColumns: [{column : 2, direction: 2}]
        };

        var d1 = this.webRequestBusinessUnit.listBusinessUnitSummary(businessUnitFilter);
        var d2 = this.webRequestGroup.listGroupSummary(groupFilter);
        var d3 = this.webRequestEnumResource.listEnumResource(enumGroupByTypeFilter);

        $.when(d1, d2, d3).done((r1, r2, r3) => {
            
            this.businessUnits(r1.items);
            this.userGroups(r2.items);
            this.groupByTypes(r3.items);

            //add report all
            this.userGroups().unshift({
                name: this.i18n('Report_All')(),
                id: 0
            });

            //set value true is 1 and false is 0
            this.userEnables().forEach(function(arr) {
                arr.value = (arr.value)?1:0;
            });
            //add report all
            this.userEnables().unshift({
                title: this.i18n('Report_All')(),
                value: 2
            });

            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }

    buildActionBar(actions){
        actions.push(this.createAction("actPreview", this.i18n('Report_Preview')()));
    }

    onActionClick(sender) {
        if(sender.id === "actPreview") {

            var BusinessUnitID = this.selectedBusinessUnit();
            var User = this.selectedUsers() && this.selectedUsers().id;
            var UserGroup = this.selectedUserGroups() && this.selectedUserGroups().id;
            var UserEnable = this.selectedUserEnables() && this.selectedUserEnables().value;
            var IncludeSubBU = (this.isIncludeSubBU())?1:0;
            var GroupByType = this.selectedGroupByType() && this.selectedGroupByType().value;

            var formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.start())) + " " + this.startTime() + ":00";
            var formatEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.end())) + " " + this.endTime() + ":00";
    
            // varidate Model and set param repoprt
            if(this.validationModel.isValid()) {
                
                var objReport = this.reports = {
                    report:"Gisc.Csd.Service.Report.Library.Admin.UserLogReport, Gisc.Csd.Service.Report.Library", 
                                    parameters: {
                                                    UserID:WebConfig.userSession.id,
                                                    CompanyID:WebConfig.userSession.currentCompanyId,
                                                    BusinessUnitID:BusinessUnitID,
                                                    IncludeSubBusinessUnit:IncludeSubBU,
                                                    SelectedUserID:User,
                                                    GroupID:UserGroup,
                                                    Status:UserEnable,
                                                    SDate:formatSDate,
                                                    EDate:formatEDate,
                                                    GroupBy:GroupByType,
                                                    PrintBy:WebConfig.userSession.fullname,
                                                    Language:WebConfig.userSession.currentUserLanguage,
                                                }
                                };
                this.navigate("cw-report-reportviewer", {reportSource: this.reports, reportName:this.i18n("UserLogReport_UserLog")()});
            } else {
                // This is unreachable code, since we disable save button when VM is not valid
                this.validationModel.errors.showAllMessages();
               
            }
        } 
    }
}

export default {
    viewModel: ScreenBase.createFactory(FormOverspeed),
    template: templateMarkup
};
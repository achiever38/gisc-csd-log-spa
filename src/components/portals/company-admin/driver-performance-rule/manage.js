﻿import "jquery";
import ko from "knockout";
import ScreenBase from "../../screenBase";
import ScreenHelper from "../../screenhelper";
import templateMarkup from "text!./manage.html";
import {
    Enums,
    EntityAssociation
} from "../../../../app/frameworks/constant/apiConstant";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../app/frameworks/constant/screen";
import WebRequestAlertConfiguration from "../../../../app/frameworks/data/apitrackingcore/webRequestAlertConfiguration";
import WebRequestDriveRule from "../../../../app/frameworks/data/apitrackingcore/webRequestDriveRule";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import DefaultSoring from "../../../../app/frameworks/constant/defaultSorting";
import RegulationInfo from "./regulation-info";

/**
 * View Model for Create Driver Performance Rule screen
 * 
 * @class CreateDriverPerformanceRule
 * @extends {ScreenBase}
 */
class CreateDriverPerformanceRule extends ScreenBase {

    /**
     * Creates an instance of CreateDriverPerformanceRule.
     */
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.XLarge;

        // Binding properties on view
        this.driveRuleId = this.ensureNonObservable(params.id, -1);
        this.driveRuleName = ko.observable("");
        this.driveRuleDescription = ko.observable("");
        this.driveRuleEnableDS = ScreenHelper.createYesNoObservableArray();
        this.driveRuleEnabled = ko.observable(ScreenHelper.findOptionByProperty(this.driveRuleEnableDS, "value", true));
        this.driveRuleRegulations = ko.observableArray([]);
        this.driveRuleRegulationsOriginal = [];
        this.alertConfigDS = ko.observableArray([]);
        this.allowAddRegulations = ko.pureComputed(() => {
            return this.driveRuleRegulations().length < 20;
        });
        this.validationModel = ko.validatedObservable({});
        this.alertConfigurationFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            sortingColumns: DefaultSoring.DrivePerformanceRuleAlertConfiguration,
            includeAssociationNames: [
                EntityAssociation.AlertConfiguration.CustomPOI,
                EntityAssociation.AlertConfiguration.CustomPOICategory,
                EntityAssociation.AlertConfiguration.CustomArea,
                EntityAssociation.AlertConfiguration.CustomAreaCategory,
                EntityAssociation.AlertConfiguration.CustomAreaType,
                EntityAssociation.AlertConfiguration.Feature
            ],
            includeIds: [],
            enable: true
        };

        // Update title based on view mode.
        this.mode = this.ensureNonObservable(params.mode);
        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("DriverPerformanceRules_CreateDriverPerformanceRule")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("DriverPerformanceRules_UpdateDriverPerformanceRule")());
                break;
        }

        this.advanceScoreValue = ko.observable();
        this.amberMinScore = ko.observable();
        this.amberMaxScore = ko.observable();
        this.isvalidate = ko.observable(false);
        // Service properties.
        this.webRequestAlertConfiguration = WebRequestAlertConfiguration.getInstance();
        this.webRequestDriveRule = WebRequestDriveRule.getInstance();
    }

    /**
     * Handle load screen.
     * @public
     * @param {any} isFirstLoad
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        this.minimizeAll(false);
        var dfd = $.Deferred();
        var dfdDriveRuleLoader = $.Deferred();

        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                // Do nothing for loading drive rule.
                dfdDriveRuleLoader.resolve();
                break;
            case Screen.SCREEN_MODE_UPDATE:
                // Loading drive rule then continue load alert configuration.
                this.webRequestDriveRule.getDriveRule(this.driveRuleId, [EntityAssociation.DriverPerformanceRule.DriveRuleRegulations])
                    .done((driveRule) => {
                        // Find all alert configuration ids for alert configuration filter.
                        driveRule.driveRuleRegulations.forEach((r) => {
                            this.alertConfigurationFilter.includeIds.push(r.alertConfigurationId);
                        });
                        dfdDriveRuleLoader.resolve(driveRule);
                    })
                    .fail((e) => {
                        dfdDriveRuleLoader.reject(e);
                        dfd.reject(e);
                    });
                break;
        }

        $.when(dfdDriveRuleLoader)
            .done((driveRule) => {
                // Load all alert configuration filter.
                this.webRequestAlertConfiguration.listAlertConfiguration(this.alertConfigurationFilter)
                    .done((alertConfig) => {
                        this.alertConfigDS(alertConfig.items);
                        this.populateModel(driveRule);
                        dfd.resolve();
                    })
                    .fail((e) => {
                        dfd.reject(e);
                    });
            });

        return dfd;
    }

    /**
     * Handle when user close page or hide.
     */
    onUnload() {}

    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        // Tracking changes.
        this.driveRuleName.extend({
            trackChange: true
        });
        this.driveRuleDescription.extend({
            trackChange: true
        });
        this.driveRuleEnabled.extend({
            trackChange: true
        });
        this.driveRuleRegulations.extend({
            trackArrayChange: true
        });

        // Validators
        this.driveRuleName.extend({
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });
        this.driveRuleEnabled.extend({
            required: true
        });
        this.driveRuleRegulations.extend({
            required: true,
            //arrayIsValid: { params: "isValid", message: this.i18n("M001")()}
        });

        this.validationModel({
            driveRuleName: this.driveRuleName,
            driveRuleDescription: this.driveRuleDescription,
            driveRuleEnabled: this.driveRuleEnabled,
            driveRuleRegulations: this.driveRuleRegulations
        });
    }

    /**
     * 
     * @public
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel("actCancel", this.i18n("Common_Cancel")()));
    }

    /**
     * Handle when user perform action.
     * @public
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        // Force observable array notify changes -> force validation evaluate.
        ko.validation.validateObservable(this.driveRuleRegulations);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid() || !this.validateRegulations()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
            if (this.amberMinScore() || this.amberMaxScore()) {
                if (this.amberMinScore() >= this.amberMaxScore()) {
                    this.isvalidate(true)
                    return;
                }
            }


            var model = this.generateModel();
            this.isBusy(true);

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestDriveRule.createDriveRule(model)
                        .done((response) => {
                            this.publishMessage("ca-driver-performance-rule-changed", response.id);
                            this.close(true);
                        })
                        .fail((e) => {
                            this.handleError(e);
                        })
                        .always(() => {
                            this.isBusy(false);
                        });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.webRequestDriveRule.updateDriveRule(model)
                        .done((response) => {
                            this.publishMessage("ca-driver-performance-rule-changed");
                            this.close(true);
                        })
                        .fail((e) => {
                            this.handleError(e);
                        })
                        .always(() => {
                            this.isBusy(false);
                        });
                    break;
            }
        }
    }

    /**
     * Populate drive rule model to page.
     * 
     * @param {any} driveRule
     */
    populateModel(driveRule) {
        if (!driveRule) {
            return;
        }

        this.driveRuleName(driveRule.name);
        this.driveRuleDescription(driveRule.description);
        this.driveRuleEnabled(ScreenHelper.findOptionByProperty(this.driveRuleEnableDS, "value", driveRule.enable));
        this.amberMaxScore(driveRule.amberMaxScore);
        this.amberMinScore(driveRule.amberMinScore);
        // Backup original regulations for submit comparision.
        this.driveRuleRegulationsOriginal = driveRule.driveRuleRegulations;

        // Transform service regulation to flat regulation.
        var flatRegualtions = [];

        // Prepare datasoruce of available alert configuration dropdown.
        driveRule.driveRuleRegulations.forEach((r) => {
            var newRegulation = new RegulationInfo(r);
            newRegulation.alertConfigurations = this.alertConfigDS();
            flatRegualtions.push(newRegulation);
        });

        this.driveRuleRegulations(flatRegualtions);
    }

    /**
     * Populate mode from page.
     */
    generateModel() {
        var model = {
            id: this.driveRuleId,
            name: this.driveRuleName(),
            description: this.driveRuleDescription(),
            enable: this.driveRuleEnabled().value,
            driveRuleRegulations: [],
            companyId: WebConfig.userSession.currentCompanyId,
            amberMinScore: this.amberMinScore(),
            amberMaxScore: this.amberMaxScore(),
        };

        // Looping for driveRule regulations.
        var lastModifiedRegulationIds = [];
        this.driveRuleRegulations().forEach((r) => {
            var regulation = {
                id: r.id,
                alertConfigurationId: r.alertConfigurationId,
                driveRuleGrades: r.driveRuleGrades,
                infoStatus: r.infoStatus,
                advanceScore: r.advanceScore
            };
            // Store id for compare missing regulation, server generated id should greater than 0.
            if (r.id > 0) {
                lastModifiedRegulationIds.push(r.id);
            }

            model.driveRuleRegulations.push(regulation);
        });

        // Verify deleted regulation for update mode.
        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                model.infoStatus = Enums.InfoStatus.Add;
                // Do nothing because create mode has no original reguations.
                break;
            case Screen.SCREEN_MODE_UPDATE:
                model.infoStatus = Enums.InfoStatus.Update;
                // Check missing origial regulation then set as deleted item.
                this.driveRuleRegulationsOriginal.forEach((originalReg) => {
                    if (lastModifiedRegulationIds.indexOf(originalReg.id) === -1) {
                        // Found missing regulation so append to model as deleted status.
                        originalReg.infoStatus = Enums.InfoStatus.Delete;
                        // Append to model which be sent to server.
                        model.driveRuleRegulations.push(originalReg);
                    }
                });
                break;
        }

        return model;
    }

    /**
     * Add new regulation row.
     */
    addDriveRuleRegulation() {
        var newRegulation = new RegulationInfo();
        newRegulation.alertConfigurations = this.alertConfigDS();

        // TODO: Remove this from test.
        // newRegulation.F.min = 0; newRegulation.F.max = 0;
        // newRegulation.D.min = 1; newRegulation.D.max = 1;
        // newRegulation.C.min = 2; newRegulation.C.max = 2;
        // newRegulation.B.min = 3; newRegulation.B.max = 3;
        // newRegulation.A.min = 4; newRegulation.A.max = 4;

        // Adding default validation message.
        var M001Str = this.i18n("M001")();
        var M070Str = this.i18n("M070")();
        newRegulation.error = M001Str;
        newRegulation.F.error = M070Str;
        newRegulation.D.error = M070Str;
        newRegulation.C.error = M070Str;
        newRegulation.B.error = M070Str;
        newRegulation.A.error = M070Str;
        newRegulation.advanceScore = 1;
        // Append new regulation to collection.
        this.driveRuleRegulations.push(newRegulation);
    }

    /**
     * Handle when user changes data source.
     * @param {any} regulation
     */
    onDataSourceChanged(regulation) {
        // Update info status of regulation
        switch (regulation.infoStatus) {
            case Enums.InfoStatus.Original:
                regulation.infoStatus = Enums.InfoStatus.Update;
                break;
            case Enums.InfoStatus.Add:
            case Enums.InfoStatus.Update:
            case Enums.InfoStatus.Delete:
                // Keep as it is.
                break;
        }

        // Perform validation for all regulations.
        this.validateRegulations();

        // Force observable array notify changes -> force validation evaluate.
        ko.validation.validateObservable(this.driveRuleRegulations);
    }

    /**
     * Perform regulation validation.
     */
    validateRegulations() {
        var hasError = false;
        var selectedAlertConfigurationIds = [];
        var duplictedAlertConfigurationIds = [];
        var M001Str = this.i18n("M001")();
        var M097Str = this.i18n("M097")();
        var M070Str = this.i18n("M070")();
        var driveRuleRegulationsAll = this.driveRuleRegulations();

        // แสดง M001 ในกรณีไม่เลือก Alert Configuration
        driveRuleRegulationsAll.forEach((r) => {
            var currentAlertConfigurationId = parseInt(r.alertConfigurationId);
            if (isNaN(currentAlertConfigurationId)) {
                // Display required message.
                r.error = M001Str;
                hasError = true;
            } else {
                // Clear prev error message.
                r.error = null;

                // Check duplicate alert configuration.
                if (selectedAlertConfigurationIds.indexOf(currentAlertConfigurationId) >= 0) {
                    // Push to duplicate list.
                    duplictedAlertConfigurationIds.push(currentAlertConfigurationId);
                } else {
                    // Push to selected list.
                    selectedAlertConfigurationIds.push(currentAlertConfigurationId);
                }
            }
        });

        // แสดง M097 ในกรณีที่เลือก Alert Configuration ซ้ำ
        if (duplictedAlertConfigurationIds.length) {
            this.driveRuleRegulations().forEach((r) => {
                var currentAlertConfigurationId = parseInt(r.alertConfigurationId);
                if (duplictedAlertConfigurationIds.indexOf(currentAlertConfigurationId) >= 0) {
                    r.error = M097Str;
                    hasError = true;
                }
            });
        }

        // แสดง M070 ในกรณีที่คะแนนที่กรอกในทุกลำดับขั้น เมื่อนำมาเรียงกันแล้วมีช่องว่างหรือมีค่าคาบเกี่ยวกัน
        // ทิศทางของค่า จะรองรับ มากไปน้อย หรือ น้อยไปมาก
        driveRuleRegulationsAll.forEach((r) => {
            var validGrades = [r.F, r.D, r.C, r.B, r.A];
            var currentIndex = 0;
            var regulationDirection = 1; // ASC by default.
            validGrades.forEach((currentGrade) => {
                if (currentGrade.isValidRange()) {
                    // Has valid range.
                    currentGrade.error = null;

                    // Continue comparison when index > 1.
                    if (currentIndex > 0) {
                        var prevGrade = validGrades[currentIndex - 1];

                        // Calculate of direction for first pair.
                        if (currentIndex == 1) {
                            regulationDirection = currentGrade.compareTo(prevGrade);
                        }

                        // Compute continueous for each parie.
                        if (!currentGrade.continueFrom(prevGrade, regulationDirection)) {
                            currentGrade.error = M070Str;
                            hasError = true;
                        } else {
                            currentGrade.error = null;
                        }
                    }
                } else {
                    // Has invalid range.
                    currentGrade.error = M070Str;
                    hasError = true;
                }

                currentIndex++;
            });
        });

        return !hasError;
    }
}

export default {
    viewModel: ScreenBase.createFactory(CreateDriverPerformanceRule),
    template: templateMarkup
};
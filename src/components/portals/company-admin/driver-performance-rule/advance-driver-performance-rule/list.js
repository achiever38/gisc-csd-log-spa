﻿import ko from "knockout";
import ScreenBase from "../../../screenBase";
import templateMarkup from "text!./list.html";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import {Constants, Enums} from "../../../../../app/frameworks/constant/apiConstant";
import DriverPerformanceRuleSearchInfo from "../search-info";

import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestAdvanceDriveRule from "../../../../../app/frameworks/data/apitrackingcore/webRequestAdvanceDriveRule";

/**
 * 
 * @class AdvanceDriverPerformanceRuleList
 * @extends {ScreenBase}
 */
class AdvanceDriverPerformanceRuleList extends ScreenBase {

    /**
     * Creates an instance of DriverPerformanceRuleList.
     */
    constructor(params) {
        super(params);

        // Blade properties.
        this.bladeTitle(this.i18n("AdvanceDriverPerformanceRules_AdvanceDriverPerformanceRules")());
        this.bladeSize = BladeSize.Medium;

        // View model properties.
        this.selectedItem = ko.observable(); 
        this.searchText = ko.observable("");
        this.recentChangedRowIds = ko.observableArray();
        this.order = ko.observable([[ 1, "asc" ]]);
        this.items = ko.observableArray([]);

        //this.searchFilter = ko.observable(new DriverPerformanceRuleSearchInfo());
        //this.searchFilterIsActive = ko.pureComputed(() => {
        //    return this.searchFilter().isActive();
        //});

        this.subscribeMessage("ca-advance-driver-performance-rule-changed", (id) => {
            this.refreshDataSource(id);
        });

        this.subscribeMessage("ca-advance-driver-performance-rule-delete", () => {
            this.refreshDataSource(null);
        });

    }

    /**
     * Refresh datasource of datatable.
     */
    refreshDataSource(createdId) {
        this.isBusy(true);
        this.webRequestAdvanceDriveRule.listAdvanceDriverRuleSummary({})
        .done((r) => {
            this.items.replaceAll(r.items);

            if(createdId) {
                this.recentChangedRowIds.replaceAll([createdId]);
            }
            else {
                this.recentChangedRowIds.replaceAll([]);
            }
        })
        .fail((e) => {
            this.handleError(e);
        })
        .always(() => {
            this.isBusy(false);
        });
    }

    get webRequestAdvanceDriveRule() {
        return WebRequestAdvanceDriveRule.getInstance();
    }
    /**
     * Handle screen loading.
     * 
     * @param {any} isFirstLoad
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();
        this.webRequestAdvanceDriveRule.listAdvanceDriverRuleSummary({})
        .done((result) => {
            this.items(result.items);
            dfd.resolve();
        })
        .fail((e)=>{
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Handle when screen is unload
     */
    onUnload() {
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedItem(null);
    }

    /**
     * Life cycle: create command bar
     * @public
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.CreateAdvanceDriverPerformanceRule)) {
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
        
    }
    
    /**
     * Life cycle: handle command click
     * @public
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch(sender.id) {
            case "cmdCreate":
                this.navigate("ca-advance-driver-performance-rule-manage", {mode: Screen.SCREEN_MODE_CREATE});
                break;
            case "cmdSearch":
                this.navigate("ca-driver-performance-rule-search", {filterState: this.searchFilter()});
                break;
        }
    }

    /**
     * Handle to viewing readonly mode when user clicks on table.
     * 
     * @param {any} item
     * @returns
     */
    selectingRowHandler(item) {
        return this.navigate("ca-advance-driver-performance-rule-view", {
            id: item.id
        });
    }
}

export default {
    viewModel: ScreenBase.createFactory(AdvanceDriverPerformanceRuleList),
    template: templateMarkup
};
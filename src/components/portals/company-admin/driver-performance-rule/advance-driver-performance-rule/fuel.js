﻿import ko from "knockout";
import templateMarkup from "text!./fuel.html";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import ScreenBase from "../../../screenBase";


/**
 * 
 * @class AdvanceDriverPerformanceRuleList
 * @extends {ScreenBase}
 */
class AdvanceDriverPerformanceRuleFuel extends ScreenBase {
    constructor(params) {
        super(params);
        this.mode = this.ensureNonObservable(params.mode);
        this.bladeTitle(this.i18n("AdvanceDriverPerformanceRules_Fuel")());
        this.bladeSize = BladeSize.Medium;
        this.fuelInfosParams = this.ensureNonObservable(_.cloneDeep(params.fuelInfos), []);
        this.fuelInfos = ko.observableArray([]);

        if (_.size(this.fuelInfosParams) == 0) {
            this.fuelInfos([
                { id: 1, fuelDiffFrom: 0, score: 100 },
                { id: 2, canDelete: true, fuelDiffFrom: 0, fuelDiffTo: -5, score: 80, enable: true },
                { id: 3, canDelete: true, fuelDiffFrom: -5, fuelDiffTo: -10, score: 60, enable: true },
                { id: 4, canDelete: true, fuelDiffFrom: -10, fuelDiffTo: -15, score: 40, enable: true },
                { id: 5, canDelete: true, fuelDiffFrom: -15, fuelDiffTo: -20, score: 20, enable: true },
                { id: 6, fuelDiffTo: -20, score: 0 }
            ]);
            
        }
        else {
            this.fuelInfos(this.fuelInfosParams);
        }
        
    }
    addFuel() {
        let lastIndex = _.size(this.fuelInfos());
        let tmpFuel = _.clone(this.fuelInfos());

        this.fuelInfos([]);

        let lastItem = _.clone(tmpFuel[lastIndex - 1]);
        tmpFuel.splice(lastIndex - 1, 1);


        var newSpeed = {
            fuelDiffFrom: _.last(tmpFuel).fuelDiffTo != undefined  ? _.last(tmpFuel).fuelDiffTo 
                                                            : lastIndex == 2 ? 0 : "",//(_.maxBy(tmpFuel, 'fuelDiffTo')) ? _.maxBy(tmpFuel, 'fuelDiffTo').fuelDiffTo : "",
            fuelDiffTo: "",
            error: null,
            enable: true,
            canDelete: true,
            score: "",
            id: lastIndex + 1
        };

        lastItem.fuelDiffTo = "";
        lastItem.score = 0;
        lastItem.enable = false;

        tmpFuel.push(newSpeed);
        tmpFuel.push(lastItem);

        this.fuelInfos.replaceAll(tmpFuel);
    }

    setupExtend() {
        this.fuelInfos.extend({
            trackArrayChange: true
        });
    }

    onDataSourceChanged(data) {
        let tmpItem = _.clone(this.fuelInfos());
        this.fuelInfos([]);

        for (var i = 0; i < tmpItem.length; i++) {
            if (i == 0) continue;
            else {
                let maxRangePrev = i == 1 ? 0 : tmpItem[i - 1].fuelDiffTo;

                if (i == tmpItem.length - 1) {
                    tmpItem[i].fuelDiffTo = maxRangePrev;
                    tmpItem[i].score = isNaN(tmpItem[i].score) ? "" : tmpItem[i].score;
                    break;
                }
                tmpItem[i].fuelDiffFrom = maxRangePrev;
            }

            //Range FuelDiffTo
            let rangeMaxVal = parseFloat(tmpItem[i].fuelDiffTo);
            if (!isNaN(rangeMaxVal)) {  // Check value of float
                if (rangeMaxVal > 0) {  // Check value must less than 100 
                    tmpItem[i].fuelDiffTo = "";
                }
                else {
                    tmpItem[i].fuelDiffTo = _.round(rangeMaxVal, 2);
                }
            }

            //Score 
            let scoreVal = parseFloat(tmpItem[i].score);
            if (!isNaN(scoreVal)) {  // Check value of float
                if (scoreVal > 100) {  // Check value must less than 100 
                    tmpItem[i].score = "";
                }
                else {
                    tmpItem[i].score = scoreVal < 0 ? 0 : _.round(scoreVal, 2);
                }
            }
            else {
                tmpItem[i].score = "";
            }
        }

        this.fuelInfos.replaceAll(tmpItem);
    }

    onRemovable(data) {
        if (_.size(this.fuelInfos()) == 2) {
            let tmpItem = _.clone(this.fuelInfos());
            this.fuelInfos([]);

            tmpItem[1].enable = true;
            tmpItem[1].fuelDiffTo = 0;
            this.fuelInfos.replaceAll(tmpItem);
        }
        else {
            let tmpItem = _.clone(this.fuelInfos());
            this.fuelInfos([]);

            for (var i = 0; i < tmpItem.length; i++) {
                if (i == 0) continue;
                else {
                    let maxRangePrev = i == 1 ? 0 : tmpItem[i - 1].fuelDiffTo;

                    if (i == tmpItem.length - 1) {
                        tmpItem[i].fuelDiffTo = maxRangePrev;
                        tmpItem[i].score = isNaN(tmpItem[i].score) ? "" : tmpItem[i].score;
                        break;
                    }
                    tmpItem[i].fuelDiffFrom = maxRangePrev;
                }

                //Range FuelDiffTo
                let rangeMaxVal = parseFloat(tmpItem[i].fuelDiffTo);
                if (!isNaN(rangeMaxVal)) {  // Check value of float
                    if (rangeMaxVal > 100) {  // Check value must less than 100 
                        tmpItem[i].fuelDiffTo = 100;
                    }
                    else {
                        tmpItem[i].fuelDiffTo = _.round(rangeMaxVal, 2);
                    }
                }
            }

            this.fuelInfos.replaceAll(tmpItem);
        }
    }

    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel("actCancel", this.i18n("Common_Cancel")()));
    }

    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id == "actSave") {
            let inValid = false;
            let M001Str = this.i18n("M001")(); //required
            let M065Str = this.i18n("M065")(); //invalid

            let tmpItem = this.fuelInfos();

            for (var i = 0; i < tmpItem.length; i++) {
                if (i == 0 || i == tmpItem.length - 1) continue;

                // Check Range FuelDiffTo
                let rangeMaxVal = parseFloat(tmpItem[i].fuelDiffTo);
                let rangeMinVal = tmpItem[i].fuelDiffFrom;

                if (isNaN(rangeMaxVal)) {
                    inValid = true;
                    tmpItem[i].error = M001Str;
                }
                else {
                    tmpItem[i].error = null;

                    if (rangeMaxVal >= rangeMinVal) {  // Check value of float
                        inValid = true;
                        tmpItem[i].error = M065Str;
                    }
                    else {
                        tmpItem[i].error = null;
                    }
                }


                //Score 
                let scoreVal = parseFloat(tmpItem[i].score);
                if (isNaN(scoreVal)) { // Check Empty String
                    inValid = true;
                    tmpItem[i].errorScore = M001Str;
                }
                else {
                    tmpItem[i].errorScore = null;
                }

            }

            let fuelInfoItems = _.clone(tmpItem);
            this.fuelInfos([]);
            this.fuelInfos.replaceAll(fuelInfoItems);

            if (inValid) return;

            this.publishMessage("ca-advance-driver-rule-fuel", this.fuelInfos());
            this.close(true);
        }
       
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();

        dfd.resolve();

        return dfd;
    }

    onUnload() {
        this.fuelInfosParams = [];
        this.fuelInfos([]);
    }
}



export default {
    viewModel: ScreenBase.createFactory(AdvanceDriverPerformanceRuleFuel),
    template: templateMarkup
};
﻿import "jquery";
import ko from "knockout";
import templateMarkup from "text!./manage.html";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import WebRequestAdvanceDriveRule from "../../../../../app/frameworks/data/apitrackingcore/webRequestAdvanceDriveRule";
import ScreenBase from "../../../screenBase";
import ScreenHelper from "../../../screenhelper";

/**
 * View Model for Create Driver Performance Rule screen
 * 
 * @class CreateDriverPerformanceRule
 * @extends {ScreenBase}
 */
class ManageAdvanceDriverPerformanceRule extends ScreenBase {

    /**
     * Creates an instance of CreateAdvanceDriverPerformanceRule.
     */
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Medium;

        // Binding properties on view
        this.advanceDriveRuleId = this.ensureNonObservable(params.id, -1);
        this.driveRuleName = ko.observable("");
        this.driveRuleDescription = ko.observable("");
        this.driveRuleEnableDS = ScreenHelper.createYesNoObservableArray();
        this.driveRuleEnabled = ko.observable();
        this.targetScore = ko.observable();
        this.speedingScore = ko.observable();
        this.behaviorScore = ko.observable();
        this.fuelRateScore = ko.observable();
        this.badMin = ko.observable(0);
        this.badMax = ko.observable();
        this.fairMin = ko.observable();
        this.fairMax = ko.observable();
        this.goodMin = ko.observable();
        this.goodMax = ko.observable(100);

        this.selectedSubMenuItem = ko.observable(null);
        this.accessibleBusinessUnits = ko.observableArray([]);
        this.speedInfos = ko.observableArray([]);
        this.behaviorInfos = ko.observableArray([]);
        this.fuelInfos = ko.observableArray([]);

        // Update title based on view mode.
        this.mode = this.ensureNonObservable(params.mode);
        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("AdvanceDriverPerformanceRules_CreateAdvanceDriverPerformanceRule")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("AdvanceDriverPerformanceRules_UpdateAdvanceDriverPerformanceRule")());
                break;
        }

        // Observe change about Accessible Business Units
        this.subscribeMessage("ca-advance-driver-rule-accessible-business-unit-selected", (selectedBusinessUnits) => {
            var mappedSelectedBUs = selectedBusinessUnits.map((bu) => {
                let mbu = bu;
                mbu.businessUnitId = bu.id;
                mbu.businessUnitName = bu.name;
                mbu.businessUnitPath = bu.businessUnitPath;
                mbu.canManage = true;
                return mbu;
            });

            // #30250 - need to keep unable to manage bu + mappedSelectedBUs
            var unableToManageBUs = _.filter(this._originalAccessibleBusinessUnits, (o) => {
                return !o.canManage;
            });
            if (unableToManageBUs) {
                // exclude unable to manage BU before merge
                var unableToManageBUIds = unableToManageBUs.map((bu) => { return bu.businessUnitId });
                mappedSelectedBUs = _.filter(mappedSelectedBUs, (bu) => {
                    return _.indexOf(unableToManageBUIds, bu.businessUnitId) === -1;
                });

                // merge select BU and unable to manage items
                mappedSelectedBUs = mappedSelectedBUs.concat(unableToManageBUs);
            }

            this.accessibleBusinessUnits.replaceAll(mappedSelectedBUs);
        });

        this.subscribeMessage("ca-advance-driver-rule-speed", (item) => {
            this.speedInfos.replaceAll(item);
        });

        this.subscribeMessage("ca-advance-driver-rule-behavior", (item) => {
            this.behaviorInfos.replaceAll(item);
        });

        this.subscribeMessage("ca-advance-driver-rule-fuel", (item) => {
            this.fuelInfos.replaceAll(item);
        });

        this.fairMin.subscribe((data) => {
            this.badMax(data);
        });
        this.goodMin.subscribe((data) => {
            this.fairMax(data);
        });

    }

    get webRequestAdvanceDriveRule() {
        return WebRequestAdvanceDriveRule.getInstance();
    }

    /**
     * Handle load screen.
     * @public
     * @param {any} isFirstLoad
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();

        let webRequestAdvanceDriverRule = this.advanceDriveRuleId == -1 ? null : this.webRequestAdvanceDriveRule.getAdvanceDriverRule(this.advanceDriveRuleId);

        $.when(webRequestAdvanceDriverRule).done((r) => {
            if (r) {
                this.driveRuleName(r.name);
                this.driveRuleDescription(r.description);
                this.driveRuleEnabled(ScreenHelper.findOptionByProperty(this.driveRuleEnableDS, "value", r.enable));
                this.targetScore(r.targetScore);
                this.speedingScore(r.speedingScore);
                this.behaviorScore(r.behaviourScore);
                this.fuelRateScore(r.fuelUsedScore);
                this.badMin(r.badScoreFrom);
                this.badMax(r.badScoreTo);
                this.fairMin(r.fairScoreFrom);
                this.fairMax(r.fairScoreTo);
                this.goodMin(r.goodScoreFrom);
                this.goodMax(r.goodScoreTo);

                r.advanceDriverRuleFuels = _.map(r.advanceDriverRuleFuels, (item, index) => {
                    let enable = index == 0 || index == _.findLastIndex(r.advanceDriverRuleFuels) ? false : true;
                    item.enable = enable;
                    item.canDelete = enable;
                    return item;
                });

                r.advanceDriverRuleSpeeds = _.map(r.advanceDriverRuleSpeeds, (item, index) => {
                    item.canDelete = index == 0 || index == _.findLastIndex(r.advanceDriverRuleSpeeds) ? false : true;
                    return item;
                });

                this.speedInfos(r.advanceDriverRuleSpeeds);
                this.fuelInfos(r.advanceDriverRuleFuels);
                this.accessibleBusinessUnits(r.accessibleBusinessUnits);
                this.behaviorInfos(r.advanceDriverRuleBehaviors);
            }
            else {
                this.driveRuleEnabled(ScreenHelper.findOptionByProperty(this.driveRuleEnableDS, "value", true));
            }
            dfd.resolve();
        }).fail((ex) => {
            this.handleError(ex);
            dfd.reject();
        });

        return dfd;
    }

    /**
     * Handle when user close page or hide.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedSubMenuItem(null);
    }


    goToSubPage(index) {
        var page = "";
        var options = {
            mode: this.mode
        };

        switch (index) {
            case 1:
                let selectedBusinessUnitIds = this.accessibleBusinessUnits().map((obj) => {
                    return obj.businessUnitId;
                });
                page = "ca-advance-driver-performance-rule-accessible-bu-select";
                options.selectedBusinessUnitIds = selectedBusinessUnitIds;
                break;
            case 2:
                page = "ca-advance-driver-performance-rule-speed";
                options.speedInfos = this.speedInfos();
                break;
            case 3:
                page = "ca-advance-driver-performance-rule-behavior";
                options.behaviorInfos = this.behaviorInfos();
                options.advanceDriveRuleId = this.advanceDriveRuleId;
                break;
            case 4:
                page = "ca-advance-driver-performance-rule-fuel";
                options.fuelInfos = this.fuelInfos();
                break;
        }

        this.navigate(page, options);
        this.selectedSubMenuItem(index);
    }

    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        // Tracking changes.

        var self = this;
        this.driveRuleName.extend({
            trackChange: true
        });
        this.driveRuleDescription.extend({
            trackChange: true
        });
        this.driveRuleEnabled.extend({
            trackChange: true
        });
        this.accessibleBusinessUnits.extend({
            arrayRequired: true
        });
        this.accessibleBusinessUnits.extend({
            trackArrayChange: true
        });
        this.speedInfos.extend({
            arrayRequired: true
        });
        this.speedInfos.extend({
            trackArrayChange: true
        });
        this.behaviorInfos.extend({
            arrayRequired: true
        });
        this.behaviorInfos.extend({
            trackArrayChange: true
        });
        this.fuelInfos.extend({
            arrayRequired: true
        });
        this.fuelInfos.extend({
            trackArrayChange: true
        });
        this.driveRuleName.extend({
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });
        this.driveRuleEnabled.extend({
            required: true
        });
        this.driveRuleEnabled.extend({
            trackChange: true
        });
        
        this.targetScore.extend({
            required: true
        });
        this.targetScore.extend({
            trackChange: true
        });

        this.speedingScore.extend({
            required: true
        });
        this.speedingScore.extend({
            trackChange: true
        });

        this.behaviorScore.extend({
            required: true
        });
        this.behaviorScore.extend({
            trackChange: true
        });

        this.fuelRateScore.extend({
            required: true
        });
        this.fuelRateScore.extend({
            trackChange: true
        });

        this.fairMin.extend({
            required: true
        });
        this.fairMin.extend({
            trackChange: true
        });

        this.goodMin.extend({
            required: true,
            validation: {
                validator: (val) => {
                    if (self.fairMin() != undefined && val < self.fairMin()) {
                        return false;
                    }
                    return true;
                },
                message: this.i18n("M065")()
            }
        });
        this.goodMin.extend({
            trackChange: true
        });

        this.validationModel = ko.validatedObservable({
            driveRuleName: this.driveRuleName,
            driveRuleEnabled: this.driveRuleEnabled,
            speedingScore: this.speedingScore,
            behaviorScore: this.behaviorScore,
            fuelUsedScore: this.fuelRateScore,
            goodMin: this.goodMin,
            fairMin: this.fairMin,
            accessibleBusinessUnits: this.accessibleBusinessUnits,
            speedInfos: this.speedInfos,
            behaviorInfos: this.behaviorInfos,
            fuelInfos: this.fuelInfos,
            targetScore: this.targetScore
        });
    }

    /**
     * 
     * @public
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel("actCancel", this.i18n("Common_Cancel")()));
    }

    /**
     * Handle when user perform action.
     * @public
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            var model = this.generateModel();
            console.log(model);
            this.isBusy(true);

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestAdvanceDriveRule.createAdvanceDriverRule(model)
                        .done((response) => {
                            this.publishMessage("ca-advance-driver-performance-rule-changed", response.id);
                            this.close(true);
                        })
                        .fail((e) => {
                            this.handleError(e);
                        })
                        .always(() => {
                            this.isBusy(false);
                        });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.webRequestAdvanceDriveRule.updateAdvanceDriverRule(model)
                        .done((response) => {
                            this.publishMessage("ca-advance-driver-performance-rule-changed");
                            this.close(true);
                        })
                        .fail((e) => {
                            this.handleError(e);
                        })
                        .always(() => {
                            this.isBusy(false);
                        });
                    break;
            }
        }
    }


    /**
     * Populate mode from page.
     */
    generateModel() {
        var model = {
            id: this.advanceDriveRuleId,
            name: this.driveRuleName(),
            description: this.driveRuleDescription(),
            enable: this.driveRuleEnabled().value,
            targetScore: this.targetScore(),
            speedingScore: this.speedingScore(),
            behaviourScore: this.behaviorScore(),
            fuelUsedScore: this.fuelRateScore(),
            badScoreFrom: this.badMin(),
            badScoreTo: this.badMax(),
            fairScoreFrom: this.fairMin(),
            fairScoreTo: this.fairMax(),
            goodScoreFrom: this.goodMin(),
            goodScoreTo: this.goodMax(),
            companyId: WebConfig.userSession.currentCompanyId,
            accessibleBusinessUnits: this.accessibleBusinessUnits(),
            advanceDriverRuleSpeeds: this.speedInfos(),
            advanceDriverRuleBehaviors: this.behaviorInfos(),
            advanceDriverRuleFuels: this.fuelInfos()
        };

        return model;
    }
    

    /**
    * 
    * Handle when sub menu item click
    * @param {any} index
    * 
    * @memberOf VehicleManageScreen
    */
    onSelectSubMenuItem(index) {
        if (this.journeyCanClose()) {
            this.goToSubPage(index);
        }
        else {
            this.showMessageBox(null, this.i18n("M100")(),
                BladeDialog.DIALOG_OKCANCEL).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_OK:
                            this.goToSubPage(index);
                            break;
                    }
                });
        }
    }

    validScore() {
        if (this.speedingScore() == "" || this.behaviorScore() == ""  || this.fuelRateScore() == "") {
            return true;
        }

        let sumScore = this.speedingScore() + this.behaviorScore() + this.fuelRateScore();
        let checkScore = parseInt(sumScore) != 100 ? false : true;
        return checkScore;
    }

}

export default {
    viewModel: ScreenBase.createFactory(ManageAdvanceDriverPerformanceRule),
    template: templateMarkup
};
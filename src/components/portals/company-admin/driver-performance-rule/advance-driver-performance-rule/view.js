﻿import "jquery";
import ko from "knockout";
import templateMarkup from "text!./view.html";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenBase";
import ScreenHelper from "../../../screenhelper";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebRequestAdvanceDriveRule from "../../../../../app/frameworks/data/apitrackingcore/webRequestAdvanceDriveRule";
import WebRequestAlertConfiguration from "../../../../../app/frameworks/data/apitrackingcore/webRequestAlertConfiguration";
import { Constants, EntityAssociation} from "../../../../../app/frameworks/constant/apiConstant";
import DefaultSoring from "../../../../../app/frameworks/constant/defaultSorting";

/**
 * View Model for View Driver Performance Rule screen
 * 
 * @class ViewAdvanceDriverPerformanceRule
 * @extends {ScreenBase}
 */
class ViewAdvanceDriverPerformanceRule extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("AdvanceDriverPerformanceRules_ViewAdvanceDriverPerformanceRule")());
        this.bladeSize = BladeSize.Medium;

        this.advanceDriverId = this.ensureNonObservable(params.id, -1);
        this.name = ko.observable();
        this.description = ko.observable();
        this.enable = ko.observable();
        this.targetScore = ko.observable();
        this.speedingScore = ko.observable();
        this.behaviorScore = ko.observable();
        this.fuelUsedRateScore = ko.observable();
        this.badFromScore = ko.observable();
        this.badToScore = ko.observable();
        this.fairFromScore = ko.observable();
        this.fairToScore = ko.observable();
        this.goodFromScore = ko.observable();
        this.goodToScore = ko.observable();
        this.lastUpdateDate = ko.observable();
        this.lastUpdateBy = ko.observable();
        this.createDate = ko.observable();
        this.createBy = ko.observable();

        this.buInfos = ko.observableArray();
        this.speedInfos = ko.observableArray();
        this.behaviorInfos = ko.observableArray();
        this.fuelInfos = ko.observableArray();

        this.collapsedNodes = ko.observableArray();
        this.alertConfigDS = ko.observableArray([]);
        this.subscribeMessage("ca-advance-driver-performance-rule-changed", (id) => {
            this.getAdvanceDriverRuleId(this.advanceDriverId);
        });
    }

    get webRequestAdvanceDriveRule() {
        return WebRequestAdvanceDriveRule.getInstance();
    }
    get webRequestAlertConfiguration(){
        return WebRequestAlertConfiguration.getInstance();
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();
        $.when(this.getAdvanceDriverRuleId(this.advanceDriverId)).done(() => {
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });


        return dfd;
    }

    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.UpdateAdvanceDriverPerformanceRule)) {
            commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.DeleteAdvanceDriverPerformanceRule)) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }

    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdUpdate":
                this.navigate("ca-advance-driver-performance-rule-manage", { mode: Screen.SCREEN_MODE_UPDATE, id: this.advanceDriverId });
                break;
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M218")(),
                    BladeDialog.DIALOG_YESNO).done((button) => {
                        switch (button) {
                            case BladeDialog.BUTTON_YES:
                                this.isBusy(true);

                                this.webRequestAdvanceDriveRule.deleteAdvanceDriverRule(this.advanceDriverId).done(() => {
                                    this.isBusy(false);
                                    this.publishMessage("ca-advance-driver-performance-rule-delete");
                                    this.close(true);
                                }).fail((e) => {
                                    this.handleError(e);
                                    this.isBusy(false);
                                });
                                break;
                        }
                    });
                break;
        }
    }

    getAdvanceDriverRuleId(id) {
        let dfd = $.Deferred();
        let webRequestAdvanceDriverRule = id == -1 ? null : this.webRequestAdvanceDriveRule.getAdvanceDriverRule(id);

        $.when(webRequestAdvanceDriverRule).done((r) => {
            if (r) {
                this.name(r.name);
                this.description(r.description);
                this.enable(r.formatEnable);
                this.targetScore(r.targetScore);
                this.speedingScore(r.speedingScore);
                this.behaviorScore(r.behaviourScore);
                this.fuelUsedRateScore(r.fuelUsedScore);
                this.badFromScore(r.badScoreFrom);
                this.badToScore(r.badScoreTo);
                this.fairFromScore(r.fairScoreFrom);
                this.fairToScore(r.fairScoreTo);
                this.goodFromScore(r.goodScoreFrom);
                this.goodToScore(r.goodScoreTo);
                this.lastUpdateDate(r.formatUpdateDate);
                this.lastUpdateBy(r.updateBy);
                this.createDate(r.formatCreateDate);
                this.createBy(r.createBy);

                let speedInfos = r.advanceDriverRuleSpeeds;
                let fuelInfos = r.advanceDriverRuleFuels;
                this.buInfos(r.businessUnitInfos);
                this.behaviorInfos(r.advanceDriverRuleBehaviors);

                if (_.size(speedInfos)) {
                    speedInfos.forEach((item) => {
                        item.enable = false;
                        if (item.speedFrom != undefined && item.speedTo != undefined) {
                            item.formatSpeed = ">= "+item.speedFrom + " <= " + item.speedTo;
                        }
                        else {
                            item.formatSpeed = " > "+item.speedTo;
                        }
                    });

                    this.speedInfos(speedInfos);
                }

                if (_.size(fuelInfos)) {
                    fuelInfos.forEach((item) => {
                        if (item.fuelDiffFrom != undefined && item.fuelDiffTo != undefined) {
                            item.formatFuel = `< ${item.fuelDiffFrom} >= ${item.fuelDiffTo} %`;
                        }
                        else {
                            item.formatFuel = item.fuelDiffTo != undefined ? " < "+item.fuelDiffTo+" % " : "  >= "+item.fuelDiffFrom+" % ";
                        }
                    });
                    this.fuelInfos(fuelInfos);
                }
            }
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return  dfd;
    }
    
}

export default {
    viewModel: ScreenBase.createFactory(ViewAdvanceDriverPerformanceRule),
    template: templateMarkup
}
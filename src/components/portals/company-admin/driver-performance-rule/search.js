﻿import "jquery";
import ko from "knockout";
import ScreenBase from "../../screenBase";
import ScreenHelper from "../../screenhelper";
import Utility from "../../../../app/frameworks/core/utility";
import templateMarkup from "text!./search.html";
import WebRequestAlertConfiguration from "../../../../app/frameworks/data/apitrackingcore/webRequestAlertConfiguration";
import DriverPerformanceRuleSearchInfo from "./search-info";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import DefaultSoring from "../../../../app/frameworks/constant/defaultSorting";
import {EntityAssociation} from "../../../../app/frameworks/constant/apiConstant";

/**
 * Filter list of Driver Performance Rule based on criteria
 * 
 * @class DriverPerformanceRuleSearch
 * @extends {ScreenBase}
 */
class DriverPerformanceRuleSearch extends ScreenBase {
    /**
     * Creates an instance of DriverPerformanceRuleSearch.
     */
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Common_Search")());

        // Web Request for alert configuration.
        this.webRequestAlertConfiguration = WebRequestAlertConfiguration.getInstance();
        this.alertConfigurationFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            sortingColumns: DefaultSoring.DrivePerformanceRuleAlertConfiguration,
            includeAssociationNames: [
                EntityAssociation.AlertConfiguration.CustomPOI,
                EntityAssociation.AlertConfiguration.CustomPOICategory,
                EntityAssociation.AlertConfiguration.CustomArea,
                EntityAssociation.AlertConfiguration.CustomAreaCategory,
                EntityAssociation.AlertConfiguration.CustomAreaType,
                EntityAssociation.AlertConfiguration.Feature
            ] // Always display all configuration include disabled item.
        };

        // Internal filer state.
        this.filterState = Utility.extractParam(params.filterState, null);

        this.alertConfigDS = ko.observableArray([]);
        this.alertConfigSelected = ko.observable(null);

        this.ruleEnableDS = ScreenHelper.createYesNoObservableArray();
        this.ruleEnabled = ko.observable(null);
    }

    /**
     * Handle load filter.
     * 
     * @param {any} isFirstLoad
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        // Invoke web request for available configuration.
        this.webRequestAlertConfiguration.listAlertConfiguration(this.alertConfigurationFilter)
        .done((result) => {
            this.alertConfigDS(result.items);

            // Prepare datasoruce of available alert configuration dropdown.
            if(this.filterState) {
                if(this.filterState.alertConfigId !== null) {
                    this.alertConfigSelected(ScreenHelper.findOptionByProperty(this.alertConfigDS, "id", this.filterState.alertConfigId));
                }

                if(this.filterState.enable !== null) {
                    this.ruleEnabled(ScreenHelper.findOptionByProperty(this.ruleEnableDS, "value", this.filterState.enable));
                }
            }

            dfd.resolve();
        })
        .fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Handle when screen is unload
     */
    onUnload() {
    }

    /**
     * Handle when user click on action bar buttons.
     * @public
     * @param {any} actions
     */
    buildActionBar(actions){
        actions.push(this.createAction("actSearch", this.i18n("Common_Search")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdClear", this.i18n("Common_Clear")(), "svg-cmd-clear"));
    }

    /**
     * Handle when user click action from screen.
     * @public
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        switch(sender.id) {
            case "actSearch":
                if(this.shouldCreateStateObject()) {
                    this.filterState = new DriverPerformanceRuleSearchInfo();
                }

                var selectedAlertConfig = this.alertConfigSelected();
                if(selectedAlertConfig) {
                    // Add to selected state.
                    this.filterState.alertConfigurationIds = [selectedAlertConfig.id];
                }
                else {
                    // Clear to default value.
                    this.filterState.alertConfigurationIds = [];
                }

                this.filterState.enable = this.ruleEnabled() ? this.ruleEnabled().value : null;

                this.publishMessage("driver-performance-rule-search-changed", this.filterState);
                break;
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch(sender.id) {
            case "cmdClear":
                this.alertConfigSelected(null);
                this.ruleEnabled(null);
                this.filterState.reset();
                break;
        }
    }

    /**
     * Determine whether state object should be created or not.
     * @private 
     */
    shouldCreateStateObject() {
        if(this.filterState) return false;
        return (this.alertConfigSelected() !== null || (this.ruleEnabled() !== null));
    }
}

export default {
    viewModel: ScreenBase.createFactory(DriverPerformanceRuleSearch),
    template: templateMarkup
};
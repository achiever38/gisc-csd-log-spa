import DefaultSorting from "../../../../app/frameworks/constant/defaultSorting";

/**
 * State of Driver Performance Rule List Filter, used for interact with Driver Performance Rules screen
 * 
 * @public
 * @class DriverPerformanceRuleSearchInfo
 */
class DriverPerformanceRuleSearchInfo {

    /**
     * Creates an instance of DriverPerformanceRuleSearchInfo.
     */
    constructor() {
        this.reset();
    }

    /**
     * Check if the filter is active
     * @public
     * @returns true if there is at least one property set, false if all are not set.
     */
    isActive() {
        var isActive = false;

        if((this.alertConfigurationIds.length) || (this.enable !== null)) {
            isActive = true;
        }

        return isActive;
    }

    /**
     * Reset all properties to its inactive state
     */
    reset() {
        this.alertConfigurationIds = [];
        this.enable = null;
        this.sortingColumns = DefaultSorting.DrivePerformanceRule;
    }
}

export default DriverPerformanceRuleSearchInfo;
﻿import ko from "knockout";
import templateMarkup from "text!./menu.html";
import ScreenBase from "../../screenbase";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import {
    Constants,
    Enums
} from "../../../../app/frameworks/constant/apiConstant";


/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class AssetMenuScreen extends ScreenBase {

    /**
     * Creates an instance of AssetMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Common_Assets")());
        this.bladeMargin = BladeMargin.Narrow;
        this.bladeSize = BladeSize.XSmall;

        this.selectedIndex = ko.observable();

        this._selectingRowHandler = (item) => {
            if (item) {
                return this.navigate(item.id);
            }
            return false;
        };

        this.items = ko.observableArray([]);
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.CreateBoxMaintenance) ||
            WebConfig.userSession.hasPermission(Constants.Permission.UpdateBoxMaintenance) ||
            WebConfig.userSession.hasPermission(Constants.Permission.DeleteBoxMaintenance)) {
            this.items.push({
                text: this.i18n("Common_Boxes")(),
                id: 'ca-asset-box'
            });
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.Vehicle)) {
            this.items.push({
                text: this.i18n("Common_Vehicles")(),
                id: 'ca-asset-vehicle'
            });
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.Driver)) {
            this.items.push({
                text: this.i18n("Common_Drivers")(),
                id: 'ca-asset-driver'
            });
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.FleetService)) {
            this.items.push({
                text: this.i18n("Assets_FleetServices")(),
                id: 'ca-asset-fleet-service'
            });
        }


        if (WebConfig.userSession.hasPermission(Constants.Permission.MobileService)) {
            this.items.push({
                text: this.i18n("Assets_MobileServices")(),
                id: 'ca-asset-mobile-service'
            });
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.SMSService)) {
            this.items.push({
                text: this.i18n("Assets_SMSServices")(),
                id: 'ca-asset-sms-service'
            });
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.ViewMaintenance) ||
            WebConfig.userSession.hasPermission(Constants.Permission.UpdateMaintenance)) {
            this.items.push({
                text: this.i18n("Assets_Maintenance_Management")(),
                id: 'ca-asset-maintenance-management'
            });
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.Trainer)) {
            this.items.push({
                text: this.i18n("Assets_Trainer")(),
                id: 'ca-asset-trainer'
            });
        }


    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedIndex(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}
}

export default {
    viewModel: ScreenBase.createFactory(AssetMenuScreen),
    template: templateMarkup
};
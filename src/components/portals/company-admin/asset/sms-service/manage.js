﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import ScreenHelper from "../../../screenhelper";
import WebRequestSMSService from "../../../../../app/frameworks/data/apitrackingcore/webRequestSMSService";
import { Enums } from "../../../../../app/frameworks/constant/apiConstant";

class SMSServiceManageScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties
        this.mode = this.ensureNonObservable(params.mode);
        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("Assets_CreateSMSService")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("Assets_UpdateSMSService")());
                break;
        }

        this.id = this.ensureNonObservable(params.id, 0);
        this.maxItems = ko.observable();
        this.limitRate = ko.observable();
        this.startDate = ko.observable();
        this.endDate = ko.observable();
        this.enable = ko.observable();
        this.dateFormat = WebConfig.companySettings.shortDateFormat;
        this.enableOptions = ScreenHelper.createYesNoObservableArray();
    }

    /**
     * Get WebRequest specific for SMS Service module in Web API access.
     * @readonly
     */
    get webRequestSMSService() {
        return WebRequestSMSService.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            this.webRequestSMSService.getSMSService(this.id).done((response) => {
                this.maxItems(response.maxItems);
                this.limitRate(response.limitRate);
                this.startDate(response.startDate);
                this.endDate(response.endDate);
                this.enable(ScreenHelper.findOptionByProperty(this.enableOptions, "value", response.enable));
                dfd.resolve();
            }).fail((e) => {
                dfd.reject(e);
            });
        }
        else {
            dfd.resolve();
        }

        return dfd;
    }

    setupExtend() {
        this.maxItems.extend({ trackChange: true });
        this.limitRate.extend({ trackChange: true });
        this.startDate.extend({ trackChange: true });
        this.endDate.extend({ trackChange: true });
        this.enable.extend({ trackChange: true });

        // validation
        this.maxItems.extend({ required: true, min: 1 });
        this.limitRate.extend({ required: true, min: 0 });
        this.startDate.extend({ required: true });
        this.endDate.extend({ required: true });
        this.enable.extend({ required: true });

        this.validationModel = ko.validatedObservable({
            maxItems: this.maxItems,
            limitRate: this.limitRate,
            startDate: this.startDate,
            endDate: this.endDate,
            enable: this.enable
        });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);

            var info = {
                companyId: WebConfig.userSession.currentCompanyId,
                maxItems: this.maxItems(),
                limitRate: this.limitRate(),
                startDate: this.startDate(),
                endDate: this.endDate(),
                enable: this.enable().value,
                infoStatus: this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update,
                id: this.id
            };

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestSMSService.createSMSService(info, true).done((response) => {
                        this.publishMessage("ca-asset-sms-service-created", response.id);
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.webRequestSMSService.updateSMSService(info).done(() => {
                        this.publishMessage("ca-asset-sms-service-updated", this.id);
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });
                    break;
            }
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(SMSServiceManageScreen),
    template: templateMarkup
};
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";

class SMSServiceFilter {
    constructor (startDate = null, endDate = null, enable = null) {
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.enable = enable;
        this.sortingColumns = DefaultSorting.SMSService;
    }
}

export { SMSServiceFilter };
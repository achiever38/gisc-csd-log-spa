﻿import ko from "knockout";
import templateMarkup from "text!./maintenance.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import { Constants } from "../../../../../app/frameworks/constant/apiConstant";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestBoxMaintenance from "../../../../../app/frameworks/data/apitrackingcore/webRequestBoxMaintenance";

class BoxMaintenanceListScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties

        this.bladeTitle(this.i18n("Assets_BoxMaintenances")());
        this.bladeSize = BladeSize.Medium;

        this.boxId = this.ensureNonObservable(params.boxId);
        this.boxMaintenances = ko.observableArray([]);
        this.selectedBoxMaintenance = ko.observable();
        this.recentChangedRowIds = ko.observableArray([]);
        this.order = ko.observable([[ 0, "desc" ]]);

        this.filter = {
            companyId: WebConfig.userSession.currentCompanyId,
            boxIds: [ this.boxId ],
            sortingColumns: DefaultSorting.BoxMaintenance
        };

        this._selectingRowHandler = (row) => {
            if (row) {
                return this.navigate("ca-asset-box-view-maintenance-view", { id: row.id });
            }
            return false;
        };

        // Subscribe Message when box maintenance created
        this.subscribeMessage("ca-asset-box-maintenance-created", (info) => {
            this.refreshDatasource().done(() => {
                this.recentChangedRowIds.replaceAll([info.id]);
            });
        });

        // Subscribe Message when box maintenance updated
        this.subscribeMessage("ca-asset-box-maintenance-updated", (info) => {
            this.refreshDatasource();
        });

        // Subscribe Message when box maintenance deleted
        this.subscribeMessage("ca-asset-box-maintenance-deleted", () => {
            this.refreshDatasource();
            this.selectedBoxMaintenance(null);
        });
    }

    /**
     * Get WebRequest specific for Box Maintenance module in Web API access.
     * @readonly
     */
    get webRequestBoxMaintenance() {
        return WebRequestBoxMaintenance.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        this.webRequestBoxMaintenance.listBoxMaintenance(this.filter).done((response) => {
            this.boxMaintenances(response.items);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedBoxMaintenance(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.CreateBoxMaintenance)) {
            commands.push(this.createCommand("cmdAdd", this.i18n("Common_Add")(), "svg-cmd-add"));
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdAdd":
                this.navigate("ca-asset-box-view-maintenance-manage", { mode: Screen.SCREEN_MODE_CREATE, boxId: this.boxId });
                break;
        }

        this.selectedBoxMaintenance(null);
        this.recentChangedRowIds([]);
    }
    
    /**
     * Refresh DataTable Datasource
     */
    refreshDatasource() {
        this.isBusy(true);
        return this.webRequestBoxMaintenance.listBoxMaintenance(this.filter).done((response) => {
            this.boxMaintenances.replaceAll(response.items);
            this.isBusy(false);
        }).fail((e) => {
            this.handleError(e);
            this.isBusy(false);
        });
    }
}

export default {
    viewModel: ScreenBase.createFactory(BoxMaintenanceListScreen),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestVendor from "../../../../../app/frameworks/data/apitrackingcore/webRequestVendor";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestBoxTemplate from "../../../../../app/frameworks/data/apitrackingcore/webRequestBoxTemplate";
import { BoxFilter } from "./infos";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";

class BoxSearchScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Common_Search")());

        this.searchFilter = this.ensureNonObservable($.extend(true, {}, params.searchFilter));

        this.vendor = ko.observable();
        this.businessUnitId = ko.observable(null);
        this.status = ko.observable();
        this.template = ko.observable();
        this.isForRent = ko.observable();
        this.includeSubBusinessUnitEnable = ko.pureComputed(() => {return !_.isEmpty(this.businessUnitId());});
        this.includeSubBusinessUnit = ko.observable(false);
        this.vendorOptions = ko.observableArray([]);
        this.businessUnitOptions = ko.observableArray([]);
        this.statusOptions = ko.observableArray([]);
        this.templateOptions = ko.observableArray([]);
        this.isForRentOptions = ScreenHelper.createYesNoObservableArray();
    }
    /**
     * Get WebRequest specific for Vendor module in Web API access.
     * @readonly
     */
    get webRequestVendor() {
        return WebRequestVendor.getInstance();
    }
    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }
    /**
     * Get WebRequest specific for Box Template module in Web API access.
     * @readonly
     */
    get webRequestBoxTemplate() {
        return WebRequestBoxTemplate.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        var d1 = this.webRequestVendor.listVendor({ sortingColumns: DefaultSorting.Vendor });
        var d2 = this.webRequestBusinessUnit.listBusinessUnitSummary({ companyId: WebConfig.userSession.currentCompanyId, includeAssociationNames: [ EntityAssociation.BusinessUnit.ChildBusinessUnits ], sortingColumns: DefaultSorting.BusinessUnit });
        var d3 = this.webRequestEnumResource.listEnumResource({ companyId: WebConfig.userSession.currentCompanyId, types: [ Enums.ModelData.EnumResourceType.BoxStatus ], sortingColumns: DefaultSorting.EnumResource });
        var d4 = this.webRequestBoxTemplate.listBoxTemplateSummary({ sortingColumns: DefaultSorting.BoxTemplate });

        $.when(d1, d2, d3, d4).done((r1, r2, r3, r4) => {
            this.vendorOptions(r1["items"]);
            this.businessUnitOptions(r2["items"]);
            this.statusOptions(r3["items"]);
            this.templateOptions(r4["items"]);

            if (this.searchFilter.vendorId) {
                this.vendor(ScreenHelper.findOptionByProperty(this.vendorOptions, "id", this.searchFilter.vendorId));
            }
            if (this.searchFilter.businessUnitId) {
                this.businessUnitId(this.searchFilter.businessUnitId.toString());
            }
            if (this.searchFilter.boxStatus) {
                this.status(ScreenHelper.findOptionByProperty(this.statusOptions, "value", this.searchFilter.boxStatus));
            }
            if (this.searchFilter.boxTemplateId) {
                this.template(ScreenHelper.findOptionByProperty(this.templateOptions, "id", this.searchFilter.boxTemplateId));
            }
            if (this.searchFilter.isForRent) {
                this.isForRent(ScreenHelper.findOptionByProperty(this.stockOptions, "value", this.searchFilter.isForRent));
            }

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSearch", this.i18n("Common_Search")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdClear", this.i18n("Common_Clear")(), "svg-cmd-clear"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSearch") {
            this.searchFilter = new BoxFilter(
                this.vendor() ? this.vendor().id : null,
                null,
                this.status() ? this.status().value : null,
                this.template() ? this.template().id : null,
                this.isForRent() ? this.isForRent().value : null
            );

            // Default selection is single.
            if(this.includeSubBusinessUnitEnable()){
                var selectedBusinessUnitId = parseInt(this.businessUnitId());

                // If user included sub children then return all business unit ids.
                if(this.includeSubBusinessUnit()){
                    this.searchFilter.businessUnitIds = ScreenHelper.findBusinessUnitsById(this.businessUnitOptions(), selectedBusinessUnitId);
                }
                else {
                    // Single selection for business unit.
                    this.searchFilter.businessUnitIds = [selectedBusinessUnitId];
                }
            }

            this.publishMessage("ca-asset-box-search-filter-changed", this.searchFilter);
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdClear") {
            this.vendor(null);
            this.businessUnitId(null);
            this.status(null);
            this.template(null);
            this.isForRent(null);
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(BoxSearchScreen),
    template: templateMarkup
};
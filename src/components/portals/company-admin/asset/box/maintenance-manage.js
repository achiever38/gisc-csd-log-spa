﻿import ko from "knockout";
import templateMarkup from "text!./maintenance-manage.html";
import ScreenBase from "../../../screenbase";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import { Enums } from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestBoxMaintenance from "../../../../../app/frameworks/data/apitrackingcore/webRequestBoxMaintenance";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";

class BoxMaintenanceManageScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties

        this.mode = this.ensureNonObservable(params.mode);

        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("Assets_AddBoxMaintenance")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("Assets_UpdateBoxMaintenance")());
                break;
        }

        this.id = this.ensureNonObservable(params.id, 0);
        this.boxId = this.ensureNonObservable(params.boxId, 0);
        this.dateTime = ko.observable();
        this.description = ko.observable();
        this.author = ko.observable();

        this.dateFormat = WebConfig.companySettings.shortDateFormat;
        this.timeFormat = WebConfig.companySettings.shortTimeFormat;
    }

    /**
     * Get WebRequest specific for Box Maintenance module in Web API access.
     * @readonly
     */
    get webRequestBoxMaintenance() {
        return WebRequestBoxMaintenance.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            this.webRequestBoxMaintenance.getBoxMaintenance(this.id).done((response) => {
                this.boxId = response.boxId;
                this.dateTime(response.dateTime);
                this.description(response.description);
                this.author(response.author);
                dfd.resolve();
            }).fail((e) => {
                dfd.reject(e);
            });
        }
        else {
            dfd.resolve();
        }

        return dfd;
    }

    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        this.dateTime.extend({ trackChange: true });
        this.description.extend({ trackChange: true });
        this.author.extend({ trackChange: true });

        // validation
        this.dateTime.extend({ required: true });
        this.description.extend({ required: true });
        this.author.extend({ required: true });

        this.validationModel = ko.validatedObservable({
            dateTime: this.dateTime,
            description: this.description,
            author: this.author,
        });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);

            var info = {
                boxId: this.boxId,
                dateTime: this.dateTime(),
                description: this.description(),
                author: this.author(),
                infoStatus: this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update,
                id: this.id
            };

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestBoxMaintenance.createBoxMaintenance(info, true).done((response) => {
                        this.isBusy(false);
                        this.publishMessage("ca-asset-box-maintenance-created", response);
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                        this.isBusy(false);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.webRequestBoxMaintenance.updateBoxMaintenance(info, true).done((response) => {
                        this.isBusy(false);
                        this.publishMessage("ca-asset-box-maintenance-updated", response);
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                        this.isBusy(false);
                    });
                    break;
            }
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(BoxMaintenanceManageScreen),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Enums, Constants } from "../../../../../app/frameworks/constant/apiConstant";
import { TrainerFilter } from './infos';
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestAssetTrainer from '../../../../../app/frameworks/data/apitrackingcore/webRequestAssetTrainer';
import Utility from "../../../../../app/frameworks/core/utility";

class AssetTraineListScreen extends ScreenBase
{
    constructor(params)
    {
        super(params);

        let self = this;

        this.order = ko.observable([[ 1, "asc" ]]);
        this.pageSizeOptions = ko.observableArray([10, 20, 50, 100]);
        this.apiDataSource = ko.observableArray([]);
        this.searchFilter = new TrainerFilter();

        this.searchFilterDefault = new TrainerFilter();

        this.pageFiltering = ko.observable(false);
        
        this.bladeSize = BladeSize.XLarge;
        // this.bladeIsMaximized = true;

        this.bladeTitle(this.i18n("Assets_Trainer_List")());

        this.canCreate = ko.pureComputed(function() {
            return WebConfig.userSession.hasPermission(Constants.Permission.CreateTrainer);
            //return true;
        }, this);

        this.canUpdate = ko.pureComputed(function() {
            return WebConfig.userSession.hasPermission(Constants.Permission.UpdateTrainer);
            //return true;
        }, this);

        


        this.onSelectingRow = (obj, e) =>  {
            if(obj) {
                self.navigate("ca-asset-trainer-view", { id : obj.id });
            }
        }

        this.subscribeMessage("ca-asset-trainer-changed", (res) => {
            this.refreshDataSource();
        });

        this.subscribeMessage("ca-asset-trainer-search-filter-changed", (searchFilter) => {
            this.searchFilter = searchFilter;
            
            // apply active command state if searchFilter does not match default search
            //this.pageFiltering(!_.isEqual(this.searchFilter, this.searchFilterDefault));

            this.refreshDataSource();
        });
    }

    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;
    }

    onUnload() {}

    onChildScreenClosed() {}

    buildActionBar(actions) {}

    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add", this.canCreate));
        commands.push(this.createCommand("cmdImport", this.i18n("Common_Import")(), "svg-cmd-import", this.canCreate));
        commands.push(this.createCommand("cmdImportPic", this.i18n("Common_ImportPicture")(), "svg-cmd-import-picture", this.canCreate));
        commands.push(this.createCommand("cmdExport", this.i18n("Common_Export")(), "svg-cmd-export"));
    }

    onActionClick(sender) {
        super.onActionClick(sender);
    }

    refreshDataSource() {
        this.dispatchEvent("dgAssetTrainers", "refresh");
    }

    onDatasourceRequestRead(gridOption) {
        this.isBusy(true);
        let dfd = $.Deferred();    
        let filter = Object.assign({}, gridOption, this.searchFilter);
        this.webRequestAssetTrainer.listTrainerSummary(filter).done((res) => {
            dfd.resolve({
                items: res.items,
                totalRecords: res.totalRecords
            });
        }).fail((e)=> {
            this.handleError(e);
        }).always(()=> {
            this.isBusy(false);
        });


        return dfd;
    }

    onCommandClick(sender) {
        switch(sender.id) {
            case "cmdCreate": 
                this.navigate("ca-asset-trainer-manage", { mode: "create"});
                break;
            case "cmdImport": 
                this.navigate("ca-asset-trainer-import");
                break;
            case "cmdImportPic": 
                this.navigate("ca-asset-trainer-import-picture");
                break;
            case "cmdExport": 
                this.showMessageBox(null, this.i18n("M128")(), BladeDialog.DIALOG_YESNO).done((button)=> {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            var filter = $.extend(true, {}, this.searchFilter);
                            filter.templateFileType = Utility.isiOSDevice() ? Enums.TemplateFileType.Xls : Enums.TemplateFileType.Xlsx;

                            filter.sortingColumns = this.generateSortingColumns();

                            this.webRequestAssetTrainer.exportTrainer(filter).done((response) => {
                                this.isBusy(false);
                                ScreenHelper.downloadFile(response.fileUrl);
                            }).fail((e) => {
                                this.isBusy(false);
                                this.handleError(e);
                            });
                            this.isBusy(false);
                            break;
                    }
                });
                break;
            default: 
                break;
        }
    }

    generateSortingColumns(){
        
        var sortingColumns = [{},{}];

        sortingColumns[0].direction = this.order()[0][1] === "asc" ? 1 : 2 ;

         switch (this.order()[0][0]) {
            case 1:
                 sortingColumns[0].column = Enums.SortingColumnName.EmployeeId;
                break;
        
            case 2:
                 sortingColumns[0].column = Enums.SortingColumnName.FirstName;
                break;
            case 3:
                 sortingColumns[0].column = Enums.SortingColumnName.LastName;
                break;
        
            case 4:
                 sortingColumns[0].column = Enums.SortingColumnName.BusinessUnitName;
                break;        
        }

        sortingColumns[1].column = Enums.SortingColumnName.Id;
        sortingColumns[1].direction = sortingColumns[0].direction;

        return sortingColumns;
    }

    get webRequestAssetTrainer() {
        return WebRequestAssetTrainer.getInstance();
    }
}

export default {
    viewModel: ScreenBase.createFactory(AssetTraineListScreen),
    template: templateMarkup
};
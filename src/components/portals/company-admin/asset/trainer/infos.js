﻿import ko from "knockout";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import DefaultSoring from "../../../../../app/frameworks/constant/defaultSorting";
import {Enums,EntityAssociation} from "../../../../../app/frameworks/constant/apiConstant";

class BusinessUnitFilter {
    constructor () {
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.sortingColumns = DefaultSoring.BusinessUnit;
    }
}
export { BusinessUnitFilter };

class EnumResourceFilter {
    constructor () {
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.types = [Enums.ModelData.EnumResourceType.DriverCardType, Enums.ModelData.EnumResourceType.Gender, Enums.ModelData.EnumResourceType.DriverLicenseType];
        this.sortingColumns = DefaultSoring.EnumResource;
    }
}
export { EnumResourceFilter };

class TrainerFilter {
    constructor() {
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.businessUnitIds = [];
    }
}
export { TrainerFilter };
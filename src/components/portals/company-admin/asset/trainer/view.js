﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Enums, Constants } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestAssetTrainer from '../../../../../app/frameworks/data/apitrackingcore/webRequestAssetTrainer';

class AssetTraineViewScreen extends ScreenBase
{
    constructor(params)
    {
        super(params);

        this.bladeTitle(this.i18n("Assets_Trainer_View")());
        this.bladeSize = BladeSize.Small;

        this.id = this.ensureNonObservable(params.id, null);

        this.pictureUrl = ko.observable();
        this.pictureFileName = ko.observable();
        this.employeeId = ko.observable();
        this.nationalId = ko.observable();
        this.gender = ko.observable();
        this.title = ko.observable();
        this.firstName = ko.observable();
        this.lastName = ko.observable();
        this.businessUnit = ko.observable();
        this.phone = ko.observable();
        this.formatCreateDate = ko.observable();
        this.createBy = ko.observable();
        this.formatUpdateDate = ko.observable();
        this.updateBy = ko.observable();
        this.desc = ko.observable();

        this.canEdit = ko.pureComputed(function() {
            return WebConfig.userSession.hasPermission(Constants.Permission.UpdateTrainer);
            //return true;
        }, this);

        this.canDelete = ko.pureComputed(function() {
            return WebConfig.userSession.hasPermission(Constants.Permission.DeleteTrainer);
            //return true;
        }, this);
    }

    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        this.getTrainerDetail();
    }

    getTrainerDetail() {
        this.isBusy(true);
            
        this.webRequestAssetTrainer.getTrainer(this.id, null)
        .done((res) => {
            if (res.image) {
                this.pictureUrl(res.image.fileUrl);
                this.pictureFileName(res.image.fileName);
            }else{
                this.pictureUrl(this.resolveUrl("~/images/upload-img-placeholder-100x100.jpg"));
                this.pictureFileName('');
            }

            this.employeeId(res.employeeId);
            this.nationalId(res.citizenId);
            this.gender(res.genderDisplayName);
            this.title(res.title);
            this.firstName(res.firstName);
            this.lastName(res.lastName);
            this.phone(res.phone);
            this.formatCreateDate(res.formatCreateDate);
            this.createBy(res.createBy);
            this.formatUpdateDate(res.formatUpdateDate);
            this.updateBy(res.updateBy);
            this.desc(res.description);
        })
        .fail((e) => {
            this.handleError(e);
        })
        .always(() => {
            this.isBusy(false);
        });
    }

    onUnload() {}

    onChildScreenClosed() {}

    buildActionBar(actions) {}

    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdEdit", this.i18n("Common_Update")(), "svg-cmd-edit", this.canEdit));
        commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete", this.canDelete));
    }

    onActionClick(sender) {
        super.onActionClick(sender);
    }

    onCommandClick(sender) {
        switch(sender.id) {
            case "cmdEdit" :
                this.navigate("ca-asset-trainer-manage", {  mode: "update", id: this.id });
                break;
            case "cmdDelete" :
                this.showMessageBox(null, this.i18n("M211")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);

                            this.webRequestAssetTrainer.deleteTrainer(this.id).done(() => {
                                this.isBusy(false);

                                this.publishMessage("ca-asset-trainer-changed");
                                this.close(true);
                            }).fail((e)=> {
                                this.handleError(e);
                                this.isBusy(false);
                            });
                            this.publishMessage("ca-asset-trainer-changed");
                            this.close(true);
                            break;
                    }
                });
                break;
            default:
                break;
        }
    }

    get webRequestAssetTrainer()
    {
        return WebRequestAssetTrainer.getInstance();
    }
}

export default {
    viewModel: ScreenBase.createFactory(AssetTraineViewScreen),
    template: templateMarkup
};
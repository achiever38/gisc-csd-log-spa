﻿import ko from "knockout";
import templateMarkup from "text!./maintenance-log-create.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import WebRequestVehicleMaintenancePlan from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenancePlan";
import WebRequestVehicleMaintenanceType from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenanceType";
import { Enums } from "../../../../../app/frameworks/constant/apiConstant";

/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class MaintenanceLogCreate extends ScreenBase {
    
    /**
     * Creates an instance of AssetMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.vehicleId = this.ensureNonObservable(params.vehicleId);

        this.bladeTitle(this.i18n("Vehicle_MA_Btn_Create Maintenance Log")());
        this.bladeSize = BladeSize.Medium;
        
        this.currentDate = ko.observable(new Date());
        this.maintenanceTypes = ko.observableArray([]);
        this.maintenanceSubTypes = ko.observableArray([]);

        this.distanceUnit = ko.observable();
        this.currency = ko.observable();

        this.selectedMA = ko.observable();
        this.selectedSubMA = ko.observable();
        this.title = ko.observable();
        this.description = ko.observable();
        this.selectedDate = ko.observable();
        this.displayCurrentDistance = ko.observable();
        this.actualCost = ko.observable();
        this.assignTo = ko.observable();
        this.performBy = ko.observable();
        
        this.currentDistance = ko.observable();

        this.validationModel = ko.validatedObservable({
            selectedMA: this.selectedMA,
            selectedSubMA: this.selectedSubMA,
            title: this.title,
            actualCost: this.actualCost,
            performBy: this.performBy,
            selectedDate: this.selectedDate
        });
    }

    setupExtend() {
        this.selectedMA.extend({ trackChange: true });
        this.selectedSubMA.extend({ trackChange: true });
        this.title.extend({ trackChange: true });
        this.description.extend({ trackChange: true });
        this.selectedDate.extend({ trackChange: true });
        this.displayCurrentDistance.extend({ trackChange: true });
        this.actualCost.extend({ trackChange: true });
        this.assignTo.extend({ trackChange: true });
        this.performBy.extend({ trackChange: true });

        this.selectedMA.extend({ required: true });
        this.selectedSubMA.extend({ required: true });
        this.title.extend({ required: true });
        this.actualCost.extend({ required: true });
        this.performBy.extend({ required: true });
        this.selectedDate.extend({ required: true });
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) {
            return;
        }

        let dfd = $.Deferred();

        let dfdCompanySettings = this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId).done((response) => {
            this.distanceUnit(response.distanceUnitSymbol);
            this.currency(response.currencySymbol);
        });

        let dfdVehicleCurrentDistance = this.webRequestVehicle.getCurrentDistance(this.vehicleId).done((response) => {
            this.currentDistance(response.currentDistance);
            this.displayCurrentDistance(response.formatCurrentDistance);
        });

        let dfdMaintenanceType = this.webRequestVehicleMaintenanceType.listVehicleMaintenanceType({ companyId: WebConfig.userSession.currentCompanyId }).done((response) => {
            this.maintenanceTypes(response["items"]);
        });

        this.selectedMA.subscribe((ddlValue) => {
            if (ddlValue != null){
                var maTypeID = ddlValue.id;
                var maintenanceSubTypeFilter = {
                    companyId: WebConfig.userSession.currentCompanyId,
                    maintenanceTypeId: maTypeID
                };

                this.webRequestVehicleMaintenanceType.listVehicleMaintenanceSubType(maintenanceSubTypeFilter).done((responseMASubType) => {
                    this.maintenanceSubTypes(responseMASubType["items"]);
                });

            }
        });

        this.selectedSubMA.subscribe((ddlValue) => {
            if (this.selectedMA() != null && this.selectedSubMA() != null){
                this.title(this.selectedMA().name + " " + this.i18n("Vehicle_MA_Common_Type")() + " "  + this.selectedSubMA().name);
            }
        });

        $.when(dfdCompanySettings, dfdVehicleCurrentDistance, dfdMaintenanceType).done(() => {
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        
        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {

            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);

            var info = {
                vehicleMaintenanceType: Enums.ModelData.VehicleMaintenanceType.Log,
                maintenanceSubTypeId: this.selectedSubMA().id,
                //maintenanceTypeId: this.selectedMA().id,
                companyId: WebConfig.userSession.currentCompanyId,
                vehicleIds: [this.vehicleId],
                title: this.title(),
                description: this.description(),
                actualDate: this.selectedDate(),
                maintenanceDate: this.selectedDate(),
                currentDistance: this.currentDistance(),
                maintenanceStatus: Enums.ModelData.MaintenanceStatus.Completed,
                actualCost: this.actualCost(),
                assignTo:this.assignTo(),
                performBy:this.performBy(),
                actualDistance: this.currentDistance()
            };

            this.webRequestVehicleMaintenancePlan.createVehicleMaintenancePlan(info, true).done((response) => {
                this.isBusy(false);
                this.publishMessage("ca-asset-vehicle-maintenance-plan-created", response);
                this.close(true);
            }).fail((e) => {
                this.handleError(e);
                this.isBusy(false);
            });
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}

    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }

    get webRequestVehicleMaintenancePlan() {
        return WebRequestVehicleMaintenancePlan.getInstance();
    }

    get webRequestVehicleMaintenanceType() {
        return WebRequestVehicleMaintenanceType.getInstance();
    }
}

export default {
viewModel: ScreenBase.createFactory(MaintenanceLogCreate),
    template: templateMarkup
};
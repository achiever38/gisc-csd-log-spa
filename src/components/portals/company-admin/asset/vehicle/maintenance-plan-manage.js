﻿import ko from "knockout";
import templateMarkup from "text!./maintenance-plan-manage.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import WebRequestVehicleMaintenancePlan from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenancePlan";
import WebRequestVehicleMaintenanceType from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenanceType";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";

class VehicleMaintenancePlanManageScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties

        this.mode = this.ensureNonObservable(params.mode);
        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("Assets_CreateMaintenancePlan")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("Assets_UpdateMaintenancePlan")());
                break;
        }


        //var tomorrow = new Date();
        //tomorrow.setDate(tomorrow.getDate() + 1);
        this.allowedDate = ko.observable(new Date());

        this.id = this.ensureNonObservable(params.id, 0);
        this.vehicleId = this.ensureNonObservable(params.vehicleId);
        this.maintenanceType = ko.observableArray([]);
        this.maintenanceSubtype = ko.observableArray([]);
        this.maType = ko.observable();
        this.maSubType = ko.observable();

        this.maTypeUpdate = this.ensureNonObservable(0);
        this.maSubTypeUpdate = this.ensureNonObservable(0);
        this.maTypeUpdateText = ko.observable();
        this.maSubTypeUpdateText = ko.observable();
        
        this.title = ko.observable();
        this.description = ko.observable();
        this.maintenanceDate = ko.observable();
        this.dayRemind = ko.observable();
        this.currentDistance = ko.observable();
        this.displayCurrentDistance = ko.observable();
  
        this.maintenanceDistance = ko.observable();
        this.distanceRemind = ko.observable();
        this.estimateCost = ko.observable();
        this.enableWebsiteAlert = ko.observable(true);
        this.enableEmailAlert = ko.observable();
        this.enableSmsAlert = ko.observable();
        this.maintenanceStatus = Enums.ModelData.MaintenanceStatus.Pending;
        this.dateFormat = WebConfig.companySettings.shortDateFormat;
        this.distanceUnit = ko.observable();
        this.currency = ko.observable();

        this.shouldDisplayDDL = ko.pureComputed(() => {
            var isDisplay = true;
            isDisplay = this.mode == Screen.SCREEN_MODE_CREATE ? true : false;
            return isDisplay;
        });

        this.shouldDisplayMATypeLabel = ko.pureComputed(() => {
            var isDisplay = true;
            isDisplay = this.mode == Screen.SCREEN_MODE_CREATE ? false : true;
            return isDisplay;
        });

    }

    /**
     * Get WebRequest specific for Company module in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }

    /**
     * Get WebRequest specific for Vehicle Maintenance Plan module in Web API access.
     * @readonly
     */
    get webRequestVehicleMaintenancePlan() {
        return WebRequestVehicleMaintenancePlan.getInstance();
    }


    get webRequestVehicleMaintenanceType() {
        return WebRequestVehicleMaintenanceType.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }


        var dfd = $.Deferred();


        var dfdMaintenanceType = null;
        var dfdMaintenanceSubtype = null;

       

        var dfdCompanySettings = null;
        var dfdVehicleCurrentDistance = null;
        var dfdVehicleMaintenancePlan = null;
        
        var maintenanceTypeFilter = {
            companyId: WebConfig.userSession.currentCompanyId
        };

        dfdMaintenanceType = this.webRequestVehicleMaintenanceType.listVehicleMaintenanceType(maintenanceTypeFilter).done((response) => {
            this.maintenanceType(response["items"]);
            //this.maType(ScreenHelper.findOptionByProperty(this.maintenanceType, "id"));
        });

        this.maType.subscribe((ddlValue) => {
            if (ddlValue != null){
                var maTypeID = ddlValue.id;
                var maintenanceSubTypeFilter = {
                    companyId: WebConfig.userSession.currentCompanyId,
                    maintenanceTypeId: maTypeID
                };
                dfdMaintenanceSubtype = this.webRequestVehicleMaintenanceType.listVehicleMaintenanceSubType(maintenanceSubTypeFilter).done((responseMASubType) => {
                    this.maintenanceSubtype(responseMASubType["items"]);
                    this.maSubType(ScreenHelper.findOptionByProperty(this.maintenanceSubtype, "id"));
                });

                $.when(dfdMaintenanceSubtype).done(() => {
                    dfdMaintenanceSubtype.resolve();
                }).fail((e) => {
                    dfdMaintenanceSubtype.reject(e);
                });
            }
        });

        this.maSubType.subscribe((ddlValue) => {
            if (this.maType() != null && this.maSubType() != null){
                this.title(this.maType().name + " " + this.i18n("Vehicle_MA_Common_Type")() + " "  + this.maSubType().name);
            }
        });

       

        

        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            dfdVehicleMaintenancePlan = this.webRequestVehicleMaintenancePlan.getVehicleMaintenancePlan(this.id, [EntityAssociation.VehicleMaintenancePlan.CurrentDistance]).done((response) => {

                this.maTypeUpdate = response.maintenanceTypeId;
                this.maSubTypeUpdate = response.maintenanceSubTypeId;
                this.maTypeUpdateText(response.maintenanceTypeName);
                this.maSubTypeUpdateText(response.maintenanceSubTypeName);
                //this.vehicleId = response.vehicleId;
                this.title(response.title);
                this.description(response.description);
                this.maintenanceDate(response.maintenanceDate);
                this.dayRemind(response.dayRemind);
                this.currentDistance(response.currentDistance);
                this.displayCurrentDistance(response.formatCurrentDistance);
                
                var formatMaintenanceDistance = response.formatMaintenanceDistance;
                if (formatMaintenanceDistance != undefined && formatMaintenanceDistance != null) {
                    while (formatMaintenanceDistance.indexOf(",") != -1) {
                        formatMaintenanceDistance = formatMaintenanceDistance.replace(",", "");
                    }

                    formatMaintenanceDistance = parseInt(formatMaintenanceDistance, 10);
                }
               


                var formatDistanceRemind = response.formatDistanceRemind;
                if (formatDistanceRemind != undefined && formatDistanceRemind != null) {
                    while (formatDistanceRemind.indexOf(",") != -1) {
                        formatDistanceRemind = formatDistanceRemind.replace(",", "");
                    }
                    formatDistanceRemind = parseInt(formatDistanceRemind, 10);
                }
   

                this.maintenanceDistance(formatMaintenanceDistance);
                this.distanceRemind(formatDistanceRemind);
                this.estimateCost(response.estimateCost);
                this.enableWebsiteAlert(response.enableWebsiteAlert);
                this.enableEmailAlert(response.enableEmailAlert);
                this.enableSmsAlert(response.enableSmsAlert);
                this.maintenanceStatus = response.maintenanceStatus;
                //this.distanceUnit(response.distanceUnitSymbol);
                //this.currency(response.currencySymbol);
            });
        } else {
            
        }

        dfdCompanySettings = this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId).done((response) => {
            this.distanceUnit(response.distanceUnitSymbol);
            this.currency(response.currencySymbol);
        });

        dfdVehicleCurrentDistance = this.webRequestVehicle.getCurrentDistance(this.vehicleId).done((response) => {
            this.currentDistance(response.currentDistance);
            this.displayCurrentDistance(response.formatCurrentDistance);
        });



        $.when(dfdMaintenanceType, dfdMaintenanceSubtype, dfdCompanySettings, dfdVehicleMaintenancePlan, dfdVehicleCurrentDistance ).done(() => {
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return  dfd;
    }

    setupExtend() {
        this.maType.extend({ trackChange: true });
        this.maSubType.extend({ trackChange: true });
        this.maTypeUpdateText.extend({ trackChange: true });
        this.maSubTypeUpdateText.extend({ trackChange: true });
        this.title.extend({ trackChange: true });
        this.description.extend({ trackChange: true });
        this.maintenanceDate.extend({ trackChange: true });
        this.dayRemind.extend({ trackChange: true });
        this.maintenanceDistance.extend({ trackChange: true });
        this.distanceRemind.extend({ trackChange: true });
        this.estimateCost.extend({ trackChange: true });
        this.enableWebsiteAlert.extend({ trackChange: true });
        this.enableEmailAlert.extend({ trackChange: true });
        this.enableSmsAlert.extend({ trackChange: true });

        // validation        
        if (this.mode == Screen.SCREEN_MODE_CREATE) {
            this.maType.extend({ required: true });
            this.maSubType.extend({ required: true });
        }
        
        this.title.extend({ required: true });
        this.maintenanceDate.extend({ 
            required: {
                onlyIf: () => {
                    return _.isNil(this.maintenanceDistance()) 
                        || this.maintenanceDistance() == "" 
                        || (!_.isNil(this.dayRemind()) && this.dayRemind() != "");
                }
            },
            validation: {
                validator: () => {
                    var isValidate = true;
                    if (this.maintenanceDate() != null && this.maintenanceDate() != "") {
                        var arrSplit = (this.maintenanceDate()).split('T');
                        var arrSplitDates = arrSplit[0].split('-');

                        var maintenanceDate = new Date(arrSplitDates[0], (parseInt(arrSplitDates[1], 10) - 1), parseInt(arrSplitDates[2], 10) + 1);
                        //var dateCompare = new Date(arrSplitDates[0], (parseInt(arrSplitDates[1], 10) - 1), arrSplitDates[2]);
                        var dateCompare = new Date();
                        if (this.dayRemind() != undefined && this.dayRemind() != null && this.dayRemind() != "") {
                            dateCompare.setDate(dateCompare.getDate() + parseInt(this.dayRemind().toString(), 10));
                        }
                        if (dateCompare > maintenanceDate) {
                            isValidate = false;
                        }
                    }

                    return isValidate;
                },
                message: this.i18n("M146")()
            }
        });
        this.dayRemind.extend({ min: 0 });
        this.maintenanceDistance.extend({ 
            min: 1,
            required: {
                onlyIf: () => {
                    return _.isNil(this.maintenanceDate()) 
                        || (!_.isNil(this.distanceRemind()) && this.distanceRemind() != "")
                }
            },
            validation: [
                {
                    validator: () => {
                        let isValidate = true;
                        if (this.maintenanceDistance() != null && this.maintenanceDistance() != "" &&
                            this.distanceRemind() != null && this.distanceRemind() != "") {
                            if (this.maintenanceDistance() < this.distanceRemind()) {
                                isValidate = false;
                            }
                        }
                        return isValidate;
                    },
                    message: this.i18n("M161")()
                },
                
                {
                validator: () => {
                    let isValidate = true;
                    if (this.maintenanceDistance() != null && this.maintenanceDistance() != "" &&
                        this.currentDistance() != null && this.currentDistance() != "") {
                        var compareDistance = parseInt(this.maintenanceDistance(), 10);

                        if (this.distanceRemind() != undefined && this.distanceRemind() != null && this.distanceRemind != ""){
                            compareDistance -= parseInt(this.distanceRemind(),10);
                        }
                        if (compareDistance < this.currentDistance()) {
                            isValidate = false;
                        }
                    }
                    return isValidate;
                },
                message: this.i18n("M153")()
            }
            ]
         });
        this.distanceRemind.extend({ min: 0 });
        this.estimateCost.extend({ min: 0 });

        var paramsValidation = {
            title: this.title,
            maintenanceDate: this.maintenanceDate,
            dayRemind: this.dayRemind,
            maintenanceDistance: this.maintenanceDistance,
            distanceRemind: this.distanceRemind,
            estimateCost: this.estimateCost
        };

        if (this.mode == Screen.SCREEN_MODE_CREATE){
            paramsValidation.maintenanceType = this.maType;
            paramsValidation.maintenanceSubtype = this.maSubType;
        }
        this.validationModel = ko.validatedObservable(paramsValidation);
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            var info = {
                vehicleMaintenanceType: Enums.ModelData.VehicleMaintenanceType.Plan,//(this.mode == Screen.SCREEN_MODE_CREATE ? 1 : this.maTypeUpdate),//this.maType().id,
                maintenanceSubTypeId: (this.mode == Screen.SCREEN_MODE_CREATE ? this.maSubType().id : this.maSubTypeUpdate),
                companyId: WebConfig.userSession.currentCompanyId,
                vehicleIds: [this.vehicleId],
                //vehicleId: this.vehicleId,
                title: this.title(),
                description: this.description(),
                maintenanceDate: this.maintenanceDate(),
                dayRemind: this.dayRemind(),
                maintenanceDistance: this.maintenanceDistance(),
                distanceRemind: this.distanceRemind(),
                currentDistance: this.currentDistance(),
                estimateCost: this.estimateCost(),
                enableWebsiteAlert: this.enableWebsiteAlert(),
                enableEmailAlert: this.enableEmailAlert(),
                enableSmsAlert: this.enableSmsAlert(),
                maintenanceStatus: this.maintenanceStatus,
                infoStatus: this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update,
                id: this.id
            };

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestVehicleMaintenancePlan.createVehicleMaintenancePlan(info, true).done((response) => {
                        this.isBusy(false);
                        this.publishMessage("ca-asset-vehicle-maintenance-plan-created", response);
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                        this.isBusy(false);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.webRequestVehicleMaintenancePlan.updateVehicleMaintenancePlan(info, true).done((response) => {
                        this.isBusy(false);
                        this.publishMessage("ca-asset-vehicle-maintenance-plan-updated", response);
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                        this.isBusy(false);
                    });
                    break;
            }
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(VehicleMaintenancePlanManageScreen),
    template: templateMarkup
};
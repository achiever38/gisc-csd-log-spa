﻿import ko from "knockout";
import templateMarkup from "text!./maintenance-plan-view-cancel.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestVehicleMaintenancePlan from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenancePlan";
import WebRequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicle";
import { Constants, Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class MaintenancePlanCancel extends ScreenBase {
    
    /**
     * Creates an instance of AssetMenuScreen.
     * 
     * @param {any} params
     */

    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Asset_Vehicle_Maintenance_Plan_Cancel")());

        
        this.bladeSize = BladeSize.Medium;
       
        this.items = ko.observableArray([]);
        this.vehicleData = params.vehicle;
        this.appointment = params.appointment;
        this.subTypeId = null;
        this.selectedItems = ko.observableArray([]);
        this.order = ko.observable([[ 0, "asc" ]]);
      
        this.id = this.ensureNonObservable(params.id);
        this.vehicleId = this.ensureNonObservable(params.vehicleId);
        this.title = ko.observable();
        this.description = ko.observable();
        this.formatMaintenanceDate = ko.observable();
        this.dayRemind = ko.observable();
        this.remainingMaintenanceDay = ko.observable();
        this.currentDistance = ko.observable();
        this.displayCurrentDistance = ko.observable();
        this.maintenanceDistance = ko.observable();
        this.distanceRemind = ko.observable();
        this.remainingDistance = ko.observable();
        this.estimateCost = ko.observable();
        this.formatAlertVia = ko.observable();
        this.maintenanceStatus = ko.observable();
        this.maintenanceStatusDisplayName = ko.observable();
      
        this.distanceUnit = ko.observable();
        this.currency = ko.observable();
        this.remark = ko.observable();
       
        this.maintenanceSubTypeName = ko.observable();
        this.maintenanceTypeName = ko.observable();
        this.infoVehicle  = null;
     
      
    
    
  
    
        this.canAppointment = ko.pureComputed(() => {
            return this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Pending || this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Overdue || this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Ready ||this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Inprogress ||this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Completed || this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Cancelled;
        });
    
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) {
            return;
        }

   

        var dfd = $.Deferred();

        var dfdListMaintenancePlan = null;
        

        var dfdMAPlan =  this.webRequestVehicleMaintenancePlan.getVehicleMaintenancePlan(this.id, [EntityAssociation.VehicleMaintenancePlan.CurrentDistance]).done((response) => {
            this.populateData(response);
            this.subTypeId = response.maintenanceSubTypeId;
            this.infoVehicle = response;
            //this.infoVehicle.remark = "Test Remark";
          
            this.items(response.checkListInfos);
       
            for(var i = 0 ; i < this.items().length ;i++){

                this.items()[i].enable =  false;
                  
            }

            
            
        });
        $.when(dfdMAPlan).done(() => {
                 
                dfd.resolve();
            }).fail((e) => {
                dfd.reject(e);
            });

            return dfd;
        

        
    }

    get webRequestVehicleMaintenancePlan() {
        return WebRequestVehicleMaintenancePlan.getInstance();
    }

    get webRequestVehicle() {
        return WebRequestVehicle.getInstance();
    }

    populateData(response) {
        this.title(response.title);
        this.description(response.description);
        this.formatMaintenanceDate(response.formatMaintenanceDate);
        this.dayRemind(response.dayRemind);
        this.remainingMaintenanceDay(response.remainingMaintenanceDay);
        // this.currentDistance(response.currentDistance);
        this.maintenanceDistance(response.formatMaintenanceDistance);

        this.distanceRemind(response.formatDistanceRemind);
        this.remainingDistance(response.formatRemainingDistance);

        this.estimateCost(response.formatEstimateCost);
        this.formatAlertVia(response.formatAlertVia);
        this.maintenanceStatus(response.maintenanceStatus);
        this.maintenanceStatusDisplayName(response.maintenanceStatusDisplayName);
        this.remark(response.remark);
 
    
        this.distanceUnit(response.distanceUnitSymbol);
        this.currency(response.currencySymbol);
        this.maintenanceTypeName(response.maintenanceTypeName);
        this.maintenanceSubTypeName(response.maintenanceSubTypeName);


      


 
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    
        if (WebConfig.userSession.hasPermission(Constants.Permission.UpdateVehicleMaintenancePlan)) {
            commands.push(this.createCommand("cmdAppointment", "Appointment", "svg-ic-mt-appo-manage", this.canAppointment));
        }
    
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    
        switch (sender.id) {
            case "cmdAppointment":
                this.navigate("ca-asset-maintenance-management-appointment", { appointment: this.appointment, vehicle: this.vehicleData, isFromAppointment: false });
                break;
        }

    }
}

export default {
viewModel: ScreenBase.createFactory(MaintenancePlanCancel),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./dms-detail.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import WebRequestDMS from "../../../../../app/frameworks/data/apitrackingcore/webRequestDMS";
import WebRequestDMSModel from "../../../../../app/frameworks/data/apitrackingcore/WebRequestDMSModel";
import {Enums} from "../../../../../app/frameworks/constant/apiConstant";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";

class DMSScreen extends ScreenBase {
    constructor(params) {
        
        super(params);
        this.mode = this.ensureNonObservable(params.dmsmode);
        this.bladeTitle(this.i18n("Assets_DMSDetail")());
        this.modeEnableModel = this.mode === Screen.SCREEN_MODE_CREATE ? true : null;
        this.modeDevice = this.mode === Screen.SCREEN_MODE_CREATE ? false : null;
        this.dataTable = this.ensureNonObservable(params.dataTable,[]); 
        this.currentDms = this.ensureNonObservable(params.selectedData,null);
        this.vehicleId = this.ensureNonObservable(params.vehicleId,0);

        this.dmsModelOptions = ko.observableArray([]);
        this.dmsDeviceOptions = ko.observableArray([]);
        this.selectedDMSModel = ko.observable();
        this.selectedDMSDevice = ko.observable();

        this.selectedDMSModel.subscribe((val) => {
            if(val){
                this.webRequestDMS.listDMSSummary({modelId : val.id,inuse : this.modeDevice}).done((resp) => {
                    let arrRefId = this.dataTable.map(x => x.referenceId);
                    let itemCompute = [];
                    
                    if(arrRefId && arrRefId.length > 0){
                        _.each(resp.items,(item) => {
                            let checkExistItem = $.grep(arrRefId,(x) => {
                                return x == item.referenceId
                            });
                            if(checkExistItem.length == 0 || (this.currentDms && this.currentDms.id == item.id)){
                                itemCompute.push(item);
                            }
                        });
                    }
                    else{
                        itemCompute = resp.items;
                    }
                    
                    this.dmsDeviceOptions(itemCompute);
                    if(this.currentDms){
                        this.selectedDMSDevice(ScreenHelper.findOptionByProperty(this.dmsDeviceOptions,"referenceId",this.currentDms.referenceId));
                    }
                       
                });
            }
            else {
                this.dmsDeviceOptions([]);
            }
        });
    }
    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestDMS() {
        return WebRequestDMS.getInstance();
    }

    get webRequestDMSModel() {
        return WebRequestDMSModel.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;
        var dfd = $.Deferred();
        
        let modelId = this.currentDms ? this.currentDms.dmsModelId : -1 ; 
        var r1 = this.webRequestDMSModel.listDMSModelSummary({enable : this.modeEnableModel});
        var r2 = this.webRequestDMS.listDMSSummary({modelId : modelId,inuse : this.modeDevice});

        $.when(r1,r2).done((res1,res2) => {
            
            this.dmsModelOptions(res1.items);
            if(modelId != -1){
                this.selectedDMSModel(ScreenHelper.findOptionByProperty(this.dmsModelOptions,"id",modelId));
            }
           
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        this.selectedDMSDevice.extend({ trackChange: true });
        this.selectedDMSModel.extend({ trackChange: true });
        this.selectedDMSDevice.extend({ required: true });
        this.selectedDMSModel.extend({ required: true });

        this.validationModel = ko.validatedObservable({
            selectedDMSDevice : this.selectedDMSDevice,
            selectedDMSModel : this.selectedDMSModel
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {

    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_OK")()));
        actions.push(this.createAction("actCancel", this.i18n("Common_Cancel")(), "cancel"));
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * Generate fleet service model from View Model
     * 
     * @returns fleet service model which is ready for ajax request 
     */
    generateModel() {
        return { 
                id : this.selectedDMSDevice().id,
                dmsModelName : this.selectedDMSModel().model ,  
                referenceId : this.selectedDMSDevice().referenceId,
                dmsModelId : this.selectedDMSModel().id,
                infoStatus : Enums.InfoStatus.Add
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if(sender.id === "actSave"){
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            var result = this.generateModel();
            this.webRequestDMS.CheckUsingDevice({ids:[result.id],vehicleId :this.vehicleId }).done((resp) => {
                if(resp){
                    this.showMessageBox(null, this.i18n("M233")(),BladeDialog.DIALOG_YESNO).done((button) => {
                        switch(button){
                            case BladeDialog.BUTTON_NO :
                                return;
                                break;
                            default :
                                this.manageDMSItem(result);
                        }
                    });
                }
                else{
                    this.manageDMSItem(result);
                }
               
            });
            


            
            
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
    
    manageDMSItem(result){
        var strMessage = null;
        var rowId = 0;
        if(this.dataTable && this.dataTable.length > 0){
            rowId = Math.max.apply(Math, this.dataTable.map(function (item) { return item.rowId; })); //หา maxId จาก object array 
        }
            
        if(this.currentDms){
            strMessage = Enums.InfoStatus.Update;
            let itemDMS = this.dataTable;
            _.each(this.dataTable,(item) => {
                if(item.rowId == this.currentDms.rowId){
                    item.id = result.id;
                    item.dmsModelName = result.dmsModelName;
                    item.referenceId = result.referenceId;
                    item.dmsModelId = result.dmsModelId;
                    item.infoStatus = Enums.InfoStatus.Update;
                }
            });
            this.publishMessage("ca-vehicle-dms-addItem",{ source : itemDMS,message : strMessage , selectedRowId : this.currentDms.rowId}); // sent data to replaceAll in manage.js
        }
            // เมื่อเป็นการ Add     
        else{
            result.rowId = rowId+1;
            this.publishMessage("ca-vehicle-dms-addItem",{ source : result,message : strMessage,selectedRowId : result.rowId}); // sent data to replaceAll in manage.js
        }

        this.close(true);
                    
    }

}

export default {
viewModel: ScreenBase.createFactory(DMSScreen),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./import.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import Utility from "../../../../../app/frameworks/core/utility";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import ScreenHelper from "../../../screenhelper";
import { Enums } from "../../../../../app/frameworks/constant/apiConstant";

class VehicleImportScreen extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeTitle(this.i18n("Common_Import")());
        this.bladeSize = BladeSize.XLarge;

        this.fileName = ko.observable("");
        this.selectedFile = ko.observable(null);
        this.mimeType = [
            Utility.getMIMEType("xls"),
            Utility.getMIMEType("xlsx")
        ].join();
        this.media = ko.observable(null);
        this.previewData = ko.observableArray([]);
        this.previewDataVisible = ko.observable(false);
        this.importValidationError = ko.observable("");
        this.volumeUnit = ko.observable("");

        this.urlUpload = WebConfig.appSettings.uploadUrlTrackingCore;

        this.canSaveImportData = ko.pureComputed(() => {
            return _.isEmpty(this.importValidationError()) && !_.isNil(this.media());
        });

        // Set custom column title with DataTable
        // This function is called AFTER screen onload, so this.volumeUnit already has value.
        this.onImportVehiclesTableReady = () => {
            if(!_.isEmpty(this.volumeUnit())) {
                this.dispatchEvent("dtImportVehicles", "setColumnTitle", [{ 
                    columnIndex: 7, 
                    columnTitle: this.i18n("Assets_Volume", [this.volumeUnit()])
                }]);
            }
        };
    }
    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }
    /**
     * Get WebRequest specific for Company module in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId).done((setting) => {
            this.volumeUnit(setting.volumeUnitSymbol);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    
    /**
     * 
     * setup Extend trackChange/validation
     * 
     * @memberOf VehicleImportScreen
     */
    setupExtend() {
        this.media.extend({ trackChange: true });
        
        // validation
        this.selectedFile.extend({
            fileRequired: true,
            fileExtension: ['xlsx', 'xls'],
            fileSize: 4
        });

        this.validationModel = ko.validatedObservable({
            selectedFile: this.selectedFile
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actOK", this.i18n("Common_OK")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdDownloadTemplate", this.i18n("Common_DownloadTemplate")(), "svg-cmd-download-template"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actOK") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            if (!this.canSaveImportData()) {
                return;
            }

            this.isBusy(true);
            this.webRequestVehicle.importVehicle({ companyId: WebConfig.userSession.currentCompanyId, media: this.media() }, true).done(() => {
                this.isBusy(false);
                this.publishMessage("ca-asset-vehicle-changed");
                this.close(true);
            }).fail((e) => {
                this.isBusy(false);
                this.handleError(e);
            });
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdDownloadTemplate") {
            this.showMessageBox(null, this.i18n("M117")(),
                BladeDialog.DIALOG_OKCANCEL).done((button) => {
                switch (button) {
                    case BladeDialog.BUTTON_OK:
                        this.isBusy(true);
                        this.webRequestVehicle.downloadVehicleTemplate(Utility.isiOSDevice() ? Enums.TemplateFileType.Xls : Enums.TemplateFileType.Xlsx).done((response) => {
                            this.isBusy(false);
                            ScreenHelper.downloadFile(response.fileUrl);
                        }).fail((e)=> {
                            this.isBusy(false);
                            this.handleError(e);
                        });
                        break;
                }
            });
        }
    }

    /**
     * Hook on fileupload remove
     */
    onRemoveFile() {
        this.previewData.removeAll();
        this.previewDataVisible(false);
        this.importValidationError("");
        this.media(null);
    }

    /**
     * Hook on beforefileupload
     */
    onBeforeUpload() {
        //remove current file before upload new file
        this.onRemoveFile();
    }
    /**
     * 
     * Hook on fileuploadsuccess
     * @param {any} data
     * 
     * @memberOf VehicleImportScreen
     */
    onUploadSuccess(data) {
        if (data && data.length > 0) {
            this.isBusy(true);
            var media = data[0];
            this.webRequestVehicle.importVehicle({ companyId: WebConfig.userSession.currentCompanyId, media: media }, false).done((response) => {
                var messageIdToErrors = response.messageIdToErrors;
                var previewData = response.data;

                if (previewData && previewData.length === 0 && Object.keys(messageIdToErrors).length === 0) {
                    this.previewData.replaceAll(previewData);
                    this.previewDataVisible(true);
                }
                else if (previewData && previewData.length > 0) {
                    //case validation pass
                    this.media(media);
                    // set unique tempId for dataTable
                    var index = 1;
                    ko.utils.arrayForEach(previewData, (item) => {
                        item.tempId = index;
                        index++;
                    });
                    
                    this.previewData.replaceAll(previewData);
                    this.previewDataVisible(true);
                }
                else if (Object.keys(messageIdToErrors).length > 0) {
                    //case validation error
                    if (messageIdToErrors.M076) {
                        this.importValidationError(ScreenHelper.formatImportValidationErrorMessage("M076", messageIdToErrors.M076));

                    }
                    else if (messageIdToErrors.M077) {
                        this.importValidationError(ScreenHelper.formatImportValidationErrorMessage("M077", messageIdToErrors.M077));
                    }
                }
                this.isBusy(false);  
            }).fail((e) => {
                this.isBusy(false);
                this.handleError(e);
            });
        }
    }
    /**
     * 
     * Hook on fileuploadfail
     * @param {any} e
     * 
     * @memberOf VehicleImportScreen
     */
    onUploadFail(e) {
        this.handleError(e);
    }

    /**
     * 
     * Render Icon in dataTable
     * @param {any} data
     * @param {any} type
     * @param {any} row
     * @returns
     * 
     * @memberOf VehicleImportScreen
     */
    renderIconColumn(data, type, row) {
        return _.isEmpty(data) ? '' : '<img src="' + data + '"/>';
    }
}

export default {
    viewModel: ScreenBase.createFactory(VehicleImportScreen),
    template: templateMarkup
};
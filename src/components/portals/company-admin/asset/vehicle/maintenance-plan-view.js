﻿import ko from "knockout";
import templateMarkup from "text!./maintenance-plan-view.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import { Constants, Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestVehicleMaintenancePlan from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenancePlan";
import WebRequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicle";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestAppointment from "../../../../../app/frameworks/data/apitrackingcore/webRequestAppointment";

import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";

class VehicleMaintenancePlanViewScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties

        this.bladeTitle(this.i18n("Common_ViewMaintenancePlan")());
        this.bladeSize = BladeSize.Medium;

        this.items = ko.observableArray([]);
        this.vehicleData = this.ensureNonObservable(params.vehicle);
        this.appointment = params.appointment;
        this.subTypeId = null;
        this.selectedItems = ko.observableArray([]);
        this.order = ko.observable([[0, "asc"]]);
        this.isChecked = ko.observable(false);
        this.id = this.ensureNonObservable(params.id);
        this.vehicleId = this.ensureNonObservable(params.vehicleId);
        this.title = ko.observable();
        this.description = ko.observable();
        this.formatMaintenanceDate = ko.observable();
        this.dayRemind = ko.observable();
        this.remainingMaintenanceDay = ko.observable();
        this.currentDistance = ko.observable();
        this.displayCurrentDistance = ko.observable();
        this.maintenanceDistance = ko.observable();
        this.distanceRemind = ko.observable();
        this.remainingDistance = ko.observable();
        this.estimateCost = ko.observable();
        this.formatAlertVia = ko.observable();
        this.maintenanceStatus = ko.observable();
        this.maintenanceStatusDisplayName = ko.observable();
        this.performBy = ko.observable();
        this.formatActualDate = ko.observable();
        this.actualDistance = ko.observable();
        this.actualCost = ko.observable();
        this.distanceUnit = ko.observable();
        this.currency = ko.observable();
        this.remark = ko.observable();
        this.status_Check = ko.observable();
        this.maintenanceSubTypeName = ko.observable();
        this.maintenanceTypeName = ko.observable();
        this.infoVehicle = null;
        this.checkListAllID = [];
        this.tblChecklist = ko.observableArray([]);
        this.trueAll = ko.observable(false);
        this.firsttime = true;
        this.rowsCheckbox = 1;
        this.haveRows = ko.pureComputed(() => {
            return this.rowsCheckbox == 0;
        });
        this.isPending = ko.pureComputed(() => {
            return this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Pending;
        });
        this.selectedItems.subscribe(function (value) {
        }.bind(this));
        this.isCompleted = ko.pureComputed(() => {
            if (this.isCancelled()) {
                return false;
            } else {
                return this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Completed || this.rowsCheckbox == 0;
            }
        });

        this.isCancelled = ko.pureComputed(() => {
            return this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Cancelled;
        });

        this.canUpdate = ko.pureComputed(() => {
            return this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Pending;
        });
        this.canComplete = ko.pureComputed(() => {
            if (this.items().length > 0) {
                return this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Ready || this.trueAll();
            } else {

                return this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Ready || this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Pending || this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Overdue;
            }
        });
        this.canCancel = ko.pureComputed(() => {
            return this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Pending || this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Overdue || this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Ready || this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Inprogress;
        });
        this.canDelete = ko.pureComputed(() => {
            return this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Pending;
        });
        this.canAppointment = ko.pureComputed(() => {
            return this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Pending || this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Overdue || this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Ready || this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Inprogress || this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Completed || this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Cancelled;
        });

        // Subscribe Message when vehicle maintenance plan updated
        this.subscribeMessage("ca-asset-vehicle-maintenance-plan-updated", (info) => {
            this.isBusy(true);
            if (info) {
                this.populateData(info);
                this.isBusy(false);
            }
            else {
                this.refreshData();
                this.isBusy(false);
            }
        });


        var isBeingCheckAll = false;
        var isBeingCheck = false;

        //Checkall
        this.isChecked.subscribe(function (value) {
            
            if (!this.firsttime) {
                this.isBusy(true);
                isBeingCheckAll = true;
                if (!isBeingCheck) {
                    var dfdlist = $.Deferred();
                    var checkStatus = value;

                    var params = {
                        status: checkStatus,
                        maintenancePlanId: this.id,
                        isCheckAll: true

                    }

                    var dfdUpdateCheckList = this.webRequestVehicleMaintenancePlan.updateChecklistMaintenancePlan(params);
                    $.when(dfdUpdateCheckList).done(() => {
                        this.isBusy(false);
                        dfdlist.resolve();
                    }).fail((e) => {
                        this.isBusy(false);
                        dfdlist.reject(e);
                    });

                    $.when(dfdlist).done(() => {
                        this.webRequestVehicleMaintenancePlan.getVehicleMaintenancePlan(this.id, [EntityAssociation.VehicleMaintenancePlan.CurrentDistance]).done((response) => {
                            this.populateData2(response);
                        })
                        isBeingCheckAll = false;
                        this.isBusy(false);
                        this.publishMessage("ca-asset-vehicle-maintenance-plan-updated");
                    });
                }
                isBeingCheckAll = false;
            } else {
                this.firsttime = false;
            }
        }.bind(this));


        //Checklist
        this.itemsChanged = (newValue) => {
            this.isBusy(true);
            isBeingCheck = true;
            if (!isBeingCheckAll) {
                var dfdlist = $.Deferred();
                var dfdUpdateList = null;
                var dfdCheckStatus = null;

                dfdUpdateList = this.webRequestVehicleMaintenancePlan.updateChecklistMaintenancePlan(newValue).done((response) => {

                    dfdCheckStatus = this.webRequestVehicleMaintenancePlan.getVehicleMaintenancePlan(this.id, [EntityAssociation.VehicleMaintenancePlan.CurrentDistance]).done((response) => {

                        this.status_Check(response.maintenanceStatus);
                        this.maintenanceStatusDisplayName(response.maintenanceStatusDisplayName);
                        this.maintenanceStatus(response.maintenanceStatus);
                        // Ready
                        if (this.status_Check() == 6) 
                        {
                            this.isChecked(true);
                            isBeingCheck = false;
                        }
                        else 
                        {
                            this.isChecked(false);
                            isBeingCheck = false;
                        }

                        this.isBusy(false);
                        this.publishMessage("ca-asset-vehicle-maintenance-plan-updated");
                        
                    });

                });
            }


        }



    }

    /**
     * Get WebRequest specific for Vehicle Maintenance Plan module in Web API access.
     * @readonly
     */
    get webRequestVehicleMaintenancePlan() {
        return WebRequestVehicleMaintenancePlan.getInstance();
    }

    get webRequestVehicle() {
        return WebRequestVehicle.getInstance();
    }

    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    get webRequestAppointment() {
        return WebRequestAppointment.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        var dfdListMaintenancePlan = null;


        var dfdMAPlan = this.webRequestVehicleMaintenancePlan.getVehicleMaintenancePlan(this.id, [EntityAssociation.VehicleMaintenancePlan.CurrentDistance]).done((response) => {
            if (response.maintenanceStatus != Enums.ModelData.MaintenanceStatus.Ready) {

                this.firsttime = false;
            }
            this.populateData(response);
            this.subTypeId = response.maintenanceSubTypeId;
            this.infoVehicle = response;

            var testArr = [];
            var params = null;

            if (response.checkListInfos.length == 0) {

                this.rowsCheckbox = 0;
            }



            this.items(response.checkListInfos);

            if (this.isCompleted()) {

                for (var i = 0; i < this.items().length; i++) {

                    params = this.items()[i].listName + " (" + this.items()[i].actionCode + ":" + this.items()[i].actionName + ")";
                    testArr.push({
                        ListName: params
                    });

                    this.tblChecklist(testArr);
                }
                if (this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Completed) {
                    this.bladeTitle(this.i18n("Vehicle_MA_View_Complete_Maintenance_Plan")());
                }
            }
            else {

                for (var i = 0; i < this.items().length; i++) {

                    this.items()[i].listName = this.items()[i].listName + " (" + this.items()[i].actionCode + ":" + this.items()[i].actionName  + ")";

                    if (response.maintenanceStatus === Enums.ModelData.MaintenanceStatus.Cancelled) {

                        // this.items()[i].enable =  false;
                        this.items()[i].cancel = false;
                        this.bladeTitle(this.i18n("Vehicle_MA_View_Cancel_Maintenance_Plan")());
                    } else {

                        // this.items()[i].enable =  true;

                    }
                }




            }

        });

        var dfdVehicleCurrentDistance = this.webRequestVehicle.getCurrentDistance(this.vehicleId).done((response) => {
            this.currentDistance(response.currentDistance);
            this.displayCurrentDistance(response.formatCurrentDistance);
        });


        var dfdCompanySettings = this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId).done((response) => {
            this.distanceUnit(response.distanceUnitSymbol);
            this.currency(response.currencySymbol);
        });

        $.when(dfdMAPlan, dfdVehicleCurrentDistance, dfdCompanySettings).done(() => {

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) 
    {
        if (WebConfig.userSession.hasPermission(Constants.Permission.UpdateVehicleMaintenancePlan) || WebConfig.userSession.hasPermission(Constants.Permission.UpdateMaintenance)) {
            commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit", this.canUpdate));
            commands.push(this.createCommand("cmdComplete", this.i18n("Common_Complete")(), "svg-cmd-maintenance-complete", this.canComplete));
            commands.push(this.createCommand("cmdCancel", this.i18n("Common_Cancel")(), "svg-cmd-maintenance-cancel", this.canCancel));

        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.DeleteVehicleMaintenancePlan) 
            || WebConfig.userSession.hasPermission(Constants.Permission.UpdateMaintenance)) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete", this.canDelete));
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.UpdateVehicleMaintenancePlan) 
            || WebConfig.userSession.hasPermission(Constants.Permission.UpdateMaintenance)) {
            commands.push(this.createCommand("cmdAppointment", this.i18n("Vehicle_MA_Appointment")(), "svg-ic-mt-appo-manage", this.canAppointment));
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdUpdate":
                this.navigate("ca-asset-vehicle-view-maintenance-plan-manage", { mode: Screen.SCREEN_MODE_UPDATE, id: this.id, vehicleId: this.vehicleId });
                break;
            case "cmdComplete":




                //let model2 = {
                //    companyId: WebConfig.userSession.currentCompanyId,
                //    ids: [this.id],
                //    status: Enums.ModelData.AppointmentStatus.Active
                //};

                //this.webRequestAppointment.listAppointment(model2).done((response) => {


                //    var message = "M145";
                ////    if (response["items"].length > 0) {

                //        this.showMessageBox(null, this.i18n(message)(),
                //            BladeDialog.DIALOG_YESNO).done((button) => {
                //                switch (button) {
                //                    case BladeDialog.BUTTON_YES:

                //                        this.navigate("ca-asset-vehicle-view-maintenance-plan-complete", { id: this.id });
                //                        break;
                //                }
                //            });

                //    } else {
                //        this.navigate("ca-asset-vehicle-view-maintenance-plan-complete", { id: this.id });
                //    }



                //}).fail((e) => {
                //    this.handleError(e);
                //}).always(() => {
                //    this.isBusy(false);
                //});
                this.navigate("ca-asset-vehicle-view-maintenance-plan-complete", { id: this.id ,currentDistance:this.currentDistance()});
                break;
            case "cmdCancel":
                this.navigate("ca-asset-vehicle-view-maintenance-plan-cancel", { info: this.infoVehicle });
                break;
            case "cmdDelete":

                let model = {
                    companyId: WebConfig.userSession.currentCompanyId,
                    ids: [this.id],
                    status: Enums.ModelData.AppointmentStatus.Completed
                };

                this.webRequestAppointment.listAppointment(model).done((response) => {

                    

                   // var messageReplace =  this.i18n('M145', [textChange]);

                    var message = this.i18n("M032")();
                    if (response["items"].length > 0) {
                        var textChange = this.i18n("Vehicle_MA_Common_Appointment")();
                        message = this.i18n('M144', [textChange])
                    }

                    this.showMessageBox(null, message,
                        BladeDialog.DIALOG_YESNO).done((button) => {
                            switch (button) {
                                case BladeDialog.BUTTON_YES:
                                    this.isBusy(true);
                                    this.webRequestVehicleMaintenancePlan.deleteVehicleMaintenancePlan(this.id).done(() => {
                                        this.publishMessage("ca-asset-vehicle-maintenance-plan-deleted");
                                        this.close(true);
                                    }).fail((e) => {
                                        this.handleError(e);
                                    }).always(() => {
                                        this.isBusy(false);
                                    });
                                    break;
                            }
                        });

                }).fail((e) => {
                    this.handleError(e);
                }).always(() => {
                    this.isBusy(false);
                });

                break;
            case "cmdAppointment":
                this.navigate("ca-asset-maintenance-management-appointment", { appointment: this.appointment, vehicle: this.vehicleData, isFromAppointment: false });
                break;
        }
    }


    /**
     * Refresh Data
     */
    refreshData() {
        this.isBusy(true);
        this.webRequestVehicleMaintenancePlan.getVehicleMaintenancePlan(this.id, [EntityAssociation.VehicleMaintenancePlan.CurrentDistance]).done((response) => {
            this.populateData3(response);


        }).fail((e) => {
            this.handleError(e);
        }).always(() => {
            this.isBusy(false);
        });
    }


    /**
     * Populate Data from ajax response
     * @param {any} response
     */
    populateData(response) {
        this.title(response.title);
        this.description(response.description);
        this.formatMaintenanceDate(response.formatMaintenanceDate);
        this.dayRemind(response.dayRemind);
        this.remainingMaintenanceDay(response.remainingMaintenanceDay);
        this.maintenanceDistance(response.formatMaintenanceDistance);

        this.distanceRemind(response.formatDistanceRemind);
        this.remainingDistance(response.formatRemainingDistance);

        this.estimateCost(response.formatEstimateCost);
        this.formatAlertVia(response.formatAlertVia);
        this.maintenanceStatus(response.maintenanceStatus);
        this.maintenanceStatusDisplayName(response.maintenanceStatusDisplayName);
        this.remark(response.remark);
        this.performBy(response.performBy);
        this.formatActualDate(response.formatActualDate);
        this.actualDistance(response.formatActualDistance);
        this.actualCost(response.formatActualCost);
        this.maintenanceTypeName(response.maintenanceTypeName);
        this.maintenanceSubTypeName(response.maintenanceSubTypeName);

        if (this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Cancelled) {

            for (var i = 0; i < this.items().length; i++) {

                this.items()[i].cancel = false;
                this.bladeTitle(this.i18n("Vehicle_MA_View_Cancel_Maintenance_Plan")());

            }
        } else if (this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Completed) {

            this.bladeTitle(this.i18n("Vehicle_MA_View_Complete_Maintenance_Plan")());

        }



        ///if (this.maintenanceStatusDisplayName() == "Ready") {
        if (this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Ready) {
            this.isChecked(true);
        } else {

            this.isChecked(false);
        }



        //console.log(this.maintenanceStatus());
        //console.log(Enums.ModelData.MaintenanceStatus.Overdue);
        //if (this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Overdue) {

        //    var checkcountTrue = 0;
        //    console.log(response.checkListInfos);
        //    for (var i = 0; i < response.checkListInfos.length; i++) {

        //        response.checkListInfos[i].listName = response.checkListInfos[i].listName + " (" + response.checkListInfos[i].actionCode + ")";

        //        if (response.checkListInfos[i].status) {
        //            checkcountTrue = checkcountTrue + 1;
        //        }
        //    }
        //    if (checkcountTrue == response.checkListInfos.length) {
        //        this.trueAll(true);
        //        console.log("AAAAAAAAA");
        //    } else {
        //        this.trueAll(false);
        //    }
        //}

        this.items(response.checkListInfos);
    }
    populateData2(response) {
        this.title(response.title);
        this.description(response.description);
        this.formatMaintenanceDate(response.formatMaintenanceDate);
        this.dayRemind(response.dayRemind);
        this.remainingMaintenanceDay(response.remainingMaintenanceDay);
        this.maintenanceDistance(response.maintenanceDistance);
        this.distanceRemind(response.distanceRemind);
        this.remainingDistance(response.remainingDistance);
        this.estimateCost(response.estimateCost);
        this.formatAlertVia(response.formatAlertVia);
        this.maintenanceStatus(response.maintenanceStatus);
        this.maintenanceStatusDisplayName(response.maintenanceStatusDisplayName);
        this.remark(response.remark);
        this.performBy(response.performBy);
        this.formatActualDate(response.formatActualDate);
        this.actualDistance(response.actualDistance);
        this.actualCost(response.actualCost);


        var dfdMA = $.Deferred();
        var dfdVehicleCurrentDistance = this.webRequestVehicle.getCurrentDistance(this.vehicleId).done((response) => {
            this.currentDistance(response.currentDistance);
            dfdMA.resolve();
        }).fail((e) => {
            dfdMA.reject(e);
        });







        var checkcountTrue = 0;

        for (var i = 0; i < response.checkListInfos.length; i++) {

            response.checkListInfos[i].listName = response.checkListInfos[i].listName + " (" + response.checkListInfos[i].actionCode + ":" + response.checkListInfos[i].actionName + ")";

            if (response.checkListInfos[i].status) {
                checkcountTrue = checkcountTrue + 1;

            }
        }


        if (checkcountTrue == response.checkListInfos.length) {
            this.trueAll(true);
        } else {
            this.trueAll(false);
        }


        this.items(response.checkListInfos);
    }

    populateData3(response) {
        this.title(response.title);
        this.description(response.description);
        this.formatMaintenanceDate(response.formatMaintenanceDate);
        this.dayRemind(response.dayRemind);
        this.remainingMaintenanceDay(response.remainingMaintenanceDay);
        this.maintenanceDistance(response.formatMaintenanceDistance);
        this.trueAll(false);
        this.distanceRemind(response.formatDistanceRemind);
        this.remainingDistance(response.formatRemainingDistance);

        this.estimateCost(response.formatEstimateCost);
        this.formatAlertVia(response.formatAlertVia);
        this.maintenanceStatus(response.maintenanceStatus);
        this.maintenanceStatusDisplayName(response.maintenanceStatusDisplayName);
        this.remark(response.remark);
        this.performBy(response.performBy);
        this.formatActualDate(response.formatActualDate);
        this.actualDistance(response.formatActualDistance);
        this.actualCost(response.formatActualCost);
        this.maintenanceTypeName(response.maintenanceTypeName);
        this.maintenanceSubTypeName(response.maintenanceSubTypeName);


        this.appointment.maintenanceStatus = response.maintenanceStatus;

        for (var i = 0; i < response.checkListInfos.length; i++) {

            response.checkListInfos[i].listName = response.checkListInfos[i].listName + " (" + response.checkListInfos[i].actionCode + ":" + response.checkListInfos[i].actionName + ")";

        }

        this.items(response.checkListInfos);

        if (this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Cancelled) {
            for (var i = 0; i < this.items().length; i++) {

                this.items()[i].cancel = false;
                this.bladeTitle(this.i18n("Vehicle_MA_View_Cancel_Maintenance_Plan")());

            }

            this.trueAll(false);
        } else if (this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Completed) {

            this.bladeTitle(this.i18n("Vehicle_MA_View_Complete_Maintenance_Plan")());

        }


        ///
        ///if (this.maintenanceStatusDisplayName() == "Ready") {
        if (this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Ready) {
            this.isChecked(true);
        }
        else if (this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Cancelled || this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Completed) {
        //else if (this.maintenanceStatusDisplayName() == "Complete" || this.maintenanceStatusDisplayName() == "Cancelled") {
        } else {
            this.isChecked(false);
        }



        var testArr = [];
        var params = null;


        if (this.isCompleted()) {

            for (var i = 0; i < this.items().length; i++) {

                params = this.items()[i].listName;
                testArr.push({
                    ListName: params
                });


            }
            this.tblChecklist(testArr);

            if (this.maintenanceStatus() === Enums.ModelData.MaintenanceStatus.Completed) {
                this.bladeTitle(this.i18n("Vehicle_MA_View_Complete_Maintenance_Plan")());
            }
        }
    }

  
}

export default {
    viewModel: ScreenBase.createFactory(VehicleMaintenancePlanViewScreen),
    template: templateMarkup
};
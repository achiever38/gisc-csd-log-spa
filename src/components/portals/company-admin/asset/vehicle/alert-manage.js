﻿import ko from "knockout";
import templateMarkup from "text!./alert-manage.html";
import ScreenBase from "../../../screenbase";
import { VehicleAlerts } from "./infos";
import { Enums } from "../../../../../app/frameworks/constant/apiConstant";

class VehicleManageAlertsScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Common_Alerts")());
        
        var alerts = this.ensureNonObservable($.extend(true, {}, params.alerts));

        this.smsAlert = ko.observable(alerts.smsAlert);
        this.emailAlert = ko.observable(alerts.emailAlert);
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
    }
    /**
     * Setup trackChange/validation
     * @memberOf VehicleManageLicenseDetails
     */
    setupExtend() {
        this.smsAlert.extend({ trackChange: true });
        this.emailAlert.extend({ trackChange: true });

        this.emailAlert.extend({
            validation: {
                validator: (val) => {
                    var valid = true;

                    if (val) {
                        var emails = val.split(",");
                        if (emails.length) {
                            // regEx from knockout validation 2.0.3
                            // https://cdnjs.cloudflare.com/ajax/libs/knockout-validation/2.0.3/knockout.validation.js
                            var regExEmail = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i;

                            for (var i = 0; i < emails.length; i++) { 
                                var email = emails[i].trim();
                                if (!regExEmail.test(email)) {
                                    valid = false;
                                    break;
                                }
                            }
                        }
                    }
                    
                    return valid;
                },
                message: this.i18n("M064")()
            }
        });

        this.validationModel = ko.validatedObservable({
            emailAlert: this.emailAlert
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actOK", this.i18n("Common_OK")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actOK") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.publishMessage("ca-asset-vehicle-manage-alerts-changed", new VehicleAlerts(this.smsAlert(), this.emailAlert()));
            this.close(true);
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(VehicleManageAlertsScreen),
    template: templateMarkup
};
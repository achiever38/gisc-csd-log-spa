﻿import ko from "knockout";
import templateMarkup from "text!./maintenance-log-view.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Constants, Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestVehicleMaintenancePlan from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenancePlan";
import WebRequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicle";

/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class MaintenanceLogView extends ScreenBase {
    
    /**
     * Creates an instance of AssetMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Vehicle_MA_Maintenance_View_Log")());

        this.id = this.ensureNonObservable(params.id);
        this.vehicleId = this.ensureNonObservable(params.vehicleId);

        this.maType = ko.observable();
        this.maSubType = ko.observable();
        this.title = ko.observable();
        this.description = ko.observable();
        this.maDate = ko.observable();
        this.currentDistance = ko.observable();
        this.distanceUnit = ko.observable();
        this.actualCost = ko.observable();
        this.currency = ko.observable();
        this.assignTo = ko.observable();
        this.performBy = ko.observable();
        this.status = ko.observable();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) {
            return;
        }

        let dfd = $.Deferred();

        let dfdCompanySettings = this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId).done((response) => {
            this.distanceUnit(response.distanceUnitSymbol);
            this.currency(response.currencySymbol);
        });

        let dfdVehicleCurrentDistance = this.webRequestVehicle.getCurrentDistance(this.vehicleId).done((response) => {
            this.currentDistance(response.formatCurrentDistance);
        });

        let dfdMAPlanInfo = this.webRequestVehicleMaintenancePlan.getVehicleMaintenancePlan(this.id).done((response) => {
            this.maType(response["maintenanceTypeName"]);
            this.maSubType(response["maintenanceSubTypeName"]);
            this.title(response["title"]);
            this.description(response["description"]);
            this.maDate(response["formatActualDate"]);
            this.assignTo(response["assignTo"]);
            this.actualCost(response["formatActualCost"]);
            this.performBy(response["performBy"]);
            this.status(response["maintenanceStatusDisplayName"]);
        });

        $.when(dfdCompanySettings, dfdVehicleCurrentDistance, dfdMAPlanInfo).done(() => {
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        
        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.DeleteVehicleMaintenancePlan)) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch(sender.id) {
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M154")(),
                    BladeDialog.DIALOG_YESNO).done((button) => {
                        switch (button) {
                            case BladeDialog.BUTTON_YES:
                                this.isBusy(true);
                                this.webRequestVehicleMaintenancePlan.deleteVehicleMaintenancePlan(this.id).done(() => {
                                    this.publishMessage("ca-asset-vehicle-maintenance-plan-deleted");
                                    this.close(true);
                                }).fail((e)=> {
                                    this.handleError(e);
                                }).always(() => {
                                    this.isBusy(false);
                                });
                                break;
                        }
                    });
                break;
            default : break;
        }
    }

    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    get webRequestVehicle() {
        return WebRequestVehicle.getInstance();
    }

    get webRequestVehicleMaintenancePlan() {
        return WebRequestVehicleMaintenancePlan.getInstance();
    }
}

export default {
viewModel: ScreenBase.createFactory(MaintenanceLogView),
    template: templateMarkup
};
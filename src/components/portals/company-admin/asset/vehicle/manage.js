﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenHelper from "../../../screenhelper";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import WebrequestVehicleModel from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicleModel";
import WebrequestVehicleIcon from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicleIcon";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestMDVRModel from "../../../../../app/frameworks/data/apitrackingcore/webRequestMDVRModel";
import WebRequestMDVR from "../../../../../app/frameworks/data/apitrackingcore/webRequestMDVR";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestCustomPOI from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomPOI";
import WebRequestCategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestCategory";
import {
    Enums,
    EntityAssociation
} from "../../../../../app/frameworks/constant/apiConstant";
import Utility from "../../../../../app/frameworks/core/utility";
import {
    VehicleImage,
    VehicleLicenseDetails,
    VehicleAlerts,
    VehicleDefaultDrivers
} from "./infos";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import moment from "moment";

class VehicleManageScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.mode = this.ensureNonObservable(params.mode);
        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("Assets_CreateVehicle")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("Assets_UpdateVehicle")());
                break;
        }
        this.bladeSize = BladeSize.Medium;

        this.id = this.ensureNonObservable(params.id, 0);

        this.imagePlaceholderUrl = this.resolveUrl("~/images/upload-img-placeholder-100x100.jpg");
        this.imageSelectedFile = ko.observable(null);
        this.imageSelectedFileEnable = ko.pureComputed(() => {
            //enable if display images < 4 or if display images contains invalid image
            return this.displayImages().length < 4 || this.displayImagesContainsInvalidImage();
        });
        this.imageMimeType = [
            Utility.getMIMEType("jpg"),
            Utility.getMIMEType("png")
        ].join();

        this.deleteImages = ko.observableArray([]);
        this.displayImages = ko.observableArray([]);
        //check if display images array contains invalid image
        this.displayImagesContainsInvalidImage = ko.pureComputed(() => {
            return !_.isNil(this.getInvalidImage());
        });

        this.images = ko.pureComputed(() => {
            return this.displayImages().concat(this.deleteImages());
        });

        this.image0 = ko.pureComputed(() => {
            return this.displayImages()[0];
        });
        this.image0FileName = ko.pureComputed(() => {
            return this.image0() ? this.image0().fileName : "";
        });
        this.image0Url = ko.pureComputed(() => {
            return this.image0() ? this.image0().fileUrl : this.imagePlaceholderUrl;
        });
        this.image0RemoveIconVisible = ko.pureComputed(() => {
            return this.image0() ? true : false;
        });

        this.image1 = ko.pureComputed(() => {
            return this.displayImages()[1];
        });
        this.image1FileName = ko.pureComputed(() => {
            return this.image1() ? this.image1().fileName : "";
        });
        this.image1Url = ko.pureComputed(() => {
            return this.image1() ? this.image1().fileUrl : this.imagePlaceholderUrl;
        });
        this.image1RemoveIconVisible = ko.pureComputed(() => {
            return this.image1() ? true : false;
        });

        this.image2 = ko.pureComputed(() => {
            return this.displayImages()[2];
        });
        this.image2FileName = ko.pureComputed(() => {
            return this.image2() ? this.image2().fileName : "";
        });
        this.image2Url = ko.pureComputed(() => {
            return this.image2() ? this.image2().fileUrl : this.imagePlaceholderUrl;
        });
        this.image2RemoveIconVisible = ko.pureComputed(() => {
            return this.image2() ? true : false;
        });

        this.image3 = ko.pureComputed(() => {
            return this.displayImages()[3];
        });
        this.image3FileName = ko.pureComputed(() => {
            return this.image3() ? this.image3().fileName : "";
        });
        this.image3Url = ko.pureComputed(() => {
            return this.image3() ? this.image3().fileUrl : this.imagePlaceholderUrl;
        });
        this.image3RemoveIconVisible = ko.pureComputed(() => {
            return this.image3() ? true : false;
        });

        this.categoryVisible = ko.pureComputed(() => {
            var isCategoryVehicleVisible = (Object.keys(this.CategoryOptions()).length > 0) ? true : false;
            return isCategoryVehicleVisible;
        });

        this.fleetTypeVisible = ko.pureComputed(() => {
            var isFleetTypeVisible = (Object.keys(this.fleetTypeOptions()).length > 0) ? true : false;
            return isFleetTypeVisible;
        });

        this.type = ko.observable();
        this.ermType = ko.observable();
        this.isNotTrail = ko.pureComputed(() => {
            return _.isNil(this.type()) || this.type().value !== Enums.ModelData.VehicleType.Trail;
        });
        this.isTrailer = ko.pureComputed(() => {
            return this.type() && this.type().value === Enums.ModelData.VehicleType.Trailer;
        });
        this.model = ko.observable();
        this.icon = ko.observable();
        this.businessUnitId = ko.observable(null);
        this.enable = ko.observable();
        this.chassisNo = ko.observable("");
        this.seat = ko.observable("");
        this.volume = ko.observable("");
        this.volumeUnit = ko.observable("");
        this.description = ko.observable("");
        this.referenceNo = ko.observable("");
        this.fuelRate = ko.observable("");
        this.stopDuration = ko.observable("");
        this.registeredType = ko.observable();
        this.registeredTypeVisible = ko.pureComputed(() => {
            return this.type() && [Enums.ModelData.VehicleType.Truck, Enums.ModelData.VehicleType.Trailer, Enums.ModelData.VehicleType.PassengerCar].indexOf(this.type().value) > -1;
        });
        this.trail = ko.observable();
        this.overSpeedDelay = ko.observable("");
        this.parkEngineOnLimit = ko.observable("");
        this.defaultOverSpeedLimit = ko.observable("");
        this.Category = ko.observable();
        this.fleetType = ko.observable();

        this.selectedSubMenuItem = ko.observable(null);

        this.typeOptions = ko.observableArray([]);
        this.ermTypeOptions = ko.observableArray([]);
        this.modelOptions = ko.observableArray([]);
        this.iconOptions = ko.observableArray([]);
        this.businessUnitOptions = ko.observableArray([]);
        this.enableOptions = ScreenHelper.createYesNoObservableArrayWithDisplayValue();
        this.registeredTypeOptions = ko.observableArray([]);
        this._registeredTypePassengerCarOptions = [];
        this._registeredTypeTruckTrailerOptions = [];
        this.CategoryOptions = ko.observableArray([]);
        this.fleetTypeOptions = ko.observableArray([]);

        this.trailOptions = ko.observableArray([]);

        this.license = ko.observable(new VehicleLicenseDetails());

        this.defaultDriverId = ko.observable(null);
        this.coDriver1Id = ko.observable(null);
        this.coDriver2Id = ko.observable(null);

        this.smsAlert = ko.observable("");
        this.emailAlert = ko.observable("");

        this.optionMDVRModels = ko.observableArray([]);
        this.mdvrModel = ko.observable();

        this.optionDcCustomPOIs = ko.observableArray([]);
        this.dcCustomPOI = ko.observable();

        this.optionMDVRs = ko.observableArray([]);
        this.mdvrId = ko.observable();
        this.mdvrDeviceId = ko.observable();
        this.inuseMDVR = this.mode == Screen.SCREEN_MODE_UPDATE ? true : false;
        this.enableModelMDVR = this.mode == Screen.SCREEN_MODE_UPDATE ? null : true;
        this.dmsItem = ko.observableArray([]);
        this.adsItem = ko.observableArray([]);
        this.orderDms = ko.observable([
            [1, "asc"]
        ]);
        this.orderAds = ko.observable([
            [1, "asc"]
        ]);
        this.recentChangedRowIds = ko.observableArray();
        this.recentChangedRowAdsIds = ko.observableArray();
        this.installDate = ko.observable();
        this.installTime = ko.observable();
        this.maxDate = new Date();

        this.selectedInverseFeatures = ko.observableArray([]);

        this.prevBusinessUnitId = ko.observable();
        this.defaultDriverBu = ko.observable();
        this.driverBuId = ko.observable();

        // Subscribe when License changed
        this.subscribeMessage("ca-asset-vehicle-manage-license-details-changed", (licenseDetails) => {
            this.license(licenseDetails);
        });

        // Subscribe when Alerts changed
        this.subscribeMessage("ca-asset-vehicle-manage-alerts-changed", (alerts) => {
            this.smsAlert(alerts.smsAlert);
            this.emailAlert(alerts.emailAlert);
        });

        // Subscribe when Default Driver changed
        this.subscribeMessage("ca-asset-vehicle-manage-default-drivers-changed", (defaultDrivers) => {
            this.driverBuId(defaultDrivers.buId);
            this.defaultDriverId(defaultDrivers.defaultDriverId);
            this.coDriver1Id(defaultDrivers.coDriver1Id);
            this.coDriver2Id(defaultDrivers.coDriver2Id);
        });

        // Add Item DMS
        this.subscribeMessage("ca-vehicle-dms-addItem", (item) => {
            if (item.message == null) {
                this.dmsItem.push(item.source);
                this.recentChangedRowIds.replaceAll([item.selectedRowId]);
            } else {
                this.dmsItem.replaceAll(item.source);
                this.recentChangedRowIds.replaceAll([item.selectedRowId]);
            }

        });

        // Add Item ADAS
        this.subscribeMessage("ca-vehicle-ads-addItem", (item) => {
            if (item.message == null) {
                this.adsItem.push(item.source);
                this.recentChangedRowAdsIds.replaceAll([item.selectedRowId]);
            } else {
                this.adsItem.replaceAll(item.source);
                this.recentChangedRowAdsIds.replaceAll([item.selectedRowId]);
            }

        });
        // Navigate DMS
        this._selectingRowHandler = (dms) => {
            if (dms) {
                return this.navigate("ca-asset-vehicle-dms-detail", {
                    selectedData: dms,
                    dataTable: this.dmsItem(),
                    dmsmode: this.mode,
                    vehicleId: this.id == 0 ? null : this.id
                });
            }
            return false;
        };

        // Navigate ADAS
        this._selectingRowAdsHandler = (ads) => {
            if (ads) {
                return this.navigate("ca-asset-vehicle-adas-detail", {
                    selectedData: ads,
                    dataTable: this.adsItem(),
                    adsmode: this.mode,
                    vehicleId: this.id == 0 ? null : this.id
                });
            }
            return false;
        };

        this.subscribeMessage("bo-accessible-inverse-features-apply", (ids) => {
            this.selectedInverseFeatures(ids);
        });

        this.valChangeBu = ko.observable(null);
        this.subscribeMessage("ca-asset-vehicle-confirm-dialog-cahnge-bu", (val) => {
             // 3 คือ Remove Default Driver จะ remove default driver
             if(val == 3){
                this.driverBuId(this.businessUnitId());
                this.defaultDriverId(null);
                this.coDriver1Id(null);
                this.coDriver2Id(null);
            }

            this.valChangeBu(val);
        });
        
        this.subscribeMessage("ca-asset-vehicle-confirm-dialog-cahnge-bu-cancel", (val) => {
            this.businessUnitId([val.toString()]);
        });

    }
    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }
    /**
     * Get WebRequest specific for Vehicle Model module in Web API access.
     * @readonly
     */
    get webRequestVehicleModel() {
        return WebrequestVehicleModel.getInstance();
    }
    /**
     * Get WebRequest specific for Vehicle Icon module in Web API access.
     * @readonly
     */
    get webRequestVehicleIcon() {
        return WebrequestVehicleIcon.getInstance();
    }
    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }
    /**
     * Get WebRequest specific for Company module in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
     * Get WebRequest specific for Category module in Web API access.
     * @readonly
     */
    get webRequestCategory() {
        return WebRequestCategory.getInstance();
    }
    get webRequestMDVRModel() {
        return WebRequestMDVRModel.getInstance();
    }
    get webRequestMDVR() {
        return WebRequestMDVR.getInstance();
    }
    get webRequestCustomPOI() {
        return WebRequestCustomPOI.getInstance();
    }
    
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        var d0 = $.Deferred();
        var d1 = this.mode === Screen.SCREEN_MODE_UPDATE ? this.webRequestVehicle.getVehicle(this.id,
            [EntityAssociation.Vehicle.Images,
                EntityAssociation.Vehicle.Icon,
                EntityAssociation.Vehicle.Trail,
                EntityAssociation.Vehicle.DefaultDriver,
                EntityAssociation.Vehicle.CoDriver1,
                EntityAssociation.Vehicle.CoDriver2,
                EntityAssociation.Vehicle.AlertConfigurations,
                EntityAssociation.Vehicle.Model,
                EntityAssociation.Vehicle.VehicleRegisteredType,
                EntityAssociation.Vehicle.License,
                EntityAssociation.Vehicle.InverseFeatures
        ]) : null;

        var d2 = $.Deferred(); // Vehicle Type/Registered Type
        var d3 = $.Deferred(); // Vehicle Model
        var d4 = $.Deferred(); // Vehicle Icon
        var d5 = $.Deferred(); // Business Unit
        var d6 = $.Deferred(); // Trail
        var d7 = $.Deferred(); // Company Settings
        var d8 = $.Deferred(); // Catagory
        var d9 = $.Deferred(); // FleetType
        var d10 = $.Deferred(); // MDVR Model
        var d11 = $.Deferred(); // MDVRs
        var d12 = $.Deferred(); 


        $.when(d1).done((r1) => {
            var vehicle = r1;

            this._originalDMS = [];
            this._originalADS = [];
            // Vehicle Type/Registered Type
            this.webRequestEnumResource.listEnumResource({
                companyId: WebConfig.userSession.currentCompanyId,
                types: [
                    Enums.ModelData.EnumResourceType.VehicleType,
                    Enums.ModelData.EnumResourceType.ERMVehicleType,
                    Enums.ModelData.EnumResourceType.VehicleRegisteredType
                ],
                sortingColumns: DefaultSorting.EnumResource
            }).done((response) => {
                Utility.applyFormattedPropertyToCollection(response["items"], "displayText", "{0} ({1})", "displayName", "value");
                ko.utils.arrayForEach(response["items"], (item) => {
                    switch (item.type) {
                        case Enums.ModelData.EnumResourceType.VehicleType:
                            this.typeOptions.push(item);
                            break;
                        case Enums.ModelData.EnumResourceType.VehicleRegisteredType:
                            if (item.value >= 1000 && item.value <= 1720) {
                                this._registeredTypePassengerCarOptions.push(item);
                            } else if (item.value >= 2000 && item.value <= 2920) {
                                this._registeredTypeTruckTrailerOptions.push(item);
                            }
                            break;
                        case Enums.ModelData.EnumResourceType.ERMVehicleType:
                            this.ermTypeOptions.push(item);
                            break;
                    }
                });

                if (vehicle && vehicle.vehicleType) {
                    this.type(ScreenHelper.findOptionByProperty(this.typeOptions, "value", vehicle.vehicleType));

                    if (vehicle.vehicleType === Enums.ModelData.VehicleType.PassengerCar) {
                        this.registeredTypeOptions(this._registeredTypePassengerCarOptions);
                    } else if (vehicle.vehicleType === Enums.ModelData.VehicleType.Truck || vehicle.vehicleType === Enums.ModelData.VehicleType.Trailer) {
                        this.registeredTypeOptions(this._registeredTypeTruckTrailerOptions);
                    }

                    if (vehicle.vehicleRegisteredTypeId) {
                        this.registeredType(ScreenHelper.findOptionByProperty(this.registeredTypeOptions, "id", vehicle.vehicleRegisteredTypeId));
                    }
                }

                if (vehicle && vehicle.ermVehicleType) {
                    this.ermType(ScreenHelper.findOptionByProperty(this.ermTypeOptions, "value", vehicle.ermVehicleType));
                }

                d2.resolve();
            }).fail((e) => {
                d2.reject(e);
            });

            // Vehicle Model
            var includeVehicleModelIds = vehicle && vehicle.modelId ? [vehicle.modelIds] : null;
            this.webRequestVehicleModel.listVehicleModelSummary({
                enable: true,
                includeIds: includeVehicleModelIds,
                sortingColumns: DefaultSorting.VehicleModelByBrandModelCode
            }).done((response) => {
                Utility.applyFormattedPropertyToCollection(response["items"], "displayText", "{0} - {1} ({2})", "brand", "model", "code");
                this.modelOptions(response["items"]);
                if (vehicle && vehicle.modelId) {
                    this.model(ScreenHelper.findOptionByProperty(this.modelOptions, "id", vehicle.modelId));
                }
                d3.resolve();
            }).fail((e) => {
                d3.reject(e);
            });

            // Vehicle Icon
            var includeVehicleIconIds = vehicle && vehicle.iconId ? [vehicle.iconId] : null;
            this.webRequestVehicleIcon.listVehicleIconSummary({
                includeAssociationNames: [EntityAssociation.VehicleIcon.PreviewImage],
                includeIds: includeVehicleIconIds,
                sortingColumns: DefaultSorting.VehicleIconByNameCode
            }).done((response) => {
                this.iconOptions(this.getVehicleIconOptions(response["items"]));
                if (vehicle && vehicle.iconId) {
                    this.icon(ScreenHelper.findOptionByProperty(this.iconOptions, "value", vehicle.iconId));
                } else if (this.iconOptions().length > 0) {
                    //get first icon to set as default
                    this.icon(_.head(this.iconOptions()));
                }
                d4.resolve();
            }).fail((e) => {
                d4.reject(e);
            });

            // Business Unit
            this.webRequestBusinessUnit.listBusinessUnitSummary({
                companyId: WebConfig.userSession.currentCompanyId,
                isIncludeFullPath: true,
                sortingColumns: DefaultSorting.BusinessUnit
            }).done((response) => {
                Utility.applyFormattedPropertyToCollection(response["items"], "displayText", "{0} ({1})", "name", "fullCode");
                this.businessUnitOptions(response.items);
                if (vehicle && vehicle.businessUnitId) {
                    this.businessUnitId(vehicle.businessUnitId.toString());
                }
                d5.resolve();
            }).fail((e) => {
                d5.reject(e);
            });

            // Trail
            if (vehicle && vehicle.businessUnitId) {
                var includeTrailIds = vehicle && vehicle.trailId ? [vehicle.trailId] : null;
                this.webRequestVehicle.listVehicleSummary({
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: [vehicle.businessUnitId],
                    vehicleTypes: [Enums.ModelData.VehicleType.Trail],
                    enable: true,
                    includeIds: includeTrailIds,
                    sortingColumns: DefaultSorting.VehicleByLicenseReferenceNo
                }).done((response) => {
                    this.trailOptions(this.getTrailOptions(response["items"]));
                    if (vehicle.trailId) {
                        this.trail(ScreenHelper.findOptionByProperty(this.trailOptions, "id", vehicle.trailId));
                    }
                    d6.resolve();
                }).fail((e) => {
                    d6.reject(e);
                });
            } else {
                d6.resolve();
            }

            //Category
            this.webRequestCategory.listCategory({
                categoryId: Enums.ModelData.CategoryType.Vehicle,
            }).done((category) => {
                this.CategoryOptions(category.items);
                if (vehicle && vehicle.categoryId) {
                    this.Category(ScreenHelper.findOptionByProperty(this.CategoryOptions, "id", vehicle.categoryId));
                }
                d8.resolve();
            }).fail((e) => {
                d8.reject(e);
            });

            //FleetType
            this.webRequestCategory.listCategory({
                categoryId: Enums.ModelData.CategoryType.ShippingType,
            }).done((fleetType) => {
                this.fleetTypeOptions(fleetType.items);
                if (vehicle && vehicle.fleetType) {
                    this.fleetType(ScreenHelper.findOptionByProperty(this.fleetTypeOptions, "id", vehicle.fleetType));
                }
                d9.resolve();
            }).fail((e) => {
                d9.reject(e);
            });

            // MDVR Model
            this.webRequestMDVRModel.listMDVRModelList({
                enable: this.enableModelMDVR
            }).done((resultmdvrModel) => {
                this.optionMDVRModels(resultmdvrModel["items"]);
                if (vehicle && vehicle.mdvrModelId) {
                    this.mdvrModel(ScreenHelper.findOptionByProperty(this.optionMDVRModels, "id", vehicle.mdvrModelId));
                }
                d10.resolve();
            }).fail((e) => {
                d10.reject(e);
            });

            // MDVR 
            if (vehicle && vehicle.mdvrModel) {
                this.webRequestMDVR.listMDVRSummary({
                    modelId: vehicle.mdvrModelId,
                    inUse: this.inuseMDVR
                }).done((resultmdvr) => {
                    this.optionMDVRs(resultmdvr["items"]);
                    // if (this.mdvrId()) {
                    this.mdvrDeviceId(ScreenHelper.findOptionByProperty(this.optionMDVRs, "id", vehicle.mdvrId));
                    // }
                    d11.resolve();
                }).fail((e) => {
                    d11.reject(e);
                });
            } else {
                d11.resolve();
            }

            if (vehicle) {
                this.displayImages(vehicle.images);
                this.enable(ScreenHelper.findOptionByProperty(this.enableOptions, "value", vehicle.enable));
                this.chassisNo(vehicle.chassisNo);
                this.seat(vehicle.seat);
                this.volume(vehicle.volume);
                this.volumeUnit(vehicle.volumeUnitSymbol);
                this.description(vehicle.description);
                this.referenceNo(vehicle.referenceNo);
                this.fuelRate(vehicle.fuelRate);
                this.stopDuration(vehicle.stopDuration);
                this.overSpeedDelay(vehicle.overSpeedDelay);
                this.parkEngineOnLimit(vehicle.parkEngineOnLimit);
                this.defaultOverSpeedLimit(vehicle.defaultOverSpeedLimit);
                this.defaultDriverBu(vehicle.DriverBuId);

                let rowId = 1;
                vehicle.dmsInfos.forEach(x => {
                    x.rowId = rowId;
                    rowId++;
                });
                rowId = 1;
                vehicle.adasInfos.forEach(x => {
                    x.rowId = rowId;
                    rowId++;
                })
                
                this.dmsItem(vehicle.dmsInfos);
                this.adsItem(vehicle.adasInfos);
                this.selectedInverseFeatures(_.size(vehicle.inverseFeatureIds) ? vehicle.inverseFeatureIds : []);
                this._originalDMS = _.cloneDeep(vehicle.dmsInfos);
                this._originalADS = _.cloneDeep(vehicle.adasInfos);
                

                this.license(new VehicleLicenseDetails(
                    vehicle.license.license,
                    vehicle.license.licensePlate,
                    vehicle.license.licenseProvince,
                    vehicle.license.licenseCountry,
                    vehicle.license.infoStatus,
                    vehicle.license.id
                ));

                this.defaultDriverId(vehicle.defaultDriverId);
                this.coDriver1Id(vehicle.coDriver1Id);
                this.coDriver2Id(vehicle.coDriver2Id);

                let buId = vehicle.driverBuId ? vehicle.driverBuId : vehicle.businessUnitId;
                this.driverBuId(buId);
                this.prevBusinessUnitId(vehicle.businessUnitId);

                this.smsAlert(vehicle.smsAlert);
                this.emailAlert(vehicle.emailAlert);


                if (vehicle.installedDate) {
                    let installDate = new Date(vehicle.installedDate);
                    let getTime = moment(installDate).format('HH:mm');

                    this.installDate(installDate);
                    this.installTime(getTime);
                }
            }
        
        // DC Custom POI 
        this.webRequestCustomPOI.listCustomPOISummary({
            companyId: WebConfig.userSession.currentCompanyId,
            includeAssociationNames: [
                EntityAssociation.BusinessUnit.ChildBusinessUnits
            ],
            sortingColumns: DefaultSorting.BusinessUnit,
            poiCategoryType : 1 // 1 is Poi Category type DC
        }).done((response) => {

            this.optionDcCustomPOIs(response["items"]);
            this.dcCustomPOI(ScreenHelper.findOptionByProperty(this.optionDcCustomPOIs, "value", response.name));

            d12.resolve();
        }).fail((e) => {
            d12.reject(e);
        });
	   
	   
        }).fail((e) => {
            d2.reject(e);
            d3.reject(e);
            d4.reject(e);
            d5.reject(e);
            d6.reject(e);
            d8.reject(e);
            d9.reject(e);
            d10.reject(e); // MDVR Model
            d11.reject(e); // MDVR Model            
            d12.reject(e); 

        });

        if (this.mode === Screen.SCREEN_MODE_CREATE) {
            this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId).done((setting) => {
                this.volumeUnit(setting.volumeUnitSymbol);
                d7.resolve();
            }).fail((e) => {
                d7.reject(e);
            });
        } else {
            d7.resolve();
        }

        $.when(d2, d3, d4, d5, d6, d7, d10, d11,d12).done(() => {
            d0.resolve();
        }).fail((e) => {
            d0.reject(e);
        });

        $.when(d0).done(() => {
            this._changeTypeSubscribe = this.type.subscribe((type) => {
                if (type) {
                    if (type.value === Enums.ModelData.VehicleType.Trail) {
                        this.seat(null);
                        this.fuelRate(null);

                        // clear Default Drivers
                        this.defaultDriverId(null);
                        this.coDriver1Id(null);
                        this.coDriver2Id(null);

                        //publishMessage to close default drivers blade
                        this.publishMessage("ca-asset-vehicle-manage-type-changed", type.value);
                    }

                    if (type.value !== Enums.ModelData.VehicleType.Trailer) {
                        this.trail(null);
                    }

                    this.registeredType(null);
                    if ([Enums.ModelData.VehicleType.Truck, Enums.ModelData.VehicleType.Trailer, Enums.ModelData.VehicleType.PassengerCar].indexOf(type.value) > -1) {
                        if (type.value === Enums.ModelData.VehicleType.PassengerCar) {
                            this.registeredTypeOptions.replaceAll(this._registeredTypePassengerCarOptions);
                        } else {
                            this.registeredTypeOptions.replaceAll(this._registeredTypeTruckTrailerOptions);
                        }
                    } else {
                        this.registeredTypeOptions.removeAll();
                    }
                }
            });

            this._changeBusinessUnitSubscribe = this.businessUnitId.subscribe((businessUnitId) => {
                // ถ้า Bu มาจากการ cancel ของ Dialog Change Bu จะเป็น array 
                if(Array.isArray(businessUnitId)){
                    businessUnitId = businessUnitId[0];
                }

                // Reset default drivers
                if(this.mode === Screen.SCREEN_MODE_CREATE){

                    if (this.defaultDriverId() || this.coDriver1Id() || this.coDriver2Id()) {
                        this.showMessageBox(null, this.i18n("M095")(), BladeDialog.DIALOG_OK).done((button) => {
                        });
                    }

                    this.defaultDriverId(null);
                    this.coDriver1Id(null);
                    this.coDriver2Id(null);

                    this.driverBuId(businessUnitId);
                }
                else if(businessUnitId && this.prevBusinessUnitId() != businessUnitId){

                    if(this.defaultDriverId() || this.coDriver1Id() || this.coDriver2Id()){
                        let originalBu = this.prevBusinessUnitId();
                        this.prevBusinessUnitId(businessUnitId);

                        this.widget('ca-asset-vehicle-view-confirm-dialog-changeBu',originalBu, {
                            title: this.i18n('Vehicle_PleaseSelectOptionChangeBu'),
                            modal: true,
                            resizable: false,
                            minimize: false,
                            target: "ca-asset-vehicle-view-confirm-dialog-changeBu",
                            width: "325",
                            height: "150",
                            id: 'ca-asset-vehicle-view-confirm-dialog-changeBu',
                            left: "40%",
                            bottom: "30%"
                        });
                    }
                    else{
                        this.driverBuId(businessUnitId);
                    }
                }
                this.publishMessage("ca-asset-vehicle-manage-changed-business-unit", businessUnitId);

                // Reset trail
                this.trail(null);
                if (businessUnitId) {
                    this.isBusy(true);
                    this.webRequestVehicle.listVehicleSummary({
                        companyId: WebConfig.userSession.currentCompanyId,
                        businessUnitIds: [businessUnitId],
                        vehicleTypes: [Enums.ModelData.VehicleType.Trail],
                        enable: true,
                        sortingColumns: DefaultSorting.VehicleByLicenseReferenceNo
                    }).done((response) => {
                        this.trailOptions.replaceAll(this.getTrailOptions(response["items"]));
                        this.isBusy(false);
                    }).fail((e) => {
                        this.trailOptions.removeAll();
                        this.isBusy(false);
                        this.handleError(e);
                    });
                } else {
                    this.trailOptions.removeAll();
                }
            });

            // MDVR Model
            this._mdvrModelSubscribe = this.mdvrModel.subscribe((mdvrModel) => {
                if (mdvrModel) {
                    this.webRequestMDVR.listMDVRSummary({
                        modelId: mdvrModel.id,
                        inUse: this.inuseMDVR
                    }).done((resultmdvr) => {
                        this.optionMDVRs(resultmdvr["items"]);

                        //  if (this.mdvrId()) {
                        //      this.mdvrDeviceId(ScreenHelper.findOptionByProperty(this.optionMDVRs, "deviceId", this.mdvrId()));
                        //  }

                    }).fail((e) => {

                    });
                } else {
                    this.optionMDVRs([])
                }

            });

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Setup trackChange/validation
     * @memberOf VehicleViewScreen
     */
    setupExtend() {
        this.images.extend({
            trackArrayChange: true
        });
        this.type.extend({
            trackChange: true
        });
        this.ermType.extend({
            trackChange: true
        });
        this.model.extend({
            trackChange: true
        });
        this.icon.extend({
            trackChange: true
        });
        this.businessUnitId.extend({
            trackChange: true
        });
        this.enable.extend({
            trackChange: true
        });
        this.chassisNo.extend({
            trackChange: true
        });
        this.seat.extend({
            trackChange: true
        });
        this.volume.extend({
            trackChange: true
        });
        this.description.extend({
            trackChange: true
        });
        this.referenceNo.extend({
            trackChange: true
        });
        this.fuelRate.extend({
            trackChange: true
        });
        this.stopDuration.extend({
            trackChange: true
        });
        this.registeredType.extend({
            trackChange: true
        });
        this.trail.extend({
            trackChange: true
        });
        this.overSpeedDelay.extend({
            trackChange: true
        });
        this.parkEngineOnLimit.extend({
            trackChange: true
        });
        this.defaultOverSpeedLimit.extend({
            trackChange: true
        });
        this.license.extend({
            trackChange: true
        });
        this.defaultDriverId.extend({
            trackChange: true
        });
        this.coDriver1Id.extend({
            trackChange: true
        });
        this.coDriver2Id.extend({
            trackChange: true
        });
        this.smsAlert.extend({
            trackChange: true
        });
        this.emailAlert.extend({
            trackChange: true
        });

        // validation
        this.imageSelectedFile.extend({
            fileExtension: ['JPG', 'JPEG', 'PNG'],
            fileSize: 4,
        });

        this.type.extend({
            required: true
        });
        this.model.extend({
            required: true
        });
        this.icon.extend({
            required: true
        });
        this.businessUnitId.extend({
            required: true
        });
        this.enable.extend({
            required: true
        });
        this.chassisNo.extend({
            serverValidate: {
                params: "ChassisNo",
                message: this.i18n("M010")()
            }
        });
        this.seat.extend({
            min: 1
        });
        this.volume.extend({
            min: 0.01
        });
        this.referenceNo.extend({
            serverValidate: {
                params: "ReferenceNo",
                message: this.i18n("M010")()
            }
        });
        this.fuelRate.extend({
            min: 0.01
        });
        this.stopDuration.extend({
            required: true,
            min: 1
        });
        this.overSpeedDelay.extend({
            required: true,
            min: 0
        });
        this.parkEngineOnLimit.extend({
            min: 1
        });
        this.defaultOverSpeedLimit.extend({
            min: 0.01
        });
        this.license.extend({
            validation: {
                validator: (val) => {
                    return !_.isNil(val) && !_.isNil(val.license) && !_.isEmpty(val.license);
                },
                message: this.i18n("M001")()
            }
        });

        this.validationModel = ko.validatedObservable({
            imageSelectedFile: this.imageSelectedFile,
            type: this.type,
            model: this.model,
            icon: this.icon,
            businessUnitId: this.businessUnitId,
            enable: this.enable,
            chassisNo: this.chassisNo,
            seat: this.seat,
            volume: this.volume,
            referenceNo: this.referenceNo,
            fuelRate: this.fuelRate,
            stopDuration: this.stopDuration,
            overSpeedDelay: this.overSpeedDelay,
            parkEngineOnLimit: this.parkEngineOnLimit,
            defaultOverSpeedLimit: this.defaultOverSpeedLimit,
            license: this.license
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}
    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedSubMenuItem(null);
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            // check if display images array contains invalid image, return.            
            if (this.displayImagesContainsInvalidImage()) {
                return;
            }

            var model = this.generateModel();
            model.dmsInfos = this.dmsItem() ? this.dmsItem().map( function(element) {return {"id":element.id}}) : [];
            this.isBusy(true);

            // if(this.mode == Screen.SCREEN_MODE_UPDATE && this.businessUnitId() != this.prevBusinessUnitId()){
                

            //     this.isBusy(false);

            //     this.subscribeMessage("ca-asset-vehicle-confirm-dialog-changeBu", () => {
            //         this.close(true);
            //     });
                
            // }
            //else{
                switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestVehicle.createVehicle(model, true).done((vehicle) => {
                        this.isBusy(false);
                        this.publishMessage("ca-asset-vehicle-changed", [vehicle.id]);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);
                        this.handleError(e);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.webRequestVehicle.updateVehicle(model).done(() => {
                        this.isBusy(false);
                        this.publishMessage("ca-asset-vehicle-changed");
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);
                        this.handleError(e);
                    });
                    break;
                }
            //}
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}


    /**
     * 
     * Handle when sub menu item click
     * @param {any} index
     * 
     * @memberOf VehicleManageScreen
     */
    onSelectSubMenuItem(index) {
        if (this.journeyCanClose()) {
            this.goToSubPage(index);
        } else {
            this.showMessageBox(null, this.i18n("M100")(),
                BladeDialog.DIALOG_OKCANCEL).done((button) => {
                switch (button) {
                    case BladeDialog.BUTTON_OK:
                        this.goToSubPage(index);
                        break;
                }
            });
        }
    }

    /**
     * 
     * Go to sub menu item screen
     * @param {any} index
     * 
     * @memberOf VehicleManageScreen
     */
    goToSubPage(index) {
        var page = "";
        var options = {
            mode: this.mode
        };

        switch (index) {
            case 1:
                page = "ca-asset-vehicle-manage-license-detail-manage";
                options.licenseDetails = this.license();
                break;
            case 2:
                page = "ca-asset-vehicle-manage-default-driver-manage";
                options.businessUnitId =  this.driverBuId();
                options.defaultDrivers = new VehicleDefaultDrivers(this.defaultDriverId(), this.coDriver1Id(), this.coDriver2Id());
                options.enableDropdown = this.valChangeBu() == 1 ? false : true;
                break;
            case 3:
                page = "ca-asset-vehicle-manage-alert-manage";
                options.alerts = new VehicleAlerts(this.smsAlert(), this.emailAlert());
                break;
            case 4:
                page = "bo-shared-accessible-inverse-features-select";
                options.selectedInverseFeatures = _.clone(this.selectedInverseFeatures());
                options.mode = null;
                break;
                
        }

        this.forceNavigate(page, options);
        this.selectedSubMenuItem(index);
    }

    /**
     * Generate info for webRequest from view model
     * @memberOf VehicleManageScreen
     */
    generateModel() {
        var dmsInfosItem = [];
        var adsInfosItem = [];
        var model = {
            images: this.images(),
            vehicleType: this.type().value,
            ermVehicleType: this.ermType() ? this.ermType().value : null,
            modelId: this.model().id,
            iconId: this.icon().value,
            businessUnitId: this.businessUnitId(),
            enable: this.enable().value,
            chassisNo: this.chassisNo(),
            seat: this.seat(),
            volume: this.volume(),
            description: this.description(),
            referenceNo: this.referenceNo(),
            fuelRate: this.fuelRate(),
            stopDuration: this.stopDuration(),
            vehicleRegisteredTypeId: this.registeredType() ? this.registeredType().id : null,
            trailId: this.trail() ? this.trail().id : null,
            overSpeedDelay: this.overSpeedDelay(),
            parkEngineOnLimit: this.parkEngineOnLimit(),
            defaultOverSpeedLimit: this.defaultOverSpeedLimit(),
            license: this.license(),
            defaultDriverId: this.defaultDriverId(),
            coDriver1Id: this.coDriver1Id(),
            coDriver2Id: this.coDriver2Id(),
            smsAlert: this.smsAlert(),
            emailAlert: this.emailAlert(),
            infoStatus: this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update,
            id: this.id,
            categoryId: this.Category() ? this.Category().id : null,
            fleetType: this.fleetType() ? this.fleetType().id : null,
            mdvrModelId: this.mdvrModel() ? this.mdvrModel().id : null,
            mdvrId: this.mdvrDeviceId() ? this.mdvrDeviceId().id : null,
            inverseFeatureIds: this.selectedInverseFeatures(),
            setDefaultDriver : this.mode == Screen.SCREEN_MODE_UPDATE && this.valChangeBu() == 1 ? this.valChangeBu() : null,
            dcpoiId : this.dcCustomPOI() ? this.dcCustomPOI().id : null
        };

        if (this.installDate()) {
            model.installedDate = moment(new Date(this.installDate())).format("YYYY-MM-DD") + " " + 
                                  (this.installTime() ? this.installTime() : "00:00");
        }

        model.dmsInfos = this.dmsItem();
        model.adasInfos = this.adsItem();
        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            this.dmsItem().forEach((v) => {
                dmsInfosItem.push(v);
            });

            this.adsItem().forEach((v) => {
                adsInfosItem.push(v);
            });

            this._originalDMS.forEach((o) => {
                let findItem = $.grep(this.dmsItem(), (n, m) => {
                    return o.id == n.id;
                });
                if (findItem.length == 0) {
                    o.infoStatus = Enums.InfoStatus.Delete;
                    dmsInfosItem.push(o);
                }
            });

            this._originalADS.forEach((o) => {
                let findItem = $.grep(this.adsItem(), (n, m) => {
                    return o.id == n.id;
                });
                if (findItem.length == 0) {
                    o.infoStatus = Enums.InfoStatus.Delete;
                    adsInfosItem.push(o);
                }
            });

            model.dmsInfos = dmsInfosItem;
            model.adasInfos = adsInfosItem;
        }
        return model;
    }

    /**
     * 
     * Create vehicle icon array for image combobox
     * @static
     * @param {any} vehicleIcons
     * @returns
     * 
     * @memberOf VehicleManageScreen
     */
    getVehicleIconOptions(vehicleIcons) {
        return ko.utils.arrayMap(vehicleIcons, function (vehicleIcon) {
            Utility.applyFormattedPropertyPredicate(vehicleIcon, "displayText", (x) => {
                if (!_.isNil(x.code) && !_.isEmpty(x.code)) {
                    return "{0} ({1})";
                }
                return "{0}";
            }, "name", "code");
            return {
                image: vehicleIcon.previewImage ? vehicleIcon.previewImage.fileUrl : "",
                value: vehicleIcon.id,
                text: vehicleIcon.displayText
            };
        });
    }


    /**
     * Prepare options in format "license (reference No)"
     * 
     * @param {any} items
     * @returns
     * 
     * @memberOf VehicleManageScreen
     */
    getTrailOptions(items) {
        // loop for prepare displayText
        items.forEach((item) => {
            Utility.applyFormattedPropertyPredicate(item, "displayText", (x) => {
                if (!_.isNil(x.referenceNo) && !_.isEmpty(x.referenceNo)) {
                    return "{0} ({1})";
                }
                return "{0}";
            }, "license", "referenceNo");
        });

        return items;
    }

    /**
     * On click remove image icon
     */
    onRemoveFile(index) {
        var image = null;

        switch (index) {
            case 0:
                image = this.image0();
                break;
            case 1:
                image = this.image1();
                break;
            case 2:
                image = this.image2();
                break;
            case 3:
                image = this.image3();
                break;
        }

        if (image && image.id > 0) {
            image.infoStatus = Enums.InfoStatus.Delete;
            this.deleteImages.push($.extend(true, {}, image));
        }

        this.displayImages.splice(index, 1);

        if (index === 0) {
            this.imageSelectedFile(null);
        }
    }

    /**
     * Hook onbefore fileupload
     */
    onBeforeUpload(isValidToSubmit) {
        if (!isValidToSubmit) {
            // add invalid image object which id = 0, infoStatus = 0 and fileUrl = placeholderUrl
            this.addImage({
                fileName: this.imageSelectedFile().name,
                entityId: 0,
                entityType: 0,
                fieldName: 0,
                filePath: "",
                fileType: 0,
                isPrimary: false,
                fileUrl: this.imagePlaceholderUrl,
                tempUploadFilePath: "",
                infoStatus: 0,
                id: 0
            });
        }
    }

    /**
     * Hook on fileuploadsuccess
     * @param {any} data
     */
    onUploadSuccess(data) {
        if (data && data.length > 0) {
            if (this.displayImages().length < 4 || this.displayImagesContainsInvalidImage()) {
                var image = data[0];
                image.infoStatus = Enums.InfoStatus.Add;
                this.addImage(image);
            }
        }
    }

    /**
     * 
     * Hook on fileuploadfail
     * @param {any} e
     */
    onUploadFail(e) {
        this.handleError(e);
    }

    /**
     * Add image to the beginning of display images array
     * 
     * @param {any} image
     * 
     * @memberOf VehicleManageScreen
     */
    addImage(image) {
        // find if displayImages array contains invalid image
        var invalidImage = this.getInvalidImage();

        if (invalidImage) {
            //if found invalid image, replace invalid image with new upload image
            var index = _.findIndex(this.displayImages(), invalidImage);
            this.displayImages.splice(index, 1, image);
        } else {
            //if not found invaid image, insert new upload image as first item in array
            this.displayImages.unshift(image);
        }
    }

    /**
     * 
     * Get invaid image in display images array
     * @returns
     * 
     * @memberOf VehicleManageScreen
     */
    getInvalidImage() {
        return ko.utils.arrayFirst(this.displayImages(), (item) => {
            return item.id === 0 && item.infoStatus === 0 && item.fileUrl === this.imagePlaceholderUrl;
        });
    }

    addDms() {
        this.navigate('ca-asset-vehicle-dms-detail', {
            dataTable: this.dmsItem(),
            dmsmode: this.mode,
            vehicleId: this.id == 0 ? null : this.id
        });
    }

    addAds() {
        this.navigate('ca-asset-vehicle-adas-detail', {
            dataTable: this.adsItem(),
            adsmode: this.mode,
            vehicleId: this.id == 0 ? null : this.id
        });
    }
}

export default {
    viewModel: ScreenBase.createFactory(VehicleManageScreen),
    template: templateMarkup
};
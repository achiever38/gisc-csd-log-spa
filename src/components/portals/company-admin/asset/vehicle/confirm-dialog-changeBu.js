import ko from "knockout";
import ScreenBaseWidget from "../../../screenbasewidget";
import templateMarkup from "text!./confirm-dialog-changeBu.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";

class VehicleChangeBuDialog extends ScreenBaseWidget {
    constructor(params) {
        super(params);

        // this.params = this.ensureNonObservable(params);

        this.isRadioDriver = ko.observable(1);
        this.buId = this.ensureNonObservable(params);

        $(".k-window-actions").remove();
        //$(".k-window-actions").css("visibility","hidden")
    }

    /**
     * Get WebRequest specific for DMS module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
 
        if (!isFirstLoad) {
            return;
        }
    }

    actionSave(data){
        this.isBusy(true);
    }

    /**
     * Save data to web service
     */
    onClickOK(){
        this.actionSave(this.params);
        var confirmDialog = $('#ca-asset-vehicle-view-confirm-dialog-changeBu').data("kendoWindow");
        this.publishMessage("ca-asset-vehicle-confirm-dialog-cahnge-bu",this.isRadioDriver());
        confirmDialog.close();
        
        
        
    }

    /**
     * Close modal dialog
     */
    onClickCancel(){
        var confirmDialog = $('#ca-asset-vehicle-view-confirm-dialog-changeBu').data("kendoWindow");
        this.publishMessage("ca-asset-vehicle-confirm-dialog-cahnge-bu-cancel",this.buId);
        confirmDialog.close();
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        this.params = null;
        this.src = null;
    }


}

export default {
    viewModel: ScreenBaseWidget.createFactory(VehicleChangeBuDialog),
    template: templateMarkup
};
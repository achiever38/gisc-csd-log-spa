﻿import ko from "knockout";
import templateMarkup from "text!./license-detail-manage.html";
import ScreenBase from "../../../screenbase";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import { VehicleLicenseDetails } from "./infos";
import { Enums } from "../../../../../app/frameworks/constant/apiConstant";
import * as Screen from "../../../../../app/frameworks/constant/screen";

class VehicleManageLicenseDetailsScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Assets_LicenseDetails")());

        this.mode = this.ensureNonObservable(params.mode);
        var vehicleLicenseDetails = this.ensureNonObservable($.extend(true, {}, params.licenseDetails));

        this.id = vehicleLicenseDetails.id;
        this.license = ko.observable(vehicleLicenseDetails.license);
        this.licensePlate = ko.observable(vehicleLicenseDetails.licensePlate);
        this.licenseProvince = ko.observable(vehicleLicenseDetails.licenseProvince);
        this.licenseCountry = ko.observable(vehicleLicenseDetails.licenseCountry);
    }
    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
    }
    
    /**
     * Setup trackChange/validation
     * @memberOf VehicleManageLicenseDetails
     */
    setupExtend() {
        this.license.extend({ trackChange: true });
        this.licensePlate.extend({ trackChange: true });
        this.licenseProvince.extend({ trackChange: true });
        this.licenseCountry.extend({ trackChange: true });

        this.license.extend({ 
            required: true,
            serverValidate: {
                params: "License",
                message: this.i18n("M010")()
            }
        });

        this.validationModel = ko.validatedObservable({
            license: this.license
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actOK", this.i18n("Common_OK")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actOK") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);
            var model = this.generateModel();
            this.webRequestVehicle.validateVehicleLicense(model).done(() => {
                this.isBusy(false);
                this.publishMessage("ca-asset-vehicle-manage-license-details-changed", model);
                this.close(true);
            }).fail((e) => {
                this.isBusy(false);
                this.handleError(e);
            });
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
    
    /**
     * 
     * Generate VehicleLicenseInfo for webRequest from view model
     * @returns
     * 
     * @memberOf VehicleManageLicenseDetails
     */
    generateModel() {
        return new VehicleLicenseDetails(
            this.license(),
            this.licensePlate(),
            this.licenseProvince(),
            this.licenseCountry(),
            this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update,
            this.id
        );
    }
}

export default {
    viewModel: ScreenBase.createFactory(VehicleManageLicenseDetailsScreen),
    template: templateMarkup
};
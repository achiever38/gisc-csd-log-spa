﻿import ko from "knockout";
import templateMarkup from "text!./maintenance-plan-complete.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import WebRequestVehicleMaintenancePlan from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenancePlan";
import WebRequestVehicleMaintenanceType from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenanceType";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestAppointment from "../../../../../app/frameworks/data/apitrackingcore/webRequestAppointment";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import { Enums } from "../../../../../app/frameworks/constant/apiConstant";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
class VehicleMaintenancePlanCompleteScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties

        this.bladeTitle(this.i18n("Assets_CompleteMaintenance")());
        this.bladeSize = BladeSize.Medium;

        this.currentDate = ko.observable(new Date());
        this.id = this.ensureNonObservable(params.id);
        this.info = null;
        this.actualCost = ko.observable();
        this.actualDate = ko.observable();
        this.actualDistance = ko.observable();
        this.performBy = ko.observable();
        this.currentDistance = params.id;
        this.remarkText = ko.observable();

        this.nextMaintenanceDate = ko.observable();
        this.nextMaintenanceDistance = ko.observable();

        this.dateFormat = WebConfig.companySettings.shortDateFormat;
        this.distanceUnit = ko.observable();
        this.currency = ko.observable();

        this.maintenanceTypes = ko.observableArray();
        this.maintenanceSubTypes = ko.observableArray();

        this.selectedMA = ko.observable();
        this.selectedSubMA = ko.observable();
        this.maintenanceTypeName = ko.observable();
        this.maintenanceSubTypeName = ko.observable();
        this.minMaintenanceDate = ko.observable();

        this.arrayItem = null;
        this.validateNewMAPlan = null;
        this.isChecked = ko.observable(true);
        //this.validationNextPlan = ko.pureComputed(function() {
        //    let condition1 = (this.nextMaintenanceDate() == undefined && (this.nextMaintenanceDistance() == undefined || this.nextMaintenanceDistance() == ''));
        //    return condition1;
        //}, this);

        //this.validationNextPlan.subscribe((val) => {  
        //    if(!val) {
        //        this.selectedMA.extend({ required: true });
        //        this.selectedSubMA.extend({ required: true });

        //        if(!this.validateNewMAPlan.isValid()) {
        //            this.validateNewMAPlan.errors.showAllMessages();
        //            return;
        //        }
        //    }
        //    else {
        //        this.selectedMA.extend({ required: false });
        //        this.selectedSubMA.extend({ required: false });
        //        this.validateNewMAPlan.errors.showAllMessages(false);

        //    }
        //});





        this.isChecked.subscribe(function (value) {
            this.isChecked(value);
        }.bind(this));

    }

    /**
     * Get WebRequest specific for Vehicle Maintenance Plan module in Web API access.
     * @readonly
     */
    get webRequestVehicleMaintenancePlan() {
        return WebRequestVehicleMaintenancePlan.getInstance();
    }

    get webRequestVehicleMaintenanceType() {
        return WebRequestVehicleMaintenanceType.getInstance();
    }
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    get webRequestAppointment() {
        return WebRequestAppointment.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();
        var dfdCompanySettings = null;
        //let defMAPlan = this.webRequestVehicleMaintenancePlan.getVehicleMaintenancePlan(this.id).done((response) => {
        //    this.info = response;
        //    this.distanceUnit(response.distanceUnitSymbol);
        //    this.currency(response.currencySymbol);

        //}).fail((e) => {

        //});

        let defMAPlan = this.webRequestVehicleMaintenancePlan.getVehicleMaintenancePlan(this.id);
        let defMAType = this.webRequestVehicleMaintenanceType.listVehicleMaintenanceType({ companyId: WebConfig.userSession.currentCompanyId });

        dfdCompanySettings = this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId).done((response) => {
            this.distanceUnit(response.distanceUnitSymbol);
            this.currency(response.currencySymbol);
        });

        $.when(defMAPlan, defMAType, dfdCompanySettings).done((responseMAPlan, responseMAType) => {
            if (responseMAPlan) {
                this.info = responseMAPlan;

                // console.log("Hello info",this.info);

                var model = {
                    "companyId": this.info.companyId,
                    "ids": [this.info.id],
                    "vehicleId": this.info.vehicleIds[0],
                    "status": 1,
                };
                //  console.log("Model",model);
                this.webRequestAppointment.listAppointment(model).done((response) => {
                    if (response) {

                        this.arrayItem = response["items"];


                        //console.log("arrayItem",this.arrayItem);
                    }
                });


                // this.distanceUnit(responseMAPlan.distanceUnitSymbol);
                //this.currency(responseMAPlan.currencySymbol);

                this.minMaintenanceDate(responseMAPlan.maintenanceDate);
            }

            if (responseMAType) {
                let lstMA = responseMAType["items"];
                if (lstMA.length > 0) {
                    this.maintenanceTypes.replaceAll(lstMA);
                    setTimeout(() => {
                        this.selectedMA(ScreenHelper.findOptionByProperty(this.maintenanceTypes, "id", this.info["maintenanceTypeId"]))

                    }, 500);
                }
                else {
                    this.maintenanceTypes.replaceAll([]);
                }
            }
            else {
                this.maintenanceTypes.replaceAll([]);
            }


            this.selectedMA.subscribe((maType) => {
                if (maType) {
                    this.isBusy(true);

                    this.maintenanceTypeName(maType.name);
                    this.webRequestVehicleMaintenanceType.listVehicleMaintenanceSubTypeForTest({ companyId: WebConfig.userSession.currentCompanyId, maintenanceTypeId: maType.id }).done((response) => {
                        let lstSubType = response["items"];
                        this.maintenanceSubTypes.replaceAll(lstSubType);

                        //setTimeout(() => {
                        //    this.selectedSubMA(ScreenHelper.findOptionByProperty(this.maintenanceSubTypes, "id", this.info["maintenanceSubTypeId"]))

                        //}, 500);

                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });
                }
                else {
                    this.maintenanceSubTypes.replaceAll([]);
                }
            });


            this.selectedSubMA.subscribe((SubmaType) => {
                if (SubmaType) {

                    this.maintenanceSubTypeName(SubmaType.name);
                }

            });

            dfd.resolve();

        }).fail((e) => { dfd.reject(e); });

        return dfd;
    }

    setupExtend() {
        this.actualCost.extend({ trackChange: true });
        this.actualDate.extend({ trackChange: true });
        this.actualDistance.extend({ trackChange: true });
        this.performBy.extend({ trackChange: true });
        this.nextMaintenanceDate.extend({ trackChange: true });
        this.nextMaintenanceDistance.extend({ trackChange: true });

        this.selectedMA.extend({ trackChange: true });
        this.selectedSubMA.extend({ trackChange: true });

        // validation
        this.actualCost.extend({ required: true, min: 0 });
        this.actualDate.extend({ required: true });
        this.actualDistance.extend({ required: true, min: 1 });
        this.performBy.extend({ required: true });

        this.selectedMA.extend({ required: true });
        this.selectedSubMA.extend({ required: true });
        //_.isNil(this.nextMaintenanceDistance()) 
        //_.isNil(this.selectedMA()) && _.isNil(this.selectedSubMA())
        var test = true;

        this.nextMaintenanceDate.extend({
            required: {
                onlyIf: () => {
                    return _.isNil(this.nextMaintenanceDistance()) || this.nextMaintenanceDistance() == "";
                },
                message: this.i18n("M149")()
            },
            validation: {
                validator: (value) => {
                    var isValidate = true;

                    if (value == null || value == "" || value == undefined) {

                    } else {


                        if (Date.parse(value) > Date.now()) {

                        } else {
                            isValidate = false;
                        }

                    }

                    return isValidate;

                },
                message: this.i18n("M149")()
            }
        });

        this.nextMaintenanceDistance.extend({
            min: 1,
            required: {
                onlyIf: () => {
                    return _.isNil(this.nextMaintenanceDate());
                },
                message: this.i18n("M149")()
            },
            validation: [{

                validator: (value) => {

                    var isValidate = true;

                    if (value == null || value == "" || value == undefined) {

                    }
                    else {
                        if (value > this.actualDistance()) {
                        } else {
                            isValidate = false;
                        }
                    }


                    return isValidate;
                },
                message: this.i18n("M153")()
            }]
        });




        this.validationModel = ko.validatedObservable({
            actualCost: this.actualCost,
            actualDate: this.actualDate,
            actualDistance: this.actualDistance,
            performBy: this.performBy
        });

        this.validateNewMAPlan = ko.validatedObservable({
            selectedMA: this.selectedMA,
            selectedSubMA: this.selectedSubMA,
            nextMaintenanceDate: this.nextMaintenanceDate,
            nextMaintenanceDistance: this.nextMaintenanceDistance

        });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {


            // console.log("Array",this.arrayItem);
            if (this.arrayItem.length > 0) {

                if (!this.validationModel.isValid()) {
                    this.validationModel.errors.showAllMessages();
                    return;
                }
                var textChange = this.i18n("Vehicle_MA_Common_Appointment")();

                var messageReplace = this.i18n('M145', [textChange]);

                //  this.showMessageBox(null, this.i18n("M145", this.i18n("Vehicle_MA_Common_Appointment"))(),
                this.showMessageBox(null, messageReplace,
                    BladeDialog.DIALOG_YESNO).done((button) => {
                        switch (button) {
                            case BladeDialog.BUTTON_YES:
                                this.isBusy(true);

                                var info = this.info;
                                info.actualDate = this.actualDate();
                                info.actualCost = this.actualCost();
                                info.actualDistance = this.actualDistance();
                                info.performBy = this.performBy();
                                info.maintenanceStatus = Enums.ModelData.MaintenanceStatus.Completed;


                                info.remark = this.remarkText() == undefined ? null : this.remarkText();

                                info.maintenanceTypeName = null;
                                info.maintenanceTypeId = null
                                info.maintenanceSubTypeName = null;
                                info.maintenanceSubTypeId = null;


                                if (this.isChecked()) {
                                    if (this.validateNewMAPlan.isValid()) {
                                        info.maintenanceDate = this.nextMaintenanceDate();
                                        info.maintenanceDistance = this.nextMaintenanceDistance();
                                        info.vehicleMaintenanceType = Enums.ModelData.VehicleMaintenanceType.Plan;
                                        // info.maintenanceSubType = this.selectedSubMA();


                                        info.maintenanceTypeName = this.maintenanceTypeName();
                                        info.maintenanceTypeId = this.selectedMA().id;
                                        info.maintenanceSubTypeName = this.maintenanceSubTypeName();
                                        info.maintenanceSubTypeId = this.selectedSubMA().id;
                                    }
                                    else {

                                        this.validateNewMAPlan.errors.showAllMessages();
                                        this.isBusy(false);
      
                                        return;
                                    }
                                }

                                //console.log("info",info);

                                this.webRequestVehicleMaintenancePlan.completeVehicleMaintenancePlan(info).done((response) => {
                                    this.isBusy(false);
                                    this.publishMessage("ca-asset-vehicle-maintenance-plan-updated");
                                    this.close(true);
                                }).fail((e) => {
                                    this.handleError(e);
                                }).always(() => {
                                    this.isBusy(false);
                                });

                                break;
                        }
                    });
            }

            else {

                if (!this.validationModel.isValid()) {
                    this.validationModel.errors.showAllMessages();
                    return;
                }
                this.isBusy(true);

                var info = this.info;
                info.actualDate = this.actualDate();
                info.actualCost = this.actualCost();
                info.actualDistance = this.actualDistance();
                info.performBy = this.performBy();
                info.maintenanceStatus = Enums.ModelData.MaintenanceStatus.Completed;
                info.remark = this.remarkText() == undefined ? null : this.remarkText();

                info.maintenanceTypeName = null;
                info.maintenanceTypeId = null
                info.maintenanceSubTypeName = null;
                info.maintenanceSubTypeId = null;

                //  console.log("info no Active",info);
                if (this.isChecked()) {
                    if (this.validateNewMAPlan.isValid()) {
                        info.maintenanceDate = this.nextMaintenanceDate();
                        info.maintenanceDistance = this.nextMaintenanceDistance();
                        info.vehicleMaintenanceType = Enums.ModelData.VehicleMaintenanceType.Plan;
                        // info.maintenanceSubType = this.selectedSubMA();


                        info.maintenanceTypeName = this.maintenanceTypeName();
                        info.maintenanceTypeId = this.selectedMA().id;
                        info.maintenanceSubTypeName = this.maintenanceSubTypeName();
                        info.maintenanceSubTypeId = this.selectedSubMA().id;
                    }
                    else {
                        this.validateNewMAPlan.errors.showAllMessages();
                        this.isBusy(false);
                        return;
                    }
                }

                this.webRequestVehicleMaintenancePlan.completeVehicleMaintenancePlan(info).done((response) => {
                    this.isBusy(false);
                    this.publishMessage("ca-asset-vehicle-maintenance-plan-updated");
                    this.close(true);
                }).fail((e) => {
                    this.handleError(e);
                }).always(() => {
                    this.isBusy(false);
                });

            }
        }



        //if (sender.id === "actSave") {











        //    }else{

        //        this.isBusy(true);

        //        var info = this.info;
        //        info.actualDate = this.actualDate();
        //        info.actualCost = this.actualCost();
        //        info.actualDistance = this.actualDistance();
        //        info.performBy = this.performBy();
        //        info.maintenanceStatus = Enums.ModelData.MaintenanceStatus.Completed;
        //        info.remark = this.remarkText();
        //        info.CurrentDistance =  5000;

        //        info.maintenanceTypeName = null;
        //        info.maintenanceTypeId = null
        //        info.maintenanceSubTypeName = null;                    
        //        info.maintenanceSubTypeId = null;

        //        console.log("info no Active",info);
        //        if(this.isChecked()) {
        //            if(this.validateNewMAPlan.isValid()){ 

        //                info.maintenanceDate = this.nextMaintenanceDate();
        //                info.maintenanceDistance = this.nextMaintenanceDistance();
        //                info.vehicleMaintenanceType = Enums.ModelData.VehicleMaintenanceType.Plan;
        //                // info.maintenanceSubType = this.selectedSubMA();


        //                info.maintenanceTypeName = this.maintenanceTypeName();
        //                info.maintenanceTypeId = this.selectedMA().id;
        //                info.maintenanceSubTypeName = this.maintenanceSubTypeName();                    
        //                info.maintenanceSubTypeId = this.selectedSubMA().id;
        //            }
        //            else {
        //                this.validateNewMAPlan.errors.showAllMessages();
        //                return;
        //            }
        //        }

        //        this.webRequestVehicleMaintenancePlan.completeVehicleMaintenancePlan(info).done((response) => {
        //            this.isBusy(false);
        //            this.publishMessage("ca-asset-vehicle-maintenance-plan-updated");
        //            this.close(true);

        //        }

        //        }



    }




    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(VehicleMaintenancePlanCompleteScreen),
    template: templateMarkup
};


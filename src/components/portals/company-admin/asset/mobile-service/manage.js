﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestMobileService from "../../../../../app/frameworks/data/apitrackingcore/webRequestMobileService";
import WebRequestOperatorPackage from "../../../../../app/frameworks/data/apitrackingcore/webRequestOperatorPackage";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestUser from "../../../../../app/frameworks/data/apicore/webRequestUser";
import {Constants} from "../../../../../app/frameworks/constant/apiConstant";
import {BusinessUnitFilter, PromotionFilter, UserFilter} from "./infos";
import Utility from "../../../../../app/frameworks/core/utility";

class MobileServiceManageScreen extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.mode = this.ensureNonObservable(params.mode);
        this.companyId = WebConfig.userSession.currentCompanyId;

        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
               this.bladeTitle(this.i18n("Assets_CreateMobileService")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("Assets_UpdateMobileService")());
                break;
        }

        this.mobileServiceId = this.ensureNonObservable(params.mobileServiceId, -1);

        this.promotions = ko.observableArray([]);
        this.businessUnits = ko.observableArray([]);
        this.users = ko.observableArray([]);

        this.mobile = ko.observable('');
        this.simCardSerialNumber = ko.observable('');
        this.selectedPromotion = ko.observable();
        this.selectedBusinessUnit = ko.observable(null);
        this.selectedUser = ko.observable();
        this.imei = ko.observable('');
        this.stopDuration = ko.observable('');
        this.overSpeedDelay = ko.observable('');
        this.startJobInBeginWaypointOnly = ko.observable(false);
        this.checkInInWaypointOnly = ko.observable(false);
        this.finishJobInEndWaypointOnly = ko.observable(false);

        this._businessUnitId = '';
        this._userId = 0;

        this._selectedBusinessUnitRef = this.selectedBusinessUnit.ignorePokeSubscribe((value)=>{
            this.selectedUser(null);
            var selectedBusinessUnit = this.selectedBusinessUnit();
            if(selectedBusinessUnit){
                this.isBusy(true);

                var userFilter = null;
                switch (this.mode) {
                    case Screen.SCREEN_MODE_CREATE:
                        userFilter = new UserFilter(selectedBusinessUnit);
                        break;
                    case Screen.SCREEN_MODE_UPDATE:
                        if(this._businessUnitId == selectedBusinessUnit){
                            userFilter = new UserFilter(selectedBusinessUnit, this._userId);
                        }else{
                            userFilter = new UserFilter(selectedBusinessUnit);
                        }
                        break;
                }

                this.webRequestUser.listUserSummary(userFilter).done((r)=> {
                    this.users(r["items"]);

                    this.isBusy(false);
                }).fail((e)=> {
                    this.handleError(e);
                    this.isBusy(false);
                });
            }else{
                this.users([]);
            }
        });
    }
    /**
     * Get WebRequest specific for Mobile Service module in Web API access.
     * @readonly
     */
    get webRequestMobileService() {
        return WebRequestMobileService.getInstance();
    }
    /**
     * Get WebRequest specific for Operator Package module in Web API access.
     * @readonly
     */
    get webRequestOperatorPackage() {
        return WebRequestOperatorPackage.getInstance();
    }
    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    /**
     * Get WebRequest specific for User in Web API access.
     * @readonly
     */
    get webRequestUser() {
        return WebRequestUser.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();
        var businessUnitFilter = new BusinessUnitFilter();
        var promotionFilter = new PromotionFilter();

        var a1 = (this.mode === Screen.SCREEN_MODE_UPDATE) ? this.webRequestMobileService.getMobileService(this.mobileServiceId) : null;
        var a2 = this.webRequestOperatorPackage.listOperatorPackage(promotionFilter);
        var a3 = this.webRequestBusinessUnit.listBusinessUnitSummary(businessUnitFilter);

        $.when(a1, a2, a3).done((r1, r2, r3) => {
            var mobileService = r1;

            var promotions = r2["items"];
            Utility.applyFormattedPropertyToCollection(promotions, "displayText", "{0} – {1}", "operator", "promotion")
            this.promotions(promotions);

            this.businessUnits(r3["items"]);
            if (mobileService) {
                this.mobile(mobileService.mobile);
                this.simCardSerialNumber(mobileService.simCardSerialNumber);
                this.selectedPromotion(ScreenHelper.findOptionByProperty(this.promotions, "id", mobileService.promotionId));
                this.selectedBusinessUnit.poke(mobileService.businessUnitId.toString());
                this.imei(mobileService.imei);
                this.stopDuration(mobileService.stopDuration);
                this.overSpeedDelay(mobileService.overSpeedDelay);
                this.startJobInBeginWaypointOnly(mobileService.startJobInBeginWaypointOnly);
                this.checkInInWaypointOnly(mobileService.checkInInWaypointOnly);
                this.finishJobInEndWaypointOnly(mobileService.finishJobInEndWaypointOnly);

                this._businessUnitId = mobileService.businessUnitId.toString();
                this._userId = mobileService.userId;

                var userFilter = new UserFilter(mobileService.businessUnitId, this._userId);
                this.webRequestUser.listUserSummary(userFilter).done((r4)=> {
                    this.users(r4["items"]);

                    this.selectedUser(ScreenHelper.findOptionByProperty(this.users, "id", this._userId));

                    dfd.resolve();
                }).fail((e) => {
                    dfd.reject(e);
                });
            }else{
                dfd.resolve();
            }   
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {

        this.mobile.extend({
            trackChange: true
        });
        this.simCardSerialNumber.extend({
            trackChange: true
        });
        this.selectedPromotion.extend({
            trackChange: true
        });
        this.selectedBusinessUnit.extend({
            trackChange: true
        });
        this.selectedUser.extend({
            trackChange: true
        });
        this.imei.extend({
            trackChange: true
        });
        this.stopDuration.extend({
            trackChange: true
        });
        this.overSpeedDelay.extend({
            trackChange: true
        });
        this.startJobInBeginWaypointOnly.extend({
            trackChange: true
        });
        this.checkInInWaypointOnly.extend({
            trackChange: true
        });
        this.finishJobInEndWaypointOnly.extend({
            trackChange: true
        });

        // Manual setup validation rules
        this.mobile.extend({
            required: true
        });
        this.selectedBusinessUnit.extend({
            required: true
        });
        this.selectedUser.extend({
            required: true
        });
        this.imei.extend({
            required: true,
            serverValidate: {
                params: "Imei",
                message: this.i18n("M010")()
            } 
        });
        this.stopDuration.extend({
            required: true
        });
        this.overSpeedDelay.extend({
            required: true
        });

        this.validationModel = ko.validatedObservable({
            mobile: this.mobile,
            selectedBusinessUnit: this.selectedBusinessUnit,
            selectedUser: this.selectedUser,
            imei: this.imei,
            stopDuration: this.stopDuration,
            overSpeedDelay: this.overSpeedDelay
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        if((this.mode === Screen.SCREEN_MODE_CREATE && WebConfig.userSession.hasPermission(Constants.Permission.CreateMobileService))
            || (this.mode === Screen.SCREEN_MODE_UPDATE && WebConfig.userSession.hasPermission(Constants.Permission.UpdateMobileService))) 
        {
             actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        }
        actions.push(this.createAction("actCancel", this.i18n("Common_Cancel")(), "cancel"));
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * Generate mobile service model from View Model
     * 
     * @returns mobile service model which is ready for ajax request 
     */
    generateModel() {
        var model = {
            id: this.mobileServiceId,
            companyId: this.companyId,
            mobile: this.mobile(),
            simCardSerialNumber: this.simCardSerialNumber(),
            promotionId: this.selectedPromotion() ? this.selectedPromotion().id : null,
            businessUnitId: this.selectedBusinessUnit(),
            userId: this.selectedUser().id,
            imei: this.imei(),
            stopDuration: this.stopDuration(),
            overSpeedDelay: this.overSpeedDelay(),
            startJobInBeginWaypointOnly: this.startJobInBeginWaypointOnly(),
            checkInInWaypointOnly: this.checkInInWaypointOnly(),
            finishJobInEndWaypointOnly: this.finishJobInEndWaypointOnly(),
        };
        return model;
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);
            var model = this.generateModel();
            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestMobileService.createMobileService(model).done((response) => {
                        this.publishMessage("ca-asset-mobile-service-changed", response.id);
                        this.isBusy(false);
                        this.close(true);
                    }).fail((e)=> {
                        this.isBusy(false);

                        this.handleError(e);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.webRequestMobileService.updateMobileService(model).done((response) => {
                        this.publishMessage("ca-asset-mobile-service-changed");
                        this.isBusy(false);
                        this.close(true);
                    }).fail((e)=> {
                        this.isBusy(false);

                        this.handleError(e);
                    });
                    break;
            }
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(MobileServiceManageScreen),
    template: templateMarkup
};
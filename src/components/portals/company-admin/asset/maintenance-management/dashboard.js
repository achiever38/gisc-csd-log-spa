﻿import ko from "knockout";
import "jquery-ui";
import "jqueryui-timepicker-addon";
import templateMarkup from "text!./dashboard.html";
import ScreenBase from "../../../screenbase";
import * as BladeMargin from "../../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webRequestCompany";
import WebRequestMaintenancePlanDashboard from "../../../../../app/frameworks/data/apitrackingcore/webRequestMaintenancePlanDashboard";
import { Enums } from "../../../../../app/frameworks/constant/apiConstant";

/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class MaintenanceManagementDashboard extends ScreenBase {

    /**
     * Creates an instance of AssetMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Vehicle_MA_Maintenance_Management_Dashboard")());
        this.bladeSize = BladeSize.XLarge_A1;

        //set bladesize to maximum 
        //champ : 25/8/2017
        this.bladeIsMaximized = true;

        this.lastSync = ko.observable();

        //if (Utility.isiOSDevice()) { // Check Ipad Device
        //    $(".div-dashboard-container .div-dashboard-row").css({
        //        "clear": "none"
        //    });
        //    $(".div-dashboard-container .div-dashboard-row .div-dashboard-col").css({
        //        "width": "48%"
        //    });
        //}

        this.ellipsisLength = 30;

        this.filter = { companyId: WebConfig.userSession.currentCompanyId };

        this.TOP10VehicleMostCostRankingData = ko.observable();
        this.Next30DaysActivityData = ko.observable();
        this.next30DayMaTypeData = ko.observable();
        this.TOP10BusinessUnitMostCostRankingData = ko.observable();
        this.Next7DaysAppointmentData = ko.observable();
        this.CalendarData = ko.observableArray();
        this.TOP10VihecleMostCostRankingSelected = (val) => { }

        this.currentData = null;

        this.Next30DaysActivitySelected = (val) => {
            let filter = {};
            switch (val.type) {
                case "OD":
                    filter.maintenanceStatus = Enums.ModelData.MaintenanceStatus.Overdue;
                    filter.next = 0;
                    break;
                case "N7":
                    filter.maintenanceStatus = Enums.ModelData.MaintenanceStatus.Pending;
                    filter.next = 7;
                    break;
                case "N15":
                    filter.maintenanceStatus = Enums.ModelData.MaintenanceStatus.Pending;
                    filter.next = 15;
                    break;
                case "N30":
                    filter.maintenanceStatus = Enums.ModelData.MaintenanceStatus.Pending;
                    filter.next = 30;
                    break;
                default:
                    break;
            }
            this.navigate("ca-asset-maintenance-management-ma-plan", { dashboard: filter });
        }

        this.next30DayMaTypeSelected = (val) => {
            let filter = {};
            filter.maintenanceType = val.maintenanceTypeIds;
            filter.maintenanceStatus = Enums.ModelData.MaintenanceStatus.Pending;
            filter.next = 30;
            this.navigate("ca-asset-maintenance-management-ma-plan", { dashboard: filter });
        }

        this.TOP10BusinessUnitMostCostRankingSelected = (val) => { }

        this.Next7DaysAppointmentSelected = (val) => {
            let filter = {};
            filter.status = Enums.ModelData.AppointmentStatus.Active;
            filter.date = val.date;
            filter.next = 0;
            this.navigate("ca-asset-maintenance-management-appointment", { dashboard: filter });
        }

        this.CalendarSelected = (val) => {
            this.navigate("ca-asset-maintenance-management-appointment-view", { appointment: val });
        }

    }

    reloadData() {
        var dfd = $.Deferred();

        this.isBusy(true);

        var dfdDashboard = this.webRequestMaintenancePlanDashboard.listDashboard(this.filter);
        var dfdCompanySettings = this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId);

        $.when(dfdCompanySettings, dfdDashboard).done((
            companySettings,
            dashboardData
        ) => {
            this.currentData = dashboardData;
            //lastSync
            let lastSyncFormat = this.i18n("Vehicle_MA_LastSync")() + ": " + this.currentData.formatLastSync;
            this.lastSync(lastSyncFormat);

            //barChart TOP 10 Vehicle Most Cost Ranking

            var chartData_1 = {};
            chartData_1.label = this.i18n("Vehicle_MA_Dashboard_Title_TopTenVehicle")();
            chartData_1.backgroundColor = "rgb(70, 183, 143)";
            chartData_1.data = [];

            var chartDataOptions_1 = {};
            chartDataOptions_1.title = this.i18n("Vehicle_MA_Dashboard_Title_TopTenVehicle")();
            chartDataOptions_1.labels = [];
            chartDataOptions_1.bindingData = this.currentData.tenVehicleInfos;
            chartDataOptions_1.tooltipCallback = {
                title: function (tooltipItem, data) {
                    return "";
                },
                label: function (tooltipItem, data) {
                    return this.currentData.tenVehicleInfos[tooltipItem.index]["formatActualCost"] + " " + companySettings["currencySymbol"];
                }.bind(this)
            }
            chartDataOptions_1.yAxes = {
                title: this.i18n("Common_Vehicle")()
            }
            chartDataOptions_1.xAxes = {
                title: this.i18n("Assets_Cost", [companySettings["currencySymbol"]])
            }

            for (let i = 0; i < this.currentData.tenVehicleInfos.length; i++) {
                chartData_1.data.push(this.currentData.tenVehicleInfos[i].actualCost);
                chartDataOptions_1.labels.push(this.currentData.tenVehicleInfos[i].vehicleName);
            }

            this.TOP10VehicleMostCostRankingData({ data: [chartData_1], options: chartDataOptions_1 });

            ////barChart Next 30 Days Activity

            var chartData_2 = {};
            chartData_2.label = this.i18n("Vehicle_MA_Dashboard_Title_NextDayActivity")();
            chartData_2.backgroundColor = ["rgb(193, 39, 45)", "rgb(234, 172, 110)", "rgb(135, 204, 168)", "rgb(182, 216, 199)"];
            chartData_2.data = [];

            var chartDataOptions_2 = {};
            chartDataOptions_2.labels = [];
            chartDataOptions_2.title = this.i18n("Vehicle_MA_Dashboard_Title_NextDayActivity")();
            chartDataOptions_2.bindingData = this.currentData.nextDayActivityInfos;
            chartDataOptions_2.tooltipCallback = {
                title: function (tooltipItem, data) {
                    return "";
                },
                label: function (tooltipItem, data) {
                    return this.currentData.nextDayActivityInfos[tooltipItem.index]["formatMaintenancePlan"];
                }.bind(this)
            }
            chartDataOptions_2.yAxes = {
                title: this.i18n("Vehicle_MA_Plan_Label")()
            }
            chartDataOptions_2.xAxes = {
                title: this.i18n("Vehicle_MA_Common_Work")()
            }

            for (let i = 0; i < this.currentData.nextDayActivityInfos.length; i++) {
                chartData_2.data.push(this.currentData.nextDayActivityInfos[i].maintenancePlan);
                chartDataOptions_2.labels.push(this.currentData.nextDayActivityInfos[i].translateType);
            }

            this.Next30DaysActivityData({ data: [chartData_2], options: chartDataOptions_2 });

            ////pieChart Next 30 Days Maintenance Type

            var chartData_3 = {};
            chartData_3.label = this.i18n("Vehicle_MA_Dashboard_Title_NextDayMaintenanceType")();;
            //chartData_3.backgroundColor = ["#DB4240", "#FFC000", "#37B400", "#0066CC", "#FF6800", "#A0A700", "#FF8D00", "#678900", "#FFB53C", "#396000"];
            chartData_3.backgroundColor = ["#82CA9D","#BC8DBF","#FFD78C","#8493CA","#F9AD81","#00ACC1","#FFBFC4","#80DEEA","#FF9EA5","#7BCDC8"];
            chartData_3.data = [];

            var chartDataOptions_3 = {};
            chartDataOptions_3.labels = [];
            chartDataOptions_3.title = this.i18n("Vehicle_MA_Dashboard_Title_NextDayMaintenanceType")();
            chartDataOptions_3.bindingData = this.currentData.nextDayMaintenanceTypeInfos;

            for (let i = 0; i < this.currentData.nextDayMaintenanceTypeInfos.length; i++) {
                chartData_3.data.push(this.currentData.nextDayMaintenanceTypeInfos[i].maintenancePlan);
                chartDataOptions_3.labels.push(this.ellipsisText(this.currentData.nextDayMaintenanceTypeInfos[i].type, this.ellipsisLength));
            }

            chartDataOptions_3.tooltipCallback = {
                title: function (tooltipItem, data) {
                    return ""
                },
                label: function (tooltipItem, data) {
                    let val = this.currentData.nextDayMaintenanceTypeInfos[tooltipItem.index]["maintenancePlan"];
                    let percent = this.currentData.nextDayMaintenanceTypeInfos[tooltipItem.index].percent;
                    return val + " (" + percent + " %)"
                }.bind(this)
            }

            this.next30DayMaTypeData({ data: [chartData_3], options: chartDataOptions_3 });

            ////barChart TOP 10 Business Unit Most Cost Ranking

            var chartData_4 = {};
            chartData_4.label = this.i18n("Vehicle_MA_Dashboard_Title_TopTenBusinessUnit")();;
            chartData_4.backgroundColor = "#ffd972";
            chartData_4.data = [];

            var chartDataOptions_4 = {};
            chartDataOptions_4.labels = [];
            chartDataOptions_4.title = this.i18n("Vehicle_MA_Dashboard_Title_TopTenBusinessUnit")();
            chartDataOptions_4.bindingData = this.currentData.tenBusinessUnitInfos;
            chartDataOptions_4.tooltipCallback = {
                title: function (tooltipItem, data) {
                    return "";
                },
                label: function (tooltipItem, data) {
                    return this.currentData.tenBusinessUnitInfos[tooltipItem.index]["formatActualCost"] + " " + companySettings["currencySymbol"];
                }.bind(this)
            }
            chartDataOptions_4.yAxes = {
                title: this.i18n("Common_BusinessUnit")()
            }
            chartDataOptions_4.xAxes = {
                title: this.i18n("Assets_Cost", [companySettings["currencySymbol"]])
            }

            for (let i = 0; i < this.currentData.tenBusinessUnitInfos.length; i++) {
                chartData_4.data.push(this.currentData.tenBusinessUnitInfos[i].actualCost);
                chartDataOptions_4.labels.push(this.currentData.tenBusinessUnitInfos[i].businessUnitName);
            }

            this.TOP10BusinessUnitMostCostRankingData({ data: [chartData_4], options: chartDataOptions_4 });

            ////barChart Next 7 Days Appointment

            var chartData_5 = {};
            chartData_5.label = this.i18n("Vehicle_MA_Dashboard_Title_NextDayAppointment")();;
            chartData_5.backgroundColor = ["rgb(255, 215, 42)", "rgb(0, 169, 157)", "rgb(0, 169, 157)", "rgb(0, 169, 157)", "rgb(0, 169, 157)", "rgb(0, 169, 157)", "rgb(0, 169, 157)"];
            chartData_5.data = [];

            var chartDataOptions_5 = {};
            chartDataOptions_5.labels = [];
            chartDataOptions_5.title = this.i18n("Vehicle_MA_Dashboard_Title_NextDayAppointment")();
            chartDataOptions_5.bindingData = this.currentData.nextDayAppointmentInfos;
            chartDataOptions_5.tooltipCallback = {
                title: function (tooltipItem, data) {
                    return "";
                },
                label: function (tooltipItem, data) {
                    return this.currentData.nextDayAppointmentInfos[tooltipItem.index]["formatNumberOfAppointment"];
                }.bind(this)
            }
            chartDataOptions_5.yAxes = {
                title: this.i18n("Vehicle_MA_Common_Appointment")()
            }
            chartDataOptions_5.xAxes = {
                title: this.i18n("Assets_Date")()
            }

            for (let i = 0; i < this.currentData.nextDayAppointmentInfos.length; i++) {
                chartData_5.data.push(this.currentData.nextDayAppointmentInfos[i].numberOfAppointment);
                chartDataOptions_5.labels.push(this.currentData.nextDayAppointmentInfos[i].formatDate);
            }
            this.Next7DaysAppointmentData({ data: [chartData_5], options: chartDataOptions_5 });

            this.CalendarData(this.currentData.appointmentOnDayInfos);

            this.isBusy(false);
            dfd.resolve();
        }).fail((e) => {
            this.isBusy(false);
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        let dfd = $.Deferred();

        this.reloadData().done((e) => {
            dfd.resolve(e);
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() { }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) { }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdMAPlan", this.i18n("Vehicle_MA_Plan_Label")(), "svg-ic-mt-maintenancesplans"));
        commands.push(this.createCommand("cmdMAAppointment", this.i18n("Vehicle_MA_Appointment")(), "svg-ic-mt-appo-manage"));
        commands.push(this.createCommand("cmdMAType", this.i18n("Vehicle_MA_Common_Maintenance_Type")(), "svg-ic-mt-maintenancestypes"));
        commands.push(this.createCommand("cmdReport", this.i18n("Menu_Reports")(), "svg-ic-mt-report"));
        commands.push(this.createCommand("cmdRefresh", this.i18n("Common_Refresh")(), "svg-cmd-refresh"));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdMAPlan":
                this.navigate("ca-asset-maintenance-management-ma-plan", {});
                break;
            case "cmdMAAppointment":
                this.navigate("ca-asset-maintenance-management-appointment", { isFromApointment: true });
                break;
            case "cmdMAType":
                this.navigate("ca-asset-maintenance-management-ma-type", {});
                break;
            case "cmdReport":
                this.navigate("ca-asset-maintenance-management-report", {});
                break;
            case "cmdRefresh":
                this.reloadData();
                break;
            default:
                break;
        }
    }

    ellipsisText(text, length) {
        let returnVal = null;

        if (text.length <= 30)
            returnVal = text
        else
            returnVal = text.substr(0, length) + "...";

        return returnVal;
    }
    get webRequestMaintenancePlanDashboard() {
        return WebRequestMaintenancePlanDashboard.getInstance();
    }

    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }
}

export default {
    viewModel: ScreenBase.createFactory(MaintenanceManagementDashboard),
    template: templateMarkup
};
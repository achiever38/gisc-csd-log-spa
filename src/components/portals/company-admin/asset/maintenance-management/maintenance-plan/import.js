﻿import ko from "knockout";
import templateMarkup from "text!./import.html";
import ScreenBase from "../../../../screenbase";
import ScreenHelper from "../../../../screenhelper";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import Utility from "../../../../../../app/frameworks/core/utility";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import WebRequestCompany from "../../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestVehicleMaintenancePlan from "../../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenancePlan";
import { Enums } from "../../../../../../app/frameworks/constant/apiConstant";

class MaintenancePlanImport extends ScreenBase {
    
    /**
     * Creates an instance of AssetMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Common_Import")());
        this.bladeSize = BladeSize.XLarge_A1;

        this.fileName = ko.observable("");
        this.selectedFile = ko.observable(null);
        this.mimeType = [
            Utility.getMIMEType("xls"),
            Utility.getMIMEType("xlsx")
        ].join();
        this.distanceUnit = ko.observable();
        this.currency = ko.observable();
        this.media = ko.observable(null);
        this.importValidationError = ko.observable("");
        this.previewData = ko.observableArray([]);
        this.previewDataVisible = ko.observable(false);
        this.urlUpload = WebConfig.appSettings.uploadUrlTrackingCore;

        this.canSaveImportData = ko.pureComputed(() => {
            return _.isEmpty(this.importValidationError()) && !_.isNil(this.media());
        });

    
            // Set custom column title with DataTable
            this.onImportMaintenancePlanListTableReady = () => {
                if (!_.isEmpty(this.distanceUnit())) {


                    this.dispatchEvent("dtImportVehicles", "setColumnTitle", [
                        {
                            columnIndex: 3,
                            columnTitle: this.i18n("Common_CurrentDistance", [this.distanceUnit()])
                        },
                        {
                        columnIndex: 4,
                        columnTitle: this.i18n("Common_MaintenanceDistance", [this.distanceUnit()])
                        },
                    {
                        columnIndex: 5,
                        columnTitle: this.i18n("Common_ReminderBeforeMaintenanceDistance", [this.distanceUnit()])
                    }
                    ]);
                }

                if (!_.isEmpty(this.currency())) {


                    this.dispatchEvent("dtImportVehicles", "setColumnTitle", [{
                        columnIndex: 10,
                        columnTitle: this.i18n("Common_EstimateCost", [this.currency()])
                    }
                    ]);
                }
            };
        
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();
        
        var dfdCompanySettings = this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId).done((response) => {
            this.distanceUnit(response.distanceUnitSymbol);
            this.currency(response.currencySymbol);

   
        });
        $.when(dfdCompanySettings).done(() => {
                 
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;

    }

    setupExtend() {
        this.media.extend({ trackChange: true });
        
        // validation
        this.selectedFile.extend({
            fileRequired: true,
            fileExtension: ['xlsx', 'xls'],
            fileSize: 4
        });

        this.validationModel = ko.validatedObservable({
            selectedFile: this.selectedFile
        });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
       // console.log("on unload");
        this.isBusy(false);
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actOK", this.i18n("Common_OK")()));
        actions.push(this.createActionCancel());}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdDownloadTemplate", this.i18n("Common_DownloadTemplate")(), "svg-cmd-download-template"));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actOK") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            if (!this.canSaveImportData()) {
                return;
            }
         //   console.log("this media",this.media());
            this.isBusy(true);
            this.webRequestVehicleMaintenancePlan.importMaintenancePlan({ companyId: WebConfig.userSession.currentCompanyId, media: this.media() }, true).done((response) => {
                this.isBusy(false);
                

                //if (response.messageIdToErrors.M077.length > 0 || 
                //    response.messageIdToErrors.M140.length > 0 ||  
                //    response.messageIdToErrors.M141.length > 0 ||  
                //    response.messageIdToErrors.M142.length > 0 ||  
                //    response.messageIdToErrors.M143.length > 0 ){
                //    console.log("AAA");
                //    this.handleError(response);
                //}
                //else {
                //    console.log("BBB");
                    this.publishMessage("ca-asset-vehicle-maintenance-plan-updated");
                    this.close(true);
                //}
                
                
            }).fail((e) => {
                this.isBusy(false);
                this.handleError(e);
            });

            //console.log("is Ok");
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdDownloadTemplate") {
            this.showMessageBox(null, this.i18n("M117")(),
                BladeDialog.DIALOG_OKCANCEL).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_OK:
                            this.isBusy(true);
                            this.webRequestVehicleMaintenancePlan.downloadImportTemplate(Utility.isiOSDevice() ? Enums.TemplateFileType.Xls : Enums.TemplateFileType.Xlsx).done((response) => {
                                this.isBusy(false);
                                ScreenHelper.downloadFile(response.fileUrl);
                            }).fail((e)=> {
                                this.isBusy(false);
                                this.handleError(e);
                            });
                            break;
                    }
                });
        }
    }

    onRemoveFile() {
    
        this.previewData.removeAll();
        this.previewDataVisible(false);
        this.importValidationError("");
        this.media(null);
    }

    onBeforeUpload() {
        this.onRemoveFile();
        
    }

    onUploadSuccess(data) {
        if (data && data.length > 0) {
            this.isBusy(true);
            var media = data[0];
            this.webRequestVehicleMaintenancePlan.importMaintenancePlan({ companyId: WebConfig.userSession.currentCompanyId, media: media }, false).done((response) => {
                var messageIdToErrors = response.messageIdToErrors;
                var previewData = response.data;
                var listAlert = "";
               
                for(var i = 0;i < previewData.length;i++){
                    if(previewData[i].emailAlert){ 
                        if(listAlert === ""){
                            listAlert += "Email";
                        }else{
                            listAlert += ",Email";
                        }
                    }
                    if(previewData[i].smsAlert){ 
                        if(listAlert === ""|| listAlert == null){
                            listAlert += "SMS";
                        }else{
                            listAlert += ",SMS";
                        }
                       
                    }
                    if(previewData[i].webAlert){   
                        if(listAlert === ""|| listAlert == null){
                            listAlert += "Web";
                        }else{
                            listAlert += ",Web";
                        }
                        
                    }

                    previewData[i].listAlert = listAlert;

                    listAlert =  "";

                }
                
              //  console.log("list ALert",listAlert);
              //  console.log("Data",previewData);

                if (previewData && previewData.length === 0 && Object.keys(messageIdToErrors).length === 0) {
                    this.previewData.replaceAll(previewData);
                    this.previewDataVisible(true);
                    this.clearStatusBar();
                }
                else if (previewData && previewData.length > 0) {
                    //case validation pass
                    this.media(media);
                    // set unique tempId for dataTable
                    var index = 1;
                    ko.utils.arrayForEach(previewData, (item) => {
                        item.tempId = index;
                        index++;
                    });
                    
                    this.previewData.replaceAll(previewData);
                    this.previewDataVisible(true);
                    this.clearStatusBar();
                }
                else if (Object.keys(messageIdToErrors).length > 0) {
                    //case validation error
                    if (messageIdToErrors.M076) {
                        this.importValidationError(ScreenHelper.formatImportValidationErrorMessage("M076", messageIdToErrors.M076));
                    }
                    else if (messageIdToErrors.M077) {
                        this.importValidationError(ScreenHelper.formatImportValidationErrorMessage("M077", messageIdToErrors.M077));
                    }
                    //else if (messageIdToErrors.M140) {
                    //    this.importValidationError(ScreenHelper.formatImportValidationErrorMessage("M140", messageIdToErrors.M140));
                    //}
                    //else if (messageIdToErrors.M141) {
                    //    this.importValidationError(ScreenHelper.formatImportValidationErrorMessage("M141", messageIdToErrors.M141));
                    //}
                    //else if (messageIdToErrors.M142) {
                    //    this.importValidationError(ScreenHelper.formatImportValidationErrorMessage("M142", messageIdToErrors.M142));
                    //}
                    //else if (messageIdToErrors.M143) {
                    //    this.importValidationError(ScreenHelper.formatImportValidationErrorMessage("M143", messageIdToErrors.M143));
                    //}
                    //else if (messageIdToErrors.M161) {
                    //    this.importValidationError(ScreenHelper.formatImportValidationErrorMessage("M161", messageIdToErrors.M161));
                    //}
                    //else if (messageIdToErrors.M162) {
                    //    this.importValidationError(ScreenHelper.formatImportValidationErrorMessage("M162", messageIdToErrors.M162));
                    //}
                    //else if (messageIdToErrors.M163) {
                    //    this.importValidationError(ScreenHelper.formatImportValidationErrorMessage("M163", messageIdToErrors.M163));
                    //}
                }

                //AAA
               //  console.log("Preview Data",this.previewData);
              //  console.log("Service Success");
                this.isBusy(false);  
            }).fail((e) => {
             //   console.log("Service Fail");
                this.isBusy(false);
                this.handleError(e);
            });
        }

    }

    onUploadFail(e) {
      //  console.log("onUploadFail");
        this.isBusy(false);
        this.handleError(e);
    }

    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    
    get webRequestVehicleMaintenancePlan() {
        return WebRequestVehicleMaintenancePlan.getInstance();
    }


    renderIconColumn(data, type, row) {
        return _.isEmpty(data) ? '' : '<img src="' + data + '"/>';
    }
}

export default {
viewModel: ScreenBase.createFactory(MaintenancePlanImport),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import ScreenBase from "../../../../screenbase";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestBusinessUnit from "../../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestEnumResource from "../../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestVehicle from "../../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import WebRequestVehicleMaintenanceType from "../../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenanceType";
import WebRequestAppointment from "../../../../../../app/frameworks/data/apitrackingcore/webRequestAppointment";
import { Enums } from "../../../../../../app/frameworks/constant/apiConstant";

/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class MaintenancePlanSearch extends ScreenBase {

    /**
     * Creates an instance of AssetMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Common_Search")());

        let currentDate = new Date();
        this.maintenanceType = ko.observable();
        this.maintenanceSubtype = ko.observable();
        this.maintenanceStatus = ko.observable();
        this.bu = ko.observable();
        this.includeSubBU = ko.observable(true);
        this.vehicle = ko.observable();
        this.startDate = ko.observable(currentDate);
        this.endDate = ko.observable(currentDate);
        this.buItem = this.ensureNonObservable([]);
        this.maintenanceTypeList = ko.observableArray([]);
        this.maintenanceSubtypeList = ko.observableArray([]);
        this.maintenanceStatusList = ko.observableArray([]);
        this.businessUnitList = ko.observableArray([]);
        this.VehicleList = ko.observableArray([]);
        this.selectedSubBU = this.ensureNonObservable([]);
        this.dateFormat = WebConfig.companySettings.shortDateFormat;
    }

    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    get webRequestVehicleMaintenanceType() {
        return WebRequestVehicleMaintenanceType.getInstance();
    }
    get webRequestAppointment() {
        return WebRequestAppointment.getInstance();
    }

    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    get webRequestVehicle() {
        return WebRequestVehicle.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        var dfdMaintenanceType = null;
        var dfdMaintenanceSubtype = null;
        var dfdMaintenanceStatus = null;
        var dfdBusinessUnit = null;
        var dfdVehicle = null;

        var maintenanceTypeFilter = {
            companyId: WebConfig.userSession.currentCompanyId
        };

        // binding data Maintenance Type
        dfdMaintenanceType = this.webRequestVehicleMaintenanceType.listVehicleMaintenanceType(maintenanceTypeFilter).done((response) => {
            this.maintenanceTypeList(response["items"]);
        });

        this.maintenanceType.subscribe((ddlValue) => {
            var maTypeID = null;
            if (ddlValue != undefined && ddlValue != null && ddlValue != '') {
                maTypeID = ddlValue.id;

                var maintenanceSubTypeFilter = {
                    companyId: WebConfig.userSession.currentCompanyId,
                    maintenanceTypeId: maTypeID
                };

                // binding data Maintenance Subtype
                dfdMaintenanceSubtype = this.webRequestVehicleMaintenanceType.listVehicleMaintenanceSubType(maintenanceSubTypeFilter).done((responseMASubType) => {
                    this.maintenanceSubtypeList(responseMASubType["items"]);
                });

                $.when(dfdMaintenanceSubtype).done(() => {
                    dfdMaintenanceSubtype.resolve();
                }).fail((e) => {
                    dfdMaintenanceSubtype.reject(e);
                });
            } else {
                this.maintenanceSubtypeList([]);
            }
        });

        var maintenanceStatuseFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            types: [Enums.ModelData.EnumResourceType.MaintenanceStatus]
        };

        // binding data Maintenance Status
        dfdMaintenanceStatus = this.webRequestEnumResource.listEnumResource(maintenanceStatuseFilter).done((response) => {
            this.maintenanceStatusList(response["items"]);
        });

        var businessUnitFilter = {
            companyId: WebConfig.userSession.currentCompanyId
        }

        // binding data BU
        dfdBusinessUnit = this.webRequestBusinessUnit.listBusinessUnitSummary(businessUnitFilter).done((response) => {
            this.businessUnitList(response["items"]);


            //New Kob
            var responseItem = response["items"];

            for (var i = 0; i < responseItem.length; i++) {
                if (responseItem[i].parentBusinessUnitId == null) {

                    var groupBu = { id: responseItem[i].id, subId: [] };

                    for (var indSub = 0; indSub < responseItem.length; indSub++) {
                        if (responseItem[indSub].parentBusinessUnitId == groupBu.id) {
                            groupBu.subId.push(responseItem[indSub].id);
                        }
                    }
                    this.buItem.push(groupBu);
                }
            }

        });

        this.bu.subscribe((bu) => {
            if (bu) {
                this.selectedSubBU = [];
                if (this.includeSubBU()) {
                    $.each(this.buItem, (index, item) => {
                        if (item.id == bu) {
                            this.selectedSubBU = item.subId;
                        }

                    });
                }

                let defVehicle = this.webRequestVehicle.listVehicleSummary({
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: [bu],
                    enable: true,
                    includeIds: this.selectedSubBU
                }).done((resultVehicle) => {
                    if (resultVehicle) {
                        this.VehicleList.replaceAll(resultVehicle.items);
                    }

                });
            } else {
                this.VehicleList.replaceAll([]);
            }
        });

        this.includeSubBU.subscribe((isChecked) => {
            if (this.bu() != null) {


                var busUnit = this.bu();

                this.selectedSubBU = [];
                if (isChecked) {
                    $.each(this.buItem, (index, item) => {
                        if (item.id == busUnit) {
                            this.selectedSubBU = item.subId;
                        }

                    });
                }

                var vehicleFilter = {
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: [busUnit],
                    enable: true,
                    includeIds: this.selectedSubBU
                };

                var dfdVehicle = this.webRequestVehicle.listVehicleSummary(vehicleFilter).done((resultVehicle) => {

                    if (resultVehicle) {
                        this.VehicleList.replaceAll(resultVehicle.items);
                    }

                });



                $.when(dfdVehicle).done(() => {
                    dfdVehicle.resolve();
                }).fail((e) => {
                    dfdVehicle.reject(e);
                });
            } else {
                this.VehicleList.replaceAll([]);
            }
        });

    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() { }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSearch", this.i18n("Common_Search")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdClear", this.i18n("Common_Clear")(), "svg-cmd-clear"));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSearch") {
            var formatStartDate = null;
            var formatEndDate = null;
            if (this.startDate() != undefined) {
                formatStartDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.startDate())) + "T00:00:00";
            }

            if (this.endDate() != undefined) {
                formatEndDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.endDate())) + "T00:00:00";
            }

            var model = {
                companyId: WebConfig.userSession.currentCompanyId,
                maintenanceTypeId: this.maintenanceType() ? this.maintenanceType().id : null,
                maintenanceSubTypeId: this.maintenanceSubtype() ? this.maintenanceSubtype().id : null,
                maintenanceStatus: this.maintenanceStatus() ? [this.maintenanceStatus().value] : null,
                businessUnitId: this.bu(),
                vehicleId: this.vehicle() ? this.vehicle().id : null,
                fromDate: formatStartDate,
                toDate: formatEndDate
            }
            this.publishMessage("ca-asset-vehicle-maintenance-plan-search", model);
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdClear":
                let currentDate = new Date();

                this.maintenanceType('');
                this.maintenanceSubtype('');
                this.maintenanceStatus('');
                this.bu('');
                this.vehicle('');
                this.startDate('');
                this.endDate('');
                break;
            default:
                break;
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(MaintenancePlanSearch),
    template: templateMarkup
};
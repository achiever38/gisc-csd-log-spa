﻿import ko from "knockout";
import templateMarkup from "text!./create.html";
import ScreenBase from "../../../../screenbase";
import ScreenHelper from "../../../../screenhelper";
import * as Screen from "../../../../../../app/frameworks/constant/screen";
import WebRequestCompany from "../../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebrequestVehicle from "../../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import WebRequestVehicleMaintenancePlan from "../../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenancePlan";
import WebRequestVehicleMaintenanceType from "../../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenanceType";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import { Enums, EntityAssociation } from "../../../../../../app/frameworks/constant/apiConstant";

/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class MaintenancePlansCreate extends ScreenBase {

    /**
     * Creates an instance of AssetMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("MA_Maintenance_Plans_Create")());
        //var tomorrow = new Date();
        //tomorrow.setDate(tomorrow.getDate() + 1);
        this.allowedDate = ko.observable(new Date());

        this.id = this.ensureNonObservable(params.id, 0);

        //this.ensureNonObservable(params.vehicleId);
        this.maintenanceType = ko.observableArray([]);
        this.maintenanceSubtype = ko.observableArray([]);
        this.maType = ko.observable();
        this.maSubType = ko.observable();


        this.title = ko.observable();
        this.description = ko.observable();
        this.maintenanceDate = ko.observable();
        this.dayRemind = ko.observable();



        this.estimateCost = ko.observable();
        this.enableWebsiteAlert = ko.observable(true);
        this.enableEmailAlert = ko.observable();
        this.enableSmsAlert = ko.observable();
        this.maintenanceStatus = Enums.ModelData.MaintenanceStatus.Pending;
        this.dateFormat = WebConfig.companySettings.shortDateFormat;

        this.currency = ko.observable();


        this.vehicleDetail = ko.observable({
            businessUnitId: null,
            includeSub: true,
            vehicleIds: [],
            subBusinessUnitId: []
        });

        this.selectedSubMenuItem = ko.observable(null);

        this.subscribeMessage("ca-asset-maintenance-management-vehicles-select", (info) => {
            this.vehicleDetail(info);
        });
    }

    /**
  * Get WebRequest specific for Company module in Web API access.
  * @readonly
  */

    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }

    /**
     * Get WebRequest specific for Vehicle Maintenance Plan module in Web API access.
     * @readonly
     */
    get webRequestVehicleMaintenancePlan() {
        return WebRequestVehicleMaintenancePlan.getInstance();
    }


    get webRequestVehicleMaintenanceType() {
        return WebRequestVehicleMaintenanceType.getInstance();
    }



    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }



        var dfd = $.Deferred();


        var dfdMaintenanceType = null;
        var dfdMaintenanceSubtype = null;



        var dfdCompanySettings = null;



        var maintenanceTypeFilter = {
            companyId: WebConfig.userSession.currentCompanyId
        };

        dfdMaintenanceType = this.webRequestVehicleMaintenanceType.listVehicleMaintenanceType(maintenanceTypeFilter).done((response) => {
            this.maintenanceType(response["items"]);
            // this.maType(ScreenHelper.findOptionByProperty(this.maintenanceType, "id"));
        });

        this.maType.subscribe((ddlValue) => {
            if (ddlValue != null) {
                var maTypeID = ddlValue.id;
                var maintenanceSubTypeFilter = {
                    companyId: WebConfig.userSession.currentCompanyId,
                    maintenanceTypeId: maTypeID
                };
                dfdMaintenanceSubtype = this.webRequestVehicleMaintenanceType.listVehicleMaintenanceSubType(maintenanceSubTypeFilter).done((responseMASubType) => {
                    this.maintenanceSubtype(responseMASubType["items"]);
                    this.maSubType(ScreenHelper.findOptionByProperty(this.maintenanceSubtype, "id"));
                });

                $.when(dfdMaintenanceSubtype).done(() => {
                    dfdMaintenanceSubtype.resolve();
                }).fail((e) => {
                    dfdMaintenanceSubtype.reject(e);
                });
            }
        });

        this.maSubType.subscribe((ddlValue) => {
            if (this.maType() != null && this.maSubType() != null) {
                this.title(this.maType().name + " " + this.i18n("Vehicle_MA_Common_Type")() + " " + this.maSubType().name);
            }
        });






        dfdCompanySettings = this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId).done((response) => {

            this.currency(response.currencySymbol);
        });



        //dfdMaintenanceType, dfdMaintenanceSubtype, dfdCompanySettings, dfdVehicleCurrentDistance
        $.when(dfdMaintenanceType, dfdCompanySettings).done(() => {
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }


    setupExtend() {

        this.maType.extend({ trackChange: true });
        this.maSubType.extend({ trackChange: true });


        this.title.extend({ trackChange: true });
        this.description.extend({ trackChange: true });
        this.maintenanceDate.extend({ trackChange: true });
        this.dayRemind.extend({ trackChange: true });

        this.estimateCost.extend({ trackChange: true });
        this.enableWebsiteAlert.extend({ trackChange: true });
        this.enableEmailAlert.extend({ trackChange: true });
        this.enableSmsAlert.extend({ trackChange: true });


        this.maType.extend({ required: true });
        this.maSubType.extend({ required: true });
        this.title.extend({ required: true });

        this.maintenanceDate.extend({
            required: true,
            validation: {
                validator: () => {
                    var isValidate = true;
                    if (this.maintenanceDate() != null && this.maintenanceDate() != "") {
                        var arrSplit = (this.maintenanceDate()).split('T');
                        var arrSplitDates = arrSplit[0].split('-');

                        var maintenanceDate = new Date(arrSplitDates[0], (parseInt(arrSplitDates[1], 10) - 1), parseInt(arrSplitDates[2],10) + 1);
                        //var dateCompare = new Date(arrSplitDates[0], (parseInt(arrSplitDates[1], 10) - 1), arrSplitDates[2]);
                        var dateCompare = new Date();
                        if (this.dayRemind() != undefined && this.dayRemind() != null && this.dayRemind() != "") {
                            dateCompare.setDate(dateCompare.getDate() + parseInt(this.dayRemind().toString(), 10));
                        }

                        if (dateCompare > maintenanceDate) {
                            isValidate = false;
                        }
                    }

                    return isValidate;
                },
                message: this.i18n("M146")()
            }
        });
        this.dayRemind.extend({ min: 0 });


        this.estimateCost.extend({ min: 0 });

        this.vehicleDetail.extend({
            validation: {
                validator: (val) => {
                    return this.vehicleDetail().vehicleIds.length > 0;
                },
                message: this.i18n("M001")()
            }
        });

        var paramsValidation = {
            title: this.title,
            maintenanceDate: this.maintenanceDate,
            dayRemind: this.dayRemind,
            estimateCost: this.estimateCost,
            maintenanceType: this.maType,
            maintenanceSubtype: this.maSubType,
            vehicleDetail: this.vehicleDetail
        };



        this.validationModel = ko.validatedObservable(paramsValidation);
    }


    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() { }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
    }

    onSelectSubMenuItem(index) {
        if (this.journeyCanClose()) {
            this.goToSubPage(index);
        }
        else {
            this.showMessageBox(null, this.i18n("M100")(),
                BladeDialog.DIALOG_OKCANCEL).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_OK:
                            this.goToSubPage(index);
                            break;
                    }
                });
        }
    }

    goToSubPage(index) {
        var page = "";
        var options = {

        };

        switch (index) {
            case 1:
                page = "ca-asset-maintenance-management-ma-plan-create-vehicle";
                options.vehicleDetail = this.vehicleDetail();
                break;
        }
        //console.log(options);
        this.forceNavigate(page, options);
        this.selectedSubMenuItem(index);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());


    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            var info = {
                vehicleMaintenanceType: Enums.ModelData.VehicleMaintenanceType.Plan,//(this.mode == Screen.SCREEN_MODE_CREATE ? 1 : this.maTypeUpdate),//this.maType().id,
                maintenanceSubTypeId: this.maSubType().id,
                companyId: WebConfig.userSession.currentCompanyId,
                vehicleIds: this.vehicleDetail().vehicleIds,
                //vehicleId: this.vehicleId,
                title: this.title(),
                description: this.description(),
                maintenanceDate: this.maintenanceDate(),
                dayRemind: this.dayRemind(),
                estimateCost: this.estimateCost(),
                enableWebsiteAlert: this.enableWebsiteAlert(),
                enableEmailAlert: this.enableEmailAlert(),
                enableSmsAlert: this.enableSmsAlert(),
                maintenanceStatus: this.maintenanceStatus,
                infoStatus: Enums.InfoStatus.Add,
                id: this.id
            };

            this.isBusy(true);
            this.webRequestVehicleMaintenancePlan.createVehicleMaintenancePlan(info, true).done((response) => {
                this.isBusy(false);
                this.publishMessage("ca-asset-vehicle-maintenance-plan-created", response);
                this.close(true);
            }).fail((e) => {
                this.handleError(e);
                this.isBusy(false);
            });



        }


    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {

    }
}

export default {
    viewModel: ScreenBase.createFactory(MaintenancePlansCreate),
    template: templateMarkup
};

﻿import "jquery-ui";
import "jqueryui-timepicker-addon";
import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../../screenbase";
import ScreenHelper from "../../../../screenhelper";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../../app/frameworks/constant/screen";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestVehicleMaintenancePlan from "../../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenancePlan";
import WebRequestVehicle from "../../../../../../app/frameworks/data/apitrackingcore/webRequestVehicle";
import WebRequestAppointment from "../../../../../../app/frameworks/data/apitrackingcore/webRequestAppointment";
import WebRequestBusinessUnit from "../../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import DefaultSorting from "../../../../../../app/frameworks/constant/defaultSorting";
import { Enums, EntityAssociation } from "../../../../../../app/frameworks/constant/apiConstant";

/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class AppointmentManage extends ScreenBase {

    /**
     * Creates an instance of AssetMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.isFromAppointment = ko.observable(params.isFromAppointment == undefined ? true : this.ensureNonObservable(params.isFromAppointment));

        this.id = this.ensureNonObservable(params.id);
        this.mode = this.ensureNonObservable(params.mode);

        //let setTimeNow = ("0" + new Date().getHours()).slice(-2)+":"+("0" + new Date().getMinutes()).slice(-2);
        let setTimeNow = "00:00";
        this.formatSet = "dd/MM/yyyy";
        this.timeStart = ko.observable(setTimeNow);
        this.currentDate = ko.observable(new Date());

        this.fromUpdate = ko.observable(false);
        this.hideRequire = ko.pureComputed(function () {
            return !this.fromUpdate();
        }, this);

        if (this.mode == Screen.SCREEN_MODE_CREATE) {
            this.bladeTitle(this.i18n("Vehicle_MA_Appointment_Manage_Create_Header")());
        }
        else {
            this.bladeTitle(this.i18n("Vehicle_MA_Appointment_Manage_Update_Header")());
            this.isFromAppointment(false);
            this.fromUpdate(true);
        }

        this.bladeSize = BladeSize.Medium;

        this.businessUnitList = ko.observableArray([]);
        this.maintenancePlans = ko.observableArray([]);
        this.vehicles = ko.observableArray([]);

        this.displayDateTime = ko.observable();
        this.selectedDate = ko.observable(this.currentDate());
        this.selectedBU = ko.observable();
        this.includeSubBU = ko.observable(true);
        this.txtDescription = ko.observable();
        this.txtContactPoint = ko.observable();
        this.txtAssignTo = ko.observable();
        this.status = ko.observable(this.i18n('AppointmentStatus_Active')());
        this.selectedVehicleText = ko.observable();
        this.selectedMAText = ko.observable();
        this.selectedMA = ko.observable();
        this.selectedVehicle = ko.observable();

        this.filter = {
            companyId: WebConfig.userSession.currentCompanyId,
            vehicleIds: []
        };

        this.dataFromMA = null;

        if (this.mode == Screen.SCREEN_MODE_CREATE) {
            if (!this.isFromAppointment()) {

                let vId;

                if (params.appointment)
                    vId = params.appointment["vehicleId"];

                if (params.maPlan)
                    vId = params.maPlan["vehicleId"];

                this.webRequestVehicle.getVehicle(vId, this.generateVehicleEnityAssociations()).done((vehicle) => {
                    params.vehicle = vehicle;
                    if (params.vehicle && params.maPlan) {
                        this.dataFromMA = {
                            "maintenancePlanId": params.maPlan["id"],
                            "vehicleId": params.maPlan["vehicleId"],
                            "assignTo": null,
                            "contact": null,
                            "appointmentDate": null,
                            "description": null,
                            "appointmentStatus": null
                        }

                        if (params["vehicle"]["defaultDriverDisplayName"] != undefined &&
                            params["vehicle"]["defaultDriverDisplayName"] != null &&
                            params["vehicle"]["defaultDriverDisplayName"] != "") {
                            this.txtContactPoint(params["vehicle"]["defaultDriverDisplayName"]);
                        }

                        
                        //this.status(params.maPlan["maintenanceStatusDisplayName"]);
                        this.selectedVehicleText(params["vehicle"]["license"]["license"]);
                        this.selectedMAText(params.maPlan["title"]);
                    }
                    else {
                        if (params["vehicle"]["defaultDriverDisplayName"] != undefined &&
                            params["vehicle"]["defaultDriverDisplayName"] != null &&
                            params["vehicle"]["defaultDriverDisplayName"] != "") {
                            this.txtContactPoint(params["vehicle"]["defaultDriverDisplayName"]);
                        }
                        this.dataFromMA = {
                            "maintenancePlanId": params["appointment"]["id"],
                            "vehicleId": params["appointment"]["vehicleId"],
                            "assignTo": null,
                            "contact": null,
                            "appointmentDate": null,
                            "description": null,
                            "appointmentStatus": null
                        }

                        //this.status(params["appointment"]["maintenanceStatusDisplayName"]);
                        this.selectedVehicleText(params["appointment"]["vehicleName"]);
                        this.selectedMAText(params["appointment"]["title"]);
                    }
                });

            }
        }
        else {
            this.dataFromMA = {
                "id": this.id,
                "maintenancePlanId": null,
                "vehicleId": null,
                "assignTo": null,
                "contact": null,
                "appointmentDate": null,
                "description": null,
                "appointmentStatus": null
            }
        }
    }

    setupExtend() {

        this.selectedDate.extend({ trackChange: true });
        this.txtDescription.extend({ trackChange: true });
        this.txtContactPoint.extend({ trackChange: true });
        this.txtAssignTo.extend({ trackChange: true });
        this.selectedMA.extend({ trackChange: true });
        this.selectedVehicle.extend({ trackChange: true });
        this.timeStart.extend({ trackChange: true });

        this.selectedDate.extend({ required: true });
        this.txtContactPoint.extend({ required: true });
        this.txtAssignTo.extend({ required: true });
        this.selectedMA.extend({ required: true });
        this.selectedVehicle.extend({ required: true });
        this.timeStart.extend({ required: true });

        this.selectedDate.extend({ 
            required: true,
            validation: {
                validator: (val) => {
                    let currentDay = new Date();
                    currentDay.setHours(0);
                    currentDay.setMinutes(0);
                    currentDay.setSeconds(0);
                    currentDay.setMilliseconds(0);

                    //console.log($.datepicker.formatDate("yy-mm-dd", new Date(this.selectedDate())) + " " + this.timeStart() + ":00")

                    let newDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.selectedDate())) + " " + this.timeStart() + ":00";

                    //console.log(this.oldAppointment);

                    //return Date.parse(val) >= currentDay 
                    //    && (newDate != this.oldAppointment);

                    return Date.parse(newDate) > new Date();
                },
                message: this.i18n("M181")()
            }
        });

        let objValidate = null;

        if (this.mode == Screen.SCREEN_MODE_CREATE) {
            if (this.isFromAppointment()) {
                objValidate = {
                    selectedDate: this.selectedDate,
                    timeStart: this.timeStart,
                    txtContactPoint: this.txtContactPoint,
                    txtAssignTo: this.txtAssignTo,
                    selectedMA: this.selectedMA,
                    selectedVehicle: this.selectedVehicle
                };
            }
            else {
                objValidate = {
                    selectedDate: this.selectedDate,
                    timeStart: this.timeStart,
                    txtContactPoint: this.txtContactPoint,
                    txtAssignTo: this.txtAssignTo
                };
            }
        }
        else {
            objValidate = {
                txtContactPoint: this.txtContactPoint,
                txtAssignTo: this.txtAssignTo
            };
        }

        this.validationModel = ko.validatedObservable(objValidate);
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        //let defMAType = this.webRequestVehicleMaintenanceType.listVehicleMaintenanceType({ companyId: WebConfig.userSession.currentCompanyId });

        //let dfdVehicleMaintenancePlan = this.webRequestVehicleMaintenancePlan.listVehicleMaintenancePlanSummary(this.filter)

        let dfdBusinessUnit = this.webRequestBusinessUnit.listBusinessUnitSummary({
            companyId: WebConfig.userSession.currentCompanyId
        }).done((response) => {
            this.businessUnitList(response["items"]);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        this.selectedBU.subscribe((bu) => {
            if (bu) {
                let defVehicle = this.webRequestAppointment.listVehicle({
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: [bu],
                    includeIds: [this.includeSubBU() ? 1 : 0]
                }).done((resultVehicle) => {
                    if (resultVehicle) {
                        resultVehicle.forEach((val, ind) => {
                            resultVehicle[ind]["licenseText"] = val["license"]["license"];
                        });
                        this.vehicles.replaceAll(resultVehicle);
                        //this.selectedVehicle(ScreenHelper.findOptionByProperty(this.vehicles, "id"));
                    }

                });
            }
        });

        if (this.mode == Screen.SCREEN_MODE_UPDATE) {

            this.webRequestAppointment.getAppointment(this.id).done((resultAppointment) => {

                //this.selectedDate(resultAppointment["appointmentDate"]);
                this.dataFromMA.appointmentDate = resultAppointment["appointmentDate"];
                this.dataFromMA.maintenancePlanId = resultAppointment["maintenancePlanId"];
                this.dataFromMA.vehicleId = resultAppointment["vehicleId"];

                this.displayDateTime(resultAppointment["formatAppointmentDate"]);

                this.txtContactPoint(resultAppointment["contact"]);
                this.txtAssignTo(resultAppointment["assignTo"]);
                this.txtDescription(resultAppointment["description"]);

                this.status(resultAppointment["appointmentStatusDisplayName"]);
                this.selectedVehicleText(resultAppointment["vehicleName"]);
                this.selectedMAText(resultAppointment["maintenancePlanName"]);

                //this.selectedMA(ScreenHelper.findOptionByProperty(this.maintenancePlans, "id", resultAppointment["maintenancePlanId"]));
                //this.selectedVehicle(ScreenHelper.findOptionByProperty(this.vehicles, "id", resultAppointment["vehicleId"]));

                dfd.resolve();
            }).fail((e) => {
                dfd.reject(e);
            });
        }

        this.selectedVehicle.subscribe((vehicle) => {
            this.txtContactPoint(vehicle["defaultDriverDisplayName"]);
            if (vehicle) {
                this.filter.vehicleId = vehicle.id;
                this.filter.maintenanceStatus = [
                    Enums.ModelData.MaintenanceStatus.Pending,
                    Enums.ModelData.MaintenanceStatus.Overdue,
                    Enums.ModelData.MaintenanceStatus.Inprogress
                ]; 

                this.webRequestVehicleMaintenancePlan.listVehicleMaintenancePlanSummary(this.filter).done((maResult) => {
                    if (maResult) {

                        for (var i = 0; i < maResult.items.length; i++) { 
                            maResult.items[i]["title"] = maResult.items[i]["title"] + " (" +maResult.items[i]["maintenanceStatusDisplayName"]+ ")";
                        }

                        this.maintenancePlans.replaceAll(maResult["items"]);
                    }
                });
            }
            else {
                this.maintenancePlans.replaceAll([]);
            }
        });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() { }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) { }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {

            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);

            var formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.selectedDate())) + " " + this.timeStart() + ":00";

            let model = null;

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE: {

                    if (this.isFromAppointment()) {
                        model = {
                            "maintenancePlanId": this.selectedMA()["id"],
                            "vehicleId": this.selectedVehicle()["id"],
                            "assignTo": this.txtAssignTo(),
                            "contact": this.txtContactPoint(),
                            "appointmentDate": formatSDate,
                            "description": this.txtDescription(),
                            "appointmentStatus": Enums.ModelData.AppointmentStatus.Active
                        }
                    }
                    else {
                        model = this.dataFromMA;

                        model.assignTo = this.txtAssignTo();
                        model.contact = this.txtContactPoint();
                        model.appointmentDate = formatSDate;
                        model.description = this.txtDescription();
                        model.appointmentStatus = Enums.ModelData.AppointmentStatus.Active
                    }

                    this.webRequestAppointment.createAppointment(model, true).done((result) => {
                        this.publishMessage("ca-asset-maintenance-management-appointment-manage-save-completed", result);

                        this.isBusy(false);
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });

                    break;
                }
                case Screen.SCREEN_MODE_UPDATE:

                    model = this.dataFromMA;

                    model.assignTo = this.txtAssignTo();
                    model.contact = this.txtContactPoint();
                    model.description = this.txtDescription();
                    model.appointmentStatus = Enums.ModelData.AppointmentStatus.Active

                    this.webRequestAppointment.updateAppointment(model, true).done((result) => {
                        this.publishMessage("ca-asset-maintenance-management-appointment-manage-save-completed", result);

                        this.isBusy(false);
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });

                    break;
                default:
                    break;
            }

        }

    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) { }

    generateVehicleEnityAssociations() {
        return [EntityAssociation.Vehicle.Images,
        EntityAssociation.Vehicle.Icon,
        EntityAssociation.Vehicle.Trail,
        EntityAssociation.Vehicle.DefaultDriver,
        EntityAssociation.Vehicle.CoDriver1,
        EntityAssociation.Vehicle.CoDriver2,
        EntityAssociation.Vehicle.AlertConfigurations,
        EntityAssociation.Vehicle.Model,
        EntityAssociation.Vehicle.VehicleRegisteredType,
        EntityAssociation.Vehicle.License];
    }

    get webRequestVehicle() {
        return WebRequestVehicle.getInstance();
    }

    get webRequestVehicleMaintenancePlan() {
        return WebRequestVehicleMaintenancePlan.getInstance();
    }

    get webRequestVehicle() {
        return WebRequestVehicle.getInstance();
    }

    get webRequestAppointment() {
        return WebRequestAppointment.getInstance();
    }

    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
}

export default {
    viewModel: ScreenBase.createFactory(AppointmentManage),
    template: templateMarkup
};
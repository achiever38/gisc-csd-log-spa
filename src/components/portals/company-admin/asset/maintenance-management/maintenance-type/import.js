﻿import ko from "knockout";
import templateMarkup from "text!./import.html";
import ScreenBase from "../../../../screenbase";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import WebRequestVehicleMaintenanceType from "../../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenanceType";
import WebRequestCompany from "../../../../../../app/frameworks/data/apicore/webrequestCompany";
import Utility from "../../../../../../app/frameworks/core/utility";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import ScreenHelper from "../../../../screenhelper";
import { Enums } from "../../../../../../app/frameworks/constant/apiConstant";
/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class MaintenanceTypeImport extends ScreenBase {

    /**
     * Creates an instance of AssetMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("MA_Maintenance_Type_Import")());
        this.bladeSize = BladeSize.Medium;

        this.fileName = ko.observable("");
        this.selectedFile = ko.observable(null);
        this.mimeType = [
            Utility.getMIMEType("xls"),
            Utility.getMIMEType("xlsx")
        ].join();
        this.media = ko.observable(null);
        this.previewData = ko.observableArray([]);
        this.previewDataVisible = ko.observable(false);
        this.importValidationError = ko.observable("");
        this.urlUpload = WebConfig.appSettings.uploadUrlTrackingCore;

        this.canSaveImportData = ko.pureComputed(() => {
            return _.isEmpty(this.importValidationError()) && !_.isNil(this.media());
        });

        this.importData = ko.observable();

        // Set custom column title with DataTable
        // This function is called AFTER screen onload, so this.volumeUnit already has value.

    }


    /**
     * Get WebRequest specific for Company module in Web API access.
     * @readonly
     */

    get webRequestVehicleMaintenanceType() {
        return WebRequestVehicleMaintenanceType.getInstance();
    }

    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }


        var dfd = $.Deferred();

        dfd.resolve();
        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() { }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
    }


    setupExtend() {
        this.media.extend({ trackChange: true });

        this.selectedFile.extend({
            fileRequired: true,
            fileExtension: ['xlsx', 'xls'],
            fileSize: 4
        });

        this.validationModel = ko.validatedObservable({
            selectedFile: this.selectedFile
        });
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actOK", this.i18n("Common_OK")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdDownloadTemplate", this.i18n("Common_DownloadTemplate")(), "svg-cmd-download-template"));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);


        if (sender.id === "actOK") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            if (!this.canSaveImportData()) {
                return;
            }

            this.publishMessage("ca-asset-maintenance-management-ma-type-import", this.importData());
            this.close(true);
            //this.isBusy(true);
            //this.webRequestVehicleMaintenanceType.importChecklist({ companyId: WebConfig.userSession.currentCompanyId, media: this.media() }).done(() => {
            //    this.isBusy(false);
            //    this.publishMessage("ca-asset-vehicle-changed");
            //    this.close(true);
            //}).fail((e) => {
            //    this.isBusy(false);
            //    this.handleError(e);
            //});
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdDownloadTemplate") {
            this.showMessageBox(null, this.i18n("M117")(),
                BladeDialog.DIALOG_OKCANCEL).done((button) => {

                    switch (button) {
                        case BladeDialog.BUTTON_OK:
                            this.isBusy(true);
                            this.webRequestVehicleMaintenanceType.downloadChecklistTemplate(Utility.isiOSDevice() ? Enums.TemplateFileType.Xls : Enums.TemplateFileType.Xlsx).done((response) => {
                                this.isBusy(false);
                                ScreenHelper.downloadFile(response.fileUrl);
                            }).fail((e) => {
                                this.isBusy(false);
                                this.handleError(e);
                            });
                            break;
                    }
                });
        }
    }


    onRemoveFile() {
        this.previewData.removeAll();
        this.previewDataVisible(false);
        this.importValidationError("");
        this.media(null);
    }

    onBeforeUpload() {
        //remove current file before upload new file
        this.onRemoveFile();
    }

    onUploadSuccess(data) {
        if (data && data.length > 0) {
            this.isBusy(true);
            var media = data[0];
            this.webRequestVehicleMaintenanceType.importChecklist({ companyId: WebConfig.userSession.currentCompanyId, media: media }).done((response) => {
                this.importData(response);

                var messageIdToErrors = response.messageIdToErrors;
                var previewData = response.data;

                if (previewData && previewData.length === 0 && Object.keys(messageIdToErrors).length === 0) {
                    this.previewData.replaceAll(previewData);
                    this.previewDataVisible(true);
                    this.clearStatusBar();
                }
                else if (previewData && previewData.length > 0) {
                    //case validation pass
                    this.media(media);
                    // set unique tempId for dataTable
                    var index = 1;
                    ko.utils.arrayForEach(previewData, (item) => {
                        item.tempId = index;
                        index++;
                    });

                    this.previewData.replaceAll(previewData);
                    this.previewDataVisible(true);
                    this.clearStatusBar();
                }
                else if (Object.keys(messageIdToErrors).length > 0) {
                    //case validation error
                    if (messageIdToErrors.M076) {
                        this.importValidationError(ScreenHelper.formatImportValidationErrorMessage("M076", messageIdToErrors.M076));

                    }
                    else if (messageIdToErrors.M077) {
                        this.importValidationError(ScreenHelper.formatImportValidationErrorMessage("M077", messageIdToErrors.M077));
                    }
                }

                this.isBusy(false);
            }).fail((e) => {
                this.isBusy(false);
                this.handleError(e);
            });
        }
    }

    onUploadFail(e) {
        this.handleError(e);
    }

    renderIconColumn(data, type, row) {
        return _.isEmpty(data) ? '' : '<img src="' + data + '"/>';
    }

}

export default {
    viewModel: ScreenBase.createFactory(MaintenanceTypeImport),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./subtype-list.html";
import ScreenBase from "../../../../screenbase";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import { Enums, Constants } from "../../../../../../app/frameworks/constant/apiConstant";
import WebRequestVehicleMaintenanceType from "../../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenanceType";

/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class MaintenanceSubtypeTypeList extends ScreenBase {

    /**
     * Creates an instance of AssetMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Vehicle_MA_Common_Maintenance_SubType")());

        this.canView = ko.pureComputed(() => {
            return WebConfig.userSession.hasPermission(Constants.Permission.ViewMaintenance);
        });

        this.canEdit = ko.pureComputed(() => {
            return WebConfig.userSession.hasPermission(Constants.Permission.UpdateMaintenance);
        });

        this.id = this.ensureNonObservable(params.id);

        this.maSubTypes = ko.observableArray([]);
        this.selectedMASubType = ko.observable();
        this.filterText = ko.observable();
        this.recentChangedRowIds = ko.observableArray([]);
        this.order = ko.observable([]);

        this.queryModel = { companyId: WebConfig.userSession.currentCompanyId, maintenanceTypeId: this.id };

        this._selectingRowHandler = (row) => {
            if (row) {
                return this.navigate("ca-asset-maintenance-management-ma-type-view-subtype-view", { id: row.id, maintenanceTypeId: this.id });
            }
            return false;
        };

        this.subscribeMessage("ca-asset-maintenance-management-ma-type-view-subtype-update", (value) => {
            this.webRequestVehicleMaintenanceType.listVehicleMaintenanceSubType(this.queryModel).done((response) => {
                if (response) {
                    response = this.generateName(response);
                    this.maSubTypes.replaceAll(response["items"]);
                };
            });
        });

        this.subscribeMessage("ca-asset-maintenance-management-ma-type-view-subtype-delete", () => {
            this.webRequestVehicleMaintenanceType.listVehicleMaintenanceSubType(this.queryModel).done((response) => {
                if (response) {
                    response = this.generateName(response);
                    this.maSubTypes.replaceAll(response["items"]);
                };
            });
        });

        this.subscribeMessage("ca-asset-maintenance-management-ma-type-view-subtype-create", (info) => {
            this.webRequestVehicleMaintenanceType.listVehicleMaintenanceSubType(this.queryModel).done((response) => {
                if (response) {
                    response = this.generateName(response);
                    this.maSubTypes.replaceAll(response["items"]);
                    this.recentChangedRowIds([info.id]);
                };
            });
        });

        this.subscribeMessage("ca-asset-maintenance-management-ma-type-view-subtype-update", (info) => {
            this.webRequestVehicleMaintenanceType.listVehicleMaintenanceSubType(this.queryModel).done((response) => {
                if (response) {
                    response = this.generateName(response);
                    this.maSubTypes.replaceAll(response["items"]);
                    this.recentChangedRowIds([info.id]);
                };
            });
        });
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        let dfd = $.Deferred();

        this.webRequestVehicleMaintenanceType.listVehicleMaintenanceSubType(this.queryModel).done((response) => {
            if (response) {
                response = this.generateName(response);
                this.maSubTypes.replaceAll(response["items"]);
            }
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() { }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedMASubType(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) { }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add", this.canEdit));
    }

    generateName(response) {
        
        response["items"].forEach((val, ind) => {
            response["items"][ind]["name"] = response["items"][ind]["name"] + " (" + response["items"][ind]["code"] + ")"
        });



        return response;
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdCreate":
                this.navigate("ca-asset-maintenance-management-ma-type-view-subtype-manage", { mode: "create", maintenanceTypeId: this.id });
                break;
            default:
                break;
        }

        this.selectedMASubType(null);
        this.recentChangedRowIds.removeAll();
    }

    get webRequestVehicleMaintenanceType() {
        return WebRequestVehicleMaintenanceType.getInstance();
    }
}

export default {
    viewModel: ScreenBase.createFactory(MaintenanceSubtypeTypeList),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./subtype-view.html";
import ScreenBase from "../../../../screenbase";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import { Enums, Constants } from "../../../../../../app/frameworks/constant/apiConstant";
import WebRequestVehicleMaintenanceType from "../../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenanceType";

/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class MaintenanceSubtypeTypeView extends ScreenBase {

    /**
     * Creates an instance of AssetMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Vehicle_MA_View_Maintenance_SubType")());
        this.bladeSize = BladeSize.Medium;

        this.canView = ko.pureComputed(() => {
            return WebConfig.userSession.hasPermission(Constants.Permission.ViewMaintenance);
        });

        this.canEdit = ko.pureComputed(() => {
            return WebConfig.userSession.hasPermission(Constants.Permission.UpdateMaintenance);
        });

        this.id = this.ensureNonObservable(params.id);
        this.maintenanceTypeId = this.ensureNonObservable(params.maintenanceTypeId);

        this.txtName = ko.observable();
        this.txtCode = ko.observable();
        this.txtDesc = ko.observable();
        this.txtMaType = ko.observable();
        this.checklist = ko.observableArray([]);

    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        let dfd = $.Deferred();

        this.createView(dfd);
        this.subscribeMessage("ca-asset-maintenance-management-ma-type-view-subtype-update", (info) => {
            this.createView(dfd);
        });

        return dfd;
    }

    createView(dfd) {
        this.webRequestVehicleMaintenanceType.getVehicleMaintenanceSubType(this.id).done((response) => {
            if (response) {
                response["checkListSubTypes"].forEach((x) => {
                    x.actionCode = x.actionCode + ":" + x.actionName
                });
                this.checklist.replaceAll(response["checkListSubTypes"]);

                this.txtName(response["name"]);
                this.txtCode(response["code"]);
                this.txtDesc(response["description"]);
                this.txtMaType(response["maintenanceType"]["name"]);
            }
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() { }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) { }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit", this.canEdit));
        commands.push(this.createCommand("cmdObsolate", this.i18n("Vehicle_MA_Obsolete")(), "svg-ic-vmt-obsolate", this.canEdit));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdUpdate":
                this.navigate("ca-asset-maintenance-management-ma-type-view-subtype-manage", { mode: "update", id: this.id, maintenanceTypeId: this.maintenanceTypeId });
                break;
            case "cmdObsolate":
                this.showMessageBox(null, this.i18n("M157")(),
                    BladeDialog.DIALOG_YESNO).done((button) => {
                        switch (button) {
                            case BladeDialog.BUTTON_YES:
                                this.isBusy(true);
                                this.webRequestVehicleMaintenanceType.setObsoleteVehicleMaintenanceSubType(this.id).done(() => {
                                    this.isBusy(false);
                                    this.publishMessage("ca-asset-maintenance-management-ma-type-view-subtype-delete");
                                    this.close(true);
                                }).fail((e) => {
                                    this.isBusy(false);
                                    this.handleError(e);
                                });
                                break;
                        }
                    });
                break;
            default:
                break;
        }
    }

    get webRequestVehicleMaintenanceType() {
        return WebRequestVehicleMaintenanceType.getInstance();
    }
}

export default {
    viewModel: ScreenBase.createFactory(MaintenanceSubtypeTypeView),
    template: templateMarkup
};
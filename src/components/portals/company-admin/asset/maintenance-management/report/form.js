﻿import ko from "knockout";
import templateMarkup from "text!./form.html";
import ScreenBase from "../../../../screenbase";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import "jquery-ui";
import "jqueryui-timepicker-addon";
import WebRequestEnumResource from "../../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestBusinessUnit from "../../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebrequestVehicle from "../../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import WebRequestMaintenanceType from "../../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenanceType";
import WebConfig from "../../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../../app/frameworks/core/utility";
import DefaultSorting from "../../../../../../app/frameworks/constant/defaultSorting";
import {Enums} from "../../../../../../app/frameworks/constant/apiConstant";


/**
 * Demo for the following features:
 * 1. Validation on ViewModel
 * - Required Field
 * - Numeric Field
 * - Email
 * - Custom (RegEx)
 * - Remote Validation
 * 2. Track change on ViewModel
 * 
 * @class FormValidationDemo
 * @extends {ScreenBase}
 */
class MaintenanceReport extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeSize = BladeSize.Small;
        this.minimizeAll(false);
        this.bladeTitle(this.i18n('Report_Filter')());

        this.selectedReportType = ko.observable();
        this.selectedBusinessUnit = ko.observable(null);
        this.isIncludeSubBU = ko.observable(false);
        this.selectedVehicle = ko.observable();
        this.selectedMaintenanceType = ko.observable();
        this.selectedMaintenanceStatus = ko.observable();
        this.selectedAppointmentStatus = ko.observable();
        
        this.reportTypes = ko.observableArray([]);
        this.businessUnits = ko.observableArray([]);
        this.vehicles = ko.observableArray([]);
        this.maintenanceTypes = ko.observableArray([]);
        this.maintenanceStatus = ko.observableArray([]);
        this.appointmentStatus = ko.observableArray([]);

        //datetime
        this.formatSet = "dd/MM/yyyy";
        var addDay = 30;
        var setTimeNow = ("0" + new Date().getHours()).slice(-2)+":"+("0" + new Date().getMinutes()).slice(-2);
        this.start = ko.observable(Utility.addDays(new Date(),-1));
        this.end = ko.observable(new Date());
        this.startTime = ko.observable("00:00");
        this.endTime = ko.observable(setTimeNow);

        this.selectedVehicleBinding = ko.pureComputed(() => {
            var businessUnitIds = this.selectedBusinessUnit();
            if(businessUnitIds == null){
                return '';
            }
            this.vehicles([]);
            //if check include Sub BU
            if(this.isIncludeSubBU()){
                businessUnitIds = Utility.includeSubBU(this.businessUnits(),businessUnitIds);
            }

            
            this.webRequestVehicle.listVehicleSummary({
                companyId: WebConfig.userSession.currentCompanyId,
                businessUnitIds: businessUnitIds,
                sortingColumns: DefaultSorting.Vehicle
            }).done((response) => {
                //set displayName
                response.items.forEach(function(arr) {
                    arr.displayName = arr.license;
                });
                let listSelectEntity = response.items;
                //add displayName "Report_All" and id 0        
                if(Object.keys(listSelectEntity).length > 0 && listSelectEntity[0].id !== 0){
                    listSelectEntity.unshift({
                        displayName: this.i18n('Report_All')(),
                        id: 0
                    });
                }
                
                this.vehicles(listSelectEntity);
            });
        

            return ' ';
        });
        

        this.maintenanceStatusVisible = ko.pureComputed(() => {
            this.isMaintenanceStatusVisible = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.MaintenanceReportType.MaintenancePlan;
            return this.isMaintenanceStatusVisible;
        });

        this.appointmentStatusVisible = ko.pureComputed(() => {
            this.isAppointmentStatusVisible = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.MaintenanceReportType.Appointment;
            return this.isAppointmentStatusVisible;
        });


    }

    setupExtend() {

        // Use ko-validation to set up validation rules
        // See url for more information about rules
        // https://github.com/Knockout-Contrib/Knockout-Validation

        this.selectedReportType.extend({ required: true });
        this.selectedBusinessUnit.extend({ required: true });
        this.selectedVehicle.extend({ required: true });
        this.start.extend({ required: true });
        this.end.extend({ required: true });

        this.validationModel = ko.validatedObservable({
            selectedReportType: this.selectedReportType,
            selectedBusinessUnit: this.selectedBusinessUnit,
            selectedVehicle:this.selectedVehicle,
            end:this.end,
            start:this.start
        });

        this.validationmMaintenanceStatus = ko.validatedObservable({
            selectedMaintenanceStatus: this.selectedMaintenanceStatus
        });

        this.validationAppointmentStatus = ko.validatedObservable({
            selectedAppointmentStatus: this.selectedAppointmentStatus,
        });

    }

    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * Get WebRequest specific for BusinessUnit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }

    get webRequestMaintenanceType() {
        return WebRequestMaintenanceType.getInstance();
    }


    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        
        var dfd = $.Deferred();

        var enumReportTemplateTypeFilter = {
            types: [Enums.ModelData.EnumResourceType.MaintenanceReportType],
            values: [],
            sortingColumns: DefaultSorting.EnumResource
        };

        var businessUnitFilter = {
            companyId: WebConfig.userSession.currentCompanyId, 
            sortingColumns: DefaultSorting.BusinessUnit
        };

        var maintenanceTypeFilter = {
            companyId: WebConfig.userSession.currentCompanyId
        };

        var enumMaintenanceStatusFilter = {
            types: [Enums.ModelData.EnumResourceType.MaintenanceStatus],
            values: [],
            sortingColumns: DefaultSorting.EnumResource
        }

        var enumAppointmentStatusFilter = {
            types: [Enums.ModelData.EnumResourceType.AppointmentStatus],
            values: [],
            sortingColumns: DefaultSorting.EnumResource
        }


        var d1 = this.webRequestEnumResource.listEnumResource(enumReportTemplateTypeFilter);
        var d2 = this.webRequestBusinessUnit.listBusinessUnitSummary(businessUnitFilter);
        var d3 = this.webRequestMaintenanceType.listVehicleMaintenanceType(maintenanceTypeFilter);
        var d4 = this.webRequestEnumResource.listEnumResource(enumMaintenanceStatusFilter);
        var d5 = this.webRequestEnumResource.listEnumResource(enumAppointmentStatusFilter);

        $.when(d1, d2, d3, d4, d5).done((r1, r2, r3, r4, r5) => {

            this.reportTypes(r1.items);
            this.businessUnits(r2.items);
            this.maintenanceTypes(r3.items);
            this.maintenanceStatus(r4.items);
            this.appointmentStatus(r5.items);

            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }

    buildActionBar(actions){
        actions.push(this.createAction("actPreview", this.i18n('Report_Preview')()));
    }

    onActionClick(sender) {
        if(sender.id === "actPreview") {

            var reportNameSpace= null;
            var reportTitle = null;
            var ReportTypeID = null;
            var BusinessUnitID = this.selectedBusinessUnit();
            var MaintenanceTypeID = (this.selectedMaintenanceType() !== undefined)?this.selectedMaintenanceType().id:null;
            var VehicleID = (this.selectedVehicle() !== undefined)?this.selectedVehicle().id:null;
            var MaintenanceStatusID = null;

            var formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.start())) + " " + "00:00:00";
            var formatEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.end())) + " " + "00:00:00";
            var IncludeSubBU = (this.isIncludeSubBU())?1:0;
            

            if(this.isAppointmentStatusVisible){
                formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.start())) + " " + this.startTime() + ":00";
                formatEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.end())) + " " + this.endTime() + ":00";
            }

            //validate MaintenanceStatus 
            if(this.isMaintenanceStatusVisible){
            
                if(this.validationmMaintenanceStatus.isValid()){
                    MaintenanceStatusID = (this.selectedMaintenanceStatus() !== undefined)?this.selectedMaintenanceStatus().value:null;
                }else{
                    this.validationmMaintenanceStatus.errors.showAllMessages();
                    return false;
                }
                
            //validate AppointmentStatus 
            }else if(this.isAppointmentStatusVisible){
            
                if(this.validationAppointmentStatus.isValid()){
                    MaintenanceStatusID = (this.selectedAppointmentStatus() !== undefined)?this.selectedAppointmentStatus().value:null;
                }else{
                    this.validationAppointmentStatus.errors.showAllMessages();
                    return false;
                }
                
            }

            // varidate Model and set param repoprt
            if(this.validationModel.isValid()) {
                Enums.ModelData.MaintenanceReportType.Appointment
                switch (this.selectedReportType() && this.selectedReportType().value) {
                    case Enums.ModelData.MaintenanceReportType.Appointment:
                        reportNameSpace = "Gisc.Csd.Service.Report.Library.Maintenance.AppointmentReport, Gisc.Csd.Service.Report.Library";
                        reportTitle = "MaintenanceReport_Appointment";
                        break;
                    case Enums.ModelData.MaintenanceReportType.MaintenancePlan:
                        reportNameSpace = "Gisc.Csd.Service.Report.Library.Maintenance.MaintenanceReport, Gisc.Csd.Service.Report.Library";
                        reportTitle = "MaintenanceReport_MaintenancePlan";
                        break;
                    default:
                        reportNameSpace = null;
                        break;
                }
                var objReport = this.reports = {
                    report:reportNameSpace, 
                                    parameters: {
                                                    UserID:WebConfig.userSession.id,
                                                    CompanyID:WebConfig.userSession.currentCompanyId,
                                                    ReportTypeID:this.selectedReportType().value,
                                                    BusinessUnitID:BusinessUnitID,
                                                    IncludeSubBusinessUnit:IncludeSubBU,
                                                    VehicleID:VehicleID,
                                                    MaintenanceTypeID:MaintenanceTypeID,
                                                    MaintenanceStatusID:MaintenanceStatusID,
                                                    SDate:formatSDate,
                                                    EDate:formatEDate,
                                                    PrintBy:WebConfig.userSession.fullname,
                                                    Language:WebConfig.userSession.currentUserLanguage,
                                                }
                                };
                this.navigate("cw-report-reportviewer", {reportSource: this.reports, reportName:this.i18n(reportTitle)()});

            } else {
                // This is unreachable code, since we disable save button when VM is not valid
                this.validationModel.errors.showAllMessages();
               
            }
        } 
    }
}

export default {
    viewModel: ScreenBase.createFactory(MaintenanceReport),
    template: templateMarkup
};
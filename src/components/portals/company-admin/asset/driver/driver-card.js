﻿import ko from "knockout";
import templateMarkup from "text!./driver-card.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestDriver from "../../../../../app/frameworks/data/apitrackingcore/webRequestDriver";
import {EnumResourceFilter} from "./infos";
import {Enums, Constants, EntityAssociation} from "../../../../../app/frameworks/constant/apiConstant";
import Utility from "../../../../../app/frameworks/core/utility";
import WebRequestCountry from "../../../../../app/frameworks/data/apicore/webrequestCountry";
import WebRequestProvince from "../../../../../app/frameworks/data/apicore/webrequestProvince";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
class DriverCardScreen extends ScreenBase {
    constructor(params) {
        
        super(params);
        this.mode = this.ensureNonObservable(params.isMode,0);
        if(params.isMode == Screen.SCREEN_MODE_CREATE){
            this.bladeTitle(this.i18n("Assets_CreateDriverCard")());
        }
        else{
            this.bladeTitle(this.i18n("Assets_UpdateDriverCard")());
        }
        this.createBy = (params.data && params.data.createBy == "GatewaySystem" && params.isMode == Screen.SCREEN_MODE_UPDATE ) ? false : true  ; // Check CreateBy 
        this.issueDate = ko.observable(); 
        this.expireDate = ko.observable();
        this.cardTypes = ko.observableArray([]);
        this.licenseTypes = ko.observableArray([]);
        this.issueOfficer = ko.observableArray([]);
        this.countryOptions = ko.observableArray();
        this.provinceOptions = ko.observableArray();
        this.selectedProvince = ko.observable();
        this.selectedCountry = ko.observable();
        this.selectedCardType = ko.observable();
        this.selectedLicenseType = ko.observable();
        this.selectedIssue = ko.observable();
        this.issueRequired = ko.observable(false);
        this.cardId = ko.observable( params.data ? params.data.cardId : null );
        this.idForUpdate = params.isMode == Screen.SCREEN_MODE_UPDATE? params.data.id : null ; //id check itself
        this.idCheck = params.data ? params.data.id : null ;  // Check id for itself
        this.firstName = ko.observable( params.isMode == Screen.SCREEN_MODE_UPDATE && params.data ? params.data.firstName : null );
        this.lastName = ko.observable( params.isMode == Screen.SCREEN_MODE_UPDATE && params.data ? params.data.lastName : null);
        this.gender = params.data ? params.data.gender : null;
        this.dataOnDt = params.dataOnDt ? params.dataOnDt : null ;
        this.licenseNo = ko.observable( (params.data) ? params.data.licenseNo : null ); //LicenseType + Gender + CardID + IssueOfficer
        this.dateFormat = ko.observable("dd/MM/yyyy");
        this.licenseTypesId = ko.observable( (params.data) ? params.data.licenseType : null );
        this.issueId = (params.data) ? params.data.provinceDltId : null ;
        this.cardTypeId = (params.data) ? params.data.cardType : "";
        this.provinceId = (params.data) ? params.data.provinceId : null ;
        this.countryId = (params.data) ? params.data.countryId : null ;
        
        this.id =  params.isMode == Screen.SCREEN_MODE_CREATE ? 1 : 0 ;
        this.issueDate( (params.data) ? params.data.issueDate : new Date());
        this.expireDate((params.data) ? params.data.expireDate : new Date());

        var arrlicenseNo = [] ;
        arrlicenseNo[1] = this.gender; // gender is index[1] of arrlicenseNo
        arrlicenseNo[2] = this.cardId();
        
           
        if(this.dataOnDt && this.dataOnDt.length > 0){ //add id for driver card select last id  then +1  is present id
            if(this.idForUpdate == this.idCheck){ // if id itself not +1  is id == id itself
                this.id = this.idForUpdate ; 
            }
            else{ // if not id itself is +1 
                var tmp = params.dataOnDt.map(function(o) {
                    return o.id;
                });
           
                var maxValue = Math.max.apply(Math, tmp); // find max value or last id 
                this.id = parseInt(maxValue + 1) ;
            }
           
        }

        this.selectedLicenseType.subscribe((val)=>{
            
            if(val){
                arrlicenseNo[0] = val.value;
                this.issueRequired(true);
                if(this.isCheckforLicenseNo()){
                    this.licenseNo(arrlicenseNo.join('')); // join array
                }
               
            }
            else if(typeof(val == 'undefined')){
                this.issueRequired(false);
                this.licenseNo(null);
            }
        });
        
        
        this.cardId.subscribe((val)=>{
            
            if(val){
                arrlicenseNo[2] = val; // gender is index[2] of arrlicenseNo
                if(this.isCheckforLicenseNo()){
                    this.licenseNo(arrlicenseNo.join('')); // join array
                }
               
            }
            else if(typeof(val == 'undefined')){
                this.licenseNo(null);  // join arrlicenseNo
            }
        });

            this.selectedIssue.subscribe((val)=> {
            
                if(val){
                    arrlicenseNo[3] = val.code;
                    if(this.isCheckforLicenseNo()){
                        this.licenseNo(arrlicenseNo.join('')); // join array
                    }
            }
                else if(typeof(val == 'undefined')){
                    
                this.licenseNo(null); 
            }
        });
       
    }
    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    get webRequestDriver() {
        return WebRequestDriver.getInstance();
    }

    get webRequestCountry() {
        return WebRequestCountry.getInstance();
    }

    get webRequestProvince() {
        return WebRequestProvince.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;
        
        var dfd = $.Deferred();
        var d0 = $.Deferred();
        var enumResourceFilter = new EnumResourceFilter();

        var a1 = this.webRequestEnumResource.listEnumResource(enumResourceFilter);
        $.when(a1).done((r1) => {
            
            var enumResources = r1["items"];
            //Driver Type 
            var driverCardTypes = _.filter(enumResources, (item) => {
                return item.type === Enums.ModelData.EnumResourceType.DriverCardType;
            });
            Utility.applyFormattedPropertyToCollection(driverCardTypes, "displayName", "{0} ({1})", "displayName", "value");
            //License Type
            var driverLicenseTypes = _.filter(enumResources, (item) => {
                return item.type === Enums.ModelData.EnumResourceType.DriverLicenseType;
            });
            Utility.applyFormattedPropertyToCollection(driverLicenseTypes, "displayName", "{0} ({1})", "displayName", "value");

            this.cardTypes(driverCardTypes);
            this.licenseTypes(driverLicenseTypes);
            
            this.selectedCardType(ScreenHelper.findOptionByProperty(this.cardTypes , "value",this.cardTypeId));
            this.selectedLicenseType(ScreenHelper.findOptionByProperty(this.licenseTypes , "value",this.licenseTypesId()));

            dfd.resolve();
        }).fail((e)=>{
            dfd.reject(e);
        });
        
        switch(this.mode){
            case Screen.SCREEN_MODE_CREATE :
                this.webRequestCountry.listCountry({ sortingColumns: DefaultSorting.Country }).done((response) => {
                    var countries = response["items"];
                    this.countryOptions(countries);
                    
                    d0.resolve();
                }).fail((e) => {
                    d0.reject(e);
                    this.handleError(e);
                }).always(() => {

                });

            case Screen.SCREEN_MODE_UPDATE :
                var d1 = this.webRequestCountry.listCountry({ sortingColumns: DefaultSorting.Country });
                var d2 = this.countryId ? this.webRequestProvince.listProvince({countryIds: [this.countryId] , sortingColumns: DefaultSorting.Province}) : null ;
                var d3 = this.provinceId ? this.webRequestDriver.listProvinceDlt({ProvinceIds:[this.provinceId]}) : null ;

                $.when(d1,d2,d3).done((r1,r2,r3)=>{
                    
                    if(r1){
                        var countries = r1["items"];
                        this.countryOptions(countries);

                        if(this.countryId){
                            
                            this.selectedCountry(ScreenHelper.findOptionByProperty(this.countryOptions, "id", this.countryId));
                        }
                       
                    }
                    if(r2){
                        var province = r2["items"];
                        this.provinceOptions(province);
                        this.selectedProvince(ScreenHelper.findOptionByProperty(this.provinceOptions, "id", this.provinceId));

                    }
                    if(r3){
                        this.issueOfficer(r3);
                        this.selectedIssue(ScreenHelper.findOptionByProperty(this.issueOfficer, "id",this.issueId));
                    }
                    d0.resolve();
                }).fail((e)=>{
                    d0.reject
                    this.handleError(e);
                });
                
        }
        $.when(d0).done(() => {
            
            this._changeCountrySubscribe = this.selectedCountry.subscribe((country) => {
                
                if (country) {
                    this.webRequestProvince.listProvince({
                        countryIds: [country.id],
                        sortingColumns: DefaultSorting.Province
                    }).done((response) => {
                        var provinces = response["items"];
                        this.provinceOptions.replaceAll(provinces);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {

                    });
                } else {
                    this.provinceOptions.replaceAll([]);
                }
            });

            this._changeProvinceSubscribe = this.selectedProvince.subscribe((province) => {
                if (province) {
                    this.webRequestDriver.listProvinceDlt({
                        ProvinceIds: [province.id]
                    }).done((response) => {
                        this.issueOfficer.replaceAll(response);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {

                    });
                } else {
                    this.issueOfficer.replaceAll([]);
                }
            });
       
        });
        return dfd ;

    }
    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        var self = this;

        self.selectedCardType.extend({
            trackChange : true
        });

        self.selectedCountry.extend({
            trackChange : true
        });

        self.selectedProvince.extend({
            trackChange : true
        });

        self.selectedIssue.extend({
            trackChange : true
        });

        self.selectedLicenseType.extend({
            trackChange : true
        });

        self.issueDate.extend({
            trackChange : true
        });

        self.expireDate.extend({
            trackChange : true
        });

        self.selectedCardType.extend({
            required : true 
        });

        self.cardId.extend({
            required: {           
                onlyIf: function () { 
                    return true; 
                }
            },
            serverValidate: {
                params: "cardId",
                message: this.i18n("M010")()
            }
        });

        self.selectedIssue.extend({
            required: {           
                onlyIf: function () { 
                    return self.issueRequired(); 
                }
            }
        });

        self.validationModel = ko.validatedObservable({
            cardType : self.selectedCardType,
            cardId : self.cardId,
            issueOfficer : self.selectedIssue
        });

    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {

    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_OK")()));
        actions.push(this.createAction("actCancel", this.i18n("Common_Cancel")(), "cancel"));
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * Generate fleet service model from View Model
     * 
     * @returns fleet service model which is ready for ajax request 
     */
    generateModel() {
        var model = {
            
            cardType:(this.selectedCardType())?this.selectedCardType().value : null ,
            cardId:this.cardId() ? this.cardId() : null ,
            licenseType : this.selectedLicenseType() ? this.selectedLicenseType().value : null , 
            provinceDltId : this.selectedIssue() ? this.selectedIssue().id : null ,
            licenseNo : this.licenseNo(),
            issueDate : this.issueDate() ? this.issueDate() : null ,
            expireDate : this.expireDate() ? this.expireDate() : null,
            gender : this.gender,
            id : this.id,
            provinceId : (this.selectedProvince()) ? this.selectedProvince().id : null  ,
            countryId : (this.selectedCountry()) ? this.selectedCountry().id : null,
            infoStatus : Enums.InfoStatus.Add
        };
        return model;
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if(sender.id === "actSave"){

            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            
            else if(this.dataOnDt && this.dataOnDt.length > 0){ // if have dataAll is values 
                var match = _.find(this.dataOnDt , (data)=>{ //check exist license
                    if(data.licenseNo){
                        return data.licenseNo == this.licenseNo() ;
                    }
                    return data.cardId == this.cardId() ;     // if License is null then check with cardId
                });

                   if(this.idForUpdate == this.idCheck){
                        let matchLicense = _.find(this.dataOnDt , (data)=>{  // find licenseNo with id at Update 
                            return data.id == this.idForUpdate
                        });

                        if( match && (matchLicense.licenseNo != match.licenseNo)) { // check license itself (if LicenseNo not equal isn't LicenseNo itself )
                            this.showMessageBox(this.i18n("M010")());
                            return ;
                        }
                        this.dataOnDt.forEach((v)=>{ // Update value  
                            if(v.id == this.idForUpdate){
                                v.cardType = (this.selectedCardType())?this.selectedCardType().value : null ,
                                v.cardId = this.cardId() ? this.cardId() : null ,
                                v.licenseType =  this.selectedLicenseType() ? this.selectedLicenseType().value : null , 
                                v.provinceDltId =  this.selectedIssue() ? this.selectedIssue().id : null ,
                                v.licenseNo =  this.licenseNo(),
                                v.issueDate = this.issueDate() ? this.issueDate() : null ,
                                v.expireDate =  this.expireDate() ? this.expireDate() : null,
                                v.gender =  this.gender,
                                v.provinceId  =  (this.selectedProvince()) ? this.selectedProvince().id : null  ,
                                v.countryId  =  (this.selectedCountry()) ? this.selectedCountry().id : null,
                                v.id = this.id ,
                                v.infoStatus =  (v.infoStatus == Enums.InfoStatus.Add) ?  Enums.InfoStatus.Add : Enums.InfoStatus.Update 
                            }
                        });
                        this.publishMessage("ca-driverCard-addItem",{ dataUpdate : this.dataOnDt , idUpdate : this.idForUpdate}); // sent data to replaceAll in manage.js
                        this.close(true);
                    }
                    else if(match){
                        this.showMessageBox(this.i18n("M010")());
                        return ;
                    }
                    else{
                        this.publishMessage("ca-driverCard-addItem",this.generateModel()); // add item from model
                        this.close(true);
                    }
  
            }
            else{
                this.publishMessage("ca-driverCard-addItem",this.generateModel());
                this.close(true);
            }

        }
        
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
    
    isCheckforLicenseNo(){
        if(this.cardId() && this.selectedLicenseType() && this.selectedIssue() ){
            return true;
        }
        return false
    }

}

export default {
viewModel: ScreenBase.createFactory(DriverCardScreen),
    template: templateMarkup
};
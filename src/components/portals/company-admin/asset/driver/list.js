﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestDriver from "../../../../../app/frameworks/data/apitrackingcore/webRequestDriver";
import { Enums, Constants} from "../../../../../app/frameworks/constant/apiConstant";
import {DriverFilter} from "./infos";
import Utility from "../../../../../app/frameworks/core/utility";

class DriverListScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Common_Drivers")());
        this.bladeSize = BladeSize.Medium;
        this.pageFiltering = ko.observable(false);
        this.bladeIsMaximized = true;
        this.searchFilter = new DriverFilter();
        this.searchFilterDefault = new DriverFilter(); 

        this.drivers = ko.observableArray([]);
        this.apiDataSource = ko.observableArray([]);
        this.filterText = ko.observable('');
        this.selectedDriver = ko.observable(null);
        this.recentChangedRowIds = ko.observableArray([]);
        this.order = ko.observable([[ 1, "asc" ]]);
        this.pageSizeOptions = ko.observableArray([10, 20, 50, 100]);
        this.formatDate = (data) => {
            return moment(data.updateDate).format('dd/MM/YYYY');
        }
        this._selectingRowHandler = (driver) => {
            if(driver) {
                return this.navigate("ca-asset-driver-view", {
                    driverId: driver.id
                });
            }
            return false;
        };

        this.subscribeMessage("ca-asset-driver-changed", (driverId) => {
            this.apiDataSource({
                read: this.webRequestDriver.listDriverSummaryGrid(this.searchFilter)
            });
            //this.webRequestDriver.listDriverSummary(this.searchFilter).done((r) => {
            //    this.drivers.replaceAll(r.items);
            //    if(driverId){
            //        this.recentChangedRowIds.replaceAll([driverId]);
            //    }else{
            //        this.recentChangedRowIds.replaceAll([]);
            //    }
            //    this.isBusy(false);
            //}).fail((e) => {
            //    this.handleError(e);
            //    this.isBusy(false);
            //});
        });

        this.subscribeMessage("ca-asset-driver-deleted", () => {
            this.apiDataSource({
                read: this.webRequestDriver.listDriverSummaryGrid(this.searchFilter)
            });
            //this.webRequestDriver.listDriverSummary(this.searchFilter).done((r) => {
            //    this.drivers.replaceAll(r.items);
            //    this.recentChangedRowIds.replaceAll([]);
            //    this.isBusy(false);
            //}).fail((e) => {
            //    this.handleError(e);
            //    this.isBusy(false);
            //});
        });

        this.subscribeMessage("ca-asset-driver-search-filter-changed", (searchFilter) => {
            this.searchFilter = searchFilter;
            
            // apply active command state if searchFilter does not match default search
            this.pageFiltering(!_.isEqual(this.searchFilter, this.searchFilterDefault));

            this.apiDataSource({
                read: this.webRequestDriver.listDriverSummaryGrid(this.searchFilter)
            });
            //this.webRequestDriver.listDriverSummary(this.searchFilter).done((r) => {
            //    this.drivers.replaceAll(r.items);
            //    this.recentChangedRowIds.replaceAll([]);
            //    this.isBusy(false);
            //}).fail((e) => {
            //    this.handleError(e);
            //    this.isBusy(false);
            //});
        });

        if (params.businessUnitId) {
            this.searchFilter = new DriverFilter(params.businessUnitId);
            this.pageFiltering(true);
        }
    }
    /**
     * Get WebRequest specific for Driver module in Web API access.
     * @readonly
     */
    get webRequestDriver() {
        return WebRequestDriver.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();

        this.apiDataSource({
            read: this.webRequestDriver.listDriverSummaryGrid(this.searchFilter)
        });

        dfd.resolve();
        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.CreateDriver)){
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
        commands.push(this.createCommand("cmdSearch", this.i18n("Common_Search")(), "svg-cmd-search", true, true, this.pageFiltering));
        if(WebConfig.userSession.hasPermission(Constants.Permission.CreateDriver) && WebConfig.userSession.hasPermission(Constants.Permission.UpdateDriver)){
            commands.push(this.createCommand("cmdImport", this.i18n("Common_Import")(), "svg-cmd-import"));
        }
        if(WebConfig.userSession.hasPermission(Constants.Permission.UpdateDriver)){
            commands.push(this.createCommand("cmdImportPictures", this.i18n("Common_ImportPicture")(), "svg-cmd-import-picture"));
        }
        commands.push(this.createCommand("cmdExport", this.i18n("Common_Export")(), "svg-cmd-export"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdCreate":
                this.navigate("ca-asset-driver-manage", { mode: Screen.SCREEN_MODE_CREATE });
                break;
            case "cmdSearch":
                this.navigate("ca-asset-driver-search", { searchFilter: this.searchFilter });
                break;
            case "cmdImport":
                this.navigate("ca-asset-driver-import");
                break;
            case "cmdImportPictures":
                this.navigate("ca-asset-driver-import-picture");
                break;
            case "cmdExport":
                this.showMessageBox(null, this.i18n("M128")(), BladeDialog.DIALOG_YESNO).done((button)=> {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            var filter = $.extend(true, {}, this.searchFilter);
                            filter.templateFileType = Utility.isiOSDevice() ? Enums.TemplateFileType.Xls : Enums.TemplateFileType.Xlsx;

                            filter.sortingColumns = this.generateSortingColumns();
 
                            this.webRequestDriver.exportDriver(filter).done((response) => {
                                this.isBusy(false);
                                ScreenHelper.downloadFile(response.fileUrl);
                            }).fail((e) => {
                                this.isBusy(false);
                                this.handleError(e);
                            });
                            break;
                    }
                });
                break;
        }
        
        this.selectedDriver(null);
        this.recentChangedRowIds([]);
    }

    /**
     * Generate SortingColumns array for export driver filter.
     * @returns
     */
    generateSortingColumns(){
        
        var sortingColumns = [{},{}];

        sortingColumns[0].direction = this.order()[0][1] === "asc" ? 1 : 2 ;

         switch (this.order()[0][0]) {
            case 1:
                 sortingColumns[0].column = Enums.SortingColumnName.EmployeeId;
                break;
        
            case 2:
                 sortingColumns[0].column = Enums.SortingColumnName.FirstName;
                break;
            case 3:
                 sortingColumns[0].column = Enums.SortingColumnName.LastName;
                break;
        
            case 4:
                 sortingColumns[0].column = Enums.SortingColumnName.BusinessUnitName;
                break;        
        }

        sortingColumns[1].column = Enums.SortingColumnName.Id;
        sortingColumns[1].direction = sortingColumns[0].direction;

        return sortingColumns;
    }
    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedDriver(null);
    }
}

export default {
    viewModel: ScreenBase.createFactory(DriverListScreen),
    template: templateMarkup
};
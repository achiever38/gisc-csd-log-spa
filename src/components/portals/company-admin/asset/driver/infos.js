import ko from "knockout";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import DefaultSoring from "../../../../../app/frameworks/constant/defaultSorting";
import {Enums,EntityAssociation} from "../../../../../app/frameworks/constant/apiConstant";

class DriverFilter {
    constructor (businessUnitIds = null, isEnable = null) {
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.sortingColumns = DefaultSoring.Driver;
        this.isEnable = isEnable;

        if(businessUnitIds){
            this.businessUnitIds = [businessUnitIds];
        }else{
            this.businessUnitIds = [];
        }
    }
}
export { DriverFilter };

class BusinessUnitFilter {
    constructor (isEnable = null) {
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.sortingColumns = DefaultSoring.BusinessUnit;
        this.isEnable = isEnable;
    }
}
export { BusinessUnitFilter };

class EnumResourceFilter {
    constructor () {
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.types = [Enums.ModelData.EnumResourceType.DriverCardType, Enums.ModelData.EnumResourceType.Gender, Enums.ModelData.EnumResourceType.DriverLicenseType];
        this.sortingColumns = DefaultSoring.EnumResource;
    }
}
export { EnumResourceFilter };
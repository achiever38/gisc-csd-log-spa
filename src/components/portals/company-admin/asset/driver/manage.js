﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestDriver from "../../../../../app/frameworks/data/apitrackingcore/webRequestDriver";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import {BusinessUnitFilter, EnumResourceFilter} from "./infos";
import {Enums, Constants, EntityAssociation} from "../../../../../app/frameworks/constant/apiConstant";
import FileUploadModel from "../../../../../components/controls/gisc-ui/fileupload/fileuploadModel";
import Utility from "../../../../../app/frameworks/core/utility";

import WebRequestAssetTrainer from "../../../../../app/frameworks/data/apitrackingcore/webRequestAssetTrainer"

class DriverManageScreen extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.mode = this.ensureNonObservable(params.mode);
        this.companyId = WebConfig.userSession.currentCompanyId;
        
        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
               this.bladeTitle(this.i18n("Assets_CreateDriver")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("Assets_UpdateDriver")());
                break;
        }

        this.driverId = this.ensureNonObservable(params.driverId, -1);

        
        this.genders = ko.observableArray([]);
        this.businessUnits = ko.observableArray([]);

        this.picture = ko.observable(null);
        this.pictureFileName = ko.observable('');
        this.pictureSelectedFile = ko.observable(null);
        this.pictureUrl = ko.pureComputed(() => {
            if (this.picture() && this.picture().infoStatus !== Enums.InfoStatus.Delete) {
                return this.picture().fileUrl;
            } else {
                return this.resolveUrl("~/images/upload-img-placeholder-100x100.jpg");
            }
        });
        this.pictureRemoveIconVisible = ko.pureComputed(() => {
            return this.picture() && this.picture().infoStatus !== Enums.InfoStatus.Delete;
        });
        this.pictureMimeTypes = [
            Utility.getMIMEType("jpg"),
            Utility.getMIMEType("png")
        ].join();

        this.employeeId = ko.observable('');
        this.cardId = ko.observable('');
        
        this.selectedGender = ko.observable();
        this.title = ko.observable('');
        this.firstName = ko.observable('');
        this.lastName = ko.observable('');
        this.selectedBusinessUnit = ko.observable(null);
        this.enable = ko.observable();
        this.phone = ko.observable('');
        this.other = ko.observable('');
        this.otherVisible = ko.observable(false);
        this.minRestTime = ko.observable('');
        this.nationalId = ko.observable();
        this.desc = ko.observable();
        this.orderDriver = ko.observable([[ 1, "asc" ]]);
        this.driverCard = ko.observableArray([]);
        this.recentChangedRowIds  = ko.observableArray();
        this.trainerName = ko.observable();
        this.ddTrainer = ko.observableArray([]);
        this.enableOptions = ScreenHelper.createYesNoObservableArrayWithDisplayValue();
      
        this.subscribeMessage("ca-driverCard-addItem",(data)=>{
            if(data.dataUpdate){
                data.dataUpdate.forEach(function(c){
                    c.DisplaylicenseNo = (c.licenseNo) ? c.licenseNo : c.cardId 
                });

                this.driverCard.replaceAll(data.dataUpdate);
                this.recentChangedRowIds.replaceAll([data.idUpdate]);
            }
            else{
                data.DisplaylicenseNo = (data.licenseNo) ? data.licenseNo : data.cardId;
                this.driverCard.push(data);
                this.recentChangedRowIds.replaceAll([data.id]);
            }
            
        });

        this._selectingRowHandler = (driver) => {
            if(driver) {
                driver.gender = this.selectedGender().value ; 
                return this.navigate("ca-asset-driver-card", {
                    data : driver,
                    dataOnDt : this.driverCard(),
                    isMode : 'update'
                });
            }
            return false;
        };

        this.enableGender = ko.pureComputed(()=>{
            if(this.driverCard().length > 0 ){
                return false;
            }
            else{
                return true;
            }
        });
    }
    
    get webRequestAssetTrainer() {
        return WebRequestAssetTrainer.getInstance();
    }
    /**
     * Get WebRequest specific for Driver module in Web API access.
     * @readonly
     */
    get webRequestDriver() {
        return WebRequestDriver.getInstance();
    }
    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();
        var enumResourceFilter = new EnumResourceFilter();
        var businessUnitFilter = new BusinessUnitFilter();
        
        var a1 = (this.mode === Screen.SCREEN_MODE_UPDATE) ? this.webRequestDriver.getDriver(this.driverId, [EntityAssociation.Driver.Image]) : null;
        var a2 = this.webRequestEnumResource.listEnumResource(enumResourceFilter);
        var a3 = this.webRequestBusinessUnit.listBusinessUnitSummary(businessUnitFilter);
        var a4 = (this.mode === Screen.SCREEN_MODE_UPDATE) ? this.webRequestDriver.getDriver(this.driverId,["Driver.DriverCard"]) : null;
        var a5 = this.webRequestAssetTrainer.listTrainerSummary();

        $.when(a1, a2, a3,a4, a5).done((r1, r2, r3,r4, r5) => {
            var driver = r1;
            var driverCardInfoList = r4 ; 
            var enumResources = r2["items"];
            this._originalDriverCard = [] ;
            var genders = _.filter(enumResources, (item) => {
                return item.type === Enums.ModelData.EnumResourceType.Gender;
            });
            Utility.applyFormattedPropertyToCollection(genders, "displayName", "{0} ({1})", "displayName", "value")

            this.ddTrainer(this.formatName(r5["items"]))

            this.genders = ko.observableArray(genders);

            var businessUnits = r3["items"];
            Utility.applyFormattedPropertyToCollection(businessUnits, "displayName", "{0} ({1})", "name", "fullCode");
            this.businessUnits(businessUnits);

            if(driverCardInfoList){
                driverCardInfoList.driverCardInfos.forEach(function(a){
                    a.DisplaylicenseNo = (a.licenseNo) ? a.licenseNo : a.cardId
                });
                this.driverCard(driverCardInfoList.driverCardInfos);
                this._originalDriverCard = _.cloneDeep(driverCardInfoList.driverCardInfos);
            }
            if (driver) {
                if (driver.image) {
                    this.picture(driver.image);
                    this.pictureFileName(driver.image.fileName);
                }

                this.employeeId(driver.employeeId);
                this.selectedGender(ScreenHelper.findOptionByProperty(this.genders, "value", driver.gender));
                this.title(driver.title);
                this.firstName(driver.firstName);
                this.lastName(driver.lastName);
                this.selectedBusinessUnit(driver.businessUnitId.toString());
                this.enable(ScreenHelper.findOptionByProperty(this.enableOptions, "value", driver.enable));
                this.phone(driver.phone);
                this.minRestTime(driver.minRestTime);
                this.nationalId(driver.citizenId);
                this.desc(driver.description);
               
                this.trainerName(ScreenHelper.findOptionByProperty(this.ddTrainer, "id", driver.trainerId));
            }
            dfd.resolve();  
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    formatName(response) {
        var data = [];
        for (let i = 0; i < response.length; i++) {
            var dataItem = response[i];
            dataItem.name = dataItem.firstName + " " + dataItem.lastName;
            data.push(dataItem);
        }
        return data;
    }
    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        var self = this;

        self.pictureSelectedFile.extend({
            fileExtension: ['jpg', 'jpeg', 'png'],
            fileSize: 4
        });
        
        self.picture.extend({
            trackChange: true
        });
        self.employeeId.extend({
            trackChange: true
        });
        
        self.selectedGender.extend({
            trackChange: true
        });
        self.title.extend({
            trackChange: true
        });
        self.firstName.extend({
            trackChange: true
        });
        self.lastName.extend({
            trackChange: true
        });
        self.selectedBusinessUnit.extend({
            trackChange: true
        });
        this.enable.extend({
            trackChange: true
        });
        self.phone.extend({
            trackChange: true
        });
        
        self.minRestTime.extend({
            trackChange: true
        });
        //self.driverCard.extend({
        //    trackArrayChange: {
        //        uniqueFromPropertyName: ""
        //    }
        //});
        // Manual setup validation rules
        self.employeeId.extend({
            required: true,
            serverValidate: {
                params: "EmployeeId",
                message: this.i18n("M010")()
            }
        });
        
        self.nationalId.extend({
            serverValidate: {
                params: "CitizenId",
                message: this.i18n("M010")()
            }
        });
        self.selectedGender.extend({
            required: true
        });
        self.firstName.extend({
            required: true
        });
        self.lastName.extend({
            required: true
        });
        self.selectedBusinessUnit.extend({
            required: true
        });
        this.enable.extend({
            required: true
        });
       
        self.validationModel = ko.validatedObservable({
            pictureSelectedFile: self.pictureSelectedFile,
            employeeId: self.employeeId,
            selectedGender: self.selectedGender,
            firstName: self.firstName,
            lastName: self.lastName,
            selectedBusinessUnit: self.selectedBusinessUnit,            
            enable: this.enable,
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        if((this.mode === Screen.SCREEN_MODE_CREATE && WebConfig.userSession.hasPermission(Constants.Permission.CreateDriver))
            || (this.mode === Screen.SCREEN_MODE_UPDATE && WebConfig.userSession.hasPermission(Constants.Permission.UpdateDriver))) 
        {
             actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        }
        actions.push(this.createAction("actCancel", this.i18n("Common_Cancel")(), "cancel"));
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * Generate fleet service model from View Model
     * 
     * @returns fleet service model which is ready for ajax request 
     */
    generateModel() {

        var test  = [];
        var model = {
            id: this.driverId,
            companyId: this.companyId,
            image: this.picture(),
            employeeId: this.employeeId(),
            gender: this.selectedGender() ? this.selectedGender().value : null,
            title: this.title(),
            firstName: this.firstName(),
            lastName: this.lastName(),
            businessUnitId: this.selectedBusinessUnit(),
            enable: this.enable().value,
            phone: this.phone(),
            minRestTime: this.minRestTime(),
            citizenId : this.nationalId(),
            description : this.desc(),
            trainerId: this.trainerName() ? this.trainerName().id : null
        };
        model.driverCardInfos = this.driverCard();
        if(this.mode === Screen.SCREEN_MODE_UPDATE ) {

            this.driverCard().forEach((v)=>{
                test.push(v);
            });

            this._originalDriverCard.forEach((o)=>{
                let findItem = $.grep(this.driverCard() , (n,m) => {
                    return o.id == n.id ;
                });
                if(findItem.length == 0){
                    o.infoStatus = Enums.InfoStatus.Delete;
                    test.push(o);
                }
            });
            
            model.driverCardInfos = test;
        }
        return model;
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);
            var model = this.generateModel();
            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestDriver.createDriver(model).done((response) => {
                        this.publishMessage("ca-asset-driver-changed", response.id);
                        this.isBusy(false);
                        this.close(true);
                    }).fail((e)=> {
                        this.isBusy(false);

                        this.handleError(e);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.webRequestDriver.updateDriver(model).done((response) => {
                        this.publishMessage("ca-asset-driver-changed");
                        this.isBusy(false);
                        this.close(true);
                    }).fail((e)=> {
                        this.isBusy(false);

                        this.handleError(e);
                    });
                    break;
            }
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
    /**
     * 
     * Hook on fileupload before upload
     * @param {any} e
     */
    onBeforeUpload(isUploadFileValid) {
        if(!isUploadFileValid){
            //Show Placeholder
            if(this.picture() != null)
            {
                if (this.picture().infoStatus === Enums.InfoStatus.Add) {
                    this.picture(null);
                } else {
                    var picture = this.picture();
                    picture.infoStatus = Enums.InfoStatus.Delete;
                    this.picture(picture);
                }
            }
        }
    }
    /**
     * Hook on fileuploadsuccess
     * @param {any} data
     */
    onUploadSuccess(data) {
        if(data && data.length){
            var picture = data[0];
            var currentPicture = this.picture();
            if (currentPicture) {
                switch (currentPicture.infoStatus) {
                    case Enums.InfoStatus.Add:
                        picture.infoStatus = Enums.InfoStatus.Add;
                        break;
                    case Enums.InfoStatus.Original:
                    case Enums.InfoStatus.Update:
                    case Enums.InfoStatus.Delete:
                        picture.infoStatus = Enums.InfoStatus.Update;
                        picture.id = currentPicture.id;
                        break;
                }
            } else {
                picture.infoStatus = Enums.InfoStatus.Add;
            }

            this.picture(picture);
        }
    }
    /**
     * 
     * Hook on fileuploadfail
     * @param {any} e
     */
    onUploadFail(e) {
       this.pictureFileName('');
       this.handleError(e);
    }
    /**
     * 
     * Hook on Remove File
     * @param {any} e
     * 
     */
    onRemoveFile(e) {
        if(this.picture() != null)
        {
            if (this.picture().infoStatus === Enums.InfoStatus.Add) {
                this.picture(null);
            } else {
                this.picture().infoStatus = Enums.InfoStatus.Delete;

                var picture = this.picture();
                picture.infoStatus = Enums.InfoStatus.Delete;
                this.picture(picture);
            }
        }
        this.pictureSelectedFile(null);
        this.pictureFileName('');
    }
    addDriverCard(){

        if (!this.validationModel.isValid()) {
            this.validationModel.errors.showAllMessages();
            return;
        }

        this.navigate('ca-asset-driver-card',{
            data : this.generateModel(),
            dataOnDt : this.driverCard(),
            isMode: 'create'
        });
    }
    }

export default {
        viewModel: ScreenBase.createFactory(DriverManageScreen),
        template: templateMarkup
    };
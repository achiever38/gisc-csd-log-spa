﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenbase";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestDriver from "../../../../../app/frameworks/data/apitrackingcore/webRequestDriver";
import {Constants, EntityAssociation, Enums} from "../../../../../app/frameworks/constant/apiConstant";

class DriverViewScreen extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeTitle(this.i18n("Assets_ViewDriver")());

        this.driverId = this.ensureNonObservable(params.driverId, -1);
        this.driver = null;
        this.employeeId = ko.observable('');
        this.nationalId = ko.observable();
        this.pictureUrl = ko.observable('');
        this.pictureFileName = ko.observable('');        
        this.gender = ko.observable('');
        this.title = ko.observable('');
        this.firstName = ko.observable('');
        this.lastName = ko.observable('');
        this.businessUnit = ko.observable('');
        this.phone = ko.observable('');
        this.minRestTime = ko.observable('');
        this.driverCard = ko.observableArray();
        this.orderDriver = ko.observable([[ 1, "asc" ]]);
        this.userAllowCreate = ko.observable(false);
        this.userName = ko.observable('');
        this.enable = ko.observable("");

        this.formatCreateDate = ko.observable('');
        this.createBy = ko.observable('');
        this.formatUpdateDate = ko.observable('');
        this.updateBy = ko.observable('');
        this.desc = ko.observable();
        this.trainerName = ko.observable();
        this.subscribeMessage("ca-asset-driver-changed", () => {
            this.isBusy(true);
            this.webRequestDriver.getDriver(this.driverId, [
                    EntityAssociation.Driver.Image,
                    EntityAssociation.Driver.BusinessUnit,
                    EntityAssociation.Driver.User,
                    "Driver.DriverCard"
                ]
            ).done((driver)=> {
                this.setupModel(driver);

                driver.driverCardInfos.forEach(function(a){
                    a.DisplaylicenseNo = (a.licenseNo) ? a.licenseNo : a.cardId 
                }); // loop for check if license == null display  cardId instead   

                this.driverCard(driver.driverCardInfos);
                this.isBusy(false);
            }).fail((e)=> {
                this.handleError(e);
                this.isBusy(false);
            });
        });
    }
    /**
     * Get WebRequest specific for Driver module in Web API access.
     * @readonly
     */
    get webRequestDriver() {
        return WebRequestDriver.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();
        this.webRequestDriver.getDriver(this.driverId, [
                EntityAssociation.Driver.Image,
                EntityAssociation.Driver.BusinessUnit,
                EntityAssociation.Driver.User,
                "Driver.DriverCard"
            ]
        ).done((driver)=> {
            this.setupModel(driver);

            driver.driverCardInfos.forEach(function(a){
                a.DisplaylicenseNo = (a.licenseNo) ? a.licenseNo : a.cardId 
            });// loop for check if license == null display  cardId instead

            this.driverCard(driver.driverCardInfos);
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.UpdateDriver)) {
            commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));
        }
        if(WebConfig.userSession.hasPermission(Constants.Permission.CreateUser_Company)) {
            commands.push(this.createCommand("cmdCreateUser", this.i18n("Common_CreateUser")(), "svg-cmd-add", this.userAllowCreate));
        }
        if(WebConfig.userSession.hasPermission(Constants.Permission.DeleteDriver)) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdUpdate":
                this.navigate("ca-asset-driver-manage", { mode: Screen.SCREEN_MODE_UPDATE, driverId: this.driverId });
                break;
            case "cmdCreateUser":
                this.navigate("ca-user-manage", { mode: Screen.SCREEN_MODE_CREATE, driver: this.driver });
                break;
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M028")(),
                    BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);

                            this.webRequestDriver.deleteDriver(this.driverId).done(() => {
                                this.isBusy(false);

                                this.publishMessage("ca-asset-driver-deleted");
                                this.close(true);
                            }).fail((e)=> {
                                this.handleError(e);
                                this.isBusy(false);
                            });
                            break;
                    }
                });
                break;
        }
    }
    /**
     * Setup Driver Model
     *
     */
    setupModel(driver) {
        if(driver){
            this.driver = driver;
            if (driver.image) {
                this.pictureUrl(driver.image.fileUrl);
                this.pictureFileName(driver.image.fileName);
            }else{
                this.pictureUrl(this.resolveUrl("~/images/upload-img-placeholder-100x100.jpg"));
                this.pictureFileName('');
            }

            this.employeeId(driver.employeeId);
            
            this.gender(driver.genderDisplayName);
            this.title(driver.title);
            this.firstName(driver.firstName);
            this.lastName(driver.lastName);
            this.enable(driver.formatEnable);
            this.businessUnit(driver.businessUnitName);
            this.phone(driver.phone);
            this.nationalId(driver.citizenId);
            this.minRestTime(driver.formatMinRestTime);
            this.desc(driver.description);
            this.trainerName(driver.trainerName);
            if(driver.userId){
                 this.userAllowCreate(false);
                 this.userName(driver.userName);
            }else{
                this.userAllowCreate(true);
                this.userName('');
            }

            this.formatCreateDate(driver.formatCreateDate);
            this.createBy(driver.createBy);
            this.formatUpdateDate(driver.formatUpdateDate);
            this.updateBy(driver.updateBy);
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(DriverViewScreen),
    template: templateMarkup
};
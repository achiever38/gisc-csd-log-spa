/**
 * State of UserListFilter, used for interact with User List screen
 * 
 * @public
 * @class UserListFilterInfo
 */
class UserFilterInfo {
    
    /**
     * Creates an instance of UserListFilterState.
     */
    constructor() {
        this.reset();
    }

    /**
     * Check if the filter is active
     * @public
     * @returns true if there is at least one property set, false if all are not set.
     */
    isActive() {
        var isActive = false;
        if((this.userGroupId) || (this.businessUnitIds) || (this.isUserEnable !== null)) {
            isActive = true;
        }
        return isActive;
    }

    /**
     * Reset all properties to its inactive state
     */
    reset() {
        this.userGroupId = null;
        this.businessUnitIds = null;
        this.isUserEnable = null;
    }
}

export {UserFilterInfo};
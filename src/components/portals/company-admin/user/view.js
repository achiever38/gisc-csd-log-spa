﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import ScreenBase from "../../screenbase";
import ScreenHelper from "../../screenhelper";
import WebRequestUser from "../../../../app/frameworks/data/apicore/webRequestUser";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import {EntityAssociation, Constants, Enums} from "../../../../app/frameworks/constant/apiConstant";
import * as Screen from "../../../../app/frameworks/constant/screen";
import * as BladeDialog from "../../../../app/frameworks/constant/bladeDialog";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";


/**
 * Company Admin: View User
 * 
 * @class UserViewScreen
 * @extends {ScreenBase}
 */
class UserViewScreen extends ScreenBase {

    /**
     * Creates an instance of UserViewScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("User_ViewUser")());
        this.bladeSize = BladeSize.Medium;
        this.userId = this.ensureNonObservable(params.userId, -1);

        this.firstName = ko.observable();
        this.lastName = ko.observable();
        this.username = ko.observable();
        this.email = ko.observable();
        this.phone = ko.observable();
        this.userEnabled = ko.observable();
        this.businessUnit = ko.observable();
        this.expDate = ko.observable();
        this.ipPermit = ko.observable();
        this.userGroups = ko.observableArray();
        this.orderUserGroups = ko.observable([[ 1, "asc" ]]);
        this.accessibleBusinessUnits = ko.observableArray();
        this.orderAccessibleBusinessUnits = ko.observable([[ 1, "asc" ]]);
        this.authType = ko.observable();
        this.authTypeName = ko.observable();
        this.isPrimaryCompanyAdmin = ko.observable(false);
        this.isDeliveryMan = ko.observable(false);
        this.pmsType = ko.observable();
        this.pictureFileName = ko.observable('');        
        this.pictureUrl = ko.observable('');

        this.enablePmsType = WebConfig.userSession.permissionType == Enums.ModelData.PermissionType.Public;

        // Store command button for dynamic reference.
        this.updateCmd = this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit");
        this.resetPasswordCmd = this.createCommand("cmdResetPassword", this.i18n("Common_ResetPassword")(), "svg-cmd-reset-password"); 
        this.deleteCmd = this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete");

        this.updateCmd.visible = ko.pureComputed(() => {
            return this.canUpdate() && (this.enablePmsType || ScreenHelper.isCurrentUser(this.userId));
        });
        this.resetPasswordCmd.visible =  ko.pureComputed(() => {
            return this.canResetPassword() && (this.enablePmsType || ScreenHelper.isCurrentUser(this.userId));
        });
        this.deleteCmd.visible =  ko.pureComputed(() => {
            return this.canDelete() && this.enablePmsType;
        });

        // Observe change about user in this Journey
        this.subscribeMessage("ca-user-changed", (message) => {
            if((message.mode === "delete") || _.isNil(message.userId)) return;

            // Refresh entire datasource
            this.isBusy(true);

            this.webRequestUser.getUser(message.userId, [
                EntityAssociation.User.BusinessUnit,
                EntityAssociation.User.Groups, 
                EntityAssociation.User.AccessibleBusinessUnits,
                EntityAssociation.User.IsDeliveryMan,
                EntityAssociation.User.Translation,
                EntityAssociation.User.Image
            ]).done((user) => {
                if (user) {
                    this.user = user;
                    if (user.image) {
                        this.pictureUrl(user.image.fileUrl);
                        this.pictureFileName(user.image.fileName);
                    }else{
                        this.pictureUrl(this.resolveUrl("~/images/upload-img-placeholder-100x100.jpg"));
                        this.pictureFileName('');
                    }

                    this.username(user.username);
                    this.firstName(user.firstName);
                    this.lastName(user.lastName);
                    this.email(user.email);
                    this.phone(user.phone);
                    this.userEnabled(user.formatEnable);
                    this.businessUnit(user.businessUnit);
                    this.userGroups(user.groups);
                    this.accessibleBusinessUnits(user.accessibleBusinessUnits);
                    this.authType(user.authenticationType);
                    this.authTypeName(user.authenticationTypeDisplayName);
                    this.expDate(user.formatExpirationDate);
                    this.ipPermit(user.ipPermit);
                    this.isPrimaryCompanyAdmin(user.isPrimaryCompanyAdmin);
                    this.isDeliveryMan(user.isDeliveryMan);
                    this.pmsType(user.permissionTypeDisplayName);
                }
            }).fail((e)=> {
                this.handleError(e);
            }).always(()=>{
                this.isBusy(false);
            });
        });

        this.subscribeMessage("ca-user-reset-password-success", () => {
            this.displaySuccess(this.i18n("M104"));
        });
        this.subscribeMessage("ca-user-reset-password-fail", (e) => {
            this.handleError(e);
        });
    }

    /**
     * Get WebRequest specific for User module in Web API access.
     * @readonly
     */
    get webRequestUser() {
        return WebRequestUser.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();

        this.webRequestUser.getUser(this.userId, [
            EntityAssociation.User.BusinessUnit,
            EntityAssociation.User.Groups, 
            EntityAssociation.User.AccessibleBusinessUnits,
            EntityAssociation.User.IsDeliveryMan,
            EntityAssociation.User.Translation,
            EntityAssociation.User.Image
        ]).done((user) => {
            if (user) {
                if (user.image) {
                    this.pictureUrl(user.image.fileUrl);
                    this.pictureFileName(user.image.fileName);
                }else{
                    this.pictureUrl(this.resolveUrl("~/images/upload-img-placeholder-100x100.jpg"));
                    this.pictureFileName('');
                }

                this.username(user.username);
                this.firstName(user.firstName);
                this.lastName(user.lastName);
                this.email(user.email);
                this.phone(user.phone);
                this.userEnabled(user.formatEnable);
                this.businessUnit(user.businessUnit);
                this.userGroups(user.groups);
                this.accessibleBusinessUnits(user.accessibleBusinessUnits);
                this.authType(user.authenticationType);
                this.authTypeName(user.authenticationTypeDisplayName);
                this.expDate(user.formatExpirationDate);
                this.ipPermit(user.ipPermit);
                this.isPrimaryCompanyAdmin(user.isPrimaryCompanyAdmin);
                this.isDeliveryMan(user.isDeliveryMan);
                this.pmsType(user.permissionTypeDisplayName);
            }
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.updateCmd);
        commands.push(this.resetPasswordCmd);
        commands.push(this.deleteCmd);
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdUpdate":
                this.navigate("ca-user-manage", { 
                    mode: Screen.SCREEN_MODE_UPDATE, 
                    userId: this.userId 
                });
                break;
            case "cmdResetPassword":
                if(this.isDeliveryMan()) {
                    this.navigate("ca-user-reset-password", { 
                        userId: this.userId 
                    });
                } else {
                    this.showMessageBox(null, this.i18n("M026")(), BladeDialog.DIALOG_YESNO).done((button) => {
                        switch (button) {
                            case BladeDialog.BUTTON_YES:
                                this.isBusy(true);

                                this.webRequestUser.resetUserPassword(this.userId).done(() => {
                                    this.displaySuccess(this.i18n("M104"));
                                }).fail((e)=> {
                                    this.handleError(e);
                                }).always(()=>{
                                    this.isBusy(false);
                                });
                                break;
                        }
                    });
                }
                break;
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M103")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.webRequestUser.deleteUser(this.userId).done(() => {
                                this.publishMessage("ca-user-changed", {
                                    mode: "delete",
                                    userId: this.userId
                                });
                                this.close(true);
                            }).fail((e)=> {
                                this.handleError(e);
                            });
                            break;
                    }
                });
                break;
        }
    }

    /**
     * Check if update action is possible, see formula below
     * 
     * Facts:
     * A. {Current User} has UpdateUser_Company permission.
     * B. {Current User} is BO user.
     * C. {Target User} is not primary admin.
     * D. {Current User} is {Target User}
     * 
     * Formula = A && (B || C || D)
     * 
     * @private
     * @returns true if criteria met, false otherwise.
     */
    canUpdate() {
        var hasPermission = WebConfig.userSession.hasPermission(Constants.Permission.UpdateUser_Company);
        var currentUserIsBOAdmin = (WebConfig.userSession.userType === Enums.UserType.BackOffice);

        return hasPermission && (currentUserIsBOAdmin || 
            !this.isPrimaryCompanyAdmin() || 
            ScreenHelper.isCurrentUser(this.userId));
    }

    /**
     * Check if reset password action is possible, see formula below
     * 
     * Facts:
     * A. {Current User} has UpdateUser_Company permission.
     * B. {Current User} is BO user.
     * C. {Target User} is not primary admin.
     * D. {Current User} is {Target User}
     * E. {Target User} not has authentication type == ActiveDirectory
     * 
     * Formula = A && E && (B || C || D)
     * 
     * @private
     * @returns true if criteria met, false otherwise.
     */
    canResetPassword() {
        var hasPermission = WebConfig.userSession.hasPermission(Constants.Permission.UpdateUser_Company);
        var currentUserIsBOAdmin = (WebConfig.userSession.userType === Enums.UserType.BackOffice);
        var targetUserAuthTypeIsAD = (this.authType() === Enums.AuthenticationType.ActiveDirectory || this.authType() === Enums.AuthenticationType.PEAIdm);

        return hasPermission && !targetUserAuthTypeIsAD && 
            (currentUserIsBOAdmin || 
            !this.isPrimaryCompanyAdmin() || 
            ScreenHelper.isCurrentUser(this.userId));
    }

    /**
     * Check if delete action is possible, see formula below
     * 
     * Facts:
     * A. {Target User} is not primary admin
     * B. {Current User} has DeleteUser_Company permission and {Current User} is not {Target User}
     * 
     * @private
     * @returns true if criteria met, false otherwise.
     */
    canDelete() {
        var hasPermission = WebConfig.userSession.hasPermission(Constants.Permission.DeleteUser_Company);

        if (this.isPrimaryCompanyAdmin()) {
            return false;
        }
        return hasPermission && !ScreenHelper.isCurrentUser(this.userId);
    }
}

export default {
    viewModel: ScreenBase.createFactory(UserViewScreen),
    template: templateMarkup
};
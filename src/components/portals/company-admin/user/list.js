﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../screenbase";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import WebRequestUser from "../../../../app/frameworks/data/apicore/webRequestUser";
import {Constants, Enums} from "../../../../app/frameworks/constant/apiConstant";
import {UserFilterInfo} from "./infos";
import DefaultSorting from "../../../../app/frameworks/constant/defaultSorting";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../app/frameworks/constant/screen";

/**
 * Company Admin: User Management > List 
 * 
 * @class UserListScreen
 * @extends {ScreenBase}
 */
class UserListScreen extends ScreenBase {

    /**
     * Creates an instance of UserListScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Common_Users")());
        this.bladeSize = BladeSize.Large;

        this.users = ko.observableArray();
        this.filterText = ko.observable();
        this.selectedUser = ko.observable();
        this.filterState = ko.observable(new UserFilterInfo());
        this.recentChangedRowIds = ko.observableArray();
        this.order = ko.observable([[0, "asc"]]);
        this.businessUnitId = _.size(params) > 0 ? params.businessUnitId : -1;


        // Determine if selecting action is legal, return false to cancel selecting event.
        this._selectingRowHandler = (user) => {
            if (user) {
                return this.navigate("ca-user-view", {
                    userId: user.id
                });
            }
            return false;
        };

        // Observe change about user in this Journey
        this.subscribeMessage("ca-user-changed", (message) => {
            // If it is NOT create mode, then clear recentChangedRowIds
            var recentChangedRowIds = (message.mode === Screen.SCREEN_MODE_CREATE) ? [message.userId] : [];
            this.refreshDataSource(this.filterState(), recentChangedRowIds);
        });

        // Observe change about filter
        this.subscribeMessage("ca-user-filter-changed", (newFilter) => {
            this.filterState(newFilter);
        });

        // Observe change in this filterState
        this._filterStateSubscribe = this.filterState.subscribe((filter) => {
            this.refreshDataSource(filter);
        });

        // Custom column rendering
        this.renderUserGroups = (data, type, row) => {
            if (data) {
                return data.join(", ");
            }
            return "";
        };

        // Search Filter State
        this.isFilterActive = ko.pureComputed(function () {
            return this.filterState().isActive();
        }, this);

        // change searchFilter after subscribe to make sure active state also applied
        if (params.businessUnitId) {
            this.filterState().businessUnitIds = params.businessUnitId;
        }
    }

    /**
     * Refresh Datasource with given search filter and recent changed row ids as optional
     * 
     * @param {any} searchFilter
     * @param {any} [recentChangedRowIds=[]]
     */
    refreshDataSource(searchFilter, recentChangedRowIds = []) {
        this.isBusy(true);

        var userFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            userType: Enums.UserType.Company,
            sortingColumns: DefaultSorting.User,
            permissionType: searchFilter.permissionType
        };

        //if(searchFilter.isActive()) {
            if(searchFilter.userGroupId) {
                userFilter.groupIds = [searchFilter.userGroupId];
            }
            if(searchFilter.businessUnitIds) {
                userFilter.businessUnitIds = searchFilter.businessUnitIds;
            }
            if(searchFilter.isUserEnable !== null) {
                userFilter.enable = searchFilter.isUserEnable;
            }
        //}

        this.webRequestUser.listUserSummary(userFilter).done((response) => {
            this.users.replaceAll(response.items);
            this.recentChangedRowIds.replaceAll(recentChangedRowIds);
        }).fail((e) => {
            this.handleError(e);
        }).always(() => {
            this.isBusy(false);
        });
    }

    /**
     * Get WebRequest specific for User module in Web API access.
     * @readonly
     */
    get webRequestUser() {
        return WebRequestUser.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;
        
        var dfd = $.Deferred();
        var userFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            userType: Enums.UserType.Company
        };

        //if (this.filterState().isActive()) {
            if (this.filterState().userGroupId) {
                userFilter.groupIds = [this.filterState().userGroupId];
            }
            if (this.filterState().businessUnitIds) {
                userFilter.businessUnitIds = [this.filterState().businessUnitIds];
            }
            if (this.filterState().isUserEnable !== null) {
                userFilter.enable = this.filterState().isUserEnable;
            }
        //}
        //else if (this.filterState().businessUnitId) {
        //    userFilter.businessUnitIds = [this.filterState().businessUnitId];
        //}

        this.webRequestUser.listUserSummary(userFilter).done((response) => {
            var users = response["items"];
            this.users(users);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedUser(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.CreateUser_Company)) {
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
        commands.push(this.createCommand("cmdFilter", this.i18n("Common_Search")(),
            "svg-cmd-search", true, true, this.isFilterActive));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdCreate") {
            this.navigate("ca-user-manage", {
                mode: Screen.SCREEN_MODE_CREATE
            });   
        } else if (sender.id === "cmdFilter") {
            this.navigate("ca-user-search", {
                filterState: this.filterState()
            });
        }
        this.selectedUser(null);
        this.recentChangedRowIds.removeAll();
    }
}

export default {
    viewModel: ScreenBase.createFactory(UserListScreen),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import ScreenBase from "../../screenbase";
import ScreenHelper from "../../screenhelper";
import Utility from "../../../../app/frameworks/core/utility";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import WebRequestGroup from "../../../../app/frameworks/data/apicore/webRequestGroup";
import WebRequestBusinessUnit from "../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import {UserFilterInfo} from "./infos";
import { EntityAssociation, Constants, Enums } from "../../../../app/frameworks/constant/apiConstant";
import WebRequestEnumResource from "../../../../app/frameworks/data/apicore/webRequestEnumResource";


/**
 * Company Admin: List Filter User
 * 
 * @class UserListFilterScreen
 * @extends {ScreenBase}
 */
class UserListFilterScreen extends ScreenBase {

    /**
     * Creates an instance of UserListFilterScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Common_Search")());

        this.filterState = Utility.extractParam(params.filterState, null);
        this.enableDS = ScreenHelper.createYesNoObservableArray();

        this.userGroupDS = ko.observableArray([]);
        this.businessUnitDS = ko.observableArray([]);
        this.includeSubBusinessUnitEnable = ko.pureComputed(() => {return !_.isEmpty(this.businessUnit());});
        this.includeSubBusinessUnit = ko.observable(false);
        this.userGroup = ko.observable();
        this.businessUnit = ko.observable(null);
        this.userEnabled = ko.observable();

        this.pmsTypeOptions = ko.observableArray();
        this.pmsType = ko.observable();
        this.isPermissionTypePublic = WebConfig.userSession.permissionType == Enums.ModelData.PermissionType.Public;
    }

    /**
     * Get WebRequest for Group module in Web API access.
     * @readonly
     */
    get webRequestGroup() {
        return WebRequestGroup.getInstance();
    }

    /**
     * Get WebRequest for Group module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        var dfd = $.Deferred();

        var d1 = this.webRequestGroup.listGroupSummary({
            companyId: WebConfig.userSession.currentCompanyId,
            isSystem: false,
            isCompanyGroup: true
        });
        var d2 = this.webRequestBusinessUnit.listBusinessUnitSummary({ 
            companyId: WebConfig.userSession.currentCompanyId, 
            includeAssociationNames: [
               EntityAssociation.BusinessUnit.ChildBusinessUnits
            ]
        });

        var d3 = this.webRequestEnumResource.listEnumResource({
            types: [Enums.ModelData.EnumResourceType.PermissionType]
        });

        $.when(d1, d2,d3).done((r1, r2,r3) => {
            this.userGroupDS = ScreenHelper.createGenericOptionsObservableArray(r1["items"], "name", "id");
            this.businessUnitDS(r2["items"]);

            //let tmpPmsType = _.filter(r3["items"], (x) => {
            //    if (WebConfig.userSession.permissionType == Enums.ModelData.PermissionType.Private) {
            //        return x.value != Enums.ModelData.PoiType.Public;
            //    }
            //    return x;
            //});
            this.pmsTypeOptions(r3["items"]);

            // Preset value if there is filterState send from opener
            if(this.filterState && this.filterState.isActive()) {
                if(this.filterState.userGroupId) {
                    this.userGroup(ScreenHelper.findOptionByProperty(this.userGroupDS, "value", this.filterState.userGroupId));
                }
                if(this.filterState.businessUnitId) {
                    this.businessUnit(this.filterState.businessUnitId.toString());
                }
                if(this.filterState.isUserEnable !== null) {
                    this.userEnabled(ScreenHelper.findOptionByProperty(this.enableDS, "value", this.filterState.isUserEnable));
                }
            }
            if (WebConfig.userSession.permissionType == Enums.ModelData.PermissionType.Private)
            {
                this.pmsType(ScreenHelper.findOptionByProperty(this.pmsTypeOptions, "value", Enums.ModelData.PermissionType.Private));
            }


            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actApply", this.i18n("Common_Search")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdClear", this.i18n("Common_Clear")(), "svg-cmd-clear"));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if(sender.id === "actApply") {
            if(this.shouldCreateStateObject()) {
                this.filterState = new UserFilterInfo();
            }

            this.filterState.userGroupId = this.userGroup() ? this.userGroup().value : null;
            this.filterState.businessUnitIds = null;
            this.filterState.isUserEnable = this.userEnabled() ? this.userEnabled().value : null;
            this.filterState.permissionType = this.pmsType() ? this.pmsType().value : null;

            // Default selection is single.
            if(this.includeSubBusinessUnitEnable()){
                var selectedBusinessUnitId = parseInt(this.businessUnit());

                // If user included sub children then return all business unit ids.
                if(this.includeSubBusinessUnit()){
                    this.filterState.businessUnitIds = ScreenHelper.findBusinessUnitsById(this.businessUnitDS(), selectedBusinessUnitId);
                }
                else {
                    // Single selection for business unit.
                    this.filterState.businessUnitIds = [selectedBusinessUnitId];
                }
            }

            this.publishMessage("ca-user-filter-changed", this.filterState);
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdClear") {
            this.userGroup(null);
            this.businessUnit(null);
            this.userEnabled(null);
            this.filterState.reset();

            if (WebConfig.userSession.permissionType != Enums.ModelData.PermissionType.Private) {
                this.pmsType(null);
            }
        }
    }

    /**
     * Determine whether state object should be created or not.
     * @private 
     */
    shouldCreateStateObject() {
        if(this.filterState) return false;
        return (this.userGroup() || this.businessUnit() || (this.userEnabled() !== null));
    }
}

export default {
    viewModel: ScreenBase.createFactory(UserListFilterScreen),
    template: templateMarkup
};

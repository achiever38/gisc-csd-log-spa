﻿import ko from "knockout";
import templateMarkup from "text!./group-select.html";
import ScreenBase from "../../screenbase";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import WebRequestGroup from "../../../../app/frameworks/data/apicore/webRequestGroup";
import DefaultSorting from "../../../../app/frameworks/constant/defaultSorting";
import {Constants, Enums} from "../../../../app/frameworks/constant/apiConstant";

/**
 * Company Admin: Select User Groups
 * 
 * @class UserGroupSelectScreen
 * @extends {ScreenBase}
 */
class UserGroupSelectScreen extends ScreenBase {
    
    /**
     * Creates an instance of UserGroupSelectScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        
        this.mode = this.ensureNonObservable(params.mode);

        this.bladeTitle(this.i18n("Common_UserGroups")());

        this.userGroups = ko.observableArray();
        this.selectedItems = ko.observableArray();
        this.filterText = ko.observable();
        this.order = ko.observable([[ 1, "asc" ]]);

        // Internal member
        this._selectedUserGroupsIds = this.ensureNonObservable(params.selectedUserGroupIds, []);
        this._selectedItemsSubscribe = this.selectedItems.subscribe((group) => {
            this._selectedUserGroupsIds = group.map((g) => {
                return g.id;
            });
        });

        this._unableToManageGroupIds = this.ensureNonObservable(params.unableToManageGroupIds, []);
    }

    /**
     * Get WebRequest for Group module in Web API access.
     * @readonly
     */
    get webRequestGroup() {
        return WebRequestGroup.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;
        
        var dfd = $.Deferred();

        var groupFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            isSystem: false,
            isCompanyGroup: true,
            sortingColumns: DefaultSorting.Group
        };
        this.webRequestGroup.listGroupSummary(groupFilter).done((response) => {
            var userGroups = response["items"];

            // filter out unableToManageGroup, for case update primary admin, we won't allow to select/unselect subscription groups to prevent missing permissions 
            userGroups = _.filter(userGroups, (o) => {
                return _.indexOf(this._unableToManageGroupIds, o.id) === -1;
            });

            this.userGroups(userGroups);

            // If there is _selectedUserGroupsIds, set pre-selected value
            _.each(this._selectedUserGroupsIds, (groupId) => {
                let match = _.find(this.userGroups(), (userGroup) => {
                    return userGroup.id == groupId;
                });
                if(match) {
                    this.selectedItems.push(match);
                }
            });

            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        // Setup track change.
        this.selectedItems.extend({
            trackArrayChange: true
        });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actChoose", this.i18n("Common_OK")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}
    
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if(sender.id === "actChoose") {
            this.publishMessage("ca-user-manage-group-selected", this.selectedItems());
            this.close(true);
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}
}

export default {
    viewModel: ScreenBase.createFactory(UserGroupSelectScreen),
    template: templateMarkup
};
﻿import ko from "knockout";
import templateMarkup from "text!./business-units-hierarchy-manage.html";
import ScreenBase from "../../screenbase";
import * as Screen from "../../../../app/frameworks/constant/screen";
import WebRequestBusinessUnit from "../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestUser from "../../../../app/frameworks/data/apicore/webRequestUser";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import {Constants, Enums} from "../../../../app/frameworks/constant/apiConstant";
import DefaultSorting from "../../../../app/frameworks/constant/defaultSorting";
import {ParentBusinessUnitFilter} from "./infos";
import ScreenHelper from "../../screenhelper";

/**
 * 
 * 
 * @class BusinessUnitHierarchyManageScreen
 * @extends {ScreenBase}
 */
class BusinessUnitHierarchyManageScreen extends ScreenBase {

    /**
     * Creates an instance of BusinessUnitHierarchyManageScreen.
     * 
     * @param {any} params
     * 
     * @memberOf BusinessUnitHierarchyManageScreen
     */
    constructor(params) {
        super(params);

        window.bu = this;
        this.mode = this.ensureNonObservable(params.mode);
        this.businessUnitId = this.ensureNonObservable(params.businessUnitId, 0);
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.authorizedPersons = ko.observableArray([]);
        // this.managerEnable = ko.observable(true);
        
        this.defaultParentFilter = null;
        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("BU_CreateBusinessUnitHierarchy")());
                this.defaultParentFilter = new ParentBusinessUnitFilter();
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("BU_UpdateBusinessUnitHierarchy")());
                this.defaultParentFilter = new ParentBusinessUnitFilter(this.businessUnitId);
                break;
        }

        this.name = ko.observable('');
        this.code = ko.observable('');
        this.description = ko.observable('');
        this.parent = ko.observable(null);
        this.parentOptions = ko.observableArray([]);

        this.manager = ko.observable(null);
        this.managerOptions = ko.observableArray([]);

        this.subscribeMessage("ca-user-manage-authorized-persons-selected", (selectedName) => {
            // this.managerEnable(true);
  
            _.map(selectedName,(data) =>{
                data.fullName = data.fullName == " " ? data.username : data.fullName;
            });

            this.authorizedPersons.replaceAll(selectedName);

        });

    }

    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestUser() {
        return WebRequestUser.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;
        
        var dfd = $.Deferred();

        // let buIds = this.mode === Screen.SCREEN_MODE_UPDATE ? [this.businessUnitId] : [];
        var userFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            userType: Enums.UserType.Company,
            sortingColumns: DefaultSorting.User,
            businessUnitIds : []
        };

        var d1 = this.webRequestBusinessUnit.listBusinessUnitSummary(this.defaultParentFilter);
        var d2 = (this.mode === Screen.SCREEN_MODE_UPDATE) ? this.webRequestBusinessUnit.getBusinessUnit(this.businessUnitId) : null;
        var d3 = this.webRequestUser.listUserSummary(userFilter);

        $.when(d1, d2, d3).done((r1, r2, r3) => {
            this.parentOptions(r1["items"]);

            let userItems = r3["items"];
            _.map(userItems,(data) =>{
                data.fullName = data.fullName == " " ? data.username : data.fullName;
            });

            this.managerOptions(userItems);

            if(r2) {
                var businessUnit = r2;
                this.name(businessUnit.name);
                this.code(businessUnit.code);
                this.description(businessUnit.description);
                if(businessUnit.parentBusinessUnitId){
                    this.parent(businessUnit.parentBusinessUnitId.toString());
                }
                this.manager(ScreenHelper.findOptionByProperty(this.managerOptions, "id", businessUnit.managerId));
                
                let authenItems = businessUnit.authorizedPersonInfos;
                if(_.size(authenItems)){
                    _.map(authenItems,(item) =>{
                        item.id = item.authorizedPersonId;
                        item.fullName = item.authorizedPersonName;
                    });
                }
                this.authorizedPersons(authenItems);
            }
            
            


            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * 
     * @memberOf BusinessUnitHierarchyManageScreen
     */
    setupExtend() {
        var self = this;
        this.name.extend({ trackChange: true});
        this.code.extend({ trackChange: true});
        this.description.extend({ trackChange: true});
        this.parent.extend({ trackChange: true});
        this.manager.extend({ trackChange: true});

        // Validation
        this.name.extend(
        { 
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            } 
        });
        this.code.extend({ 
            required: true, 
            minLength: {
                params: 3,
                message: this.i18n("M058")()
            }, 
            maxLength: {
                params: 10,
                message: this.i18n("M058")()
            },
            serverValidate: {
                params: "Code",
                message: this.i18n("M010")()
            }
        });

        // this.manager.extend({
        //     validation:{
        //         validator: (val) => {
        //             let checkDup = true;
        //             let manager = this.manager(); 
        //             let authorizedPersons = this.authorizedPersons();

        //             if(authorizedPersons){
        //                 _.forEach(authorizedPersons, function(checkLstUsers) {
        //                     if(val && checkLstUsers){
        //                         checkDup = checkLstUsers.id != val.id
        //                     }
        //                     return checkDup;
        //                 });
        //             }
        //             return checkDup;
        //         },
        //         message: this.i18n("M010")()
        //     }
        // });

        this.validationModel = ko.validatedObservable({
            name: this.name,
            code: this.code
        });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        if((this.mode === Screen.SCREEN_MODE_CREATE && WebConfig.userSession.hasPermission(Constants.Permission.CreateBusinessUnit))
            || (this.mode === Screen.SCREEN_MODE_UPDATE && WebConfig.userSession.hasPermission(Constants.Permission.UpdateBusinessUnit))) 
        {
             actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        }
        actions.push(this.createAction("actCancel", this.i18n("Common_Cancel")(), "cancel"));
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * Generate Business Unit model from View Model
     * 
     * @returns Business Unit model which is ready for ajax request 
     */
    generateModel() {
        let userAuthenItems = [];
        if(_.size(this.authorizedPersons())){
            userAuthenItems = _.map(this.authorizedPersons(),(data) => {
                data.authorizedPersonId = data.id;
                return data;
            });
        }
        // _.map();
        var model = {
            id: this.businessUnitId,
            name: this.name(),
            code: this.code(),
            description: this.description(),
            companyId : this.companyId,
            parentBusinessUnitId: this.parent() ? this.parent() : null,
            managerId : this.manager() ? this.manager().id : null,
            authorizedPersonInfos: userAuthenItems,

        };
        return model;
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            // Mark start of long operation
            this.isBusy(true);
            var model = this.generateModel();

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestBusinessUnit.createBusinessUnit(model).done(() => {
                        this.publishMessage("ca-business-unit-changed");
                        this.close(true);
                    }).fail((e)=> {
                        this.handleError(e);
                    }).always(()=>{
                        this.isBusy(false);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.webRequestBusinessUnit.updateBusinessUnit(model).done(() => {
                        this.publishMessage("ca-business-unit-changed");
                        this.close(true);
                    }).fail((e)=> {
                        this.handleError(e);
                    }).always(()=>{
                        this.isBusy(false);
                    });
                    break;
            }
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}

    addAuthorizedPersonName(){
        // this.managerEnable(false);
        // let selectedName = this.manager();
        let managerOptions = this.managerOptions();

        // let lstUsers = _.filter(managerOptions,(data) =>{
        //     if(selectedName && data.id != selectedName.id){
        //         return data;
        //     }
        // });   

        // let userAuthenItems = [];
        // if(_.size(this.managerOptions())){
        //     userAuthenItems = _.map(this.managerOptions(),(data) => {
        //         data.authorizedPersonId = data.id;
        //         return data;
        //     });
        // }
        
        this.navigate('ca-business-units-authorized-persons', {
             items: managerOptions,
             selectedItems: _.cloneDeep(this.authorizedPersons())
        });

    }
}

export default {
    viewModel: ScreenBase.createFactory(BusinessUnitHierarchyManageScreen),
    template: templateMarkup
};
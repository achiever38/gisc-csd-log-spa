import ko from "knockout";
import templateMarkup from "text!./authorized-persons.html";
import * as Screen from "../../../../app/frameworks/constant/screen";
import ScreenBase from "../../screenbase";
import Utility from "../../../../app/frameworks/core/utility";
import {BusinessUnitFilter} from "./infos";
import WebRequestUser from "../../../../app/frameworks/data/apicore/webRequestUser";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import DefaultSorting from "../../../../app/frameworks/constant/defaultSorting";
import {Constants, Enums} from "../../../../app/frameworks/constant/apiConstant";

/**
 * 
 * 
 * @class BusinessUnitAuthorizedPersonsScreen
 * @extends {ScreenBase}
 */
class BusinessUnitAuthorizedPersonsScreen extends ScreenBase {
    /**
     * Creates an instance of BusinessUnitAuthorizedPersonsScreen.
     * 
     * @param {any} params
     * 
     * @memberOf BusinessUnitAuthorizedPersonsScreen
     */
    constructor(params) {
        super(params);
        
        this.mode = this.ensureNonObservable(params.mode);
        this.bladeTitle(this.i18n("BU_AuthorizedPersons")());

        let items = params.items ? params.items : [];
        let paramsSelectedItems = params.selectedItems ? params.selectedItems : [];
        this.userItem = ko.observableArray(items);
        this.selectedItems = ko.observableArray(paramsSelectedItems);
        this.filterText = ko.observable();
    }

    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestUser() {
        return WebRequestUser.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;
        // var userItem = this.items;

        // _.map(userItem,(data) =>{
        //     data.fullName = data.fullName == " " ? data.username : data.fullName;
        // });
        
        // this.userItem(userItem);

        // _.each(this._selectedAuthorizedPersonsIds, (authorizedPersonsId) => {
        //     let match = _.find(this.userItem(), (userItem) => {
        //         return userItem.id == authorizedPersonsId;
        //     });
        //     if(match) {
        //         this.selectedItems.push(match);
        //     }
        // });
    }

    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        this.selectedItems.extend({
            trackArrayChange: true
        });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actChoose", this.i18n("Common_OK")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}
    
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if(sender.id === "actChoose") {
            this.publishMessage("ca-user-manage-authorized-persons-selected", this.selectedItems());
            this.close(true);
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}
}

export default {
    viewModel: ScreenBase.createFactory(BusinessUnitAuthorizedPersonsScreen),
    template: templateMarkup
};
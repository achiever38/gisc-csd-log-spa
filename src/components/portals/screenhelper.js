import ko from "knockout";
import i18nextko from "knockout-i18next";
import {Constants, Enums} from "../../app/frameworks/constant/apiConstant";
import Utility from "../../app/frameworks/core/utility";
import WebConfig from "../../app/frameworks/configuration/webconfiguration";
import MessageboxManager from "../../components/controls/gisc-chrome/messagebox/messageboxManager";
import * as BladeDialog from "../../app/frameworks/constant/bladeDialog";

/**
 * Helper class for ScreenBase.
 * It is intended to be static class, with all static method.
 * 
 * @class ScreenHelper
 */
class ScreenHelper {

    /**
     * Create Yes/No options and put in ko.observableArray
     * 
     * @static
     * @public
     * @returns observableArray with items structure {title:'{string}', value: {boolean}}
     */
    static createYesNoObservableArray() {
        return ko.observableArray([{
            title: i18nextko.t("Common_Yes")(),
            value: true
        }, {
            title: i18nextko.t("Common_No")(),
            value: false
        }, ]);
    }
    
    /**
     * 
     * Create Yes/No options with value 0,1 and put in ko.observableArray
     * @static
     * @returns
     * 
     * @memberOf ScreenHelper
     */
    static createYesNoObservableArrayWithDisplayValue() {
        return ko.observableArray([{
            title: Utility.stringFormat("{0} ({1})", i18nextko.t("Common_Yes")(), "1"),
            value: true,
        }, {
            title: Utility.stringFormat("{0} ({1})", i18nextko.t("Common_No")(), "0"),
            value: false
        }, ]);
    }

     static createAllCustomObservableArray() {
        return ko.observableArray([{
            title: i18nextko.t("Common_All")(),
            value: true
        }, {
            title: i18nextko.t("Common_Custom")(),
            value: false
        }, ]);
    }

    /**
     * Create Groups options and put in ko.observableArray
     * 
     * @public
     * @static
     * @returns observableArray with items structure {title:'{string}', value: {Number}}
     */
    static createGroupsObservableArray(jsonData) {
        return ScreenHelper.createGenericOptionsObservableArray(jsonData, "name", "id");
    }

    /**
     * Create Authentication Type options and put in ko.observableArray
     * FIXME: This is obsolete, use enumResourceWebRequest instead.
     * 
     * @public
     * @static
     * @returns
     */
    static createAuthenticationTypeObservableArray() {
        return ko.observableArray([{
            title: i18nextko.t("AuthenticationType_Form")(),
            value: 1
        }, {
            title: i18nextko.t("AuthenticationType_ActiveDirectory")(),
            value: 2
        }, ]);
    }

    /**
     * Create Event Type options and put in ko.observableArray
     * 
     * @static
     * @returns
     */
    static createEventTypeObservableArray() {
        return ko.observableArray([{
            title: i18nextko.t("EventType_Input")(),
            value: Enums.ModelData.EventType.Input
        }, {
            title: i18nextko.t("EventType_Output")(),
            value: Enums.ModelData.EventType.Output
        }, ]);
    }
    
    /**
     * Create Event Data Type options and put in ko.observableArray
     * 
     * @static
     * @returns
     */
    static createEventDataTypeObservableArray() {
        return ko.observableArray([{
            title: i18nextko.t("EventDataType_Digital")(),
            value: Enums.ModelData.EventDataType.Digital
        }, {
            title: i18nextko.t("EventDataType_Analog")(),
            value: Enums.ModelData.EventDataType.Analog
        }, {
            title: i18nextko.t("EventDataType_Text")(),
            value: Enums.ModelData.EventDataType.Text
        } ]);
    }

    /**
     * Create generic options and put in ko.observableArray
     * title and value in options determine by titleProp and valueProp
     * 
     * @public
     * @static
     * @param {any} arrayData - datasource to transform
     * @param {any} titleProp - title property for option.
     * @param {any} valueProp - value property for option.
     * @returns
     */
    static createGenericOptionsObservableArray(arrayData, titleProp, valueProp) {
        var options = [];
        arrayData.forEach(function(item) {
            options.push({ 
                title: item[titleProp],
                value: item[valueProp]
            });
        }, this);
        return ko.observableArray(options);
    }

    /**
     * Find Option from observableArrayOptions given propertyName and propertyValue
     * 
     * @public
     * @static
     * @param {any} observableArrayOptions
     * @param {any} propertyName
     * @param {any} propertyValue
     * @returns an item in observableArrayOptions if found, null otherwise.
     */
    static findOptionByProperty(observableArrayOptions, propertyName, propertyValue) {
        var option = ko.utils.arrayFirst(observableArrayOptions(), function(item) {
            return item[propertyName] === propertyValue;
        });
        return option;
    }

    /**
     * Get i18n Yes/No string from given boolean
     * 
     * @public
     * @static
     * @param {boolean} bool
     * @returns
     */
    static getYesNoTextFromBoolean(bool) {
        return bool ? i18nextko.t("Common_Yes")() : i18nextko.t("Common_No")();
    }

    /**
     * Format Import Validation Error M076, M077
     * @static
     * @param {any} messageNumber
     * @param {any} errors
     * @returns
     * 
     * @memberOf ScreenHelper
     */
    static formatImportValidationErrorMessage(messageNumber, errors) {
        var errorMessage = "";
        if (errors && errors.length > 0) {
            var errorReasons = ko.utils.arrayMap(errors, (error) => {
                return error.reason;
            });
            errorMessage = Utility.stringFormat(i18nextko.t(messageNumber)(), _.join(errorReasons, "\r\n"));
        }
        return errorMessage;
    }

    /**
     * Check if given user is in Enums.UserGroup.SystemAdmin group.
     * Note that currently we checked on groupName following BL pattern.
     * 
     * user is JSON similar to this:
     * { 
     *      username: "",
     *      userType: 1,
     *      groups: [
     *          {
     *              canManage: true,
     *              groupId: 1,
     *              groupName: "System Admin"
     *          }  
     *      ] 
     * }
     * 
     * @static
     * @param {any} user
     * @returns true if user is system administrator, false otherwise.
     */
    static isUserSysAdmin(user) {
        var userGroup = user.groups[0];
        if(!_.isNil(userGroup)) {
            return userGroup.groupName === "System Admin";
        }
        return false;
    }
    
    /**
     * Check if given userId is current user.
     * 
     * @static
     * @param {any} userId
     * @returns true if userId match current login id.
     */
    static isCurrentUser(userId) {
        return WebConfig.userSession.id === userId;
    }

    
    /**
     * Handle download file
     * 
     * @static
     * @param {any} fileUrl
     * 
     */
    static downloadFile(fileUrl) {
        if (Utility.isiOSDevice()) {
            MessageboxManager.getInstance().showMessageBox(null, Utility.stringFormat(i18nextko.t("M098")(), fileUrl), BladeDialog.BUTTON_OK);
        }
        else {
            window.location.assign(fileUrl);
        }
    }

    /**
     * Find business unit by id, this will return all children include parent id.
     * 
     * @static
     * @param {any} allBusinessUnits 
     * @param {any} parentId 
     * @returns {array}
     */
    static findBusinessUnitsById(allBusinessUnits = [], parentId) {
        var result = [];

        // Recursive function.
        var traverseTree = (node, matchId) => {
            // Declare next reference id.
            var nextMatchId = matchId;

            // If id match parent id then adding to the list.
            if((node.id === matchId) || (node.parentBusinessUnitId === matchId)) {
                result.push(node.id);
                // Include all child by swapping next match id.
                nextMatchId = node.id;
            }

            // Recursive children for inner nodes.
            if(node.childBusinessUnits.length) {
                node.childBusinessUnits.forEach((cNode) => {
                    traverseTree(cNode, nextMatchId);
                });
            }
        };

        // Find until found parent node id the top level of an array is root nodes.
        if(allBusinessUnits.length) {
            allBusinessUnits.forEach((rootNode) => {
                traverseTree(rootNode, parentId);
            });
        }

        return result;
    }
}

export default ScreenHelper;
import ko from "knockout";
import "knockout-postbox";
import i18nextko from "knockout-i18next";
import ObjectBase from "../app/frameworks/core/objectBase";
import GC from "../app/frameworks/core/gc";
import Logger from "../app/frameworks/core/logger";
import Utility from "../app/frameworks/core/utility";
import ErrorBase from "../app/frameworks/core/errorBase";
import WebConfig from "../app/frameworks/configuration/webConfiguration";
import MessageboxManager from "./controls/gisc-chrome/messagebox/messageboxManager";

/**
 * Base class for all Knockout Component
 * 
 * @class ComponentBase
 * @extends {ObjectBase}
 */
class ComponentBase extends ObjectBase {
    /**
     * Creates an instance of ComponentBase.
     * @public
     */
    constructor(params) {
        super();

        // Auto assign id if passing to object.
        if (params.id) {
            this.id = params.id;
        }

        // Internal management for loading state.
        this.isLoaded = ko.observable(false);
        this.isFirstLoad = ko.observable(true);
        this.isError = ko.observable(false);
        this.errorMessage = ko.observable();
    }

    /**
     * Called when View is attached.
     * @public
     * @returns
     */
    onViewAttach() {
        return this.onLoad(this.isFirstLoad());
    }

    /**
     * Called when View is detached.
     * @public
     */
    onViewDetach() {
        // Unbind all jquery events automatically.
        let componentElem = $("#" + this.id);
        if (componentElem.length) {
            componentElem.find("*").unbind();
        }

        // // Using GC to cleanup if exists.
        // GC.dispose(this);

        // Trigger from blade Control.
        this.onUnload();
    }

    /**
     * Called when View is loaded.
     * @param {boolean} isFirstLoad
     * @public
     */
    onLoad(isFirstLoad) {
        Logger.warn("Please implement onLoad");
    }

    /**
     * Called when View is unloaded.
     * @public
     */
    onUnload() {
        Logger.warn("Please implement onUnload");
    }

    /**
     * Get i18n value for given key.
     * If mockValue is specified, return that value.
     * 
     * @param {string} key
     * @param {any} options
     * @param {string} mockValue, mock to avoid actual translation.
     * @returns
     */
    i18n(key, options) {
        var result = key;

        if(_.isArray(options)){
            // greeting: 'Hello {0} {1}''
            // Using C# string format.
            var translatedFormat = i18nextko.t(key);

            // Peform string format like c#.
            // Utility.stringFormat("{0}, {1}", firstname, lastname);
            options.unshift(translatedFormat());
            result = Utility.stringFormat.apply(Utility, options);
        }
        else {
            // greeting: 'Hello __firstname__'
            // Using i18next format.
            result = i18nextko.t(key, options);
        }

        return result;
    }

    /**
     * Resolve absolute path from server relative path which host website.
     * 
     * @param {any} serverRelativePath
     */
    resolveUrl(serverRelativePath) {
        return Utility.resolveUrl(WebConfig.appSettings.rootPath, serverRelativePath);
    }

    /**
     * Ensure bindingContext is not ko.observable
     * 
     * @public
     * @param {any} bindingContext
     * @param {any} [defaultValue=null]
     * @returns
     */
    ensureNonObservable(bindingContext, defaultValue = null) {
        var nonObservableResult = null;

        if (ko.isObservable(bindingContext)) {
            // Auto Create observable for passing parameter.
            nonObservableResult = bindingContext(); // Call getter.
        } else {
            // Verify data should be match.
            nonObservableResult = bindingContext;

            if (bindingContext === undefined) {
                nonObservableResult = defaultValue;
            }
        }

        return nonObservableResult;
    }

    /**
     * Ensure bindingContext is ko.observable
     * 
     * @public
     * @param {any} bindingContext
     * @param {any} [defaultValue=null]
     * @returns
     */
    ensureObservable(bindingContext, defaultValue = null) {
        var observableResult = null;

        if (!ko.isObservable(bindingContext)) {
            // Auto Create observable for passing parameter.
            if (bindingContext !== undefined) {
                observableResult = ko.observable(bindingContext);
            } else {
                observableResult = ko.observable(defaultValue);
            }
        } else {
            // It is already observable so we return input directly.
            observableResult = bindingContext;
        }

        return observableResult;
    }
    
    /**
     * Ensure bindingContext is a function
     * 
     * @param {any} bindingContext
     * @param {any} [defaultValue=null]
     */
    ensureFunction(bindingContext, defaultValue = null) {
        var functionRefResult = null;

        if(_.isFunction(bindingContext)) {
            functionRefResult = bindingContext;
        } else {
            functionRefResult = defaultValue;
        }

        return functionRefResult;
    }

    /**
     * Throw new error with details stack
     * 
     * @param {number} id error id, padd with 4 digit
     * @param {any} message
     */
    throwError(id, message) {
        // Throw error to debugging console only.
        Logger.error(new ErrorBase(message, id));
    }

    /**
     * Messagebox show function.
     * 
     * @param {any} title
     * @param {any} message
     * @param {any} type
     * @returns
     */
    showMessageBox (title, message, type) {
        return MessageboxManager.getInstance().showMessageBox(title, message, type);
    }
}

export default ComponentBase;
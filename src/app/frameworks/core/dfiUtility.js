import ko from "knockout";
import _ from "lodash";
import Singleton from "./singleton";
import WebRequestBase from "../data/webRequestBase";
import Logger from "./logger";
import WebConfig from "../configuration/webConfiguration";
import "iqtech-swf";


/**
 * Static class which provide utility method for system wide.
 * Note: Please keep this class as static class
 * 
 * @class Utility
 */
class DFITechUtility extends WebRequestBase {

    constructor(username,password,ip) {
        super();


        

        //this.username = username;
        //this.password = password;


        this.httpProtocol = "http://";
        if (location.protocol == 'https:') {
            this.httpProtocol = "https://";
        }
        this.serviceUrlEvent = ko.observable(null);

        //this.serviceUrl = this.httpProtocol + ip;
        this.jsession = ko.observable(null);
        this.jsessionId = ko.observable(null);
    }

    static getInstance() {
        return Singleton.getInstance("dfiUtility", new DFITechUtility());
    }

    getDevIdNo(vehiIdno, config) {

        if (config){
            this._createConfig(config);
        }
        else {
            this._createDefaultConfig();
        }

        var ret = {
            "devices": [],
            "sessionId" : null
        };

        var dfd = $.Deferred();
        var dfdLogin = this._login();
        $.when(dfdLogin).done(() => {

            ret.sessionId = this.jsession();

            var dfdGetDevIdNo = this._getDevIdNo(vehiIdno);
            $.when(dfdGetDevIdNo).done((res) => {
                if (res.result == 0) {
                    ret.devices = res.devices;
                    // if (res.devices.length > 0) {
                    //     ret.deviceId = res.devices[0].did;
                    // }
                }
                dfd.resolve(ret);
            }).fail((e)=>{
                dfd.reject(e);
            });
        });

        return dfd;
    }

    getEventVideo(vehiIdno, startDateTime, endDateTime, config) {

        if (config) {
            this._createConfig(config);
        }
        else {
            this._createDefaultConfig();
        }

        var ret = {
            "eventvideo": [],
            "deviceId": null,
            "sessionId": null
        };
        
        var dfd = $.Deferred();

        var dfdLogin = this._login();
        $.when(dfdLogin).done(() => {

            ret.sessionId = this.jsession();

            var dfdGetDevIdNo = this._getDevIdNo(vehiIdno);
            $.when(dfdGetDevIdNo).done((res) => {
                
                if (res.result == 0) {
                    
                    if (res.devices.length > 0) {

                        var deviceId = res.devices[0].did;

                        //var searchMinutesDuration = 3;

             
                        //var startDateTime = new Date(searchDateTime);
                        //var endDateTime = new Date(searchDateTime);
                        //startDateTime.setMinutes(searchDateTime.getMinutes() - searchMinutesDuration);
                        //endDateTime.setMinutes(searchDateTime.getMinutes() + searchMinutesDuration);

                        var beginFormat = this._convertTimeFormat(startDateTime);
                        var endFormat = this._convertTimeFormat(endDateTime);

                        var params = {
                            "DownType":2,
                            "DevIDNO": deviceId,
                            "LOC": 2, //Terminal? Server?
                            "FILESVR": 0,
                            "CHN": -1, //All Channel
                            "YEAR": startDateTime.getFullYear(),
                            "MON": ("0" + (startDateTime.getMonth() + 1)).slice(-2),
                            "DAY": startDateTime.getDate(),
                            "RECTYPE": 1,
                            "FILEATTR": 2,
                            "BEG": beginFormat,
                            "END": endFormat
                        }
                        
                        var dfdEventVideo = this._getEventVideo(params);
                        $.when(dfdEventVideo).done((resEventVideo) => {
                            ret.eventvideo = resEventVideo;
                            ret.sessionId = this.jsession();
                            ret.deviceId = deviceId

                            dfd.resolve(ret);
                        });
                     }
                }
            });
        });

        return dfd;
    }

    _login(count = 1) {
        var dfdLogin = $.Deferred();

        var params = {
            "account": this.username,
            "password": this.password
        };
        var loginUrl = this._ajaxPost(this.serviceUrl + "/StandardApiAction_login.action", params);
        $.when(loginUrl).done((res) => {
            this.jsession = ko.observable(res.jsession);
            this.jsessionId = ko.observable(res.JSESSIONID);
            dfdLogin.resolve(res);
        }).fail((e) => {
            Logger.warn(e);
            count++;
            if(count <= 5){
                this._login(count);
            }else{
                dfdLogin.reject(e);
                console.error(e);
            }
            
        });

        return dfdLogin;
    }

    _createConfig(config) {
        var arrConfig = config.split('|');
        this.username = arrConfig[0];
        this.password = arrConfig[1];

        this.serviceUrlEvent = arrConfig[2].split('//');
         this.serviceUrl = arrConfig[2];//this.httpProtocol + arrConfig[2];
    }

    _createDefaultConfig() {
        this.username = "";//"nostra";
        this.password = "";//"000000";
        this.serviceUrl = "";//this.httpProtocol + "180.183.244.123";
    }

    _getDevIdNo(vehiIdno) {
        var dfdGetDivIdNo = $.Deferred();

        var params = {
            "vehiIdno": vehiIdno,
            "jsession": this.jsession()
        };

        var getDivNo = this._ajaxPost(this.serviceUrl + "/StandardApiAction_getDeviceByVehicle.action", params);
        $.when(getDivNo).done((res) => {
            dfdGetDivIdNo.resolve(res);
        }).fail((e) => {
            Logger.warn(e);
            dfdGetDivIdNo.reject(e);
        });

        return dfdGetDivIdNo;
    }
// _getVideo() {
//     var loginUrl = "http://"+this.serviceUrlEvent[1] + "/StandardApiAction_getVideoFileInfo.action", params);

// }
    _getEventVideo(params) {
// console.log(">>>params>",params) 
        
        var ret = [];
        var dfdGetDivIdNo = $.Deferred();
        
        /*
        var url = location.protocol+"//"+this.serviceUrlEvent[1] +"/StandardApiAction_getVideoFileInfo.action" //":6603/3/5"//Device :6604/3/5 or server :6603/3/5  
            + "?DownType=" + params.DownType // DownType 2 query vido, 3 download video, 5 play
            + "&DevIDNO=" + params.DevIDNO
            + "&LOC=" + params.LOC
            + "&CHN=" + params.CHN
            + "&YEAR=" + params.YEAR
            + "&MON=" + params.MON
            + "&DAY=" + params.DAY
            + "&RECTYPE=" + params.RECTYPE
            + "&FILEATTR=" + params.FILEATTR
            + "&BEG=" + params.BEG
            + "&END=" + params.END
            + "&ARM1=" +0
            + "&ARM2="+0
            +"&RES="+0
            +"&STREAM="+0
            +"&STORE="+0
            +"&jsession="+this.jsession();
        var getDivNo = this._ajaxGet(url);
        $.when(getDivNo).done((res) => {
// console.log(">>url>>",url) 
// console.log(">>>>",res) 
            if (res.files){
                if (res.files.length > 0) {
                    _.forEach(res.files, (fileItem) => {

                        //{"cmsserver":1,"files":[{"arm":0,"arm1":0,"arm2":0,"beg":49492,"chn":1,"chnMask":0,"day":22,"devIdno":"118000110092",
                        //"end":49510, "file":"rec/a4/fly00043.ifv-offset0", "len":6370391,
                        //"loc":1, "mon":8, "recing":1, "res":0, "store":0, "stream":0, "svr":0, "type":1, "year":18
                        //}


                        //DownType=5&DevIDNO=118000110092&FILELOC=1&FILESVR=0&FILECHN=3
                        //&FILEBEG=58069 & FILEEND=58179 & PLAYIFRM=0 
                        //& PLAYFILE=rec / a1 / fly00309.ifv - offset256676352 & PLAYBEG=0 & PLAYEND=0 & PLAYCHN=0`
                        
                        //var url = this.serviceUrl + ":6604/3/5" //Device :6604/3/5 or server :6603/3/5  
                        var url = "http://" + this.serviceUrlEvent[1] + ":6604/3/5" //Device :6604/3/5 or server :6603/3/5  
                            + "?DownType=5" 
                            + "&DevIDNO=" + fileItem.devIdno
                            + "&FILELOC=" + fileItem.loc
                            + "&FILESVR=" + fileItem.svr
                            + "&FILECHN=" + fileItem.chn
                            + "&FILEBEG=" + fileItem.beg
                            + "&FILEEND=" + fileItem.end
                            + "&PLAYBEG=" + 0
                            + "&PLAYEND=" + 0
                            + "&PLAYIFRM=0"
                            + "&PLAYFILE=" + fileItem.file
                            + "&PLAYCHN=" + fileItem.chn;
                    
                        let sd = new Date();
                        sd.setHours(0);
                        sd.setMinutes(0);
                        sd.setSeconds(fileItem.beg);

                        let ed = new Date();
                        ed.setHours(0);
                        ed.setMinutes(0);
                        ed.setSeconds(fileItem.end);

                        ret.push({
                            "devIdno": fileItem.devIdno,
                            "channel": fileItem.chn,
                            "url": url,
                            "startDate": new Date(sd),
                            "endDate": new Date(ed),
                            "beg": fileItem.beg,
                            "end": fileItem.end
                        });
                    });
                }
            }
            */
            dfdGetDivIdNo.resolve(ret);
            /*
        }).fail((e) => {
            Logger.warn(e);
            dfdGetDivIdNo.reject(e);
        });
*/
        return dfdGetDivIdNo;
        
    }

    _convertTimeFormat(paramsDate) {
        var retFormat = "";
        if (paramsDate) {
            retFormat = (paramsDate.getHours() * 3600) + (paramsDate.getMinutes() * 60) + (paramsDate.getSeconds());
        }
        return retFormat.toString();
    }



    /**
     * Raise jQuery ajax POST.
     * 
     * @param {any} url
     * @param {any} data
     * @returns jQuery Deferred object
     */
    _ajaxPost(url, data) {
        return this._ajaxWebRequest("post", url, this._postify(data));
    }

    _ajaxGet(url, data) {
        var dfdGet = $.Deferred();

        $.get(url, (data) => {
            dfdGet.resolve(JSON.parse(data));
        });

        return dfdGet;
    }

    /**
     * Base AJAX request method
     * 
     * @param {any} verb
     * @param {any} url
     * @param {any} data
     * @returns jQuery Deferred object
     */
    _ajaxWebRequest(verb, url, data) {
        var ajaxOption = {
            url: this.resolveUrl(url),
            type: verb,
            data: $.isEmptyObject(data) ? "{}" : data,
            crossDomain: true,
            dataType: 'JSONP'
        };

        // jQuery AJAX response is array with 3 objects, 
        // 1. the actual response object
        // 2. response status
        // 3. XHR object.
        // We need only 1, so create Deferred to handle it manually.
        var dfd = $.Deferred();

        var beginAjax = new Date();
        $.ajax(ajaxOption).done((r) => {
            dfd.resolve(r);
        }).fail((e) => {
            dfd.reject(e);
        }).always(() => {
            // Inform developer when service is greather than 1 seconds.
            var ajaxDuration = ((new Date() - beginAjax) / 1000);
            if (ajaxDuration > 1) {
                Logger.warn(`${verb} ${url} service takes ${ajaxDuration} seconds, please contact your service provider.`);
            }
        });

        return dfd;
    }
}

export default DFITechUtility;
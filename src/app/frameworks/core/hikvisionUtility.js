import ko from "knockout";
import _ from "lodash";
import Singleton from "./singleton";
import WebRequestBase from "../data/webRequestBase";
import Logger from "./logger";
import { WebControl } from "hikvision";

/**
 * Static class which provide utility method for system wide.
 * Note: Please keep this class as static class
 * 
 * @class Utility
 */
class HikvisionUtility extends WebRequestBase {

    constructor(applicationUrl) {
        super(applicationUrl);
        $.support.cors = true;
        this.oLoginWebControl = null;
        this.userName="admin";
        this.password="DGFP@$$w0rddgf";
        // this.pwd = "FB00666FBB97806A087BE15ECD2ED720";
        this.uri="http://49.231.64.169";
        this.domainName="CDG";
        this.address = "192.168." + Math.floor(Math.random() * 254) + "." + Math.floor(Math.random() * 254)  ;
        this.codeUuid = new Date().getTime()+Math.random();
        this.permission = {
                    "liveView": 1,
                    "playback": 1,
                    "videoSearch": 1,
                    "manualRecording": 1,
                    "twoWayAudio": 1,
                    "ptzControl": 1,
                    "capturePrint": 1,
                    "volume": 1
                };
        
        this.isPasswordEncry = true;
        this.passwordEncry = ko.observable();
        this.nodes = ko.observable();
        this.sessionId = ko.observable();


    }

    static getInstance() {
        return Singleton.getInstance(
            "hikvisionUtility",
            new HikvisionUtility("http://49.231.64.169")
        );
    }


    liveView(deviceId) {
        var self = this;
        // 1 init web control
        var dfdInitWebControl = $.Deferred();
        var initWebControl = self.initWebControl();
        $.when(initWebControl).done((res)=> {
            dfdInitWebControl.resolve();
        }).fail((e) => {
            Logger.warn(e);
            dfdInitWebControl.reject(e);
        });

        // 2 login
        var dfdInitLogin = $.Deferred()
        $.when(dfdInitWebControl).done((res)=> {
            self.initLogin().done((res) => {
                dfdInitLogin.resolve();
            });
        }).fail((e) => {
            Logger.warn(e);
            dfdInitLogin.reject(e);
        });

        // 3 gen node ans pass data
        var dfdInitLiveViewInfo = $.Deferred()
        $.when(dfdInitLogin).done((res)=> {
            self.initLiveViewInfo(deviceId).done((res) => {
                dfdInitLiveViewInfo.resolve({items:res});
            });
        }).fail((e) => {
            Logger.warn(e);
            dfdInitLiveViewInfo.reject(e);
        });
        
        return dfdInitLiveViewInfo;
    }

    playBack(filter) {
        var self = this;
        if(!self.sessionId()){
            // 1 init web control
            var dfdInitWebControl = $.Deferred();
            var initWebControl = self.initWebControl();
            $.when(initWebControl).done((res)=> {
                dfdInitWebControl.resolve();
            }).fail((e) => {
                Logger.warn(e);
                dfdInitWebControl.reject(e);
            });

            // 2 login
            var dfdInitLogin = $.Deferred()
            $.when(dfdInitWebControl).done((res)=> {
                self.initLogin().done((res) => {
                    dfdInitLogin.resolve();
                });
            }).fail((e) => {
                Logger.warn(e);
                dfdInitLogin.reject(e);
            });
        }
        // 3 gen node ans pass data
        var dfdInitPlayBackInfo = $.Deferred()
        $.when(dfdInitLogin).done((res)=> {
            self.initPlayBackInfo(filter).done((res) => {
                dfdInitPlayBackInfo.resolve({items:res});
            });
        }).fail((e) => {
            Logger.warn(e);
            dfdInitPlayBackInfo.reject(e);
        });
        
        return dfdInitPlayBackInfo;
    }


    startPlugin() {
        WebControl.JS_WakeUp("PlatformSDKWebControlPlugin://");
    }

    initWebControl() {
        var self = this;
        var dfd = $.Deferred();
        
        self.LoginWebControl = new WebControl({
            cbConnectSuccess : function() {
                dfd.resolve();
            },
            cbConnectError : function() {
                console.log("cbConnectError:");
                self.LoginWebControl = null;
                WebControl.JS_WakeUp("PlatformSDKWebControlPlugin://");
                // location.reload();
                dfd.reject();
            },
            cbConnectClose : function() {
                console.log("cbConnectClose");
                dfd.resolve();
                self.LoginWebControl = null;
            }
        });
        return dfd;
    }

    initLogin() {
        var self = this;
        var dfd = $.Deferred();
        self.LoginWebControl.JS_LoginEncryption(self.userName, self.password).then((data) => {
            var password = (self.isPasswordEncry) ? data.encryptInfo : self.password;
            self.passwordEncry(password);
            self.loginCameraInfo().done((res) => {
                dfd.resolve();
            }).fail((e) => {
                Logger.warn(e);
                dfd.reject(e);
            });
        });
        return dfd;
    }

    initLiveViewInfo(deviceId) {
        var self = this;
        var nodes = new Array();
        var szXml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><Params><PlatformType>3</PlatformType></Params>";
        //get all camera by device id
        var dfdCameraByDevice = $.Deferred();
        self.getCameraByDevice(deviceId).done((res) => {
            if (res.error) {
                obj.status = self._getError(ret.error);
            }
            dfdCameraByDevice.resolve();
        }).fail((e) => {
            Logger.warn(e);
            dfdCameraByDevice.reject(e);
        });

        //gen nodes
        var dfdNodes = $.Deferred();
        $.when(dfdCameraByDevice).done((res)=> {
            _.forEach(self.nodes(), (obj) => {
                var cameraName = obj.name;
                //replace streamType 0 or 1
                obj.streamType = (obj.streamType == "SubStream") ? 1 : 0;
                //get Real Stream Url
                var dfdRealStreamUrl = $.Deferred();
                self.getRealStreamUrl(obj).done((ret) => {

                    var data = ret.data;
                    //set szUrl to obj
                    obj.szUrl = (data) ? data.replace(/(?:\r\n|\r|\n)/g, '') : null;
                    if (ret.error) {
                        obj.status = self._getError(ret.error);
                    }
                    dfdRealStreamUrl.resolve();
                }).fail((e) => {
                    Logger.warn(e);
                    dfdRealStreamUrl.reject(e);
                });
                //get Encode Devices By Page
                var dfdEncodeDevicesByPage = $.Deferred();
                $.when(dfdRealStreamUrl).done((res)=> {
                    self.getEncodeDevicesByPage(obj.deviceId).done((ret) => {

                        var data = ret.data;
                        if (ret.error) {
                            obj.status = self._getError(ret.error);
                        }

                        //set device to obj
                        obj.deviceCode = data.deviceCode;
                        obj.siteId = data.siteId;
                        obj.userName = data.userName;
                        obj.userPwd = data.userPwd;
                        obj.permission = self.permission;
                        obj.szXml = szXml;
                        //obj.cameraName = cameraName;
                        obj.status = "Connected";
                      

                        dfdEncodeDevicesByPage.resolve(obj);
                    }).fail((e) => {
                        Logger.warn(e);
                        dfdEncodeDevicesByPage.reject(e);
                    });
                }).fail((e) => {
                    Logger.warn(e);
                    dfdEncodeDevicesByPage.reject(e);
                });
                nodes.push(dfdEncodeDevicesByPage);
            });

            //get all nodes and set to resolve
            Promise.all(nodes).then((data) => {
                dfdNodes.resolve(data);
            });

        }).fail((e) => {
            Logger.warn(e);
            dfdNodes.reject(e);
        });
        return dfdNodes;
    }


    initPlayBackInfo(filter) {
        var self = this;
        var nodes = new Array();

        //get all camera by device id
        var dfdCameraByDevice = $.Deferred();
        self.getCameraByDevice(filter.deviceId).done((res) => {
            if (res.error) {
                obj.status = self._getError(ret.error);
            }
            dfdCameraByDevice.resolve();
        }).fail((e) => {
            Logger.warn(e);
            dfdCameraByDevice.reject(e);
        });

        //gen nodes
        var dfdNodes = $.Deferred();
        $.when(dfdCameraByDevice).done((res)=> {
            _.forEach(self.nodes(), (obj) => {


                obj.status = null;
                obj.startTime = filter.startTime;
                obj.endTime = filter.endTime;
                obj.recordType = filter.recordType;
                obj.downloadFolder = (filter.downloadFolder) ? filter.downloadFolder : "C:/DownloadVideo";
                //get Real Stream Url
                var dfdRealStreamUrl = $.Deferred();
                self.getRecordStreamUrl(obj).done((ret) => {

                    var data = ret.data;
                    //set szUrl to obj
                    obj.szUrl = (data) ? data.replace(/(?:\r\n|\r|\n)/g, '') : null;
                    if (ret.error) {
                        obj.status = self._getError(ret.error);
                    }
                    dfdRealStreamUrl.resolve();
                }).fail((e) => {
                    Logger.warn(e);
                    dfdRealStreamUrl.reject(e);
                });
                //get Encode Devices By Page
                var dfdEncodeDevicesByPage = $.Deferred();
                $.when(dfdRealStreamUrl).done((res)=> {
                    self.getEncodeDevicesByPage(obj.deviceId).done((ret) => {
                        var data = ret.data;
                        if (ret.error) {
                            obj.status = self._getError(ret.error);
                        }

                        // xml play back and download
                        var xmlPlayBack = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><Params><PlatformType>3</PlatformType><StorageMediaType>" + obj.recordType + "</StorageMediaType><SearchTime><BeginTime>" + obj.startTime + "</BeginTime><EndTime>" + obj.endTime + "</EndTime></SearchTime></Params>";
                        var xmlDownload = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><Params><PlatformType>3</PlatformType><StorageMediaType>" + obj.recordType + "</StorageMediaType><SearchTime><BeginTime>" + obj.startTime + "</BeginTime><EndTime>" + obj.endTime + "</EndTime></SearchTime><DownloadFolder>" + obj.downloadFolder + "</DownloadFolder></Params>";
                        //set device to obj
                        obj.deviceCode = data.deviceCode;
                        obj.siteId = data.siteId;
                        obj.userName = data.userName;
                        obj.userPwd = data.userPwd;
                        obj.permission = self.permission;
                        obj.szXmlPlayback = xmlPlayBack;
                        obj.szXmlDownload = xmlDownload;

                        if (obj.status == null){
                            obj.status = "Connected";
                        }
                        //console.log(obj);
                        dfdEncodeDevicesByPage.resolve(obj);
                    }).fail((e) => {
                        Logger.warn(e);
                        dfdEncodeDevicesByPage.reject(e);
                    });
                }).fail((e) => {
                    Logger.warn(e);
                    dfdEncodeDevicesByPage.reject(e);
                });
                nodes.push(dfdEncodeDevicesByPage);
            });

            //get all nodes and set to resolve
            Promise.all(nodes).then((data) => {
                dfdNodes.resolve(data);
            });

        }).fail((e) => {
            Logger.warn(e);
            dfdNodes.reject(e);
        });
        return dfdNodes;
    }


    loginCameraInfo() {
        var self = this;
        var dfd = $.Deferred();
        var filter = {
            name:self.userName,
            password:self.passwordEncry(),
            port:'8950',
            url:self.uri,
            domainName:self.domainName,
            address:self.address,
            loginMode:3,
            codeUuid:self.codeUuid
        }
        var login = self._ajaxPost("~/webSDK/login", filter);
        $.when(login).done((res)=>{
            var result = JSON.parse(res);
            var ret = { error: null, data: null };
            if(result.errorCode == 0)
            {
                self.sessionId(result.data.sessionID);
            }
            else {
                ret.error = result.errorCode;
            }
            dfd.resolve(ret);
        }).fail((e) => {
            Logger.warn(e);
            dfd.reject(e);
        });
        return dfd;
    }

    getCameraByDevice(deviceId) {
        // searchType
        // 1 expressing that searching type is site
        // 2 expressing that searching type is area
        // 3 expressing that searching type is device
        var self = this;
        var dfd = $.Deferred();
        var searchTypeId = 3
        var filter = {
            'page':1,
            'pageSize':20,
            'searchType': searchTypeId,
            'idArrayString':deviceId,
            'sessionID':self.sessionId()
        }
        // get all camera by device id
        var camerasByPage = this._ajaxPost("~/webSDK/getCamerasByPage", filter);
        $.when(camerasByPage).done((res)=> {
            var result = JSON.parse(res);


            var ret = { error: null, data: null };

            if(result.errorCode == 0){
                var cameras = result.data;
                self.nodes(cameras);
            }
            else {
                ret.error = result.errorCode;
            }

            dfd.resolve(ret);
        }).fail((e) => {
            Logger.warn(e);
            dfd.reject(e);
        });
        return dfd;
    }

    getRealStreamUrl(node) {
        var self = this;
        var dfd = $.Deferred();
        var filter = {
            cameraIndexcode: node.indexCode,
            streamType: node.streamType,
            sessionID: self.sessionId()
        }
        // get Real Stream Url
        var realStreamUrl = this._ajaxPost("~/webSDK/getRealStreamUrl", filter);
        $.when(realStreamUrl).done((res)=> {
            var result = JSON.parse(res);
            var ret = { error: null, data: null };
            
            if(result.errorCode == 0)
            {
                ret.data = result.data;
            }
            else
            {
                ret.error = result.errorCode;
            }
            dfd.resolve(ret);
        }).fail((e) => {
            Logger.warn(e);
            dfd.reject(e);
        });
        return dfd;
    }

    getRecordStreamUrl(node) {
        // recordType
        // 1 expressing that record type is Device
        // 3 expressing that record type is CVR
        var self = this;
        var dfd = $.Deferred();
        var filter = {
            cameraIndexcode:node.indexCode,
            sessionID:self.sessionId(),
            startTime:node.startTime,
            endTime:node.endTime,
            recordMediaType:node.recordType,
            recordTypeString:"1,2,3,4,5,8,20,21"
        }
        var recordStreamUrl = this._ajaxPost("~/webSDK/getRecordStreamUrl", filter);
        $.when(recordStreamUrl).done((res)=> {
            var result = JSON.parse(res);

            var ret = { data : null , error : null };
            if(result.errorCode == 0){
                ret.data = result.data;
            }
            else {
                ret.error = result.errorCode;
            }
            dfd.resolve(ret);
        }).fail((e) => {
            Logger.warn(e);
            dfd.reject(e);
        });
        return dfd;
    }

    getEncodeDevicesByPage(deviceId) {
        // searchType
        // 1 expressing that searching type is site
        // 2 expressing that searching type is area
        // 3 expressing that searching type is device
        var self = this;
        var dfd = $.Deferred();
        var searchTypeId = 3
        var filter = {
            'page':1,
            'pageSize':20,
            'searchType': searchTypeId,
            'idArrayString':deviceId,
            'sessionID':self.sessionId()
        }
        // get all camera by device id
        var devicesByPage = this._ajaxPost("~/webSDK/getEncodeDevicesByPage", filter);
        $.when(devicesByPage).done((res)=> {
            var result = JSON.parse(res);

            var ret = { error: null, data: null };
            //var data = null;
            if(result.errorCode == 0){
                ret.data = result.data[0];
            }
            else {
                ret.error = result.errorCode;
            }
            dfd.resolve(ret);
        }).fail((e) => {
            Logger.warn(e);
            dfd.reject(e);
        });
        return dfd;
    }

    


    /**
     * Raise jQuery ajax POST.
     * 
     * @param {any} url
     * @param {any} data
     * @returns jQuery Deferred object
     */
    _ajaxPost(url, data) {
        return this._ajaxWebRequest("post", url, this._postify(data));
    }

    /**
     * Base AJAX request method
     * 
     * @param {any} verb
     * @param {any} url
     * @param {any} data
     * @returns jQuery Deferred object
     */
    _ajaxWebRequest(verb, url, data) {
        var ajaxOption  = {
            url: this.resolveUrl(url),
            type: verb,
            data: $.isEmptyObject(data) ? "{}" : data,
            crossDomain: true
        };


        // jQuery AJAX response is array with 3 objects, 
        // 1. the actual response object
        // 2. response status
        // 3. XHR object.
        // We need only 1, so create Deferred to handle it manually.
        var dfd = $.Deferred();

        var beginAjax = new Date();
        $.ajax(ajaxOption).done((r) => {
            dfd.resolve(r);
        }).fail((e) => {
            dfd.reject(e);
        }).always(()=>{
            // Inform developer when service is greather than 1 seconds.
            var ajaxDuration = ((new Date() - beginAjax) / 1000);
            if(ajaxDuration > 1) {
                Logger.warn(`${verb} ${url} service takes ${ajaxDuration} seconds, please contact your service provider.`);
            }
        });

        return dfd;
    }

    _getError(errorStatus) {
        var errorText = null;
        var errorList = {
            "1001": "Parameter error",
            "1002": "Function is not supported",
            "1003": "Insufficient memory",
            "1004": "Failed to generate data serial number",
            "1005": "Xml parsing failed",
            "1006": "Code conversion failed",
            "1007": "Message size is too large",
            "1008": "Failed to get GRPC port",
            "1009": "Failed to parse system IP or domain name",
            "1010": "GRPC connection failed",
            "1011": "Failed to decrypt by verification code",
            "1012": "Invalid URL",
            "1013": "Invalid session",
            "1014": "Http connection failed",
            "1015": "Failed to get system version",
            "1016": "Failed to create file",
            "1017": "The user has logged in",
            "2001": "Failed to connect to FS server",
            "2002": "Failed to send data to FS",
            "-1": "Session does not exist",
            "10000": "Failed",
            "10001": "The user does not exist",
            "10002": "Incorrect password",
            "10005": "The user has expired",
            "10007": "IP address and MAC address mismatched or user has already expired",
            "10008": "User has been locked",
            "10009": "No permission",
            "10010": "User session ID does not exist",
            "10011": "User permission has expired",
            "10012": "No more user is allowed to login Client",
            "10013": "AD domain name mismatches",
            "10014": "Incorrect AD domain user name or password",
            "10015": "Connection failed. AD domain configuration error",
            "10016": "No domain information has configured for AD",
            "10017": "Failed to generate verification code",
            "10018": "Verification code error",
            "10019": "Verification code has expired",
            "10020": "Risky account. Verification code is required",
            "10021": "The current password is default",
            "10023": "No more user allowed",
            "10026": "WAN is not configured",
            "10031": "Failed to display all system properties",
            "10032": "User has been deleted",
            "10033": "User name already exists",
            "10034": "Current user cannot be deleted",
            "10035": "No more role can be added",
            "10039": "Password has expired",
            "10040": "No permission to reset password",
            "10041": "IP address is locked",
            "10051": "AD domain user is disabled",
            "11004": "Failed to synchronize camera",
            "11006": "Failed to synchronize recording schedule",
            "11007": "No more EHome device allowed",
            "11008": "EHome device register code already exists",
            "11009": "EHome device name already exists",
            "11010": "The size of EHome upgrade file is too large",
            "11011": "EHome upgrade file name already exists",
            "11012": "Failed to get EHome upgrade file",
            "11013": "EHome upgrade file type error",
            "11014": "Failed to save EHome upgrade file",
            "11017": "Device connection failed",
            "11101": "Incorrect user name or password",
            "11104": "Incorrect channel No.",
            "11105": "No more Client can be added to DVR",
            "11107": "Failed to connect to server",
            "11117": "Parameters error",
            "11123": "Not supported by the server",
            "11124": "Server is busy",
            "11128": "Insufficient DVR resource",
            "11143": "Insufficient buffer",
            "11146": "No more device allowed",
            "11152": "No more user can access the device",
            "12001": "The License is already used",
            "12002": "No more areas can be added",
            "12003": "Area does not exist",
            "12004": "No available device",
            "12005": "VAG does not response in time",
            "12006": "No available camera",
            "12010": "Area levels are full",
            "12011": "Area has child area",
            "12012": "Empty area information",
            "12013": "Area name already exist",
            "20005": "System internal error",
            "20007": "Incorrect parameter",
            "20008": "Name is duplicated",
            "20009": "Exceed maximum Client number",
            "30001": "No result found",
            "30002": "Access is denied",
            "30003": "Database access exception",
            "30004": "System exception",
            "30007": "Failed to generate key",
            "30009": "Failed to get device recording schedule information",
            "30010": "Record type is not supported",
            "30011": "Failed to get device recording schedule information. Recording schedule is disabled",
            "30012": "Failed to get device information when configuring record",
            "30013": "VRM connection failed",
            "30015": "Failed delete recording schedule",
            "30016": "Failed to configure recording schedule",
            "30018": "Recording schedule configuration is not supported for HiDDNS camera",
            "30019": "Failed to get HDD groups",
            "30020": "Schedule template name already exists",
            "30021": "GUID does not exist",
            "30022": "No VRM result found",
            "30023": "No related SMS",
            "30024": "Failed to get domain information",
            "30025": "No related VAG",
            "30026": "The camera of searched video does not exist",
            "31000": "HTTP response message parsing exception",
            "40001": "The system has no License",
            "40002": "License has expired",
            "40003": "Encryption dog has expired",
            "40004": "No encryption dog",
            "40005": "Invalid License",
            "40006": "HSMA service has expired",
            "40007": "No permission to access SDK",
            "40008": "Exceed license limit",
            "40010": "System time modification",
            "40011": "License check succeeded",
            "40012": "Without license base",
            "40013": "Different PC",
            "41001": "Sending email failed, network exception",
            "41002": "Send email failed, exception",
            "42001": "Incorrect activation code",
            "42003": "General error code. Notify user to send log to technical support staff",
            "42004": "This service cannot be activated online. Activate it offline",
            "42005": "Activation code has expired",
            "42014": "The activation code has been used on other server",
            "42019": "The activation code and server are not in the same region",
            "42035": "The activation code cannot be used in virtual machine",
            "42059": "The server has been prohibited permanently",
            "42999": "License common error code",
            "43001": "Empty activation file",
            "50001": "Device is offline",
            "50002": "Configured event is not supported by device ability set",
            "50006": "Event exists",
            "50011": "Event source does not exist",
            "60000": "Unknown exception",
            "60002": "Failed to get NTP service information",
            "60003": "Failed to get NTP configuration",
            "60004": "Failed to get AD configuration",
            "60005": "Failed to find database restore file",
            "60006": "Failed to get database backup strategy",
            "60007": "Invalid database backup path",
            "60008": "Failed to reset default storage path of database backup",
            "60009": "Failed to update database backup strategy",
            "60011": "ES server is not registered",
            "60012": "VRM server is not registered",
            "60015": "Active registration of this server is not supported",
            "60016": "No WAN information",
            "60017": "No LAN information",
            "60018": "Configuration information of active registration is empty",
            "60019": "Server type is not supported. Registration is not allowed",
            "60020": "Server remote configuration failed",
            "60021": "MES is not registered",
            "60022": "Server is offline or connection timed out",
            "60023": "The content of active registration is invalid",
            "60024": "The corresponding CVR information does not exist",
            "60025": "Only one MES can be added",
            "60026": "Failed to match server domain and MSM domain",
            "60027": "No data in the cache",
            "60028": "No domain information in MSM",
            "60030": "Failed to get corresponding domain network information",
            "60031": "FS server is not registered",
            "60033": "Watchdog is disabled",
            "61001": "Unrecognized request",
            "61002": "Failed to verify requested content",
            "61003": "Configuration failed",
            "61004": "Unknown error",
            "61005": "Server exception",
            "61006": "Business server is removed",
            "61007": "Business server is added",
            "61008": "MES configuration failed",
            "61009": "VAG configuration failed",
            "70001": "MSM is not added to IPM",
            "70002": "Format exception of communication request between IPM and sub systems",
            "70003": "Format exception of communication response between IPM and sub systems",
            "70004": "HTTP request exception",
            "70005": "MSM has been added to other system",
            "70007": "Failed to connect to IPM",
            "70008": "Failed to get TLQ or NTP from IPM",
            "90002": "No permission to export reports in csv file"
        };

        if (errorList[errorStatus.toString()]){
            errorText = "Status " + errorStatus + " : " + errorList[errorStatus.toString()];
        }
        else {
            errorText = "Status " + errorStatus + " : Unknown Error";
        }


        return errorText;
    }
}

export default HikvisionUtility;
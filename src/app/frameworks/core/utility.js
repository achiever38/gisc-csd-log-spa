import i18nextko from "knockout-i18next";
import _ from "lodash";
import {Enums} from "../constant/apiConstant";
import moment from "moment";

/**
 * Static class which provide utility method for system wide.
 * Note: Please keep this class as static class
 * 
 * @class Utility
 */
class Utility {

    /**
     * Generate unqiue identifier in GUID structure.
     * This ID conform to RFC4122 version 4
     * https://tools.ietf.org/html/rfc4122
     * 
     * @static
     * @public
     * @returns {string} GUID in string format
     */
    static getUniqueId() {
        var d = new Date().getTime();
        if (window.performance && typeof window.performance.now === "function") {
            d += performance.now(); //use high-precision timer if available
        }
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;
    }

    /**
     * Extract parameter from given paramKey, if it is undefined return defaultValue
     * 
     * @static
     * @public
     * @param {any} paramKey
     * @param {any} [defaultValue=null]
     * @returns
     */
    static extractParam(paramKey, defaultValue = null) {
        return (paramKey === undefined) ? defaultValue : paramKey;
    }

    /**
     * Extract parameter with posibility to be i18n bound.
     * The i18n bound will prefixed with "i18n:" follow by resource key.
     * 
     * @static
     * @public
     * @param {any} paramKey
     * @param {any} [defaultValue=null]
     * @returns
     */
    static extractPossibleI18NParam(paramKey, defaultValue = null) {
        // Check if it is i18n: token prefix then it requires translation.
        if (paramKey !== undefined) {
            var paramKeyStr = paramKey.toString();
            var checkToken = paramKeyStr.substring(0, 5);
            if (checkToken === "i18n:") {
                var rawParamKey = paramKeyStr.substring(5);
                paramKeyStr = i18nextko.t(rawParamKey)(); // Translate with i18next
            }
        }
        return Utility.extractParam(paramKeyStr, defaultValue);
    }

    /**
     * Create Option Object with optionKey ONLY IF optionValue is existed
     * 
     * @static
     * @public
     * @param {any} optionKey
     * @param {any} optionValue
     * @returns {Object} 
     */
    static createOptionIfParamExist(optionKey, optionValue) {
        if ((optionValue !== undefined) && optionValue) {
            var option = {};
            option[optionKey] = optionValue;
            return option;
        }
        return null;
    }

    /**
     * Aggregate collection from sub-collection by given property name in options.by
     * 
     * @static
     * @public
     * @param {any} options { source: array, by: string }
     * @returns
     */
    static aggregateCollection(options) {
        var source = options.source;
        var byPropertyStr = options.by;

        if ((source === undefined) || (byPropertyStr === undefined)) {
            throw new Error("options.source or options.by is not defined");
        }

        var result = [];
        for (let i = 0; i < source.length; i++) {
            let subSource = source[i][byPropertyStr];
            result.push(...subSource);
        }
        return result;
    }

    /**
     * Ensure given rawData is Number type, with precision default to 2
     * 
     * @static
     * @public
     * @param {any} rawData
     * @param {number} [precision=2]
     * @returns
     */
    static ensureNumberPrecision(rawData, precision = 2) {
        var result = "";
        switch (typeof (rawData)) {
            case "string":
                result = parseFloat(rawData).toFixed(precision);
                break;
            case "number":
                result = rawData.toFixed(precision);
                break;
        }
        return result;
    }

    /**
     * Format string with {0} ordered token same as c#.
     * 
     * @static
     * @public
     * @param {any} format
     * @returns {string}
     */
    static stringFormat(format) {
        var args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined' ? args[number] : 'undefined';
        });
    }

    
    /**
     * Format number with comma as thousand separator.
     * 
     * @static
     * @public
     * @param {any} number
     * @returns format number with comma as thousand separator.
     */
    static numberFormat(number) {
        return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    /**
     * Generate hash code in Int32 from given stringContent
     * 
     * @static
     * @public
     * @param {any} stringContent
     * @returns
     */
    static getHashCode(stringContent) {
        var hash = 0,
            i, chr, len;
        if (stringContent === null || stringContent === undefined || stringContent.length === 0) {
            return hash;
        }
        for (let i = 0, len = stringContent.length; i < len; i++) {
            chr = stringContent.charCodeAt(i);
            hash = ((hash << 5) - hash) + chr;
            hash |= 0;
        }
        return hash;
    }

    /**
     * Generate random string with given length.
     * The default will return alpha-numeric string, you can use optional 
     * "A" or "N" flag.
     * "A" = Alpha flag return random a-Z string.
     * "N" = Numeric flag return random 0-9 string.
     * 
     * @static
     * @param {any} length
     * @param {any} an
     * @returns
     */
    static randomString(length, an) {
        an = an && an.toLowerCase();
        var str = "",
            i = 0,
            min = an == "a" ? 10 : 0,
            max = an == "n" ? 10 : 62;
        for (; i++ < length;) {
            let r = Math.random() * (max - min) + min << 0;
            str += String.fromCharCode(r += r > 9 ? r < 36 ? 55 : 61 : 48);
        }
        return str;
    }
    
    /**
     * Apply InfoStatus on given Object
     * 
     * @public
     * @static
     * @param {any} obj
     * @param {Enums.InfoStatus} infoStatus
     */
    static applyInfoStatus(obj, infoStatus) {
        obj["infoStatus"] = infoStatus;
    }

    /**
     * Generate delta array which apply InfoStatus on each item.
     * 
     * @public
     * @static
     * @param {any} originalArray
     * @param {any} currentArray
     * @param {string} [identityProperty="id"] property to check uniqueness
     * @returns {array} collection of delta 
     */
    static generateArrayWithInfoStatus(originalArray, currentArray, identityProperty = "id") {
        var oArray = [];
        var aArray = [];
        var dArray = [];

        // Find Original, Add
        currentArray.forEach(function(cItem) {
            let match = _.find(originalArray, (oItem) => {
                return cItem[identityProperty] === oItem[identityProperty];
            });
            if(match) {
                Utility.applyInfoStatus(match, Enums.InfoStatus.Original);
                oArray.push(match);
            } else {
                Utility.applyInfoStatus(cItem, Enums.InfoStatus.Add);
                aArray.push(cItem);
            }
        }, this);

        // Find Delete
        originalArray.forEach(function(oItem) {
            let match = _.find(currentArray, (cItem) => {
                return cItem[identityProperty] === oItem[identityProperty];
            });
            if(match) {
                Utility.applyInfoStatus(match, Enums.InfoStatus.Original);
                oArray.push(match);
            } else {
                Utility.applyInfoStatus(oItem, Enums.InfoStatus.Delete);
                dArray.push(oItem);
            } 
        }, this);

        // console.log("-- origianl --");
        // console.log(oArray);
        // console.log("-- added -- ");
        // console.log(aArray);
        // console.log("-- deleted --");
        // console.log(dArray);

        var result = _.uniqBy(oArray.concat(aArray, dArray), identityProperty);

        // console.log("-- RESULT --");
        // console.log(result);

        return result;
    }

    /**
     * Translate server path same as asp.net syntax.
     * 
     * @static
     * @param {string} [serverHostPrefix]
     * @param {string} [serverRelativePath]
     * 
     */
    static resolveUrl(serverHostPrefix, serverRelativePath) {
        serverHostPrefix = serverHostPrefix || "/";
        serverRelativePath = serverRelativePath || "~/";

        // Check server host prefix should be / or http://hostname.com/
        // This logic will ensure do not end with slash.
        if (_.endsWith(serverHostPrefix, "/")) {
            serverHostPrefix = serverHostPrefix.substr(0, serverHostPrefix.length -1);
        }

        // Replace asp.net token with slash.
        if(serverRelativePath.indexOf("~/") !== -1) {
            serverRelativePath = serverRelativePath.replace("~/", "/");
        }

        // Join both segment.
        return serverHostPrefix + serverRelativePath;
    }

    /**
     * Add number of days to currentDate.
     * 
     * @static
     * @param {any} startDate
     * @param {any} days
     * @returns
     * 
     * @memberOf Utility
     */
    static addDays(currentDate, days = 0) {
        var result = new Date(currentDate);
        result.setDate(result.getDate() + days);
        return result;
    }

        /**
         * Minus number of days to currentDate.
         * 
         * @static
         * @param {any} startDate
         * @param {any} days
         * @returns
         * 
         * @memberOf Utility
         */
    static minusDays(currentDate, days = 0) {
        var result = new Date(currentDate);
        result.setDate(result.getDate() - days);
        return result;
    }

        /**
         * Add number of months to currentDate.
         * 
         * @static
         * @param {any} startDate
         * @param {any} month(s)
         * @returns
         * 
         * @memberOf Utility
         */
    static addMonths(currentDate, months = 0) {
        var result = new Date(currentDate);
        result.setMonth(result.getMonth() + months);
        return result;
    }

      /**
          * Minus number of months to currentDate.
          * 
          * @static
          * @param {any} startDate
          * @param {any} month(s)
          * @returns
          * 
          * @memberOf Utility
          */
    static minusMonths(currentDate, months = 0) {
        var result = new Date(currentDate);
        result.setMonth(result.getMonth() - months);
        return result;
    }

        /**
            * Add number of years to currentDate.
            * 
            * @static
            * @param {any} startDate
            * @param {any} year(s)
            * @returns
            * 
            * @memberOf Utility
            */
    static addYears(currentDate, year = 0) {
        var result = new Date(currentDate);
        result.setFullYear(result.getFullYear() + year);
        return result;
    }

        /**
            * Minus number of years to currentDate.
            * 
            * @static
            * @param {any} startDate
            * @param {any} year(s)
            * @returns
            * 
            * @memberOf Utility
            */
    static minusYears(currentDate, year = 0) {
        var result = new Date(currentDate);
        result.setFullYear(result.getFullYear() - year);
        return result;
    }

    /**
     * Add number of minutes to currentDate.
     * 
     * @static
     * @param {any} currentDate
     * @param {number} [minutes=0]
     * @returns
     */
    static addMinutes(currentDate, minutes = 0) {
        return new Date(currentDate.getTime() + minutes * 60000);
    }

    /**
     * Get mime type from file extension
     * 
     * @static
     * @param {string} [extension=""]
     * @returns {string} mime type
     */
    static getMIMEType(extension = "") {
        // cleanup and to lower case.
        if(extension) {
            extension = extension.toLowerCase();

             // Remove dot notation.
            if(extension.indexOf(".") >= 0) {
                extension = extension.replace(/\./g,'');
            }
        }

        // Detect when extension vailable.
        // http://w3schools.sinsixx.com/media/media_mimeref.asp.htm
        switch(extension) {
            case "323"		: return "text/h323";
            case "acx"		: return "application/internet-property-stream";
            case "ai"		: return "application/postscript";
            case "aif"		: return "audio/x-aiff";
            case "aifc"		: return "audio/x-aiff";
            case "aiff"		: return "audio/x-aiff";
            case "asf"		: return "video/x-ms-asf";
            case "asr"		: return "video/x-ms-asf";
            case "asx"		: return "video/x-ms-asf";
            case "au"		: return "audio/basic";
            case "avi"		: return "video/x-msvideo";
            case "axs"	    : return "application/olescript";
            case "bas"		: return "text/plain";
            case "bcpio"	: return "application/x-bcpio";
            case "bin"		: return "application/octet-stream";
            case "bmp"		: return "image/bmp";
            case "c"		: return "text/plain";
            case "cat"		: return "application/vnd.ms-pkiseccat";
            case "cdf"		: return "application/x-cdf";
            case "cer"		: return "application/x-x509-ca-cert";
            case "class"	: return "application/octet-stream";
            case "clp"		: return "application/x-msclip";
            case "cmx"		: return "image/x-cmx";
            case "cod"		: return "image/cis-cod";
            case "cpio"		: return "application/x-cpio";
            case "crd"		: return "application/x-mscardfile";
            case "crl"		: return "application/pkix-crl";
            case "crt"		: return "application/x-x509-ca-cert";
            case "csh"		: return "application/x-csh";
            case "css"		: return "text/css";
            case "dcr"		: return "application/x-director";
            case "der"		: return "application/x-x509-ca-cert";
            case "dir"		: return "application/x-director";
            case "dll"		: return "application/x-msdownload";
            case "dms"		: return "application/octet-stream";
            case "doc"		: return "application/msword";
            case "docx"		: return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            case "dot"		: return "application/msword";
            case "dvi"		: return "application/x-dvi";
            case "dxr"		: return "application/x-director";
            case "eps"		: return "application/postscript";
            case "etx"		: return "text/x-setext";
            case "evy"		: return "application/envoy";
            case "exe"		: return "application/octet-stream";
            case "fif"		: return "application/fractals";
            case "flr"		: return "x-world/x-vrml";
            case "gif"		: return "image/gif";
            case "gtar"		: return "application/x-gtar";
            case "gz"		: return "application/x-gzip";
            case "h"		: return "text/plain";
            case "hdf"		: return "application/x-hdf";
            case "hlp"		: return "application/winhlp";
            case "hqx"		: return "application/mac-binhex40";
            case "hta"		: return "application/hta";
            case "htc"		: return "text/x-component";
            case "htm"		: return "text/html";
            case "html"		: return "text/html";
            case "htt"		: return "text/webviewhtml";
            case "ico"		: return "image/x-icon";
            case "ief"		: return "image/ief";
            case "iii"		: return "application/x-iphone";
            case "ins"		: return "application/x-internet-signup";
            case "isp"		: return "application/x-internet-signup";
            case "jfif"		: return "image/pipeg";
            case "jpe"		: return "image/jpeg";
            case "jpeg"		: return "image/jpeg";
            case "jpg"		: return "image/jpeg";
            case "js"		: return "application/x-javascript";
            case "latex"	: return "application/x-latex";
            case "lha"		: return "application/octet-stream";
            case "lsf"		: return "video/x-la-asf";
            case "lsx"		: return "video/x-la-asf";
            case "lzh"		: return "application/octet-stream";
            case "m13"		: return "application/x-msmediaview";
            case "m14"		: return "application/x-msmediaview";
            case "m3u"		: return "audio/x-mpegurl";
            case "man"		: return "application/x-troff-man";
            case "mdb"		: return "application/x-msaccess";
            case "me"		: return "application/x-troff-me";
            case "mht"		: return "message/rfc822";
            case "mhtml"	: return "message/rfc822";
            case "mid"		: return "audio/mid";
            case "mny"		: return "application/x-msmoney";
            case "mov"		: return "video/quicktime";
            case "movie"	: return "video/x-sgi-movie";
            case "mp2"		: return "video/mpeg";
            case "mp3"		: return "audio/mpeg";
            case "mpa"		: return "video/mpeg";
            case "mpe"		: return "video/mpeg";
            case "mpeg"	    : return "video/mpeg";
            case "mpg"		: return "video/mpeg";
            case "mpp"		: return "application/vnd.ms-project";
            case "mpv2"	    : return "video/mpeg";
            case "ms"		: return "application/x-troff-ms";
            case "mvb"		: return "application/x-msmediaview";
            case "nws"		: return "message/rfc822";
            case "oda"		: return "application/oda";
            case "p10"		: return "application/pkcs10";
            case "p12"		: return "application/x-pkcs12";
            case "p7b"		: return "application/x-pkcs7-certificates";
            case "p7c"		: return "application/x-pkcs7-mime";
            case "p7m"		: return "application/x-pkcs7-mime";
            case "p7r"		: return "application/x-pkcs7-certreqresp";
            case "p7s"		: return "application/x-pkcs7-signature";
            case "pbm"		: return "image/x-portable-bitmap";
            case "pdf"		: return "application/pdf";
            case "pfx"		: return "application/x-pkcs12";
            case "pgm"		: return "image/x-portable-graymap";
            case "pko"		: return "application/ynd.ms-pkipko";
            case "pma"		: return "application/x-perfmon";
            case "pmc"		: return "application/x-perfmon";
            case "pml"		: return "application/x-perfmon";
            case "pmr"		: return "application/x-perfmon";
            case "pmw"		: return "application/x-perfmon";
            case "png"		: return "image/png";
            case "pnm"		: return "image/x-portable-anymap";
            case "pot"		: return "application/vnd.ms-powerpoint";
            case "ppm"		: return "image/x-portable-pixmap";
            case "pps"		: return "application/vnd.ms-powerpoint";
            case "ppt"		: return "application/vnd.ms-powerpoint";
            case "pptx"		: return "application/vnd.openxmlformats-officedocument.presentationml.presentation";
            case "prf"		: return "application/pics-rules";
            case "ps"		: return "application/postscript";
            case "pub"		: return "application/x-mspublisher";
            case "qt"		: return "video/quicktime";
            case "ra"		: return "audio/x-pn-realaudio";
            case "ram"		: return "audio/x-pn-realaudio";
            case "ras"		: return "image/x-cmu-raster";
            case "rgb"		: return "image/x-rgb";
            case "rmi"		: return "audio/mid";
            case "roff"		: return "application/x-troff";
            case "rtf	"	: return "application/rtf";
            case "rtx"		: return "text/richtext";
            case "scd"		: return "application/x-msschedule";
            case "sct"		: return "text/scriptlet";
            case "setpay"	: return "application/set-payment-initiation";
            case "setreg"	: return "application/set-registration-initiation";
            case "sh"		: return "application/x-sh";
            case "shar"		: return "application/x-shar";
            case "sit"		: return "application/x-stuffit";
            case "snd"		: return "audio/basic";
            case "spc"		: return "application/x-pkcs7-certificates";
            case "spl"		: return "application/futuresplash";
            case "src"		: return "application/x-wais-source";
            case "sst"		: return "application/vnd.ms-pkicertstore";
            case "stl"		: return "application/vnd.ms-pkistl";
            case "stm"		: return "text/html";
            case "svg"		: return "image/svg+xml";
            case "sv4cpio"	: return "application/x-sv4cpio";
            case "sv4crc"	: return "application/x-sv4crc";
            case "swf"		: return "application/x-shockwave-flash";
            case "t"		: return "application/x-troff";
            case "tar"		: return "application/x-tar";
            case "tcl"		: return "application/x-tcl";
            case "tex"		: return "application/x-tex";
            case "texi"		: return "application/x-texinfo";
            case "texinfo"	: return "application/x-texinfo";
            case "tgz"		: return "application/x-compressed";
            case "tif"		: return "image/tiff";
            case "tiff"		: return "image/tiff";
            case "tr"		: return "application/x-troff";
            case "trm"		: return "application/x-msterminal";
            case "tsv"		: return "text/tab-separated-values";
            case "txt"		: return "text/plain";
            case "uls"		: return "text/iuls";
            case "ustar"	: return "application/x-ustar";
            case "vcf"		: return "text/x-vcard";
            case "vrml"		: return "x-world/x-vrml";
            case "wav"		: return "audio/x-wav";
            case "wcm"		: return "application/vnd.ms-works";
            case "wdb"		: return "application/vnd.ms-works";
            case "wks"		: return "application/vnd.ms-works";
            case "wmf"		: return "application/x-msmetafile";
            case "wps"		: return "application/vnd.ms-works";
            case "wri"		: return "application/x-mswrite";
            case "wrl"		: return "x-world/x-vrml";
            case "wrz"		: return "x-world/x-vrml";
            case "xaf"		: return "x-world/x-vrml";
            case "xbm"		: return "image/x-xbitmap";
            case "xla"		: return "application/vnd.ms-excel";
            case "xlc"		: return "application/vnd.ms-excel";
            case "xlm"		: return "application/vnd.ms-excel";
            case "xls"		: return "application/vnd.ms-excel";
            case "xlsx"		: return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            case "xlt"		: return "application/vnd.ms-excel";
            case "xlw"		: return "application/vnd.ms-excel";
            case "xof"		: return "x-world/x-vrml";
            case "xpm"		: return "image/x-xpixmap";
            case "xwd"		: return "image/x-xwindowdump";
            case "z"		: return "application/x-compress";
            case "zip"		: return "application/zip";
            default         : return "application/octet-stream";
        }
    }

    /**
     * Padding zero for specific number.
     * 
     * @static
     * @param {any} number
     * @param {any} digits
     * @returns
     */
    static padDigits(number, digits = 4) {
        if(isNaN(number)) {
            return "";
        }
        return Array(Math.max(digits - String(number).length + 1, 0)).join(0) + number;
    }

    /**
     * Create new property as 'formatPropertyName' with given 'format' and 'tokens'
     * which is property name for token (support variable arguments).
     * Finally apply it to 'obj'
     * 
     * @static
     * @param {any} obj
     * @param {any} formatPropertyName
     * @param {any} format
     * @param {any} tokens
     */
    static applyFormattedProperty(obj, formatPropertyName, format, ...tokens) {
        if(_.isNil(formatPropertyName)) throw new Error("formatPropertyName cannot be null");
        if(_.isNil(format)) throw new Error("format cannot be null");

        var mappedTokens = tokens.map(function(token) {
            return obj[token];
        });

        var formattedValue = Utility.stringFormat(format, ...mappedTokens);
        obj[formatPropertyName] = formattedValue;
    }
    
    /**
     * Create new property as 'formatPropertyName' with given 'format' and 'tokens'
     * which is property name for token (support variable arguments).
     * Finally apply it to each item in 'collection'
     * 
     * @static
     * @param {any} collection
     * @param {any} formatPropertyName
     * @param {any} format
     * @param {any} tokens
     */
    static applyFormattedPropertyToCollection(collection, formatPropertyName, format, ...tokens) {
        _.each(collection, function(item) {
            Utility.applyFormattedProperty(item, formatPropertyName, format, ...tokens);
        });
    }

    /**
     * Create new property as 'formatPropertyName' with given 'formatPredicate' and 'tokens'
     * which is property name for token (support variable arguments).
     * 
     * @static
     * @param {any} obj
     * @param {any} formatPropertyName
     * @param {function} formatPredicate
     * @param {any} tokens
     */
    static applyFormattedPropertyPredicate(obj, formatPropertyName, formatPredicate, ...tokens) {
        Utility.applyFormattedProperty(obj, formatPropertyName, formatPredicate(obj), ...tokens);
    }

    /**
     * Check device is iOS
     * 
     * @static
     * @returns
     * 
     */
    static isiOSDevice() {
        return /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    }
    
    /**
     * Capitalize first letter
     * @static
     * @param {any} string
     * @returns {string}
     */
    static toLowerCaseFirstLetter(string) {
        if(_.isString(string)) {
             return string.charAt(0).toLowerCase() + string.slice(1);
        }
        else {
            return "";
        }
    }

    
    /**
     * Get different minutes between 2 datetime
     * 
     * @static
     * @param {any} startDateTime
     * @param {any} endDateTime
     * @returns
     */
    static compareDateTimeAsMinutes(startDateTime, endDateTime) {
        var start = moment(startDateTime);
        var end = moment(endDateTime);
        var duration = moment.duration(end.diff(start));
        var min = duration.asMinutes();

        return min;
    }

    static compareDateAsDays(startDate, endDate) {
        var start = moment(startDate);
        var end = moment(endDate);
        return start.diff(end, "days");
    }

    
    /**
     * Mutates the original datetime (utc) by adding time.
     * @static
     * @param {any} datetime
     * @param {number} value
     * @param {string} [unit='s']
     * @returns
     */
    static addTime(datetime, value, unit = "s") {
        return moment(datetime).add(value, unit).format("YYYY-MM-DD[T]HH:mm:ss");
    }

    /**
     * 
     * Reset time from datetime to T00:00:00
     * @static
     * @param {any} datetime
     * @returns
     */

    static formatDateTime(datetime) {
        return moment(datetime).format("YYYY-MM-DD[T]HH:mm:ss");
    }

    static resetTime(datetime) {
        return moment(datetime).set({'hour': 0, 'minute': 0,'second':0 }).format("YYYY-MM-DD[T]HH:mm:ss");
    }
    static resetEndTime(datetime) {
        return moment(datetime).set({'hour': 23, 'minute': 59,'second':59 }).format("YYYY-MM-DD[T]HH:mm:ss");
    }

    /**
     * Return empty deferred
     * @static
     * @returns
     */
    static emptyDeferred() {
        var dfd = $.Deferred();
        dfd.resolve();
        return dfd;
    }

    /**
     * Get property by expression
     * @static
     * @param {any} parentObject
     * @param {string} bindingExpression
     * @returns
     */
    static getProperty(parentObject, bindingExpression = "") {
        bindingExpression = bindingExpression.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
        bindingExpression = bindingExpression.replace(/^\./, '');           // strip a leading dot
        var a = bindingExpression.split('.');

        for (var i = 0, n = a.length; i < n; ++i) {
            var k = a[i];
            if (k in parentObject) {
                parentObject = parentObject[k];
            } else {
                return;
            }
        }
        return parentObject;
    }

    /**
     * Set property by expression and value.
     * 
     * @static
     * @param {any} parentObject
     * @param {any} bindingExpression
     * @param {any} newValue
     */
    static setProperty(parentObject, bindingExpression, newValue) {
        bindingExpression = bindingExpression.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
        bindingExpression = bindingExpression.replace(/^\./, '');           // strip a leading dot
        var a = bindingExpression.split('.');

        var prevParentObject = null;
        var lastValidKey = null;

        for (var i = 0, n = a.length; i < n; ++i) {
            var k = a[i];
            if (k in parentObject) {
                prevParentObject = parentObject; // Backup obejct before navigate down level.
                lastValidKey = k;
                parentObject = parentObject[k];
            } else {
                return;
            }
        }

        // Assign new value from last
        if(_.isFunction(parentObject)) {
            parentObject(newValue);
        }
        else {
            prevParentObject[lastValidKey] = newValue;
        }
    }

    
    /**
     * 
     * 
     * @static
     * @param {any} value
     * @param {any} decimalPlace
     * @returns
     * 
     * @memberOf Utility
     */
    static trimDecimal(value, decimalPlace){
        if(!isNaN(value)){
            var val = value.toFixed(decimalPlace);
            value = Number(val);
        }
        return  value;
    }

   
    /**
     * 
     * 
     * @static
     * @param {any} value
     * @param {any} timeFormat // String time format 
     * @returns
     * 
     * @memberOf Utility
     */
    static getOnlyTime(value, timeFormat){
        if(!value) return;  
        return moment(value).format(timeFormat);
    }


    static includeSubBU(businessUnitObject, parentId){
        var parentBusinessUnitId = new Array();
        parentBusinessUnitId.push(parseInt(parentId));

        if(businessUnitObject){
            Utility.setIncludeSubBU(businessUnitObject,parentId,parentBusinessUnitId);
            //businessUnitObject.forEach(function(bu) {
            //    if(bu.parentBusinessUnitId == parentId){
            //        parentBusinessUnitId.push(bu.id);
            //    }
            //});
        }
        return parentBusinessUnitId;
    }
    //For IncludeSub BU use for get child BU
    static setIncludeSubBU(buObject,parentId,lstBu){
        let childItem = _.filter(buObject,{'parentBusinessUnitId':parseInt(parentId)});
        //let childItem = businessUnitObject.filter(item => {return item.id == parentId});

        if(childItem.length > 0){
            childItem.forEach(item => 
            {
                lstBu.push(item.id);
                Utility.setIncludeSubBU(buObject,item.id,lstBu);
            });
        }
        return lstBu;
    }


    /**
     * Detect current browser support touch device or not.
     * 
     * @static
     * @returns boolean
     */
    static isTouchDeviceSupported() 
    {
        var isTouchDevice = false;
        if(/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
            isTouchDevice = true;
        }
        return isTouchDevice;
    }

    /**
     * Detect current browser support IOS or not.
     * 
     * @static
     * @return boolean
     */
    static isIOSDevice() {
        var iOS = /iPad|iPhone/.test(navigator.userAgent);
        return iOS;
    }


     /**
     * Make HTML tag <a href "tel:xxxx" />
     * @static
     * @return boolean
     */
    static createDriverNameWithTelephoneTag(name, tel) {
        let node = "<div>";
        if (name != "" && name != null){
            let driverName = name != null ? name : "";
            node += "<span'>" + driverName + "</span>"

            if (tel != null && tel != "" && tel != "-") {
                if (this.isTouchDeviceSupported()) {
                    node += " (<a href='tel:" + tel + "'>" + tel + "</a>)";
                }
                else {
                    //node += "(<a href='tel:" + tel + "'>" + tel + "</a>)";
                    node += "<span> (" + tel + ")</span>";
                }
            }
            ////Hard Code for Test
            //else {
            //    tel = '0986622303';
            //    if (this.isTouchDeviceSupported()) {
            //        node += "(<a href='tel:" + tel + "'>" + tel + "</a>)";
            //    }
            //    else {
            //        //node += "(<a href='tel:" + tel + "'>" + tel + "</a>)";
            //        node += "<span>(" + tel + ")</span>";
            //    }

            //}
        }

        node += "</div>";

        return node;
    }

     /**
     * Convert BB Code from user to HTML Tag
     * 
     * @static
     * @return boolean
     */
    static convertUserTagToHTML(userTag) {
        var me = this;            // stores the object instance
        var token_match = /{[A-Z_]+[0-9]*}/ig;

        // regular expressions for the different bbcode tokens
        var tokens = {
            'URL': '((?:(?:[a-z][a-z\\d+\\-.]*:\\/{2}(?:(?:[a-z0-9\\-._~\\!$&\'*+,;=:@|]+|%[\\dA-F]{2})+|[0-9.]+|\\[[a-z0-9.]+:[a-z0-9.]+:[a-z0-9.:]+\\])(?::\\d*)?(?:\\/(?:[a-z0-9\\-._~\\!$&\'*+,;=:@|]+|%[\\dA-F]{2})*)*(?:\\?(?:[a-z0-9\\-._~\\!$&\'*+,;=:@\\/?|]+|%[\\dA-F]{2})*)?(?:#(?:[a-z0-9\\-._~\\!$&\'*+,;=:@\\/?|]+|%[\\dA-F]{2})*)?)|(?:www\\.(?:[a-z0-9\\-._~\\!$&\'*+,;=:@|]+|%[\\dA-F]{2})+(?::\\d*)?(?:\\/(?:[a-z0-9\\-._~\\!$&\'*+,;=:@|]+|%[\\dA-F]{2})*)*(?:\\?(?:[a-z0-9\\-._~\\!$&\'*+,;=:@\\/?|]+|%[\\dA-F]{2})*)?(?:#(?:[a-z0-9\\-._~\\!$&\'*+,;=:@\\/?|]+|%[\\dA-F]{2})*)?)))',
            'LINK': '([a-z0-9\-\./]+[^"\' ]*)',
            'EMAIL': '((?:[\\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+\.)*(?:[\\w\!\#$\%\'\*\+\-\/\=\?\^\`{\|\}\~]|&)+@(?:(?:(?:(?:(?:[a-z0-9]{1}[a-z0-9\-]{0,62}[a-z0-9]{1})|[a-z])\.)+[a-z]{2,6})|(?:\\d{1,3}\.){3}\\d{1,3}(?:\:\\d{1,5})?))',
            'TEXT': '(.*?)',
            'SIMPLETEXT': '([a-zA-Z0-9-+.,_ ]+)',
            'INTTEXT': '([a-zA-Z0-9-+,_. ]+)',
            'IDENTIFIER': '([a-zA-Z0-9-_]+)',
            'COLOR': '([a-z]+|#[0-9abcdef]+)',
            'NUMBER': '([0-9]+)'
        };

        var bbcode_matches = [];        // matches for bbcode to html
        var html_tpls = [];             // html templates for html to bbcode
        var html_matches = [];          // matches for html to bbcode
        var bbcode_tpls = [];           // bbcode templates for bbcode to html

        var _getRegEx = function (str) {
            var matches = str.match(token_match);
            var nrmatches = matches.length;
            var i = 0;
            var replacement = '';

            if (nrmatches <= 0) {
                return new RegExp(preg_quote(str), 'g');        // no tokens so return the escaped string
            }

            for (; i < nrmatches; i += 1) {
                // Remove {, } and numbers from the token so it can match the
                // keys in tokens
                var token = matches[i].replace(/[{}0-9]/g, '');

                if (tokens[token]) {
                    // Escape everything before the token
                    replacement += preg_quote(str.substr(0, str.indexOf(matches[i]))) + tokens[token];

                    // Remove everything before the end of the token so it can be used
                    // with the next token. Doing this so that parts can be escaped
                    str = str.substr(str.indexOf(matches[i]) + matches[i].length);
                }
            }

            replacement += preg_quote(str);      // add whatever is left to the string

            return new RegExp(replacement, 'gi');
        };

        /**
         * Turns a bbcode template into the replacement form used in regular expressions
         * by turning the tokens in $1, $2, etc.
         */
        var _getTpls = function (str) {
            var matches = str.match(token_match);
            var nrmatches = matches.length;
            var i = 0;
            var replacement = '';
            var positions = {};
            var next_position = 0;

            if (nrmatches <= 0) {
                return str;       // no tokens so return the string
            }

            for (; i < nrmatches; i += 1) {
                // Remove {, } and numbers from the token so it can match the
                // keys in tokens
                var token = matches[i].replace(/[{}0-9]/g, '');
                var position;

                // figure out what $# to use ($1, $2)
                if (positions[matches[i]]) {
                    position = positions[matches[i]];         // if the token already has a position then use that
                } else {
                    // token doesn't have a position so increment the next position
                    // and record this token's position
                    next_position += 1;
                    position = next_position;
                    positions[matches[i]] = position;
                }

                if (tokens[token]) {
                    replacement += str.substr(0, str.indexOf(matches[i])) + '$' + position;
                    str = str.substr(str.indexOf(matches[i]) + matches[i].length);
                }
            }

            replacement += str;

            return replacement;
        };

        /**
         * Adds a bbcode to the list
         */
        me.addBBCode = function (bbcode_match, bbcode_tpl) {
            // add the regular expressions and templates for bbcode to html
            bbcode_matches.push(_getRegEx(bbcode_match));
            html_tpls.push(_getTpls(bbcode_tpl));

            // add the regular expressions and templates for html to bbcode
            html_matches.push(_getRegEx(bbcode_tpl));
            bbcode_tpls.push(_getTpls(bbcode_match));
        };

        /**
         * Turns all of the added bbcodes into html
         */
        me.bbcodeToHtml = function (str) {
            var nrbbcmatches = bbcode_matches.length;
            var i = 0;

            for (; i < nrbbcmatches; i += 1) {
                str = str.replace(bbcode_matches[i], html_tpls[i]);
            }

            return str;
        };

        /**
         * Turns html into bbcode
         */
        me.htmlToBBCode = function (str) {
            var nrhtmlmatches = html_matches.length;
            var i = 0;

            for (; i < nrhtmlmatches; i += 1) {
                str = str.replace(html_matches[i], bbcode_tpls[i]);
            }

            return str;
        }

        /**
         * Quote regular expression characters plus an optional character
         * taken from phpjs.org
         */
        function preg_quote(str, delimiter) {
            return (str + '').replace(new RegExp('[.\\\\+*?\\[\\^\\]$(){}=!<>|:\\' + (delimiter || '') + '-]', 'g'), '\\$&');
        }

        // adds BBCodes and their HTML
        me.addBBCode('[b]{TEXT}[/b]', '<strong>{TEXT}</strong>');
        me.addBBCode('[i]{TEXT}[/i]', '<em style="font-style:italic !important;">{TEXT}</em>');
        me.addBBCode('[u]{TEXT}[/u]', '<span style="text-decoration:underline;">{TEXT}</span>');
        me.addBBCode('[s]{TEXT}[/s]', '<span style="text-decoration:line-through;">{TEXT}</span>');
        me.addBBCode('[url={URL}]{TEXT}[/url]', '<a href="{URL}" title="link" target="_blank">{TEXT}</a>');
        me.addBBCode('[url]{URL}[/url]', '<a href="{URL}" title="link" target="_blank">{URL}</a>');
        me.addBBCode('[url={LINK}]{TEXT}[/url]', '<a href="{LINK}" title="link" target="_blank">{TEXT}</a>');
        me.addBBCode('[url]{LINK}[/url]', '<a href="{LINK}" title="link" target="_blank">{LINK}</a>');
        me.addBBCode('[img={URL} width={NUMBER1} height={NUMBER2}]{TEXT}[/img]', '<img src="{URL}" width="{NUMBER1}" height="{NUMBER2}" alt="{TEXT}" />');
        me.addBBCode('[img]{URL}[/img]', '<img src="{URL}" alt="{URL}" />');
        me.addBBCode('[img={LINK} width={NUMBER1} height={NUMBER2}]{TEXT}[/img]', '<img src="{LINK}" width="{NUMBER1}" height="{NUMBER2}" alt="{TEXT}" />');
        me.addBBCode('[img]{LINK}[/img]', '<img src="{LINK}" alt="{LINK}" />');
        me.addBBCode('[color={COLOR}]{TEXT}[/color]', '<span style="color:{COLOR};">{TEXT}</span>');
        me.addBBCode('[color="{COLOR}"]{TEXT}[/color]', '<span style="color:{COLOR};">{TEXT}</span>');
        me.addBBCode('[c color={COLOR}]{TEXT}[/c]', '<span style="color:{COLOR};">{TEXT}</span>');
        me.addBBCode('[c color="{COLOR}"]{TEXT}[/c]', '<span style="color:{COLOR};">{TEXT}</span>');
        me.addBBCode('[highlight={COLOR}]{TEXT}[/highlight]', '<span style="background-color:{COLOR}">{TEXT}</span>');
        me.addBBCode('[quote="{TEXT1}"]{TEXT2}[/quote]', '<div class="quote"><cite>{TEXT1}</cite><p>{TEXT2}</p></div>');
        me.addBBCode('[quote]{TEXT}[/quote]', '<cite>{TEXT}</cite>');
        me.addBBCode('[blockquote]{TEXT}[/blockquote]', '<blockquote>{TEXT}</blockquote>');

        return me.bbcodeToHtml(userTag);
    }

        /**
     * 
     * Reset time from datetime to T00:00:00
     * @static
     * @param {any} sqmt
     * @param {any} areaUnitType
     * @returns
     */
    static convertAreaUnit(sqmt, areaUnitType) {

        var convertAreaUnit = 0;

        var numberFormat = function (n, decimals) {
            var sep = "."; // Default to period as decimal separator
            decimals = decimals || 0; // Default to 2 decimals

            var formattedNumber = n.toLocaleString().split(sep)[0];
            if (decimals > 0) {
                formattedNumber += sep + n.toFixed(decimals).split(sep)[1];
            }

            return formattedNumber;
        }

        switch (areaUnitType) {
            case Enums.AreaUnit.SquareMetres:
                convertAreaUnit = sqmt;
                break;
            case Enums.AreaUnit.SquareKilometres:
                convertAreaUnit = sqmt / 1000000;
                break;
            case Enums.AreaUnit.ThailandUnit:
                //convertAreaUnit = sqmt;
                //var c = sqmt;
                //convertAreaUnit = "";
                //var rai = 0;
                //var ngan = 0;
                //var waa = 0;

                //rai = sqmt / 1600;

                //if (rai >= 1) {
                //    convertAreaUnit = numberFormat(Math.floor(rai)).toString();

                //    var decimalRai = rai - Math.floor(rai);

          
                //    ngan = decimalRai * 4

                //    if (ngan >= 1) {
                //        convertAreaUnit += ":" + Math.floor(ngan).toString();
                //        var decimalNgan = ngan - Math.floor(ngan);

                //        waa = decimalNgan * 100;
                //        convertAreaUnit += ":" + waa.toFixed(2);
                //    }
                //    else {
                //        convertAreaUnit += ":0";
                //        waa = ngan * 100;
                //        convertAreaUnit += ":" + waa.toFixed(2);
                //    }
                //}
                //else {

                //    convertAreaUnit += "0";

                //    ngan = rai * 4;

                //    if (ngan > 1) {
                //        convertAreaUnit += ":" + Math.floor(ngan).toString();
                //        var decimalNgan = ngan - Math.floor(ngan);

                //        waa = decimalNgan * 100;
                //        convertAreaUnit += ":" + waa.toFixed(2);
                //    }
                //    else {
                //        convertAreaUnit += ":0";
                //        waa = ngan * 100;
                //        convertAreaUnit += ":" + waa.toFixed(2);
                //    }
                //}

                var _m2Km = 1000;
                var _m2ToKm2 = Math.pow(_m2Km, 2);
                var _m2ToRai2 = 1600;
                var _m2ToNgan2 = 400;
                var _m2ToWa2 = 4;

                var rai = 0, ngan = 0, wa = 0;

                //console.log("value m2",sqmt);

                var remaining = sqmt;
                rai = Math.floor(remaining / _m2ToRai2);
                remaining -= (rai * _m2ToRai2);

                if (remaining > 0) {
                    ngan = Math.floor(remaining / _m2ToNgan2);
                    remaining -= (ngan * _m2ToNgan2);
                }

                if (remaining > 0) {
                    wa = Math.floor(remaining / _m2ToWa2);
                    remaining -= (wa * _m2ToWa2);
                }

                convertAreaUnit = rai + ":" + ngan + ":" + wa;

                break;
        }

        return convertAreaUnit;
    }

    static convertMinsToformathDay(mins){
        var calParkTime = "";
        var parkDuration = mins;
        var dayInMinutes = 60 * 24;
        var day = Math.floor(parkDuration / dayInMinutes);
        calParkTime += day <= 9 ? "0"+day : day;
        calParkTime += ":"
        var hr = Math.floor((parkDuration % dayInMinutes) / 60);
        calParkTime += hr <= 9 ? "0"+hr : hr;
        calParkTime += ":"
        var minutes = Math.floor((parkDuration % dayInMinutes) % 60);
        calParkTime += minutes <= 9 ? "0"+minutes : minutes;
        return calParkTime
    }
}

export default Utility;
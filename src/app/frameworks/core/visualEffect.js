import "jquery";
import Singleton from "./singleton";
import Utility from "./utility";

/**
 * Visual Effect handle UI FX.
 * 
 * @class VisualEffect
 */
class VisualEffect {
 
    /**
     * Creates an instance of VisualEffect.
     * 
     * @param {any} params
     */
    constructor(params) {
        this.config = {};
        this.config.enable = Utility.extractParam(params.enable, true);
        this.config.snapBlade = Utility.extractParam(params.snapBlade, {
            duration: 500,
            easingFn: "easeOutExpo" // Easing is not use now - we did not include???
        });
    }
    
    /**
     * Show transition effect "Snap to blade" with target bladeId (DOM's ID)
     * 
     * @param {any} bladeId
     * @param {any} previousBladeIds
     * @returns
     */
    snapToBlade(bladeId, previousBladeIds) {
        if(!this.config.enable) return;

        this._ensureDOMElements();

        // Calculate position x to scroll to
        var posX = 0;
        previousBladeIds.reverse();
        previousBladeIds.forEach(function(id) {
            let $blade = $("#" + id);
            if($blade) {
                posX += $blade.width();
                posX += 1; // border left/right
            }
        }, this);
        // console.log("<< target posX = " + posX);

        this.$mainPanorama.animate({ scrollLeft: posX }, this.config.snapBlade.duration);
    }

    /**
     * Ensure main panorama DOM element is existed to play upon.
     * @private
     */
    _ensureDOMElements() {
        if(this.$mainPanorama === undefined) {
            this.$mainPanorama = $("#main-panorama");
        }
    }

    /**
     * Get instance of VisualEffect 
     * 
     * @public
     * @static
     * @returns
     */
    static getInstance() {
        return Singleton.getInstance("visualFX", new VisualEffect({
            enable: true,
            snapBlade: {
                duration: 500,
                easingFn: "easeOutExpo" // Easing is not use now - we did not include???
            }
        }));
    }
}

export default VisualEffect;
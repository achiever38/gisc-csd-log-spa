import Logger from "./logger";

/**
 * Handle queue process with interval.
 * 
 * @class QueueProcessor
 */
class QueueProcessor {
    constructor(id, fnCallback = null, duration = 30000) {
        this.id = id;
        this.fnCallback = fnCallback;
        this.duration = duration;
        this.timerRef = null;
        this.items = [];
    }

    /**
     * Start timer of current queue.
     */
    start() {
        this.log("started with " + this.duration + " ms.");
        this.timerRef = setTimeout(() => {
            this.process();
        }, this.duration);
    }

    /**
     * Stop timer of current queue.
     */
    stop() {
        clearTimeout(this.timerRef);
    }

    /**
     * Adding new item to queue.
     */
    push(newItem) {
        this.items.push(newItem);
        this.log("has new size " + this.items.length);
    }

    /**
     * Process queue items by registered callback.
     */
    process() {
        // Pull all from buffer then reset item state.
        var processingItems = _.pull(this.items);
        this.items = [];

        if(processingItems.length) {
            this.log("Processing " + processingItems.length + " items.");
            var callbackResult = this.fnCallback(processingItems);

            // If supports deferred object then wait until callback fn complete (always).
            if(callbackResult && callbackResult.always) {
                this.log("Waiting asynchronous callback complete.");
                callbackResult.always(() => {
                    // Start timer after deferred object done.
                    this.start();
                });
            }
            else {
                // No jQuery deferred object return so start immediately.
                this.log("Synchronous callback complete.");
                this.start();
            }
        }
        else {
            this.log("Empty queue.");
            this.start();
        }
    }

    /**
     * Internal logging machanism.
     */
    log(msg) {
        Logger.log(this.id, msg);
    }
}


export default QueueProcessor;
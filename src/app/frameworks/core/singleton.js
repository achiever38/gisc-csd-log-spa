class Singleton {
    constructor() {
        this.registry = {};
    }
    static _initialize() {
        if (!window._singleton) {
            window._singleton = new Singleton();
        }

        return window._singleton.registry;
    }
    static getInstance(key, defaultInstance) {
        var registry = Singleton._initialize();
        if (!registry[key]) {
            registry[key] = defaultInstance;
        }

        return registry[key];
    }
}

export default Singleton;
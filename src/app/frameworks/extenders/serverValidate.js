import ko from 'knockout';

/**
 * Use in conjunction with ko.validation.rules["serverValidate"]
 */
ko.extenders.serverValidate = function(target, option) {
    if(option) {
        // If it is not validatable before, mark as validatable
        // In KO-validation already skip if it is already marks by other validation rules.
        target.extend({ validatable: true });
        target.serverField = option.params;
        target.serverMessage = option.message;
    }
    return target;
};
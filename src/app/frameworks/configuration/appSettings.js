import Utility from "../core/utility";

class AppSettings {
    constructor() {
        // Finding global runtime configuration.
        this._appSettings = window.appSettings;
    }
    get isDebug() {
        return this._appSettings.isDebug;
    }
    get logLevel() {
        return this._appSettings.logLevel;
    }
    get mapUrl() {
        return this._appSettings.mapUrl;
    }
    get workingMapUrl() {
        return this._appSettings.workingMapUrl;
    }
    get apiCoreUrl() {
        return this._appSettings.apiCoreUrl;
    }
    get notificationUrl() {
        return this._appSettings.notificationUrl;
    }
    get apiTrackingCoreUrl() {
        return this._appSettings.apiTrackingCoreUrl;
    }
    get apiTrackingReportUrl() {
        return this._appSettings.apiTrackingReportUrl;
    }
    get apiMapUrl(){
        return this._appSettings.apiMapUrl;
    }
    get uploadUrl() {
        return Utility.resolveUrl(this.apiCoreUrl, "~/api/media/upload");
    }
    get uploadUrlTrackingCore() {
        return Utility.resolveUrl(this.apiTrackingCoreUrl, "~/api/media/upload");
    }
    get maximumUploadSize() {
        return 4; // MBs.
    }
    get rootPath() {
        return this._appSettings.rootPath;
    }
    get signOutUrl() {
        return this._appSettings.signOutUrl;
    }
    get changePasswordUrl() {
        return this._appSettings.changePasswordUrl;
    }
    get accountUpdateUrl() {
        return this._appSettings.accountUpdateUrl;
    }
    get backOfficeUrl() {
        return this._appSettings.backOfficeUrl;
    }
    get companyWorkspaceUrl() {
        return this._appSettings.companyWorkspaceUrl;
    }
    get companyWorkspaceUrlForOwner() {
        return this._appSettings.companyWorkspaceUrlForOwner;
    }
    get companyAdminUrl() {
        return this._appSettings.companyAdminUrl;
    }
    get companyAdminUrlForOwner() {
        return this._appSettings.companyAdminUrlForOwner;
    }
    get authTimeout() {
        return this._appSettings.authTimeout;
    }
    get connectorAPIUrl() {
        return this._appSettings.connectorAPIUrl;
    } 
    get shipmentAutoRefreshTime() {
        return (1000 * 60); // 1min 
    }
    get trackStatusAutoRefreshTime() {
        return (1000 * 60) * 5; // 5min 
    }
    get liveView() {
        return this._appSettings.liveView;
    }
    get eventView() {
        return this._appSettings.eventView;
    }
    get liveView2() {
        return this._appSettings.liveView2;
    }
    get eventView2() {
        return this._appSettings.eventView2;
    }

    get driverdebrief() {
        return this._appSettings.driverdebrief;
    }

    get iqTechSWFObjectUrl() {
        return this._appSettings.iqTechSWFObjectUrl;
    }
    get iqTechLangUrl() {
        return this._appSettings.iqTechLangUrl;
    }

    get encryptKey() {
        return this._appSettings.encryptKey;
    }
    get decryptKey() {
        return this._appSettings.decryptKey;
    }

    get shipmentTimelineDisplayRow() {
        return this._appSettings.shipmentTimelineDisplayRow;
    }

    get shipmentTimelineAutoRefreshMinutes() {
        return this._appSettings.shipmentTimelineAutoRefreshMinutes;
    }

    get productName() {
        return this._appSettings.productName;
    }

    get versionNumber() {
        return this._appSettings.versionNumber;
    }

    get buildNumber() {
        return this._appSettings.buildNumber;
    }

    get liveViewFullMenu() {
        return this._appSettings.liveViewFullMenu;
    }
}

export default AppSettings;

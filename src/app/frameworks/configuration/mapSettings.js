class MapSettings {
    get defaultZoom() { return 17; }
    get fillColor() { return "#d34f4f"; }
    get borderColor() { return "#d34f4f"; }
    get fillOpacity() { return 0.5; }
    get borderOpacity() { return 1.0; }
    get lineColor() { return "#d34f4f"; }
    get lineStyle() { return "solid"; } //Line style (solid / dash / dot)
    get lineWidth() { return 5; }
    get defaultUnit() { return "meters"; } // meters/kilometers
    get defaultRadius() { return 100; }
    get symbolWidth() { return "35"; }
    get symbolHeight() { return "50"; }
    get symbolOffsetX() { return 0; }
    get symbolOffsetY() { return 23; }
    get symbolRotation() { return 0; }
}

export default MapSettings;
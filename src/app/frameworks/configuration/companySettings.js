class CompanySettings {
    constructor () {
        this._languageCode = null;
        this._shortDateFormat = null;
        this._longDateFormat = null;
        this._shortTimeFormat = null;
        this._longTimeFormat = null;
        this._timezone = null;
        this._timezoneName = null;
        this._showPoiCluster = null;
    }
    load (settings) {
        // Load setting from ajax which support same interface
        // Between company and system settings from shell.
        //console.log("setting>>",settings)
        this._languageCode = settings.languageCode;
        this._shortDateFormat = settings.shortDateFormat;
        this._longDateFormat = settings.longDateFormat;
        this._shortTimeFormat = settings.shortTimeFormat;
        this._longTimeFormat = settings.longTimeFormat;
        this._timezone = settings.timezone;
        this._timezoneName = settings.timezoneName;
        this._showPoiCluster = settings.showPoiCluster;
    }
    get languageCode () {
        return this._languageCode;
    }
    set languageCode (value) {
        this._languageCode = value;
    }
    get shortDateFormat () {
        return this._shortDateFormat;
    }
    set shortDateFormat (value) {
        this._shortDateFormat = value;
    }
    get longDateFormat () {
        return this._longDateFormat;
    }
    set longDateFormat (value) {
        this._longDateFormat = value;
    }
    get shortTimeFormat () {
        return this._shortTimeFormat;
    }
    set shortTimeFormat (value) {
        this._shortTimeFormat = value;
    }
    get longTimeFormat () {
        return this._longTimeFormat;
    }
    set longTimeFormat (value) {
        this._longTimeFormat = value;
    }
    get timezone () {
        return this._timezone;
    }
    set timezone (value) {
        this._timezone = value;
    }
    get timezoneName () {
        return this._timezoneName;
    }
    set timezoneName (value) {
        this._timezoneName = value;
    }
    get showPoiCluster() {
        return this._showPoiCluster;
    }
    set showPoiCluster(value) {
        this._showPoiCluster = value;
    }
}

export default CompanySettings;
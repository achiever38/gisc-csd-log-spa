import ko from "knockout";
import { Enums } from "../constant/apiConstant";
import { NotificationIcons, NotificationTypeIcons } from "../constant/svg";
import UIConstants from "../constant/uiConstant";
import DefaultSorting from "../constant/defaultSorting";
import Utility from "../core/utility";

/**
 * Notification item class which be used for view model binding of notification control.
 */
class NotificationItem {
    constructor(id = 0, header = "", subHeader = "", message = "", icon = "svg-error", isUnRead = true, data = null) {
        this.id = id;
        this.header = header;
        this.subHeader = subHeader;
        this.message = message;
        this.icon = icon; // SVG binary
        this.isUnRead = ko.observable(isUnRead);
        this.data = data;
    }
}

/**
 * Main class for track all notifications of current user.
 */
class NotificationSession {
    constructor() {
        this.urgentAlertItemIds = [];
        this.urgentAlertItems = ko.observableArray();
        this.urgentAlertItemsUnreadCount = ko.observable(0);

        this.alertItemIds = [];
        this.alertItems = ko.observableArray();
        this.alertItemsUnreadCount = ko.observable(0);

        //this.announcementItemIds = []; // Using load all approach no need preload ids required.
        this.announcementItems = ko.observableArray();
        this.announcementItemsUnreadCount = ko.observable(0);

        //this.maintenanceItemIds = []; // Using load all approach no need preload ids required.
        this.maintenanceItems = ko.observableArray();
        this.maintenanceItemsUnreadCount = ko.observable(0);
        this.maximumViewPortSize = 99;
    }

    /**
     * Append alert notification to collection from first load or signalr.
     *
     * @param {any} webRequestAlert
     * @param {any} currentCompanyId
     * @param {any} [ids=[]]
     * @param {boolean} [isUrgent=false]
     * @param {boolean} [isFirstLoad=false]
     * @param {boolean} [isAlwaysAppend]
     * @returns jQuery Deferred
     */
    appendAlerts(webRequestAlert, currentCompanyId, ids = [], isUrgent = false, isFirstLoad = false, isAlwaysAppend = false, isIncreaseUnread = false) {
        var dfdLoadAlerts = $.Deferred();

        var alertNotificationFilter = {
            ids: ids,
            companyId: currentCompanyId,
            alertNotificationType: Enums.AlertNotificationType.Web,
            isUrgent: isUrgent,
            sortingColumns: DefaultSorting.Alert
        };

        webRequestAlert.listAlertNotification(alertNotificationFilter)
        .done((result) => {
            // Loop for poupulate all alert vs urgent alert items.
            result.items.forEach((item) => {
                var formattedHeader  = "";
                if(item.vehicleId) {
                    // Vehicle compose object.
                    formattedHeader = Utility.stringFormat("{0} {1} ({2})", item.vehicleBrand, item.vehicleModel, item.vehicleLicense);
                }
                else {
                    // Delivery man mode.
                    if(item.email) {
                        // Display username and email.
                        formattedHeader =  Utility.stringFormat("{0} ({1})", item.username, item.email);
                    }
                    else
                    {
                        // Display username only.
                        formattedHeader = item.username;
                    }
                }

                // Compose new notfication item.
                var newAlertNotificationItem = new NotificationItem (
                    item.id,
                    formattedHeader,
                    item.businessUnitPath,
                    item.message,
                    this.determineAlertIcon(item.alertTypeValue),
                    item.status !== Enums.ModelData.NotificationAction.Read
                );

                // Detect alert type is for urgent or normal alert.
                if (isUrgent) {
                    if(isFirstLoad || isAlwaysAppend) {
                        // Trust BL order.
                        this.urgentAlertItems.push(newAlertNotificationItem);
                    }
                    else {
                        // Append to top of the list by signalr.
                        this.urgentAlertItems.unshift(newAlertNotificationItem);
                    }
                }
                else {
                    if(isFirstLoad || isAlwaysAppend) {
                        // Trust BL order.
                        this.alertItems.push(newAlertNotificationItem);
                    }
                    else {
                        // Append to top of the list by signalr.
                        this.alertItems.unshift(newAlertNotificationItem);
                    }
                    
                }
            });

            // Increase numbers of total unread.
            if(isIncreaseUnread) {
                if (isUrgent) {
                    this.urgentAlertItemsUnreadCount(this.urgentAlertItemsUnreadCount() + result.items.length);
                }
                else {
                    this.alertItemsUnreadCount(this.alertItemsUnreadCount() + result.items.length);
                }
            }

            result.items.map((item, index)=> {
                result.items[index]['alertIcon'] = this.determineAlertIcon(item.alertTypeValue);
            });

            this.alertItems.sound = result.alertSounds

            // Resolve deferred object.
            dfdLoadAlerts.resolve(result);
        })
        .fail(() => {
            dfdLoadAlerts.reject();
        });

        return dfdLoadAlerts;
    }

    /**
     * Append announcement notfication to list.
     * 
     * @param {any} webRequestAnnouncement
     * @param {any} currentCompanyId
     * @param {any} [ids=[]]
     * @param {boolean} [isFirstLoad=false]
     * @returns
     */
    appendAnnouncements(webRequestAnnouncement, currentCompanyId, ids = [], isFirstLoad = false) {
        var dfdLoadAnnouncements = $.Deferred();
        var announcementFilter = {
            companyId: currentCompanyId
        };

        // Load specific ids if available, else load all ids.
        if(ids.length) {
            announcementFilter.ids = ids;
        }

        webRequestAnnouncement.listAnnouncementNotification(announcementFilter)
        .done((result) => {
            result.forEach((item) => {
                // Compose new notfication item.
                var newNotificationItem = new NotificationItem (
                    item.id,
                    null,
                    null,
                    item.message,
                    NotificationTypeIcons.IconAnnouncement,
                    item.status !== Enums.ModelData.NotificationAction.Read
                );

                // Append to list.
                if(isFirstLoad) {
                    this.announcementItems.push(newNotificationItem);
                }
                else {
                    this.announcementItems.unshift(newNotificationItem);
                }

                // Increase unread count.
                if(newNotificationItem.isUnRead()) {
                    this.announcementItemsUnreadCount(this.announcementItemsUnreadCount() + 1);
                }
            });

            dfdLoadAnnouncements.resolve();
        })
        .fail(() => {
            dfdLoadAnnouncements.reject();
        });

        return dfdLoadAnnouncements;
    }

    /**
     * Append maintenance plan notfication to list.
     * 
     * @param {any} webRequestVehicleMaintenancePlan
     * @param {any} currentCompanyId
     * @param {any} [ids=[]]
     * @param {boolean} [isFirstLoad=false]
     * @returns
     * 
     * @memberOf NotificationSession
     */
    appendMaintenances(webRequestVehicleMaintenancePlan, currentCompanyId, ids = [], isFirstLoad = false) {
        var dfdLoadMaintenancePlans = $.Deferred();
        var maintenanceFilter = {
            companyId: currentCompanyId,
            alertNotificationType: Enums.AlertNotificationType.Web
        };

        if(ids.length) {
            maintenanceFilter.ids = ids;
        }

        webRequestVehicleMaintenancePlan.listVehicleMaintenancePlanNotification(maintenanceFilter)
        .done((result) => {
            result.forEach((item) => {
                // Compose new notfication item.
                var formattedHeader  = Utility.stringFormat("{0} {1} ({2})", item.vehicleBrand, item.vehicleModel, item.vehicleLicense);;
                var newNotificationItem = new NotificationItem (
                    item.id,
                    formattedHeader,
                    item.maintenanceStatusText,
                    item.message,
                    NotificationTypeIcons.IconMaintenancePlan,
                    item.status !== Enums.ModelData.NotificationAction.Read,
                    item
                );

                // Append to list.
                if(isFirstLoad) {
                    this.maintenanceItems.push(newNotificationItem);
                }
                else {
                    this.maintenanceItems.unshift(newNotificationItem);
                }

                // Increase unread count.
                if(newNotificationItem.isUnRead()) {
                    this.maintenanceItemsUnreadCount(this.maintenanceItemsUnreadCount() + 1);
                }
            });
            dfdLoadMaintenancePlans.resolve();
        })
        .fail(() => {
            dfdLoadMaintenancePlans.reject();
        });

        return dfdLoadMaintenancePlans;
    }

    /**
     * Dismiss notification by user.
     * 
     * @param {any} webRequestAlert
     * @param {any} webRequestAnnouncement
     * @param {any} webRequestVehicleMaintenancePlan
     * @param {any} currentCompanyId
     * @param {any} notificationType
     * @param {any} ids=null
     */
    dismissNotification(webRequestAlert, webRequestAnnouncement, webRequestVehicleMaintenancePlan, currentCompanyId, notificationType, id) {
        var dismissedIds = [];
        var forceDelete = false;
        var isFirstLoad = false;
        // If no id means dismiss all items in memory.
        if(id !== null) {
            dismissedIds.push(id);
        }
        else {
            // Dismiss from server side because UI doesn't have completed all ids.
            forceDelete = true;
        }

        // Switch by types.
        switch(notificationType) {
            case UIConstants.SignalRNotificationType.UrgentAlert:
                webRequestAlert.updateAlertNotificationStatus({
                    ids: dismissedIds,
                    status: Enums.ModelData.NotificationAction.Hide,
                    companyId: currentCompanyId,
                    isUrgent: true
                }).done(() => {
                    // Remove all items in UI data.
                    if(forceDelete) {
                        this.urgentAlertItems.removeAll();
                        this.urgentAlertItemsUnreadCount(0);
                    }
                    else {
                        this.urgentAlertItems.remove((item) => {
                            var canDelete = _.indexOf(dismissedIds, item.id) >= 0;
                            if(canDelete && item.isUnRead()) {
                                // Reduce unread item if removed unread item.
                                this.urgentAlertItemsUnreadCount(this.urgentAlertItemsUnreadCount() - 1);
                            }
                            return canDelete;
                        });

                        // if current items < view port and unload ids exist then try to append one item.
                        if(this.urgentAlertItemIds.length > 0 &&
                            this.urgentAlertItems().length < this.maximumViewPortSize) {
                                // Fetch one item then append to the list.
                                var nextIdArr = this.unshiftNotificationIds(this.urgentAlertItemIds, 1);
                                this.appendAlerts(webRequestAlert, currentCompanyId, nextIdArr, true, isFirstLoad, true);
                        }
                    }
                });
                break;
            case UIConstants.SignalRNotificationType.Alert:
                webRequestAlert.updateAlertNotificationStatus({
                    ids: dismissedIds,
                    status: Enums.ModelData.NotificationAction.Hide,
                    companyId: currentCompanyId,
                    isUrgent: false
                }).done(() => {
                    // Remove all items in UI data.
                    if(forceDelete) {
                        this.alertItems.removeAll();
                        this.alertItemsUnreadCount(0);
                    }
                    else {
                        this.alertItems.remove((item) => {
                            var canDelete = _.indexOf(dismissedIds, item.id) >= 0;
                            if(canDelete && item.isUnRead()) {
                                // Reduce unread item if removed unread item.
                                this.alertItemsUnreadCount(this.alertItemsUnreadCount() - 1);
                            }
                            return canDelete;
                        });

                        // if current items < view port and unload ids exist then try to append one item.
                        if(this.alertItemIds.length > 0 &&
                            this.alertItems().length < this.maximumViewPortSize) {
                                // Fetch one item then append to the list.
                                var nextIdArr = this.unshiftNotificationIds(this.alertItemIds, 1);
                                this.appendAlerts(webRequestAlert, currentCompanyId, nextIdArr, false, isFirstLoad, true);
                        }
                    }
                });
                break;
            case UIConstants.SignalRNotificationType.Announcement:
                // If force delete then add all items in memory to lists.
                if(forceDelete) {
                    dismissedIds = _.map(this.announcementItems(), "id");
                }

                // Calling service to hide announcements.
                webRequestAnnouncement.updateAnnouncementNotification({
                    ids: dismissedIds,
                    status: Enums.ModelData.NotificationAction.Hide,
                    companyId: currentCompanyId
                }).done(() => {
                    this.announcementItems.remove((item) => {
                        var canDelete = _.indexOf(dismissedIds, item.id) >= 0;
                        if(canDelete && item.isUnRead()) {
                            // Reduce unread item if removed unread item.
                            this.announcementItemsUnreadCount(this.announcementItemsUnreadCount() - 1);
                        }
                        return canDelete;
                    });
                });
                break;
            case UIConstants.SignalRNotificationType.Maintenance:
                if(forceDelete) {
                    dismissedIds = _.map(this.maintenanceItems(), "id");
                }

                // Calling service to hide maintenance plan.
                webRequestVehicleMaintenancePlan.updateVehicleMaintenancePlanNotification({
                    ids: dismissedIds,
                    status: Enums.ModelData.NotificationAction.Hide,
                    companyId: currentCompanyId
                }).done(() => {
                    this.maintenanceItems.remove((item) => {
                        var canDelete = _.indexOf(dismissedIds, item.id) >= 0;
                        if(canDelete && item.isUnRead()) {
                            // Reduce unread item if removed unread item.
                            this.maintenanceItemsUnreadCount(this.maintenanceItemsUnreadCount() - 1);
                        }
                        return canDelete;
                    });
                });
                break;
        }
    }

    /**
     * Update notification reading status.
     * 
     * @param {any} webRequestAlert
     * @param {any} webRequestAnnouncement
     * @param {any} webRequestVehicleMaintenancePlan
     * @param {any} currentCompanyId
     * @param {any} notificationType
     * @param {any} item
     * @return jQuery deferred
     */
    readNotification(webRequestAlert, webRequestAnnouncement, webRequestVehicleMaintenancePlan, currentCompanyId, notificationType, id) {
        var dfdUpdate = $.Deferred();

        switch(notificationType) {
            case UIConstants.SignalRNotificationType.UrgentAlert:
            case UIConstants.SignalRNotificationType.Alert:
                var isUrgent = (notificationType === UIConstants.SignalRNotificationType.UrgentAlert);
                webRequestAlert.updateAlertNotificationStatus({
                    ids: [id],
                    status: Enums.ModelData.NotificationAction.Read,
                    companyId: currentCompanyId,
                    isUrgent: isUrgent
                })
                .done(() => {
                    // Reduce numbers of unread item.
                    if(isUrgent) {
                        this.urgentAlertItemsUnreadCount(this.urgentAlertItemsUnreadCount() - 1);
                    }
                    else {
                        this.alertItemsUnreadCount(this.alertItemsUnreadCount() - 1);
                    }
                })
                .always(() => {
                    dfdUpdate.resolve();
                });
                break;
            case UIConstants.SignalRNotificationType.Announcement:
                webRequestAnnouncement.updateAnnouncementNotification({
                    ids: [id],
                    status: Enums.ModelData.NotificationAction.Read
                })
                .done(() => {
                    this.announcementItemsUnreadCount(this.announcementItemsUnreadCount() - 1);
                })
                .always(() => {
                    dfdUpdate.resolve();
                });
                break;
            case UIConstants.SignalRNotificationType.Maintenance:
                webRequestVehicleMaintenancePlan.updateVehicleMaintenancePlanNotification({
                    ids: [id],
                    status: Enums.ModelData.NotificationAction.Read
                })
                .done(() => {
                    this.maintenanceItemsUnreadCount(this.maintenanceItemsUnreadCount() - 1);
                })
                .always(() => {
                    dfdUpdate.resolve();
                });
                break;
        }

        return dfdUpdate;
    }

    /**
     * Unshift array by id.
     * 
     * @param {any} arr
     * @param {any} number
     * @returns {array}
     */
    unshiftNotificationIds(arr, number) {
        var matchObjects = _.remove(arr, function(n, index) {
            return index < number;
        });

        var idArray = _.map(matchObjects, (o) => { return o.id; });
        return idArray;
    }

    /**
     * Load all notifications by default.
     * 
     * @param {any} webRequestAlert
     * @param {any} webRequestAnnouncement
     * @param {any} webRequestVehicleMaintenancePlan
     * @param {any} currentCompanyId
     * @returns jQuery deferred
     */
    loadStaticNotification(webRequestAlert, webRequestAnnouncement, webRequestVehicleMaintenancePlan, currentCompanyId) {
        var dfdAllItemLoaded = $.Deferred();
        var isFirstLoad = true;

        // Group 1: Find all ids for urgent alerts.
        var dfdLoadUrgentAlerts = $.Deferred();
        webRequestAlert.listAlertNotificationId({
            companyId: currentCompanyId,
            alertNotificationType: Enums.AlertNotificationType.Web,
            isUrgent: true,
            sortingColumns: DefaultSorting.Alert
        })
        .done((result) => {
            // Pouplate unread numbers, all available ids.
            this.urgentAlertItemsUnreadCount(result.totalUnread);
            this.urgentAlertItemIds = _.uniqBy(result.items, 'id');

            if(this.urgentAlertItemIds.items === 0) {
                // Complete urgent alert load.
                dfdLoadUrgentAlerts.resolve();
            }
            else {
                // Populate first screen only.
                var firstPageIds = this.unshiftNotificationIds(this.urgentAlertItemIds, this.maximumViewPortSize);
                this.appendAlerts(webRequestAlert, currentCompanyId, firstPageIds, true, isFirstLoad)
                .done(() => {
                    dfdLoadUrgentAlerts.resolve();
                });
            }
        });

        // Group 2: Find all standard alerts ids.
        var dfdLoadAlerts = $.Deferred();
        webRequestAlert.listAlertNotificationId({
            companyId: currentCompanyId,
            alertNotificationType: Enums.AlertNotificationType.Web,
            isUrgent: false,
            sortingColumns: DefaultSorting.Alert
        })
        .done((result) => {
            // Pouplate unread numbers, all available ids.
            this.alertItemsUnreadCount(result.totalUnread);
            this.alertItemIds = _.uniqBy(result.items, 'id');

            if(this.alertItemIds.length === 0) {
                // Complete urgent alert load.
                dfdLoadUrgentAlerts.resolve();
            }
            else {
                // Populate first screen only.
                var firstPageIds = this.unshiftNotificationIds(this.alertItemIds, this.maximumViewPortSize);
                this.appendAlerts(webRequestAlert, currentCompanyId, firstPageIds, false, isFirstLoad)
                .done(() => {
                    dfdLoadUrgentAlerts.resolve();
                });
            }
        });

        // Group 3: Load all announcement notification.
        var dfdLoadAnnouncements = $.Deferred();
        this.appendAnnouncements(webRequestAnnouncement, currentCompanyId, [], isFirstLoad)
        .always(() => {
            dfdLoadAnnouncements.resolve();
        });

        // Group 4; Load all maintenance plan notification.
        var dfdLoadMaintenancePlans = $.Deferred();
        this.appendMaintenances(webRequestVehicleMaintenancePlan, currentCompanyId, [], isFirstLoad)
        .always(() => {
            dfdLoadMaintenancePlans.resolve();
        });

        // Checking for loading from all groups.
        $.when(dfdLoadUrgentAlerts, dfdLoadAlerts, dfdLoadAnnouncements, dfdLoadMaintenancePlans)
        .done(() => {
            dfdAllItemLoaded.resolve(true);
        })
        .fail(() => {
            dfdAllItemLoaded.resolve(false);
        });

        return dfdAllItemLoaded;
    }

    /**
     * Handle update notification dataset for alert, announcement, maintenance plan.
     * 
     * @param {any} webRequestAlert
     * @param {any} webRequestAnnouncement
     * @param {any} webRequestVehicleMaintenancePlan
     * @param {any} ids
     * @param {any} notiType
     * @return jQuery deferred.
     */
    signalNotification(webRequestAlert, webRequestAnnouncement, webRequestVehicleMaintenancePlan, currentCompanyId, ids, notiType) 
    {
        // Skip empty array signal.
        if(_.isEmpty(ids)) {
            return Utility.emptyDeferred();
        }

        // Switch notification by type from signalr client constant.
        var isFirstLoad = false;
        switch(notiType) {
            case UIConstants.SignalRNotificationType.Alert:
                return this.appendAlerts(webRequestAlert, currentCompanyId, ids, false, isFirstLoad, false, true);
            case UIConstants.SignalRNotificationType.Announcement:
                return this.appendAnnouncements(webRequestAnnouncement, currentCompanyId, ids, isFirstLoad);
            case UIConstants.SignalRNotificationType.Maintenance:
                return this.appendMaintenances(webRequestVehicleMaintenancePlan, currentCompanyId, ids, isFirstLoad);
            case UIConstants.SignalRNotificationType.UrgentAlert:
                return this.appendAlerts(webRequestAlert, currentCompanyId, ids, true, isFirstLoad, false, true);
        }
    }

    /**
     * Determine alert icon in notification item.
     * 
     * @param {any} alertType
     * @return {string}
     */
    determineAlertIcon(alertType) {
        switch(alertType) {
            case Enums.ModelData.AlertType.AccRapid:
                return NotificationIcons.AccRapid;
            case Enums.ModelData.AlertType.AcquiredGPS:
                return NotificationIcons.AcquiredGPS;
            case Enums.ModelData.AlertType.BadGPS:
                return NotificationIcons.BadGPS;
            case Enums.ModelData.AlertType.VehicleBatteryLow:
                return NotificationIcons.VehicleBatteryLow;
            case Enums.ModelData.AlertType.DeviceBatteryLow:
                return NotificationIcons.DeviceBatteryLow;
            case Enums.ModelData.AlertType.DecRapid:
                return NotificationIcons.DecRapid;
            case Enums.ModelData.AlertType.EngineIdle:
                return NotificationIcons.EngineIdle;
            case Enums.ModelData.AlertType.EngineOff:
                return NotificationIcons.EngineOff;
            case Enums.ModelData.AlertType.EngineOn:
                return NotificationIcons.EngineOn;
            case Enums.ModelData.AlertType.GateClose:
                return NotificationIcons.GateClose;
            case Enums.ModelData.AlertType.GateOpen:
                return NotificationIcons.GateOpen;
            case Enums.ModelData.AlertType.GPSDisCnt:
                return NotificationIcons.GPSDisCnt;
            case Enums.ModelData.AlertType.GPSReCnt:
                return NotificationIcons.GPSReCnt;
            case Enums.ModelData.AlertType.Move:
                return NotificationIcons.Move;
            case Enums.ModelData.AlertType.OverSpeed:
                return NotificationIcons.OverSpeed;
            case Enums.ModelData.AlertType.OverTemperature:
                return NotificationIcons.OverTemperature;
            case Enums.ModelData.AlertType.PowerDisCnt:
                return NotificationIcons.PowerDisCnt;
            case Enums.ModelData.AlertType.PowerReCnt:
                return NotificationIcons.PowerReCnt;
            case Enums.ModelData.AlertType.Stop:
                return NotificationIcons.Stop;
            case Enums.ModelData.AlertType.Swerve:
                return NotificationIcons.Swerve;
            case Enums.ModelData.AlertType.UnderTemperature:
                return NotificationIcons.UnderTemperature;
            case Enums.ModelData.AlertType.UnsFuelDec:
                return NotificationIcons.UnsFuelDec;
            case Enums.ModelData.AlertType.UnsStop:
                return NotificationIcons.UnsStop;
            case Enums.ModelData.AlertType.CloseGateInsPOI:
                return NotificationIcons.CloseGateInsPOI;
            case Enums.ModelData.AlertType.CloseGateOtsPOI:
                return NotificationIcons.CloseGateOtsPOI;
            case Enums.ModelData.AlertType.JobAvgSpeed:
                return NotificationIcons.JobAvgSpeed;
            case Enums.ModelData.AlertType.JobTotDist:
                return NotificationIcons.JobTotDist;
            case Enums.ModelData.AlertType.JobTotTime:
                return NotificationIcons.JobTotTime;
            case Enums.ModelData.AlertType.OpenGateInsPOI:
                return NotificationIcons.OpenGateInsPOI;
            case Enums.ModelData.AlertType.OpenGateOtsPOI:
                return NotificationIcons.OpenGateOtsPOI;
            case Enums.ModelData.AlertType.OverTimePark:
                return NotificationIcons.OverTimePark;
            case Enums.ModelData.AlertType.IncmWP:
                return NotificationIcons.IncmWP;
            case Enums.ModelData.AlertType.OutgWP:
                return NotificationIcons.OutgWP;
            case Enums.ModelData.AlertType.Dispatcher:
                return NotificationIcons.Dispatcher;
            case Enums.ModelData.AlertType.Urgent:
                return NotificationIcons.Urgent;
            case Enums.ModelData.AlertType.Maintenance:
                return NotificationIcons.Maintenance;
            case Enums.ModelData.AlertType.SOS:
                return NotificationIcons.SOS;
            case Enums.ModelData.AlertType.Parking:
                return NotificationIcons.Parking;
            case Enums.ModelData.AlertType.POIParking:
                return NotificationIcons.POIParking;
            case Enums.ModelData.AlertType.POIInOut:
                return NotificationIcons.POIInOut;
            case Enums.ModelData.AlertType.AreaParking:
                return NotificationIcons.AreaParking;
            case Enums.ModelData.AlertType.AreaInOutVehicle:
                return NotificationIcons.AreaInOutVehicle;
            case Enums.ModelData.AlertType.AreaInOutJob:
                return NotificationIcons.AreaInOutJob;
            case Enums.ModelData.AlertType.MoveNonWorking:
                return NotificationIcons.MoveNonWorking;
            case Enums.ModelData.AlertType.ExceedLimitDriving:
                return NotificationIcons.ExceedLimitDriving;
            case Enums.ModelData.AlertType.InvalidDriver:
                return NotificationIcons.InvalidDriver;
            case Enums.ModelData.AlertType.CustomAlert:
                return NotificationIcons.CustomAlert;
            case Enums.ModelData.AlertType.ExceedLimitDrivingIn24Hours:
                return NotificationIcons.ExceedLimitDrivingIn24Hours;
            case Enums.ModelData.AlertType.TemperatureError:
                return NotificationIcons.TemperatureError;
            case Enums.ModelData.AlertType.Acceleration:
                return NotificationIcons.Acceleration;
            case Enums.ModelData.AlertType.Breaking:
                return NotificationIcons.Breaking;
            case Enums.ModelData.AlertType.Turn:
                return NotificationIcons.Turn;
            case Enums.ModelData.AlertType.WideTurn:
                return NotificationIcons.WideTurn;
            case Enums.ModelData.AlertType.SwerveWhileAccelerating:
                return NotificationIcons.SwerveWhileAccelerating;
            case Enums.ModelData.AlertType.WideSwerveWhileAccelerating:
                return NotificationIcons.WideSwerveWhileAccelerating;
            case Enums.ModelData.AlertType.SwerveWhileDecelerating:
                return NotificationIcons.SwerveWhileDecelerating;
            case Enums.ModelData.AlertType.WideSwerveWhileDecelerating:
                return NotificationIcons.WideSwerveWhileDecelerating;
            case Enums.ModelData.AlertType.Roundabout:
                return NotificationIcons.Roundabout;
            case Enums.ModelData.AlertType.LaneChange:
                return NotificationIcons.LaneChange;
            case Enums.ModelData.AlertType.Bypassing:
                return NotificationIcons.Bypassing;
            case Enums.ModelData.AlertType.AccidentSuspicious:
                return NotificationIcons.AccidentSuspicious;
            case Enums.ModelData.AlertType.SpeedBump:
                return NotificationIcons.SpeedBump;
            case Enums.ModelData.AlertType.RealAccident:
                return NotificationIcons.RealAccident;
            case Enums.ModelData.AlertType.GreenBandDriving:
                return NotificationIcons.GreenBandDriving;
            case Enums.ModelData.AlertType.OverRPM:
                return NotificationIcons.OverRPM;
            case Enums.ModelData.AlertType.NoSeatBelt:
                return NotificationIcons.NoSeatBelt;
            case Enums.ModelData.AlertType.FCW:
                return NotificationIcons.FCW;
            case Enums.ModelData.AlertType.UFCW:
                return NotificationIcons.UFCW;
            case Enums.ModelData.AlertType.PCW:
                return NotificationIcons.PCW;
            case Enums.ModelData.AlertType.LDW:
                return NotificationIcons.LDW;
            case Enums.ModelData.AlertType.HMW:
                return NotificationIcons.HMW;

            case Enums.ModelData.AlertType.TSR:
                return NotificationIcons.TSR;
            case Enums.ModelData.AlertType.Tow:
                return NotificationIcons.Tow;
            case Enums.ModelData.AlertType.UnauthorizedStop:
                return NotificationIcons.UnauthorizedStop;
            case Enums.ModelData.AlertType.FatigueOther:
                return NotificationIcons.FatigueOther;
            case Enums.ModelData.AlertType.FatigueMicroSleep:
                return NotificationIcons.FatigueMicroSleep;
            case Enums.ModelData.AlertType.FatigueYawning:
                return NotificationIcons.FatigueYawning;
            case Enums.ModelData.AlertType.FatigueDrowsiness:
                return NotificationIcons.FatigueDrowsiness;
            case Enums.ModelData.AlertType.Distraction:
                return NotificationIcons.Distraction;
            case Enums.ModelData.AlertType.FieldOfViewException:
                return NotificationIcons.FieldOfViewException;
        }
    }
}

export default NotificationSession;
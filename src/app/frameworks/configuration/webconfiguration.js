import Singleton from "../core/singleton";
import AppSettings from "./appSettings";
import UserSession from "./userSession";
import CompanySettings from "./companySettings";
import FleetMonitoring from "./fleetMonitoring";
import MapSettings from "./mapSettings";
import NotificationSession from "./notificationSession";
import ReportSession from "./reportSession";

class WebConfiguration {
    static get appSettings(){
        return Singleton.getInstance("appSettings", new AppSettings());
    }
    static get userSession(){
        return Singleton.getInstance("userSession", new UserSession());
    }
    static get companySettings(){
        return Singleton.getInstance("companySettings", new CompanySettings());
    }
    static get fleetMonitoring(){
        return Singleton.getInstance("fleetMonitoring", new FleetMonitoring());
    }
    static get mapSettings(){
        return Singleton.getInstance("mapSettings", new MapSettings());
    }
    static get notificationSession(){
        return Singleton.getInstance("notificationSession", new NotificationSession());
    }
    static get reportSession(){
        return Singleton.getInstance("reportSession", new ReportSession());
    }
}

export default WebConfiguration;

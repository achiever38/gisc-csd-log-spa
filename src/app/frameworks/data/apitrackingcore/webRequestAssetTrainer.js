﻿import 'jquery';
import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";
import { Enums } from "../../constant/apiConstant";

class WebRequestAssetTrainer extends WebRequestBase { 

    constructor(applicationUrl) {
        super(applicationUrl);
    }

    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestAssetMonitoring",
            new WebRequestAssetTrainer(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    listTrainerSummary(filter) {
        return this.ajaxPost('~/api/trainer/summary/list', filter);
    }

    getTrainer(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/trainer/${id}`, includeAssociationNames);
    }

    createTrainer(info, returnResult = false) {
        return this.ajaxPost(`~/api/trainer?returnResult=${returnResult}`, info);
    }

    updateTrainer(info, returnResult = false) {
        return this.ajaxPut(`~/api/trainer?returnResult=${returnResult}`, info);
    }

    deleteTrainer(id) {
        return this.ajaxDelete("~/api/trainer", [id]);
    }

    exportTrainer(filter) {
        return this.ajaxPost('~/api/trainer/export', filter);
    }

    downloadTrainerTemplate(templateFileType = Enums.TemplateFileType.Xlsx) {
        return this.ajaxGet(`~/api/trainer/template/download?templateFileType=${templateFileType}`);
    }

    importTrainer(info, save = false) {
        return this.ajaxPost(`~/api/trainer/import?save=${save}`, info)
    }

    importTrainerPicture(info, save = false) {
        return this.ajaxPost(`~/api/trainer/pictures/import?save=${save}`, info)
    }

    downloadTrainerPictureTemplate () {
        return this.ajaxGet('~/api/trainer/pictures/template/download');
    }
}

export default WebRequestAssetTrainer;
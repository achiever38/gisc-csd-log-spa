import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";
import { Enums } from "../../constant/apiConstant";

/**
 * WebRequest proxy class for vehicle maintenance plan.
 * 
 * @class WebRequestVehicleMaintenancePlanIcon
 * @extends {WebRequestBase}
 */
class WebRequestVehicleMaintenancePlan extends WebRequestBase {

    /**
     * Creates an instance of WebRequestVehicleMaintenancePlan.
     * 
     * @param {any} applicationUrl
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get Singleton object of current service.
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestVehicleMaintenancePlan",
            new WebRequestVehicleMaintenancePlan(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * Find vehicle maintenance plan notification.
     * 
     * @param {any} filter
     * @returns jQuery deferred
     */
    listVehicleMaintenancePlanNotification(filter) {
        return this.ajaxPost("~/api/vehicle/maintenancePlan/notification/list", filter);
    }

    /**
     * Update vehicle maintenance plan notification.
     * 
     * @param {any} filter
     * @returns jQuery deferred
     */
    updateVehicleMaintenancePlanNotification(filter) {
        return this.ajaxPost("~/api/vehicle/maintenancePlan/notification/status", filter);
    }

    /**
     * Find vehicle maintenance plan summary list.
     * 
     * @param {any} filter
     * @returns jQuery deferred
     */
    listVehicleMaintenancePlanSummary(filter) {
        return this.ajaxPost("~/api/vehicle/maintenancePlan/summary/list", filter);
    }

    /**
     * Get vehicle maintenance plan by id
     * @param {any} id
     * @param {any} includeAssociationNames
     * @returns
     */
    getVehicleMaintenancePlan(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/vehicle/maintenancePlan/${id}`, includeAssociationNames);
    }   

    getChecklistViewMaintenancePlan(id) {
        return this.ajaxGet(`~/api/maintenanceSubType/${id}`);
    }
    /**
     * Create Vehicle Maintenance Plan
     * 
     * @param {any} filter
     * @returns
     */
    createVehicleMaintenancePlan(info, returnResult = true) {
        return this.ajaxPost(`~/api/vehicle/maintenancePlan?returnResult=${returnResult}`, info);
    }

    /**
     * Update Vehicle Maintenance Plan
     * 
     * @param {any} info
     * @param {boolean} [returnResult=true]
     * @returns
     */
    updateVehicleMaintenancePlan(info, returnResult = true) {
        return this.ajaxPut(`~/api/vehicle/maintenancePlan?returnResult=${returnResult}`, info);
    }

    updateChecklistMaintenancePlan(info) {
        return this.ajaxPut(`~/api/vehicle/maintenancePlan/checklist`,info);
    }
    /**
     * Delete Vehicle Maintenance Plan
     * 
     * @param {any} id
     * @returns
     */
    deleteVehicleMaintenancePlan(id) {
        return this.ajaxDelete("~/api/vehicle/maintenancePlan", [id]);
    }

 

        //http://110.77.151.187:10095/api/maintenanceSubType/6
    /**
     * Cancel Vehicle Maintenance Plan
     * 
     * @param {any} id
     * @returns
     */
    cancelVehicleMaintenancePlan(info, returnResult = true) {
        return this.ajaxPut(`~/api/vehicle/maintenancePlan/cancel?returnResult=${returnResult}`,info);
    }

    
    /**
     * Complete Vehicle Maintenance Plan
     * 
     * @param {any} info
     * @returns
     */
    completeVehicleMaintenancePlan(info) {
        //return this.ajaxPost("~/api/vehicle/maintenancePlan/complete", info);
        return this.ajaxPut("~/api/vehicle/maintenancePlan/complete", info);
    }

    downloadImportTemplate(templateFileType = Enums.TemplateFileType.Xlsx) {
        return this.ajaxGet(`~/api/vehicle/maintenancePlan/template/download?templateFileType=${templateFileType}`);
    }

    exportMaintenancePlan(filter) {
        return this.ajaxPost("~/api/vehicle/maintenancePlan/export", filter);
    }

    importMaintenancePlan(info,save = false) {
        return this.ajaxPost(`~/api/vehicle/maintenancePlan/import?save=${save}`, info);
    }

}

export default WebRequestVehicleMaintenancePlan;
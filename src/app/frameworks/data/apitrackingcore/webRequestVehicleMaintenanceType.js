﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

/**
 * WebRequest proxy class for vehicle maintenance type.
 * 
 * @class WebRequestVehicleMaintenanceTypeIcon
 * @extends {WebRequestBase}
 */
class WebRequestVehicleMaintenanceType extends WebRequestBase {

    /**
     * Creates an instance of WebRequestVehicleMaintenanceType.
     * 
     * @param {any} applicationUrl
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get Singleton object of current service.
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestVehicleMaintenanceType",
            new WebRequestVehicleMaintenanceType(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * Find vehicle maintenance type
     * 
     * @param {any} filter
     * @returns jQuery deferred
     */
    listVehicleMaintenanceType(filter) {
        return this.ajaxPost("~/api/maintenanceType/summary/list", filter);
    }

    listVehicleMaintenanceSubType(filter) {
        return this.ajaxPost("~/api/maintenanceSubType/summary/list", filter);
    }

    listVehicleMaintenanceSubTypeForTest(filter) {
        return this.ajaxPost("~/api/maintenanceSubType/summary/list", filter);
    }

    createVehicleMaintenanceType(info, returnResult = true) {
        return this.ajaxPost(`~/api/maintenanceType?returnResult=${returnResult}`, info);
    }

    updateVehicleMaintenanceType(info, returnResult = true) {
        return this.ajaxPut(`~/api/maintenanceType?returnResult=${returnResult}`, info);
    }

    getVehicleMaintenanceType(id) {
        return this.ajaxGet(`~/api/maintenanceType/${id}`);
    }

    setObsoleteVehicleMaintenanceType(id) {
        return this.ajaxPost(`~/api/maintenanceType/Obsolete?id=${id}`);
    }

    getVehicleMaintenanceSubType(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/maintenanceSubType/${id}`, includeAssociationNames);
    }

    createVehicleMaintenanceSubType(info, returnResult = true) {
        return this.ajaxPost(`~/api/maintenanceSubType?returnResult=${returnResult}`, info);
    }

    updateVehicleMaintenanceSubType(info, returnResult = true) {
        return this.ajaxPut(`~/api/maintenanceSubType?returnResult=${returnResult}`, info);
    }

    setObsoleteVehicleMaintenanceSubType(id) {
        return this.ajaxPost(`~/api/maintenanceSubType/Obsolete?id=${id}`);
    }

    downloadChecklistTemplate(templateFileType = Enums.TemplateFileType.Xlsx){
        return this.ajaxGet(`~/api/maintenanceType/template/download?templateFileType=${templateFileType}`);
    }
   
   
   
    importChecklist (info) {
        return this.ajaxPost(`~/api/maintenanceType/import`, info);
    }
}

export default WebRequestVehicleMaintenanceType;
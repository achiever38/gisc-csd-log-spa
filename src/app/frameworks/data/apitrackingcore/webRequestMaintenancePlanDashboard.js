﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

class WebRequestMaintenancePlanDashboard extends WebRequestBase {
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestMaintenancePlanDashboard",
            new WebRequestMaintenancePlanDashboard(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    listTOP10VehicleMostCostRanking(filter) {
        return this.ajaxPost("~/api/vehicle/maintenancePlan/toptenvehicle", filter);
    }

    listNext30DaysActivity(filter) {
        return this.ajaxPost("~/api/vehicle/maintenancePlan/nextdayactivity", filter);
    }

    listnext30DayMaType(filter) {
        return this.ajaxPost("~/api/vehicle/maintenancePlan/nextdaymaintenancetype", filter);
    }

    listTOP10BusinessUnitMostCostRanking(filter) {
        return this.ajaxPost("~/api/vehicle/maintenancePlan/toptenbusinessunit", filter);
    }

    listNext7DaysAppointment(filter) {
        return this.ajaxPost("~/api/vehicle/maintenancePlan/nextdayappointment", filter);
    }

    listDashboard(filter) {
        return this.ajaxPost("~/api/vehicle/maintenancePlan/dashBoard", filter);
    }
}

export default WebRequestMaintenancePlanDashboard;
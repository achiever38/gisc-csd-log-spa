import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

class WebRequestAlertType extends WebRequestBase {

    /**
     * Creates an instance of WebRequestAlertType.
     * @private
     * @param {any} applicationUrl
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of WebRequestAlertType
     * 
     * @static
     * @returns instance of WebRequestAlertType
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestAlertType",
            new WebRequestAlertType(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * Find alert types.
     * 
     * @param {any} filter
     * @returns
     */
    listAlertType(filter) {
        return this.ajaxPost("~/api/alertType/list", filter);
    }

    /**
     * Find alert types by alert configuration.
     * 
     * @param {any} filter
     * @returns
     */
    listAlertTypeByAlertConfig(filter) {
        return this.ajaxPost("~/api/alertType/listByAlertConfig", filter);
    }
}

export default WebRequestAlertType;
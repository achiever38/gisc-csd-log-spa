import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebRequestADAS
 * @extends {WebRequest}
 */
class WebRequestADASModel extends WebRequestBase {

    /**
     * Creates an instance of WebRequestADAS.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request user.
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestADAS
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-WebRequestADASModel",
            new WebRequestADASModel(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    createADASModel(filter, returnResult = true) {
        return this.ajaxPost(`~/api/ADASModel?returnResult=${returnResult}`, filter);
    }
    updateADASModel(filter, returnResult = true) {
        return this.ajaxPut(`~/api/ADASModel?returnResult=${returnResult}`, filter);
    }

    deleteADASModel(id) {
        return this.ajaxDelete("~/api/ADASModel", [id]);
    }
    /**
     * List ADASModel Summary.
     * 
     * @memberOf WebRequestADAS
     */
    listADASModelSummary(filter) {
        return this.ajaxPost("~/api/ADASModel/summary/list", filter);
    }

    getADASModel(id, includeAssociationNames = []) {
        let data = { 
            includeAssociationNames: includeAssociationNames
        };
        return this.ajaxGet(`~/api/ADASModel/${id}`, data);
    }

    getADASBrand() {
        return this.ajaxPost("~/api/ADASModel/getBrand");
    }

}

export default WebRequestADASModel;
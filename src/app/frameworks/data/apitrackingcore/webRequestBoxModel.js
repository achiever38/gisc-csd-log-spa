import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";
import {EntityAssociation} from "../../constant/apiConstant";

class WebRequestBoxModel extends WebRequestBase {

    /**
     * Creates an instance of WebRequestBoxModel.
     * @private
     * @param {any} applicationUrl
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of WebRequestBoxModel
     * 
     * @static
     * @returns instance of WebRequestBoxModel
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-WebRequestBoxModel",
            new WebRequestBoxModel(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }
    
    /**
     * Get list of box model in summary format 
     * 
     * @param {any} filter
     * @returns
     */
    listBoxModel(filter) {
        return this.ajaxPost("~/api/boxModel/list", filter);
    }

    /**
     * Get list of box model in summary format 
     * 
     * @param {any} filter
     * @returns
     */
    listBoxModelSummary(filter) {
        return this.ajaxPost("~/api/boxModel/summary/list", filter);
    }

    /**
     * Get specific box model.
     * 
     * @param {any} id
     */
    getBoxModel(id) {
        var data = { 
            includeAssociationNames: [
                EntityAssociation.BoxModel.Image,
                EntityAssociation.BoxModel.Attachment
            ]
        };
        return this.ajaxGet(`~/api/boxModel/${id}`, data);
    }

    /**
     * Create Box Model.
     * 
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     */
    createBoxModel(info, returnResult = false) {
        return this.ajaxPost(`~/api/boxModel?returnResult=${returnResult}`, info);
    }

    /**
     * Update Box Model.
     * 
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     */
    updateBoxModel(info, returnResult = false) {
        return this.ajaxPut(`~/api/boxModel?returnResult=${returnResult}`, info);
    }

    /**
     * Delete Box Model by Ids.
     * 
     * @param {any} id
     * @returns
     */
    deleteBoxModel(id) {
        return this.ajaxDelete("~/api/boxModel", [id]);
    }
}

export default WebRequestBoxModel;
import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebRequestADAS
 * @extends {WebRequest}
 */
class WebRequestADAS extends WebRequestBase {

    /**
     * Creates an instance of WebRequestADAS.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request user.
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestADAS
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-WebRequestADAS",
            new WebRequestADAS(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    createADAS(filter, returnResult = true) {
        return this.ajaxPost(`~/api/ADAS?returnResult=${returnResult}`, filter);
    }
    updateADAS(filter, returnResult = true) {
        return this.ajaxPut(`~/api/ADAS?returnResult=${returnResult}`, filter);
    }

    deleteADAS(id) {
        return this.ajaxDelete("~/api/ADAS", [id]);
    }
    /**
     * List ADAS Summary.
     * 
     * @memberOf WebRequestADAS
     */
    listADASSummary(filter) {
        return this.ajaxPost("~/api/ADAS/summary/list", filter);
    }

    /**
     * List Model ADAS Summary.
     * 
     * @memberOf WebRequestADAS
     */

    //listADASModel(filter) {
    //    return this.ajaxPost(`~/api/ADASModel/summary/list`, filter);
    //}

    getADAS(id, includeAssociationNames = []) {
        let data = { 
            includeAssociationNames: includeAssociationNames
        };
        return this.ajaxGet(`~/api/ADAS/${id}`, data);
    }

    // Check Using Device
    CheckUsingDevice(filter) {
        return this.ajaxPost(`~/api/ADAS/checkUsingDevice`, filter);
    }

    checkExistingDevice(filter) {
        return this.ajaxPost(`~/api/ADAS/CheckExistingDevice`, filter);
    }
    checkMoveDevice(filter){
        return this.ajaxPost(`~/api/ADAS/checkMoveDevice`, filter);
    }
}

export default WebRequestADAS;
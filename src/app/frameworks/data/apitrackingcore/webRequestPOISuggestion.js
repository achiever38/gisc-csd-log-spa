﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /WebRequestPOISuggestionTemplate 
 * 
 * @class WebRequestPOISuggestionTemplate
 * @extends {WebRequestBase}
 */
class WebRequestPOISuggestionTemplate extends WebRequestBase {

    /**
     * Creates an instance of WebRequestPOISuggestionTemplate.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request reportTemplate.
     * 
     * @static
     * @returns instance of WebRequestPOISuggestionTemplate
     */
    static getInstance() {
        return Singleton.getInstance("trackingCore-WebRequestPOISuggestionTemplate",
            new WebRequestPOISuggestionTemplate(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }
    /**
     * Get list of reportTemplate.
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf webRequestReportTemplate
     */
    summaryList(filter) {
       return this.ajaxPost("~/api/CustomPOISuggest/FindSummary", filter);
    }
    /**
     * Get viewOnMapData by id
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf WebRequestPOISuggestionTemplate
     */
    viewOnMapData(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/CustomPOISuggest/viewonmap/${id}`, includeAssociationNames);
    }

    createCustomPOI(filter) {
        return this.ajaxPost("~/api/CustomPOISuggest/Create", filter);
    }

    deletePOISuggestion(id) {
        return this.ajaxDelete("~/api/CustomPOISuggest", [id]);
    }

    exportPOISuggestion(filter) {
        return this.ajaxPost("~/api/CustomPOISuggest/export", filter);
    }
}

export default WebRequestPOISuggestionTemplate;
import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

class WebRequestBoxPackageType extends WebRequestBase {

    /**
     * Creates an instance of WebRequestBoxPackageType.
     * @private
     * @param {any} applicationUrl
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of WebRequestBoxPackageType
     * 
     * @static
     * @returns instance of WebRequestBoxPackageType
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-WebRequestBoxPackageType",
            new WebRequestBoxPackageType(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * Get list of box model in summary format 
     * 
     * @param {any} filter
     * @returns
     */
    listBoxPackageType(filter) {
        return this.ajaxPost("~/api/boxPackageType/list", filter);
    }
}

export default WebRequestBoxPackageType;
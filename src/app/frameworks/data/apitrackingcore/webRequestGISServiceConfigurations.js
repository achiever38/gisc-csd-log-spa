﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";


/**
 * Facade to access Core Web API in /map
 * 
 * @class GisService
 * @extends {WebRequestBase}
 */
class WebRequestGISServiceConfigurations extends WebRequestBase {

    /**
     * Creates an instance of GisService.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request map.
     * 
     * @static
     * @returns instance of GisService
     */
    static getInstance() {
        return Singleton.getInstance("trackingcore-WebRequestGISServiceConfigurations",
            new WebRequestGISServiceConfigurations(WebConfig.appSettings.apiMapUrl)
        );
    }

    /**
     * List GisService
     * @param {any} filter
     * @returns
     */
    gisServiceList(filter) {
        return this.ajaxPost("~/api/gisconfiguration/summary/list", filter);
    }

    /**
     * Get GisService
     * @param {any} id
     * @param {any} includeAssociationNames
     * @returns
     */
    getGisService(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/gisconfiguration/${id}`, includeAssociationNames);
    }
    /**
     * Create GisService
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     */
    createGisService(info, returnResult = false) {
        return this.ajaxPost(`~/api/gisconfiguration?returnResult=${returnResult}`, info);
    }
    /**
     * Update GisService
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     */
    updateGisService(info, returnResult = false) {
        return this.ajaxPut(`~/api/gisconfiguration?returnResult=${returnResult}`, info);
    }
    /**
    * Delete GisService
    * @param {any} id
    * @returns
    */
    deleteGisService(id) {
        return this.ajaxDelete("~/api/gisconfiguration", [id]);
    }


}

export default WebRequestGISServiceConfigurations;
import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

class WebRequestBoxStock extends WebRequestBase {

    /**
     * Creates an instance of WebRequestBoxStock.
     * @private
     * @param {any} applicationUrl
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of WebRequestBoxStock
     * 
     * @static
     * @returns instance of WebRequestBoxStock
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-WebrequestBoxStock",
            new WebRequestBoxStock(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }
    
    /**
     * Get list of boxStock in summary format 
     * 
     * @param {any} filter
     * @returns
     */
    listBoxStockSummary(filter) {
        return this.ajaxPost("~/api/boxStock/summary/list", filter);
    }

    /**
     * Get list of boxStock 
     * 
     * @param {any} filter
     * @returns
     */
    listBoxStock(filter) {
        return this.ajaxPost("~/api/boxStock/list", filter);
    }

    /**
     * Create boxStock. 
     * 
     * @param {any} filter
     * @returns
     */
    createBoxStock(info, returnResult = true) {
        return this.ajaxPost(`~/api/boxStock?returnResult=${returnResult}`, info);
    }

    /**
     * 
     * @param {any} id
     * @param {any} includeAssociationNames
     * @returns
     * 
     * @memberOf WebrequestBoxStock
     */
    getBoxStock(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/boxStock/${id}`, includeAssociationNames);
    }

     /**
     * Update Box-Stock
     * 
     * @param {any} info
     * @param {boolean} [returnResult=true]
     * @returns
     * 
     * @memberOf WebrequestBoxStock
     */
    updateBoxStock (info, returnResult = true) {
        return this.ajaxPut(`~/api/boxStock?returnResult=${returnResult}`, info);
    }

     /**
     * Delete Box-Stock
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf WebrequestBoxStock
     */
    deleteBoxStock (id) {
        return this.ajaxDelete("~/api/boxStock", [id]);
    }
}

export default WebRequestBoxStock;
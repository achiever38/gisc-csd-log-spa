﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";
import {EntityAssociation} from "../../constant/apiConstant";

/**
 * Facade to access Core Web API in /CustomArea
 * 
 * @class WebReqestCustomArea
 * @extends {WebRequestBase}
 */
class WebReqestCustomArea extends WebRequestBase {

    /**
     * Creates an instance of WebReqestCustomArea.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request CustomArea.
     * 
     * @static
     * @returns instance of WebReqestCustomArea
     */
    static getInstance() {
        return Singleton.getInstance("trackingcore-webrequestCustomArea",
            new WebReqestCustomArea(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * Get list of customArea in summary format.
     * 
     * @param {any} filter
     * @returns jQuery deferred.
     */
    listCustomAreaSummary(filter) {
        return this.ajaxPost("~/api/customArea/summary/list", filter);
    }

    customAreaType(){
        return this.ajaxPost("~/api/customAreaType/list");
    }

    /**
     * Get customArea by id
     * 
     * @param {any} id, customAreaId
     * @returns jQuery deferred.
     */
    getCustomArea(id) {
        var data = { 
            includeAssociationNames: [
                EntityAssociation.CustomArea.Category,
                EntityAssociation.CustomArea.Type,
                EntityAssociation.CustomArea.AccessibleBusinessUnits
            ]
        };
        return this.ajaxGet(`~/api/customArea/${id}`, data);
    }
    
    /**
     * Create CustomArea with given customAreaModel
     * 
     * @param {any} customAreaModel
     * @param {boolean} [returnResult=true]
     * @returns jQuery deferred.
     */
    createCustomArea(customAreaModel, returnResult = true) {
        return this.ajaxPost(`~/api/customArea?returnResult=${returnResult}`, customAreaModel);
    }

        /**
         * Update customArea with given customAreaModel
         * 
         * @param {any} customAreaModel
         * @param {boolean} [returnResult=true]
         * @returns jQuery deferred.
         */
        updateCustomArea(customAreaModel, returnResult = true) {
            return this.ajaxPut(`~/api/customArea?returnResult=${returnResult}`, customAreaModel);
        }

        /**
         * Delete an CustomArea 
         * 
         * @param {any} id
         * @returns
         */
        deleteCustomArea(id) {
            return this.ajaxDelete(`~/api/customArea`, [id]);
        }
        /**
        * Export CustomArea
        * @param {any} filter
        * @returns
        */
        exportCustomArea (filter) {
            return this.ajaxPost(`~/api/customArea/export`, filter);
        }

        getSubArea(id){
            return this.ajaxGet(`~/api/customArea/GetByPOI/${id}`);
        }
    }

    export default WebReqestCustomArea;
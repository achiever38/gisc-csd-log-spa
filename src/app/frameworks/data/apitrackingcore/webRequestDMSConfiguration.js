import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebRequestADAS
 * @extends {WebRequest}
 */
class WebRequestDMSConfiguration extends WebRequestBase {

    /**
     * Creates an instance of WebRequestADAS.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request user.
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestDMSConfiguration
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-WebRequestDMSConfiguration",
            new WebRequestDMSConfiguration(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

   
    /**
     * List DMS Configuration Summary.
     * 
     * @memberOf WebRequestDMSConfiguration
     */
    listDMSConfigSummary(filter) {
        return this.ajaxPost("~/api/DMSConfiguration/summary/list", filter);
    }


    /**
     * Create DMS Configuration .
     * 
     * @memberOf WebRequestDMSConfiguration
     */
    createDMSConfig(filter, returnResult = true) {
        return this.ajaxPost(`~/api/DMSConfiguration?returnResult=${returnResult}`, filter);
    }

    /**
     * Update DMS Configuration .
     * 
     * @memberOf WebRequestDMSConfiguration
     */
    updateDMSConfig(filter, returnResult = true) {
        return this.ajaxPut(`~/api/DMSConfiguration?returnResult=${returnResult}`, filter);
    }

    /**
     * Delete an DMS Configuration
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf WebRequestDMSConfiguration
     */
    deleteDMSConfig(id) {
        return this.ajaxDelete("~/api/DMSConfiguration", [id]);
    }


    /**
    * Get DMSModel by id
    * 
    * @param {any} id
    * @returns
    * 
    * @memberOf WebRequestDMSModel
    */
    getDMSConfig(id, includeAssociationNames = []) {
        let data = {
            includeAssociationNames: includeAssociationNames
        };
        return this.ajaxGet(`~/api/DMSConfiguration/${id}`, data);
    }
}

export default WebRequestDMSConfiguration;
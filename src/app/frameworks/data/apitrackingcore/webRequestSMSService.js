import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

/**
 * Facade to access Core Web API in /SMS Service 
 * 
 * @class WebRequestSMSService
 * @extends {WebRequest}
 */
class WebRequestSMSService extends WebRequestBase {

    /**
     * Creates an instance of WebRequestSMSService.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request smsService.
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestSMSService
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestSMSService",
            new WebRequestSMSService(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * Get list of smsService
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf webRequestSMSService
     */
    listSMSService(filter) {
        return this.ajaxPost("~/api/smsService/list", filter);
    }

    /**
     * Get smsService by id
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf webRequestSMSService
     */
    getSMSService(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/smsService/${id}`, includeAssociationNames);
    }

    /**
     * Create smsService with given smsService info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=true]
     * @returns
     * 
     * @memberOf webRequestSMSService
     */
    createSMSService(info, returnResult = true) {
        return this.ajaxPost(`~/api/smsService?returnResult=${returnResult}`, info);
    }

    /**
     * Update smsService with given smsService info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     * 
     * @memberOf webRequestSMSService
     */
    updateSMSService(info, returnResult = false) {
        return this.ajaxPut(`~/api/smsService?returnResult=${returnResult}`, info);
    }

    /**
     * Delete an smsService
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf webRequestSMSService
     */
    deleteSMSService(id) {
        return this.ajaxDelete("~/api/smsService", [id]);
    }
}

export default WebRequestSMSService;
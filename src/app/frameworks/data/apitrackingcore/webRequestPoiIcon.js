﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * 
 * 
 * @class WebReqesPoiIcon
 * @extends {WebRequestBase}
 */
class WebReqesPoiIcon extends WebRequestBase {

    /**
     * Creates an instance of WebReqesPoiIcon.
     * 
     * @param {any} applicationUrl
     * 
     * @memberOf WebReqesPoiIcon
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * 
     * 
     * @static
     * @returns
     * 
     * @memberOf WebReqesPoiIcon
     */
    static getInstance() {
        return Singleton.getInstance("trackingcore-webrequestPoiIcon",
            new WebReqesPoiIcon(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }


    /**
     * 
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf WebReqesPoiIcon
     */
    listPoiIconSummary(filter) {
        return this.ajaxPost("~/api/poiIcon/summary/list", filter);
    }

    /**
     * 
     * 
     * @param {any} info
     * @param {boolean} [returnResult=true]
     * @returns
     * 
     * @memberOf WebReqesPoiIcon
     */
    createPoiIcon(info, returnResult = true) {
        return this.ajaxPost(`~/api/poiIcon?returnResult=${returnResult}`, info);
    }

    /**
     * 
     * 
     * @param {any} info
     * @param {boolean} [returnResult=true]
     * @returns
     * 
     * @memberOf WebReqesPoiIcon
     */
    updatePoiIcon(info, returnResult = true) {
        return this.ajaxPut(`~/api/poiIcon?returnResult=${returnResult}`, info);
    }

    /**
     * 
     * 
     * @param {any} id
     * @param {any} includeAssociationNames
     * @returns
     * 
     * @memberOf WebReqesPoiIcon
     */
    getPoiIcon(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/poiIcon/${id}`, includeAssociationNames);
    }

    /**
     * 
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf WebReqesPoiIcon
     */
    deletePoiIcon (id) {
        return this.ajaxDelete("~/api/poiIcon", [id]);
    }


}

export default WebReqesPoiIcon;
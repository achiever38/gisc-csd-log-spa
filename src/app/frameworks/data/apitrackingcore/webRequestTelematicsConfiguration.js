﻿import WebConfig from "../../configuration/webconfiguration";
import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebRequestUser
 * @extends {WebRequest}
 */
class WebRequestTelematicsConfiguration extends WebRequestBase {

    /**
     * Creates an instance of WebRequestTelematicsConfiguration.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestTelematicsConfiguration",
            new WebRequestTelematicsConfiguration(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

   /**
   * Update TelematicsConfiguration
   * 
   * @param {any} info
   * @returns
   * 
   * @memberOf WebRequestTelematicsConfiguration
   */
    updateTelematicsConfiguration(info,returnResult = false) {
        return this.ajaxPut(`~/api/telematicsConfiguration?returnResult=${returnResult}`, info);
    }

    /**
   * Get TelematicsConfiguration
   * 
   * @param {any} info
   * @returns
   * 
   * @memberOf WebRequestTelematicsConfiguration
   */
  getTelematicsConfiguration() {
    return this.ajaxGet("~/api/telematicsConfiguration");
}
    

}
export default WebRequestTelematicsConfiguration;
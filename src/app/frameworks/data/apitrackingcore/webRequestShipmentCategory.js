﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";
import { Enums } from "../../constant/apiConstant";

/**
 * 
 * 
 * @class WebRequestShipmentCategory
 * @extends {WebRequestBase}
 */
class WebRequestShipmentCategory extends WebRequestBase {

    /**
     * Creates an instance of WebRequestShipmentCategory.
     * 
     * @param {any} applicationUrl
     * 
     * @memberOf WebRequestShipmentCategory
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * 
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestShipmentCategory
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-WebRequestShipmentCategory",
            new WebRequestShipmentCategory(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * List summary of Shipment Category
     * @param {any} filter
     * @returns
     */
    listShipmentCategory(filter) {
        return this.ajaxPost("~/api/ShipmentCategory/summary/list", filter);
    }

    /**
     * Get Shipment Category
     * @param {any} filter
     * @returns
     */
    getShipmentCategory(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/ShipmentCategory/${id}`, includeAssociationNames);
    }

    /**
     * Create Shipment Category
     * @param {any} filter
     * @returns
     */
    createShipmentCategory(filter, returnResult = false) {
        return this.ajaxPost(`~/api/ShipmentCategory?returnResult=${returnResult}`, filter)
    }

    /**
     * Update Shipment Category
     * @param {any} filter
     * @returns
     */
    updateShipmentCategory(filter, returnResult = false) {
        return this.ajaxPut(`~/api/ShipmentCategory?returnResult=${returnResult}`, filter)
    }

    /**
     * Delete Shipment Category
     * @param {any} filter
     * @returns
     */
    deleteShipmentCategory(id) {
        let filter = null;
        if(id.length){
            filter = id;
        }else{
            filter = [id];
        }
        return this.ajaxDelete("~/api/ShipmentCategory", filter);
    }

    

}

export default WebRequestShipmentCategory;
﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /Language 
 * 
 * @class WebRequestLanguage
 * @extends {WebRequestBase}
 */
class WebRequestAutoMailReport extends WebRequestBase {

    /**
     * Creates an instance of WebRequestLanguage.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request language.
     * 
     * @static
     * @returns instance of WebRequestLanguage
     */
    static getInstance() {
        return Singleton.getInstance(
            "apicore-webRequestAutoMailReport",
            new WebRequestAutoMailReport(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * Get list of MailConfig.
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf WebRequestAutoMailReport
     */
   
    checkDupicate(filter) {
        return this.ajaxPost("~/api/AutoReport/CheckDupicate", filter);
    }

    createAutoReport(filter){
        return this.ajaxPost("~/api/AutoReport", filter);
    }

    listAutoReport(filter){
        return this.ajaxPost("~/api/AutoReport/summary/list", filter);
    }

    deleteAutoReport(id){
        return this.ajaxDelete ("~/api/AutoReport",id);
    }

    getAutoReport(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/AutoReport/${id}`, includeAssociationNames);
    }

    updateAutoReport(filter){
        return this.ajaxPut ("~/api/AutoReport",filter);
    }
    

}

export default WebRequestAutoMailReport;
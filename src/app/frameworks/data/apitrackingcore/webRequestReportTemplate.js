import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /ReportTemplate 
 * 
 * @class WebRequestReportTemplate
 * @extends {WebRequestBase}
 */
class WebRequestReportTemplate extends WebRequestBase {

    /**
     * Creates an instance of WebRequestReportTemplate.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request reportTemplate.
     * 
     * @static
     * @returns instance of WebRequestReportTemplate
     */
    static getInstance() {
        return Singleton.getInstance("trackingCore-webRequestReportTemplate",
            new WebRequestReportTemplate(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }
    /**
     * Get list of reportTemplate.
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf webRequestReportTemplate
     */
    listReportTemplate(filter) {
       return this.ajaxPost("~/api/reportTemplate/list", filter);
    }
    /**
     * Get reportTemplate by id
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf webRequestReportTemplate
     */
    getReportTemplate(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/reportTemplate/${id}`, includeAssociationNames);
    }
}

export default WebRequestReportTemplate;
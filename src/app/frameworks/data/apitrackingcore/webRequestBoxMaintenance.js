import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

class WebRequestBoxMaintenance extends WebRequestBase {

    /**
     * Creates an instance of WebRequestBoxMaintenance.
     * @private
     * @param {any} applicationUrl
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of WebRequestBoxMaintenance
     * 
     * @static
     * @returns instance of WebRequestBoxMaintenance
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-WebRequestBoxMaintenance",
            new WebRequestBoxMaintenance(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }
    
     /**
     * Getting box maintenance.
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf webrequestPermission
     */
    listBoxMaintenance(filter) {
        return this.ajaxPost("~/api/box/maintenance/list", filter);
    }

    /**
     * Create Box-Maintenance
     * 
     * @param {any} filter
     * @returns
     */
    createBoxMaintenance(info, returnResult = true) {
        return this.ajaxPost(`~/api/box/maintenance?returnResult=${returnResult}`, info);
    }

    /**
     * 
     * @param {any} id
     * @param {any} includeAssociationNames
     * @returns
     * 
     * @memberOf WebRequestBoxMaintenance
     */
    getBoxMaintenance(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/box/maintenance/${id}`, includeAssociationNames);
    }

     /**
     * Update Box-Maintenance
     * 
     * @param {any} info
     * @param {boolean} [returnResult=true]
     * @returns
     * 
     * @memberOf WebRequestBoxMaintenance
     */
    updateBoxMaintenance (info, returnResult = true) {
        return this.ajaxPut(`~/api/box/maintenance?returnResult=${returnResult}`, info);
    }

     /**
     * Delete Box-Maintenances
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf WebRequestBoxMaintenance
     */
    deleteBoxMaintenance (id) {
        return this.ajaxDelete("~/api/box/maintenance", [id]);
    }
}

export default WebRequestBoxMaintenance;
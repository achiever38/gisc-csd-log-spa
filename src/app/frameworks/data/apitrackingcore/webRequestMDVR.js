﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";
import { Enums } from "../../constant/apiConstant";

/**
 * 
 * 
 * @class WebRequestMDVRIcon
 * @extends {WebRequestBase}
 */
class WebRequestMDVR extends WebRequestBase {

    /**
     * Creates an instance of WebRequestMDVR.
     * 
     * @param {any} applicationUrl
     * 
     * @memberOf WebRequestMDVR
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * 
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestMDVR
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-WebRequestMDVR",
            new WebRequestMDVR(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * List summary of MDVR
     * @param {any} filter
     * @returns
     */
    listMDVRSummary(filter) {
        return this.ajaxPost("~/api/MDVR/summary/list", filter);
    }
    listMDVRSummaryDataGrid(filter) {
        return [this.resolveUrl("~/api/MDVR/summary/list"), filter];
    }
    /**
     * List of vehicles
     * @param {any} filter
     * @returns
     */
    listMDVRLiveVdo(filter) {
        return this.ajaxPost("~/api/MDVR/LiveVdo/list", filter);
    }

    /**
     * Get Vehicle
     * @param {any} id
     * @param {any} includeAssociationNames
     * @returns
     */
    getMDVR(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/MDVR/${id}`, includeAssociationNames);
    }


    /**
     * Create MDVR
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     */
    createMDVR(info, returnResult = false) {
        return this.ajaxPost(`~/api/MDVR?returnResult=${returnResult}`, info);
    }

    /**
     * Update MDVR
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     */
    updateMDVR(info, returnResult = false) {
        return this.ajaxPut(`~/api/MDVR?returnResult=${returnResult}`, info);
    }

    /**
     * Delete MDVR
     * @param {any} id
     * @returns
     */
    deleteMDVR(info) {
        return this.ajaxDelete("~/api/MDVR", info);
    }

    
    //MDVR Config-----------
    getMDVRConfig(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/MDVRServerConfiguration/${id}`, includeAssociationNames);
    }


    /**
     * Create MDVR Config
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     */
    createMDVRConfig(info, returnResult = false) {
        return this.ajaxPost(`~/api/MDVRServerConfiguration?returnResult=${returnResult}`, info);
    }

    /**
     * Update MDVR Config
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     */
    updateMDVRConfig(info, returnResult = false) {
        return this.ajaxPut(`~/api/MDVRServerConfiguration?returnResult=${returnResult}`, info);
    }

    /**
     * Delete MDVR Config
     * @param {any} id
     * @returns
     */
    deleteMDVRConfig(id) {
        return this.ajaxDelete("~/api/MDVRServerConfiguration", [id]);
    }


    //MDVR Server Config

    listMDVRConfigSummary(filter){
        return this.ajaxPost("~/api/MDVRServerConfiguration/summary/list", filter);
    }

    /**
     * Update Multiple MDVR
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     */
    updateMultipleMDVR(info, returnResult = false) {
        return this.ajaxPut(`~/api/MDVR/updatemultiple?returnResult=${returnResult}`, info);
    }

     /**
     * Export MDVR
     */
    ExportMDVR(info) {
        return this.ajaxPost(`~/api/MDVR/export`, info);
    }

     /**
     * Export MDVR
     */
    ImportMDVR(info, save = false) {
        return this.ajaxPost(`~/api/MDVR/import?save=${save}`, info);
    }

    /**
     * Download import MDVR template
     * @returns
     */
    downloadMDVRTemplate(templateFileType = Enums.TemplateFileType.Xlsx) {
        return this.ajaxGet(`~/api/MDVR/template/download?templateFileType=${templateFileType}`);
    }
}

export default WebRequestMDVR;
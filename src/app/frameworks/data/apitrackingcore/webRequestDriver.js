import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";
import { Enums } from "../../constant/apiConstant";

/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebRequestUser
 * @extends {WebRequest}
 */
class WebRequestDriver extends WebRequestBase {

    /**
     * Creates an instance of WebRequestDriver.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request driver.
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestDriver
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestDriver",
            new WebRequestDriver(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * Get list of driver summary
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf webRequestDriver
     */
    listDriverSummary(filter) {
        return this.ajaxPost("~/api/driver/summary/list", filter);
    }

    listDriverSummaryGrid(filter) {
        //return this.ajaxPost("~/api/driver/summary/list", filter);
        return [this.resolveUrl("~/api/driver/summary/list"),filter];
    }

    /**
     * Get driver by id
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf webRequestDriver
     */
    getDriver(id, includeAssociationNames = []) {
        var data = { 
            includeAssociationNames: includeAssociationNames
        };
        return this.ajaxGet(`~/api/driver/${id}`, data);
    }

    /**
     * Create driver with given driver info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=true]
     * @returns
     * 
     * @memberOf webRequestDriver
     */
    createDriver(info, returnResult = true) {
        return this.ajaxPost(`~/api/driver?returnResult=${returnResult}`, info);
    }

    /**
     * Update driver with given driver info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     * 
     * @memberOf webRequestDriver
     */
    updateDriver(info, returnResult = false) {
        return this.ajaxPut(`~/api/driver?returnResult=${returnResult}`, info);
    }

    /**
     * Delete an driver 
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf webRequestDriver
     */
    deleteDriver(id) {
        return this.ajaxDelete("~/api/driver", [id]);
    }

    /**
     * Export driver
     * @param {any} filter
     * @returns
     * 
     * @memberOf webRequestDriver
     */
    exportDriver(filter) {
        return this.ajaxPost("~/api/driver/export", filter);
    }

    
    /**
     * Import driver
     * @returns
     * 
     * @memberOf webRequestDriver
     */
    importDriver(info, save = false) {
        return this.ajaxPost(`~/api/driver/import?save=${save}`, info);
    }

    /**
     * Download driver template
     * @returns
     * 
     * @memberOf webRequestDriver
     */
    downloadDriverTemplate (templateFileType = Enums.TemplateFileType.Xlsx) {
        return this.ajaxGet(`~/api/driver/template/download?templateFileType=${templateFileType}`);
    }

    /**
     * Import driver picture
     * @returns
     * 
     * @memberOf webRequestDriver
     */
    importDriverPicture(info, save = false) {
        return this.ajaxPost(`~/api/driver/pictures/import?save=${save}`, info);
    }

    /**
     * Download driver picture template
     * @returns
     * 
     * @memberOf webRequestDriver
     */
    downloadDriverPictureTemplate () {
        return this.ajaxGet("~/api/driver/pictures/template/download");
    }
        /**
        * summary/list/provinceDlt
        * @returns
        * 
        * @memberOf webRequestDriver
        */
    listProvinceDlt(filter){
        return this.ajaxPost("~/api/driver/summary/list/provinceDlt",filter);
    }

    driverSummarylist(filter) {
        return this.ajaxPost("~/api/driver/summary/list", filter);
    }
}

export default WebRequestDriver;
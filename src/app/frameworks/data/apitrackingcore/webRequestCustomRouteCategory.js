﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";
import {EntityAssociation} from "../../constant/apiConstant";

/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebReqestCustomRouteCategory
 * @extends {WebRequestBase}
 */
class WebReqestCustomRouteCategory extends WebRequestBase {

    /**
     * Creates an instance of WebReqestCustomRouteCategory.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request user.
     * 
     * @static
     * @returns instance of WebRequestUser
     */
    static getInstance() {
        return Singleton.getInstance("trackingcore-webrequestCustomRouteCategory",
            new WebReqestCustomRouteCategory(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * Get list of customroutecategory in summary format.
     * 
     * @param {any} filter
     * @returns jQuery deferred.
     */
    listCustomRouteCategorySummary(filter) {
        return this.ajaxPost("~/api/customRouteCategory/summary/list", filter);
    }

    /**
     * Get user by id
     * 
     * @param {any} id, userId
     * @returns jQuery deferred.
     */
    getCustomRouteCategory(id) {
        return this.ajaxGet(`~/api/customRouteCategory/${id}`);
    }
    
    /**
     * Create CustomRouteCategory with given customRouteCategoryModel
     * 
     * @param {any} customRouteCategoryModel
     * @param {boolean} [returnResult=true]
     * @returns jQuery deferred.
     */
    createCustomRouteCategory(customRouteCategoryModel, returnResult = true) {
        return this.ajaxPost(`~/api/customRouteCategory?returnResult=${returnResult}`, customRouteCategoryModel);
    }

        /**
         * Update user with given customRouteCategoryModel
         * 
         * @param {any} customRouteCategoryModel
         * @param {boolean} [returnResult=true]
         * @returns jQuery deferred.
         */
        updateCustomRouteCategory(customRouteCategoryModel, returnResult = true) {
            return this.ajaxPut(`~/api/customRouteCategory?returnResult=${returnResult}`, customRouteCategoryModel);
        }

        /**
         * Delete an CustomRouteCategory 
         * 
         * @param {any} id
         * @returns
         */
        deleteCustomRouteCategory(id) {
            return this.ajaxDelete(`~/api/customRouteCategory`, [id]);
        }
    }

    export default WebReqestCustomRouteCategory;
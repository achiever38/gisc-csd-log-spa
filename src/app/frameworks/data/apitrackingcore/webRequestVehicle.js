import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";
import { Enums } from "../../constant/apiConstant";

/**
 * 
 * 
 * @class WebRequestVehicleIcon
 * @extends {WebRequestBase}
 */
class WebRequestVehicle extends WebRequestBase {

    /**
     * Creates an instance of WebRequestVehicle.
     * 
     * @param {any} applicationUrl
     * 
     * @memberOf WebRequestVehicle
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * 
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestVehicle
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestVehicle",
            new WebRequestVehicle(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * List summary of vehicles
     * @param {any} filter
     * @returns
     */
    listVehicleSummary(filter) {
        return this.ajaxPost("~/api/vehicle/summary/list", filter);
    }

    /**
     * List of vehicles
     * @param {any} filter
     * @returns
     */
    listVehicle(filter) {
        return this.ajaxPost("~/api/vehicle/list", filter);
    }

    listCategorySummary(filter) {
        return this.ajaxPost("~/api/vehicle/CategorySummary/list", filter);
    }

    /**
     * Get Vehicle
     * @param {any} id
     * @param {any} includeAssociationNames
     * @returns
     */
    getVehicle(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/vehicle/${id}`, includeAssociationNames);
    }

    /**
     * Validate Vehicle License. 
     * @param {any} info (VehicleLicenseInfo)
     * @returns
     */
    validateVehicleLicense(info){
        return this.ajaxPost(`~/api/vehicle/license/validate`, info);
    }

    /**
     * Create Vehicle
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     */
    createVehicle(info, returnResult = false) {
        return this.ajaxPost(`~/api/vehicle?returnResult=${returnResult}`, info);
    }

    /**
     * Update Vehicle
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     */
    updateVehicle(info, returnResult = false) {
        return this.ajaxPut(`~/api/vehicle?returnResult=${returnResult}`, info);
    }

    /**
     * Delete Vehicle
     * @param {any} id
     * @returns
     */
    deleteVehicle(id) {
        return this.ajaxDelete("~/api/vehicle", [id]);
    }

    /**
     * 
     * Get vehicle current distance
     * @param {any} id
     * @returns
     * 
     * @memberOf WebRequestVehicle
     */
    getCurrentDistance(id) {
        return this.ajaxGet(`~/api/vehicle/currentDistance/${id}`);
    }

    /**
     * 
     * Update vehicle odo meter
     * info = VehicleOdoMeterInfo
     * @param {any} info
     * @returns
     * 
     * @memberOf WebRequestVehicle
     */
    updateOdoMeter(info) {
        return this.ajaxPost("~/api/vehicle/odoMeter", info);
    }

    /**
     * 
     * Download Vehicle Template for import
     * @returns
     * 
     * @memberOf WebRequestVehicle
     */
    downloadVehicleTemplate (templateFileType = Enums.TemplateFileType.Xlsx) {
        return this.ajaxGet(`~/api/vehicle/template/download?templateFileType=${templateFileType}`);
    }

    /**
     * Export vehicle
     * @param {any} filter
     * @returns
     * 
     * @memberOf WebRequestVehicle
     */
    exportVehicle (filter) {
        return this.ajaxPost("~/api/vehicle/export", filter);
    }

    /**
     * 
     * Import vehicle
     * info = ImportVehicleInfo
     * @param {any} info
     * @param {boolean} [save=false]
     * @returns
     * 
     * @memberOf WebRequestVehicle
     */
    importVehicle (info, save = false) {
        return this.ajaxPost(`~/api/vehicle/import?save=${save}`, info);
    }

    /**
     * Determine current vehicle has already associated with fleet service.
     * 
     * @param {any} id 
     * @returns {jQuery Deferred}
     */
    checkFleetServiceAssociation(id) {
        return this.ajaxGet(`~/api/vehicle/${id}/fleetService/associate`);
    }

    /**
     * 
     * Get VehicleFeatures
     * @param {any} id
     * @returns
     * 
     * @memberOf WebRequestVehicle
     */
    getVehicleFeatures(id) {
        return this.ajaxGet(`~/api/vehicle/getVehicleFeatures/${id}`);
    }

    summaryVehicleOdoFixing(info) {
        return this.ajaxPost(`/api/vehicle/summary/odoMeterFixing`, info);
    }

    vehicleOdoFixing(info) {
        return this.ajaxPost(`/api/vehicle/odoMeterFixing`, info);
    }
    
    summaryVehicleOdometer(info) {
        return this.ajaxPost(`/api/vehicle/summary/odoMeter`, info);
    }
}

export default WebRequestVehicle;
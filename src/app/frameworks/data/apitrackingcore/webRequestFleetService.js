import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebRequestUser
 * @extends {WebRequest}
 */
class WebRequestFleetService extends WebRequestBase {

    /**
     * Creates an instance of WebRequestFleetService.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request fleetService.
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestFleetService
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestFleetService",
            new WebRequestFleetService(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * Get list of fleetService summary
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf webRequestFleetService
     */
    listFleetServiceSummary(filter) {
        return this.ajaxPost("~/api/fleetService/summary/list", filter);
    }

    /**
     * Get fleetService by id
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf webRequestFleetService
     */
    getFleetService(id, includeAssociationNames = []) {
        var data = { 
            includeAssociationNames: includeAssociationNames
        };
        return this.ajaxGet(`~/api/fleetService/${id}`, data);
    }

    /**
     * Create fleetService with given fleetService info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=true]
     * @returns
     * 
     * @memberOf webRequestFleetService
     */
    createFleetService(info, returnResult = true) {
        return this.ajaxPost(`~/api/fleetService?returnResult=${returnResult}`, info);
    }

    /**
     * Update fleetService with given fleetService info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     * 
     * @memberOf webRequestFleetService
     */
    updateFleetService(info, returnResult = false) {
        return this.ajaxPut(`~/api/fleetService?returnResult=${returnResult}`, info);
    }

    /**
     * Delete an fleetService 
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf webRequestFleetService
     */
    deleteFleetService(id) {
        return this.ajaxDelete("~/api/fleetService", [id]);
    }

    exportFleetService(filter){
        return this.ajaxPost("~/api/fleetService/summary/export",filter);
    }
}

export default WebRequestFleetService;
﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";
import {EntityAssociation} from "../../constant/apiConstant";

/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebReqestCustomAreaCategory
 * @extends {WebRequestBase}
 */
class WebRequestCustomAreaCategory extends WebRequestBase {
      

    /**
     * Creates an instance of WebReqestCustomAreaCategory.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request user.
     * 
     * @static
     * @returns instance of WebRequestUser
     */
    static getInstance() {
        return Singleton.getInstance("trackingcore-webrequestCustomAreaCategory",
            new WebRequestCustomAreaCategory(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * Get list of customareacategory in summary format.
     * 
     * @param {any} filter
     * @returns jQuery deferred.
     */
    listCustomAreaCategorySummary(filter) {
        return this.ajaxPost("~/api/customAreaCategory/summary/list", filter);
    }

    /**
     * Get user by id
     * 
     * @param {any} id, userId
     * @returns jQuery deferred.
     */
    getCustomAreaCategory(id) {
        return this.ajaxGet(`~/api/customAreaCategory/${id}`);
    }
    
    /**
     * Create CustomAreaCategory with given customAreaCategoryModel
     * 
     * @param {any} customAreaCategoryModel
     * @param {boolean} [returnResult=true]
     * @returns jQuery deferred.
     */
    createCustomAreaCategory(customAreaCategoryModel, returnResult = true) {
        return this.ajaxPost(`~/api/customAreaCategory?returnResult=${returnResult}`, customAreaCategoryModel);
    }

        /**
         * Update user with given customAreaCategoryModel
         * 
         * @param {any} customAreaCategoryModel
         * @param {boolean} [returnResult=true]
         * @returns jQuery deferred.
         */
        updateCustomAreaCategory(customAreaCategoryModel, returnResult = true) {
            return this.ajaxPut(`~/api/customAreaCategory?returnResult=${returnResult}`, customAreaCategoryModel);
        }

        /**
         * Delete an CustomAreaCategory 
         * 
         * @param {any} id
         * @returns
         */
        deleteCustomAreaCategory(id) {
            return this.ajaxDelete(`~/api/customAreaCategory`, [id]);
        }
    }

    export default WebRequestCustomAreaCategory;
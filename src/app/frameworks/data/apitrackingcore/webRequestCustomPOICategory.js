﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";
import {EntityAssociation} from "../../constant/apiConstant";

/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebRequestCustomPOI
 * @extends {WebRequestBase}
 */
class WebRequestCustomPOICategory extends WebRequestBase {

    /**
     * Creates an instance of WebRequestCustomPOI.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request user.
     * 
     * @static
     * @returns instance of WebRequestUser
     */
    static getInstance() {
        return Singleton.getInstance("trackingcore-webrequestCustomPOICategory",
            new WebRequestCustomPOICategory(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * Get list of custompoicatgory in summary format.
     * 
     * @param {any} filter
     * @returns jQuery deferred.
     */
    listCustomPOICategorySummary(filter) {
        return this.ajaxPost("~/api/customPOICategory/summary/list", filter);
    }

    /**
     * Get user by id
     * 
     * @param {any} id, userId
     * @returns jQuery deferred.
     */
    getCustomPOICategory(id) {
        return this.ajaxGet(`~/api/customPOICategory/${id}`);
    }
    
    /**
     * Create CustomPOICategory with given customPOICategoryModel
     * 
     * @param {any} customPOICategoryModel
     * @param {boolean} [returnResult=true]
     * @returns jQuery deferred.
     */
    createCustomPOICategory(customPOICategoryModel, returnResult = true) {
        return this.ajaxPost(`~/api/customPOICategory?returnResult=${returnResult}`, customPOICategoryModel);
    }

        /**
         * Update user with given customPOICategoryModel
         * 
         * @param {any} customPOICategoryModel
         * @param {boolean} [returnResult=true]
         * @returns jQuery deferred.
         */
        updateCustomPOICategory(customPOICategoryModel, returnResult = true) {
            return this.ajaxPut(`~/api/customPOICategory?returnResult=${returnResult}`, customPOICategoryModel);
        }

        /**
         * Delete an CustomPOICategory 
         * 
         * @param {any} id
         * @returns
         */
        deleteCustomPOICategory(id) {
            return this.ajaxDelete(`~/api/customPoiCategory`, [id]);
        }
    }

    export default WebRequestCustomPOICategory;
import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /Alert 
 * 
 * @class WebRequestAlert
 * @extends {WebRequestBase}
 */
class WebRequestAlert extends WebRequestBase {

    /**
     * Creates an instance of WebRequestAlert.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request alert.
     * 
     * @static
     * @returns instance of WebRequestAlert
     */
    static getInstance() {
        return Singleton.getInstance("trackingCore-webRequestAlert",
            new WebRequestAlert(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }
    
    /**
     * 
     * Get list of alert dashboard
     * @param {any} companyId
     * @param {any} keyword
     * @returns jQuery Deferred
     * 
     */
    getAlertDashboard(companyId, keyword = "") {
        let webReq = null;
        if(keyword){
            webReq = this.ajaxGet(`~/api/alert/dashboard/${companyId}?Keyword=${keyword}`);
        }else{
            webReq = this.ajaxGet(`~/api/alert/dashboard/${companyId}`)
        }
        return webReq;
    }

    /**
     * 
     * Get ticket of alert dashboard
     * @param {any} keyword
     * @returns jQuery Deferred
     * 
     */

    getAlertTicketDashboard(keyword = "") {
        let webReq = null;
        if(keyword){
            webReq = this.ajaxGet(`~/api/alertTicket/dashboard?Keyword=${keyword}`);
        }else{
            webReq = this.ajaxGet(`~/api/alertTicket/dashboard`)
        }
        return webReq;
    }

    /**
     * Export Alerts
     * @param {any} filter
     * @returns jQuery Deferred
     */
    exportAlert(filter) {
        return this.ajaxPost("~/api/alert/export", filter);
    }

    /**
     * Get list of alert in summary format 
     * 
     * @param {any} filter
     * @returns jQuery Deferred
     */
    listAlertSummary(filter) {
        return this.ajaxPost("~/api/alert/summary/list", filter);
    }

    /**
     * Get single alert information by id.
     * 
     * @param {any} id
     * @param {any} includeAssociationNames
     * @returns jQuery Deferred
     */
    getAlert(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/alert/${id}`, includeAssociationNames);
    }

    /**
     * Get list of alert notification ids
     * @param {any} filter
     * @returns jQuery Deferred
     */
    listAlertNotificationId(filter) {
        return this.ajaxPost("~/api/alert/notification/id/list", filter);
    }

    /**
     * Get list of alert notifications
     * @param {any} filter
     * @returns jQuery Deferred
     */
    listAlertNotification(filter) {
        return this.ajaxPost("~/api/alert/notification/list", filter);
    }

    /**
     * Dismiss/Dismiss All for notification items
     * @param {any} filter
     * @returns jQuery Deferred
     */
    updateAlertNotificationStatus(filter) {
        return this.ajaxPost("~/api/alert/notification/status", filter);
    }

    alertTicketList(filter) {
        return this.ajaxPost("~/api/alertTicket/summary/list", filter);
    }

    alertTicketDetail(id, includeAssociationNames){
        return this.ajaxGet(`~/api/alertTicket/${id}`, includeAssociationNames);        
    }

    alertTicketCloseTicket(filter, returnResult = true) {
        return this.ajaxPost(`~/api/alertTicket/closeTicket?returnResult=${returnResult}`, filter);
    }

    alertTicketCreateLog(filter, returnResult = true) {
        return this.ajaxPost(`~/api/alertTicket/alertTicketLog?returnResult=${returnResult}`, filter);
    }

    alertTicketExport(filter) {
        return this.ajaxPost(`~/api/alertTicket/export`, filter);
    }
    
}

export default WebRequestAlert;
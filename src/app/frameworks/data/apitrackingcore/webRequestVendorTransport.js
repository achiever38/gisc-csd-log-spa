import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /Vendor 
 * 
 * @class WebRequestVendor
 * @extends {WebRequestBase}
 */
class WebRequestVendorTransport extends WebRequestBase {

    /**
     * Creates an instance of WebRequestVendor.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request vendorTransport.
     * 
     * @static
     * @returns instance of webRequestVendorTransport
     */
    static getInstance() {
        return Singleton.getInstance("trackingCore-webRequestVendorTransport",
            new WebRequestVendorTransport(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }
    /**
     * Get list of vendor.
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf webRequestVendorTransport
     */
    listVendorTransport(filter) {
        return this.ajaxPost("~/api/vendorTransporter/summary/list", filter);
    }
    /**
     * Get vendor by id
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf webRequestVendorTransport
     */
    getVendorTransport(id) {
        return this.ajaxGet(`~/api/vendorTransporter/${id}`);
    }
    /**
     * Create vendor with given vendor filter
     * 
     * @param {any} filter

     * 
     * @memberOf webRequestVendorTransport
     */
    createVendorTransport(filter) {
        return this.ajaxPost(`~/api/vendorTransporter`, filter);
    }
    /**
     * Update vendor with given vendor info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     * 
     * @memberOf webRequestVendorTransport
     */
    updateVendorTransport(filter) {
        return this.ajaxPut(`~/api/vendorTransporter`, filter);
    }

    deleteVendorTransport(id){
        return this.ajaxDelete("~/api/vendorTransporter",[id]);
    }
       
    //find Bu by Vendor
    listVendorTransportBu(filter) {
        return this.ajaxPost("~/api/vendorTransporter/businessunit/list", filter);
    }
}

    export default WebRequestVendorTransport;
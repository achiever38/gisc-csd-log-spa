import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebRequestUser
 * @extends {WebRequest}
 */
class WebRequestMobileService extends WebRequestBase {

    /**
     * Creates an instance of WebRequestMobileService.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request mobileService.
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestMobileService
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestMobileService",
            new WebRequestMobileService(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * Get list of mobileService summary
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf webRequestMobileService
     */
    listMobileServiceSummary(filter) {
        return this.ajaxPost("~/api/mobileService/summary/list", filter);
    }

    /**
     * Get mobileService by id
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf webRequestMobileService
     */
    getMobileService(id, includeAssociationNames = []) {
        var data = { 
            includeAssociationNames: includeAssociationNames
        };
        return this.ajaxGet(`~/api/mobileService/${id}`, data);
    }

    /**
     * Create mobileService with given mobileService info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=true]
     * @returns
     * 
     * @memberOf webRequestMobileService
     */
    createMobileService(info, returnResult = true) {
        return this.ajaxPost(`~/api/mobileService?returnResult=${returnResult}`, info);
    }

    /**
     * Update mobileService with given mobileService info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     * 
     * @memberOf webRequestMobileService
     */
    updateMobileService(info, returnResult = false) {
        return this.ajaxPut(`~/api/mobileService?returnResult=${returnResult}`, info);
    }

    /**
     * Delete an mobileService
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf webRequestMobileService
     */
    deleteMobileService(id) {
        return this.ajaxDelete("~/api/mobileService", [id]);
    }
}

export default WebRequestMobileService;
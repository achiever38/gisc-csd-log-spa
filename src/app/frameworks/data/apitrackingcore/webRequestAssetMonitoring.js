import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";
import { Enums } from "../../constant/apiConstant";

/**
 * 
 * 
 * @class WebRequestVehicleIcon
 * @extends {WebRequestBase}
 */
class WebRequestAssetMonitoring extends WebRequestBase {

    /**
     * Creates an instance of WebRequestVehicle.
     * 
     * @param {any} applicationUrl
     * 
     * @memberOf WebRequestVehicle
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * 
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestVehicle
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestAssetMonitoring",
            new WebRequestAssetMonitoring(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * List summary of vehicles
     * @param {any} filter
     * @returns
     */
    listAssetMonitoringSummary(filter) {
        return this.ajaxPost("~/api/assetMonitoring/summary/list", filter);
    }

    listAssetMonitoringSummaryNew(filter) {
        return [this.resolveUrl("~/api/assetMonitoring/summary/list"), filter];
    }
    exportAssetMonitoringSummary(filter){
        return this.ajaxPost("~/api/assetMonitoring/export", filter);
    }
    batchAction(filter){
        return this.ajaxPost("~/api/assetMonitoring/batchAction", filter);
    }

}

export default WebRequestAssetMonitoring;
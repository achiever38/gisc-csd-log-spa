﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";


/**
 * Facade to access Core Web API in /map
 * 
 * @class WebRequestMap
 * @extends {WebRequestBase}
 */
class WebRequestMap extends WebRequestBase {

    /**
     * Creates an instance of WebRequestMap.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request map.
     * 
     * @static
     * @returns instance of WebRequestMap
     */
    static getInstance() {
        return Singleton.getInstance("trackingcore-webrequestMap",
            new WebRequestMap(WebConfig.appSettings.apiMapUrl)
        );
    }

    /**
     * Get customroute by lat,lon
     * 
     * @param {any} lat, lon
     * @returns jQuery deferred.
     */
    identify(param) {
   
        var data = { 
            lat: param.lat,
            lon: param.lon,
            extent: param.extent ? param.extent  : "0,0,0,0"
        };
        return this.ajaxGet(`~/api/location/identify/`, data);
    }
    /**
     * solve Route
     * 
     * @param {String[]} stops :[{name:"Waypoint name",lat:"Latitude of waypoint",lon:"Longitude of waypoint"}]
     * @param {int} routeMode
     * @param {int} routeOption 
     * @param {String} lang {L,E}
     * @returns jQuery deferred.
     */
    solveRoute(param){
        return this.ajaxPost(`~/api/route/solve`,param);
    }
   
    }

    export default WebRequestMap;
import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /Vendor 
 * 
 * @class WebRequestVendor
 * @extends {WebRequestBase}
 */
class webRequestVendorTransportConnecter extends WebRequestBase {

    /**
     * Creates an instance of WebRequestVendor.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request vendorTransport.
     * 
     * @static
     * @returns instance of webRequestVendorTransport
     */
    static getInstance() {
        return Singleton.getInstance("trackingCore-webRequestVendorTransportConnecter",
            new webRequestVendorTransportConnecter(WebConfig.appSettings.connectorAPIUrl)
        );
    }

    listVendorLog(filter) {
        return this.ajaxPost("~/API/SCCCConnector/Summary/List", filter);
    }
    listVendorLogDataGrid(filter){
        return [this.resolveUrl("~/API/SCCCConnector/Summary/List"),filter];
    }
    listVendorDetail(filter) {
        return this.ajaxPost("~/API/SCCCConnector/SummaryDetail/List", filter);
    }
    listVendorDetailDataGrid(filter){
        return [this.resolveUrl("~/API/SCCCConnector/SummaryDetail/List"),filter];
    }
   

        
}

    export default webRequestVendorTransportConnecter;
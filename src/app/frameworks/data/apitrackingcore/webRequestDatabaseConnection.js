import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";


/**
 * 
 * 
 * @class WebRequestDatabaseConnection
 * @extends {WebRequestBase}
 */
class WebRequestDatabaseConnection extends WebRequestBase {
    /**
     * Creates an instance of WebRequestDatabaseConnection.
     * 
     * @param {any} applicationUrl
     * 
     * @memberOf WebRequestDatabaseConnection
     */
    constructor(applicationUrl) {
        super(applicationUrl);

    }

    /**
     * 
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestDatabaseConnection
     */
    static getInstance() {
        return Singleton.getInstance("trackingCore-WebrequestDatabaseConnection",
            new WebRequestDatabaseConnection(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * 
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf WebRequestDatabaseConnection
     */
    listDBConnectionSummary(filter) {
       return this.ajaxPost("~/api/databaseConnection/summary/list", filter);
    }

    
    /**
     * 
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf WebRequestDatabaseConnection
     */
    listDBConnection(filter) {
       return this.ajaxPost("~/api/databaseConnection/list", filter);
    }


    /**
     * 
     * 
     * @param {any} id
     * @param {any} includeAssociationNames
     * @returns
     * 
     * @memberOf WebRequestDatabaseConnection
     */
    getDBConnection(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/databaseConnection/${id}`, includeAssociationNames);
    }

    /**
     * 
     * 
     * @param {any} info
     * @param {boolean} [returnResult=true]
     * @returns
     * 
     * @memberOf WebRequestDatabaseConnection
     */
    createDBConnection(info, returnResult = true) {
        return this.ajaxPost(`~/api/databaseConnection?returnResult=${returnResult}`, info);
    }

    /**
     * 
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf WebRequestDatabaseConnection
     */
    deleteDBConnection(id) {
        return this.ajaxDelete("~/api/databaseConnection", [id]);
    }
}

export default WebRequestDatabaseConnection;
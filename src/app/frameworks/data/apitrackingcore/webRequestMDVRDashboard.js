﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";
import { Enums } from "../../constant/apiConstant";

/**
 * 
 * 
 * @class WebRequestMDVRIcon
 * @extends {WebRequestBase}
 */
class WebRequestMDVRDashboard extends WebRequestBase {

    /**
     * Creates an instance of WebRequestMDVR.
     * 
     * @param {any} applicationUrl
     * 
     * @memberOf WebRequestMDVR
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * 
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestMDVR
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestMDVRDashboard",
            new WebRequestMDVRDashboard(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    // Event Video //
    /**
     * Get Alert
     * @param {any} id
     * @param {any} includeAssociationNames
     * @returns
     */
    getAlert(id, includeAssociationNames, keyword = "") {
        let webReq = null;
        if(keyword){
            webReq = this.ajaxGet(`~/api/mdvrDashboard/getAlert/${id}?Keyword=${keyword}` ,includeAssociationNames);
        }else{
            webReq = this.ajaxGet(`~/api/mdvrDashboard/getAlert/${id}` ,includeAssociationNames);
        }
        return webReq;
    }

    // Summary list Alert
    alertSummaryList(filter) {
        return this.ajaxPost("~/api/mdvrDashboard/alert/summaryList", filter);
    }
    /**
     * Get AlertTicket
     * @returns
     */
    getAlertTicket(keyword = "") {
        let webReq = null;
        if(keyword){
            webReq = this.ajaxGet(`~/api/mdvrDashboard/getAlertTicket?Keyword=${keyword}`);
        }else{
            webReq = this.ajaxGet(`~/api/mdvrDashboard/getAlertTicket`)
        }
        return webReq;
    }

    // Summary list AlertTicket
    alertTicketSummaryList(filter) {
        return this.ajaxPost("~/api/mdvrDashboard/alertTicket/summaryList", filter);
    }

}

export default WebRequestMDVRDashboard;
﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";
import {EntityAssociation} from "../../constant/apiConstant";

/**
 * Facade to access Core Web API in /CustomRoute
 * 
 * @class WebReqestCustomRoute
 * @extends {WebRequestBase}
 */
class WebReqestCustomRoute extends WebRequestBase {

    /**
     * Creates an instance of WebReqestCustomRoute.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request CustomRoute.
     * 
     * @static
     * @returns instance of WebReqestCustomRoute
     */
    static getInstance() {
        return Singleton.getInstance("trackingcore-webrequestCustomRoute",
            new WebReqestCustomRoute(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

        /**
         * Get list of customroute in summary format.
         * 
         * @param {any} filter
         * @returns jQuery deferred.
         */
        listCustomRouteSummary(filter) {
            return this.ajaxPost("~/api/customRoute/summary/list", filter);
        }
        /**
         * Get list of customrouteOption in summary format.
         * 
         * @param {any} filter
         * @returns jQuery deferred.
         */
        listCustomRouteOption(filter) {
            return this.ajaxPost("~/api/customRouteOption/list", filter);
        }
        /**
         * Get customroute by id
         * 
         * @param {any} id, customRouteId
         * @returns jQuery deferred.
         */
        getCustomRoute(id) {
            var data = { 
                includeAssociationNames: [
                    EntityAssociation.CustomRoute.Category,
                    EntityAssociation.CustomRoute.ViaPoints,
                    EntityAssociation.CustomRoute.RouteOption,
                    EntityAssociation.CustomRoute.AccessibleBusinessUnits
                ]
            };
            return this.ajaxGet(`~/api/customRoute/${id}`, data);
        }
    
        /**
         * Create CustomRoute with given customRouteModel
         * 
         * @param {any} customRouteModel
         * @param {boolean} [returnResult=true]
         * @returns jQuery deferred.
         */
        createCustomRoute(customRouteModel, returnResult = true) {
            return this.ajaxPost(`~/api/customRoute?returnResult=${returnResult}`, customRouteModel);
        }

        /**
         * Update customRoute with given customRouteModel
         * 
         * @param {any} customRouteModel
         * @param {boolean} [returnResult=true]
         * @returns jQuery deferred.
         */
        updateCustomRoute(customRouteModel, returnResult = true) {
                return this.ajaxPut(`~/api/customRoute?returnResult=${returnResult}`, customRouteModel);
        }

        /**
         * Delete an CustomRoute 
         * 
         * @param {any} id
         * @returns
         */
        deleteCustomRoute(id) {
            return this.ajaxDelete(`~/api/customRoute`, [id]);
        }

        createCustomAreaFromRoutes(customRouteModel, returnResult = true) {
            return this.ajaxPost(`~/api/customRoute/createcustomareafromroutes?returnResult=${returnResult}`, customRouteModel);
        }

         /**
         * Export Customroute
         * @param {any} filter
         * @returns
         */
        exportCustomRoute (filter) {
            return this.ajaxPost(`~/api/customRoute/export`, filter);
        }
       
    }

    export default WebReqestCustomRoute;
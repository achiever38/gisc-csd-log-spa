import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

class WebRequestAlertConfiguration extends WebRequestBase {

    /**
     * Creates an instance of WebRequestAlertConfiguration.
     * @private
     * @param {any} applicationUrl
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of WebRequestAlertConfiguration
     * 
     * @static
     * @returns instance of WebRequestAlertConfiguration
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestAlertConfiguration",
            new WebRequestAlertConfiguration(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * Find alertConfiguration summary.
     * 
     * @param {any} filter
     * @returns
     */
    listAlertConfigurationSummary(filter) {
        return this.ajaxPost("~/api/alertConfiguration/summary/list", filter);
    }

    /**
     * Find AlertConfigurationBoxTemplate summary.
     * 
     * @param {any} filter
     * @returns
     */
    listAlertConfigurationBoxTemplateSummary(filter) {
        return this.ajaxPost("~/api/alertConfiguration/boxtemplate/summary/list", filter);
    }

    /**
     * Find alertConfiguration.
     * 
     * @param {any} filter
     * @returns
     */
    listAlertConfiguration(filter) {
        return this.ajaxPost("~/api/alertConfiguration/list", filter);
    }

    /**
     * Get alertConfiguration by id
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf webRequestAlertConfiguration
     */
    getAlertConfiguration(id, includeAssociationNames = []) {
        var data = { 
            includeAssociationNames: includeAssociationNames
        };
        return this.ajaxGet(`~/api/alertConfiguration/${id}`, data);
    }

    /**
     * Get Output AlertConfigurationBoxTemplate by id
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf webRequestAlertConfiguration
     */
    getAlertConfigurationBoxTemplate(id) {
        return this.ajaxGet(`~/api/alertConfiguration/boxtemplate/${id}`);
    }

    /**
     * Create alertConfiguration with given alertConfiguration info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=true]
     * @returns
     * 
     * @memberOf webRequestAlertConfiguration
     */
    createAlertConfiguration(info, returnResult = true) {
        return this.ajaxPost(`~/api/alertConfiguration?returnResult=${returnResult}`, info);
    }

    /**
     * Update alertConfiguration with given alertConfiguration info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     * 
     * @memberOf webRequestAlertConfiguration
     */
    updateAlertConfiguration(info, returnResult = false) {
        return this.ajaxPut(`~/api/alertConfiguration?returnResult=${returnResult}`, info);
    }

    /**
     * Delete an alertConfiguration
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf webRequestAlertConfiguration
     */
    deleteAlertConfiguration(id) {
        return this.ajaxDelete("~/api/alertConfiguration", [id]);
    }
}

export default WebRequestAlertConfiguration;
﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * 
 * 
 * @class WebReqesPoiIcon
 * @extends {WebRequestBase}
 */
class WebReqesMapService extends WebRequestBase {

    /**
     * Creates an instance of WebReqesPoiIcon.
     * 
     * @param {any} applicationUrl
     * 
     * @memberOf WebReqesPoiIcon
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * 
     * 
     * @static
     * @returns
     * 
     * @memberOf WebReqesPoiIcon
     */
    static getInstance() {
        return Singleton.getInstance("trackingcore-webReqesMapService",
            new WebReqesMapService(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }


    /**
     * 
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf WebReqesPoiIcon
     */
    // listPoiIconSummary(filter) {
    //     return this.ajaxPost("~/api/poiIcon/summary/list", filter);
    // }

    mapServiceSummaryList(filter) {
        return this.ajaxPost("~/api/mapservice/summary/list", filter);
    }

    mapServiceCreate(filter){
        return this.ajaxPost("~/api/mapservice", filter);
    }

    /**
     * 
     * 
     * @param {any} info
     * @param {boolean} [returnResult=true]
     * @returns
     * 
     * @memberOf WebReqesPoiIcon
     */
    // createPoiIcon(info, returnResult = true) {
    //     return this.ajaxPost(`~/api/poiIcon?returnResult=${returnResult}`, info);
    // }
   

    /**
     * 
     * 
     * @param {any} info
     * @param {boolean} [returnResult=true]
     * @returns
     * 
     * @memberOf WebReqesPoiIcon
     */

    // mapServiceUpdate(info , returnResult = true){
    //     return this.ajaxPut(`~/api/mapservice=${returnResult}`, info);
    // }
    
    mapServiceUpdate(info , returnResult = true){
        return this.ajaxPut(`~/api/mapservice`, info);
    }

    /**
     * 
     * 
     * @param {any} id
     * @param {any} includeAssociationNames
     * @returns
     * 
     * @memberOf WebReqesPoiIcon
     */
    getMapservice(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/mapservice/${id}`, includeAssociationNames);
    }

    /**
     * 
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf WebReqesPoiIcon
     */
    // deletePoiIcon(id) {
    //     return this.ajaxDelete("~/api/poiIcon", [id]);
    // }
    deleteMapservice(id) {
        return this.ajaxDelete("~/api/mapservice", [id]);
    }


}

export default WebReqesMapService;
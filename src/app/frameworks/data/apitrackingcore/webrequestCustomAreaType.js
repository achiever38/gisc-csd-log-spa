import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

class WebrequestCustomAreaType extends WebRequestBase {

    /**
     * Creates an instance of WebrequestCustomAreaType.
     * @private
     * @param {any} applicationUrl
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of WebrequestCustomAreaType
     * 
     * @static
     * @returns instance of WebrequestCustomAreaType
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webrequestCustomAreaType",
            new WebrequestCustomAreaType(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * Find alert types.
     * 
     * @param {any} filter
     * @returns
     */
    listCustomAreaType(filter) {
        return this.ajaxPost("~/api/customAreaType/list", filter);
    }

    listCustomSubAreaType(filter) {
        return this.ajaxPost("~/api/customAreaType/subArea/list", filter);
    }
}

export default WebrequestCustomAreaType;
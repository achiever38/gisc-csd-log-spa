import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";
import {EntityAssociation} from "../../constant/apiConstant";

/**
 * Handle System Configurations class.
 * 
 * @class WebRequestSystemConfiguration
 * @extends {WebRequestBase}
 */
class WebRequestSystemConfiguration extends WebRequestBase {
    /**
     * Creates an instance of WebRequestSystemConfiguration.
     * 
     * @param {any} applicationUrl
     */
    constructor (applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Return single instance of web request system configuration.
     * 
     * @static
     */
    static getInstance() {
        return Singleton.getInstance("apicore-webRequestSystemConfiguration",
            new WebRequestSystemConfiguration(WebConfig.appSettings.apiCoreUrl)
        );
    }

    /**
     * Get System regional settings which contains date/time formats and timezone.
     * @returns
     * 
     * @memberOf webRequestSystemConfiguration
     */
    getRegionalSystemConfiguration() {
        return this.ajaxGet("~/api/systemConfiguration/regional");
    }
    /**
     * Get list of System Configuration.
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf webRequestSystemConfiguration
     */
    listSystemConfiguration(filter) {
        return this.ajaxPost("~/api/systemConfiguration/list", filter);
    }
    /**
     * Get vendor by id
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf webRequestSystemConfiguration
     */
    getSystemConfiguration(id) {
        return this.ajaxGet(`~/api/systemConfiguration/${id}`);
    }
    /**
     * Update user with given System Configuration info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=true]
      * @returns
     * 
     * @memberOf webRequestSystemConfiguration
     */
    updateSystemConfiguration(info, returnResult = false) {
        return this.ajaxPut(`~/api/systemConfiguration?returnResult=${returnResult}`, info);
    }
}

export default WebRequestSystemConfiguration;
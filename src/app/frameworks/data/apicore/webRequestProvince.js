import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /Province 
 * 
 * @class WebRequestProvince
 * @extends {WebRequest}
 */
class WebRequestProvince extends WebRequestBase {

    /**
     * Creates an instance of WebRequestProvince.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Create singleton for WebRequestProvince.
     * 
     * @static
     * @returns
     */
    static getInstance() {
        return Singleton.getInstance("apicore-webrequestProvince", 
            new WebRequestProvince(WebConfig.appSettings.apiCoreUrl)
        );
    }

    /**
     * Get list of province.
     * 
     * @param {any} filter
     * @returns
     */
    listProvince(filter) {
        return this.ajaxPost("~/api/province/list", filter);
    }
    listRegion(filter) {
        return this.ajaxPost("~/api/region/list", filter);
    }
}

export default WebRequestProvince;
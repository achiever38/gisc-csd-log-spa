import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /Town 
 * 
 * @class WebRequestTown
 * @extends {WebRequest}
 */
class WebRequestTown extends WebRequestBase {

    /**
     * Creates an instance of WebRequestTown.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Create singleton for WebRequestTown.
     * 
     * @static
     * @returns
     */
    static getInstance() {
        return Singleton.getInstance("apicore-webrequestTown", 
            new WebRequestTown(WebConfig.appSettings.apiCoreUrl)
        );
    }

    /**
     * Get list of town.
     * 
     * @param {any} filter
     * @returns
     */
    listTown(filter) {
        return this.ajaxPost("~/api/town/list", filter);
    }
}

export default WebRequestTown;
import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /City 
 * 
 * @class webRequestDLTReport
 * @extends {WebRequest}
 */
class WebRequestDLTSendEmail extends WebRequestBase {

    /**
     * Creates an instance of WebRequestDLTSendEmail.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Create singleton for WebRequestDLTSendEmail.
     * 
     * @static
     * @returns
     */
    static getInstance() {
        return Singleton.getInstance("apicore-webRequestDLTSendEmail", 
            new WebRequestDLTSendEmail(WebConfig.appSettings.apiCoreUrl)
        );
    }

    /**
     * Get list of WebRequestDLTSendEmail.
     * 
     * @param {any} filter
     * @returns
     */
    listDLTSendEmailSummary(filter) {
        return [this.resolveUrl("~/api/DLTSendEmail/summary/list"),filter];
    }

    /**
     * Get list of WebRequestDLTSendEmail Grid.
     * 
     * @param {any} filter
     * @returns
     */
    listDLTSendEmailSummaryGrid(filter) {
        return this.ajaxPost("~/api/DLTSendEmail/summary/list",filter);
    }

    /**
     * Get vendorsignature of WebRequestDLTSendEmail.
     * 
     * @param {any} filter
     * @returns
     */
    listvendorsignature() {
        return this.ajaxPost("~/api/DLTSendEmail/vendorsignature");
    }

    /**
     * Get sendEmail of WebRequestDLTSendEmail.
     * 
     * @param {any} filter
     * @returns
     */
    sendEmail(filter) {
        return this.ajaxPost("~/api/DLTSendEmail/sendemail",filter);
    }

    /**
     * Get allyear of WebRequestDLTSendEmail.
     * 
     * @param {any} filter
     * @returns
     */
    allYear() {
        return this.ajaxPost("~/api/DLTSendEmail/allyear");
    }
    


}

    export default WebRequestDLTSendEmail;
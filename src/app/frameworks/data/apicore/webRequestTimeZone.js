﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /City 
 * 
 * @class WebRequestTimeZone
 * @extends {WebRequest}
 */
class WebRequestTimeZone extends WebRequestBase {

    /**
     * Creates an instance of WebRequestTimeZone.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Create singleton for WebRequestTimeZone.
     * 
     * @static
     * @returns
     */
    static getInstance() {
        return Singleton.getInstance("apicore-webrequestTimeZone", 
            new WebRequestTimeZone(WebConfig.appSettings.apiCoreUrl)
        );
    }

    /**
     * Get list of timeZone.
     * 
     * @param {any} filter
     * @returns
     */
    getAllTimeZones() {
        return this.ajaxGet("~/api/timeZone/all");
    }
}

export default WebRequestTimeZone;
import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /Subscription 
 * 
 * @class WebRequestSubscription
 * @extends {WebRequest}
 */
class WebRequestSubscription extends WebRequestBase {

    /**
     * Creates an instance of WebRequestSubscription.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Create singleton for WebRequestSubscription.
     * 
     * @static
     * @returns instance of WebRequestSubscription
     */
    static getInstance() {
        return Singleton.getInstance("apicore-webrequestSubscription", 
            new WebRequestSubscription(WebConfig.appSettings.apiCoreUrl)
        );
    }

    /**
     * Get list of subscription.
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf webrequestSubscription
     */
    listSubscription(filter) {
        return this.ajaxPost("~/api/subscription/list", filter);
    }

    /**
     * Get list of subscription summary
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf webrequestSubscription
     */
    listSubscriptionSummary(filter) {
        return this.ajaxPost("~/api/subscription/summary/list", filter);
    }

    /**
     * Get subscription by id
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf webrequestSubscription
     */
    getSubscription(id) {
        return this.ajaxGet(`~/api/subscription/${id}`);
    }

    /**
     * Create subscription with given subscription info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=true]
     * @returns
     * 
     * @memberOf webrequestSubscription
     */
    createSubscription(info, returnResult = true) {
        return this.ajaxPost(`~/api/subscription?returnResult=${returnResult}`, info);
    }

    /**
     * Update subscription with given subscription info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     * 
     * @memberOf webrequestSubscription
     */
    updateSubscription(info, returnResult = false) {
        return this.ajaxPut(`~/api/subscription?returnResult=${returnResult}`, info);
    }

    /**
     * Delete an subscription 
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf webrequestSubscription
     */
    deleteSubscription(id) {
        return this.ajaxDelete("~/api/subscription", [id]);
    }
}

export default WebRequestSubscription;
﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /DateTimeFormat 
 * 
 * @class WebRequestDateTimeFormat
 * @extends {WebRequest}
 */
class WebRequestDateTimeFormat extends WebRequestBase {

    /**
     * Creates an instance of WebRequestDateTimeFormat.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Create singleton for WebRequestDateTimeFormat.
     * 
     * @static
     * @returns
     */
    static getInstance() {
        return Singleton.getInstance("apicore-webrequestDateTimeFormat", 
            new WebRequestDateTimeFormat(WebConfig.appSettings.apiCoreUrl)
        );
    }

    /**
     * Get list of dateTimeFormat.
     * 
     * @param {any} filter
     * @returns
     */
    listDateTimeFormat(filter) {
        return this.ajaxPost("~/api/dateTimeFormat/list", filter);
    }
}

export default WebRequestDateTimeFormat;
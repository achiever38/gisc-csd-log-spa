import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /Group 
 * 
 * @class WebRequestGroup
 * @extends {WebRequest}
 */
class WebRequestGroup extends WebRequestBase {

    /**
     * Creates an instance of WebRequestGroup.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of WebRequestGroup.
     * 
     * @static
     * @returns instance of WebRequestGroup
     */
    static getInstance() {
        return Singleton.getInstance("apicore-webRequestGroup",
            new WebRequestGroup(WebConfig.appSettings.apiCoreUrl)
        );
    }
    
    /**
     * Get list of group in summary format.
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf webRequestGroup
     */
    listGroupSummary(filter) {
        return this.ajaxPost("~/api/group/summary/list", filter);
    }

     /**
     * Get group by id
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf webRequestGroup
     */
    getGroup(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/group/${id}`, includeAssociationNames);
    }

    /**
     * Create group with given group info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=true]
       * @returns
     * 
     * @memberOf webRequestGroup
     */
    createGroup(info, returnResult = true) {
        return this.ajaxPost(`~/api/group?returnResult=${returnResult}`, info);
    }

    /**
     * Update group with given group info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     * 
     * @memberOf webRequestGroup
     */
    updateGroup(info, returnResult = false) {
        return this.ajaxPut(`~/api/group?returnResult=${returnResult}`, info);
    }

    /**
     * Delete an group 
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf webRequestGroup
     */
    deleteGroup(id) {
        return this.ajaxDelete("~/api/group", [id]);
    }
}

export default WebRequestGroup;
﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /Company 
 * 
 * @class WebRequestCompany
 * @extends {WebRequest}
 */
class WebRequestSqlTemplate extends WebRequestBase {

    /**
     * Creates an instance of WebRequestCompany.
     * @private





     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Create singleton for WebRequestCompany.
     * 
     * @static
     * @returns
     */
    static getInstance() {
        return Singleton.getInstance("apicore-webrequestSqlTemplate", 
            new WebRequestSqlTemplate(WebConfig.appSettings.apiCoreUrl)
        );
    }

    /**
     * Get list of company in summary format.
     * 
     * @param {any} filter
     * @returns
     */
    listSqlTemplateSummary(filter) {
        return this.ajaxPost("~/api/sqltemplate/summary/list", filter);
    }


    /**
     * Get company information.
     * 
     * @param {any} id, companyId
     * @param {Array} includeAssociationNames, associations in company result.
     * @returns jQuery deferred.
     */
    getSqlTemplate(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/sqltemplate/${id}`, includeAssociationNames);
    }

    /**
     * 
     * Info = CompanyInfo
     * @param {any} info
     * @returns
     */

    createSqlTemplate (info, returnResult = false) {
        return this.ajaxPost(`~/api/sqltemplate?returnResult=${returnResult}`, info);
    }

    updateSqlTemplate (info, returnResult = false) {
        return this.ajaxPut(`~/api/sqltemplate?returnResult=${returnResult}`, info);
    }

    deleteSqlTemplate (id) {
        return this.ajaxDelete("~/api/sqltemplate", [id]);
    }

    validateSqlStatement (info){
        return this.ajaxPost("~/api/sqltemplate/validate",info);
    }
        /**
         * 
         * Get Company Setting
         * @param {any} id
         * @returns
         * 
         * @memberOf WebRequestCompany
         */
    }

    export default WebRequestSqlTemplate;
import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";
import { Enums } from "../../constant/apiConstant";

class WebRequestStringResource extends WebRequestBase {
    /**
     * Creates an instance of WebRequestStringResource.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of WebRequestStringResource.
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestStringResource
     */
    static getInstance() {
        return Singleton.getInstance("apicore-webrequestStringResource",
            new WebRequestStringResource(WebConfig.appSettings.apiCoreUrl));
    }

    /**
     * Get Resource for specific language.
     * @returns
     * 
     * @memberOf webRequestStringResource
     */
    list(resourceTypes, companyId = null) {
        // Do not send CultureCode.
        return this.ajaxPost("~/api/stringResource/list", {
            companyId: companyId,
            resourceTypes: resourceTypes
        });
    }

    /**
     * Get String Resources Summary.
     * @returns
     * 
     * @memberOf webRequestStringResource
     */
    listStringResourceSummary(filter) {
        return this.ajaxPost("~/api/stringResource/summary/list", filter);
    }

    /**
     * Export String Resources.
     * @returns
     * 
     * @memberOf webRequestStringResource
     */
    exportStringResource(filter) {
        return this.ajaxPost("~/api/stringResource/export", filter);
    }

    /**
     * Import String Resources.
     * @returns
     * 
     * @memberOf webRequestStringResource
     */
    importStringResource(info, save) {
        return this.ajaxPost(`~/api/stringResource/import?save=${save}`, info);
    }

    
    /**
     * Download String Resource Template.
     * @returns
     * 
     * @memberOf webRequestStringResource
     */
    downloadStringResourceTemplate(resourceType, templateFileType = Enums.TemplateFileType.Xlsx) {
        return this.ajaxGet(`~/api/stringResource/template/download/${resourceType}?templateFileType=${templateFileType}`);
    }
}

export default WebRequestStringResource;
import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /Vendor 
 * 
 * @class WebRequestVendor
 * @extends {WebRequestBase}
 */
class WebRequestBusinessUnitLevel extends WebRequestBase {

    /**
     * Creates an instance of WebRequestBusinessUnitLevel.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request business unit.
     * 
     * @static
     * @returns instance of WebRequestBusinessUnitLevel
     */
    static getInstance() {
        return Singleton.getInstance("apicore-webRequestBusinessUnitLevel",
            new WebRequestBusinessUnitLevel(WebConfig.appSettings.apiCoreUrl)
        );
    }

    /**
     * Get list of summary business unit.
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf webRequestBusinessUnitLevel
     */
    listBusinessUnitLevelSummary(filter) {
       return this.ajaxPost("~/api/businessUnitLevel/summary/list", filter);
    }

    /**
     * Create business unit with given business unit info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=true]
     * @returns
     * 
     * @memberOf webRequestBusinessUnitLevel
     */
    createBusinessUnitLevel(info, returnResult = false) {
        return this.ajaxPost(`~/api/businessUnitLevel?returnResult=${returnResult}`, info);
    }
    /**
     * Update business unit with given business unit info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=true]
     * @returns
     * 
     * @memberOf webRequestBusinessUnitLevel
     */
    updateBusinessUnitLevel(info, returnResult = false) {
        return this.ajaxPut(`~/api/businessUnitLevel?returnResult=${returnResult}`, info);
    }
    /**
     * Delete an business unit 
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf webRequestBusinessUnitLevel
     */
    deleteBusinessUnitLevel(id) {
        return this.ajaxDelete("~/api/businessUnitLevel", [id]);
    }
}

export default WebRequestBusinessUnitLevel;
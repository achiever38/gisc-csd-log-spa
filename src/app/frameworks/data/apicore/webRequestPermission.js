import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebRequestUser
 * @extends {WebRequestBase}
 */
class WebRequestPermission extends WebRequestBase {

    /**
     * Creates an instance of WebRequestUser.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request permission.
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestPermission
     */
    static getInstance() {
        return Singleton.getInstance("apicore-webrequestPermission",
            new WebRequestPermission(WebConfig.appSettings.apiCoreUrl)
        );
    }

    /**
     * Geting permission.
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf webrequestPermission
     */
    listPermission(filter) {
        return this.ajaxPost("~/api/permission/list", filter);
    }
}

export default WebRequestPermission;
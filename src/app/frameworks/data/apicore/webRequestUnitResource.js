﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /UnitResource 
 * 
 * @class WebRequestUnitResource
 * @extends {WebRequest}
 */
class WebRequestUnitResource extends WebRequestBase {

    /**
     * Creates an instance of WebRequestUnitResource.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Create singleton for WebRequestUnitResource.
     * 
     * @static
     * @returns
     */
    static getInstance() {
        return Singleton.getInstance("apicore-webrequestUnitResource", 
            new WebRequestUnitResource(WebConfig.appSettings.apiCoreUrl)
        );
    }

    /**
     * Get list of unitResource.
     * 
     * @param {any} filter
     * @returns
     */
    listUnitResource(filter) {
        return this.ajaxPost("~/api/unitResource/list", filter);
    }
}

export default WebRequestUnitResource;
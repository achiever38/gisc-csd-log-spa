﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /Language 
 * 
 * @class WebRequestLanguage
 * @extends {WebRequestBase}
 */
class WebRequestLanguage extends WebRequestBase {

    /**
     * Creates an instance of WebRequestLanguage.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request language.
     * 
     * @static
     * @returns instance of WebRequestLanguage
     */
    static getInstance() {
        return Singleton.getInstance("apicore-webRequestLanguage",
            new WebRequestLanguage(WebConfig.appSettings.apiCoreUrl)
        );
    }

    /**
     * Get list of language.
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf webRequestLanguage
     */
    listLanguage(filter) {
        return this.ajaxPost("~/api/language/list", filter);
    }

    /**
     * Get all languages.
     * @returns
     * 
     * @memberOf webRequestLanguage
     */
    getAllLanguages() {
        return this.ajaxGet("~/api/language/all");
    }
    /**
     * Enable available languages with given infos
     * 
     * @param {any} info
     * @returns
     * 
     * @memberOf webRequestLanguage
     */
    enableAvailableLanguages(infos, returnResult = false) {
        return this.ajaxPost(`~/api/language/available/enable?returnResult=${returnResult}`, infos);
    }
}

export default WebRequestLanguage;
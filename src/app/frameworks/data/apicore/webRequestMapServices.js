﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /Subscription 
 * 
 * @class WebRequestMapServices
 * @extends {WebRequest}
 */
class WebRequestMapServices extends WebRequestBase {

    /**
     * Creates an instance of WebRequestMapServices.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Create singleton for WebRequestMapServices.
     * 
     * @static
     * @returns instance of WebRequestMapServices
     */
    static getInstance() {
        return Singleton.getInstance("apicore-webRequestMapServices",
            new WebRequestMapServices(WebConfig.appSettings.apiCoreUrl)
        );
    }

    /**
     * Get list of subscription.
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf WebRequestMapServices
     */
    listDefaultMapServices(filter) {
        return this.ajaxPost("~/api/company/mapservices/default/summary/list", filter);
    }

    /**
     * Get list of subscription summary
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf WebRequestMapServices
     */
    listAdditionalMapServices(filter) {
        return this.ajaxPost("~/api/company/mapservices/additional/summary/list", filter);
    }

}

export default WebRequestMapServices;
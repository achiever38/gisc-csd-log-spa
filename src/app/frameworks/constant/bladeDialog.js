export const DIALOG_YESNO = "yn";
export const DIALOG_OKCANCEL = "oc";
export const DIALOG_OK = "ok";

export const BUTTON_YES = "yes";
export const BUTTON_NO = "no";
export const BUTTON_OK = "ok";
export const BUTTON_CANCEL = "cancel";
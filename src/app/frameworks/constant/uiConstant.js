export default class UIConstants {
    static get SignalRNotificationType () {
        return class SignalRNotificationType {
            static get Alert () { return 0;}
            static get Announcement () { return 1;}
            static get Maintenance () { return 2;}
            static get UrgentAlert () { return 3;}
        };
    }

    static get SignalRTrackingType () {
        return class SignalRTrackingType {
            static get Fleet () { return 0;}
            static get MobileService () { return 1;}
        };
    }

    // https://datatables.net/reference/event/error
    static get DataTableErrorMode () {
        return class DataTableErrorMode {
            static get Alert () { return 'alert';}
            static get Throw () { return 'throw';}
            static get None () { return 'none';}
        };
    }

    static get Themes() {
        return class Themes {
            static get Default () { return 'app-theme-gis';}
        }
    }

    static get LogoLayout(){
        return class LogoLayout {
            static get Default () { return 'logo-layout-s'; }
        }
    }

    static get POISuggestion() {
        return class POISuggestion {
            static get BufferRadius() { return 500; }
            static get BufferRadiusUnit() { return 'meters'; }
            static get ZoomLevel() { return 19; }            
        }
    }

    static get ToastrType() {
        return class ToastrType {
            static get Success() { return "success"; }
            static get Info() { return "info"; }
            static get Warning() { return "warning"; }
            static get Error() { return "error"; }
        }
    }

    static get AlertCategoies() {
        return class AlertCategoies {
            static get Rank() {
                return [{
                    id: 1,
                    value: 'A'
                }, {
                    id: 2,
                    value: 'B'
                }, {
                    id: 3,
                    value: 'C'
                }, {
                    id: 4,
                    value: 'D'
                }, {
                    id: 5,
                    value: 'E'
                }, {
                    id: 6,
                    value: 'F'
                }, {
                    id: 7,
                    value: 'G'
                }, {
                    id: 8,
                    value: 'H'
                }, {
                    id: 9,
                    value: 'I'
                }, {
                    id: 10,
                    value: 'J'
                }, {
                    id: 11,
                    value: 'K'
                }, {
                    id: 12,
                    value: 'L'
                }, {
                    id: 13,
                    value: 'M'
                }, {
                    id: 14,
                    value: 'N'
                }, {
                    id: 15,
                    value: 'O'
                }, {
                    id: 16,
                    value: 'P'
                }, {
                    id: 17,
                    value: 'Q'
                }, {
                    id: 18,
                    value: 'R'
                }, {
                    id: 19,
                    value: 'S'
                }, {
                    id: 20,
                    value: 'T'
                }, {
                    id: 21,
                    value: 'U'
                }, {
                    id: 22,
                    value: 'V'
                }, {
                    id: 23,
                    value: 'W'
                }, {
                    id: 24,
                    value: 'X'
                }, {
                    id: 25,
                    value: 'Y'
                }, {
                    id: 26,
                    value: 'Z'
                }, ];
            }
        }
    }

    static get SimType(){
        return [
            {
                id: 1,
                name: "Box"
            },
            {
                id: 2,
                name: "MDVR"
            },
            {
                id: 3,
                name: "DMS"
            },
            {
                id: 4,
                name: "ADAS"
            }
        ]
    }
}
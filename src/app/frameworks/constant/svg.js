import IconMovementTemplateMarkup from "text!../../../components/svgs/icon-movement.html";
import IconSpeedTemplateMarkup from "text!../../../components/svgs/icon-speed.html";
import IconDirectionTemplateMarkup from "text!../../../components/svgs/icon-direction.html";
import IconDirectionNTemplateMarkup from "text!../../../components/svgs/icon-direction-n.html";
import IconDirectionNETemplateMarkup from "text!../../../components/svgs/icon-direction-ne.html";
import IconDirectionETemplateMarkup from "text!../../../components/svgs/icon-direction-e.html";
import IconDirectionSETemplateMarkup from "text!../../../components/svgs/icon-direction-se.html";
import IconDirectionSTemplateMarkup from "text!../../../components/svgs/icon-direction-s.html";
import IconDirectionSWTemplateMarkup from "text!../../../components/svgs/icon-direction-sw.html";
import IconDirectionWTemplateMarkup from "text!../../../components/svgs/icon-direction-w.html";
import IconDirectionNWTemplateMarkup from "text!../../../components/svgs/icon-direction-nw.html";
import IconGpsTemplateMarkup from "text!../../../components/svgs/icon-gps.html";
import IconGsmBadTemplateMarkup from "text!../../../components/svgs/icon-gsm-bad.html";
import IconGsmMediumTemplateMarkup from "text!../../../components/svgs/icon-gsm-medium.html";
import IconGsmGoodTemplateMarkup from "text!../../../components/svgs/icon-gsm-good.html";
import IconGsmTemplateMarkup from "text!../../../components/svgs/icon-gsm.html";
import IconAltitudeTemplateMarkup from "text!../../../components/svgs/icon-altitude.html";
import IconDltStatusTemplateMarkup from "text!../../../components/svgs/icon-dltstatus.html";
import IconDltStatusSuccessTemplateMarkup from "text!../../../components/svgs/icon-dltstatus-success.html";
import IconDltStatusFailTemplateMarkup from "text!../../../components/svgs/icon-dltstatus-fail.html";
import IconEngineTemplateMarkup from "text!../../../components/svgs/icon-engine.html";
import IconGateTemplateMarkup from "text!../../../components/svgs/icon-gate.html";
import IconGateOnTemplateMarkup from "text!../../../components/svgs/icon-gate-on.html";
import IconGateOffTemplateMarkup from "text!../../../components/svgs/icon-gate-off.html";
import IconFuelTemplateMarkup from "text!../../../components/svgs/icon-fuel.html";
import IconTemparetureTemplateMarkup from "text!../../../components/svgs/icon-temperature.html";
import IconVehicleBatteryTemplateMarkup from "text!../../../components/svgs/icon-vehiclebattery.html";
import IconDeviceBatteryTemplateMarkup from "text!../../../components/svgs/icon-devicebattery.html";
import IconSosTemplateMarkup from "text!../../../components/svgs/icon-sos.html";
import IconPowerTemplateMarkup from "text!../../../components/svgs/icon-power.html";
import IconDriverCardTemplateMarkup from "text!../../../components/svgs/icon-drivercard.html";
import IconPassengerCardTemplateMarkup from "text!../../../components/svgs/icon-passengercard.html";
import IconMileageTemplateMarkup from "text!../../../components/svgs/icon-mileage.html";
import IconCustomFeatureTemplateMarkup from "text!../../../components/svgs/icon-customfeature.html";
import IconEventTemplateMarkup from "text!../../../components/svgs/icon-event.html";
import IconDelayTemplateMarkup from "text!../../../components/svgs/icon-delay.html";
import IconMovementOver35HrsTemplateMarkup from "text!../../../components/svgs/ic_vehicle_over3.5.html";
import IconMovementOver4HrsTemplateMarkup from "text!../../../components/svgs/ic_vehicle_over4.html";

// Notification Icon Markups.
import IconAccRapidMarkUp from "text!../../../components/svgs/icon-noti-01.html";
import IconAcquiredGPSMarkUp from "text!../../../components/svgs/icon-noti-02.html";
import IconBadGPSMarkUp from "text!../../../components/svgs/icon-noti-03.html";
import IconVehicleBatteryLowMarkUp from "text!../../../components/svgs/icon-noti-04.html";
import IconDeviceBatteryLowMarkUp from "text!../../../components/svgs/icon-noti-05.html";
import IconDecRapidMarkUp from "text!../../../components/svgs/icon-noti-06.html";
import IconEngineIdleMarkUp from "text!../../../components/svgs/icon-noti-07.html";
import IconEngineOffMarkUp from "text!../../../components/svgs/icon-noti-08.html";
import IconEngineOnMarkUp from "text!../../../components/svgs/icon-noti-09.html";
import IconGateCloseMarkUp from "text!../../../components/svgs/icon-noti-10.html";
import IconGateOpenMarkUp from "text!../../../components/svgs/icon-noti-11.html";
import IconGPSDisCntMarkUp from "text!../../../components/svgs/icon-noti-12.html";
import IconGPSReCntMarkUp from "text!../../../components/svgs/icon-noti-13.html";
import IconMoveMarkUp from "text!../../../components/svgs/icon-noti-14.html";
import IconOverSpeedMarkUp from "text!../../../components/svgs/icon-noti-15.html";
import IconOverTemperatureMarkUp from "text!../../../components/svgs/icon-noti-16.html";
import IconPowerDisCntMarkUp from "text!../../../components/svgs/icon-noti-17.html";
import IconPowerReCntMarkUp from "text!../../../components/svgs/icon-noti-18.html";
import IconStopMarkUp from "text!../../../components/svgs/icon-noti-19.html";
import IconSwerveMarkUp from "text!../../../components/svgs/icon-noti-20.html";
import IconUnderTemperatureMarkUp from "text!../../../components/svgs/icon-noti-21.html";
import IconUnsFuelDecMarkUp from "text!../../../components/svgs/icon-noti-22.html";
import IconUnsStopMarkUp from "text!../../../components/svgs/icon-noti-23.html";
import IconCloseGateInsPOIMarkUp from "text!../../../components/svgs/icon-noti-24.html";
import IconCloseGateOtsPOIMarkUp from "text!../../../components/svgs/icon-noti-25.html";
import IconJobAvgSpeedMarkUp from "text!../../../components/svgs/icon-noti-26.html";
import IconJobTotDistMarkUp from "text!../../../components/svgs/icon-noti-27.html";
import IconJobTotTimeMarkUp from "text!../../../components/svgs/icon-noti-28.html";
import IconOpenGateInsPOIMarkUp from "text!../../../components/svgs/icon-noti-29.html";
import IconOpenGateOtsPOIMarkUp from "text!../../../components/svgs/icon-noti-30.html";
import IconOverTimeParkMarkUp from "text!../../../components/svgs/icon-noti-31.html";
import IconIncmWPMarkUp from "text!../../../components/svgs/icon-noti-32.html";
import IconOutgWPMarkUp from "text!../../../components/svgs/icon-noti-33.html";
import IconDispatcherMarkUp from "text!../../../components/svgs/icon-noti-34.html";
import IconUrgentMarkUp from "text!../../../components/svgs/icon-noti-35.html";
import IconMaintenanceMarkUp from "text!../../../components/svgs/icon-noti-36.html";
import IconSOSMarkUp from "text!../../../components/svgs/icon-noti-37.html";
import IconParkingMarkUp from "text!../../../components/svgs/icon-noti-38.html";
import IconPOIParkingMarkUp from "text!../../../components/svgs/icon-noti-39.html";
import IconPOIInOutMarkUp from "text!../../../components/svgs/icon-noti-40.html";
import IconAreaParkingMarkUp from "text!../../../components/svgs/icon-noti-41.html";
import IconAreaInOutVehicleMarkUp from "text!../../../components/svgs/icon-noti-42.html";
import IconAreaInOutJobMarkUp from "text!../../../components/svgs/icon-noti-43.html";
import IconMoveNonWorkingMarkUp from "text!../../../components/svgs/icon-noti-44.html";
import IconExceedLimitDrivingMarkUp from "text!../../../components/svgs/icon-noti-45.html";
import IconInvalidDriverMarkUp from "text!../../../components/svgs/icon-noti-46.html";
import IconCustomAlertMarkUp from "text!../../../components/svgs/icon-noti-47.html";
import IconExceedLimitDrivingIn24HoursMarkUp from "text!../../../components/svgs/icon-noti-48.html";
import IconTemperatureErrorMarkUp from "text!../../../components/svgs/icon-noti-49.html";
 import IconAccelerationMarkUp from "text!../../../components/svgs/icon-noti-50.html";
 import IconBreakingMarkUp from "text!../../../components/svgs/icon-noti-51.html";
 import IconTurnMarkUp from "text!../../../components/svgs/icon-noti-52.html";
 import IconWideTurnMarkUp from "text!../../../components/svgs/icon-noti-53.html";
 import IconSwerveWhileAcceleratingMarkUp from "text!../../../components/svgs/icon-noti-54.html";
 import IconWideSwerveWhileAcceleratingMarkUp from "text!../../../components/svgs/icon-noti-55.html";
 import IconSwerveWhileDeceleratingMarkUp from "text!../../../components/svgs/icon-noti-56.html";
 import IconWideSwerveWhileDeceleratingMarkUp from "text!../../../components/svgs/icon-noti-57.html";
 import IconRoundaboutMarkUp from "text!../../../components/svgs/icon-noti-58.html";
 import IconLaneChangeMarkUp from "text!../../../components/svgs/icon-noti-59.html";
 import IconBypassingMarkUp from "text!../../../components/svgs/icon-noti-60.html";
 import IconSpeedBumpMarkUp from "text!../../../components/svgs/icon-noti-61.html";
 import IconAccidentSuspiciousMarkUp from "text!../../../components/svgs/icon-noti-62.html";
 import IconRealAccidentMarkUp from "text!../../../components/svgs/icon-noti-63.html";
 import IconGreenBandDrivingMarkUp from "text!../../../components/svgs/icon-noti-64.html";
 import IconOverRPMMarkUp from "text!../../../components/svgs/icon-noti-65.html";
 import IconNoSeatBeltMarkUp from "text!../../../components/svgs/icon-noti-66.html";
 import IconFCWMarkUp from "text!../../../components/svgs/icon-noti-67.html";
 import IconUFCWMarkUp from "text!../../../components/svgs/icon-noti-68.html";
 import IconPCWMarkUp from "text!../../../components/svgs/icon-noti-69.html";
 import IconLDWMarkUp from "text!../../../components/svgs/icon-noti-70.html";
 import IconHMWMarkUp from "text!../../../components/svgs/icon-noti-71.html";
 import IconTSRMarkUp from "text!../../../components/svgs/icon-noti-72.html";
 import IconTowMarkUp from "text!../../../components/svgs/icon-noti-73.html";
 import IconUnauthorizedStopMarkUp from "text!../../../components/svgs/icon-noti-74.html";
 import IconFatigueOtherMarkUp from "text!../../../components/svgs/icon-noti-75.html";
 import IconFatigueMicroSleepMarkUp from "text!../../../components/svgs/icon-noti-76.html";
 import IconFatigueYawningMarkUp from "text!../../../components/svgs/icon-noti-77.html";
 import IconFatigueDrowsinessMarkUp from "text!../../../components/svgs/icon-noti-78.html";
 import IconDistractionMarkUp from "text!../../../components/svgs/icon-noti-79.html";
 import IconFieldOfViewExceptionMarkUp from "text!../../../components/svgs/icon-noti-80.html";

//AnnouncementIcon
import IconAnnouncement from "text!../../../components/svgs/icon-noti-announcement.html";
import IconMaintenancePlan from "text!../../../components/svgs/icon-noti-maintenance.html";

//Company Loading Icon
import GISCompanyLoadingIcon from "text!../../../components/svgs/company-loading/app-theme-gis.html";
import SCCCCompanyLoadingIcon from "text!../../../components/svgs/company-loading/app-theme-sccc.html";
import KubotaCompanyLoadingIcon from "text!../../../components/svgs/company-loading/app-theme-kubota.html";

//Company Logo Icon
import GISCompanyLogoIcon from "text!../../../components/svgs/company-logo/app-theme-gis.html";
import SCCCCompanyLogoIcon from "text!../../../components/svgs/company-logo/app-theme-sccc.html";
import KubotaCompanyLogoIcon from "text!../../../components/svgs/company-logo/app-theme-kubota.html";
import PEACompanyLogoIcon from "text!../../../components/svgs/company-logo/app-theme-pea.html";
import BoonrawdCompanyLogoIcon from "text!../../../components/svgs/company-logo/app-theme-boonrawd.html";


export class AssetIcons {
    static get IconMovementTemplateMarkup () {return IconMovementTemplateMarkup;}
    static get IconSpeedTemplateMarkup () {return IconSpeedTemplateMarkup;}
    static get IconDirectionTemplateMarkup () {return IconDirectionTemplateMarkup;}
    static get IconDirectionNTemplateMarkup () {return IconDirectionNTemplateMarkup;}
    static get IconDirectionNETemplateMarkup () {return IconDirectionNETemplateMarkup;}
    static get IconDirectionETemplateMarkup () {return IconDirectionETemplateMarkup;}
    static get IconDirectionSETemplateMarkup () {return IconDirectionSETemplateMarkup;}
    static get IconDirectionSTemplateMarkup () {return IconDirectionSTemplateMarkup;}
    static get IconDirectionSWTemplateMarkup () {return IconDirectionSWTemplateMarkup;}
    static get IconDirectionWTemplateMarkup () {return IconDirectionWTemplateMarkup;}
    static get IconDirectionNWTemplateMarkup () {return IconDirectionNWTemplateMarkup;}
    static get IconGpsTemplateMarkup () {return IconGpsTemplateMarkup;}
    static get IconGsmBadTemplateMarkup () {return IconGsmBadTemplateMarkup;}
    static get IconGsmMediumTemplateMarkup () {return IconGsmMediumTemplateMarkup;}
    static get IconGsmGoodTemplateMarkup () {return IconGsmGoodTemplateMarkup;}
    static get IconGsmTemplateMarkup () {return IconGsmTemplateMarkup;}
    static get IconAltitudeTemplateMarkup () {return IconAltitudeTemplateMarkup;}
    static get IconDltStatusTemplateMarkup () {return IconDltStatusTemplateMarkup;}
    static get IconDltStatusSuccessTemplateMarkup () {return IconDltStatusSuccessTemplateMarkup;}
    static get IconDltStatusFailTemplateMarkup () {return IconDltStatusFailTemplateMarkup;}
    static get IconEngineTemplateMarkup () {return IconEngineTemplateMarkup;}
    static get IconGateTemplateMarkup () {return IconGateTemplateMarkup;}
    static get IconGateOnTemplateMarkup () {return IconGateOnTemplateMarkup;}
    static get IconGateOffTemplateMarkup () {return IconGateOffTemplateMarkup;}
    static get IconFuelTemplateMarkup () {return IconFuelTemplateMarkup;}
    static get IconTemparetureTemplateMarkup () {return IconTemparetureTemplateMarkup;}
    static get IconVehicleBatteryTemplateMarkup () {return IconVehicleBatteryTemplateMarkup;}
    static get IconDeviceBatteryTemplateMarkup () {return IconDeviceBatteryTemplateMarkup;}
    static get IconSosTemplateMarkup () {return IconSosTemplateMarkup;}
    static get IconPowerTemplateMarkup () {return IconPowerTemplateMarkup;}
    static get IconDriverCardTemplateMarkup () {return IconDriverCardTemplateMarkup;}
    static get IconPassengerCardTemplateMarkup () {return IconPassengerCardTemplateMarkup;}
    static get IconMileageTemplateMarkup () {return IconMileageTemplateMarkup;}
    static get IconCustomFeatureTemplateMarkup () {return IconCustomFeatureTemplateMarkup;}
    static get IconEventTemplateMarkup () {return IconEventTemplateMarkup;}
    static get IconDelayTemplateMarkup() { return IconDelayTemplateMarkup; }
    static get IconMovementOver4HrsTemplateMarkup() { return IconMovementOver4HrsTemplateMarkup; }
    static get IconMovementOver35HrsTemplateMarkup() { return IconMovementOver35HrsTemplateMarkup; }
}

export class Asset2Icons {
    static get IconMovementTemplateMarkup () {return IconMovementTemplateMarkup;}
}

export class NotificationTypeIcons {
    static get IconAnnouncement () {return IconAnnouncement;}
    static get IconMaintenancePlan () {return IconMaintenancePlan;}
}

export class NotificationIcons {
    static get AccRapid () {return IconAccRapidMarkUp;}
    static get AcquiredGPS () {return IconAcquiredGPSMarkUp;}
    static get BadGPS () {return IconBadGPSMarkUp;}
    static get VehicleBatteryLow () {return IconVehicleBatteryLowMarkUp;}
    static get DeviceBatteryLow () {return IconDeviceBatteryLowMarkUp;}
    static get DecRapid () {return IconDecRapidMarkUp;}
    static get EngineIdle () {return IconEngineIdleMarkUp;}
    static get EngineOff () {return IconEngineOffMarkUp;}
    static get EngineOn () {return IconEngineOnMarkUp;}
    static get GateClose () {return IconGateCloseMarkUp;}
    static get GateOpen () {return IconGateOpenMarkUp;}
    static get GPSDisCnt () {return IconGPSDisCntMarkUp;}
    static get GPSReCnt () {return IconGPSReCntMarkUp;}
    static get Move () {return IconMoveMarkUp;}
    static get OverSpeed () {return IconOverSpeedMarkUp;}
    static get OverTemperature () {return IconOverTemperatureMarkUp;}
    static get PowerDisCnt () {return IconPowerDisCntMarkUp;}
    static get PowerReCnt () {return IconPowerReCntMarkUp;}
    static get Stop () {return IconStopMarkUp;}
    static get Swerve () {return IconSwerveMarkUp;}
    static get UnderTemperature () {return IconUnderTemperatureMarkUp;}
    static get UnsFuelDec () {return IconUnsFuelDecMarkUp;}
    static get UnsStop () {return IconUnsStopMarkUp;}
    static get CloseGateInsPOI () {return IconCloseGateInsPOIMarkUp;}
    static get CloseGateOtsPOI () {return IconCloseGateOtsPOIMarkUp;}
    static get JobAvgSpeed () {return IconJobAvgSpeedMarkUp;}
    static get JobTotDist () {return IconJobTotDistMarkUp;}
    static get JobTotTime () {return IconJobTotTimeMarkUp;}
    static get OpenGateInsPOI () {return IconOpenGateInsPOIMarkUp;}
    static get OpenGateOtsPOI () {return IconOpenGateOtsPOIMarkUp;}
    static get OverTimePark () {return IconOverTimeParkMarkUp;}
    static get IncmWP () {return IconIncmWPMarkUp;}
    static get OutgWP () {return IconOutgWPMarkUp;}
    static get Dispatcher () {return IconDispatcherMarkUp;}
    static get Urgent () {return IconUrgentMarkUp;}
    static get Maintenance () {return IconMaintenanceMarkUp;}
    static get SOS () {return IconSOSMarkUp;}
    static get Parking () {return IconParkingMarkUp;}
    static get POIParking () {return IconPOIParkingMarkUp;}
    static get POIInOut () {return IconPOIInOutMarkUp;}
    static get AreaParking () {return IconAreaParkingMarkUp;}
    static get AreaInOutVehicle () {return IconAreaInOutVehicleMarkUp;}
    static get AreaInOutJob () {return IconAreaInOutJobMarkUp;}
    static get MoveNonWorking () {return IconMoveNonWorkingMarkUp;}
    static get ExceedLimitDriving () {return IconExceedLimitDrivingMarkUp;}
    static get InvalidDriver () {return IconInvalidDriverMarkUp;}
    static get CustomAlert () {return IconCustomAlertMarkUp;}
    static get ExceedLimitDrivingIn24Hours () {return IconExceedLimitDrivingIn24HoursMarkUp;}
    static get TemperatureError () {return IconTemperatureErrorMarkUp;}
    static get Acceleration () {return IconAccelerationMarkUp;}
    static get Breaking () {return IconBreakingMarkUp;}
    static get Turn () {return IconTurnMarkUp;}
    static get WideTurn () {return IconWideTurnMarkUp;}
    static get SwerveWhileAccelerating () {return IconSwerveWhileAcceleratingMarkUp;}
    static get WideSwerveWhileAccelerating () {return IconWideSwerveWhileAcceleratingMarkUp;}
    static get SwerveWhileDecelerating () {return IconSwerveWhileDeceleratingMarkUp;}
    static get WideSwerveWhileDecelerating () {return IconWideSwerveWhileDeceleratingMarkUp;}
    static get Roundabout () {return IconRoundaboutMarkUp;}
    static get LaneChange () {return IconLaneChangeMarkUp;}
    static get Bypassing () {return IconBypassingMarkUp;}
    static get AccidentSuspicious() {return IconSpeedBumpMarkUp;}
    static get SpeedBump () {return IconAccidentSuspiciousMarkUp;}
    static get RealAccident () {return IconRealAccidentMarkUp;}
    static get GreenBandDriving () {return IconGreenBandDrivingMarkUp;}
    static get OverRPM () {return IconOverRPMMarkUp;}
    static get NoSeatBelt () {return IconNoSeatBeltMarkUp;}
    static get FCW () {return IconFCWMarkUp;}
    static get UFCW () {return IconUFCWMarkUp;}
    static get PCW () {return IconPCWMarkUp;}
    static get LDW () {return IconLDWMarkUp;}
    static get HMW () {return IconHMWMarkUp;}
    static get TSR () {return IconTSRMarkUp;}
    static get Tow () {return IconTowMarkUp;}
    static get UnauthorizedStop () {return IconUnauthorizedStopMarkUp;}
    static get FatigueOther () {return IconFatigueOtherMarkUp;}
    static get FatigueMicroSleep () {return IconFatigueMicroSleepMarkUp;}
    static get FatigueYawning () {return IconFatigueYawningMarkUp;}
    static get FatigueDrowsiness () {return IconFatigueDrowsinessMarkUp;}
    static get Distraction () {return IconDistractionMarkUp;}
    static get FieldOfViewException () {return IconFieldOfViewExceptionMarkUp;}
}

export class CompanyIcons {
    static get LoadingLogo() { 
        return {
            "app-theme-gis" : GISCompanyLoadingIcon,
            "app-theme-sccc" : SCCCCompanyLoadingIcon,
            "app-theme-kubota" : KubotaCompanyLoadingIcon
        }; 
    }
    static get AppLogo() {  
        return {
            "app-theme-gis" : GISCompanyLogoIcon,
            "app-theme-sccc" : SCCCCompanyLogoIcon,
            "app-theme-kubota" : KubotaCompanyLogoIcon,
            "app-theme-pea": PEACompanyLogoIcon,
            "app-theme-boonrawd": BoonrawdCompanyLogoIcon
        }; 
    }
}
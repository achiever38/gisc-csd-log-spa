import {Enums} from "./apiConstant";

class SortingColumnInfo {
    constructor(column, direction) {
        this.column = column;
        this.direction = direction;
    }
}

class DefaultSorting {
    static get Box() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Imei, Enums.SortingDirection.Ascending)
        ];
    }
    static get BoxBySerialNo() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.SerialNo, Enums.SortingDirection.Ascending)
        ];
    }
    static get BoxFeature() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Name, Enums.SortingDirection.Ascending)
        ];
    }
    static get BoxMaintenance() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.DateTime, Enums.SortingDirection.Descending)
        ];
    }
    static get BoxModel() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Name, Enums.SortingDirection.Ascending)
        ];
    }
    static get BoxStock() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Name, Enums.SortingDirection.Ascending)
        ];
    }
    static get BoxTemplate() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Name, Enums.SortingDirection.Ascending)
        ];
    }
    static get Feature() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Name, Enums.SortingDirection.Ascending)
        ];
    }
    static get Vehicle() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.License, Enums.SortingDirection.Ascending)
        ];
    }
    static get VehicleByLicenseReferenceNo() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.License, Enums.SortingDirection.Ascending),
            new SortingColumnInfo(Enums.SortingColumnName.ReferenceNo, Enums.SortingDirection.Ascending)
        ]
    }
    static get VehicleModel() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Brand, Enums.SortingDirection.Ascending)
        ];
    }
    static get VehicleModelByBrandModel() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Brand, Enums.SortingDirection.Ascending),
            new SortingColumnInfo(Enums.SortingColumnName.Model, Enums.SortingDirection.Ascending)
        ];
    }
    static get VehicleModelByBrandModelCode() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Brand, Enums.SortingDirection.Ascending),
            new SortingColumnInfo(Enums.SortingColumnName.Model, Enums.SortingDirection.Ascending),
            new SortingColumnInfo(Enums.SortingColumnName.Code, Enums.SortingDirection.Ascending)
        ];
    }
    static get VehicleIcon() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Name, Enums.SortingDirection.Ascending)
        ];
    }
    static get VehicleIconByNameCode() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Name, Enums.SortingDirection.Ascending),
            new SortingColumnInfo(Enums.SortingColumnName.Code, Enums.SortingDirection.Ascending),
        ];
    }
    static get BusinessUnit() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Name, Enums.SortingDirection.Ascending)
        ];
    }
    static get FleetService() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Mobile, Enums.SortingDirection.Ascending)
        ];
    }
    static get MobileService() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Mobile, Enums.SortingDirection.Ascending)
        ];
    }
    static get SMSService() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.StartDate, Enums.SortingDirection.Descending)
        ];
    }
    static get User() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Username, Enums.SortingDirection.Ascending)
        ];
    }
    static get Subscription() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Name, Enums.SortingDirection.Ascending)
        ];
    }
    static get Group() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Name, Enums.SortingDirection.Ascending)
        ];
    }
    static get OperatorPackage() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Operator, Enums.SortingDirection.Ascending)
        ];
    }
    static get OperatorPackageByOperatorPromotion() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Operator, Enums.SortingDirection.Ascending),
            new SortingColumnInfo(Enums.SortingColumnName.Promotion, Enums.SortingDirection.Ascending)
        ];
    }
    static get SystemSetting() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Name, Enums.SortingDirection.Ascending)
        ];
    }
    static get Vendor() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.VendorId, Enums.SortingDirection.Ascending)
        ];
    }
    static get Language() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Name, Enums.SortingDirection.Ascending)
        ];
    }
    static get LanguageByCode() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Code, Enums.SortingDirection.Ascending)
        ];
    }
    static get EnumResource() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Order, Enums.SortingDirection.Ascending)
        ];
    }
    static get Company() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Id, Enums.SortingDirection.Ascending)
        ];
    }
    static get CompanyByName() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Name, Enums.SortingDirection.Ascending)
        ];
    }
    static get Country() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Name, Enums.SortingDirection.Ascending)
        ];
    }
    static get Province() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Name, Enums.SortingDirection.Ascending)
        ];
    }
    static get City() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Name, Enums.SortingDirection.Ascending)
        ];
    }
    static get Town() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Name, Enums.SortingDirection.Ascending)
        ];
    }
    static get UnitResource() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Order, Enums.SortingDirection.Ascending)
        ];
    }
    static get Driver() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.EmployeeId, Enums.SortingDirection.Ascending)
        ];
    }
    static get DriverByFirstNameLastName() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.FirstName, Enums.SortingDirection.Ascending),
            new SortingColumnInfo(Enums.SortingColumnName.LastName, Enums.SortingDirection.Ascending)
        ];
    }
    static get DriverByFirstNameLastNameEmployeeID() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.FirstName, Enums.SortingDirection.Ascending),
            new SortingColumnInfo(Enums.SortingColumnName.LastName, Enums.SortingDirection.Ascending),
            new SortingColumnInfo(Enums.SortingColumnName.EmployeeId, Enums.SortingDirection.Ascending)
        ];
    }
    static get DrivePerformanceRule() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Name, Enums.SortingDirection.Ascending)
        ];
    }
    static get DrivePerformanceRuleAlertConfiguration() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.AlertType, Enums.SortingDirection.Ascending)
        ];
    }
    static get AlertConfiguration() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.AlertTypeId, Enums.SortingDirection.Ascending)
        ];
    }
    static get Permission() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Order, Enums.SortingDirection.Ascending)
        ];
    }
    static get CustomPOI() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Name, Enums.SortingDirection.Ascending)
        ];
    }
    static get CustomPOICategory() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Name, Enums.SortingDirection.Ascending)
        ];
    }
    static get CustomArea() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Name, Enums.SortingDirection.Ascending)
        ];
    }
    static get CustomAreaTypeCategory() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Name, Enums.SortingDirection.Ascending)
        ];
    }
    static get CustomAreaType() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.Order, Enums.SortingDirection.Ascending)
        ];
    }

    static get TrackLocation() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.DateTime, Enums.SortingDirection.Descending)
        ];
    }

    static get Alert() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.AlertDateTime, Enums.SortingDirection.Descending)
        ];
    }

    static get TrackingDatabase() {
        return [
            new SortingColumnInfo(Enums.SortingColumnName.TrackingDatabaseName, Enums.SortingDirection.Ascending)
        ];
    }
}

export default DefaultSorting;
import "jquery";
import ko from "knockout";
import "knockout-validation";
import "knockout-postbox";
import Utility from "./frameworks/core/utility";
import "lodash";

// Custom Handler registration
import "./frameworks/validations/custom-validations";

// Custom extenders registraion.
import "./frameworks/extenders/trackChange";
import "./frameworks/extenders/serverValidate";

// General config registration.
import "../components/config";

let allComponents = Utility.aggregateCollection({ 
    source: componentConfig.components,
    by: "items"
});

import "./frameworks/core/plugin";

// Perform register component from configuration file.
ko.utils.arrayForEach(componentConfig.bundles, function (bundle) {  
    ko.utils.arrayForEach(bundle.components, function(name) {
        let component = ko.utils.arrayFirst(allComponents, function(comp) {
            return comp.name === name;
        });

        if(component) {
            var path = component.path;
            var option = { require: path };

            if (path.indexOf("text!") === 0) {
                option = {
                    template: { require: path }
                };
            }

            if (!ko.components.isRegistered(name)){
                ko.components.register(name, option);
            } else {
                throw new Error("Duplicate regiter component " + name + " in bundle " + bundle.name);
            }
        } else {
            throw new Error("Invalid component " + name + " in bundle " + bundle.name);
        }
    });
});

// Initialize for validation options from https://github.com/Knockout-Contrib/Knockout-Validation/wiki/Configuration
ko.validation.init({
    decorateInputElement: true,
    decorateElementOnModified: true,
    errorElementClass: "ko-input-err",
    insertMessages: true,
    errorMessageClass: "ko-inline-msg-err",
    messagesOnModified: true
});

// Start the application
ko.applyBindings({}, document.getElementById("shell"));
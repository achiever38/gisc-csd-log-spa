// require.js looks for the following global when initializing
var require = {
    baseUrl: ".",
    waitSeconds:200,
    paths: {
        "crossroads": "bower_modules/crossroads/dist/crossroads.min",
        "hasher": "bower_modules/hasher/dist/js/hasher.min",
        "jquery": "bower_modules/jquery/dist/jquery",
        "knockout": "bower_modules/knockout/dist/knockout",
        "knockout-validation": "bower_modules/knockout-validation/dist/knockout.validation.min",
        "knockout-i18next": "bower_modules/i18next-ko/lib/i18next-ko.bundle",
        "knockout-postbox": "bower_modules/knockout-postbox/build/knockout-postbox.min",
        "signals": "bower_modules/js-signals/dist/signals.min",
        "text": "bower_modules/requirejs-text/text",
        "amcharts-pie": "bower_modules/amcharts3/amcharts/pie",
        "signalr": "bower_modules/signalr/jquery.signalR.min",
        "datatables.net": "bower_modules/datatables.net/js/jquery.dataTables.min",
        "datatables.net-responsive": "bower_modules/datatables.net-responsive/js/dataTables.responsive.min",
        "hammerjs": "bower_modules/hammerjs/hammer.min",
        "jquery.ui.widget": "bower_modules/blueimp-file-upload/js/vendor/jquery.ui.widget",
        "jquery-iframe-transport": "bower_modules/blueimp-file-upload/js/jquery.iframe-transport",
        "blueimp.fileupload": "bower_modules/blueimp-file-upload/js/jquery.fileupload",
        "jquery-ui": "bower_modules/jquery-ui/jquery-ui.min",
        "jquery-ui-datepicker": "bower_modules/jquery-ui/ui/widgets/datepicker",
        "jqueryui-timepicker-addon": "bower_modules/jqueryui-timepicker-addon/dist/jquery-ui-timepicker-addon.min",
        "moment": "bower_modules/moment/min/moment.min",
        "chart": "bower_modules/chart.js/dist/Chart.min",
        "markdown-it": "bower_modules/markdown-it/dist/markdown-it.min",
        "highlightjs": "bower_modules/highlightjs/highlight.pack.min",
        "jquery-alphanum": "bower_modules/jquery-alphanum/jquery.alphanum",
        "lodash": "bower_modules/lodash/dist/lodash.min",
        "jstree" : "bower_modules/jstree/dist/jstree.min",
        "jquery-minicolors":"bower_modules/jquery-minicolors/jquery.minicolors.min",
        "ms-dropdown": "bower_modules/ms-dropdown/js/msdropdown/jquery.dd.min",
        "full-calendar": "bower_modules/fullcalendar/dist/fullcalendar.min",
        "full-calendar-locale": "bower_modules/fullcalendar/dist/locale-all",
        "jquery-autogrow-textarea": "bower_modules/jquery-autogrow-textarea/dist/jquery.autogrow.min",
        "kendo.all.min": "vendors/kendo-ui/js/kendo.all.min",
        "select2": "bower_modules/select2/dist/js/select2.min",

        //amchart3
        "amcharts": "vendors/amchart/amcharts",
        "amcharts-serial": "vendors/amchart/serial",
        "amcharts-amstock": "vendors/amchart/amstock",
        "amcharts-donut": "vendors/amchart/pie",
        "amcharts-radar":"vendors/amchart/radar",

        //amchart4
        "am4core": "bower_modules/amcharts4/dist/script/core",
        "am4charts": "bower_modules/amcharts4/dist/script/charts",
        "am4themes_animated": "bower_modules/amcharts4/dist/script/themes/animated",

        "gridstack": "vendors/gridstack/gridstack",
        "gridstack-jQueryUI": "vendors/gridstack/gridstack.jQueryUI",
        "gridstack-all": "vendors/gridstack/gridstack.all",
        //"toastr": "bower_modules/toastr/toastr.min"
        //"jquery-ui-touch-punch": "vendors/jquery-ui-touch-punch/jquery.ui.touch-punch.min"

        //"gridstack": "bower_modules/gridstack/dist/gridstack",
        //"gridstack-jQueryUI": "bower_modules/gridstack/dist/gridstack.jQueryUI",
        //"gridstack-all": "bower_modules/gridstack/dist/gridstack.all",
        "elevatezoom": "vendors/elevatezoom/jquery.elevatezoom",
        "konva": "bower_modules/konva/konva",
        "photoswipe": "bower_modules/photoswipe/dist/photoswipe.min",
        "photoswipe-ui": "bower_modules/photoswipe/dist/photoswipe-ui-default.min",
        "hikvision": "vendors/hikvision/jsWebControl-1.0.0.min",

        "iqtech-swf": "vendors/iqtech/swfobject",
        "jsencrypt": "vendors/jsencrypt/jsencrypt.min",
        "videojs": "vendors/videojs/video.min",
        "flatpickr": "vendors/flatpickr/dist/flatpickr.min"
    },
    shim: {
        "knockout-validation": {
            deps: ["knockout"]
        },
        "knockout-i18next": {
            deps: ["knockout"]
        },
        "signalr": {
            deps: ["jquery"]
        },
        "blueimp.fileupload": {
            deps: ["jquery-iframe-transport"]
        },
        "jquery-alphanum":{
            deps:["jquery"]
        },
        "lodash": {
            exports: "_"
        },
        "full-calendar": {
            deps: ["jquery","moment"]
        },
        "amcharts-serial": {
            deps: ["amcharts"]
        },
        "amcharts-amstock": {
            deps: ["amcharts", "amcharts-serial"]
        },
        "amcharts-donut": {
            deps: ["amcharts"]
        },
        "amcharts-radar": {
            deps: ["amcharts"]
        },
        "gridstack": {
            deps: ["jquery", "jquery-ui","lodash"]
        },
        "gridstack-jQueryUI": {
            deps: ["gridstack"]
        },
        "gridstack-all": {
            deps: ["gridstack", "gridstack-jQueryUI"]
        },
        "hikvision": {
            deps: ["jquery"]
        },
        "iqtech-swf": {
            deps: ["jquery"]
        },
         "toastr": {
             deps: ["jquery"]
         },
        "jquery-ui-touch-punch": {
            deps: ["jquery", "jquery-ui"]
        },
        "am4charts": {
            deps: ["am4core"]
        },
        "am4themes_animated": {
            deps: ["am4core"]
        },
    },

    map: {
        '*': {
            'jquery-ui/data': 'jquery-ui',
            'jquery-ui/disable-selection': 'jquery-ui',
            'jquery-ui/focusable': 'jquery-ui',
            'jquery-ui/form': 'jquery-ui',
            'jquery-ui/ie': 'jquery-ui',
            'jquery-ui/keycode': 'jquery-ui',
            'jquery-ui/labels': 'jquery-ui',
            'jquery-ui/jquery-1-7': 'jquery-ui',
            'jquery-ui/plugin': 'jquery-ui',
            'jquery-ui/safe-active-element': 'jquery-ui',
            'jquery-ui/safe-blur': 'jquery-ui',
            'jquery-ui/scroll-parent': 'jquery-ui',
            'jquery-ui/tabbable': 'jquery-ui',
            'jquery-ui/unique-id': 'jquery-ui',
            'jquery-ui/version': 'jquery-ui',
            'jquery-ui/widget': 'jquery-ui',
            'jquery-ui/widgets/mouse': 'jquery-ui',
            'jquery-ui/widgets/draggable': 'jquery-ui',
            'jquery-ui/widgets/droppable': 'jquery-ui',
            'jquery-ui/widgets/resizable': 'jquery-ui',
        }
    }
};
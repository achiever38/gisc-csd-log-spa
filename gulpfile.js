// Node modules
var fs = require('fs'), vm = require('vm'), merge = require('deeply'), chalk = require('chalk'), es = require('event-stream'), path = require('path'), url = require('url'), slash = require('slash'), streamqueue = require('streamqueue');

// Gulp and plugins
var gulp = require('gulp'), rjs = require('gulp-requirejs-bundler-with-baseurl'), concat = require('gulp-concat'), filter = require('gulp-filter'),
    replace = require('gulp-replace'), uglify = require('gulp-uglify'), htmlreplace = require('gulp-html-replace'),
    connect = require('gulp-connect'), babelCore = require('babel-core'), babel = require('gulp-babel'), objectAssign = require('object-assign'),
    sass = require('gulp-sass'),
    gulpInsert = require('gulp-insert'),
    gulpIISExpress = require('gulp-iis-express'),
    msbuild = require('gulp-msbuild'),
    del = require('del'),
    gulpOpen = require('gulp-open'),
    gulpSequence = require('gulp-sequence'),
    jsdoc = require('gulp-jsdoc3'),
    cleanCSS = require('gulp-clean-css');
var KarmaServer = require('karma').Server;

// Config
var componentConfig = vm.runInNewContext(fs.readFileSync('src/components/config.js'));
var optimizerInclude = ['requireLib'];
var optimizerBundle = {};

// Utility function used by /src/components/config.js
function aggregateCollection(options) {
    var source = options.source;
    var byPropertyStr = options.by;

    if ((source === undefined) || (byPropertyStr === undefined)) {
        throw new Error("options.source or options.by is not defined");
    }

    var result = [];
    for (var i = 0; i < source.length; i++) {
        var subSource = source[i][byPropertyStr];
        result = result.concat(subSource);
    }
    return result;
}

// For current config.js structure
var allComponents = aggregateCollection({
    source: componentConfig.components,
    by: "items"
});
componentConfig.bundles.forEach(function (bundle) {
    bundle.components.forEach(function (name) {
        // Avoid for-each for performance here
        var component = null;
        for (var i = 0; i < allComponents.length; i++) {
            if (allComponents[i].name === name) {
                component = allComponents[i];
                break;
            }
        }
        if (component) {
            var componentPath = component.path;
            if (bundle.name == 'core') {
                // Adding to include list.
                optimizerInclude.push(componentPath);
            }
            else {
                // Adding to optimizer bundle.
                if (!optimizerBundle[bundle.name]) {
                    optimizerBundle[bundle.name] = [];
                }

                optimizerBundle[bundle.name].push(componentPath);
            }
        }
    });
});

var requireJsRuntimeConfig = vm.runInNewContext(fs.readFileSync('src/app/require.config.js') + '; require;'),
    requireJsOptimizerConfig = merge(requireJsRuntimeConfig, {
        out: 'gisc-spa.js',
        baseUrl: './src',
        name: 'app/startup',
        paths: {
            requireLib: 'bower_modules/requirejs/require'
        },
        include: optimizerInclude,
        insertRequire: ['app/startup'],
        bundles: optimizerBundle,
        bundleUrl: '/scripts'
    }),
    transpilationConfig = {
        root: 'src',
        skip: ['bower_modules/**', 'app/require.config.js', 'gis-js/**', 'vendors/**'],
        babelConfig: {
            modules: 'amd',
            sourceMaps: 'inline'
        }
    },
    babelIgnoreRegexes = transpilationConfig.skip.map(function (item) {
        return babelCore.util.regexify(item);
    });

// Pushes all the source files through Babel for transpilation
gulp.task('js:babel', function () {
    return gulp.src(requireJsOptimizerConfig.baseUrl + '/**')
        .pipe(es.map(function (data, cb) {
            if (!data.isNull()) {
                babelTranspile(data.relative, function (err, res) {
                    if (res) {
                        data.contents = new Buffer(res.code);
                    }
                    cb(err, data);
                });
            } else {
                cb(null, data);
            }
        }))
        .pipe(gulp.dest('./temp'));
});

// Discovers all AMD dependencies, concatenates together all required .js files, minifies them
gulp.task('js:optimize', ['js:babel'], function () {
    var config = objectAssign({}, requireJsOptimizerConfig, { baseUrl: 'temp' });
    return rjs(config)
        .pipe(uglify())
        .pipe(gulp.dest('./dist/scripts/'));
});

// Builds the distributable .js files by calling Babel then the r.js optimizer
gulp.task('js', ['js:optimize'], function () {
    // Now clean up
    //return del('./temp');
});

// Concatenates CSS files, rewrites relative paths to Bootstrap fonts, copies Bootstrap fonts
gulp.task('css', function () {
    var dataTablesCore = gulp.src('src/bower_modules/datatables.net-dt/css/jquery.dataTables.min.css'),
        dataTablesResponsive = gulp.src('src/bower_modules/datatables.net-responsive-dt/css/responsive.dataTables.min.css'),
        fontAwesomeCss = gulp.src('src/bower_modules/font-awesome/css/font-awesome.min.css'),
        datePicker = gulp.src('src/bower_modules/jquery-ui/themes/base/jquery-ui.min.css'),
        timePicker = gulp.src('src/bower_modules/jqueryui-timepicker-addon/dist/jquery-ui-timepicker-addon.min.css'),
        appCss = gulp.src('src/css/*.css'),
        miniColor = gulp.src('src/bower_modules/jquery-minicolors/jquery.minicolors.css'),
        msDropdown = gulp.src('src/bower_modules/ms-dropdown/css/msdropdown/dd.css').pipe(replace('url(../../images', 'url(../images')),
        kendoCommon = gulp.src('src/vendors/kendo-ui/styles/kendo.common.min.css').pipe(replace('fonts/glyphs', '../fonts')),
        kendoTheme = gulp.src('src/vendors/kendo-ui/styles/kendo.material.min.css').pipe(replace('Material', '../images')),
        kendoMobile = gulp.src('src/vendors/kendo-ui/styles/kendo.mobile.all.min.css').pipe(replace('images', '../fonts')),
        fullCalendarCss = gulp.src('src/bower_modules/fullcalendar/dist/fullcalendar.min.css'),
        fullCalendarPrintCss = gulp.src('src/bower_modules/fullcalendar/dist/fullcalendar.print.min.css'),
        gridBootstrap = gulp.src('css/grid.min.css'),
        select2 = gulp.src('src/bower_modules/select2/dist/css/select2.min.css'),
        gridstackMinified = gulp.src('src/vendors/gridstack/gridstack.min.css'),
        gridstackCustom = gulp.src('src/vendors/gridstack/gridstack-custom.css'),
        toastr = gulp.src('src/bower_modules/toastr/toastr.min.css'),
        photoswipe = gulp.src('src/bower_modules/photoswipe/dist/photoswipe.css'),
        photoswipeUI = gulp.src('src/bower_modules/photoswipe/dist/default-skin/default-skin.css').pipe(replace('url(', 'url(../images/'))
        videojs = gulp.src('src/vendors/videojs/video-js.min.css'),
        flatpickr = gulp.src('src/vendors/flatpickr/dist/flatpickr.min.css'),
        fontAwesome = gulp.src('src/bower_modules/font-awesome/css/font-awesome.min.css')

    // Copy stylesheets to content folder.
    streamqueue({ objectMode: true }, dataTablesCore, dataTablesResponsive, fontAwesomeCss, datePicker, timePicker, miniColor, msDropdown, appCss,
        kendoCommon, kendoTheme, kendoMobile, fullCalendarCss, gridBootstrap, select2, gridstackMinified, gridstackCustom, toastr, photoswipe, photoswipeUI, videojs, flatpickr, fontAwesome)
        .pipe(concat('gisc-spa.css'))
        .pipe(cleanCSS({ keepSpecialComments: 0 }))
        .pipe(gulp.dest('./dist/content/'));

    // Prepare css resource files.
    var customFontFiles = gulp.src('./src/fonts/*', { base: './src/' });
    var fontFiles = gulp.src('./src/bower_modules/font-awesome/fonts/*', { base: './src/bower_modules/font-awesome/' });
    var dataTableImages = gulp.src('./src/bower_modules/datatables.net-dt/images/*', { base: './src/bower_modules/datatables.net-dt' });
    var msDropdownImages = gulp.src('./src/bower_modules/ms-dropdown/images/msdropdown/**/*', { base: './src/bower_modules/ms-dropdown' });
    var kendoFonts = gulp.src('./src/vendors/kendo-ui/styles/theme/fonts/*', { base: './src/vendors/kendo-ui/styles/theme/' });
    var kendoImages = gulp.src('./src/vendors/kendo-ui/styles/theme/images/*', { base: './src/vendors/kendo-ui/styles/theme/' });
    var amChartImage = gulp.src('./src/vendors/amChart/images/*', { base: './src/vendors/amChart/' });
    return streamqueue({ objectMode: true }, customFontFiles, fontFiles, dataTableImages, msDropdownImages, kendoFonts, kendoImages, amChartImage)
        .pipe(gulp.dest('./dist/'));
});

//process sass map
gulp.task('map:sass', function () {
    //return gulp.src(['./src/gis-js/scss/*.scss', '!./src/gis-js/scss/theme.scss'])
    // .pipe(sass().on('error', sass.logError))
    // .pipe(gulp.dest('./src/gis-js/css'));

    return gulp.src(['./src/gis-js/scss/main-theme.scss', './src/gis-js/scss/print-theme.scss'])
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./src/gis-js/css'));
});

gulp.task('map:css', function () {
    return gulp.src('./src/gis-js/css/*')
     .pipe(gulp.dest('./dist/gis-js/css'));
});

// Pre process SASS file to general CSS file.
gulp.task('sass', function () {
    return gulp.src('./src/css/styles.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest('./src/css'));
});

// Auto complie SASS file to CSS.
gulp.task('sass:watch', function () {
    gulp.watch('./src/css/*.scss', ['sass']);
});

// Copies index.html, replacing <script> and <link> tags to reference production URLs
gulp.task('html', function () {

    var currentBuild = (new Date().toString());

    var favicon = gulp.src('./src/favicon.ico');
    var staticImages = gulp.src('./src/images/*', { base: './src/' });
    var staticMedia = gulp.src('./src/media/*', { base: './src/' });
    var gisScripts = gulp.src(['./src/gis-js/**/*','!./src/gis-js/scss/*'], { base: './src/' });
    var gisHtmls = gulp.src(['./src/MapPage.html', './src/PrintMap.html', './src/PrintRoute.html']);
    var fakeLoginHtml = gulp.src('./src/login.html');
    var fakeChangePwdHtml = gulp.src('./src/change-password.html');
    var indexHtml = gulp.src('./src/index.html')
        .pipe(htmlreplace({
            'css': 'content/gisc-spa.css',
            'js': 'scripts/gisc-spa.js',
            'version': '<script>var buildVersion = \'' + currentBuild + '\';</script>'
        }));
    var reportViewerTemplates = gulp.src('./src/report-viewer/templates/*', { base: './src/' });
    
    // Copy all tasks to dist folder.
    return streamqueue({ objectMode: true }, favicon, staticImages, staticMedia, gisScripts, gisHtmls, indexHtml, fakeLoginHtml, fakeChangePwdHtml, reportViewerTemplates)
        .pipe(gulp.dest('./dist/'));
});

var mvcProjectPath = '../Gisc.Csd.Log.Web/';

gulp.task('build:mvc', function (callback) {
    // Please make sure all copied resources files should be added to MVC CS project.
    // Web Publishing will copy web resources within project configuration only.
    console.log('\nCopy Client resource file to MVC project');
    gulpSequence('default', 'build:mvcScripts', 'build:mvcCSS', 'build:mvcImage', 'build:mvcFont', 'build:mvcMedia', 'build:mvcMap', 'build:reportViewer')(callback);
});

gulp.task('build:mvcResources', function (callback) {
    // Please make sure all copied resources files should be added to MVC CS project.
    // Web Publishing will copy web resources within project configuration only.
    console.log('\nCopy Client resource files to MVC project');
    gulpSequence('build:mvcScripts', 'build:mvcCSS', 'build:mvcImage', 'build:mvcFont', 'build:mvcMedia')(callback);
});

gulp.task('build:mvcMap', function () {
    console.log('\nCopy Gis resource files to root site.');
    var gisScripts = gulp.src('./dist/gis-js/**/*', { base: './dist/' });

    //return es.concat(gisScripts, gisHtmls)
    //    .pipe(gulp.dest(mvcProjectPath));

    return gisScripts.pipe(gulp.dest(mvcProjectPath));
});

gulp.task('build:mvcScripts', function () {
    // Current gulp-reuqire-bundle doesn't support custom baseUrl
    // Scripts will be loaded with current document relatively.
    // Final app required to run at root domain only.
    console.log('\nCopy Javascript resource files to root site.');
    gulp.src('./dist/scripts/*.js')
        .pipe(gulp.dest(mvcProjectPath + 'Scripts/'));
});

gulp.task('build:mvcCSS', function () {
    console.log('\nCopy CSS files to root site.');
    gulp.src('./dist/content/*.css')
        .pipe(gulp.dest(mvcProjectPath + 'Content/'));
});

gulp.task('build:mvcImage', function () {
    console.log('\nCopy image resource files to root site.');
    gulp.src('./dist/images/**/*')
        .pipe(gulp.dest(mvcProjectPath + 'Images/'));
});

gulp.task('build:mvcFont', function () {
    console.log('\nCopy font resource files to root site.');
    gulp.src('./dist/fonts/*')
        .pipe(gulp.dest(mvcProjectPath + 'fonts/'));
});

gulp.task('build:mvcMedia', function () {
    console.log('\nCopy font media files to root site.');
    gulp.src('./dist/media/*')
        .pipe(gulp.dest(mvcProjectPath + 'media/'));
});

gulp.task('build:reportViewer', function () {
    console.log('\nCopy font media files to root site.');
    gulp.src('./dist/report-viewer/templates/*')
        .pipe(gulp.dest(mvcProjectPath + 'report-viewer/templates/'));
});

// Generate document with jsdoc, the output is under ./docs
// Using default docstrap template.
// Create jsdoc.json for additional settings. See https://www.npmjs.com/package/gulp-jsdoc3
gulp.task('doc', function (cb) {
    gulp.src(['./src/app/**/*.js'], { read: false })
        .pipe(jsdoc(cb));
});

// Run test with karma once then exit, suit for Continuos Integration
gulp.task('test', function (cb) {
    new KarmaServer({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true
    }, cb).start();
});

// Watch for file changes and re-run tests on each change
gulp.task('tdd', function (cb) {
    new KarmaServer({
        configFile: __dirname + '/karma.conf.js'
    }, cb).start();
});

// Cleanup Tasks.
gulp.task('clean', function (callback) {
    gulpSequence('clean:dist', 'clean:temp')(callback);
});
gulp.task('clean:dist', function () {
    return del('./dist');
});
gulp.task('clean:temp', function () {
    return del('./temp');
});

// Starts a simple static file server that transpiles ES6 on the fly to ES5
gulp.task('serve:src', ['open:app'], function () {
    // We do not keep css file in git history, so need to compile scss file whenever start server
    gulpSequence('sass:watch', 'map:sass', function () {
        return connect.server({
            root: transpilationConfig.root,
            middleware: function (connect, opt) {
                return [
                    function (req, res, next) {
                        var pathname = path.normalize(url.parse(req.url).pathname);
                        babelTranspile(pathname, function (err, result) {
                            if (err) {
                                next(err);
                            } else if (result) {
                                res.setHeader('Content-Type', 'application/javascript');
                                res.end(result.code);
                            } else {
                                next();
                            }
                        });
                    }
                ];
            }
        });
    });

});

// After building, starts a trivial static file server
gulp.task('serve:dist', ['default'], function () {
    return connect.server({ root: './dist' });
});

function babelTranspile(pathname, callback) {
    pathname = slash(pathname);
    if (babelIgnoreRegexes.some(function (re) { return re.test(pathname); })) return callback();
    if (!babelCore.canCompile(pathname)) return callback();
    var src = path.join(transpilationConfig.root, pathname);
    var opts = objectAssign({ sourceFileName: '/source/' + pathname }, transpilationConfig.babelConfig);
    babelCore.transformFile(src, opts, callback);
}

var msbuildOption = {
    targets: ['Clean', 'Build'],
    toolsVersion: 14
};

gulp.task('msbuild:mvc', function () {
    return gulp.src('../Gisc.Csd.Log.API.Core/Gisc.Csd.Log.Web.csproj')
		.pipe(msbuild(msbuildOption));
});

gulp.task('msbuild:apicore', function () {
    return gulp.src('../Gisc.Csd.Log.API.Core/Gisc.Csd.Log.API.Core.csproj')
		.pipe(msbuild(msbuildOption));
});

gulp.task('msbuild:signalr', function () {
    return gulp.src('../Gisc.Csd.Log.SignalR/Gisc.Csd.Log.SignalR.csproj')
		.pipe(msbuild(msbuildOption));
});

gulp.task('msbuild:apitrackingcore', function () {
    return gulp.src('../Gisc.Csd.Log.SignalR/Gisc.Csd.Log.API.TrackingCore.csproj')
		.pipe(msbuild(msbuildOption));
});

gulp.task('serve:mvc', ['msbuild:mvc'], function () {
    return gulpIISExpress({
        port: '40062',
        appPath: path.resolve('../Gisc.Csd.Log.Web')
    });
});

gulp.task('serve:apicore', ['msbuild:apicore'], function () {
    return gulpIISExpress({
        port: '40060',
        appPath: path.resolve('../Gisc.Csd.Log.API.Core')
    });
});

gulp.task('serve:signalr', ['msbuild:signalr'], function () {
    var configuration = {
        port: '40061',
        appPath: path.resolve('../Gisc.Csd.Log.SignalR')
    };
    return gulpIISExpress(configuration);
});

gulp.task('serve:apitrackingcore', ['msbuild:apitrackingcore'], function () {
    var configuration = {
        port: '40065',
        appPath: path.resolve('../Gisc.Csd.Log.API.TrackingCore')
    };
    return gulpIISExpress(configuration);
});

gulp.task('default', function (callback) {
    gulpSequence('clean:dist', 'clean:temp', 'html', 'sass', 'map:sass', 'css', 'map:css', 'js:optimize', 'insert:buildVersion', 'build:success')(callback);
});

gulp.task('transpiler', function (callback) {
    gulpSequence('clean:temp', 'js:babel')(callback);
});

gulp.task('insert:buildVersion', function (cb) {

    gulp.src('./dist/scripts/gisc-spa.js')
        .pipe(gulpInsert.append(';var buildVersion =\'' + (new Date().toString()) + '\';'))
        .pipe(gulp.dest('./dist/scripts/'));

    cb();
});

gulp.task('build:success', function (callback) {
    console.log('\nPlaced optimized files in ' + chalk.magenta('dist/\n'));
    callback();
});

gulp.task('open:app', function (callback) {
    return gulp.src('')
        .pipe(gulpOpen({ app: 'chrome', uri: 'http://localhost:8080' }));
});

gulp.task('help:list', function () {
    console.log('help:es6');
    console.log('help:knockout');
    console.log('help:scss');
});

gulp.task('help:es6', function () {
    gulp.src('')
        .pipe(gulpOpen({ app: 'chrome', uri: 'http://es6-features.org' }));
});

gulp.task('help:knockout', function () {
    gulp.src('')
        .pipe(gulpOpen({ app: 'chrome', uri: 'http://knockoutjs.com/documentation/introduction.html' }));
});

gulp.task('help:scss', function () {
    gulp.src('')
        .pipe(gulpOpen({ app: 'chrome', uri: 'http://sass-lang.com/guide' }));
});
We have defined our own snippets for specific languages. 
Snippets are defined in a JSON format and stored in a per user (languageId).json file.
    - Knockout View Model (javascript.json)
    - Knockout Template (html.json)

To open up a snippet file for editing, open User Snippets under File > Preferences (Code > Preferences on Mac OS X) and select the language for which the snippets should appear.